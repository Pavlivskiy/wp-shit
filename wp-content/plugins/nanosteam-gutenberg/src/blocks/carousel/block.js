/**
 * BLOCK: Nano Alert
 *
 * Registering a basic block with Gutenberg.
 */

import icons from '../../common/icons';

/**
 * Internal block libraries
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const {
	InnerBlocks,
	ColorPalette,
	InspectorControls,
	Fragment,
} = wp.editor;

const {
	TabPanel,
	Dashicon,
	PanelBody,
	RangeControl,
	SelectControl,
} = wp.components;

const {
	withState,
} = wp.compose;

// Importing Common Libraries
import colors from '../../common/nanoBrandColors.js';
import nanosteamHexToName from '../../common/hexToBrand.js';

function nanosteamHexToRGB( hex, alpha ) {
	hex = hex.replace( '#', '' );
	const r = parseInt( hex.length === 3 ? hex.slice( 0, 1 ).repeat( 2 ) : hex.slice( 0, 2 ), 16 );
	const g = parseInt( hex.length === 3 ? hex.slice( 1, 2 ).repeat( 2 ) : hex.slice( 2, 4 ), 16 );
	const b = parseInt( hex.length === 3 ? hex.slice( 2, 3 ).repeat( 2 ) : hex.slice( 4, 6 ), 16 );
	return 'rgba(' + r + ', ' + g + ', ' + b + ', ' + alpha + ')';
}

const catSelections = [];

const allCategories = wp.apiFetch( { path: '/wp/v2/project_category/?per_page=-1' } ).then( tax => {
	catSelections.push( { label: 'All Categories', value: 0 } );
	$.each( tax, function( key, val ) {
		//console.log(val.name);
		catSelections.push( { label: val.name, value: val.id } );
	} );

	return catSelections;
} );

/**
 * Update Column
 */
function carouselColumns( deskColumns, tabColumns, mobColumns ) {
	const deskCol = 'col-lg-' + ( 12 / deskColumns );
	const tabCol = 'col-sm-' + ( 12 / tabColumns );
	const mobCol = 'col-' + ( 12 / mobColumns );

	return [ deskCol, tabCol, mobCol ];
}

/**
 * Register: a Gutenberg Block.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */

registerBlockType( 'nanosteam/carousel', {
	title: __( 'Projects Carousel' ),
	icon: 'slides',
	category: 'common',
	attributes: {
		deskColumns: {
			type: 'number',
			default: 3,
		},
		tabColumns: {
			type: 'number',
			default: 2,
		},
		mobColumns: {
			type: 'number',
			default: 1,
		},
		category: {
			type: 'Number',
			default: 0,
		},
		orderby: {
			type: 'string',
			default: 'date',
		},
		order: {
			type: 'string',
			default: 'ASC',
		},
		max: {
			type: 'number',
			default: 8,
		},
	},
	edit: props => {
		const { attributes: { deskColumns, tabColumns, mobColumns, category, orderby, order, max }, setAttributes } = props;
		const orderbyArr = [
			{ value: 'date', label: __( 'Date' ) },
			{ value: 'days_left', label: __( 'Days Left' ) },
			{ value: 'percent_raised', label: __( 'Percent Raised' ) },
			{ value: 'funds_raised', label: __( 'Funds Raised' ) },
			{ value: 'featured', label: __( 'Featured' ) },
			{ value: 'popular', label: __( 'Popularity' ) },
			{ value: 'zip', label: __( 'Zip' ) },
		];
		let sortRes;

		sortRes = orderbyArr.filter( p => p.value === orderby );

		return (
			<div className={ 'nanosteam-carousel' }>

				<InspectorControls>
					<h2>{ __( 'Categories' ) }</h2>
					<SelectControl
						value={ category }
						options={ catSelections }
						onChange={ ( value ) => {
							setAttributes( {
								category: value,
							} );
						} }
					/>
					<h2>{ __( 'Sort by' ) }</h2>
					<SelectControl
						value={ orderby }
						options={ orderbyArr }
						onChange={ ( value ) => {
							setAttributes( {
								orderby: value,
							} );
						} }
					/>
					<h2>{ __( 'Order by' ) }</h2>
					<SelectControl
						value={ order }
						options={ [
							{ value: 'ASC', label: __( 'Ascending' ) },
							{ value: 'DESC', label: __( 'Decending' ) },
						] }
						onChange={ ( value ) => {
							setAttributes( {
								order: value,
							} );
						} }
					/>
					<h2>{ __( 'Columns' ) }</h2>
					<RangeControl
						label={ __( 'Desktop Columns' ) }
						value={ ( deskColumns ? deskColumns : '' ) }
						onChange={ ( value ) => setAttributes( { deskColumns: value } ) }
						min="1"
						max="4"
						step="1"
					/>
					<RangeControl
						label={ __( 'Tablet Columns' ) }
						value={ ( tabColumns ? tabColumns : '' ) }
						onChange={ ( value ) => setAttributes( { tabColumns: value } ) }
						min="1"
						max="4"
						step="1"
					/>
					<RangeControl
						label={ __( 'Mobile Columns' ) }
						value={ ( mobColumns ? mobColumns : '' ) }
						onChange={ ( value ) => setAttributes( { mobColumns: value } ) }
						min="1"
						max="4"
						step="1"
					/>
					<h2>{ __( 'Max to show' ) }</h2>
					<RangeControl
						value={ ( max ? max : '' ) }
						onChange={ ( value ) => setAttributes( { max: value } ) }
						min="2"
						max="16"
						step="2"
					/>
				</InspectorControls>

				<div className={ 'row small bg-light px-3' }>
					<div className={ 'col-12' }>
						<h2>Projects Carousel sorted by: { sortRes[ 0 ].label }</h2>
					</div>
				</div>
			</div>
		);
	},

	save( { attributes } ) {
	    // console.log(attributes);

		return null;
	},

} );
