/**
 * BLOCK: Nanosteam Advanced Btn
 */
import times from 'lodash/times';
import GenIcon from '../../common/icons/genicon';
import Ico from '../../common/icons/svgicons';
import FaIco from '../../common/icons/faicons';

/**
 * Import Icons
 */
import icons from './icon';

// Import Nanosteam Common
import nanosteamHexToName from '../../common/hexToBrand.js';

/**
 * Import Css
 */
import './style.scss';
import './editor.scss';

import edit from './edit';
/**
 * Internal block libraries
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { RichText } = wp.editor;

/**
 * Register: a Gutenberg Block.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'nanosteam/accordion', {
	title: __( 'Accordion' ), // Block title.
	icon: 'align-center',
	category: 'common',
	keywords: [
		__( 'Accordion' ),
		__( 'Icon' ),
	],
	attributes: {
		uniqueID: {
			type: 'string',
			default: '',
		},
		accordionCount: {
			type: 'number',
			default: 1,
		},
		headingCol: {
			type: 'string',
			default: '',
		},
		headingBgCol: {
			type: 'string',
			default: '-light',
		},
		textCol: {
			type: 'string',
			default: '',
		},
		level: {
			type: 'number',
			default: 5,
		},
		weight: {
			type: 'string',
			default: 'heading-light',
		},
		accds: {
			type: 'array',
			default: [ {
				text: '',
				headingText: '',
			} ],
		},
	},
	edit,
	save: props => {
		const { attributes: { accordionCount, accds, uniqueID, textCol, headingBgCol, headingCol, weight, level } } = props;
		//const accordionCount = 1;
		const Heading = 'h' + ( undefined !== level ? level : '3' );
		const renderSaveBtns = ( index ) => {
			const textColor = textCol ? 'text' + nanosteamHexToName( textCol ) : '';
			const headingBgColor = headingBgCol ? 'btn' + nanosteamHexToName( headingBgCol ) : '';
			const headingColor = headingCol ? 'text' + nanosteamHexToName( headingCol ) : '';

			return (

				<div className="card">
					<div className="p-0 m-0 card-header" id={ `heading${ index }` }>
						<a className={ `btn btn-block ${ headingColor } ${ headingBgColor } ${ Heading } ${ weight } text-left px-3` }
							data-toggle="collapse" data-target={ `#collapse${ index }` }
							aria-expanded={ `${ ( 0 === index ? 'true' : 'false' ) }` }
							aria-controls={ `collapse${ index }` }>
							<RichText.Content value={ accds[ index ].headingText } />
						</a>
					</div>

					<div id={ `collapse${ index }` } className={ `collapse ${ ( 0 === index ? 'show' : '' ) }` } aria-labelledby={ `heading${ index }` }
						data-parent={ `accordion${ uniqueID }` }>
						<div className={ `card-body ${ textColor }` }>
							<RichText.Content value={ accds[ index ].text } />
						</div>
					</div>
				</div>

			);
		};
		return (
			<div id={ `accordion${ uniqueID }` }>
				{ times( accordionCount, n => renderSaveBtns( n ) ) }
			</div>
		);
	},
} );
