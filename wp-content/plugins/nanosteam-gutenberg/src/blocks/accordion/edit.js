/**
 * BLOCK: Nanosteam Advanced Btn
 *
 * Editor for accordion
 */
import times from 'lodash/times';
import colors from '../../common/nanoBrandColors.js';
import nanosteamHexToName from '../../common/hexToBrand.js';

/**
 * Import Css
 */
import './editor.scss';
import range from 'lodash/range';
/**
 * Internal block libraries
 */
const { __, sprintf } = wp.i18n;
const {
	RichText,
	URLInput,
	InspectorControls,
	ColorPalette,
	BlockControls,
	AlignmentToolbar,
} = wp.editor;
const {
	Component,
	Fragment,
} = wp.element;
const {
	IconButton,
	TabPanel,
	PanelBody,
	RangeControl,
	Toolbar,
	RadioControl,
	TextControl,
	SelectControl,
} = wp.components;

class NanosteamAccordion extends Component {
	constructor() {
		super( ...arguments );
		this.state = {
			btnFocused: 'false',
		};
		// Initial state
		this.state = { open: false };
	}
	componentDidMount() {
		if ( ! this.props.attributes.uniqueID ) {
			const blockConfig = nanosteam_blocks_params.config[ 'nanosteam/accordion' ];
			if ( blockConfig !== undefined && typeof blockConfig === 'object' ) {
				Object.keys( blockConfig ).map( ( attribute ) => {
					this.props.attributes[ attribute ] = blockConfig[ attribute ];
				} );
			}
			this.props.setAttributes( {
				uniqueID: '_' + this.props.clientId.substr( 2, 9 ),
			} );
		} else if ( this.props.attributes.uniqueID && this.props.attributes.uniqueID !== '_' + this.props.clientId.substr( 2, 9 ) ) {
			this.props.setAttributes( {
				uniqueID: '_' + this.props.clientId.substr( 2, 9 ),
			} );
		}
	}
	componentDidUpdate( prevProps ) {
		if ( ! this.props.isSelected && prevProps.isSelected && this.state.btnFocused ) {
			this.setState( {
				btnFocused: 'false',
			} );
		}
	}
	saveArrayUpdate( value, index ) {
		const { attributes, setAttributes } = this.props;
		const { accds } = attributes;

		const newItems = accds.map( ( item, thisIndex ) => {
			if ( index === thisIndex ) {
				item = { ...item, ...value };
			}

			return item;
		} );
		setAttributes( {
			accds: newItems,
		} );
	}

	toggle() {
		this.setState( {
			open: ! this.state.open,
		} );
	}

	render() {
		const { attributes: { uniqueID, accordionCount, accds, level, hAlign, headingCol, textCol, headingBgCol, weight, cssClass, letterSpacing, fontStyle, fontWeight, typography, googleFont, loadGoogleFont, fontSubset, fontVariant }, className, setAttributes, isSelected } = this.props;
		const Heading = 'h' + ( undefined !== level ? level : '3' );
		const headingBGString = ( headingBgCol ? nanosteamHexToName( headingBgCol ) : '' );
		const headingColString = ( headingCol ? nanosteamHexToName( headingCol ) : '' );
		const textColString = ( textCol ? nanosteamHexToName( textCol ) : '' );
		const createLevelControl = ( targetLevel ) => {
			return [ {
				icon: 'heading',
				// translators: %s: heading level e.g: "1", "2", "3"
				title: sprintf( __( 'Heading %d' ), targetLevel ),
				value: level,
				isActive: targetLevel === level,
				onClick: () => setAttributes( { level: targetLevel } ),
				subscript: String( targetLevel ),
			} ];
		};

		const renderAccordion = ( index ) => {
			return (
				<div className="card" style={ {
					maxWidth: '100%',
				} }>
					<div className="p-0 m-0 card-header" id={ `heading${ index }` }>
						<a className={ `btn btn-block text${ headingColString } btn${ headingBGString } ${ Heading } ${ weight } text-left` }
							data-toggle="collapse" data-target={ `#collapse${ index }` }
							onClick={ this.toggle.bind( this ) }
							aria-expanded={ `${ ( 0 === index ? 'true' : 'false' ) }` } aria-controls={ `collapse${ index }` }>
							<RichText
								tagName={ Heading }
								format="string"
								placeholder={ __( 'Heading Text...' ) }
								value={ accds[ index ].headingText }
								unstableOnFocus={ () => {
									if ( 1 === index ) {
										onFocusAccd1();
									} else if ( 2 === index ) {
										onFocusAccd2();
									} else if ( 3 === index ) {
										onFocusAccd3();
									} else if ( 4 === index ) {
										onFocusAccd4();
									} else if ( 5 === index ) {
										onFocusAccd4();
									} else if ( 6 === index ) {
										onFocusAccd4();
									} else if ( 7 === index ) {
										onFocusAccd4();
									} else if ( 8 === index ) {
										onFocusAccd4();
									} else if ( 9 === index ) {
										onFocusAccd4();
									} else {
										onFocusAccd();
									}
								} }
								onChange={ value => {
									this.saveArrayUpdate( { headingText: value }, index );
								} }
								className={ `p-0 m-0 ns-heading-text${ uniqueID } ${ weight }` }
								formattingControls={ [ '' ] }
								keepPlaceholderOnFocus
							/>
						</a>

					</div>

					<div id={ `collapse${ index } ` } className={ `collapse ${ ( 0 === index ? 'show' : 'show' ) }` + ( this.state.open ? 'show' : '' ) } aria-labelledby={ `heading${ index }` }
						data-parent="#accordion">
						<div className="card-body">
							<RichText
								tagName="div"
								format="string"
								placeholder={ __( 'Lorem Ipsum...' ) }
								value={ accds[ index ].text }
								unstableOnFocus={ () => {
									if ( 1 === index ) {
										onFocusAccd1();
									} else if ( 2 === index ) {
										onFocusAccd2();
									} else if ( 3 === index ) {
										onFocusAccd3();
									} else if ( 4 === index ) {
										onFocusAccd4();
									} else if ( 5 === index ) {
										onFocusAccd4();
									} else if ( 6 === index ) {
										onFocusAccd4();
									} else if ( 7 === index ) {
										onFocusAccd4();
									} else if ( 8 === index ) {
										onFocusAccd4();
									} else if ( 9 === index ) {
										onFocusAccd4();
									} else {
										onFocusAccd();
									}
								} }
								onChange={ value => {
									this.saveArrayUpdate( { text: value }, index );
								} }
								formattingControls={ [ 'bold', 'italic', 'strikethrough' ] }
								className={ `ns-body-text${ uniqueID } text${ textColString }` }
								keepPlaceholderOnFocus
							/>
						</div>
					</div>
				</div>

			);
		};
		const onFocusAccd = () => {
			if ( 'btn0' !== this.state.btnFocused ) {
				this.setState( {
					btnFocused: 'btn0',
				} );
			}
		};
		const onFocusAccd1 = () => {
			if ( 'btn1' !== this.state.btnFocused ) {
				this.setState( {
					btnFocused: 'btn1',
				} );
			}
		};
		const onFocusAccd2 = () => {
			if ( 'btn2' !== this.state.btnFocused ) {
				this.setState( {
					btnFocused: 'btn2',
				} );
			}
		};
		const onFocusAccd3 = () => {
			if ( 'btn3' !== this.state.btnFocused ) {
				this.setState( {
					btnFocused: 'btn3',
				} );
			}
		};
		const onFocusAccd4 = () => {
			if ( 'btn4' !== this.state.btnFocused ) {
				this.setState( {
					btnFocused: 'btn4',
				} );
			}
		};
		const renderPreviewArray = (
			<div id={ 'accordion' }>
				{ times( accordionCount, n => renderAccordion( n ) ) }
			</div>
		);
		return (
			<Fragment>

				<div id={ `ns-accordion-${ uniqueID }` } className={ `ns-accordion ${ className }` }>
					<BlockControls>
						<AlignmentToolbar
							value={ hAlign }
							onChange={ ( value ) => setAttributes( { hAlign: value } ) }
						/>
					</BlockControls>
					<InspectorControls>
						<PanelBody
							title={ __( 'Accordion Count' ) }
							initialOpen={ true }
						>
							<RangeControl
								label={ __( 'Number of sections' ) }
								value={ accordionCount }
								onChange={ newcount => {
									const newAccds = accds;
									if ( newAccds.length < newcount ) {
										const amount = Math.abs( newcount - newAccds.length );
										{ times( amount, n => {
											newAccds.push( {
												text: newAccds[ 0 ].text,
												headingText: newAccds[ 0 ].headingText,
											} );
										} ); }
										setAttributes( { accds: newAccds } );
									}
									setAttributes( { accordionCount: newcount } );
								} }
								min={ 1 }
								max={ 10 }
							/>
						</PanelBody>
						<PanelBody
							title={ __( 'Accordion Settings' ) }
							initialOpen={ false }
						>
							<p>{ __( 'HTML Tag' ) }</p>
							<Toolbar controls={ range( 1, 7 ).map( createLevelControl ) } />
							<RadioControl
								label="Heading Weight"
								selected={ weight }
								options={ [
									{ label: 'Bold', value: '' },
									{ label: 'Normal', value: 'heading-light' },
								] }
								onChange={ ( value ) => setAttributes( { weight: value } ) }
							/>
							<h2 className="ns-tab-wrap-title">{ __( 'Color Settings' ) }</h2>
							<p className="ns-setting-label">{ __( 'Heading Color' ) }</p>
							<ColorPalette
								colors={ colors }
								value={ headingCol }
								onChange={ ( value ) => setAttributes( { headingCol: value } ) }
							/>
							<p className="ns-setting-label">{ __( 'Heading Background Color' ) }</p>
							<ColorPalette
								colors={ colors }
								value={ headingBgCol }
								onChange={ ( value ) => setAttributes( { headingBgCol: value } ) }
							/>
							<p className="ns-setting-label">{ __( 'Text Color' ) }</p>
							<ColorPalette
								colors={ colors }
								value={ textCol }
								onChange={ ( value ) => setAttributes( { textCol: value } ) }
							/>
							<TextControl
								label={ __( 'Add Custom CSS Class' ) }
								value={ cssClass }
								onChange={ ( value ) => setAttributes( { cssClass: value } ) }
							/>
						</PanelBody>
					</InspectorControls>
					{ renderPreviewArray }
				</div>
			</Fragment>
		);
	}
}
export default ( NanosteamAccordion );
