/**
 * BLOCK: Nanosteam Advanced Btn
 *
 * Editor for Advanced Btn
 */
import times from 'lodash/times';
import colors from '../../common/nanoBrandColors.js';
import nanosteamHexToName from '../../common/hexToBrand.js';

/**
 * Import Css
 */
import './editor.scss';
/**
 * Internal block libraries
 */
const { __ } = wp.i18n;
const {
	RichText,
	URLInput,
	InspectorControls,
	ColorPalette,
	BlockControls,
	AlignmentToolbar,
} = wp.editor;
const {
	Component,
	Fragment,
} = wp.element;
const {
	IconButton,
	TabPanel,
	PanelBody,
	RangeControl,
	RadioControl,
	TextControl,
	SelectControl,
} = wp.components;

class NanosteamAdvancedButton extends Component {
	constructor() {
		super( ...arguments );
		this.state = {
			btnFocused: 'false',
		};
	}
	componentDidMount() {
		if ( ! this.props.attributes.uniqueID ) {
			const blockConfig = nanosteam_blocks_params.config[ 'nanosteam/advancedbtn' ];
			if ( blockConfig !== undefined && typeof blockConfig === 'object' ) {
				Object.keys( blockConfig ).map( ( attribute ) => {
					this.props.attributes[ attribute ] = blockConfig[ attribute ];
				} );
			}
			this.props.setAttributes( {
				uniqueID: '_' + this.props.clientId.substr( 2, 9 ),
			} );
		} else if ( this.props.attributes.uniqueID && this.props.attributes.uniqueID !== '_' + this.props.clientId.substr( 2, 9 ) ) {
			this.props.setAttributes( {
				uniqueID: '_' + this.props.clientId.substr( 2, 9 ),
			} );
		}
	}
	componentDidUpdate( prevProps ) {
		if ( ! this.props.isSelected && prevProps.isSelected && this.state.btnFocused ) {
			this.setState( {
				btnFocused: 'false',
			} );
		}
	}
	saveArrayUpdate( value, index ) {
		const { attributes, setAttributes } = this.props;
		const { btns } = attributes;

		const newItems = btns.map( ( item, thisIndex ) => {
			if ( index === thisIndex ) {
				item = { ...item, ...value };
			}

			return item;
		} );
		setAttributes( {
			btns: newItems,
		} );
	}
	render() {
		const { attributes: { uniqueID, btnCount, btns, hAlign, letterSpacing, fontStyle, fontWeight, typography, googleFont, loadGoogleFont, fontSubset, fontVariant }, className, setAttributes, isSelected } = this.props;
		const gconfig = {
			google: {
				families: [ typography + ( fontVariant ? ':' + fontVariant : '' ) ],
			},
		};
		const config = ( googleFont ? gconfig : '' );
		const renderBtns = ( index ) => {
			return (
				<div className={ `ns-btn-${ index }-area mr-1 mb-1 ${ ( btns[ index ].buttonType ? 'btn' + btns[ index ].buttonType : '' ) }` }>
					<span className={ `ns-button-${ index } ${ ( btns[ index ].paddingLR ? 'px-' + btns[ index ].paddingLR : '' ) } btn btn${ ( btns[ index ].buttonStyle ? btns[ index ].buttonStyle : '' ) }${ nanosteamHexToName( btns[ index ].background ) } ${ btns[ index ].size } ${ ( btns[ index ].color ? 'text' + nanosteamHexToName( btns[ index ].color ) : '' ) } ${ ( btns[ index ].buttonType ? 'btn' + btns[ index ].buttonType : '' ) }` }>
						<RichText
							tagName="div"
							format="string"
							placeholder={ __( 'Button...' ) }
							value={ btns[ index ].text }
							unstableOnFocus={ () => {
								if ( 1 === index ) {
									onFocusBtn1();
								} else if ( 2 === index ) {
									onFocusBtn2();
								} else if ( 3 === index ) {
									onFocusBtn3();
								} else if ( 4 === index ) {
									onFocusBtn4();
								} else {
									onFocusBtn();
								}
							} }
							onChange={ value => {
								this.saveArrayUpdate( { text: value }, index );
							} }
							formattingControls={ [ 'bold', 'italic', 'strikethrough' ] }
							className={ 'ns-button-text' }
							keepPlaceholderOnFocus
						/>
					</span>
					{ isSelected && ( ( this.state.btnFocused && 'btn' + [ index ] === this.state.btnFocused ) || ( this.state.btnFocused && 'false' === this.state.btnFocused && '0' === index ) ) && (
						<form
							key={ 'form-link' }
							onSubmit={ ( event ) => event.preventDefault() }
							className="blocks-button__inline-link">
							<URLInput
								value={ btns[ index ].link }
								onChange={ value => {
									this.saveArrayUpdate( { link: value }, index );
								} }
							/>
							<IconButton
								icon={ 'editor-break' }
								label={ __( 'Apply' ) }
								type={ 'submit' }
							/>
						</form>
					) }
				</div>
			);
		};
		const onFocusBtn = () => {
			if ( 'btn0' !== this.state.btnFocused ) {
				this.setState( {
					btnFocused: 'btn0',
				} );
			}
		};
		const onFocusBtn1 = () => {
			if ( 'btn1' !== this.state.btnFocused ) {
				this.setState( {
					btnFocused: 'btn1',
				} );
			}
		};
		const onFocusBtn2 = () => {
			if ( 'btn2' !== this.state.btnFocused ) {
				this.setState( {
					btnFocused: 'btn2',
				} );
			}
		};
		const onFocusBtn3 = () => {
			if ( 'btn3' !== this.state.btnFocused ) {
				this.setState( {
					btnFocused: 'btn3',
				} );
			}
		};
		const onFocusBtn4 = () => {
			if ( 'btn4' !== this.state.btnFocused ) {
				this.setState( {
					btnFocused: 'btn4',
				} );
			}
		};
		const tabControls = ( index ) => {
			return (
				<PanelBody
					title={ __( 'Button' ) + ' ' + ( index + 1 ) + ' ' + __( 'Settings' ) }
					initialOpen={ false }
				>
					<p className="components-base-control__label">{ __( 'Link' ) }</p>
					<URLInput
						value={ btns[ index ].link }
						onChange={ value => {
							this.saveArrayUpdate( { link: value }, index );
						} }
					/>
					<SelectControl
						label={ __( 'Link Target' ) }
						value={ btns[ index ].target }
						options={ [
							{ value: '_self', label: __( 'Same Window' ) },
							{ value: '_blank', label: __( 'New Window' ) },
						] }
						onChange={ value => {
							this.saveArrayUpdate( { target: value }, index );
						} }
					/>
					<RadioControl
						label="Button Size"
						selected={ btns[ index ].size }
						options={ [
							{ label: 'Small', value: 'btn-sm' },
							{ label: 'Medium', value: '' },
							{ label: 'Large', value: 'btn-lg' },
						] }
						onChange={ value => {
							this.saveArrayUpdate( { size: value }, index );
						} }
					/>
					<RadioControl
						label="Button type"
						help="The type of the current user"
						selected={ btns[ index ].buttonType }
						options={ [
							{ label: 'Default', value: '' },
							{ label: 'Block', value: '-block' },
						] }
						onChange={ value => {
							this.saveArrayUpdate( { buttonType: value }, index );
						} }
					/>
					<RangeControl
						label={ __( 'Left and Right Padding' ) }
						value={ btns[ index ].paddingLR }
						onChange={ value => {
							this.saveArrayUpdate( { paddingLR: value }, index );
						} }
						min={ 0 }
						max={ 5 }
					/>
					<RadioControl
						label="Button style"
						selected={ btns[ index ].buttonStyle }
						options={ [
							{ label: 'Default', value: '' },
							{ label: 'Outline', value: '-outline' },
						] }
						onChange={ value => {
							this.saveArrayUpdate( { buttonStyle: value }, index );
						} }
					/>
					<h2 className="ns-tab-wrap-title">{ __( 'Color Settings' ) }</h2>
					<p className="ns-setting-label">{ __( 'Text Color' ) }</p>
					<ColorPalette
						colors={ colors }
						value={ btns[ index ].color }
						onChange={ value => {
							this.saveArrayUpdate( { color: value }, index );
						} }
					/>
					<p className="ns-setting-label">{ __( 'Button Color' ) }</p>
					<ColorPalette
						colors={ colors }
						value={ btns[ index ].background }
						onChange={ value => {
							this.saveArrayUpdate( { background: value }, index );
						} }
					/>
					<TextControl
						label={ __( 'Add Custom CSS Class' ) }
						value={ ( btns[ index ].cssClass ? btns[ index ].cssClass : '' ) }
						onChange={ ( value ) => this.saveArrayUpdate( { cssClass: value }, index ) }
					/>
				</PanelBody>
			);
		};
		const renderArray = (
			<Fragment>
				{ times( btnCount, n => tabControls( n ) ) }
			</Fragment>
		);
		const renderPreviewArray = (
			<div className={ `d-flex flex-wrap justify-content-${ ( hAlign === 'left' ? 'start' : ( hAlign === 'right' ? 'end' : hAlign ) ) }` }>
				{ times( btnCount, n => renderBtns( n ) ) }
			</div>
		);
		return (
			<Fragment>

				<div id={ `ns-btns_${ uniqueID }` } className={ `${ className } ns-btn-align-${ hAlign }` }>
					<BlockControls>
						<AlignmentToolbar
							value={ hAlign }
							onChange={ ( value ) => setAttributes( { hAlign: value } ) }
						/>
					</BlockControls>
					<InspectorControls>
						<PanelBody
							title={ __( 'Button Count' ) }
							initialOpen={ true }
						>
							<RangeControl
								label={ __( 'Number of Buttons' ) }
								value={ btnCount }
								onChange={ newcount => {
									const newbtns = btns;
									if ( newbtns.length < newcount ) {
										const amount = Math.abs( newcount - newbtns.length );
										{ times( amount, n => {
											newbtns.push( {
												text: newbtns[ 0 ].text,
												link: newbtns[ 0 ].link,
												target: newbtns[ 0 ].target,
												size: newbtns[ 0 ].size,
												paddingLR: newbtns[ 0 ].paddingLR,
												color: newbtns[ 0 ].color,
												background: newbtns[ 0 ].background,
												cssClass: ( newbtns[ 0 ].cssClass ? newbtns[ 0 ].cssClass : '' ),
											} );
										} ); }
										setAttributes( { btns: newbtns } );
									}
									setAttributes( { btnCount: newcount } );
								} }
								min={ 1 }
								max={ 5 }
							/>
						</PanelBody>
						{ renderArray }
					</InspectorControls>
					<div className={ 'btn-inner-wrap' } >
						{ renderPreviewArray }
					</div>
				</div>
			</Fragment>
		);
	}
}
export default ( NanosteamAdvancedButton );
