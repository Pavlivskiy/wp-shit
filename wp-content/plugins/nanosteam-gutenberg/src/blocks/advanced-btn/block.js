/**
 * BLOCK: Nanosteam Advanced Btn
 */
import times from 'lodash/times';
import GenIcon from '../../common/icons/genicon';
import Ico from '../../common/icons/svgicons';
import FaIco from '../../common/icons/faicons';


/**
 * Import Icons
 */
import icons from './icon';

/**
 * Import Css
 */
import './style.scss';
import './editor.scss';

import edit from './edit';
/**
 * Internal block libraries
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { RichText } = wp.editor;

function nanosteamHexToName( hex ) {
    let name = '';
    name = hex === '#FFB400' ? '-primary' : '-primary';
    name = hex === '#848e97' ? '-secondary' : name;
    name = hex === '#3dc644' ? '-success' : name;
    name = hex === '#005dcb' ? '-info' : name;
    name = hex === '#fc9c19' ? '-warning' : name;
    name = hex === '#d72229' ? '-danger' : name;
    name = hex === '#f6f6f6' ? '-light' : name;
    name = hex === '#36404a' ? '-dark' : name;

    return name;
}
/**
 * Register: a Gutenberg Block.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'nanosteam/advancedbtn', {
	title: __( 'Advanced Button' ), // Block title.
	icon: {
		src: icons.block,
	},
	category: 'common',
	keywords: [
		__( 'Button' ),
		__( 'Icon' ),
		__( 'KT' ),
	],
	attributes: {
		hAlign: {
			type: 'string',
			default: 'center',
		},
		btnCount: {
			type: 'number',
			default: 1,
		},
		uniqueID: {
			type: 'string',
			default: '',
		},
		btns: {
			type: 'array',
			default: [ {
				text: '',
				link: '',
				target: '_self',
				size: '',
				paddingLR: '3',
				color: '',
				background: '#FFB400',
				cssClass: '',
			} ],
		},
        fontVariant: {
            type: 'string',
            default: '',
        },
        fontWeight: {
            type: 'string',
            default: 'regular',
        },
        fontStyle: {
            type: 'string',
            default: 'normal',
        },
	},
	edit,
	save: props => {
		const { attributes: { btnCount, btns, hAlign, uniqueID } } = props;
		const renderSaveBtns = ( index ) => {
			return (
				<a className={ `ns-button mr-1 mb-1 ${ ( btns[ index ].paddingLR ? 'px-' + btns[ index ].paddingLR : '' ) } btn btn${ ( btns[ index ].buttonStyle ?  btns[ index ].buttonStyle : '' ) }${ nanosteamHexToName( btns[ index ].background ) } ${ btns[ index ].size } ${ ( btns[ index ].color ? 'text' + nanosteamHexToName( btns[ index ].color ) : '' ) } ${ ( btns[ index ].buttonType ? 'btn' + btns[ index ].buttonType : '' ) } ${ ( btns[ index ].cssClass ? ' ' + btns[ index ].cssClass : '' ) }` } href={ ( ! btns[ index ].link ? '#' : btns[ index ].link ) } target={ btns[ index ].target } rel={ '_blank' === btns[ index ].target ? 'noreferrer noopener' : undefined }>
					<span className="ns-btn-inner-text">
                        <RichText.Content value={ btns[ index ].text } />
					</span>
				</a>
			);
		};
		return (
			<div className={ `ns-btn-align-${ hAlign } ns-btns${ uniqueID }` }>
				{ times( btnCount, n => renderSaveBtns( n ) ) }
			</div>
		);
	},
} );
