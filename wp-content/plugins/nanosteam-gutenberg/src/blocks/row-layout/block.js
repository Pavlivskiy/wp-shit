/**
 * BLOCK: Nano Row / Layout
 */

/**
 * Import Icons
 */
import icons from '../../common/icons';
/**
 * Import attributes
 */
import attributes from './attributes';
/**
 * Import edit
 */
import edit from './edit';
/**
 * Import save
 */
import save from './save';

import classnames from 'classnames';
const {
	Fragment,
} = wp.element;
const {
	InnerBlocks,
} = wp.editor;

/**
 * Import Css
 */
import './style.scss';
import './editor.scss';

/**
 * Internal block libraries
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;

/**
 * Register: a Gutenberg Block.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'nanosteam/rowlayout', {
	title: __( 'Row Layout' ), // Block title.
	icon: 'editor-kitchensink',
	category: 'layout',
	keywords: [
		__( 'row' ),
		__( 'layout' ),
	],
	supports: {
		anchor: true,
	},
	attributes,
	getEditWrapperProps( { blockAlignment } ) {
		if ( 'full' === blockAlignment || 'wide' === blockAlignment || 'center' === blockAlignment ) {
			return { 'data-align': blockAlignment };
		}
	},
	edit,
	save,
	deprecated: [
		{
			attributes: {
				uniqueID: {
					type: 'string',
					default: '',
				},
				columns: {
					type: 'number',
					default: 2,
				},
				mobileLayout: {
					type: 'string',
					default: 'row',
				},
				tabletLayout: {
					type: 'string',
					default: 'inherit',
				},
				collapseOrder: {
					type: 'string',
					default: 'left-to-right',
				},
				colLayout: {
					type: 'string',
					default: '',
				},
				currentTab: {
					type: 'string',
					default: 'desk',
				},
				currentOverlayTab: {
					type: 'string',
					default: 'normal',
				},
				htmlTag: {
					type: 'string',
					default: 'div',
				},
				minHeight: {
					type: 'number',
					default: 0,
				},
				maxWidth: {
					type: 'number',
					default: '',
				},
				topPadding: {
					type: 'number',
					default: 25,
				},
				bottomPadding: {
					type: 'number',
					default: 25,
				},
				leftPadding: {
					type: 'number',
					default: '',
				},
				rightPadding: {
					type: 'number',
					default: '',
				},
				topPaddingM: {
					type: 'number',
					default: '',
				},
				bottomPaddingM: {
					type: 'number',
					default: '',
				},
				leftPaddingM: {
					type: 'number',
					default: '',
				},
				rightPaddingM: {
					type: 'number',
					default: '',
				},
				topMargin: {
					type: 'number',
					default: '',
				},
				bottomMargin: {
					type: 'number',
					default: '',
				},
				topMarginM: {
					type: 'number',
					default: '',
				},
				bottomMarginM: {
					type: 'number',
					default: '',
				},
				bgColor: {
					type: 'string',
					default: '',
				},
				bgImg: {
					type: 'string',
					default: '',
				},
				bgImgID: {
					type: 'string',
					default: '',
				},
				bgImgSize: {
					type: 'string',
					default: 'cover',
				},
				bgImgPosition: {
					type: 'string',
					default: 'center center',
				},
				bgImgAttachment: {
					type: 'string',
					default: 'scroll',
				},
				bgImgRepeat: {
					type: 'string',
					default: 'no-repeat',
				},
				overlay: {
					type: 'string',
					default: '',
				},
				overlaySecond: {
					type: 'string',
					default: '#00B5E2',
				},
				overlayGradLoc: {
					type: 'number',
					default: 0,
				},
				overlayGradLocSecond: {
					type: 'number',
					default: 100,
				},
				overlayGradType: {
					type: 'string',
					default: 'linear',
				},
				overlayGradAngle: {
					type: 'number',
					default: 180,
				},
				overlayBgImg: {
					type: 'string',
					default: '',
				},
				overlayBgImgID: {
					type: 'string',
					default: '',
				},
				overlayBgImgSize: {
					type: 'string',
					default: 'cover',
				},
				overlayBgImgPosition: {
					type: 'string',
					default: 'center center',
				},
				overlayBgImgAttachment: {
					type: 'string',
					default: 'scroll',
				},
				overlayBgImgRepeat: {
					type: 'string',
					default: 'no-repeat',
				},
				overlayOpacity: {
					type: 'number',
					default: 30,
				},
				overlayBlendMode: {
					type: 'string',
					default: 'none',
				},
				blockAlignment: {
					type: 'string',
					default: 'none',
				},
				verticalAlignment: {
					type: 'string',
					default: 'top',
				},
                contentContainerWidth: {
                    type: 'string',
                    default: 'constrained',
                },
			},
			save: ( { attributes } ) => {
				const { columns, blockAlignment, mobileLayout, currentOverlayTab, overlayBgImg, overlay, colLayout, tabletLayout, collapseOrder, uniqueID, bgColor, bgImg, verticalAlignment, htmlTag, bottomPadding, leftPadding, rightPadding, topPadding, topPaddingM, topMargin, bottomMargin, contentContainerWidth } = attributes;
				const layoutClass = ( ! colLayout ? 'equal' : colLayout );
				const tabLayoutClass = ( ! tabletLayout ? 'inherit' : tabletLayout );
				const HtmlTagOut = ( ! htmlTag ? 'div' : htmlTag );
				const mobileLayoutClass = ( ! mobileLayout ? 'inherit' : mobileLayout );
				const classId = ( ! uniqueID ? 'notset' : uniqueID );
				const hasBG = ( bgColor || bgImg || overlay || overlayBgImg ? 'kt-row-has-bg' : '' );
				const overlayType = ( ! currentOverlayTab || 'grad' !== currentOverlayTab ? 'normal' : 'gradient')
				const classes = classnames( `yimmmy2 align${ blockAlignment }` );
				const innerClasses = classnames( `row ${ hasBG }` );
				const innerColumnClasses = classnames( `container-width-${ contentContainerWidth } pt-lg-${ topPadding } pt-${ topPaddingM } kt-row-column-wrap kt-has-${ columns }-columns kt-row-valign-${ verticalAlignment } kt-row-layout-${ layoutClass } kt-tab-layout-${ tabLayoutClass } kt-m-colapse-${ collapseOrder } kt-mobile-layout-${ mobileLayoutClass }` );
                const parentColumns = wp.element.createContext();

                const TEMPLATE = [ [ 'nanosteam/rowlayout', {}, [
                    [ 'nanosteam/column', {
                        columns: columns,
                        colLayout: layoutClass,
                    }, [
                        [ 'core/paragraph', { placeholder: 'Enter your content...' } ],
                    ] ],
                ] ] ];

				return (
                    <div id={ `nano-id${ uniqueID }` } className={ innerClasses }>
							{ contentContainerWidth && 'full' !== contentContainerWidth && (
								<div className={ `container` }>
									<div className={ `row` }>
										<div className={ innerColumnClasses }>
                                            <div className={ innerColumnClasses }>
												<InnerBlocks.Content template={ TEMPLATE } />
                                            </div>
										</div>
									</div>
								</div>
							) }
							{ contentContainerWidth && 'full' === contentContainerWidth && (
								<div className={ innerColumnClasses }>
                                    <div className={ innerColumnClasses }>
										<InnerBlocks.Content template={ TEMPLATE } />
                                    </div>
								</div>
							) }
                    </div>
				);
			},
		},
	],
} );
