/**
 * BLOCK: Nanosteam Row / Layout
 */

/**
 * Import Icons
 */
import icons from '../../common/icons';

/**
 * Import External
 */
import times from 'lodash/times';
import map from 'lodash/map';
import classnames from 'classnames';
import memoize from 'memize';
import ResizableBox from 're-resizable';
import ContainerDimensions from 'react-container-dimensions';
/**
 * Import Css
 */
import './style.scss';
import './editor.scss';

// Importing Common Libraries
import colors from '../../common/nanoBrandColors.js';
import nanosteamHexToName from '../../common/hexToBrand.js';

const {
	Component,
	Fragment,
} = wp.element;
const {
	MediaUpload,
	InnerBlocks,
	InspectorControls,
	ColorPalette,
	BlockControls,
	BlockAlignmentToolbar,
} = wp.editor;
const {
	Button,
	ButtonGroup,
	Tooltip,
	TabPanel,
	IconButton,
	Dashicon,
	PanelBody,
	RangeControl,
	Toolbar,
	SelectControl,
} = wp.components;
const {
    dispatch,
    select,
} = wp.data;
/**
 * Internal block libraries
 */
const { __ } = wp.i18n;

const ALLOWED_BLOCKS = [ 'nanosteam/column' ];
/**
 * Returns the layouts configuration for a given number of columns.
 *
 * @param {number} columns Number of columns.
 *
 * @return {Object[]} Columns layout configuration.
 */

const getColumnsTemplate = memoize( ( columns, layout, tabletLayout, mobileLayout, clientId ) => {
	const cols = select('core/editor').getBlocksByClientId(clientId)[0].innerBlocks;
	const attrs = select('core/editor').getBlocksByClientId(clientId)[0].attributes;

	// Dispatc the new state if the template irt changed to the contained columns
    for (let col of cols) {

    	const colId = col.attributes.id - 1;
        const colVals = onChangeWidth(colId, columns, layout, tabletLayout, mobileLayout);

        dispatch('core/editor').updateBlockAttributes(col.clientId, {
			columns: columns,
			colLayout: colVals[0][colId] + ' ' + colVals[1][colId] + ' ' + colVals[2][colId],
		})

	}
    
	return times( columns, n => [ 'nanosteam/column', { id: n + 1, columns: columns, colLayout: layout,  } ] );
} );

const overlayOpacityOutput = memoize( ( opacity ) => {
	if ( opacity < 10 ) {
		return '0.0' + opacity;
	} else if ( opacity >= 100 ) {
		return '1';
	}
	return '0.' + opacity;
} );

/**
 * Update Column
 */
function onChangeWidth(id, columns, layout, tabletLayout, mobileLayout) {

	let col = '';
    let tabCol = '';
    let mobCol = '';
    const colId = id - 1;

    if ( columns === 1 ) {

        col = ['col-sm-12'];

    } else if ( columns === 2 ) {

    	// Equal
    	if ( layout === 'equal' ) {
            col = ['col-lg-6', 'col-lg-6'];
		}
        if ( tabletLayout === 'equal' ) {
            tabCol = ['col-sm-6', 'col-sm-6'];
        }
        if ( mobileLayout === 'equal' ) {
            mobCol = ['col-6', 'col-6'];
        }
        // Left Golden
        if ( layout === 'left-golden' ) {
            col = ['col-lg-9', 'col-lg-3'];
        }
        if ( tabletLayout === 'left-golden' ) {
            tabCol = ['col-sm-9', 'col-sm-3'];
        }
        if ( mobileLayout === 'left-golden' ) {
            mobCol = ['col-8', 'col-4'];
        }
        // Right Golden
        if ( layout === 'right-golden' ) {
            col = ['col-lg-3', 'col-lg-9'];
        }
        if ( tabletLayout === 'right-golden' ) {
            tabCol = ['col-sm-3', 'col-sm-9'];
        }
        if ( mobileLayout === 'right-golden' ) {
            mobCol = ['col-4', 'col-8'];
        }
        // Inherit
        if ( tabletLayout === 'inherit' ) {
            tabCol = ['', ''];
        }
        if ( mobileLayout === 'row' ) {
            mobCol = ['', ''];
        }

	} else if ( columns === 3 ) {

        // Equal
        if ( layout === 'equal' ) {
            col = ['col-lg-4', 'col-lg-4', 'col-lg-4'];
        }
        if ( tabletLayout === 'equal' ) {
            tabCol = ['col-sm-4', 'col-sm-4', 'col-sm-4'];
        }
        if ( mobileLayout === 'equal' ) {
            mobCol = ['col-4', 'col-4', 'col-4'];
        }
        // Left Half
        if ( layout === 'left-half' ) {
            col = ['col-lg-6', 'col-lg-3', 'col-lg-3'];
        }
        if ( tabletLayout === 'left-half' ) {
            tabCol = ['col-sm-6', 'col-sm-3', 'col-sm-3'];
        }
        if ( mobileLayout === 'left-half' ) {
            mobCol = ['col-8', 'col-4', 'col-lg-4'];
        }
        // Right Half
        if ( layout === 'right-half' ) {
            col = ['col-lg-3', 'col-lg-3', 'col-lg-6'];
        }
        if ( tabletLayout === 'right-half' ) {
            tabCol = ['col-sm-3', 'col-sm-3', 'col-sm-6'];
        }
        if ( mobileLayout === 'right-half' ) {
            mobCol = ['col-3', 'col-3', 'col-6'];
        }
        // Center Half
        if ( layout === 'center-half' ) {
            col = ['col-lg-3', 'col-lg-6', 'col-lg-3'];
        }
        if ( tabletLayout === 'center-half' ) {
            tabCol = ['col-sm-3', 'col-sm-6', 'col-sm-3'];
        }
        if ( mobileLayout === 'center-half' ) {
            mobCol = ['col-3', 'col-6', 'col-3'];
        }
        // Center Wide
        if ( layout === 'center-wide' ) {
            col = ['col-lg-2', 'col-lg-8', 'col-lg-2'];
        }
        if ( tabletLayout === 'center-wide' ) {
            tabCol = ['col-sm-2', 'col-sm-8', 'col-sm-2'];
        }
        if ( mobileLayout === 'center-wide' ) {
            mobCol = ['col-2', 'col-8', 'col-2'];
        }
        // Center Wide
        if ( layout === 'center-exwide' ) {
            col = ['col-lg-1', 'col-lg-10', 'col-lg-1'];
        }
        if ( tabletLayout === 'center-exwide' ) {
            tabCol = ['col-sm-1', 'col-sm-10', 'col-sm-1'];
        }
        if ( mobileLayout === 'center-exwide' ) {
            mobCol = ['col-1', 'col-10', 'col-1'];
        }
        // Inherit
        if ( tabletLayout === 'inherit' ) {
            tabCol = ['', '', ''];
        }
        if ( mobileLayout === 'row' ) {
            mobCol = ['', '', ''];
        }

    } else if ( columns === 4 ) {

        // Equal
        if ( layout === 'equal' ) {
            col = ['col-lg-3', 'col-lg-3' , 'col-lg-3', 'col-lg-3'];
        }
        if ( tabletLayout === 'equal' ) {
            tabCol = ['col-sm-3', 'col-sm-3' , 'col-sm-3', 'col-sm-3'];
        }
        if ( mobileLayout === 'equal' ) {
            mobCol = ['col-3', 'col-3' , 'col-3', 'col-3'];
        }
        // left-forty
        if ( layout === 'equal' ) {
            col = ['col-lg-5', 'col-lg-3' , 'col-lg-3', 'col-lg-2'];
        }
        if ( tabletLayout === 'equal' ) {
            tabCol = ['col-sm-5', 'col-sm-3' , 'col-sm-3', 'col-sm-2'];
        }
        if ( mobileLayout === 'equal' ) {
            mobCol = ['col-5', 'col-3' , 'col-3', 'col-2'];
        }
        // right-half
        if ( layout === 'equal' ) {
            col = ['col-lg-2', 'col-lg-3' , 'col-lg-3', 'col-lg-5'];
        }
        if ( tabletLayout === 'equal' ) {
            tabCol = ['col-sm-2', 'col-sm-3' , 'col-sm-3', 'col-sm-5'];
        }
        if ( mobileLayout === 'equal' ) {
            mobCol = ['col-2', 'col-3' , 'col-3', 'col-5'];
        }
        //Grid
        if ( tabletLayout === 'two-grid' ) {
            mobCol = ['col-sm-6', 'col-sm-6' , 'col-sm-6', 'col-sm-6'];
        }
        if ( mobileLayout === 'two-grid' ) {
            mobCol = ['col-6', 'col-6' , 'col-6', 'col-6'];
        }
        // Inherit
        if ( tabletLayout === 'inherit' || tabletLayout === 'row') {
            tabCol = ['', '', '', ''];
        }
        if ( mobileLayout === 'row' ) {
            mobCol = ['', '', '', ''];
        }

    } else if ( columns === 5 ) {

        // Left Golden
        if ( layout === 'left-five' ) {
            col = ['col-lg-4', 'col-lg-2', 'col-lg-2', 'col-lg-2', 'col-lg-2'];
        }
        if ( tabletLayout === 'left-five' ) {
            tabCol = ['col-sm-4', 'col-sm-2', 'col-sm-2', 'col-sm-2', 'col-sm-2'];
        }
        if ( mobileLayout === 'left-five' ) {
            mobCol = ['col-4', 'col-2', 'col-2', 'col-2', 'col-2'];
        }
        // Right Golden
        if ( layout === 'right-five' ) {
            col = ['col-lg-2', 'col-lg-2', 'col-lg-2', 'col-lg-2', 'col-lg-4'];
        }
        if ( tabletLayout === 'right-five' ) {
            tabCol = ['col-sm-2', 'col-sm-2', 'col-sm-2', 'col-sm-2', 'col-sm-4'];
        }
        if ( mobileLayout === 'right-five' ) {
            mobCol = ['col-2', 'col-2', 'col-2', 'col-2', 'col-4'];
        }
        // Inherit
        if ( tabletLayout === 'inherit' || tabletLayout === 'row' ) {
            tabCol = ['', '', '', '', '', ''];
        }
        if ( mobileLayout === 'row' ) {
            mobCol = ['', '', '', '', '', ''];
        }

    } else if ( columns === 6 ) {

        // Left Golden
        if ( layout === 'equal' ) {
            col = ['col-lg-2', 'col-lg-2', 'col-lg-2', 'col-lg-2', 'col-lg-2', 'col-lg-2'];
        }
        if ( tabletLayout === 'equal' ) {
            tabCol = ['col-sm-2', 'col-sm-2', 'col-sm-2', 'col-sm-2', 'col-sm-2', 'col-sm-2'];
        }
        if ( mobileLayout === 'equal' ) {
            mobCol = ['col-2', 'col-2', 'col-2', 'col-2', 'col-2', 'col-2'];
        }
        // Two Grid
        if ( tabletLayout === 'two-grid' ) {
            tabCol = ['col-sm-6', 'col-sm-6', 'col-sm-6', 'col-sm-6', 'col-sm-6', 'col-sm-6'];
        }
        if ( mobileLayout === 'right-five' ) {
            mobCol = ['col-6', 'col-6', 'col-6', 'col-6', 'col-6', 'col-6'];
        }
        // Three Grid
        if ( tabletLayout === 'two-grid' ) {
            tabCol = ['col-sm-4', 'col-sm-4', 'col-sm-4', 'col-sm-4', 'col-sm-4', 'col-sm-4'];
        }
        if ( mobileLayout === 'right-five' ) {
            mobCol = ['col-4', 'col-4', 'col-4', 'col-4', 'col-4', 'col-4'];
        }
        // Inherit
        if ( tabletLayout === 'inherit' || tabletLayout === 'row' ) {
            tabCol = ['', '', '', '', '', '', ''];
        }
        if ( mobileLayout === 'row' ) {
            mobCol = ['', '', '', '', '', '', ''];
        }

    }

	return [ col, tabCol, mobCol ];

}


/**
 * Build the row edit
 */
class NanosteamRowLayout extends Component {
	constructor() {
		super( ...arguments );
		this.state = {
			firstWidth: null,
		};
	}
	componentDidMount() {
		if ( ! this.props.attributes.uniqueID ) {
			const blockConfig = nanosteam_blocks_params.config[ 'nanosteam/rowlayout' ];
			if ( blockConfig !== undefined && typeof blockConfig === 'object' ) {
				Object.keys( blockConfig ).map( ( attribute ) => {
					this.props.attributes[ attribute ] = blockConfig[ attribute ];
				} );
			}
			this.props.setAttributes( {
				uniqueID: '_' + this.props.clientId.substr( 2, 9 ),
			} );
		} else if ( this.props.attributes.uniqueID && this.props.attributes.uniqueID !== '_' + this.props.clientId.substr( 2, 9 ) ) {
			this.props.setAttributes( {
				uniqueID: '_' + this.props.clientId.substr( 2, 9 ),
			} );
		}
	}
	render() {
		const { attributes: { uniqueID, columns, blockAlignment, mobileLayout, currentTab, colLayout, tabletLayout, collapseOrder, topPadding, bottomPadding, leftPadding, rightPadding, topPaddingM, bottomPaddingM, leftPaddingM, rightPaddingM, topMargin, bottomMargin, topMarginM, bottomMarginM, bgColor, bgImg, bgImgAttachment, bgImgSize, bgImgPosition, bgImgRepeat, bgImgID, verticalAlignment, overlayOpacity, overlayBgImg, overlayBgImgAttachment, overlayBgImgID, overlayBgImgPosition, overlayBgImgRepeat, overlayBgImgSize, currentOverlayTab, overlayBlendMode, overlayGradAngle, overlayGradLoc, overlayGradLocSecond, overlayGradType, overlay, overlaySecond, htmlTag, minHeight, maxWidth, bottomSep, bottomSepColor, bottomSepHeight, bottomSepHeightMobile, bottomSepHeightTablet, bottomSepWidth, bottomSepWidthMobile, bottomSepWidthTablet, topSep, topSepColor, topSepHeight, topSepHeightMobile, topSepHeightTablet, topSepWidth, topSepWidthMobile, topSepWidthTablet, firstColumnWidth }, toggleSelection, className, setAttributes, clientId, contentContainerWidth} = this.props;
		const onResize = ( event, direction, elt ) => {
			this.setState( {
				firstWidth: Math.round( parseInt( elt.style.width ) / 5 ) * 5,
			} );
			document.getElementById( 'left-column-width-' + uniqueID ).innerHTML = ( Math.round( parseInt( elt.style.width ) / 5 ) * 5 ) + '%';
			document.getElementById( 'right-column-width-' + uniqueID ).innerHTML = Math.abs( ( Math.round( parseInt( elt.style.width ) / 5 ) * 5 ) - 100 ) + '%';
		};
		const onResizeStop = ( event, direction, elt ) => {
			setAttributes( { firstColumnWidth: Math.round( parseInt( elt.style.width ) / 5 ) * 5 } );
			this.setState( {
				firstWidth: null,
			} );
		};
		const temporaryColumnWidth = this.state.firstWidth;
		const widthString = `${ temporaryColumnWidth || firstColumnWidth || colLayout }`;
		let widthNumber;
		if ( widthString === parseInt( widthString ) ) {
			widthNumber = widthString + '%';
		} else if ( 'left-golden' === widthString ) {
			widthNumber = '66.67%';
		} else if ( 'right-golden' === widthString ) {
			widthNumber = '33.37%';
		} else {
			widthNumber = '50%';
		}
		const layoutClass = ( ! colLayout ? 'equal' : colLayout );
		const tabLayoutClass = ( ! tabletLayout ? 'inherit' : tabletLayout );
		const mobileLayoutClass = ( ! mobileLayout ? 'inherit' : mobileLayout );
		const selectColLayout = ( columns && 2 === columns ? widthString : colLayout );
		const hasBG = ( bgColor || bgImg || overlay || overlayBgImg ? 'ns-row-has-bg' : '' );
		const classes = classnames( className, `ns-has-${ columns }-columns ns-row-layout-${ layoutClass } ns-row-valign-${ verticalAlignment } ns-tab-layout-${ tabLayoutClass } ns-mobile-layout-${ mobileLayoutClass } current-tab-${ currentTab } ns-custom-first-width-${ widthString } ${ hasBG }` );
		let layoutOptions;
		let mobileLayoutOptions;
		const startlayoutOptions = [
			{ key: 'equal', col: 1, name: __( 'Row' ), icon: icons.row },
			{ key: 'equal', col: 2, name: __( 'Two: Equal' ), icon: icons.twocol },
			{ key: 'left-golden', col: 2, name: __( 'Two: Left Heavy 66/33' ), icon: icons.twoleftgolden },
			{ key: 'right-golden', col: 2, name: __( 'Two: Right Heavy 33/66' ), icon: icons.tworightgolden },
			{ key: 'equal', col: 3, name: __( 'Three: Equal' ), icon: icons.threecol },
			{ key: 'left-half', col: 3, name: __( 'Three: Left Heavy 50/25/25' ), icon: icons.lefthalf },
			{ key: 'right-half', col: 3, name: __( 'Three: Right Heavy 25/25/50' ), icon: icons.righthalf },
			{ key: 'center-half', col: 3, name: __( 'Three: Center Heavy 25/50/25' ), icon: icons.centerhalf },
			{ key: 'center-wide', col: 3, name: __( 'Three: Wide Center 20/60/20' ), icon: icons.widecenter },
			{ key: 'center-exwide', col: 3, name: __( 'Three: Wider Center 15/70/15' ), icon: icons.exwidecenter },
			{ key: 'equal', col: 4, name: __( 'Four: Equal' ), icon: icons.fourcol },
			{ key: 'left-forty', col: 4, name: __( 'Four: Left Heavy 50/16/16/16' ), icon: icons.lfourforty },
			{ key: 'right-forty', col: 4, name: __( 'Four: Right Heavy 16/16/16/50' ), icon: icons.rfourforty },
			{ key: 'left-five', col: 5, name: __( 'Five: Left Heavy' ), icon: icons.fivecolleft },
            { key: 'right-five', col: 5, name: __( 'Five: Right Heavy' ), icon: icons.fivecolleft },
			{ key: 'equal', col: 6, name: __( 'Six: Equal' ), icon: icons.sixcol },
		];
		if ( 2 === columns ) {
			layoutOptions = [
				{ key: 'equal', name: __( 'Equal' ), icon: icons.twocol },
				{ key: 'left-golden', name: __( 'Left Heavy 66/33' ), icon: icons.twoleftgolden },
				{ key: 'right-golden', name: __( 'Right Heavy 33/66' ), icon: icons.tworightgolden },
			];
		} else if ( 3 === columns ) {
			layoutOptions = [
				{ key: 'equal', name: __( 'Equal' ), icon: icons.threecol },
				{ key: 'left-half', name: __( 'Left Heavy 50/25/25' ), icon: icons.lefthalf },
				{ key: 'right-half', name: __( 'Right Heavy 25/25/50' ), icon: icons.righthalf },
				{ key: 'center-half', name: __( 'Center Heavy 25/50/25' ), icon: icons.centerhalf },
				{ key: 'center-wide', name: __( 'Wide Center 20/60/20' ), icon: icons.widecenter },
				{ key: 'center-exwide', name: __( 'Wider Center 15/70/15' ), icon: icons.exwidecenter },
			];
		} else if ( 4 === columns ) {
			layoutOptions = [
				{ key: 'equal', name: __( 'Equal' ), icon: icons.fourcol },
				{ key: 'left-forty', name: __( 'Four: Left Heavy 50/16/16/16' ), icon: icons.lfourforty },
				{ key: 'right-forty', name: __( 'Four: Right Heavy 16/16/16/50' ), icon: icons.rfourforty },
			];
		} else if ( 5 === columns ) {
			layoutOptions = [
				{ key: 'left-five', name: __( 'Five: Left Heavy' ), icon: icons.fivecolleft },
                { key: 'right-five', name: __( 'Five: Left Heavy' ), icon: icons.fivecolright },
			];
		} else if ( 6 === columns ) {
			layoutOptions = [
				{ key: 'equal', name: __( 'Equal' ), icon: icons.sixcol },
			];
		} else {
			layoutOptions = [
				{ key: 'equal', name: __( 'Single Row' ), icon: icons.row },
			];
		}
		if ( 2 === columns ) {
			mobileLayoutOptions = [
				{ key: 'equal', name: __( 'Equal' ), icon: icons.twocol },
				{ key: 'left-golden', name: __( 'Left Heavy 66/33' ), icon: icons.twoleftgolden },
				{ key: 'right-golden', name: __( 'Right Heavy 33/66' ), icon: icons.tworightgolden },
				{ key: 'row', name: __( 'Collapse to Rows' ), icon: icons.collapserow },
			];
		} else if ( 3 === columns ) {
			mobileLayoutOptions = [
				{ key: 'equal', name: __( 'Equal' ), icon: icons.threecol },
				{ key: 'left-half', name: __( 'Left Heavy 50/25/25' ), icon: icons.lefthalf },
				{ key: 'right-half', name: __( 'Right Heavy 25/25/50' ), icon: icons.righthalf },
				{ key: 'center-half', name: __( 'Center Heavy 25/50/25' ), icon: icons.centerhalf },
				{ key: 'center-wide', name: __( 'Wide Center 20/60/20' ), icon: icons.widecenter },
				{ key: 'center-exwide', name: __( 'Wider Center 15/70/15' ), icon: icons.exwidecenter },
				{ key: 'row', name: __( 'Collapse to Rows' ), icon: icons.collapserowthree },
			];
		} else if ( 4 === columns ) {
			mobileLayoutOptions = [
				{ key: 'equal', name: __( 'Equal' ), icon: icons.fourcol },
				{ key: 'left-forty', name: __( 'Left Heavy 40/20/20/20' ), icon: icons.lfourforty },
				{ key: 'right-forty', name: __( 'Right Heavy 20/20/20/40' ), icon: icons.rfourforty },
				{ key: 'two-grid', name: __( 'Two Column Grid' ), icon: icons.grid },
				{ key: 'row', name: __( 'Collapse to Rows' ), icon: icons.collapserowfour },
			];
		} else if ( 5 === columns ) {
			mobileLayoutOptions = [
                { key: 'left-five', name: __( 'Five: Left Heavy' ), icon: icons.fivecolleft },
                { key: 'right-five', name: __( 'Five: Left Heavy' ), icon: icons.fivecolright },
				{ key: 'row', name: __( 'Collapse to Rows' ), icon: icons.collapserowfive },
			];
		} else if ( 6 === columns ) {
			mobileLayoutOptions = [
				{ key: 'equal', name: __( 'Equal' ), icon: icons.sixcol },
				{ key: 'two-grid', name: __( 'Two Column Grid' ), icon: icons.grid },
				{ key: 'three-grid', name: __( 'Three Column Grid' ), icon: icons.threegrid },
				{ key: 'row', name: __( 'Collapse to Rows' ), icon: icons.collapserowsix },
			];
		} else {
			mobileLayoutOptions = [
				{ key: 'row', name: __( 'Single Row' ), icon: icons.row },
			];
		}
		const onTabSelect = ( tabName ) => {
			setAttributes( { currentTab: tabName } );
		};
        const changeColumn = ( clientId ) => {

            const container = select('core/editor').getBlocksByClientId(clientId)[0];
            const ContainerWidth = container.attributes.contentContainerWidth;
            const ContainerId = 'block-' + container.clientId;
            const WrapContainer = document.getElementById(ContainerId);

            if ( WrapContainer ) {

                if ( ContainerWidth !== 'full' ) {
                    WrapContainer.setAttribute("style", "max-width:1140px;");
                }
                if ( ContainerWidth === 'full' ) {
                    WrapContainer.setAttribute("style", "width:100%;max-width:100%;");
                }
            }

        };
		const onOverlayTabSelect = ( tabName ) => {
			setAttributes( { currentOverlayTab: tabName } );
		};
		const mobileControls = (
			<div>
				<PanelBody>
					<p className="components-base-control__label">{ __( 'Mobile Layout' ) }</p>
					<ButtonGroup aria-label={ __( 'Mobile Layout' ) }>
						{ map( mobileLayoutOptions, ( { name, key, icon } ) => (
							<Tooltip text={ name }>
								<Button
									key={ key }
									className="ns-layout-btn"
									isSmall
									isPrimary={ mobileLayout === key }
									aria-pressed={ mobileLayout === key }
									onClick={ () => setAttributes( { mobileLayout: key } ) }
								>
									{ icon }
								</Button>
							</Tooltip>
						) ) }
					</ButtonGroup>
				</PanelBody>
				<PanelBody
					title={ __( 'Mobile Padding/Margin' ) }
					initialOpen={ false }
				>
					<h2>{ __( 'Padding Levels' ) }</h2>
					<RangeControl
						label={ icons.outlinetop }
						value={ topPaddingM }
						className="ns-icon-rangecontrol ns-top-padding"
						onChange={ ( value ) => {
							setAttributes( {
								topPaddingM: value,
							} );
						} }
						min={ 0 }
						max={ 5 }
					/>
					<RangeControl
						label={ icons.outlineright }
						value={ rightPaddingM }
						className="ns-icon-rangecontrol ns-right-padding"
						onChange={ ( value ) => {
							setAttributes( {
								rightPaddingM: value,
							} );
						} }
						min={ 0 }
						max={ 5 }
					/>
					<RangeControl
						label={ icons.outlinebottom }
						value={ bottomPaddingM }
						className="ns-icon-rangecontrol ns-bottom-padding"
						onChange={ ( value ) => {
							setAttributes( {
								bottomPaddingM: value,
							} );
						} }
						min={ 0 }
						max={ 5 }
					/>
					<RangeControl
						label={ icons.outlineleft }
						value={ leftPaddingM }
						className="ns-icon-rangecontrol ns-left-padding"
						onChange={ ( value ) => {
							setAttributes( {
								leftPaddingM: value,
							} );
						} }
						min={ 0 }
						max={ 5 }
					/>
					<h2>{ __( 'Mobile Margin Levels' ) }</h2>
					<RangeControl
						label={ icons.outlinetop }
						value={ topMarginM }
						className="ns-icon-rangecontrol ns-top-margin"
						onChange={ ( value ) => {
							setAttributes( {
								topMarginM: value,
							} );
						} }
						min={ 0 }
						max={ 5 }
					/>
					<RangeControl
						label={ icons.outlinebottom }
						value={ bottomMarginM }
						className="ns-icon-rangecontrol ns-bottom-margin"
						onChange={ ( value ) => {
							setAttributes( {
								bottomMarginM: value,
							} );
						} }
						min={ 0 }
						max={ 5 }
					/>
				</PanelBody>
			</div>
		);
		const tabletControls = (
			<PanelBody>
				<p className="components-base-control__label">{ __( 'Tablet Layout' ) }</p>
				<ButtonGroup aria-label={ __( 'Tablet Layout' ) }>
					{ map( mobileLayoutOptions, ( { name, key, icon } ) => (
						<Tooltip text={ name }>
							<Button
								key={ key }
								className="ns-layout-btn"
								isSmall
								isPrimary={ tabletLayout === key }
								aria-pressed={ tabletLayout === key }
								onClick={ () => setAttributes( { tabletLayout: key } ) }
							>
								{ icon }
							</Button>
						</Tooltip>
					) ) }
				</ButtonGroup>
			</PanelBody>
		);

		const deskControls = (
			<div>
				<PanelBody>
					<RangeControl
						label={ __( 'Columns' ) }
						value={ columns }
						onChange={ ( nextColumns ) => {
							setAttributes( {
								columns: nextColumns,
								colLayout: 'equal',
								tabletLayout: 'inherit',
								mobileLayout: 'row',
							} );
						} }
						min={ 1 }
						max={ 6 }
					/>
					<p className="components-base-control__label">{ __( 'Layout' ) }</p>
					<ButtonGroup aria-label={ __( 'Column Layout' ) }>
						{ map( layoutOptions, ( { name, key, icon } ) => (
							<Tooltip text={ name }>
								<Button
									key={ key }
									className="ns-layout-btn"
									isSmall
									isPrimary={ selectColLayout === key }
									aria-pressed={ selectColLayout === key }
									onClick={ () => {
										setAttributes( {
											colLayout: key,
										} );
										setAttributes( {
											firstColumnWidth: undefined,
										} );
									} }
								>
									{ icon }
								</Button>
							</Tooltip>
						) ) }
					</ButtonGroup>
                    <SelectControl
                        label={ __( 'Container Width' ) }
                        value={ this.props.attributes.contentContainerWidth }
                        options={ [
                            { value: 'constrained', label: __( 'Contained' ) },
                            { value: 'full', label: __( 'Full' ) },
                        ] }
						aria-val={ changeColumn( clientId ) }
                        onChange={ ( value ) => {
                        	setAttributes( { contentContainerWidth: value, } );
                            changeColumn( clientId );
                        } }
                    />
				</PanelBody>
				<PanelBody
					title={ __( 'Padding/Margin' ) }
					initialOpen={ false }
				>
					<h2>{ __( 'Padding Levels' ) }</h2>
					<RangeControl
						label={ icons.outlinetop }
						value={ topPadding }
						className="ns-icon-rangecontrol"
						onChange={ ( value ) => {
							setAttributes( {
								topPadding: value,
							} );
						} }
						min={ 0 }
						max={ 5 }
					/>
					<RangeControl
						label={ icons.outlinebottom }
						value={ bottomPadding }
						className="ns-icon-rangecontrol"
						onChange={ ( bottomPadding ) => {
							setAttributes( {
								bottomPadding: bottomPadding,
							} );
						} }
						min={ 0 }
						max={ 5 }
					/>
					<RangeControl
						label={ icons.outlineright }
						value={ rightPadding }
						className="ns-icon-rangecontrol"
						onChange={ ( rightPadding ) => {
							setAttributes( {
								rightPadding: rightPadding,
							} );
						} }
						min={ 0 }
						max={ 5 }
					/>
					<RangeControl
						label={ icons.outlineleft }
						value={ leftPadding }
						className="ns-icon-rangecontrol"
						onChange={ ( leftPadding ) => {
							setAttributes( {
								leftPadding: leftPadding,
							} );
						} }
						min={ 0 }
						max={ 5 }
					/>
					<h2>{ __( 'Margin Levels' ) }</h2>
					<RangeControl
						label={ icons.outlinetop }
						value={ topMargin }
						className="ns-icon-rangecontrol"
						onChange={ ( value ) => {
							setAttributes( {
								topMargin: value,
							} );
						} }
						min={ 0 }
						max={ 5 }
					/>
					<RangeControl
						label={ icons.outlinebottom }
						value={ bottomMargin }
						className="ns-icon-rangecontrol"
						onChange={ ( value ) => {
							setAttributes( {
								bottomMargin: value,
							} );
						} }
						min={ 0 }
						max={ 5 }
					/>
				</PanelBody>
			</div>
		);

		const backgroundControls = (
			<PanelBody
				title={ __( 'Background Settings' ) }
				initialOpen={ false }
			>
				<p>{ __( 'Background Color' ) }</p>
				<ColorPalette
                    colors = { colors }
					value={ bgColor }
					onChange={ bgColor => setAttributes( { bgColor } ) }
				/>
			</PanelBody>
		);
		const tabControls = (
			<TabPanel className="ns-inspect-tabs"
				activeClass="active-tab"
				initialTabName={ currentTab }
				onSelect={ onTabSelect }
				tabs={ [
					{
						name: 'desk',
						title: <Dashicon icon="desktop" />,
						className: 'ns-desk-tab',
					},
					{
						name: 'tablet',
						title: <Dashicon icon="tablet" />,
						className: 'ns-tablet-tab',
					},
					{
						name: 'mobile',
						title: <Dashicon icon="smartphone" />,
						className: 'ns-mobile-tab',
					},
				] }>
				{
					( tab ) => {
						let tabout;
						if ( tab.name ) {
							if ( 'mobile' === tab.name ) {
								tabout = mobileControls;
							} else if ( 'tablet' === tab.name ) {
								tabout = tabletControls;
							} else {
								tabout = deskControls;
							}
						} else {
							if ( 'mobile' === tab ) {
								tabout = mobileControls;
							} else if ( 'tablet' === tab ) {
								tabout = tabletControls;
							} else {
								tabout = deskControls;
							}
						}
						return <div>{ tabout }</div>;
					}
				}
			</TabPanel>
		);
		return (
			<Fragment>
				<InspectorControls>
					{ tabControls }
					{ backgroundControls }
				</InspectorControls>
				<div className={ classes }style={ {
                    backgroundColor: ( bgColor ? bgColor : undefined ),
                } }>
					{ ! colLayout && (
						<div className="ns-select-layout">
							<div className="ns-select-layout-title">
								{ __( 'Select Your Layout' ) }
							</div>
							<ButtonGroup aria-label={ __( 'Column Layout' ) }>
								{ map( startlayoutOptions, ( { name, key, icon, col } ) => (
									<Tooltip text={ name }>
										<Button
											key={ key }
											className="ns-layout-btn"
											isSmall
											onClick={ () => setAttributes( {
												colLayout: key,
												columns: col,
											} ) }
										>
											{ icon }
										</Button>
									</Tooltip>
								) ) }
							</ButtonGroup>
						</div>
					) }
					{ colLayout && (
						<ResizableBox
							size={ {
								height: topPadding,
							} }
							minHeight="0"
							handleClasses={ {
								top: 'wp-block-nanosteam-rowlayout-handler-top',
								bottom: 'wp-block-nanosteam-rowlayout-handler-bottom',
							} }
							enable={ {
								top: true,
								right: false,
								bottom: false,
								left: false,
								topRight: false,
								bottomRight: false,
								bottomLeft: false,
								topLeft: false,
							} }
							className={ 'ns-top-padding-resize ns-padding-resize-box' }
							onResize={ ( event, direction, elt, delta ) => {
								const el = document.getElementById( 'row-top-' + uniqueID );
                                if(el) {
                                    el.className += el.className ? ' pt-' + topPadding : 'pt-' + topPadding;
                                }
                                //element.classList.add('pt-' + parseInt( topPadding + delta.height, 10 ));
                                //= parseInt( topPadding + delta.height, 10 ) + 'px';
							} }
							onResizeStop={ ( event, direction, elt, delta ) => {
								setAttributes( {
									topPadding: parseInt( topPadding + delta.height, 10 ),
								} );
								toggleSelection( true );
							} }
							onResizeStart={ () => {
								toggleSelection( false );
							} }
						>
						</ResizableBox>
					) }
					{ colLayout && (
						<div className={ `innerblocks-wrap pl-${ leftPadding } pr-${ rightPadding } pb-${ bottomPadding } pt-${ topPadding }` }>
							<InnerBlocks
								template={ getColumnsTemplate( columns, colLayout, tabletLayout, mobileLayout, clientId ) }
								templateLock="all"
								allowedBlocks={ ALLOWED_BLOCKS } />
						</div>
					) }
					{ colLayout && (
						<ResizableBox
							size={ {
								height: bottomPadding,
							} }
							minHeight="0"
							handleClasses={ {
								top: 'wp-block-nanosteam-rowlayout-handler-top',
								bottom: 'wp-block-nanosteam-rowlayout-handler-bottom',
							} }
							enable={ {
								top: false,
								right: false,
								bottom: true,
								left: false,
								topRight: false,
								bottomRight: false,
								bottomLeft: false,
								topLeft: false,
							} }
							className={ 'ns-bottom-padding-resize ns-padding-resize-box' }
							onResize={ ( event, direction, elt, delta ) => {
								document.getElementById( 'row-bottom-' + uniqueID ).innerHTML = parseInt( bottomPadding + delta.height, 10 ) + 'px';
							} }
							onResizeStop={ ( event, direction, elt, delta ) => {
								setAttributes( {
									bottomPadding: parseInt( bottomPadding + delta.height, 10 ),
								} );
								toggleSelection( true );
							} }
							onResizeStart={ () => {
								toggleSelection( false );
							} }
						>
						</ResizableBox>
					) }
				</div>
			</Fragment>
		);
	}
}
export default ( NanosteamRowLayout );
