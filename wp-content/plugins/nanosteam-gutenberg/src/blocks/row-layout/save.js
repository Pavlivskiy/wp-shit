/**
 * BLOCK: Nano Row / Layout
 */

import classnames from 'classnames';
const {
	Component,
	Fragment,
} = wp.element;
const {
	InnerBlocks,
} = wp.editor;

function nanosteamHexToName( hex ) {
    let name = '';
    name = hex === '#FFB400' ? 'bg-primary' : '';
    name = hex === '#848e97' ? 'bg-secondary' : name;
    name = hex === '#3dc644' ? 'bg-success' : name;
    name = hex === '#005dcb' ? 'bg-info' : name;
    name = hex === '#fc9c19' ? 'bg-warning' : name;
    name = hex === '#d72229' ? 'bg-danger' : name;
    name = hex === '#f6f6f6' ? 'bg-light' : name;
    name = hex === '#36404a' ? 'bg-dark' : name;

    return name;
}


class NanosteamRowLayoutSave extends Component {
	render() {
		const { attributes: { firstColumnWidth, columns, blockAlignment, mobileLayout, currentOverlayTab, overlayBgImg, overlay, colLayout, tabletLayout, collapseOrder, uniqueID, bgColor, bgImg, verticalAlignment, htmlTag, bottomPadding, leftPadding, rightPadding, bottomPaddingM, leftPaddingM, rightPaddingM, topPadding, topPaddingM, topMargin, bottomMargin, topMarginM, bottomMarginM, contentContainerWidth } } = this.props;
		const layoutClass = ( ! colLayout ? 'equal' : colLayout );
		const tabLayoutClass = ( ! tabletLayout ? 'inherit' : tabletLayout );
		const HtmlTagOut = ( ! htmlTag ? 'div' : htmlTag );
		const mobileLayoutClass = ( ! mobileLayout ? 'inherit' : mobileLayout );
		const classId = ( ! uniqueID ? 'notset' : uniqueID );
		const bgColorBrand = nanosteamHexToName( bgColor );
		const classes = classnames( `align${ blockAlignment }` );
		const innerClasses = classnames( `row` );
		const topPad = ( topPadding ? 'pt-lg-' + topPadding + ' ' : '' ) + ( topPaddingM ? 'pt-' + topPaddingM + ' ' : '' );
        const leftPad = ( leftPadding ? 'pl-lg-' + leftPadding + ' ' : '' ) + ( leftPaddingM ? 'pl-' + leftPaddingM + ' ' : '' );
        const rightPad = ( rightPadding ? 'pr-lg-' + rightPadding + ' ' : '' ) + ( rightPaddingM ? 'pr-' + rightPaddingM + ' ' : '' );
        const botPad = ( bottomPadding ? 'pb-lg-' + bottomPadding + ' ' : '' ) + ( bottomPaddingM ? 'pb-' + bottomPaddingM + ' ' : '' );
        const topMar = ( topMargin ? 'mt-lg-' + topMargin + ' ' : '' ) + ( topMarginM ? 'mt-' + topMarginM + ' ' : '' );
        const botMar = ( bottomMargin ? 'mb-lg-' + bottomMargin + ' ' : '' ) + ( bottomMarginM ? 'mb-' + bottomMarginM + ' ' : '' );
		const rowPadding = topPad + leftPad + rightPad + botPad + topMar + botMar;
		const innerColumnClasses = classnames( `row ${ bgColorBrand } ${ rowPadding }` );

        const TEMPLATE = [ [ 'nanosteam/rowlayout', {}, [
            [ 'nanosteam/column', {
        		columns: columns,
				colLayout: layoutClass,
			}, [
                [ 'core/paragraph', { placeholder: 'Enter your content...' } ],
            ] ],
        ] ] ];

		return (
            <div id={ `nano-id${ uniqueID }` } className={ innerClasses }>
                { contentContainerWidth && 'full' !== contentContainerWidth && (
                    <div className={ `container` }>
                        <div className={ `${ innerColumnClasses }` }>
							<InnerBlocks.Content template={ TEMPLATE } />
                        </div>
                    </div>
                ) }
                { contentContainerWidth && 'full' === contentContainerWidth && (
                    <div className={ `container-fluid` }>
                        <div className={ `${ innerColumnClasses }` }>
                            <InnerBlocks.Content template={ TEMPLATE } />
                        </div>
                    </div>
                ) }
            </div>
		);
	}
}
export default NanosteamRowLayoutSave;
