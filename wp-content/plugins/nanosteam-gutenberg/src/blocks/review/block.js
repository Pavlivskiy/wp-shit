/**
 * General Project Admin functions compatable with Gutenberg
 *
 */

/**
 * Internal block libraries
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const {
	InnerBlocks,
	ColorPalette,
	InspectorControls,
	Fragment,
} = wp.editor;

const {
	withState,
} = wp.compose;

const {
	dispatch,
	select,
} = wp.data;

wp.domReady( function() {
	function isOneChecked( elementName ) {
		// All <input> tags...
		const chx = document.getElementsByName( elementName );
		for ( let i = 0; i < chx.length; i++ ) {
			// If you have more than one radio group, also check the name attribute
			// for the one you want as in && chx[i].name == 'choose'
			// Return true from the function on first match of a checked item
			if ( chx[ i ].type == 'radio' && chx[ i ].checked ) {
				if ( chx[ i ].value !== 'pending' ) {
					return true;
				}
			}
		}
		// End of the loop, return false
		return false;
	}

	function addRemovePrePubNote( prePublishDiv, projectReviewApproved, matchingRadio, postStatus ) {
		const notificationDiv = jQuery( '#added-note' );
		const draftNotification = jQuery( '#draft-note' );

		if ( jQuery( prePublishDiv ).length > 0 && notificationDiv.length === 0 && ( postStatus === 'pending' || postStatus === 'publish' ) ) {
			if ( ! projectReviewApproved.checked ) {
				jQuery( prePublishDiv ).prepend( '<div id="added-note"><p id="project_rev_req"><strong>You need to approve the project in <span style="color:red">Project Review</span> for this button to be enabled.</strong></p></div>' );
			} else if ( projectReviewApproved.checked ) {
				jQuery( '#project_rev_req' ).remove();
			}
			if ( matchingRadio.length > 0 && isOneChecked( 'fifty_fifty_grant' ) === false ) {
				if ( jQuery( '#project_rev_req' ).length === 0 ) {
					jQuery( prePublishDiv ).prepend( '<div id="added-note"><p><strong>You also need to either approve or decline the <span style="color:red">Matching Grant</span> Application.</strong></p></div>' );
				} else {
					jQuery( '#project_rev_req' ).after( '<p id="match_rev_req"><strong>You also need to either approve or decline the <span style="color:red">Matching Grant</span> Application.</strong></p>' );
				}

				jQuery( '#nano_grant h2' ).css( 'color', '#FF0000' );
			}
		}

		if ( postStatus === 'draft' && draftNotification.length === 0 ) {
			jQuery( prePublishDiv ).prepend( '<div id="draft-note"><p><strong>This project is in <strong>Draft</strong> mode and cannot be published.</p></div>' );
			dispatch( 'core/editor' ).lockPostSaving();
		}

		if ( projectReviewApproved.checked ) {
			jQuery( '#project_rev_req' ).hide();
		}
		if ( isOneChecked( 'fifty_fifty_grant' ) === true ) {
			jQuery( '#match_rev_req' ).hide();
		}
		if ( projectReviewApproved.checked && isOneChecked( 'fifty_fifty_grant' ) === true ) {
			notificationDiv.hide();
		}

		if ( notificationDiv.length === 1 ) {
			if ( ! projectReviewApproved.checked ) {
				jQuery( '#project_rev_req' ).show();
			}
			if ( isOneChecked( 'fifty_fifty_grant' ) === false ) {
				jQuery( '#match_rev_req' ).show();
			}
			if ( ! projectReviewApproved.checked && isOneChecked( 'fifty_fifty_grant' ) === false ) {
				notificationDiv.show();
			}
		}
	}

	if ( null !== document.querySelector( '#nano_project_id' ) ) {
		const nanContainer = document.querySelector( '#nano_project_id' );
		const nanChildNode = nanContainer.querySelector( '.inside strong' ).innerText;
		const projectReviewApproved = document.getElementById( 'approved' );
		const projectReviewdeclined = document.getElementById( 'resubmit' );
		const prePublishDiv = document.getElementsByClassName( 'editor-post-publish-panel__prepublish' );
		const publishButton = document.getElementsByClassName( 'editor-post-publish-panel__toggle' );
		const publishedState = select( 'core/editor' ).isCurrentPostPublished();
		const postStatus = select( 'core/editor' ).getEditedPostAttribute( 'status' );

		// Matching Grant Elements

		const matchingRadio = document.getElementsByName( 'fifty_fifty_grant' );

		if ( nanChildNode === 'PENDING' && publishedState === false ) {
			jQuery( '#wpbody-content' ).on( 'blur', '.components-navigate-regions', function() {
				addRemovePrePubNote( prePublishDiv, projectReviewApproved, matchingRadio, postStatus );
			} );

			jQuery( '#nano_project_review h2' ).css( 'color', '#FF0000' );

			dispatch( 'core/editor' ).lockPostSaving();

			if ( projectReviewApproved.checked && matchingRadio.length === 0 ) {
				dispatch( 'core/editor' ).unlockPostSaving();
			}
			if ( projectReviewApproved.checked && matchingRadio.length > 0 && isOneChecked( 'fifty_fifty_grant' ) === true ) {
				dispatch( 'core/editor' ).unlockPostSaving();
			}

			projectReviewApproved.addEventListener( 'change', function() {
				if ( projectReviewApproved.checked && matchingRadio.length === 0 ) {
					dispatch( 'core/editor' ).unlockPostSaving();
				}
				if ( projectReviewApproved.checked && matchingRadio.length > 0 && isOneChecked( 'fifty_fifty_grant' ) === true ) {
					dispatch( 'core/editor' ).unlockPostSaving();
				}
				addRemovePrePubNote( prePublishDiv, projectReviewApproved, matchingRadio, postStatus );
			} );

			projectReviewdeclined.addEventListener( 'change', function() {
				if ( projectReviewdeclined.checked && matchingRadio.length === 0 ) {
					dispatch( 'core/editor' ).lockPostSaving();
				}
				if ( projectReviewdeclined.checked && matchingRadio.length > 0 && isOneChecked( 'fifty_fifty_grant' ) === true ) {
					dispatch( 'core/editor' ).lockPostSaving();
				}
				addRemovePrePubNote( prePublishDiv, projectReviewApproved, matchingRadio, postStatus );
			} );

			if ( matchingRadio.length > 0 ) {
				for ( let i = 0; i < matchingRadio.length; i++ ) {
					matchingRadio[ i ].addEventListener( 'change', function() {
						if ( projectReviewApproved.checked && isOneChecked( 'fifty_fifty_grant' ) === true ) {
							dispatch( 'core/editor' ).unlockPostSaving();
						}

						if ( projectReviewdeclined.checked && isOneChecked( 'fifty_fifty_grant' ) === true ) {
							dispatch( 'core/editor' ).lockPostSaving();
						}
						addRemovePrePubNote( prePublishDiv, projectReviewApproved, matchingRadio, postStatus );
					} );
				}
			}
		}
	}
} );

