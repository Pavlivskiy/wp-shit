/**
 * BLOCK: Nanosteam Advanced Heading
 *
 * Registering a block with Gutenberg.
 */

/**
 * Import Icons
 */
import icons from './icon';

// Import Nanosteam Common
import nanosteamHexToName from '../../common/hexToBrand.js';
/**
 * Import Css
 */
import './style.scss';
import './editor.scss';
import edit from './edit';
/**
 * Internal block libraries
 */
const { __ } = wp.i18n;
const {
	registerBlockType,
	createBlock,
} = wp.blocks;
const {
	RichText,
} = wp.editor;

/**
 * Register: a Gutenberg Block.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'nanosteam/advancedheading', {
	title: __( 'Advanced Heading' ),
	icon: {
		src: icons.block,
	},
	category: 'common',
	keywords: [
		__( 'title' ),
		__( 'heading' ),
		__( 'NS' ),
	],
	attributes: {
		content: {
			type: 'array',
			source: 'children',
			selector: 'h1,h2,h3,h4,h5,h6',
		},
		level: {
			type: 'number',
			default: 2,
		},
		uniqueID: {
			type: 'string',
		},
		align: {
			type: 'string',
		},
		color: {
			type: 'string',
		},
		size: {
			type: 'number',
		},
		sizeType: {
			type: 'string',
			default: 'px',
		},
		lineHeight: {
			type: 'number',
		},
		lineType: {
			type: 'string',
			default: 'px',
		},
		tabSize: {
			type: 'number',
		},
		tabLineHeight: {
			type: 'number',
		},
		mobileSize: {
			type: 'number',
		},
		mobileLineHeight: {
			type: 'number',
		},
		letterSpacing: {
			type: 'number',
		},
		typography: {
			type: 'string',
			default: '',
		},
		googleFont: {
			type: 'boolean',
			default: false,
		},
		loadGoogleFont: {
			type: 'boolean',
			default: true,
		},
		fontSubset: {
			type: 'string',
			default: '',
		},
		fontVariant: {
			type: 'string',
			default: '',
		},
		fontWeight: {
			type: 'string',
			default: 'regular',
		},
		fontStyle: {
			type: 'string',
			default: 'normal',
		},
		topMargin: {
			type: 'number',
			default: '',
		},
		bottomMargin: {
			type: 'number',
			default: '',
		},
		marginType: {
			type: 'string',
			default: 'px',
		},
		markSize: {
			type: 'array',
			default: [ '', '', '' ],
		},
		markSizeType: {
			type: 'string',
			default: 'px',
		},
		markLineHeight: {
			type: 'array',
			default: [ '', '', '' ],
		},
		markLineType: {
			type: 'string',
			default: 'px',
		},
		markLetterSpacing: {
			type: 'number',
		},
		markTypography: {
			type: 'string',
			default: '',
		},
		markFontSubset: {
			type: 'string',
			default: '',
		},
		markFontVariant: {
			type: 'string',
			default: '',
		},
		markFontWeight: {
			type: 'string',
			default: 'regular',
		},
		markFontStyle: {
			type: 'string',
			default: 'normal',
		},
		markColor: {
			type: 'string',
			default: '#f76a0c',
		},
	},
	transforms: {
		from: [
			{
				type: 'block',
				blocks: [ 'core/paragraph' ],
				transform: ( { content } ) => {
					return createBlock( 'nanosteam/advancedheading', {
						content,
					} );
				},
			},
			{
				type: 'block',
				blocks: [ 'core/heading' ],
				transform: ( { content, level } ) => {
					return createBlock( 'nanosteam/advancedheading', {
						content: content,
						level: level,
					} );
				},
			},
		],
		to: [
			{
				type: 'block',
				blocks: [ 'core/paragraph' ],
				transform: ( { content } ) => {
					return createBlock( 'core/paragraph', {
						content,
					} );
				},
			},
			{
				type: 'block',
				blocks: [ 'core/heading' ],
				transform: ( { content, level } ) => {
					return createBlock( 'core/heading', {
						content: content,
						level: level,
					} );
				},
			},
		],
	},
	edit,
	save: props => {
		const { attributes: { align, level, content, color, uniqueID, letterSpacing, topMargin, bottomMargin, marginTypes, size, sizeType, lineHeight, lineType, fontWeight } } = props;
		const tagName = 'h' + level;
		let color_text, align_text, top_margin, bottom_margin, font_weight;

		if ( color ) {
			color_text = 'text' + nanosteamHexToName( color );
		} else {
			color_text = '';
		}

		if ( align ) {
			align_text = 'text-' + align;
		} else {
			align_text = '';
		}

		if ( topMargin ) {
			top_margin = 'mt-' + topMargin;
		} else {
			top_margin = '';
		}

		if ( bottomMargin ) {
			bottom_margin = 'mb-' + bottomMargin;
		} else {
			bottom_margin = '';
		}
		if ( fontWeight ) {
			font_weight = 'font-weight-' + ( fontWeight === 'regular' || fontWeight === '400' ? 'normal' : fontWeight );
		} else {
			font_weight = '';
		}

		return (
			<RichText.Content
				tagName={ tagName }
				id={ `ns-adv-heading${ uniqueID }` }
				className={ `${ color_text } ${ align_text } ${ top_margin } ${ bottom_margin } ${ font_weight }` }
				style={ {
					letterSpacing: ( letterSpacing ? letterSpacing + 'px' : undefined ),
					fontSize: ( size ? size + sizeType : undefined ),
					lineHeight: ( lineHeight ? lineHeight + lineType : undefined ),
				} }
				value={ content }
			/>
		);
	},
	deprecated: [
		{
			attributes: {
				content: {
					type: 'array',
					source: 'children',
					selector: 'h1,h2,h3,h4,h5,h6',
				},
				level: {
					type: 'number',
					default: 2,
				},
				uniqueID: {
					type: 'string',
				},
				align: {
					type: 'string',
				},
				color: {
					type: 'string',
				},
				size: {
					type: 'number',
				},
				sizeType: {
					type: 'string',
					default: 'px',
				},
				lineHeight: {
					type: 'number',
				},
				lineType: {
					type: 'string',
					default: 'px',
				},
				tabSize: {
					type: 'number',
				},
				tabLineHeight: {
					type: 'number',
				},
				mobileSize: {
					type: 'number',
				},
				mobileLineHeight: {
					type: 'number',
				},
				letterSpacing: {
					type: 'number',
				},
				typography: {
					type: 'string',
					default: '',
				},
				fontSubset: {
					type: 'string',
					default: '',
				},
				fontVariant: {
					type: 'string',
					default: '',
				},
				fontWeight: {
					type: 'string',
					default: 'regular',
				},
				fontStyle: {
					type: 'string',
					default: 'normal',
				},
			},
			save: ( { attributes } ) => {
				const { align, level, content, color, uniqueID, letterSpacing } = attributes;
				const tagName = 'h' + level;
				return (
					<RichText.Content
						tagName={ tagName }
						id={ `ns-adv-heading${ uniqueID }` }
						style={ {
							textAlign: align,
							color: color,
							letterSpacing: ( letterSpacing ? letterSpacing + 'px' : undefined ),
						} }
						value={ content }
					/>
				);
			},
		},
	],
} );
