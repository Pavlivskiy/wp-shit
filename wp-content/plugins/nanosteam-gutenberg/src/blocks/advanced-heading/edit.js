/**
 * BLOCK: Nanosteam Advanced Heading
 *
 */

/**
 * Import Css
 */
import './editor.scss';
import './markformat';
import range from 'lodash/range';
import map from 'lodash/map';
import TypographyControls from '../../common/typography-control';
import icons from '../../common/icons';

// Importing Common Libraries
import colors from '../../common/nanoBrandColors.js';
import nanosteamHexToName from '../../common/hexToBrand.js';
/**
 * Internal block libraries
 */
const { __, sprintf } = wp.i18n;
const {
	createBlock,
} = wp.blocks;
const {
	InspectorControls,
	ColorPalette,
	BlockControls,
	AlignmentToolbar,
	RichText,
} = wp.editor;
const {
	Component,
	Fragment,
} = wp.element;
const {
	PanelBody,
	Toolbar,
	RangeControl,
	ButtonGroup,
	Button,
	Dashicon,
	TabPanel,
	SelectControl,
} = wp.components;
class NanosteamAdvancedHeading extends Component {
	componentDidMount() {
		if ( ! this.props.attributes.uniqueID ) {
			this.props.setAttributes( {
				uniqueID: '_' + this.props.clientId.substr( 2, 9 ),
			} );
		} else if ( this.props.attributes.uniqueID && this.props.attributes.uniqueID !== '_' + this.props.clientId.substr( 2, 9 ) ) {
			this.props.setAttributes( {
				uniqueID: '_' + this.props.clientId.substr( 2, 9 ),
			} );
		}
	}
	render() {
		const { attributes: { uniqueID, align, level, content, color, size, sizeType, lineType, lineHeight, tabLineHeight, tabSize, mobileSize, mobileLineHeight, letterSpacing, typography, fontVariant, fontWeight, fontStyle, fontSubset, marginType, topMargin, bottomMargin, markSize, markSizeType, markLineHeight, markLineType, markLetterSpacing, markTypography, markFontSubset, markFontVariant, markFontWeight, markFontStyle, markPadding, markPaddingControl, markColor, markBG, markBGOpacity, markBorder, markBorderWidth, markBorderOpacity, markBorderStyle }, className, setAttributes, mergeBlocks, insertBlocksAfter, onReplace } = this.props;
		const markBGString = ( markBG ? nanosteamHexToName( markBG ) : '' );
		const markBorderString = ( markBorder ? nanosteamHexToName( markBorder ) : '' );
		const tagName = 'h' + level;
		const sizeTypes = [
			{ key: 'px', name: __( 'px' ) },
			{ key: 'em', name: __( 'em' ) },
		];

		const fontMin = ( sizeType === 'em' ? 0.2 : 5 );
		const marginMin = 0;
		const marginMax = 5;
		const marginStep = 1;
		const fontMax = ( sizeType === 'em' ? 12 : 200 );
		const fontStep = ( sizeType === 'em' ? 0.1 : 1 );
		const lineMin = ( lineType === 'em' ? 0.2 : 5 );
		const lineMax = ( lineType === 'em' ? 12 : 200 );
		const lineStep = ( lineType === 'em' ? 0.1 : 1 );
		const createLevelControl = ( targetLevel ) => {
			return [ {
				icon: 'heading',
				// translators: %s: heading level e.g: "1", "2", "3"
				title: sprintf( __( 'Heading %d' ), targetLevel ),
				isActive: targetLevel === level,
				onClick: () => setAttributes( { level: targetLevel } ),
				subscript: String( targetLevel ),
			} ];
		};
		const createLevelControlToolbar = ( targetLevel ) => {
			return [ {
				icon: icons[ 'h' + targetLevel ],
				// translators: %s: heading level e.g: "1", "2", "3"
				title: sprintf( __( 'Heading %d' ), targetLevel ),
				isActive: targetLevel === level,
				onClick: () => setAttributes( { level: targetLevel } ),
				subscript: String( targetLevel ),
			} ];
		};
		const deskControls = (
			<PanelBody>
				<ButtonGroup className="ns-size-type-options" aria-label={ __( 'Size Type' ) }>
					{ map( sizeTypes, ( { name, key } ) => (
						<Button
							key={ key }
							className="ns-size-btn"
							isSmall
							isPrimary={ sizeType === key }
							aria-pressed={ sizeType === key }
							onClick={ () => setAttributes( { sizeType: key } ) }
						>
							{ name }
						</Button>
					) ) }
				</ButtonGroup>
				<RangeControl
					label={ __( 'Font Size' ) }
					value={ ( size ? size : '' ) }
					onChange={ ( value ) => setAttributes( { size: value } ) }
					min={ fontMin }
					max={ fontMax }
					step={ fontStep }
				/>
				<ButtonGroup className="ns-size-type-options" aria-label={ __( 'Size Type' ) }>
					{ map( sizeTypes, ( { name, key } ) => (
						<Button
							key={ key }
							className="ns-size-btn"
							isSmall
							isPrimary={ lineType === key }
							aria-pressed={ lineType === key }
							onClick={ () => setAttributes( { lineType: key } ) }
						>
							{ name }
						</Button>
					) ) }
				</ButtonGroup>
				<RangeControl
					label={ __( 'Line Height' ) }
					value={ ( lineHeight ? lineHeight : '' ) }
					onChange={ ( value ) => setAttributes( { lineHeight: value } ) }
					min={ lineMin }
					max={ lineMax }
					step={ lineStep }
				/>
			</PanelBody>
		);
		return (
			<Fragment>
				<style>
					{ `.ns-adv-heading${ uniqueID } mark {
						color: ${ markColor };
						background: ${ ( markBG ? markBGString : 'transparent' ) };
						font-weight: ${ ( markTypography && markFontWeight ? markFontWeight : 'inherit' ) };
						font-style: ${ ( markTypography && markFontStyle ? markFontStyle : 'inherit' ) };
						font-size: ${ ( markSize && markSize[ 0 ] ? markSize[ 0 ] + markSizeType : 'inherit' ) };
						line-height: ${ ( markLineHeight && markLineHeight[ 0 ] ? markLineHeight[ 0 ] + markLineType : 'inherit' ) };
						letter-spacing: ${ ( markLetterSpacing ? markLetterSpacing + 'px' : 'inherit' ) };
						font-family: ${ ( markTypography ? markTypography : 'inherit' ) };
						border-color: ${ ( markBorder ? markBorderString : 'transparent' ) };
						border-width: ${ ( markBorderWidth ? markBorderWidth + 'px' : '0' ) };
						border-style: ${ ( markBorderStyle ? markBorderStyle : 'solid' ) };
						padding: ${ ( markPadding ? markPadding[ 0 ] + 'px ' + markPadding[ 1 ] + 'px ' + markPadding[ 2 ] + 'px ' + markPadding[ 3 ] + 'px' : '' ) };
					}` }
				</style>
				<BlockControls>
					<Toolbar
						isCollapsed={ true }
						icon={ icons[ 'h' + level ] }
						label={ __( 'Change Heading Level' ) }
						controls={ range( 1, 7 ).map( createLevelControlToolbar ) }
					/>
					<AlignmentToolbar
						value={ align }
						onChange={ ( nextAlign ) => {
							setAttributes( { align: nextAlign } );
						} }
					/>
				</BlockControls>
				<InspectorControls>
					<PanelBody title={ __( 'Heading Settings' ) }>
						<p>{ __( 'HTML Tag' ) }</p>
						<Toolbar controls={ range( 1, 7 ).map( createLevelControl ) } />
						<p>{ __( 'Text Alignment' ) }</p>
						<AlignmentToolbar
							value={ align }
							onChange={ ( nextAlign ) => {
								setAttributes( { align: nextAlign } );
							} }
						/>
						<p className="ns-setting-label">{ __( 'Heading Color' ) }</p>
						<ColorPalette
                            colors = { colors }
							value={ color }
							onChange={ ( value ) => setAttributes( { color: value } ) }
						/>
						<h2 className="ns-heading-size-title">{ __( 'Size Controls' ) }</h2>
						{ deskControls }
						<TypographyControls
							letterSpacing={ letterSpacing }
							onLetterSpacing={ ( value ) => setAttributes( { letterSpacing: value } ) }
							fontFamily={ typography }
							onFontFamily={ ( value ) => setAttributes( { typography: value } ) }
							onFontChange={ ( select ) => {
								setAttributes( {
									typography: select.value,
								} );
							} }
							fontVariant={ fontVariant }
							onFontVariant={ ( value ) => setAttributes( { fontVariant: value } ) }
							fontWeight={ fontWeight }
							onFontWeight={ ( value ) => setAttributes( { fontWeight: value } ) }
							fontStyle={ fontStyle }
							onFontStyle={ ( value ) => setAttributes( { fontStyle: value } ) }
							fontSubset={ fontSubset }
							onFontSubset={ ( value ) => setAttributes( { fontSubset: value } ) }
						/>
					</PanelBody>
					<PanelBody
						title={ __( 'Margin Settings' ) }
						initialOpen={ false }
					>
						<RangeControl
							label={ __( 'Top Margin' ) }
							value={ ( topMargin ? topMargin : '' ) }
							onChange={ ( value ) => setAttributes( { topMargin: value } ) }
							min={ marginMin }
							max={ marginMax }
							step={ marginStep }
						/>
						<RangeControl
							label={ __( 'Bottom Margin' ) }
							value={ ( bottomMargin ? bottomMargin : '' ) }
							onChange={ ( value ) => setAttributes( { bottomMargin: value } ) }
							min={ marginMin }
							max={ marginMax }
							step={ marginStep }
						/>
					</PanelBody>
				</InspectorControls>
				<RichText
					formattingControls={ [ 'bold', 'italic', 'link', 'strikethrough', 'mark' ] }
					wrapperClassName={ className + ' mt-' + topMargin + ' mb-' + bottomMargin + ' ' }
					tagName={ tagName }
					value={ content }
					onChange={ ( value ) => setAttributes( { content: value } ) }
					onMerge={ mergeBlocks }
					unstableOnSplit={
						insertBlocksAfter ?
							( before, after, ...blocks ) => {
								setAttributes( { content: before } );
								insertBlocksAfter( [
									...blocks,
									createBlock( 'core/paragraph', { content: after } ),
								] );
							} :
							undefined
					}
					onRemove={ () => onReplace( [] ) }
					style={ {
						textAlign: align,
						color: color,
						fontWeight: fontWeight,
						fontStyle: fontStyle,
						fontSize: size + sizeType,
						lineHeight: lineHeight + lineType,
						letterSpacing: letterSpacing + 'px',
						fontFamily: ( typography ? typography : '' ),
					} }
					className={ `ns-adv-heading${ uniqueID }` }
					placeholder={ __( 'Write heading…' ) }
				/>
			</Fragment>
		);
	}
}
export default ( NanosteamAdvancedHeading );
