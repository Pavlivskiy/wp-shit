/**
 * BLOCK: Nano Column
 *
 * Registering a basic block with Gutenberg.
 */

/**
 * Import Icons
 */
import icons from '../../common/icons';

/**
 * Internal block libraries
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const {
	InnerBlocks,
	ColorPalette,
	InspectorControls,
} = wp.editor;
const {
	TabPanel,
	Dashicon,
	PanelBody,
	RangeControl,
} = wp.components;

// Importing Common Libraries
import colors from '../../common/nanoBrandColors.js';
import nanosteamHexToName from '../../common/hexToBrand.js';

function nanosteamHexToRGB( hex, alpha ) {
    hex = hex.replace( '#', '' );
    const r = parseInt( hex.length === 3 ? hex.slice( 0, 1 ).repeat( 2 ) : hex.slice( 0, 2 ), 16 );
    const g = parseInt( hex.length === 3 ? hex.slice( 1, 2 ).repeat( 2 ) : hex.slice( 2, 4 ), 16 );
    const b = parseInt( hex.length === 3 ? hex.slice( 2, 3 ).repeat( 2 ) : hex.slice( 4, 6 ), 16 );
    return 'rgba(' + r + ', ' + g + ', ' + b + ', ' + alpha + ')';
}

/**
 * Update Column
 */
function onChangeWidth(id, columns, layout, tabletLayout, mobileLayout) {

    let col = layout;
    let tabCol = '';
    let mobCol = '';
    const colId = id - 1;

    if ( columns === 1 ) {

        col = ['col-sm-12'];

    } else if ( columns === 2 ) {

        // Equal
        if ( layout === 'equal' ) {
            col = ['col-lg-6', 'col-lg-6'];
        }
        if ( tabletLayout === 'equal' ) {
            tabCol = ['col-sm-6', 'col-sm-6'];
        }
        if ( mobileLayout === 'equal' ) {
            mobCol = ['col-6', 'col-6'];
        }
        // Left Golden
        if ( layout === 'left-golden' ) {
            col = ['col-lg-9', 'col-lg-3'];
        }
        if ( tabletLayout === 'left-golden' ) {
            tabCol = ['col-sm-9', 'col-sm-3'];
        }
        if ( mobileLayout === 'left-golden' ) {
            mobCol = ['col-8', 'col-4'];
        }
        // Right Golden
        if ( layout === 'right-golden' ) {
            col = ['col-lg-3', 'col-lg-9'];
        }
        if ( tabletLayout === 'right-golden' ) {
            tabCol = ['col-sm-3', 'col-sm-9'];
        }
        if ( mobileLayout === 'right-golden' ) {
            mobCol = ['col-4', 'col-8'];
        }
        // Inherit
        if ( tabletLayout === 'inherit' ) {
            tabCol = ['', ''];
        }
        if ( mobileLayout === 'row' ) {
            mobCol = ['', ''];
        }

    } else if ( columns === 3 ) {

        // Equal
        if ( layout === 'equal' ) {
            col = ['col-lg-4', 'col-lg-4', 'col-lg-4'];
        }
        if ( tabletLayout === 'equal' ) {
            tabCol = ['col-sm-4', 'col-sm-4', 'col-sm-4'];
        }
        if ( mobileLayout === 'equal' ) {
            mobCol = ['col-4', 'col-4', 'col-4'];
        }
        // Left Half
        if ( layout === 'left-half' ) {
            col = ['col-lg-6', 'col-lg-3', 'col-lg-3'];
        }
        if ( tabletLayout === 'left-half' ) {
            tabCol = ['col-sm-6', 'col-sm-3', 'col-sm-3'];
        }
        if ( mobileLayout === 'left-half' ) {
            mobCol = ['col-8', 'col-4', 'col-lg-4'];
        }
        // Right Half
        if ( layout === 'right-half' ) {
            col = ['col-lg-3', 'col-lg-3', 'col-lg-6'];
        }
        if ( tabletLayout === 'right-half' ) {
            tabCol = ['col-sm-3', 'col-sm-3', 'col-sm-6'];
        }
        if ( mobileLayout === 'right-half' ) {
            mobCol = ['col-3', 'col-3', 'col-6'];
        }
        // Center Half
        if ( layout === 'center-half' ) {
            col = ['col-lg-3', 'col-lg-6', 'col-lg-3'];
        }
        if ( tabletLayout === 'center-half' ) {
            tabCol = ['col-sm-3', 'col-sm-6', 'col-sm-3'];
        }
        if ( mobileLayout === 'center-half' ) {
            mobCol = ['col-3', 'col-6', 'col-3'];
        }
        // Center Wide
        if ( layout === 'center-wide' ) {
            col = ['col-lg-2', 'col-lg-8', 'col-lg-2'];
        }
        if ( tabletLayout === 'center-wide' ) {
            tabCol = ['col-sm-2', 'col-sm-8', 'col-sm-2'];
        }
        if ( mobileLayout === 'center-wide' ) {
            mobCol = ['col-2', 'col-8', 'col-2'];
        }
        // Center Wide
        if ( layout === 'center-exwide' ) {
            col = ['col-lg-1', 'col-lg-10', 'col-lg-1'];
        }
        if ( tabletLayout === 'center-exwide' ) {
            tabCol = ['col-sm-1', 'col-sm-10', 'col-sm-1'];
        }
        if ( mobileLayout === 'center-exwide' ) {
            mobCol = ['col-1', 'col-10', 'col-1'];
        }
        // Inherit
        if ( tabletLayout === 'inherit' ) {
            tabCol = ['', '', ''];
        }
        if ( mobileLayout === 'row' ) {
            mobCol = ['', '', ''];
        }

    } else if ( columns === 4 ) {

        // Equal
        if ( layout === 'equal' ) {
            col = ['col-lg-3', 'col-lg-3' , 'col-lg-3', 'col-lg-3'];
        }
        if ( tabletLayout === 'equal' ) {
            tabCol = ['col-sm-3', 'col-sm-3' , 'col-sm-3', 'col-sm-3'];
        }
        if ( mobileLayout === 'equal' ) {
            mobCol = ['col-3', 'col-3' , 'col-3', 'col-3'];
        }
        // left-forty
        if ( layout === 'equal' ) {
            col = ['col-lg-5', 'col-lg-3' , 'col-lg-3', 'col-lg-2'];
        }
        if ( tabletLayout === 'equal' ) {
            tabCol = ['col-sm-5', 'col-sm-3' , 'col-sm-3', 'col-sm-2'];
        }
        if ( mobileLayout === 'equal' ) {
            mobCol = ['col-5', 'col-3' , 'col-3', 'col-2'];
        }
        // right-half
        if ( layout === 'equal' ) {
            col = ['col-lg-2', 'col-lg-3' , 'col-lg-3', 'col-lg-5'];
        }
        if ( tabletLayout === 'equal' ) {
            tabCol = ['col-sm-2', 'col-sm-3' , 'col-sm-3', 'col-sm-5'];
        }
        if ( mobileLayout === 'equal' ) {
            mobCol = ['col-2', 'col-3' , 'col-3', 'col-5'];
        }
        //Grid
        if ( tabletLayout === 'two-grid' ) {
            mobCol = ['col-sm-6', 'col-sm-6' , 'col-sm-6', 'col-sm-6'];
        }
        if ( mobileLayout === 'two-grid' ) {
            mobCol = ['col-6', 'col-6' , 'col-6', 'col-6'];
        }
        // Inherit
        if ( tabletLayout === 'inherit' || tabletLayout === 'row') {
            tabCol = ['', '', '', ''];
        }
        if ( mobileLayout === 'row' ) {
            mobCol = ['', '', '', ''];
        }

    } else if ( columns === 5 ) {

        // Left Golden
        if ( layout === 'left-five' ) {
            col = ['col-lg-4', 'col-lg-2', 'col-lg-2', 'col-lg-2', 'col-lg-2'];
        }
        if ( tabletLayout === 'left-five' ) {
            tabCol = ['col-sm-4', 'col-sm-2', 'col-sm-2', 'col-sm-2', 'col-sm-2'];
        }
        if ( mobileLayout === 'left-five' ) {
            mobCol = ['col-4', 'col-2', 'col-2', 'col-2', 'col-2'];
        }
        // Right Golden
        if ( layout === 'right-five' ) {
            col = ['col-lg-2', 'col-lg-2', 'col-lg-2', 'col-lg-2', 'col-lg-4'];
        }
        if ( tabletLayout === 'right-five' ) {
            tabCol = ['col-sm-2', 'col-sm-2', 'col-sm-2', 'col-sm-2', 'col-sm-4'];
        }
        if ( mobileLayout === 'right-five' ) {
            mobCol = ['col-2', 'col-2', 'col-2', 'col-2', 'col-4'];
        }
        // Inherit
        if ( tabletLayout === 'inherit' || tabletLayout === 'row' ) {
            tabCol = ['', '', '', '', '', ''];
        }
        if ( mobileLayout === 'row' ) {
            mobCol = ['', '', '', '', '', ''];
        }

    } else if ( columns === 6 ) {

        // Left Golden
        if ( layout === 'equal' ) {
            col = ['col-lg-2', 'col-lg-2', 'col-lg-2', 'col-lg-2', 'col-lg-2', 'col-lg-2'];
        }
        if ( tabletLayout === 'equal' ) {
            tabCol = ['col-sm-2', 'col-sm-2', 'col-sm-2', 'col-sm-2', 'col-sm-2', 'col-sm-2'];
        }
        if ( mobileLayout === 'equal' ) {
            mobCol = ['col-2', 'col-2', 'col-2', 'col-2', 'col-2', 'col-2'];
        }
        // Two Grid
        if ( tabletLayout === 'two-grid' ) {
            tabCol = ['col-sm-6', 'col-sm-6', 'col-sm-6', 'col-sm-6', 'col-sm-6', 'col-sm-6'];
        }
        if ( mobileLayout === 'right-five' ) {
            mobCol = ['col-6', 'col-6', 'col-6', 'col-6', 'col-6', 'col-6'];
        }
        // Three Grid
        if ( tabletLayout === 'two-grid' ) {
            tabCol = ['col-sm-4', 'col-sm-4', 'col-sm-4', 'col-sm-4', 'col-sm-4', 'col-sm-4'];
        }
        if ( mobileLayout === 'right-five' ) {
            mobCol = ['col-4', 'col-4', 'col-4', 'col-4', 'col-4', 'col-4'];
        }
        // Inherit
        if ( tabletLayout === 'inherit' || tabletLayout === 'row' ) {
            tabCol = ['', '', '', '', '', '', ''];
        }
        if ( mobileLayout === 'row' ) {
            mobCol = ['', '', '', '', '', '', ''];
        }

    }

    return [ col, tabCol, mobCol ];

}
/**
 * Register: a Gutenberg Block.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */

registerBlockType( 'nanosteam/column', {
	title: __( 'My Column' ),
	icon: icons.blockColumn,
	category: 'common',
	parent: [ 'nanosteam/rowlayout' ],
	attributes: {
		id: {
			type: 'number',
			default: 1,
		},
		topPadding: {
			type: 'number',
			default: '',
		},
		bottomPadding: {
			type: 'number',
			default: '',
		},
		leftPadding: {
			type: 'number',
			default: '',
		},
		rightPadding: {
			type: 'number',
			default: '',
		},
		topPaddingM: {
			type: 'number',
			default: '',
		},
		bottomPaddingM: {
			type: 'number',
			default: '',
		},
		leftPaddingM: {
			type: 'number',
			default: '',
		},
		rightPaddingM: {
			type: 'number',
			default: '',
		},
		topMargin: {
			type: 'number',
			default: '',
		},
		bottomMargin: {
			type: 'number',
			default: '',
		},
		topMarginM: {
			type: 'number',
			default: '',
		},
		bottomMarginM: {
			type: 'number',
			default: '',
		},
		background: {
			type: 'string',
			default: '',
		},
		backgroundOpacity: {
			type: 'number',
			default: 1,
		},
        columns: {
            type: 'number',
            default: '',
        },
        colLayout: {
            type: 'string',
            default: '',
        },
	},
	edit: props => {
		const { attributes: { columns, colLayout, id, topPadding, bottomPadding, leftPadding, rightPadding, topPaddingM, bottomPaddingM, leftPaddingM, rightPaddingM, topMargin, bottomMargin, topMarginM, bottomMarginM, backgroundOpacity, background }, setAttributes } = props;
		
		const mobileControls = (
			<PanelBody
				title={ __( 'Mobile Padding/Margin' ) }
				initialOpen={ false }
			>
				<h2>{ __( 'Mobile Padding (px)' ) }</h2>
				<RangeControl
					label={ icons.outlinetop }
					value={ topPaddingM }
					className="ns-icon-rangecontrol ns-top-padding"
					onChange={ ( value ) => {
						setAttributes( {
							topPaddingM: value,
						} );
					} }
					min={ 0 }
					max={ 500 }
				/>
				<RangeControl
					label={ icons.outlineright }
					value={ rightPaddingM }
					className="ns-icon-rangecontrol ns-right-padding"
					onChange={ ( value ) => {
						setAttributes( {
							rightPaddingM: value,
						} );
					} }
					min={ 0 }
					max={ 500 }
				/>
				<RangeControl
					label={ icons.outlinebottom }
					value={ bottomPaddingM }
					className="ns-icon-rangecontrol ns-bottom-padding"
					onChange={ ( value ) => {
						setAttributes( {
							bottomPaddingM: value,
						} );
					} }
					min={ 0 }
					max={ 500 }
				/>
				<RangeControl
					label={ icons.outlineleft }
					value={ leftPaddingM }
					className="ns-icon-rangecontrol ns-left-padding"
					onChange={ ( value ) => {
						setAttributes( {
							leftPaddingM: value,
						} );
					} }
					min={ 0 }
					max={ 500 }
				/>
				<h2>{ __( 'Mobile Margin (px)' ) }</h2>
				<RangeControl
					label={ icons.outlinetop }
					value={ topMarginM }
					className="ns-icon-rangecontrol ns-top-margin"
					onChange={ ( value ) => {
						setAttributes( {
							topMarginM: value,
						} );
					} }
					min={ -200 }
					max={ 200 }
				/>
				<RangeControl
					label={ icons.outlinebottom }
					value={ bottomMarginM }
					className="ns-icon-rangecontrol ns-bottom-margin"
					onChange={ ( value ) => {
						setAttributes( {
							bottomMarginM: value,
						} );
					} }
					min={ -200 }
					max={ 200 }
				/>
			</PanelBody>
		);
		const deskControls = (
			<PanelBody
				title={ __( 'Padding/Margin' ) }
				initialOpen={ true }
			>
				<h2>{ __( 'Padding (px)' ) }</h2>
				<RangeControl
					label={ icons.outlinetop }
					value={ topPadding }
					className="ns-icon-rangecontrol ns-top-padding"
					onChange={ ( value ) => {
						setAttributes( {
							topPadding: value,
						} );
					} }
					min={ 0 }
					max={ 500 }
				/>
				<RangeControl
					label={ icons.outlineright }
					value={ rightPadding }
					className="ns-icon-rangecontrol ns-right-padding"
					onChange={ ( value ) => {
						setAttributes( {
							rightPadding: value,
						} );
					} }
					min={ 0 }
					max={ 500 }
				/>
				<RangeControl
					label={ icons.outlinebottom }
					value={ bottomPadding }
					className="ns-icon-rangecontrol ns-bottom-padding"
					onChange={ ( value ) => {
						setAttributes( {
							bottomPadding: value,
						} );
					} }
					min={ 0 }
					max={ 500 }
				/>
				<RangeControl
					label={ icons.outlineleft }
					value={ leftPadding }
					className="ns-icon-rangecontrol ns-left-padding"
					onChange={ ( value ) => {
						setAttributes( {
							leftPadding: value,
						} );
					} }
					min={ 0 }
					max={ 500 }
				/>
				<h2>{ __( 'Margin (px)' ) }</h2>
				<RangeControl
					label={ icons.outlinetop }
					value={ topMargin }
					className="ns-icon-rangecontrol ns-top-margin"
					onChange={ ( value ) => {
						setAttributes( {
							topMargin: value,
						} );
					} }
					min={ -200 }
					max={ 200 }
				/>
				<RangeControl
					label={ icons.outlinebottom }
					value={ bottomMargin }
					className="ns-icon-rangecontrol ns-bottom-margin"
					onChange={ ( value ) => {
						setAttributes( {
							bottomMargin: value,
						} );
					} }
					min={ -200 }
					max={ 200 }
				/>
			</PanelBody>
		);
		const tabControls = (
			<TabPanel className="ns-inspect-tabs"
				activeClass="active-tab"
				tabs={ [
					{
						name: 'desk',
						title: <Dashicon icon="desktop" />,
						className: 'ns-desk-tab',
					},
					{
						name: 'mobile',
						title: <Dashicon icon="smartphone" />,
						className: 'ns-mobile-tab',
					},
				] }>
				{
					( tab ) => {
						let tabout;
						if ( tab.name ) {
							if ( 'mobile' === tab.name ) {
								tabout = mobileControls;
							} else {
								tabout = deskControls;
							}
						} else {
							if ( 'mobile' === tab ) {
								tabout = mobileControls;
							} else {
								tabout = deskControls;
							}
						}
						return <div>{ tabout }</div>;
					}
				}
			</TabPanel>
		);
		const backgroundString = ( background ? nanosteamHexToRGB( background, backgroundOpacity ) : 'transparent' );

		return (
			<div className={ `nanosteam-column inner-column-${ id }` }>
				<InspectorControls>
					<h2>{ __( 'Background Color' ) }</h2>
					<ColorPalette
						colors = { colors }
						value={ background }
						onChange={ ( value ) => setAttributes( { background: value } ) }
					/>
				</InspectorControls>
				<div className="nanosteam-inner-column-inner px-3 py-1" style={ {
                    background: backgroundString,
                } }>
					<InnerBlocks templateLock={ false } />
				</div>
			</div>
		);
	},

	save( { attributes } ) {

		const { id, background, backgroundOpacity, columns, colLayout } = attributes;
		const backgroundName = ( background ? nanosteamHexToName( background ) : '' );
		const startingValues = [ 'equal', 'left-golden', 'right-golden', 'left-half', 'right-half', 'center-half', 'center-wide', 'center-exwide', 'left-forty', 'right-forty', 'left-five', 'right-five' ];

        let colLayoutAdjusted = '';
        // When we first insert the columns we will need to change their initial state top proper bootstrap columns
        if ( startingValues.indexOf( colLayout ) > -1 ) {
            const firstColLayout = onChangeWidth( id, columns, colLayout, 'inherit', 'row' );
            colLayoutAdjusted = firstColLayout[ 0 ][ id - 1 ];
        } else {
            colLayoutAdjusted = colLayout;
		}

		return (
			<div className={ `inner-column-${ id } ${ backgroundName } ${ colLayoutAdjusted }` }>
				<InnerBlocks.Content />
			</div>
		);
	},
	deprecated: [
		{
			attributes: {
				id: {
					type: 'number',
					default: 1,
				},
			},
			save: ( { attributes } ) => {
				const { id } = attributes;
				return (
                    <div className={ `inner-column-${ id } col` }>
                        <InnerBlocks.Content />
                    </div>
				);
			},
		},
		{
			attributes: {
				id: {
					type: 'number',
					default: 1,
				},
			},
			save: ( { attributes } ) => {
				const { id } = attributes;

				return (
                    <div className={ `inner-column-${ id } col` }>
                        <InnerBlocks.Content />
                    </div>
				);
			},
		},
	],
} );
