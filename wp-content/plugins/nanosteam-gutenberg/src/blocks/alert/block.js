/**
 * BLOCK: Nano Alert
 *
 * Registering a basic block with Gutenberg.
 */


/**
 * Internal block libraries
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const {
	InnerBlocks,
	ColorPalette,
	InspectorControls,
    Fragment,
} = wp.editor;

const {
	TabPanel,
	Dashicon,
	PanelBody,
	RangeControl,
    FormToggle,
} = wp.components;

const {
    withState,
} = wp.compose;


// Importing Common Libraries
import colors from '../../common/nanoBrandColors.js';
import nanosteamHexToName from '../../common/hexToBrand.js';

function nanosteamHexToRGB( hex, alpha ) {
    hex = hex.replace( '#', '' );
    const r = parseInt( hex.length === 3 ? hex.slice( 0, 1 ).repeat( 2 ) : hex.slice( 0, 2 ), 16 );
    const g = parseInt( hex.length === 3 ? hex.slice( 1, 2 ).repeat( 2 ) : hex.slice( 2, 4 ), 16 );
    const b = parseInt( hex.length === 3 ? hex.slice( 2, 3 ).repeat( 2 ) : hex.slice( 4, 6 ), 16 );
    return 'rgba(' + r + ', ' + g + ', ' + b + ', ' + alpha + ')';
}

/**
 * Register: a Gutenberg Block.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */

registerBlockType( 'nanosteam/alert', {
	title: __( 'Alert' ),
	icon: 'warning',
	category: 'common',
	attributes: {
        aColor: {
            type: 'string',
            default: 'info',
        },
        aDismissable: {
            type: 'Boolean',
            default: false,
        },
	},
	edit: props => {

		const { attributes: { aColor, aDismissable }, setAttributes } = props;
		const alertColor = nanosteamHexToName( aColor );

		return (
			<div className={ `nanosteam-alert` }>

				<InspectorControls>
					<h2>{ __( 'Alert Color' ) }</h2>
					<ColorPalette
						colors = { colors }
						value={ aColor }
						onChange={ ( value ) => setAttributes( { aColor: value } ) }
					/>
                    <h2>{ __( 'Dismissable' ) }</h2>
                    <FormToggle
                        checked={ aDismissable }
                        onChange={ () => setAttributes( {aDismissable: ! aDismissable } ) }
                    />
				</InspectorControls>

				<div className={ `px-3 py-1 alert alert${ alertColor }` }>
					<InnerBlocks templateLock={ false } />
				</div>
			</div>
		);
	},

	save( { attributes } ) {

		const { aColor, aDismissable } = attributes;
        const alertColor = nanosteamHexToName( aColor );
        let dismissableButton, dismissClass;

        if ( aDismissable === true ) {
            dismissableButton = (
                <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            );
			dismissClass = 'alert-dismissible fade show';
        }

		return (
			<div className={ `alert alert${ alertColor } ${ dismissClass }` }>
				{ dismissableButton }
				<InnerBlocks.Content />
			</div>
		);
	},
	deprecated: [
		{
			attributes: {
				aColor: {
					type: 'string',
					default: '',
				},
                aDismissable: {
                    type: 'Boolean',
                    default: false,
                },
			},
			save: ( { attributes } ) => {

				const { aColor, aDismissable } = attributes;
                const alertColor = nanosteamHexToName( aColor );
                let dismissableButton, dismissClass;

                if ( aDismissable === true ) {
                    dismissableButton = (
                        <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    );
                    dismissClass = 'alert-dismissible fade show';
                }

				return (
                    <div className={ `alert alert${ alertColor } ${ dismissClass }` }>
						{ dismissableButton }
                        <InnerBlocks.Content />
                    </div>
                );
			},
		},
	],
} );
