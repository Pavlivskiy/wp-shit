<?php
/**
 * Blocks Initializer
 *
 * Enqueue CSS/JS of all the blocks.
 *
 * @since   1.0.0
 * @package CGB
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Enqueue Gutenberg block assets for both frontend + backend.
 *
 * @uses {wp-editor} for WP editor styles.
 * @since 1.0.0
 */
function nano_block_cgb_block_assets() { // phpcs:ignore
	// Styles.
	wp_enqueue_style(
		'nano_block-cgb-style-css', // Handle.
		plugins_url( 'dist/blocks.style.build.css', dirname( __FILE__ ) ), // Block style CSS.
		array( 'wp-editor' ) // Dependency to include the CSS after it.
		// filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.style.build.css' ) // Version: File modification time.
	);
}

// Hook: Frontend assets.
add_action( 'enqueue_block_assets', 'nano_block_cgb_block_assets' );

/**
 * Enqueue Gutenberg block assets for backend editor.
 *
 * @uses {wp-blocks} for block type registration & related functions.
 * @uses {wp-element} for WP Element abstraction — structure of blocks.
 * @uses {wp-i18n} to internationalize the block's text.
 * @uses {wp-editor} for WP editor styles.
 * @since 1.0.0
 */
function nano_block_cgb_editor_assets() { // phpcs:ignore
	// Scripts.
	wp_enqueue_script(
		'nano_block-cgb-block-js', // Handle.
		plugins_url( '/dist/blocks.build.js', dirname( __FILE__ ) ), // Block.build.js: We register the block here. Built with Webpack.
		array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor', 'wp-dom-ready', 'wp-data' ), // Dependencies, defined above.
		filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.build.js' ), // Version: File modification time.
		true // Enqueue the script in the footer.
	);

	$editor_widths  = get_option( 'nano_block_editor_width', array() );
	$sidebar_size   = 750;
	$nosidebar_size = 1140;
	$jssize         = 2000;
	if ( ! isset( $editor_widths['enable_editor_width'] ) || 'true' === $editor_widths['enable_editor_width'] ) {
		if ( isset( $editor_widths['limited_margins'] ) && 'true' === $editor_widths['limited_margins'] ) {
			$add_size = 10;
		} else {
			$add_size = 30;
		}
		$post_type = get_post_type();
		if ( isset( $editor_widths['page_default'] ) && ! empty( $editor_widths['page_default'] ) && isset( $editor_widths['post_default'] ) && ! empty( $editor_widths['post_default'] ) ) {
			if ( isset( $post_type ) && 'page' === $post_type ) {
				$defualt_size_type = $editor_widths['page_default'];
			} else {
				$defualt_size_type = $editor_widths['post_default'];
			}
		} else {
			$defualt_size_type = 'sidebar';
		}
		if ( isset( $editor_widths['sidebar'] ) && ! empty( $editor_widths['sidebar'] ) ) {
			$sidebar_size = $editor_widths['sidebar'] + $add_size;
		} else {
			$sidebar_size = 750;
		}
		if ( isset( $editor_widths['nosidebar'] ) && ! empty( $editor_widths['nosidebar'] ) ) {
			$nosidebar_size = $editor_widths['nosidebar'] + $add_size;
		} else {
			$nosidebar_size = 1140 + $add_size;
		}
		if ( 'sidebar' == $defualt_size_type ) {
			$default_size = $sidebar_size;
		} elseif ( 'fullwidth' == $defualt_size_type ) {
			$default_size = 'none';
		} else {
			$default_size = $nosidebar_size;
		}
		if ( 'none' === $default_size ) {
			$jssize = 2000;
		} else {
			$jssize = $default_size;
		}
	}
	wp_localize_script( 'nano_block-cgb-block-js', 'nanosteam_blocks_params', array(
		'sidebar_size'   => $sidebar_size,
		'nosidebar_size' => $nosidebar_size,
		'default_size'   => $jssize,
		'config'         => get_option( 'nano_block_config_blocks' ),
		'settings'       => get_option( 'nano_block_settings_blocks' ),
	) );

	// Styles.
	wp_enqueue_style(
		'nano_block-cgb-block-editor-css', // Handle.
		plugins_url( 'dist/blocks.editor.build.css', dirname( __FILE__ ) ), // Block editor CSS.
		array( 'wp-edit-blocks' ) // Dependency to include the CSS after it.
		// filemtime( plugin_dir_path( __DIR__ ) . 'dist/blocks.editor.build.css' ) // Version: File modification time.
	);
}

// Hook: Editor assets.
add_action( 'enqueue_block_editor_assets', 'nano_block_cgb_editor_assets' );

/**
 * Disable the custom color picker.
 */
function nano_gutenberg_disable_custom_colors() {
	add_theme_support( 'disable-custom-colors' );
}
add_action( 'after_setup_theme', 'nano_gutenberg_disable_custom_colors' );


add_filter( 'allowed_block_types', 'nano_allowed_block_types', 10, 2 );

function nano_allowed_block_types( $allowed_blocks, $post ) {

	$allowed_blocks = array(
		'core/image',
		'core/paragraph',
		'core/heading',
		'core/list',
		'core/quote',
		'core/table',
		'core/html',
		'core/button',
		'core/media-text',
		'core/more',
		'core/nextpage',
		'core/separator',
		'core/spacer',
		'core/shortcode',
		'core-embed/twitter',
		'core-embed/youtube',
		'core-embed/vimeo',
		// Nanosteam
		'nanosteam/rowlayout',
		'nanosteam/column',
		'nanosteam/advancedbtn',
		'nanosteam/carousel',
		'nanosteam/advancedheading',
		'nanosteam/alert',
		'nanosteam/carousel',
		'nanosteam/accordion'
	);

	if( $post->post_type === 'page' ) {
		//$allowed_blocks[] = 'core/shortcode';
	}

	return $allowed_blocks;

}

// Adding to project categories for gutenberg

function wpse_modify_taxonomy() {
	// get the arguments of the already-registered taxonomy
	$project_category_args = get_taxonomy( 'project_category' ); // returns an object

	// make changes to the args
	// in this example there are three changes
	// again, note that it's an object
	$project_category_args->show_in_rest = true;
	$project_category_args->rest_base    = 'project_category';
	//$project_category_args->rewrite['with_front'] = false;

	// re-register the taxonomy
	register_taxonomy( 'project_category', 'ignition_product', (array) $project_category_args );
}
// hook it up to 11 so that it overrides the original register_taxonomy function
add_action( 'init', 'wpse_modify_taxonomy', 11 );