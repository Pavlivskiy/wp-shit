const colors = [
	{ name: 'primary', color: '#FFB400' },
	{ name: 'secondary', color: '#848e97' },
	{ name: 'success', color: '#3dc644' },
	{ name: 'info', color: '#005dcb' },
	{ name: 'warning', color: '#fc9c19' },
	{ name: 'danger', color: '#d72229' },
	{ name: 'light', color: '#f6f6f6' },
	{ name: 'dark', color: '#36404a' },
];

export default colors;
