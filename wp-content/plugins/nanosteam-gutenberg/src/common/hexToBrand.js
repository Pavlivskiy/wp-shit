export default function nanosteamHexToName( hex ) {
    let name = '';
    name = hex === '#FFB400' ? '-primary' : '-primary';
    name = hex === '#848e97' ? '-secondary' : name;
    name = hex === '#3dc644' ? '-success' : name;
    name = hex === '#005dcb' ? '-info' : name;
    name = hex === '#fc9c19' ? '-warning' : name;
    name = hex === '#d72229' ? '-danger' : name;
    name = hex === '#f6f6f6' ? '-light' : name;
    name = hex === '#36404a' ? '-dark' : name;

    return name;
}