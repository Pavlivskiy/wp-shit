<?php
/**
 * Plugin Name: Nanosteam Gutenberg Interface
 * Description: Nanosteam Gutenberg Interface — is a Gutenberg plugin created via create-guten-block.
 * Author: James Roy
 * Version: 1.0.0
 *
 * @package CGB
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Block Initializer.
 */
require_once plugin_dir_path( __FILE__ ) . 'src/init.php';
