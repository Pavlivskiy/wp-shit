<?php if ( ! defined( 'ABSPATH' ) ) {
	die( 'Direct access forbidden.' );
}

class Class_Nanosteam {

	function __construct() {
		self::constants();
		self::autoload();
		self::set_filters();
	}



	private static function autoload() {
		require dirname( __FILE__ ) . '/' . 'nanosteam_hooks.php';
		require_once dirname( __FILE__ ) . '/' . 'library/stripe_connect/stripe.php'; // Stripe Connect API
		require dirname( __FILE__ ) . '/' . 'library/tokens/tokens.php';
		require dirname( __FILE__ ) . '/' . 'library/email/nano_email.php';
		require dirname( __FILE__ ) . '/' . 'library/classes/class-nanosteam_metaboxes.php';
		require dirname( __FILE__ ) . '/' . 'library/classes/class-nanosteam_form.php';
		require dirname( __FILE__ ) . '/' . 'library/project_form/public/nanosteamProjectFields.php';
		require dirname( __FILE__ ) . '/' . 'library/matching_grant/nano_matching_grant.php';
		require dirname( __FILE__ ) . '/' . 'library/geo_locate/geo_locate.php';
		require_once dirname( __FILE__ ) . '/' . 'library/endorsements/endorsements.php'; //
		require_once dirname( __FILE__ ) . '/' . 'library/team_members/team_members.php'; //
		require_once dirname( __FILE__ ) . '/' . 'library/my_account/my_account.php'; //
		require_once dirname( __FILE__ ) . '/' . 'library/badges/badges.php'; //
		require_once dirname( __FILE__ ) . '/' . 'library/labnotes_faqs/labnotes_faqs.php'; //
		require_once dirname( __FILE__ ) . '/' . 'library/endorsements/endorsements.php'; //
		require_once dirname( __FILE__ ) . '/' . 'library/google_api/google_api.php'; //
		require_once dirname( __FILE__ ) . '/' . 'library/meta_data/admin_meta_data.php'; //  Admin Metadata
		require_once dirname( __FILE__ ) . '/' . 'library/meta_data/meta_tables.php'; //  Admin Metadata
		include_once dirname( __FILE__ ) . '/' . 'library/shared/class-deserializer.php'; // Admin Pages
		foreach ( glob( dirname( __FILE__ ) . '/' . 'library/admin/*.php' ) as $file ) {
			include_once $file;
		}
	}

	/**
	 * Constants
	 *
	 */
	private static function constants() {
		define( 'NANO_STRIPE', plugin_dir_path( __FILE__ ) );
		define( 'NANO_STRIPE_API_VERSION', '2018-05-21' );
	}

	private function set_filters() {
		add_action( 'wp_enqueue_scripts', array( $this, 'nano_scripts' ), '', 1 );
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_nano_scripts' ) );
		add_action( 'admin_init', 'admin_nano_fifty_metabox', 1 );
		add_action( 'init', 'nano_admin_settings', 10 );
		add_action( 'save_post', 'save_nano_meta', 10, 2 );
		add_action( 'init', 'save_multi_categories', 99 );
		add_filter( 'id_fes_form', array( 'Class_Nanosteam_Form_Container', 'nanosteam_form' ), 10, 2 );
		add_filter( 'the_goal', array( 'Class_Nanosteam_Form_Container', 'new_goal' ), 10, 3 );
		add_action( 'wp_ajax_ajax_register_team_members', 'ajax_register_team_members' ); // Registering Team Members
		add_action( 'wp_ajax_nopriv_ajax_register_team_members', 'ajax_register_team_members' ); // Registering Team Members
		add_action( 'wp_ajax_register_endorsers', 'register_endorsers' ); // Registering Endorsers
		add_action( 'wp_ajax_nopriv_register_endorsers', 'register_endorsers' ); // Registering Endorsers
		add_action( 'wp_ajax_duplicate_title_check_ajax', 'duplicate_title_check_ajax' ); // Duplicate title for project check
		add_action( 'wp_ajax_nopriv_duplicate_title_check_ajax', 'duplicate_title_check_ajax' ); // Duplicate title for project check
		add_action( 'wp_ajax_nano_stripe_connect_custom', 'nano_stripe_connect_custom' ); // Ajax Stripe Connect User Account
		add_action( 'wp_ajax_nopriv_nano_stripe_connect_custom', 'nano_stripe_connect_custom' ); // Ajax Stripe Connect User Account
		add_action( 'wp_ajax_nano_stripe_connect_update', 'nano_stripe_connect_update' ); // Ajax Stripe Connect User Updating
		add_action( 'wp_ajax_nopriv_nano_stripe_connect_update', 'nano_stripe_connect_update' ); // Ajax Stripe Connect User Updating
		add_action( 'wp_ajax_nano_stripe_creator_payment_ajax', 'nano_stripe_creator_payment_ajax' ); // Payout Function
		add_action( 'wp_ajax_nano_ajax_payout_overdue_recheck', 'nano_ajax_payout_overdue_recheck' ); // Overdue Recheck Function
		add_action( 'wp_ajax_geo_locate_by_city_state_ajax', 'geo_locate_by_city_state_ajax' ); // Ajax Autocomplete Cities
		add_action( 'wp_ajax_nopriv_geo_locate_by_city_state_ajax', 'geo_locate_by_city_state_ajax' ); // Ajax Autocomplete Cities
		add_action( 'wp_ajax_ajax_instant_checkout', 'ajax_instant_checkout' ); // Registering Team Members
		add_action( 'wp_ajax_nopriv_ajax_instant_checkout', 'ajax_instant_checkout' ); // Registering Team Members
		add_action( 'wp_ajax_nano_ajax_token_tracking', 'nano_ajax_token_tracking' ); // Sorting token data for admins
		add_action( 'wp_ajax_nopriv_nano_ajax_token_tracking', 'nano_ajax_token_tracking' ); // Sorting token data for admins
		add_action( 'wp_ajax_crop_image_ajax', 'crop_image_ajax' ); // Saving image data from project creation form
		add_action( 'wp_ajax_nopriv_crop_image_ajax', 'crop_image_ajax' ); // Saving image data from project creation form
		add_action( 'wp_ajax_ajax_nano_project_review', 'ajax_nano_project_review' );
		add_action( 'wp_ajax_ajax_purchase', 'ajax_purchase' );
		add_action( 'wp_ajax_nopriv_ajax_purchase', 'ajax_purchase' );
	}

	function admin_nano_scripts() {
		wp_register_script( 'nanoCredits_js', plugins_url( 'library/stripe_connect/js/nanoCredits.js', __FILE__ ) );
		wp_register_script( 'nanoReview_js', plugins_url( 'library/js/nanoReview.js', __FILE__ ) );
		wp_enqueue_script( 'nanoCredits_js', '', '', '099', true );
		wp_enqueue_script( 'nanoReview_js', '', '', '', true );
		if ( isset( $_GET['action'] ) && 'edit' == $_GET['action'] && isset( $_GET['post'] ) ) {

			$first_transfer     = get_post_meta( $_GET['post'], 'first_transfer_transaction_id', true ) != '' ? true : false;
			$final_transfer     = get_post_meta( $_GET['post'], 'final_transfer_transaction_id', true ) != '' ? true : false;
			$standard_transfer  = get_post_meta( $_GET['post'], 'standard_transfer_transaction_id', true ) != '' ? true : false;
			$transfer_status    = true == $first_transfer || true == $standard_transfer ? true : false;
			$default_project_id = function_exists( 'nano_get_default_project_id' ) ? nano_get_default_project_id() : '';
			$level_id           = function_exists( 'nano_get_level_id' ) ? nano_get_level_id( $default_project_id ) : '';

			wp_localize_script( 'nanoCredits_js', 'nano_transfer', array(
				'first_transfer_status' => $transfer_status,
				'final_transfer_status' => $final_transfer,
				'default_project_id'    => $level_id,
			) );

		}

		if ( isset( $_GET['page'] ) && ( 'token_tracking' == $_GET['page'] || 'project_tracking' == $_GET['page'] || 'pending_payouts' == $_GET['page'] ) ) {

			wp_enqueue_style( 'nano-daterangepicker', plugins_url( 'includes/bootstrap-daterangepicker/daterangepicker.css', __FILE__ ) );
			wp_register_script( 'nanoDatatables_js', plugins_url( 'library/js/datatables.min.js', __FILE__ ) );
			wp_register_script( 'nanoDatatablesButtons_js', plugins_url( 'library/js/buttons.bootstrap4.min.js', __FILE__ ) );
			wp_register_script( 'nanoDatatablesscript_js', plugins_url( 'library/js/nanoDatatables.js', __FILE__ ), '', nano_filetime( plugin_dir_path( 'library/js/nanoDatatables.js' ) ) );
			wp_register_script( 'nanoDateRangePicker_js', plugins_url( 'includes/bootstrap-daterangepicker/daterangepicker.js', __FILE__ ), '', '2.1.25', true );
			wp_register_script( 'nanoMoment_js', plugins_url( 'includes/bootstrap-daterangepicker/moment.js', __FILE__ ), '', '2.1.25', true );
			wp_register_script( 'nanoDateSelect_js', plugins_url( 'library/js/nanoDateSelect.js', __FILE__ ), '', nano_filetime( plugin_dir_path( 'library/js/nanoDateSelect.js' ) ), true );

			wp_enqueue_script( 'nanoMoment_js' );
			wp_enqueue_script( 'nanoDateRangePicker_js' );
			wp_enqueue_script( 'nanoDateSelect_js' );
			wp_enqueue_script( 'nanoDatatablesscript_js' );

		}

		if ( isset( $_GET['page'] ) && ( 'donations_paid' == $_GET['page'] || 'donations_received' == $_GET['page'] ) ) {
			wp_register_script( 'nanoDatatablesscript_js', plugins_url( 'library/js/nanoDatatables.js', __FILE__ ), '', nano_filetime( plugin_dir_path( 'library/js/nanoDatatables.js' ) ) );
			wp_enqueue_script( 'nanoDatatablesscript_js' );
		}

	}

	function nano_scripts() {

		$settings = get_option( 'memberdeck_gateways' );

		if ( ! empty( $settings ) ) {
			if ( is_array( $settings ) ) {
				$test = $settings['test'];
				if ( 1 == $test ) {
					$key = $settings['tpk'];
				} else {
					$key = $settings['pk'];
				}
			}
		}

		remove_shortcode( 'memberdeck_checkout', 'memberdeck_checkout' );
		remove_shortcode( 'idc_checkout', 'memberdeck_checkout' );
		add_shortcode( 'memberdeck_checkout', 'nano_memberdeck_checkout' );
		add_shortcode( 'idc_checkout', 'nano_memberdeck_checkout' );

		wp_register_script( 'nanosteamProjects_js', plugins_url( 'library/js/nanosteamProjectFields.js', __FILE__ ), '', filemtime( NANO_STRIPE . '/library/js/nanosteamProjectFields.js' ), true );
		wp_register_script( 'nanoStripeConnect_js', plugins_url( 'library/stripe_connect/js/nanoStripeConnect.js', __FILE__ ), 'nanoStripe', filemtime( NANO_STRIPE . '/library/stripe_connect/js/nanoStripeConnect.js' ), true );
		wp_register_script( 'nanoCredits_js', plugins_url( 'library/stripe_connect/js/nanoCredits.js', __FILE__ ), '', filemtime( NANO_STRIPE . '/library/stripe_connect/js/nanoCredits.js' ), true );
		// wp_register_script( 'nanoValidation_js', plugins_url( 'library/js/nanoProjectValidation.js', __FILE__ ), array( 'formvalidation-js', 'formvalidation-bootstrap-js', 'formvalidation-jquery-js' ), filemtime( NANO_STRIPE . '/library/js/nanoProjectValidation.js' ), true );
		wp_register_script( 'nanoDatatables_js', plugins_url( 'library/js/datatables.min.js', __FILE__ ) );
		wp_register_script( 'nanoDatatablesButtons_js', plugins_url( 'library/js/buttons.bootstrap4.min.js', __FILE__ ) );
		wp_register_script( 'nanoDatatablesscript_js', plugins_url( 'library/js/nanoDatatables.js', __FILE__ ), '', nano_filetime( plugin_dir_path( 'library/js/nanoDatatables.js' ) ) );
		wp_register_script( 'nanoCroppie_js', plugins_url( 'library/js/nanoCroppie.js', __FILE__ ), array( 'jquery', 'nanoCroppie' ) );
		wp_register_script( 'nanoCroppie', plugins_url( 'includes/croppie/croppie.min.js', __FILE__ ), array( 'jquery' ) );
		wp_register_script( 'nanoBudgetChart_js', plugins_url( 'library/js/Chart.min.js', __FILE__ ), array( 'jquery' ), '', true );
		wp_register_script( 'nanoBudget_js', plugins_url( 'library/js/nanoBudget.js', __FILE__ ), 'nanoBudgetSVG', filemtime( NANO_STRIPE . '/library/js/nanoBudget.js' ), true );
		wp_register_script( 'nanoDateRangePicker_js', plugins_url( 'includes/bootstrap-daterangepicker/daterangepicker.js', __FILE__ ), __FILE__, '2.1.25', true );
		wp_register_script( 'nanoMoment_js', plugins_url( 'includes/bootstrap-daterangepicker/moment.js', __FILE__ ), __FILE__, '2.1.25', true );
		wp_register_script( 'nanoDateSelect_js', plugins_url( 'library/js/nanoDateSelect.js', __FILE__ ), '', filemtime( NANO_STRIPE . '/library/js/nanoDateSelect.js' ), true );
		wp_register_script( 'nanoAutoComplete_js', plugins_url( 'library/js/jquery.auto-complete.min.js', __FILE__ ), 'jquery', '1.0.7', true );
		// Formvalidation.io Related

		wp_register_script( 'formvalidation-es-shim-js', 'https://cdnjs.cloudflare.com/ajax/libs/es6-shim/0.35.3/es6-shim.min.js', false, '0.35.3', false );
		wp_register_script( 'nano-formvalidation-js', plugins_url( 'includes/formvalidation/dist/js/FormValidation.min.js', __FILE__ ), 'formvalidation-es-shim-js', '1.1.0', true );
		wp_register_script( 'formvalidation-bootstrap-js', plugins_url( 'includes/formvalidation/dist/js/plugins/Bootstrap.min.js', __FILE__ ), 'nano-formvalidation-js', '1.1.0', true );
		wp_register_script( 'formvalidation-jquery-js', plugins_url( 'includes/formvalidation/dist/js/plugins/J.min.js', __FILE__ ), 'nano-formvalidation-js', '1.1.0', true );
		wp_register_script( 'formvalidation-profile-js', plugins_url( 'library/js/account-validation.js', __FILE__ ), array( 'nano-formvalidation-js', 'formvalidation-jquery-js' ), '', true );
		wp_register_script( 'nanoValidation_js', plugins_url( 'library/js/nanoProjectValidation.js', __FILE__ ), array( 'nano-formvalidation-js', 'formvalidation-bootstrap-js', 'formvalidation-jquery-js' ), '', true );

		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'nanosteamProjects_js' );
		wp_enqueue_script( 'nanoCredits_js' );

		// Budget Pie Chart
		wp_enqueue_script( 'nanoBudgetChart_js' );
		wp_enqueue_script( 'nanoBudget_js' );

		if ( isset( $_GET['edit_project'] ) || isset( $_GET['create_project'] ) || isset( $_GET['edit-profile'] ) || is_page('dashboard') ) {
			// Jquery Plugin for image cropping
			wp_enqueue_style( 'nano-croppie', plugins_url( 'includes/croppie/croppie.css', __FILE__ ) );
			wp_enqueue_script( 'nanoCroppie' );
			wp_enqueue_script( 'nanoCroppie_js' );
		}

		if ( isset( $_GET['create_project'] ) || ( isset( $_GET['edit_project'] ) && get_post( $_GET['edit_project'] )->post_status == 'draft' ) ) {
			wp_enqueue_script( 'nanoMoment_js' );
			wp_enqueue_script( 'nanoDateRangePicker_js' );
			wp_enqueue_script( 'nanoDateSelect_js' );
		}

		if ( isset( $_GET['edit_project'] ) || isset( $_GET['create_project'] ) || isset( $_GET['payment_settings'] ) ) {
			wp_enqueue_script( 'nanoStripe', 'https://js.stripe.com/v3/', false, '', true );
			wp_enqueue_script( 'nanoStripeConnect_js' );
		}

		if ( isset( $_GET['edit_project'] ) || isset( $_GET['create_project'] ) ) {
			wp_enqueue_script( 'nanoAutoComplete_js' );
			wp_enqueue_script( 'nanoValidation_js' );
			wp_enqueue_style( 'nano-autocomplete', plugins_url( 'library/styles/jquery.auto-complete.css', __FILE__ ) );
		}


		if ( isset( $_GET['create_project'] ) || isset( $_GET['edit_project'] ) || isset( $_GET['edit-profile'] ) || isset( $_GET['payment_settings'] ) || isset( $_GET['purchaseform'] ) || ! is_user_logged_in() || is_page('dashboard') ) {
			wp_enqueue_style( 'formvalidation', plugins_url( '/includes/formvalidation/dist/css/formValidation.min.css', __FILE__ ) );
			wp_enqueue_script( 'formvalidation-es-shim-js' );
			wp_enqueue_script( 'nano-formvalidation-js' );
			wp_enqueue_script( 'formvalidation-bootstrap-js' );
			wp_enqueue_script( 'formvalidation-jquery-js' );
			wp_enqueue_script( 'formvalidation-profile-js' );
		}

		wp_localize_script( 'nanosteamProjects_js', 'php_variables', array(
			'nano_grant_amt'  => get_option( 'nano_grant_amt', 1500 ),
			'pleaseWaitLabel' => __( 'Please wait...', 'default' ),
		) );

		// Possible fix for localize scripts issues with php 7
		wp_localize_script('idcommerce-js', 'memberdeck_ccode', '$');

		wp_localize_script( 'nanoValidation_js', 'team_member_object', array(
			'ajaxurl'        => admin_url( 'admin-ajax.php' ),
			'direct_url'     => plugin_dir_url( __FILE__ ) . 'library/geo_locate/geo_db_ajax_search.php',
			'dupe_url'       => plugin_dir_url( __FILE__ ) . 'library/project_form/ajax/duplicate_title_search.php',
			'post_status'    => ( isset( $_GET['edit_project'] ) ? get_post_status( $_GET['edit_project'] ) : 'draft' ),
			'loadingmessage' => __( 'Sending, please wait...' ),
		) );

		wp_localize_script( 'nanoStripeConnect_js', 'nano_stripe', array(
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
			'sc_pk'   => $key,
		) );

		$default_project_id = function_exists( 'nano_get_default_project_id' ) ? nano_get_default_project_id() : '';
		$level_id           = function_exists( 'nano_get_level_id' ) ? nano_get_level_id( $default_project_id ) : '';
		wp_localize_script( 'nanoCredits_js', 'nano_transfer', array(
			'default_project_id' => $level_id,
		) );

		wp_localize_script( 'nanoBudget_js', 'php_budget', nano_budget( get_the_ID() ) );
	}

}

$nano_fifty = new Class_Nanosteam();
