<div class="wrap">

	<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>

	<form method="post" action="<?php echo esc_html( admin_url( 'admin-post.php' ) ); ?>">

		<div id="nano-50-grant-limit">
			<h2>Matching Grant Limit</h2>

			<div class="options">
				<p>
					<label>Enter the maximum grant level which projects will be allowed to qualify</label>
					<br/>
					$ <input type="text" name="nano_grant_amt_val"
							value="<?php echo esc_attr( $this->deserializer->get_value( 'nano_grant_amt' ) ); ?>"/>
				</p>
			</div><!-- nano-50-grant-limit -->

			<h2>Popular Projects Settings</h2>

			<div class="options">
				<p>
					<label>Specify how many unique views a single backer counts for.</label>
					<br/>
					<input type="text" name="nano_backer_mult_val"
							value="<?php echo esc_attr( $this->deserializer->get_value( 'nano_backer_multiplier' ) ); ?>"
					/>
				</p>
			</div><!-- Popular settings weighting for single backers vs views -->

			<h2>Stripe account reserve amount</h2>

			<div class="options">
				<p>
					<label>Specify the minimum amount to be in the Platforms Stripe Account for Creator Payments to be allowed.</label>
					<br/>
					$<input type="text" name="nano_platform_stripe_amt_val"
							value="<?php echo esc_attr( $this->deserializer->get_value( 'nano_platform_stripe_amt' ) ); ?>"
					/>
				</p>
			</div><!-- Popular settings weighting for single backers vs views -->

			<h2>Recaptcha Settings</h2>

			<div class="options">
				<p>
					<label>You need to <a href="https://www.google.com/recaptcha/admin" rel="external">register here</a>
						for the keys to make this work.</label>
					<br/>
					<input type="text" name="captcha_site_key" id="captcha_site_key" placeholder="Site key"
							value="<?php echo esc_attr( $this->deserializer->get_value( 'captcha_site_key_val' ) ); ?>"
					/>
					<input type="text" name="captcha_secret_key" id="captcha_secret_key" placeholder="Secret key"
							value="<?php echo esc_attr( $this->deserializer->get_value( 'captcha_secret_key_val' ) ); ?>"
					/>
				</p>
			</div><!-- Recapthca Settings -->

			<h2>Maximum Donation Amount</h2>

			<div class="options">
				<p>
					<label>The maximum allowable donation amount for a project for a single donation</label>
					<br/>
					$ <input type="text" name="max_donation_amount" id="max_donation_amount" placeholder="10000"
							value="<?php echo esc_attr( $this->deserializer->get_value( 'max_donation_amount' ) ); ?>"
					/>
				</p>
			</div><!-- Donation Amount -->

			<h2>Credits Expiry</h2>

			<div class="options">
				<p>
					<label for="nano_token_expiry">Enter how many days credits should be available before expiring</label>
					<br/>
					<input type="text" name="nano_token_expiry" id="nano_token_expiry" placeholder="30"
							value="<?php echo esc_attr( $this->deserializer->get_value( 'nano_token_expiry' ) ); ?>"
					/>
				</p>
			</div><!-- Token Expiry Date -->

			<h2>Stripe Connect accounts for deposits and debits</h2>
			<p>Enter the two Stripe connected accounts for the depositing and debiting system for the creator payment
				flow</p>

			<div class="options">
				<p>
					<label for="nano_deposit_account">Stripe Connect Deposit Account</label>
					<br/>
					<input type="text" name="nano_deposit_account" id="nano_deposit_account"
							placeholder="acct_1BzWMYGPaLlQVE7h"
							value="<?php echo esc_attr( $this->deserializer->get_value( 'nano_deposit_account' ) ); ?>"
					/>
					<?php
					echo ( '' !== get_option('nano_deposit_account_message') ? '<br/><strong style="color: red">' . get_option('nano_deposit_account_message') . '</strong>' : '' );
					$deposit_account = admin_connect_account_update( $this->deserializer->get_value( 'nano_deposit_account' ), false );
					update_option( 'nano_deposit_account_message', $deposit_account );
					?>
				</p>
				<p>
					<label for="nano_debit_account">Stripe Connect Debit Account</label>
					<br/>
					<input type="text" name="nano_debit_account" id="nano_debit_account"
							placeholder="acct_1BzWMYGPaLlQVE7h"
							value="<?php echo esc_attr( $this->deserializer->get_value( 'nano_debit_account' ) ); ?>"
					/>
					<?php
					echo ( '' !== get_option('nano_debit_account_message') ? '<br/><strong style="color: red">' . get_option('nano_debit_account_message') . '</strong>' : '' );
					$debit_account = admin_connect_account_update( $this->deserializer->get_value( 'nano_debit_account' ), false );
					update_option( 'nano_debit_account_message', $debit_account );
					?>
				</p>
			</div><!-- Donation Amount -->

			<h2>Default project for payments</h2>

			<?php $var = $this->deserializer->get_value( 'default_project' ); ?>

			<div class="options">
				<p>
					<label>Any project selected here will be automatically attributed to the default project author (
						Username = nano_default_project_author ).
						<br/>Additionally the project's settings will be adjusted for use as the default payment
						project.
						<br/>While this project is selected it cannot be deleted.
						<br/>You can view the project <a href="<?php echo get_edit_post_link( $var ); ?>">here</a>
						<br/>
					</label>
					<br/>
					<?php

					$var           = $this->deserializer->get_value( 'default_project' );
					$default_posts = new Serializer;
					$default_posts = $default_posts->nano_select_default_post( $var );

					?>
				</p>
			</div><!-- nano-50-grant-limit -->

			<?php
			wp_nonce_field( 'nano-admin-settings-save', 'nano-admin-custom-message' );
			submit_button();
			?>

	</form>

</div><!-- .wrap -->
