<?php

/**
 * Performs all sanitization functions required to save the option values to
 * the database.
 *
 * This will also check the specified nonce and verfy that the current user has
 * permission to save the data.
 *
 */
class Serializer {

	/**
	 * Initializes the funtion by registering the save function with the
	 * admin_post hook so that we can save our options to the database.
	 */
	public function init() {
		add_action( 'admin_post', array( $this, 'save' ) );
	}

	/**
	 * Validates the incoming nonce value, verifies the current user has
	 * permission to save the value from the options page and saves the
	 * option to the database.
	 */
	public function save() {

		// First, validate the nonce and verify the user as permission to save.
		if ( ! ( $this->has_valid_nonce() && current_user_can( 'administrator' ) ) ) {
			// TODO: Display an error message.
		}

		// If the above are valid, sanitize and save the option.
		if ( isset( $_POST['nano_grant_amt_val'] ) && null !== wp_unslash( $_POST['nano_grant_amt_val'] ) ) {

			$value = sanitize_text_field( $_POST['nano_grant_amt_val'] );
			update_option( 'nano_grant_amt', $value );

		}

		if ( isset( $_POST['nano_backer_mult_val'] ) && null !== wp_unslash( $_POST['nano_backer_mult_val'] ) ) {

			$value = sanitize_text_field( $_POST['nano_backer_mult_val'] );
			update_option( 'nano_backer_multiplier', $value );

		}

		if ( isset( $_POST['nano_platform_stripe_amt_val'] ) && null !== wp_unslash( $_POST['nano_platform_stripe_amt_val'] ) ) {

			$value = sanitize_text_field( $_POST['nano_platform_stripe_amt_val'] );
			update_option( 'nano_platform_stripe_amt', $value );

		}

		if (  isset( $_POST['captcha_site_key'] ) && null !== wp_unslash( $_POST['captcha_site_key'] ) ) {

			$value = sanitize_text_field( $_POST['captcha_site_key'] );
			update_option( 'captcha_site_key_val', $value );

		}

		if ( isset( $_POST['captcha_secret_key'] ) && null !== wp_unslash( $_POST['captcha_secret_key'] ) ) {

			$value = sanitize_text_field( $_POST['captcha_secret_key'] );
			update_option( 'captcha_secret_key_val', $value );

		}

		if ( isset( $_POST['max_donation_amount'] ) && null !== wp_unslash( $_POST['max_donation_amount'] ) ) {

			$value = sanitize_text_field( $_POST['max_donation_amount'] );
			update_option( 'max_donation_amount', $value );

		}

		if ( isset( $_POST['nano_token_expiry'] ) && null !== wp_unslash( $_POST['nano_token_expiry'] ) ) {

			$value = sanitize_text_field( $_POST['nano_token_expiry'] );
			update_option( 'nano_token_expiry', $value );

		}

		if ( isset( $_POST['nano_deposit_account'] ) && null !== wp_unslash( $_POST['nano_deposit_account'] ) ) {

			$value                 = sanitize_text_field( $_POST['nano_deposit_account'] );
			$stripe_update_deposit = '';

			$old_val = get_option( 'nano_deposit_account' );

			if ( $value != $old_val ) {
				$stripe_update_deposit = admin_connect_account_update( $value, true );
			}

			if ( '' === $stripe_update_deposit ) {
				update_option( 'nano_deposit_account', $value );
				update_option( 'nano_deposit_account_message', '' );
			} else {
				update_option( 'nano_deposit_account_message', $stripe_update_deposit );
			}

		}

		if ( isset( $_POST['nano_debit_account'] ) && null !== wp_unslash( $_POST['nano_debit_account'] ) ) {

			$value               = sanitize_text_field( $_POST['nano_debit_account'] );
			$stripe_update_debit = '';

			$old_val = get_option( 'nano_debit_account' );
			if ( $value != $old_val ) {
				$stripe_update_debit = admin_connect_account_update( $value, true );
			}

			if ( '' === $stripe_update_debit ) {
				update_option( 'nano_debit_account', $value );
				update_option( 'nano_debit_account_message', '' );
			} else {
				update_option( 'nano_debit_account_message', $stripe_update_debit );
			}

		}

		if ( isset( $_POST['default_project'] ) && null !== wp_unslash( $_POST['default_project'] ) ) {

			$value = sanitize_text_field( $_POST['default_project'] );
			update_option( 'default_project', $value );

		}

		if ( isset( $_POST['nano_help_field'] ) && null !== wp_unslash( $_POST['nano_help_field'] ) ) {

			$value = nano_sanitize_array( 'nano_help_field' );
			update_option( 'nano_help_field', $value );

		}

		if ( isset( $_POST['nano_description_field'] ) && null !== wp_unslash( $_POST['nano_description_field'] ) ) {

			$value = nano_sanitize_array( 'nano_description_field' );
			update_option( 'nano_description_field', $value );

		}

		$this->project_default_setup();

		if ( ! isset( $_POST['unittest'] ) ) {
			$this->redirect();
		}

	}

	/**
	 * Determines if the nonce variable associated with the options page is set
	 * and is valid.
	 *
	 * @access private
	 *
	 * @return boolean False if the field isn't set or the nonce value is invalid;
	 *                 otherwise, true.
	 */
	private function has_valid_nonce() {

		// If the field isn't even in the $_POST, then it's invalid.
		if ( ! isset( $_POST['nano-admin-custom-message'] ) ) { // Input var okay.
			return false;
		}

		$field  = wp_unslash( $_POST['nano-admin-custom-message'] );
		$action = 'nano-admin-settings-save';

		return wp_verify_nonce( $field, $action );

	}

	/**
	 * Redirect to the page from which we came (which should always be the
	 * admin page. If the referred isn't set, then we redirect the user to
	 * the login page.
	 *
	 * @access private
	 */
	private function redirect() {

		// To make the Coding Standards happy, we have to initialize this.
		if ( ! isset( $_POST['_wp_http_referer'] ) ) { // Input var okay.
			$_POST['_wp_http_referer'] = wp_login_url();
		}

		// Sanitize the value of the $_POST collection for the Coding Standards.
		$url = sanitize_text_field(
			wp_unslash( $_POST['_wp_http_referer'] ) // Input var okay.
		);

		// Finally, redirect back to the admin page.
		wp_safe_redirect( urldecode( $url ) );
		exit;

	}


	/**
	 * Selection tool for the default project which is the default payment project to convert payments to tokens
	 *
	 * @param array $var
	 * @access public
	 */
	public function nano_select_default_post( $var ) {

		$args = array(
			'post_type'      => 'ignition_product',
			'post_status'    => array( 'private', 'pending' ),
			'posts_per_page' => - 1,
		);

		$query = new WP_Query( $args );

		$content = '<select name="default_project">';
		while ( $query->have_posts() ) {

			$query->the_post();
			$post_id = get_the_ID();
			$title   = get_the_title();

			$content .= '<option ' . ( $var == $post_id ? ' selected ' : '' ) . ' value="' . $post_id . '">' . $title . '</option>';
		}
		$content .= '</select>';

		echo $content;

		wp_reset_query();

	}


	/**
	 * Updating the project default payment post settings to the default nanosteam user
	 *
	 * @access private
	 */
	private function project_default_setup() {

		global $coauthors_plus;

		$this->nano_default_project_author();

		$default_project_post_id = get_option( 'default_project' );
		$default_author          = get_user_by( 'login', 'nano_default_project_author' );
		// Update Post to Default Author plus chnage its attributes
		$arg = array(
			'ID'          => $default_project_post_id,
			'post_author' => $default_author->ID,
			'post_status' => 'private',
		);

		wp_update_post( $arg );

		$old_fund_goal = get_post_meta( $default_project_post_id, 'ign_fund_goal', true );
		$old_end_type  = get_post_meta( $default_project_post_id, 'ign_end_type', true );
		$old_author    = get_post_meta( $default_project_post_id, 'original_author', true );
		$old_approved  = get_post_meta( $default_project_post_id, 'project_review_status_radio', true );

		update_post_meta( $default_project_post_id, 'ign_fund_goal', '10000000.00', $old_fund_goal );
		update_post_meta( $default_project_post_id, 'ign_end_type', 'open', $old_end_type );
		update_post_meta( $default_project_post_id, 'original_author', 'nano_default_project_author', $old_author );
		update_post_meta( $default_project_post_id, 'project_review_status_radio', 'approved', $old_approved );

		$project_id                  = get_post_meta( $default_project_post_id, 'ign_project_id', true );
		$nano_default_project_author = get_user_by( 'login', 'nano_default_project_author' );
		$user_id                     = $nano_default_project_author->ID;
		update_option( 'md_level_' . $project_id . '_owner', $user_id );

		$user = get_post_meta( $default_project_post_id, 'original_author', true );
		$coauthors_plus->add_coauthors( $default_project_post_id, array( $user ) );

	}

	private function nano_default_project_author() {

		$user_name  = 'nano_default_project_author';
		$user_email = 'default_user@nanosteam.org';
		$user_id    = username_exists( $user_name );

		if ( ! $user_id && email_exists( $user_email ) == false ) {
			$random_password = wp_generate_password( 16, false );
			$user_id         = wp_create_user( $user_name, $random_password, $user_email );

			// Update user to author
			$userdata = array(
				'ID'   => $user_id,
				'role' => 'author',
			);
			wp_update_user( $userdata );
		}

	}

}
