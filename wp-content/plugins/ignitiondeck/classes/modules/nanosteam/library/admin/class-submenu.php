<?php
/**
 * Creates the submenu item.
 *
 */

class Submenu {

	/**
	 * A reference the class responsible for rendering the submenu page.
	 *
	 * @var    Submenu_Page
	 * @access private
	 */
	private $submenu_page;

	/**
	 * Initializes all of the partial classes.
	 *
	 * @param Submenu_Page $submenu_page A reference to the class that renders the
	 * page for the plugin.
	 */
	public function __construct( $submenu_page ) {
		$this->submenu_page = $submenu_page;
	}

	/**
	 * Adds a submenu for this plugin to the 'Tools' menu.
	 */
	public function init() {
		if ( current_user_can( 'administrator' ) && ( wp_get_current_user()->ID == 3 || wp_get_current_user()->ID == 2 ) ) {
			add_action( 'admin_menu', array( $this, 'add_options_page' ) );
		}
	}

	/**
	 * Creates the submenu item and calls on the Submenu Page object to render
	 * the actual contents of the page.
	 */
	public function add_options_page() {

		add_submenu_page(
			'project_tracking',
			'Nanosteam Administration Options',
			'Options',
			'manage_options',
			'nanosteam-admin-options',
			array( $this->submenu_page, 'render' )
		);

		add_submenu_page(
			'project_tracking',
			'Project Creation Help and Description Fields',
			'Help and Description Fields',
			'manage_options',
			'nanosteam-admin-help-fields',
			array( $this->submenu_page, 'help_fields' )
		);
	}
}
