<div class="wrap">

	<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>

	<form method="post" action="<?php echo esc_html( admin_url( 'admin-post.php' ) ); ?>">

		<?php
		$help_fields       = get_option( 'nano_help_fields' );
		$saved_help_fields = get_option( 'nano_help_field' );

		if ( is_array( $help_fields ) ) {
			echo '<h3>Help Fields</h3>';
			asort( $help_fields );
			foreach ( $help_fields as $key => $help_field ) {
				$readable_title = ucfirst( str_replace( '_', ' ', $help_field ) );
				$field          = isset( $saved_help_fields[ $help_field ] ) ? $saved_help_fields[ $help_field ] : null;
					echo '<p>
					<label>' . $readable_title . '</label>
					<br/>
					<textarea rows="4" cols="50" name="nano_help_field[' . $help_field . ']"/>' . esc_attr( $field ) . '</textarea>
				</p>';
			}
		}

		$description_fields       = get_option( 'nano_description_fields' );
		$saved_description_fields = get_option( 'nano_description_field' );

		if ( is_array( $description_fields ) ) {
			echo '<h3>Description Fields</h3>';
			asort( $description_fields );
			foreach ( $description_fields as $key => $description_field ) {
				$readable_title_desc = ucfirst( str_replace( '_', ' ', $description_field ) );
				$field_desc          = isset( $saved_description_fields[ $description_field ] ) ? $saved_description_fields[ $description_field ] : null;
				echo '<p>
					<label>' . $readable_title_desc . '</label>
					<br/>
					<textarea rows="4" cols="50" name="nano_description_field[' . $description_field . ']"/>' . esc_attr( $field_desc ) . '</textarea>
				</p>';
			}
		}

		wp_nonce_field( 'nano-admin-settings-save', 'nano-admin-custom-message' );
		submit_button();
		?>

	</form>

</div><!-- .wrap -->
