<?php if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }


/**
 * Counting Labnotes
 *
 * @param $attrs
 *
 * @return int|string
*/

function count_lab_notes($attrs) {
	$args = array(
		'post_type'      => 'ignition_update',
		'posts_per_page' => -1,
		'post_status'    => array( 'publish, private' ),
		'meta_query'     => array(
			array(
				'key'     => 'idfu_project_update',
				'value'   => $attrs['product'],
				'compare' => '=',
			),
		),
	);

	$update_query = new WP_Query( $args );
	$value        = count( $update_query->posts );

	if ( 0 == $value ) {
		$value = '';
	} else {
		$value = $value . ' ';
	}

	return $value;

}

// Customize the layout for the updates tab on the project overview

function nano_distribute_updates($content, $attrs) {

    remove_filter( 'id_updates', 'distribute_updates', 3 );

    $args = array(
        'post_type'      => 'ignition_update',
        'posts_per_page' => -1,
        'post_status'    => array('publish, private'),
        'meta_query'     => array(
            array(
                'key'     => 'idfu_project_update',
                'value'   => $attrs['product'],
                'compare' => '='
            ),
        ),
    );


    $update_query = new WP_Query($args);

    if ( !empty($update_query->posts) ) {

        $content = '';
        foreach ($update_query->posts as $post) {

            $backer_public = get_post_meta($post->ID, 'nano_update_backer_'.$post->ID, true);
            $is_backer     = is_user_backer( $attrs, $backer_public, $post );
            $content      .= $is_backer;
        }

    }
    wp_reset_postdata();

    return $content;
}

add_filter('id_updates', 'nano_distribute_updates', 2, 2);


// Check if current user is a backer of the project and if not make the post private in labnotes archive.

function is_user_backer($attrs, $backer_public, $labnote) {

    // Get the current post information
    global $post;
    // Make all $wpdb references within this function refer to this variable
    global $wpdb;

    $content = '';

    $date_format = get_option('date_format');

    $content .= '<div class="col-sm-12"><div class="card"><div class="card-body">';

    if ($backer_public == 'backer') {

        $user = wp_get_current_user()->ID;

        if (class_exists('ID_Member_Order')) {

            // Backer Stats
            $misc = ' WHERE user_id = "' . $user . '"';
            $orders = ID_Member_Order::get_orders(null, null, $misc);

            if (!empty($orders)) {

                $mdid_orders = array();
                foreach ($orders as $order) {
                    $mdid_order = mdid_by_orderid($order->id);
                    if (!empty($mdid_order)) {
                        $mdid_orders[] = $mdid_order;
                    }
                }
                if (!empty($mdid_orders)) {
                    $id_orders = array();
                    foreach ($mdid_orders as $payment) {
                        $payment_id = $payment->pay_info_id + 1;
                        $order = new ID_Order($payment_id);
                        $the_order = $order->get_order();
                        if (!empty($the_order)) {
                            $id_orders[] = $the_order;
                        }
                    }
                    $id_orders = apply_filters('ide_backer_profile_projects', $id_orders, $user);
                    if (!empty($id_orders)) {
                        foreach ($id_orders as $id_order) {
                            $project = new ID_Project($id_order->product_id);
                            $the_project = $project->the_project();
                            $listed = array();
                            if (!empty($the_project) && !in_array($id_order->product_id, $listed)) {
                                $deck = new Deck($id_order->product_id);
                                $stats_backer['product_id'][] = $deck->project_id;
                            }
                        }
                        delete_user_meta($user, 'backed_projects');
                        add_user_meta($user, 'backed_projects', $stats_backer['product_id']);
                    }
                }
            }

            // Get current post's meta
            $project_id = $attrs['product'];

            // Current user's backed projects
            $user_backed_projects = get_user_meta($user, 'backed_projects', true) ? get_user_meta($user, 'backed_projects', true) : array();

            // The project's team members always see all posts
            $project_team = wp_get_post_terms($post->ID, 'author');

            $team_member_id = array();
            foreach ($project_team as $team_member) {
                $team_member_name = $team_member->name;
                $team_member_details = get_user_by('login', $team_member_name);
                $team_member_id[] = $team_member_details->ID;
            }

            if (!in_array($project_id, $user_backed_projects)) {
                if (!in_array($user, $team_member_id)) {
                    $content .= '<h6 class="title text-uppercase heading-normal">'.get_the_title($labnote).' <small>(Backers Only)</small></h6>';
                }
            }

            if (in_array($user, $team_member_id) || in_array($project_id, $user_backed_projects)) {
                $content .= '<a href="'. get_the_permalink($labnote).'" class="update-link" alt="'.get_the_title($labnote).'" title="'.get_the_title($labnote).'">';
                $content .= '<h6 class="title text-uppercase heading-normal">'.get_the_title($labnote).'</h6>';
                $content .= '</a>';
            }

        }

    } else {
        $content .= '<a href="'. get_the_permalink($labnote).'" class="update-link" alt="'.get_the_title($labnote).'" title="'.get_the_title($labnote).'">';
        $content .= '<h6 class="title text-uppercase heading-normal">'.get_the_title($labnote).'</h6>';
        $content .= '</a>';
    }

    $wpdb->query($wpdb->prepare("UPDATE $wpdb->posts SET post_status = 'publish' WHERE ID = %s", $labnote->ID));

    $author_name = get_user_by('id', $labnote->post_author);
    $author_name = $author_name->data->display_name;

    $content .= '<p class="card-text small mb-2">'. wp_trim_words( $labnote->post_content, $num_words = 10, $more = null ).'</p>';
    $content .= '<p class="small muted">' . (date($date_format, strtotime($labnote->post_date))) . ' ' . ($labnote->post_date !== $labnote->post_modified ? ' (' . __('Edited: ', 'idfu') . date($date_format, strtotime($labnote->post_modified)) . ')' : '');
    $content .= ' ' . ($labnote->comment_count > 0 ? '<i class="fa fa-comment"></i> ' .$labnote->comment_count .' ' : '') . '<i class="fa fa-user"></i> <a href="'. get_author_posts_url( $labnote->post_author ) .'">'.$author_name .'</a></p>' ;
    $content .= '</div></div></div>';

    return $content;

}

// General Check to see if the user is allowed to view the post in single.php

function is_user_privleged($post) {

    $user = wp_get_current_user()->ID;

    $backer_public = get_post_meta($post->ID, 'nano_update_backer_' .$post->ID. '', true);

    if ($backer_public == 'backer') {

        if (class_exists('ID_Member_Order')) {

            // Backer Stats
            $misc = ' WHERE user_id = "' . $user . '"';
            $orders = ID_Member_Order::get_orders(null, null, $misc);

            if (!empty($orders)) {

                $mdid_orders = array();
                foreach ($orders as $order) {
                    $mdid_order = mdid_by_orderid($order->id);
                    if (!empty($mdid_order)) {
                        $mdid_orders[] = $mdid_order;
                    }
                }
                if (!empty($mdid_orders)) {
                    $id_orders = array();
                    foreach ($mdid_orders as $payment) {
                        $payment_id = $payment->pay_info_id + 1;
                        $order = new ID_Order($payment_id);
                        $the_order = $order->get_order();
                        if (!empty($the_order)) {
                            $id_orders[] = $the_order;
                        }
                    }
                    $id_orders = apply_filters('ide_backer_profile_projects', $id_orders, $user);
                    if (!empty($id_orders)) {
                        $stats_backer = array();
                        foreach ($id_orders as $id_order) {

                            $project = new ID_Project($id_order->product_id);
                            $the_project = $project->the_project();
                            $listed = array();
                            if (!empty($the_project) && !in_array($id_order->product_id, $listed)) {
                                $deck = new Deck($id_order->product_id);
                                $stats_backer['product_id'][] = $deck->project_id;
                            }
                        }
                        delete_user_meta($user, 'backed_projects');
                        add_user_meta($user, 'backed_projects', $stats_backer['product_id']);
                    }
                }
            }

            // Query current post's meta
            $project_id = get_post_meta($post->ID, 'idfu_project_update', true);
            $post_id = idf_get_object('id_project-get_project_postid-'.$project_id);

            // Current user's backed projects
            $user_backed_projects = get_user_meta($user, 'backed_projects', true) ? get_user_meta($user, 'backed_projects', true) : array();

            // The project's team members always see all posts
            $project_team = wp_get_post_terms($post_id, 'author');

            $team_member_id = array();
            foreach ($project_team as $team_member) {
                $team_member_name = $team_member->name;
                $team_member_details = get_user_by('login', $team_member_name);
                $team_member_id[] = $team_member_details->ID;
            }

            if (!in_array($project_id, $user_backed_projects)) {
                if (!in_array($user, $team_member_id)) {

                    return false;

                } else {

                    return true;
                }
            }

            if (in_array($user, $team_member_id) || in_array($project_id, $user_backed_projects)) {

                return true;

            }

        }

    } else {

        return true;

    }

}


function nano_swap_fes_form() {

    // Clear out the IDK default functions

    remove_action('init', 'swap_fes_form');
    remove_action('ide_fes_submit', 'idfu_fes_submit', 10);
    remove_action('ide_fes_submit', 'idfu_fes_update', 10);
    remove_action('fes_new_update_content_after', 'fes_list_updates');

    add_filter('fes_updates_form', function() {
        $subject = array(
            'label' => apply_filters('idfu_update_subject', __('Update Subject', 'idfu')),
            'value' => '',
            'name' => 'new_update_subject',
            'id' => 'new_update_subject',
            'type' => 'text',
            'class' => '',
            'wclass' => 'col-sm',
            'misc' => 'placeholder="Subject"',
            'heading' => apply_filters('idfu_project_updates', __('Lab Notes', 'idfu')),
            'help' => nano_help_field( 'new_update_subject' ),
            'before' => '<div class="form-group form-row">',
            'after' => '</div>'
        );
        return $subject;
    });
    add_filter('fes_updates_after', function() {
        $content = array(
            'label' => apply_filters('idfu_update_content', __('Update Lab Note', 'idfu')),
            'value' => '',
            'name' => 'new_update_content',
            'id' => 'new_update_content',
            'type' => 'wpeditor',
            'wclass' => 'col wpeditor',
            'misc' => 'placeholder="Subject"',
            'before' => '<div class="form-group form-row pb-3">',
            'after' => '</div>'
        );
        return $content;
    });

    add_action('fes_new_update_content_after', 'nano_fes_list_updates');
    add_action('ide_fes_submit', 'nano_idfu_fes_submit', 10, 3);
    add_action('ide_fes_submit', 'nano_idfu_fes_update', 10, 3);
}

add_action('init', 'nano_swap_fes_form', 9);

function nano_idfu_fes_submit($post_id, $project_id, $vars) {
    if (isset($_POST['new_update_subject'])) {
        $subject = stripslashes(strip_tags($_POST['new_update_subject']));
    }
    if (isset($_POST['new_update_content'])) {
        $content = wp_kses_post(balanceTags($_POST['new_update_content']));
    }
    if (!empty($subject) && !empty($content)) {
        // we can post an update
        if (is_multisite()) {
            require(ABSPATH . WPINC . '/pluggable.php');
        }
        $current_user = wp_get_current_user();
        $user_id = get_post_field( 'post_author', $post_id );
        $user_projects = get_user_meta($user_id, 'ide_user_projects', true);
        if (!empty($user_projects)) {
            if (!is_array($user_projects)) {
                $user_projects = unserialize($user_projects);
            }
            if (in_array($post_id, $user_projects)) {
                $args = array(
                    'post_title' => $subject,
                    'post_content' => $content,
                    'post_status' => 'publish',
                    'post_type' => 'ignition_update',
                    'post_author' => $current_user->ID
                );
                $post = wp_insert_post($args);
                if (!empty($post)) {
                    update_post_meta($post, 'idfu_project_update', $project_id);
                    $backer_public = sanitize_text_field($_POST['nano_public_backer']);
                    if ( ! add_post_meta( $post, 'nano_update_backer_'.$post.'', $backer_public, true ) ) {
                        update_post_meta( $post, 'nano_update_backer_'.$post.'', $backer_public );
                    }
                }
                //do_action('idfu_update_create', $post, $project_id);
                do_action('idfu_update_save', $post, $project_id);

                // Send emails to project backers on labnote update (only if backer has indicated they wish to receive emails)
                backer_project_update_notify($post_id, $project_id);
                nano_update_project_status_meta_data( $post_id );
            }
        }
    }
    return;
}

function nano_idfu_fes_update($post_id, $project_id, $vars) {
    if (is_multisite()) {
        require(ABSPATH . WPINC . '/pluggable.php');
    }
    $current_user = wp_get_current_user();
    $user_id = get_post_field( 'post_author', $post_id );
    $user_projects = get_user_meta($user_id, 'ide_user_projects', true);
    if (!empty($user_projects)) {
        if (!is_array($user_projects)) {
            $user_projects = unserialize($user_projects);
        }
        if (in_array($post_id, $user_projects)) {

            $post_array = array();
            foreach ($_POST as $k => $v) {
                if (strpos($k, 'idfu_update_') !== false) {
                    $post_id = str_replace('idfu_update_', '', $k);
                    $backer_original = get_post_meta($post_id, 'nano_update_backer_'.$post_id, true);
                    $key_name = 'nano_update_backer_' . (string) $post_id;
                    $backer_update = sanitize_text_field($_POST[$key_name]);

                    if ($post_id > 0) {
                        $post = array();
                        $post['id'] = $post_id;
                        $post['content'] = wp_kses_post($v);
                        $old_post = get_post($post_id);
                        $post_content = $old_post->post_content;
                        if (($post_content !== $post['content']) || (apply_filters('idfu_update_content_changed', false, $old_post->ID) || ($backer_update != $backer_original))) {
                            $post_array[] = $post;
                        }
                    }
                }
            }
            if (!empty($post_array)) {
                foreach ($post_array as $post) {
                    $args = array(
                        'ID' => $post['id'],
                        'post_content' => $post['content']
                    );
                    wp_update_post($args);
                    //error_log(print_r($post, true));
                    $backer_public = sanitize_text_field($_POST['nano_update_backer_'.$post['id'].'']);
                    update_post_meta( $post['id'], 'nano_update_backer_'.$post['id'].'', $backer_public );

                    do_action('idfu_update_edit', $post['id'], $project_id);
                    do_action('idfu_update_save', $post['id'], $project_id);

                    // Send emails to project backers on labnote update (only if backer has indicated they wish to receive emails)
                    backer_project_update_notify($post['id'], $project_id);
                }
            }
        }
    }
}

function nano_fes_list_updates($post_id) {
    $updates = nano_idfu_get_updates($post_id);
    $date_format = get_option('date_format');
    if (!empty($updates)) {
        echo '<h4>'.apply_filters('idfu_previous_updates', __('Previous Lab Notes', 'idfu')).'</h4>';
        ?>
        <div class="form-row idfu_prevupdates">
        <div id="accordion" role="tablist" class="col">
        <?php
        foreach ($updates as $update) {
            $backer_public = get_post_meta($update->ID, 'nano_update_backer_' . $update->ID, true);
            $public = $backer_public == 'public' || $backer_public == '' ? 'checked="checked"' : '';
            $backer = $backer_public == 'backer' ? 'checked="checked"' : '';

            ?>
            <div class="card bg-light mb-3">
                <div class="card-header m-0 p-0" role="tab" id="heading<?php echo $update->ID ?>">
                    <p class="m-0">
                        <a class="btn btn-light btn-block p-4 text-left" data-toggle="collapse" href="#<?php echo $update->ID ?>"
                           aria-expanded="true" aria-controls="<?php echo $update->ID ?>">
                            <?php echo $update->post_title; ?>
                            <?php echo '<span class="small update_posted"> (<em>' . __('Posted on: ', 'idfu') . date($date_format, strtotime($update->post_date)) . '</em>' . ($update->post_date !== $update->post_modified ? '<em>' . __(' | Edited: ', 'idfu') . date($date_format, strtotime($update->post_modified)) : '') . '</em>)</span>'; ?>
                        </a>
                    </p>
                </div>
                <div id="<?php echo $update->ID ?>" class="collapse" role="tabpanel"
                     aria-labelledby="heading<?php echo $update->ID ?>" data-parent="#accordion">
                    <div class="card-body pb-0 border-top">
                        <?php
                        do_action( 'idfu_edit_update_before', $post_id, $update );
                        wp_editor( $update->post_content, 'idfu_update_' . $update->ID );
                        ?>

                    </div>
                    <div class="card-footer bg-light">
                        <?php
                        echo '<div class="custom-control custom-radio custom-control-inline"><input ' . $backer . ' type="radio" id="new_update_backer_' . $update->ID . '" name="nano_update_backer_' . $update->ID . '" class="custom-control-input new_update_backer" value="backer"><label class="custom-control-label" for="new_update_backer_' . $update->ID . '">Visible to Backers Only</label></div>';
                        echo '<div class="custom-control custom-radio custom-control-inline"><input ' . $public . ' type="radio" id="new_update_public_' . $update->ID . '" name="nano_update_backer_' . $update->ID . '" class="custom-control-input new_update_backer" value="public"><label class="custom-control-label" for="new_update_public_' . $update->ID . '">Visible to Public</label></div>';
                        do_action('idfu_edit_update_after', $post_id, $update);
                        ?>
                    </div>
                </div>
            </div>
            <?php
        }
        echo '</div></div>';
    }
}

function nano_idfu_get_updates($post_id) {
    $project_id = get_post_meta($post_id, 'ign_project_id', true);
    $args = array(
        'post_type' => 'ignition_update',
        'meta_key' => 'idfu_project_update',
        'posts_per_page' => -1,
        'meta_value' => $project_id
    );
    $posts = get_posts($args);
    return $posts;
}