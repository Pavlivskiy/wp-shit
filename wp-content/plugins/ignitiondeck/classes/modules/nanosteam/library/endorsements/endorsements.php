<?php if ( ! defined( 'ABSPATH' ) ) {
	die( 'Direct access forbidden.' );
}

/**
 * Ajax function to register and invite endorsers
 */

function register_endorsers() {

	// First check the nonce, if it fails the function will break
	check_ajax_referer( 'nano_endorsement_secure', 'security' );

	global $coauthors_plus;

	$post_id = isset( $_POST['id'] ) && '' != $_POST['id'] ? $_POST['id'] : '';

	$user_profile_page = strtolower( wp_generate_password( 16, false ) );

	// Nonce is checked, get the POST data and sign user on
	$info                  = array();
	$info['user_nicename'] = generate_unique_user_nicename( $user_profile_page );
	$info['user_pass']     = idmember_pw_gen();
	$info['user_login']    = sanitize_email( $_POST['email'] );
	$info['user_email']    = sanitize_email( $_POST['email'] );

	$prev_members   = maybe_unserialize( get_post_meta( $post_id, 'nano_endorsement_members', true ) );
	$prev_members   = is_array( $prev_members ) ? $prev_members : array( $prev_members );
	$new_member     = array( $info['user_email'] );
	$existing_email = count( array_diff( $new_member, $prev_members ) );

	// Checking whether the project has already finished it's crowdfunding round and if so deny adding new endorsers.
	$post_status = strtoupper( get_post_status( $post_id ) ) == 'PUBLISH' ? true : false;
	if ( true === $post_status ) {
		echo json_encode(
			array(
				'loggedin' => false,
				'message'  => __( 'Sorry, you can no longer request endorsements' ),
			)
		);

		if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
			wp_die();
		}
	}

	// Check whether the user has already been invited
	if ( 0 === $existing_email ) {

		echo json_encode(
			array(
				'loggedin' => false,
				'message'  => __( 'Sorry, this person has already been invited' ),
			)
		);

		if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
			wp_die();
		}
	}

	$nano_team_members = array_filter( array_merge( $prev_members, $new_member ) );

	// Register the user
	$user_register = wp_insert_user( $info );

	if ( is_wp_error( $user_register ) ) {

		$error = $user_register->get_error_codes();

		if ( in_array( 'empty_user_login', $error ) ) {
			echo json_encode( array(
				'loggedin' => false,
				'message'  => $user_register->get_error_message( 'empty_user_login' ),
			) );
		} elseif ( in_array( 'existing_user_login', $error ) ) {
			echo json_encode(
				array(
					'loggedin' => false,
					'message'  => __( 'Successfully Sent' ),
				)
			);
		} elseif ( in_array( 'existing_user_email', $error ) ) {
			echo json_encode(
				array(
					'loggedin' => false,
					'message'  => __( 'Successfully Sent' ),
				)
			);
		}

		// If the project is already created then lets see if we can email the new endorsers.
		if ( '' != $post_id ) {
			$update_teammember = update_post_meta( $post_id, 'nano_endorsement_members', $nano_team_members );
			if ( true == $update_teammember ) {
				$team_members = get_post_meta( $post_id, 'nano_endorsement_members', true );
				foreach ( $team_members as $team_member ) {
					$user            = get_user_by( 'email', $team_member );
					$check_if_mailed = null !== get_user_meta( $user->ID, 'project_endorsement_connection', true ) ? get_user_meta( $user->ID, 'project_endorsement_connection', true ) : null;
					$check_if_mailed = is_array( $check_if_mailed ) ? $check_if_mailed : array();
					if ( ! in_array( $post_id, $check_if_mailed ) ) {
						$post      = get_post();
						$recipient = $user->user_email;
						nano_email_notify( $post_id, $recipient, 'nano_endorsement_member' );
						$project_ids = $check_if_mailed;
						array_push( $project_ids, $post_id );
						update_user_meta( $user->ID, 'project_endorsement_connection', $project_ids );
					}
				}
			}
		}
	} else {
		echo json_encode(
			array(
				'loggedin' => false,
				'message'  => __( 'Successfully Sent' ),
			)
		);
		$user    = get_user_by( 'email', $info['user_email'] );
		$user_id = $user->ID;
		idc_welcome_email( $user_id, $info['user_email'] );
		wp_new_user_notification( $user_id, null, 'both' );
		assign_avatar_init( $user_id );
		if ( '' != $post_id ) {
			$update_teammember = update_post_meta( $post_id, 'nano_endorsement_members', $nano_team_members );
			if ( true == $update_teammember ) {
				$team_members = get_post_meta( $post_id, 'nano_endorsement_members', true );
				foreach ( $team_members as $team_member ) {
					$user            = get_user_by( 'email', $team_member );
					$check_if_mailed = is_array( get_user_meta( $user->ID, 'project_endorsement_connection', true ) ) ? get_user_meta( $user->ID, 'project_endorsement_connection', true ) : array();
					if ( ! in_array( $post_id, $check_if_mailed ) ) {
						$recipient = $user->user_email;
						nano_email_notify( $post_id, $recipient, 'nano_endorsement_member' );
						$project_ids = $check_if_mailed;
						array_push( $project_ids, $post_id );
						update_user_meta( $user->ID, 'project_endorsement_connection', $project_ids );
					}
				}
			}
		}
	}

	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		wp_die();
	}

}

/**
 * Display endorsements in the about me section for the potential endorser
 */
function endorsements_backend() {
	$user       = wp_get_current_user();
	$user_login = isset( $user->data->user_login ) ? $user->data->user_login : null;
	$post_ids   = get_user_meta( $user->ID, 'project_endorsement_connection', true );
	// $user       = get_user_by( 'id', $user->ID );
	// $name       = apply_filters( 'ide_profile_name', $user->display_name, $user );

	$form_done    = '';
	$form_pending = '';
	$modal_top    = '';
	$modal_bottom = '';

	if ( '' != $post_ids ) {

		$modal_top  = '<div class="modal fade" id="endorse_modal" tabindex="-1" role="dialog" aria-labelledby="endorse_modal_label" aria-hidden="true">';
		$modal_top .= '<div class="modal-dialog modal-center" role="document">';
		$modal_top .= '<div class="modal-content">';
		$modal_top .= '<div class="modal-header">';
		//$modal_top .= '<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>';
		$modal_top .= '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
		$modal_top .= '</div>';
		$modal_top .= '<div class="modal-body">';

		$form_done    .= '<div class="row"><div class="col-sm-12">';
		$form_pending .= '<div class="row"><div class="col-sm-12">';

		$counter = 0;

		$post_ids = array_reverse( $post_ids );

		foreach ( $post_ids as $post_id ) {

			$post_status = strtoupper( get_post_status( $post_id ) ) == 'PUBLISH' ? true : false;

			if ( get_post_status( $post_id ) != false && false == $post_status ) {

				// Saving the endorsement
				if ( isset( $_POST[ 'edit_endorsement_' . $post_id ] ) ) {

					if ( isset( $_POST['nano_endorsements'] ) ) {

						$sanitized_val                               = array();
						$sanitized_val['endorsement'][ $user_login ] = sanitize_text_field( $_POST['nano_endorsements'][ $counter ] );
						// Check if there are other endorsements for the project
						$org_endorsements = get_post_meta( $post_id, 'nano_endorsements', true );
						//Check to see if the current endorser has already posted an endorsement and if so delete it
						$new_endorsements = '';
						$endorsement      = false;

						if ( '' != $sanitized_val['endorsement'][ $user_login ] ) {
							if ( isset( $org_endorsements['endorsement'][ $user_login ] ) ) {
								unset( $org_endorsements['endorsement'][ $user_login ] );
							}
							$org_endorsements = is_array( $org_endorsements ) ? $org_endorsements : array();
							$new_endorsements = array_merge_recursive( $org_endorsements, $sanitized_val );
						}

						// Update the project with the new endorsement.
						if ( '' != $new_endorsements ) {
							$endorsement = update_post_meta( $post_id, 'nano_endorsements', $new_endorsements );
						}
						// If the endorsement is saved let the project leader know.
						if ( true == $endorsement ) {
							$original_author = get_post_meta( $post_id, 'original_author', true );
							$recipient = false !== get_user_by( 'login', $original_author ) ? get_user_by( 'login', $original_author ) : get_user_by( 'email', $original_author );
							$recipient = $recipient->user_email;
							nano_email_notify( $post_id, $recipient, 'nano_endorsement_notification' );
						}
					}
				}

				$title        = get_the_title( $post_id );
				$endorsements = get_post_meta( $post_id, 'nano_endorsements', true );

				// The endorsement

				$form_both = '<a class="btn btn-sm btn-outline-primary mt-2 px-3 float-right" href="' . home_url() . '/?post_type=ignition_product&p=' . $post_id . '&preview=1">' . __( 'View Project', 'memberdeck' ) . '</a>';
				//$form_both .= '<button type="button" class="close btn btn-sm btn-secondary px-3 mt-2 mr-2 float-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';

				if ( '' != $endorsements && isset( $endorsements['endorsement'][ $user_login ] ) && '' != $endorsements['endorsement'][ $user_login ] ) {
					$form_done .= '<div class="clearfix">';
					$form_done .= '<label for="description"><strong>' . $title . '</strong> ' . ( false == $post_status ? __( 'endorsement has been saved', 'memberdeck' ) : __( 'is now published and this can no longer be updated', 'memberdeck' ) ) . '</label>';
					$form_done .= '<textarea rows="6" class="form-control px-3 js-auto-size" name="nano_endorsements[]" ' . ( false == $post_status ? '' : 'readonly' ) . '>' . $endorsements['endorsement'][ $user_login ] . '</textarea>';
					$form_done .= false == $post_status ? '<button type="submit" id="edit-endorsement-' . $post_id . '" class="mt-2 ml-2 px-3 submit-button btn btn-sm btn-primary float-right" name="edit_endorsement_' . $post_id . '">Update Endorsement</button>' : '';
					$form_done .= $form_both;
					$form_done .= '</div>';
					$form_done .= '<hr>';
				} else {
					$form_done .= '<div class="clearfix">';
					$form_done .= '<label for="nano_endorsements">' . __( 'Please fill out the endorsement for', 'memberdeck' ) . ' <strong>' . $title . '</strong></label>';
					$form_done .= '<textarea rows="6" class="form-control px-3 js-auto-size" name="nano_endorsements[]"></textarea>';
					$form_done .= '<button type="submit" id="edit-endorsement-' . $post_id . '" class="mt-2 ml-2 px-3 submit-button btn btn-sm btn-primary float-right" name="edit_endorsement_' . $post_id . '">Submit Endorsement</button>';
					$form_done .= $form_both;
					$form_done .= '</div>';
					$form_done .= '<hr>';
				}

				$counter ++;
			}
		}

		$form_done    .= '</div></div>';
		$form_pending .= '</div></div>';

		$modal_bottom = '</div>';
		//$modal_bottom .= '<div class="modal-footer">';
		//$modal_bottom .= '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>';
		//$modal_bottom .= '<button type="button" class="btn btn-primary">Save changes</button>';
		//$modal_bottom .= '</div>';
		$modal_bottom .= '</div>';
		$modal_bottom .= '</div>';
		$modal_bottom .= '</div>';
	}

	echo $modal_top . $form_done . $modal_bottom;
}

/**
 * Display endorsements on the project
 *
 * @param $post_id
 */

function endorsements_frontend( $post_id ) {

	$endorsements = get_post_meta( $post_id, 'nano_endorsements', true );

	if ( '' != $endorsements ) {

		$content = '<h5 class="pt-5">' . __( 'Endorsements', 'nanosteam_8n' ) . '</h5>';
		$content .= '<div class="pl-sm-4">';

		foreach ( $endorsements['endorsement'] as $key => $endorsement ) {

			$user = get_user_by( 'login', $key );
			// $user_meta            = get_user_meta( $user->ID );
			$user_data = get_userdata( $user->ID );
			// $default_img_location = get_stylesheet_directory_uri() . '/images/krown-gravatar.png';

			//Author
			$nano_avatar     = get_user_meta( $user->ID, 'nano_idc_avatar', true );
			$nano_avatar_img = isset( $nano_avatar[0] ) && is_array( $nano_avatar ) ? $nano_avatar[0] : $nano_avatar;
			$avatar          = '<a class="align-self-top mr-2" href="' . get_author_posts_url( $user->ID ) . '"><img class="rounded-circle" src="' . $nano_avatar_img . '"></a>';

			$endorser_name  = $user_data->display_name;
			$endorser_title = get_user_meta( $user->ID, 'nano_user_title', true );

			$content .= '<div class="media pb-3 ">';
			$content .= $avatar;
			$content .= '<div class="media-body">';
			$content .= '<em>' . wpautop( $endorsement ) . '</em>';
			$content .= '<a href="' . get_author_posts_url( $user->ID ) . '">';
			$content .= '<strong class="m-0 pb-0 d-block">' . $endorser_name . '</strong>';
			$content .= '<small class="text-muted">' . $endorser_title . '</small>';
			$content .= '</a></div></div>';

		}
		$content .= '</div>';

		echo $content;

	}

}

/**
 * Display endorsements for the endorser to review
 *
 * @return array
 */

function endorsements_user_profile() {

	$current_user = wp_get_current_user();
	$user         = get_userdata( get_query_var( 'author' ) );
	$user_login   = $user->user_login;
	$user_id      = $user->ID;

	$post_ids = get_user_meta( $user_id, 'project_endorsement_connection', true );

	$value = null;
	if ( '' != $post_ids && is_array( $post_ids ) ) {

		$content = '';
		$counter = 0;

		$value = array();

		foreach ( $post_ids as $post_id ) {

			if ( get_post_status( $post_id ) != false ) {

				if ( strtoupper( get_post_status( $post_id ) ) == 'PUBLISH' || $current_user->ID == $user_id ) {

					$endorsements = get_post_meta( $post_id, 'nano_endorsements', true );

					if ( isset( $endorsements['endorsement'][ $user_login ] ) && '' != $endorsements['endorsement'][ $user_login ] ) {

						$endorsement = wpautop( $endorsements['endorsement'][ $user_login ] );

						$post_title  = '<a href="' . get_the_permalink( $post_id ) . '" class="update-link" alt="' . get_the_title( $post_id ) . '" title="' . get_the_title( $post_id ) . '">';
						$post_title .= '<h6 class="title text-uppercase heading-normal">' . get_the_title( $post_id ) . '</h6>';
						$post_title .= '</a>';
						$card_link   = '<a class="card-link" href="' . get_permalink( $post_id ) . '"></a>';

						$content .= '<div class="col-sm-12">';
						$content .= '<div class="card">';
						$content .= $card_link;
						$content .= '<div class="card-body">';
						$content .= $post_title;
						$content .= '<div class="card-text small mb-2">' . $endorsement . '</div>';
						$content .= '</div></div></div>';

						$counter ++;

					}
				}
			}
		}

		$value['total']   = $counter;
		$value['content'] = $content;

	}

	return $value;

}


/**
 * Send out email reminders to endorsers who have not filled out an endorsement
 */
function endorsement_reminder() {

	$args = array(
		'post_type'      => 'ignition_product',
		'post_status'    => 'pending',
		'posts_per_page' => - 1,

	);

	$query = new WP_Query( $args );

	if ( $query->have_posts() ) {

		while ( $query->have_posts() ) {
			$query->the_post();

			$post_id = get_the_id();

			$endorsements = get_post_meta( $post_id, 'nano_endorsements', true );
			$endorsers    = get_post_meta( $post_id, 'nano_endorsement_members', true );
			// $sumbit_date  = get_the_date( '', $post_id );

			if ( is_array( $endorsers ) ) {
				$endorsers = array_unique( $endorsers );

				foreach ( $endorsers as $endorser ) {

					if ( ! isset( $endorsements['endorsement'][ $endorser ] ) ) {

						$today       = nano_get_formated_date_time_gmt( get_option( 'date_format' ) );
						$sumbit_date = get_the_date( '', $post_id );
						$datetime1   = new DateTime( $today );
						$datetime2   = new DateTime( $sumbit_date );
						$interval    = $datetime1->diff( $datetime2 );
						$diff        = $interval->format( '%a' );
						$recipient   = $endorser;
						// We'll send out the first reminder at 3 days then there after every 3 days until day 21
						if ( $diff >= 3 && $diff < 22 ) {
							if ( 0 == $diff % 3 ) {
								nano_email_notify( $post_id, $recipient, 'nano_endorsement_member_reminder' );
							}
						}

						// If the day has exceeded 21 days should we do something?
						if ( $diff > 22 ) {
							//
						}
					}
				}
			}
		}

		/* Restore original Post Data */
		wp_reset_postdata();
	}

}

/**
 * Email notify pending endorsements to endorsers
 *
 * @param $status
 */

function endorsement_notify_pending( $status ) {

	$user       = wp_get_current_user();
	$user_login = isset( $user->data->user_login ) ? $user->data->user_login : null;
	$post_ids   = get_user_meta( $user->ID, 'project_endorsement_connection', true );

	$alert = '';
	if ( '' != $post_ids ) {
		$counter_pending  = 0;
		$counter_finished = 0;

		$alert_color = 'pending' == $status ? 'warning' : 'success';

		foreach ( $post_ids as $key => $post_id ) {
			if ( get_post_status( $post_id ) == false ) {
				unset( $post_ids[ $key ] );
			}
		}

		foreach ( $post_ids as $key => $post_id ) {

			$title        = get_the_title( $post_id );
			$endorsements = get_post_meta( $post_id, 'nano_endorsements', true );

			if ( get_post_status( $post_id ) != false ) {

				if ( ! isset( $endorsements['endorsement'][ $user_login ] ) || '' == $endorsements['endorsement'][ $user_login ] ) {
					$pending_check = true;
					break;
				} else {
					$pending_check = false;
				}
			}
		}

		if ( true == $pending_check ) {

			$post_status = strtoupper( get_post_status( $post_id ) ) == 'PUBLISH' ? true : false;

			if ( get_post_status( $post_id ) != false && false === $post_status ) {

				$message = 'pending' == $status ? 'Congratulations, you were asked to endorse a Steam Leader! Please fill in your endorsement ' : 'Thank you, you can view your endorsements ';

				$alert .= '<div class="alert alert-' . $alert_color . '" role="alert">' . $message;
				$alert .= '<a class="alert-link "href="" data-toggle="modal" data-target="#endorse_modal">here</a>.';
				$alert .= '</div>';

			}
		}
	}
	echo $alert;
}

/**
 * On staging site home and archives, drafts should be visible.
 *
 * @param $query
 */

function show_drafts_pending_to_endorsers( $query ) {

	global $wp_the_query;

	if ( $wp_the_query === $query ) {

		$user       = wp_get_current_user();
		$user_login = isset( $user->data->user_login ) ? $user->data->user_login : null;

		$post_ids        = get_user_meta( $user->ID, 'project_endorsement_connection', true );
		$page_id         = isset( $query->query['p'] ) ? $query->query['p'] : '';
		$team_members    = get_post_meta( $page_id, 'nano_team_members', true );
		$original_author = get_post_meta( $page_id, 'original_author', true );
		$post_type       = get_post_type();

		// Either in the admin or as a feed, send the user back with no adjustments to their post status review
		if ( is_admin() || is_feed() ) {
			return;
		}

		if ( 'ignition_product' == $post_type ) {
			if ( ! is_user_logged_in() ) {
				$query->set( 'post_status', array( 'publish' ) );
			} elseif ( current_user_can( 'administrator' ) && is_single() ) {
				$query->set( 'post_status', array( 'pending', 'draft', 'publish' ) );
			} elseif ( $user_login == $original_author ) {
				$query->set( 'post_type', 'ignition_product' );
				$query->set( 'post_status', array( 'pending', 'draft', 'publish' ) );
			} elseif ( is_array( $team_members ) && in_array( $user_login, $team_members ) ) {
				$query->set( 'post_type', 'ignition_product' );
				$query->set( 'post_status', array( 'pending', 'draft', 'publish' ) );
			} elseif ( is_array( $post_ids ) ) {

				if ( in_array( $page_id, $post_ids ) ) {
					$query->set( 'post_type', 'ignition_product' );
					$query->set( 'post_status', array( 'pending', 'draft' ) );
				} else {
					$query->set( 'post_status', array( 'publish' ) );
				}
			} else {
				$query->set( 'post_status', array( 'publish' ) );
			}
		}
	}
}

add_action( 'pre_get_posts', 'show_drafts_pending_to_endorsers' );


/**
 * Make drafts visible on staging site single views.
 *
 * Because on single views, WP_Query goes through logic to make sure the
 * current user can edit the post before displaying a draft.
 *
 * @param $posts
 * @param WP_Query $query
 *
 * @return array
 */
function show_single_drafts_pending_to_endorsers( $posts, WP_Query $query ) {

	$results = null;

	if ( count( $posts ) ) {
		$results = $posts;
	}

	if ( is_user_logged_in() ) {

		$user     = wp_get_current_user();
		$post_ids = get_user_meta( $user->ID, 'project_endorsement_connection', true );
		$page_id  = isset( $query->query['p'] ) ? $query->query['p'] : '';

		//making sure the post is a preview to avoid showing published private posts
		if ( ! is_preview() ) {

			if ( is_array( $post_ids ) ) {

				if ( in_array( $page_id, $post_ids ) ) {

					$results = $posts;

				}
			}
		}

		if ( ! empty( $query->query['p'] ) ) {

			if ( is_array( $post_ids ) ) {
				if ( in_array( $page_id, $post_ids ) ) {

					$results = array( get_post( $query->query['p'] ) );

				}
			}
		}
	}

	return $results;
}

add_filter( 'the_posts', 'show_single_drafts_pending_to_endorsers', 10, 2 );
