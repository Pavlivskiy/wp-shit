<?php if ( ! defined( 'ABSPATH' ) ) {
	die( 'Direct access forbidden.' );
}


function tdrows( $elements ) {
	$arr   = array();
	$count = 0;
	foreach ( $elements as $key => $element ) {

		if ( ! preg_match( '/[^A-Za-z0-9]+$/', $element->nodeValue ) ) {
			// string contains only english letters & digits
			$arr[ $count ] = $element->nodeValue;
			$count ++;
		}


	}

	return $arr;
}

function nano_is_blacklisted() {

	$server_ip_address = file_get_contents( "http://www.geoplugin.com/ip.php" );
	$blacklist_url     = 'http://www.geoplugin.net/ip_status.gp?ip=' . $server_ip_address;
	$black_list_check  = file_get_contents( $blacklist_url );

	$contents = $black_list_check;
	$DOM      = new DOMDocument;
	$DOM->loadHTML( $contents );

	$items = $DOM->getElementsByTagName( 'tr' );

	$new_array = array();
	foreach ( $items as $key => $node ) {
		$child       = tdrows( $node->childNodes );
		$freqs       = array_count_values( $child );
		$freq_absent = isset( $freqs['ABSENT'] ) ? $freqs['ABSENT'] : null;

		if ( is_int( $freq_absent ) && $freq_absent < 2 ) {

			$subject = 'The Geo-location service www.geoplugin.com has banned us for more than 120 requests per minute.';
			$message = ( 'Geo Location Service: http://www.geoplugin.com' . "\n" );
			$message .= ( 'Our Service IP Address: ' . $server_ip_address . "\n" );
			$message .= ( 'Black List Address Check: ' . $blacklist_url . "\n" );
			$message .= ( 'You should consider purchasing a premium license to stop these errors.' . "\n" );

			email_general_admin_notification( $subject, $message );
		}
	}

}

// lat/long from IP
function ip_info( $ip = null, $purpose = 'location', $deep_detect = true ) {
	$output = null;
	if ( filter_var( $ip, FILTER_VALIDATE_IP ) === false ) {
		$ip = $_SERVER['REMOTE_ADDR'];
		if ( $deep_detect ) {
			if ( isset( $_SERVER['HTTP_X_FORWARDED_FOR'] ) && filter_var( @$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP ) ) {
				$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
			}
			if ( isset( $_SERVER['HTTP_CLIENT_IP'] ) && filter_var( @$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP ) ) {
				$ip = $_SERVER['HTTP_CLIENT_IP'];
			}
		}
	}
	$purpose    = str_replace( array( 'name', '\n', '\t', ' ', '-', '_' ), null, strtolower( trim( $purpose ) ) );
	$support    = array( 'country', 'countrycode', 'state', 'region', 'city', 'location', 'address' );
	$continents = array(
		'AF' => 'Africa',
		'AN' => 'Antarctica',
		'AS' => 'Asia',
		'EU' => 'Europe',
		'OC' => 'Australia (Oceania)',
		'NA' => 'North America',
		'SA' => 'South America',
	);
	if ( filter_var( $ip, FILTER_VALIDATE_IP ) && in_array( $purpose, $support ) ) {
		$ipdat = @json_decode( file_get_contents( 'http://www.geoplugin.net/json.gp?ip=' . $ip ) );

		if ( json_last_error() === JSON_ERROR_NONE ) {
			// JSON is valid
			if ( @strlen( trim( $ipdat->geoplugin_countryCode ) ) == 2 ) {
				switch ( $purpose ) {
					case 'location':
						$output = array(
							//'city'           => @$ipdat->geoplugin_city,
							//'state'          => @$ipdat->geoplugin_regionCode,
							//'country'        => @$ipdat->geoplugin_countryName,
							//'country_code'       => @$ipdat->geoplugin_countryCode,
							'latitude'  => @$ipdat->geoplugin_latitude,
							'longitude' => @$ipdat->geoplugin_longitude,
						);
						break;
					case 'address':
						$address = array( $ipdat->geoplugin_countryName );
						if ( @strlen( $ipdat->geoplugin_regionName ) >= 1 ) {
							$address[] = $ipdat->geoplugin_regionName;
						}
						if ( @strlen( $ipdat->geoplugin_city ) >= 1 ) {
							$address[] = $ipdat->geoplugin_city;
						}
						$output = implode( ', ', array_reverse( $address ) );
						break;
					case 'city':
						$output = @$ipdat->geoplugin_city;
						break;
					case 'state':
						$output = @$ipdat->geoplugin_regionName;
						break;
					case 'region':
						$output = @$ipdat->geoplugin_regionName;
						break;
					case 'country':
						$output = @$ipdat->geoplugin_countryName;
						break;
					case 'countrycode':
						$output = @$ipdat->geoplugin_countryCode;
						break;
				}
			}
		} else {
			nano_is_blacklisted();
		}
	}

	return $output;
}

function nano_is_bot( $user_agent ) {

	$botRegexPattern = "(googlebot\/|Googlebot\-Mobile|Googlebot\-Image|Google favicon|Mediapartners\-Google|bingbot|slurp|java|wget|curl|Commons\-HttpClient|Python\-urllib|libwww|httpunit|nutch|phpcrawl|msnbot|jyxobot|FAST\-WebCrawler|FAST Enterprise Crawler|biglotron|teoma|convera|seekbot|gigablast|exabot|ngbot|ia_archiver|GingerCrawler|webmon |httrack|webcrawler|grub\.org|UsineNouvelleCrawler|antibot|netresearchserver|speedy|fluffy|bibnum\.bnf|findlink|msrbot|panscient|yacybot|AISearchBot|IOI|ips\-agent|tagoobot|MJ12bot|dotbot|woriobot|yanga|buzzbot|mlbot|yandexbot|purebot|Linguee Bot|Voyager|CyberPatrol|voilabot|baiduspider|citeseerxbot|spbot|twengabot|postrank|turnitinbot|scribdbot|page2rss|sitebot|linkdex|Adidxbot|blekkobot|ezooms|dotbot|Mail\.RU_Bot|discobot|heritrix|findthatfile|europarchive\.org|NerdByNature\.Bot|sistrix crawler|ahrefsbot|Aboundex|domaincrawler|wbsearchbot|summify|ccbot|edisterbot|seznambot|ec2linkfinder|gslfbot|aihitbot|intelium_bot|facebookexternalhit|yeti|RetrevoPageAnalyzer|lb\-spider|sogou|lssbot|careerbot|wotbox|wocbot|ichiro|DuckDuckBot|lssrocketcrawler|drupact|webcompanycrawler|acoonbot|openindexspider|gnam gnam spider|web\-archive\-net\.com\.bot|backlinkcrawler|coccoc|integromedb|content crawler spider|toplistbot|seokicks\-robot|it2media\-domain\-crawler|ip\-web\-crawler\.com|siteexplorer\.info|elisabot|proximic|changedetection|blexbot|arabot|WeSEE:Search|niki\-bot|CrystalSemanticsBot|rogerbot|360Spider|psbot|InterfaxScanBot|Lipperhey SEO Service|CC Metadata Scaper|g00g1e\.net|GrapeshotCrawler|urlappendbot|brainobot|fr\-crawler|binlar|SimpleCrawler|Livelapbot|Twitterbot|cXensebot|smtbot|bnf\.fr_bot|A6\-Indexer|ADmantX|Facebot|Twitterbot|OrangeBot|memorybot|AdvBot|MegaIndex|SemanticScholarBot|ltx71|nerdybot|xovibot|BUbiNG|Qwantify|archive\.org_bot|Applebot|TweetmemeBot|crawler4j|findxbot|SemrushBot|yoozBot|lipperhey|y!j\-asr|Domain Re\-Animator Bot|AddThis|YisouSpider|BLEXBot|YandexBot|SurdotlyBot|AwarioRssBot|FeedlyBot|Barkrowler|Gluten Free Crawler|Cliqzbot)";

	return preg_match( "/{$botRegexPattern}/", $user_agent );

}


/**
 * get user's geo location whether logged in or not.
 */
function save_user_geo_location() {
	//If user is registered and logged in
	if ( is_user_logged_in() ) {
		/** @var wpdb $wpdb */
		global $wpdb;
		$user    = wp_get_current_user();
		$user_id = $user->ID;
		// Check if user is a creator or backer already and get their zip code here

		if ( isset( nano_get_sc_params( $user_id )->id ) ) {
			$user_zip = nano_get_sc_params( $user_id )->legal_entity->address->postal_code;
		} elseif ( get_user_meta( $user->ID, 'stripe_customer_id', true ) != '' ) {
			$zip      = nano_get_stripe_customer_params( $user_id )->sources->data;
			$user_zip = $zip[0]->address_zip;
		} else {
			$user_zip = null;
		}

		$table_name = $wpdb->prefix . 'zipcodes';
		$wpdb->hide_errors();
		$sql     = $wpdb->prepare( "SELECT * FROM $table_name WHERE zip_code = %s", $user_zip );
		$results = $wpdb->get_results( $sql, OBJECT );

		if ( isset( $results[0]->zip_code ) ) {
			$location = $results[0];
		} else {
			$location = '000000';
			if ( isset( $_SERVER['HTTP_USER_AGENT'] ) && ! nano_is_bot( $_SERVER['HTTP_USER_AGENT'] ) ) {
				$location = ip_info( 'visitor', 'Location' );
			}
		}

		$update_user = update_user_meta( $user_id, 'nano_location_zip', $location );

		if ( true == $update_user ) {
			delete_user_meta( $user_id, 'nano_location_zip' );
			update_user_meta( $user_id, 'nano_location_zip', $location );
		}
	}
}


/**
 * Get the user's geo location
 * @return mixed
 */
function get_user_geo_location() {

	$user_info = null;

	if ( isset( $_SERVER['HTTP_USER_AGENT'] ) && ! nano_is_bot( $_SERVER['HTTP_USER_AGENT'] ) ) {

		if ( is_user_logged_in() ) {
			$user      = wp_get_current_user();
			$user_id   = $user->ID;
			$user_info = get_user_meta( $user->ID, 'nano_location_zip', true );
			if ( '' == $user_info ) {
				$_SESSION['user_loc'] = ip_info( 'Visitor', 'Location' );
				$user_info            = $_SESSION['user_loc'];
			}
		} else {
			$_SESSION['user_loc'] = ip_info( 'Visitor', 'Location' );
			$user_info            = $_SESSION['user_loc'];
		}
	}


	return $user_info;
}


/**
 * For modifying the filtering system to show distance for the carousel shortcode and filtering system for the projects
 * page.
 *
 * @param $args
 *
 * @return mixed
 */
function modify_wp_query_geo_location( $args ) {

	$args['posts_per_page'] = '-1';

	$new_posts = new WP_Query( $args );

	$user_info = get_user_geo_location();

	$new_order = array();
	foreach ( $new_posts->posts as $key => $val ) {
		$project_lat_long = get_post_meta( $val->ID, 'nano_location_zip', true );
		if ( '' != $project_lat_long ) {
			$project_lat  = isset( $project_lat_long->latitude ) ? $project_lat_long->latitude : 0;
			$project_long = isset( $project_lat_long->longitude ) ? $project_lat_long->longitude : 0;
			if ( is_object( $user_info ) ) {
				$user_lat  = isset( $user_info->latitude ) ? $user_info->latitude : 0;
				$user_long = isset( $user_info->longitude ) ? $user_info->longitude : 0;
			} else {
				$user_lat  = isset( $user_info['latitude'] ) ? $user_info['latitude'] : 0;
				$user_long = isset( $user_info['longitude'] ) ? $user_info['longitude'] : 0;
			}
			$new_order[ $key ] = (int) geo_distance( $user_lat, $user_long, $project_lat, $project_long );
			$id[]              = $val->ID;

		}
	}

	uasort( $new_order, 'geo_sorting' );

	$resort_order = array_replace( $new_order, $new_posts->posts );

	$final_order = array();
	foreach ( $resort_order as $order ) {
		$final_order[] = $order->ID;
	}

	$args['posts_per_page'] = get_option( 'posts_per_page' );
	$args['post__in']       = $final_order;
	$args['orderby']        = 'post__in';

	wp_reset_postdata();

	return $args;

}


/**
 * Add lat/long to a project's metadata on save.
 *
 * @param $city
 * @param $state
 * @param $id
 */
function geo_locate_by_city_state( $city, $state, $id ) {
	global $wpdb;
	$table_name = $wpdb->prefix . 'zipcodes';
	$wpdb->hide_errors();
	$sql      = "SELECT * FROM $table_name WHERE state = %s AND city = %s";
	$sql_prep = $wpdb->prepare( $sql, $state, $city );
	$results  = $wpdb->get_results( $sql_prep, OBJECT );
	if ( isset( $results[0]->zip_code ) ) {
		delete_post_meta( $id, 'nano_location_zip' );
		update_post_meta( $id, 'nano_location_zip', $results[0] );
	}
}


/**
 * Multi dimensional unqiue array for the cities and states
 *
 * @param $array
 * @param $key
 *
 * @return array
 */
function unique_multidim_array( $array, $key ) {
	$temp_array = array();
	$i          = 0;
	$key_array  = array();

	foreach ( $array as $val ) {
		if ( is_array( $val ) ) {
			if ( ! in_array( $val[ $key ], $key_array ) ) {
				$key_array[ $i ]  = $val[ $key ];
				$temp_array[ $i ] = $val;
			}
		}
		if ( is_object( $val ) ) {
			send_error( 'object' );
			if ( ! in_array( $val->$key, $key_array ) ) {
				$key_array[ $i ]  = $val->$key;
				$temp_array[ $i ] = $val;
			}
		}
		$i ++;
	}

	return $temp_array;
}


/**
 * Calculate Distance between points
 *
 * @param $lat1
 * @param $lon1
 * @param $lat2
 * @param $lon2
 *
 * @return float
 */
function geo_distance( $lat1, $lon1, $lat2, $lon2 ) {

	$theta = $lon1 - $lon2;
	$dist  = sin( deg2rad( $lat1 ) ) * sin( deg2rad( $lat2 ) ) + cos( deg2rad( $lat1 ) ) * cos( deg2rad( $lat2 ) ) * cos( deg2rad( $theta ) );
	$dist  = acos( $dist );
	$dist  = rad2deg( $dist );
	$miles = $dist * 60 * 1.1515;

	return $miles;

}


/**
 * Sorting function for distance
 *
 * @param $a
 * @param $b
 *
 * @return int
 */
function geo_sorting( $a, $b ) {
	if ( $a == $b ) {
		//echo "a ($a) is same priority as b ($b), keeping the same\n";
		return 0;
	} elseif ( $a > $b ) {
		//echo "a ($a) is higher priority than b ($b), moving b down array\n";
		return 1;
	} else {
		//echo "b ($b) is higher priority than a ($a), moving b up array\n";
		return - 1;
	}
}

