<?php

/* ------------------------
 * A direct query to the WP zip code DB rather than going through admin-ajax.php
 * https://wordpress.stackexchange.com/questions/41808/ajax-takes-10x-as-long-as-it-should-could/41812#41812
 *
------------------------------*/

ini_set( 'html_errors', 0 );
define( 'SHORTINIT', true );

require( $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php' );
require( ABSPATH . WPINC . '/formatting.php' );
wp_plugin_directory_constants();

function send_error( $i ) {
	error_log( print_r( $i, true ) );
}


/**
 * Multi dimensional unique array for the cities and states
 *
 * @param $array
 * @param $key
 *
 * @return array
 */
function unique_multidim_array( $array, $key ) {
	$temp_array = array();
	$i          = 0;
	$key_array  = array();

	foreach ( $array as $val ) {
		if ( ! in_array( $val[ $key ], $key_array ) ) {
			$key_array[ $i ]  = $val[ $key ];
			$temp_array[ $i ] = $val;
		}
		$i ++;
	}

	return $temp_array;
}

/**
 * Comparison between city names and states
 * @param $a
 * @param $b
 *
 * @return int
 */
function cmp( $a, $b ) {
	return strcmp( $a->city, $b->city );
}

global $wpdb;
$table_name = $wpdb->prefix . 'zipcodes';
$wpdb->hide_errors();

$city_name  = strtok( $_POST['name'], ',' );
$state_name = '';

if ( ( $pos = strpos( $_POST['name'], ',' ) ) !== false ) {
	$state_pos = 1;
	if ( strpos( $_POST['name'], ', ' ) !== false ) {
		$state_pos = 2;
	}
	$state_name = substr( $_POST['name'], $pos + $state_pos );
	$clean_code = preg_replace( '/[^a-zA-Z]/', '', $state_name );
	$state_name = strtoupper( $clean_code );
}

$city_name_like  = '%' . $wpdb->esc_like( stripslashes( $city_name ) ) . '%';
$state_name_like = $wpdb->esc_like( stripslashes( $state_name ) ) . '%';
$name_is         = $wpdb->esc_like( stripslashes( $city_name ) ) . '%';
$sql             = "SELECT * FROM $table_name WHERE city LIKE %s AND ( state LIKE %s OR state_full LIKE %s ) ORDER BY (city LIKE %s) DESC, city ASC";

$sql_prep = $wpdb->prepare( $sql, $city_name_like, $state_name_like, $state_name_like, $name_is );
$results  = $wpdb->get_results( $sql_prep );

$cities = array();
if ( count( $results ) > 0 ) {

	foreach ( $results as $r ) {
		$cities[] = array(
			'city'     => addslashes( $r->city ),
			'state'    => $r->state_full,
			'combined' => addslashes( $r->city ) . ', ' . $r->state_full,
		);
	}

	$cities = array_values( unique_multidim_array( $cities, 'combined' ) );

	echo json_encode( $cities ); //encode into JSON format and output

} else {

	$cities[] = array(
		'city'     => '',
		'state'    => '',
		'combined' => 'Sorry no matches, please try again.',
	);

	echo json_encode( $cities ); //encode into JSON format and output
}

die(); //stop "0" from being output