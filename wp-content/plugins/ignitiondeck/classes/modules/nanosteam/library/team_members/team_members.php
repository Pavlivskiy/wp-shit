<?php if ( ! defined( 'ABSPATH' ) ) {
	die( 'Direct access forbidden.' );
}
// Include the co-authors pliugin (modified from the original which is still in the co_authors_plus folder.
require_once dirname( __FILE__ ) . '/co-authors-plus.php'; //  Team Members

function teammember_post_check( $post_id ) {
	$coauthors = wp_get_post_terms( $post_id, 'author', array( 'fields' => 'names' ) );
	return $coauthors;
}


/**
 * Ajax function to register team members
 */

function ajax_register_team_members() {

	// First check the nonce, if it fails the function will break
	check_ajax_referer( 'nano_teammember_secure', 'security' );

	global $coauthors_plus;

	$post_id = isset( $_POST['id'] ) && '' != $_POST['id'] ? $_POST['id'] : '';

	$user_profile_page = strtolower( wp_generate_password( 16, false ) );

	// Nonce is checked, get the POST data and sign user on
	$info                  = array();
	$info['user_nicename'] = generate_unique_user_nicename( $user_profile_page );
	$info['user_pass']     = idmember_pw_gen();
	$info['user_login']    = sanitize_email( $_POST['email'] );
	$info['user_email']    = sanitize_email( $_POST['email'] );

	$prev_members = maybe_unserialize( get_post_meta( $post_id, 'nano_team_members', true ) );
	$prev_members = is_array( $prev_members ) ? $prev_members : array( $prev_members );
	$new_member   = array( $info['user_email'] );

	$nano_team_members = array_filter( array_merge( $prev_members, $new_member ) );

	// Register the user
	$user_register = wp_insert_user( $info );

	if ( is_wp_error( $user_register ) ) {

		$error = $user_register->get_error_codes();

		if ( in_array( 'empty_user_login', $error ) ) {
			echo json_encode(
				array(
					'loggedin' => false,
					'message'  => $user_register->get_error_message( 'empty_user_login' ),
				)
			);
		} elseif ( in_array( 'existing_user_login', $error ) ) {
			echo json_encode(
				array(
					'loggedin' => false,
					'message'  => __( 'Successfully Sent' ),
				)
			);
		} elseif ( in_array( 'existing_user_email', $error ) ) {
			echo json_encode(
				array(
					'loggedin' => false,
					'message'  => __( 'Successfully Sent' ),
				)
			);
		}

		// If the project is already created then lets see if we can email the new team mambers.
		if ( '' != $post_id ) {
			$update_teammember = update_post_meta( $post_id, 'nano_team_members', $nano_team_members );
			if ( true == $update_teammember ) {
				$team_members = get_post_meta( $post_id, 'nano_team_members', true );
				foreach ( $team_members as $team_member ) {
					$user            = get_user_by( 'email', $team_member );
					$check_if_mailed = is_array( get_user_meta( $user->ID, 'project_connection', true ) ) ? get_user_meta( $user->ID, 'project_connection', true ) : array();
					if ( ! in_array( $post_id, $check_if_mailed ) ) {
						$post      = get_post();
						$recipient = $user->user_email;
						nano_email_notify( $post_id, $recipient, 'nanosteam_teammember' );
						$project_ids = $check_if_mailed;
						array_push( $project_ids, $post_id );
						update_user_meta( $user->ID, 'project_connection', $project_ids );
					}
				}
			}
		}
	} else {
		echo json_encode(
			array(
				'loggedin' => false,
				'message'  => __( 'Successfully Sent' ),
			)
		);
		$user    = get_user_by( 'email', $info['user_email'] );
		$user_id = $user->ID;
		idc_welcome_email( $user_id, $info['user_email'] );
		wp_new_user_notification( $user_id, null, 'both' );
		assign_avatar_init( $user_id );
		if ( '' != $post_id ) {
			$update_teammember = update_post_meta( $post_id, 'nano_team_members', $nano_team_members );
			if ( true == $update_teammember ) {
				$team_members = get_post_meta( $post_id, 'nano_team_members', true );
				foreach ( $team_members as $team_member ) {
					$user            = get_user_by( 'email', $team_member );
					$check_if_mailed = is_array( get_user_meta( $user->ID, 'project_connection', true ) ) ? get_user_meta( $user->ID, 'project_connection', true ) : array();
					if ( ! in_array( $post_id, $check_if_mailed ) ) {
						$recipient = $user->user_email;
						nano_email_notify( $post_id, $recipient, 'nanosteam_teammember' );
						$project_ids = $check_if_mailed;
						array_push( $project_ids, $post_id );
						update_user_meta( $user->ID, 'project_connection', $project_ids );
					}
				}
			}
		}
	}

	if (defined('DOING_AJAX') && DOING_AJAX) {
		wp_die();
	}

}

/* ------------------------
-----   Re-filter creator profile page   -----
------------------------------*/

function nano_md_ide_check_creator_profile() {
	if ( isset( $_GET['creator_projects'] ) && 1 == $_GET['creator_projects'] && is_user_logged_in() ) {
		add_filter( 'the_content', 'nano_md_ide_creator_projects' );
	}
}

remove_action( 'init', 'krown_md_ide_check_creator_profile' );
add_action( 'init', 'nano_md_ide_check_creator_profile', 11 );

function nano_md_ide_creator_projects( $content ) {
	ob_start();
	global $current_user;
	global $permalink_structure;
	if ( empty( $permalink_structure ) ) {
		$prefix = '&';
	} else {
		$prefix = '?';
	}
	//get_currentuserinfo();
	$user_id = $current_user->user_login;
	echo '<div class="memberdeck">';
	include_once NANO_STRIPE . '/library/my_account/templates/_mdProfileTabs.php';
	echo '<div class="md-box-wrapper full-width cf"><div class="md-box full"><div class="md-profile">';
	echo '<div class="nano_alert alert alert-warning d-none fade"></div>';
	echo '<table class="md-projects-list table table-responsive">';

	echo '<tbody>';
	// Custom Args for the team members to be recognized for shared projects
	$creator_args  = array(
		'post_type'      => 'ignition_product',
		'post_status'    => array( 'draft', 'pending', 'publish' ),
		'posts_per_page' => - 1,
		'tax_query'      => array(
			array(
				'taxonomy'         => 'author',
				'field'            => 'name',
				'terms'            => $user_id,
				'include_children' => false,
			),
		),
	);
	$user_projects = get_posts( $creator_args );

	if ( ! empty( $user_projects ) ) {
		foreach ( $user_projects as $post ) {
			$post_id    = $post->ID;
			$project_id = get_post_meta( $post_id, 'ign_project_id', true );

			if ( get_post_meta( $post_id, 'original_author', true ) == $user_id ) {
				$project_check = true;
			} elseif ( is_array( get_post_meta( $post_id, 'nano_team_members', true ) ) && in_array( $user_id, get_post_meta( $post_id, 'nano_team_members', true ) ) ) {
				$project_check = true;
			} else {
				$project_check = false;
			}

			if ( ! empty( $project_id ) ) {
				if ( true == $project_check ) {
					$status = $post->post_status;
					if ( strtoupper( $status ) !== 'TRASH' ) {
						$project     = new ID_Project( $project_id );
						$the_project = $project->the_project();
						$thumb       = ID_Project::get_project_thumbnail( $post_id );
						$permalink   = get_permalink( $post_id );
						if ( strtoupper( $status ) == 'DRAFT' ) {
							$permalink = $permalink . '&preview=true';
						}
						include NANO_STRIPE . '/library/my_account/templates/_myProjects.php';
					}
				}
			}
		}
	} else {
		echo '<tr class="project"><td colspan="100%" class="thumb col border-0"><p class="text-center">' . __( 'It looks as though you haven\'t created your first project yet! ', 'nanosteam_8n' ) . '</p></td><td colspan="75%"></td></tr>';
	}
	echo '</tbody>';
	echo '</table><div class="col-sm-12 ' . ( empty( $user_projects ) ? 'text-center' : 'text-right' ) . '"><button class="create_project btn btn-primary" onclick="location.href=\'' . md_get_durl() . $prefix . 'create_project=1\'">' . __( 'Create Project', 'memberdeck' ) . '</button></div></div></div></div>';
	echo '</div>';
	$content = ob_get_contents();
	ob_end_clean();

	return $content;
}

// Check to see if current user can edit project
if ( isset( $_GET['create_project'] ) || isset( $_GET['edit_project'] ) ) {
	add_action( 'init', 'nano_ide_check_create_project', 3 );
}

/**
 * Check the created project and applying multiple team members to it
 */
function nano_ide_check_create_project() {
	if ( isset( $_GET['create_project'] ) && is_user_logged_in() ) {
		add_action( 'wp_enqueue_scripts', 'enqueue_enterprise_js' );
		add_filter( 'the_content', 'ide_create_project' );
		if ( class_exists( 'WPSEO_OpenGraph' ) ) {
			remove_action( 'init', 'initialize_wpseo_front' );
		}
		add_filter( 'jetpack_enable_open_graph', '__return_false', 99 );
	} elseif ( isset( $_GET['edit_project'] ) ) {
		$project_id   = absint( $_GET['edit_project'] );
		$current_user = wp_get_current_user();
		$user_id      = $current_user->user_login;

		$project_editor = apply_filters( 'ide_fes_edit_project_editor', false, $project_id );
		$ide_edit_hooks = false;

		//Nano Multi Author Check
		$team_members       = get_post_meta( $_GET['edit_project'], 'nano_team_members', true );
		$current_user_email = $current_user->user_email;

		// If we are getting project_editor true, then we don't need to check for the project owner
		if ( $project_editor ) {
			$ide_edit_hooks = true;
		} else {
			// Check if current user is project owner
			// $user_projects = get_user_meta($user_id, 'ide_user_projects', true);
			$post            = get_post( $project_id );
			$original_author = get_post_meta( $post->ID, 'original_author', true );

			if ( ! empty( $post ) ) {
				if ( ( $user_id == $original_author ) || current_user_can( 'administrator' ) ) {
					$ide_edit_hooks = true;
				}
				//Nano Multi Author Check
				if ( is_array( $team_members ) && 0 < count($team_members ) ) {
					foreach ( $team_members as $team_member ) {
						if ( $current_user_email == $team_member ) {
							$ide_edit_hooks = true;
						}
					}
				}
			}
		}
		// If current user can edit project using FES, the attach edit action functions
		if ( $ide_edit_hooks ) {
			add_filter( 'the_content', 'ide_edit_project' );
			add_action( 'wp_enqueue_scripts', 'enqueue_enterprise_js' );
		}
		if ( class_exists( 'WPSEO_OpenGraph' ) ) {
			remove_action( 'init', 'initialize_wpseo_front' );
		}
		add_filter( 'jetpack_enable_open_graph', '__return_false', 99 );
	} elseif ( isset( $_GET['export_project'] ) ) {
		$project_id = get_post_meta( $_GET['export_project'], 'ign_project_id', true );
		if ( $project_id > 0 ) {
			$force_download = ID_Member::export_members( $project_id, true );
		}
	}
}



/**
 * Check to see if current user can see projects tab if they have been added by another user but has not created their own project.
 */
function nano_md_creator_projects() {

	if ( current_user_can( 'create_edit_projects' ) ) {
		$show_psettings = false;
		$settings       = get_option( 'memberdeck_gateways' );
		if ( ! empty( $settings ) ) {
			if ( is_array( $settings ) ) {
				$epp_fes = $settings['epp_fes'];
				$esc     = $settings['esc'];
				if ( $epp_fes || $esc ) {
					$show_psettings = true;
				}
			}
		}
		if ( is_multisite() ) {
			require( ABSPATH . WPINC . '/pluggable.php' );
		}
		global $permalink_structure;
		if ( empty( $permalink_structure ) ) {
			$prefix = '&';
		} else {
			$prefix = '?';
		}
		$current_user   = wp_get_current_user();
		$user_id        = $current_user->user_login;
		$enable_creator = get_user_meta( $user_id, 'enable_creator', true );
		// Old Args from original query
		$creator_args = array(
			'post_type'      => 'ignition_product',
			'author'         => $user_id,
			'posts_per_page' => - 1,
			'post_status'    => array( 'draft', 'pending', 'publish' ),
		);
		//New args with the team member additions
		$creator_args  = array(
			'post_type'      => 'ignition_product',
			'post_status'    => array( 'draft', 'pending', 'publish' ),
			'posts_per_page' => - 1,
			'tax_query'      => array(
				array(
					'taxonomy'         => 'author',
					'field'            => 'name',
					'terms'            => $user_id,
					'include_children' => false,
				),
			),
		);
		$user_projects = apply_filters( 'id_creator_projects', get_posts( apply_filters( 'id_creator_args', $creator_args ) ) );

		if ( ! empty( $user_projects ) ) {

			$tab = __( 'My Projects', 'memberdeck' );
			$url = md_get_durl() . $prefix . apply_filters( 'idc_creator_projects_slug', 'creator_projects' ) . '=1';
		} else {
			$tab = __( 'Create Project', 'memberdeck' );
			$url = md_get_durl() . $prefix . 'create_project=1';
		}
		echo '<li ' . ( isset( $_GET[ apply_filters( 'idc_creator_projects_slug', 'creator_projects' ) ] ) || isset( $_GET['create_project'] ) || isset( $_GET['project_files'] ) ? 'class="active"' : '' ) . '><a href="' . $url . '">' . $tab . '</a></li>';
	}
}

remove_action( 'md_profile_extratabs', 'md_creator_projects', 2 );
add_action( 'md_profile_extratabs', 'nano_md_creator_projects', 3 );


/**
 * Front End Team Member Function
 *
 * @param $post_id
 */
function meet_the_team( $post_id ) {
	$team_members          = get_post_meta( $post_id, 'nano_team_members', true );
	$original_author       = get_post_meta( $post_id, 'original_author', true );
	$original_author       = false !== get_user_by( 'login', $original_author ) ? get_user_by( 'login', $original_author ) : get_user_by( 'email', $original_author );
	$original_author_email = isset( $original_author->user_email ) ? $original_author->user_email : '';

	if ( is_array( $team_members ) ) {

		$team_members = array_unique( $team_members );

		foreach ( $team_members as $team_member ) {

			if ( $team_member !== $original_author_email ) {

				if ( '' != $team_member ) {
					$user            = get_user_by( 'email', $team_member );
					$nano_avatar     = get_user_meta( $user->ID, 'nano_idc_avatar', true );
					$title           = get_user_meta( $user->ID, 'nano_user_title', true );
					$nano_avatar_img = isset( $nano_avatar[0] ) && is_array( $nano_avatar ) ? $nano_avatar[0] : $nano_avatar;
					$nano_avatar_img = '<img class="img-fluid rounded-circle" src="' . $nano_avatar_img . '">';
					if ( $user->user_email != $user->display_name ) {

						$profile  = $user->ID;
						$content  = '<div class="pl-sm-4">';
						$content .= '<div class="media mb-3">';
						$name     = apply_filters( 'ide_profile_name', $user->display_name, $user );
						$content .= '<a class="float-left mr-3" href="' . get_author_posts_url( $profile ) . '">' . $nano_avatar_img . '</a>';
						$content .= '<div class="media-body">';
						$content .= '<h6>' . apply_filters( 'ide_backer_name', $name, $user ) . ' <small class="text-muted">' . $title . '</small></h6>';
						$content .= '<p>' . wp_trim_words( wpautop( apply_filters( 'ide_profile_description', $user->description, $user ) ), '30', ' ...' ) . '</p>';
						$content .= '</div></div></div>';
						echo $content;

					}
				}
			}
		}
	}
}

/**
 * Returns the user's profile link
 * @param $post_id
 */
function nano_team_links( $post_id ) {

	$team_members          = get_post_meta( $post_id, 'nano_team_members', true );
	$original_author       = get_post_meta( $post_id, 'original_author', true );
	$original_author       = false !== get_user_by( 'login', $original_author ) ? get_user_by( 'login', $original_author ) : get_user_by( 'email', $original_author );
	$original_author_email = isset( $original_author->user_email ) ? $original_author->user_email : '';

	if ( is_array( $team_members ) ) {

		array_unshift( $team_members, $original_author_email );
		$team_members = array_unique( $team_members );

		$len  = 0;

		// Need to count actual amount of members to be displayed
		foreach ( $team_members as $team_member ) {

			if ( '' != $team_member ) {

				$user = false !== get_user_by( 'login', $team_member ) ? get_user_by( 'login', $team_member ) : get_user_by( 'email', $team_member );

				if ( $user->user_email != $user->display_name ) {

					$len++;

				}
			}
		}

		$i       = 0;
		$content = '';
		//Lets go through the team members
		foreach ( $team_members as $team_member ) {

			if ( $team_member  ) {

				if ( '' != $team_member ) {

					$user        = get_user_by( 'email', $team_member );
					$nano_avatar = get_user_meta( $user->ID, 'nano_idc_avatar', true );

					if ( $user->user_email != $user->display_name ) {

						$position = 'between';
						if ( $i == 0 ) {
							$position = 'between';
						} elseif ( $i == $len - 1 ) {
							// last
							$position = 'last';
						} elseif ( $i == $len - 2 ) {
							// second to last
							$position = 'between_last';
						}

						$profile_id = $user->ID;

						$content .= '<a href="' . get_author_posts_url( $profile_id ) . '">' . $user->display_name . '</a>' . nano_separator_punct( $position );

						$i++;

					}
				}
			}
		}
		return $content;
	}
}


//Helper function for the following new template tags
function nano_separator_punct( $position ) {

	$default_before       = '';
	$default_between      = ', ';
	$default_between_last = __( ' and ', 'nanosteam_8n' );
	$default_after        = '. ';

	if ( 'first' === $position ) {
		$output = $default_before;
	}
	if ( 'between' === $position ) {
		$output = $default_between;
	}
	if ( 'between_last' === $position ) {
		$output = $default_between_last;
	}
	if ( 'last' === $position ) {
		$output = $default_after;
	}

	return $output;
}
