jQuery( document ).ready( function() {

    // Create Stripe Bank Account Token for User Creation

    var stripe = Stripe( nano_stripe.sc_pk ),
        stripeFunding = jQuery( '.funding' ),
        stripeConnectUpdate = jQuery( '#nano_stripe_connect_update' );

    var numberFields, trackinElements, last4SSN, bankingValuesAccount, bankingValuesRouting, accountHolder, bankAccountParams,
        required, mainRequired, bothFields, accountNumber, accountVerfiy;

    function idempotencyKey() {
        var text = '';
        var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var i;

        for ( i = 0; 5 > i; i++ ) {
            text += possible.charAt( Math.floor( Math.random() * possible.length ) );
        }

        return text;
    }


    // Apply Stripe Token to Ajax POST to create user
    function nanoStripeConnect( result ) {

        var errorElement = jQuery( '.stripe-error' ),
            stripeConnectBtn = jQuery( '#nano_stripe_connect' );
        var dataToSend;

        stripeConnectBtn.text( 'Verifying...' );

        if ( result.token ) {

            jQuery( 'input[name="bk_token"]' ).val( result.token.id );

            dataToSend = {
                action: 'nano_stripe_connect_custom',
                security: jQuery( '#nano_stripe_connect_security' ).val(),
                _wp_http_referer: jQuery( 'input[name=_wp_http_referer]' ).val(),
                business_name: jQuery( '#biz_dba' ).val(),
                business_url: jQuery( '#url' ).val(),
                city: jQuery( '#biz_city' ).val(),
                line1: jQuery( '#biz_street' ).val(),
                postal_code: jQuery( '#biz_zip' ).val(),
                state: jQuery( '#biz_state' ).val(),
                business_tax_id: jQuery( '#biz_tax_id_no' ).val(),
                day: jQuery( '#owner_dob_day' ).val(),
                month: jQuery( '#owner_dob_mon' ).val(),
                year: jQuery( '#owner_dob_year' ).val(),
                first_name: jQuery( '#owner_first_name' ).val(),
                last_name: jQuery( '#owner_last_name' ).val(),
                ssn_last_4: jQuery( '#owner_personal_id_no' ).val(),
                type: jQuery( 'input[name=biz_type]:checked' ).val(),
                external_account: result.token.id,
                idempotency_key: idempotencyKey()
            };

            jQuery.ajax({
                type: 'POST',
                dataType: 'json',
                url: nano_stripe.ajaxurl,
                data: dataToSend,
                success: function( data ) {
                    if ( data.stripeError === false ) {
                        errorElement.html( 'Your account has been sucessfully connected! Stripe has some final checks to do and if there are any problems you will be notified by email shortly' );
                        errorElement.addClass( 'show active' ).removeClass( 'd-none' ).removeClass( 'd-none' );
                        stripeConnectBtn.text( 'Connected Account' ).prop( 'disabled', true );
                        jQuery( 'html,body' ).animate({ scrollTop: 0 }, 500, 'swing' );
                    } else {
                        errorElement.text( data.message );
                        errorElement.addClass( 'show active' ).removeClass( 'd-none' );
                        stripeConnectBtn.text( 'Register Account' );
                        jQuery( 'html,body' ).animate({ scrollTop: 0 }, 500, 'swing' );
                    }
                },
                error: function( data ) {
                    console.log( data );
                }
            });

        } else {
            errorElement.text( result.error.message + ' : ' + result.error.param );
            errorElement.addClass( 'show active' ).removeClass( 'd-none' );
            stripeConnectBtn.text( 'Register Account' );
            jQuery( 'html,body' ).animate({ scrollTop: 0 }, 500, 'swing' );
        }
    }

    function nanoStripeConnectUpdateAccount( result ) {

        var errorElement = jQuery( '#add_alert' );
        var fd = new FormData();
        var nanoStripeConnectUpdate = jQuery( '#nano_stripe_connect_update' ),
            identityDocument = jQuery( '#identity_document' );

        errorElement.addClass( 'hide' );

        fd.append( 'action', 'nano_stripe_connect_update' );
        fd.append( 'security', jQuery( '#nano_stripe_connect_security' ).val() );
        if ( identityDocument.length ) {
            fd.append( 'identity_document', identityDocument.prop( 'files' )[ 0 ] );
        }
        if ( jQuery( '#biz_dba' ).length ) {
            fd.append( 'business_name', jQuery( '#biz_dba' ).val() );
        }
        fd.append( 'business_url', jQuery( '#url' ).val() );
        fd.append( 'city', jQuery( '#biz_city' ).val() );
        fd.append( 'line1', jQuery( '#biz_street' ).val() );
        fd.append( 'postal_code', jQuery( '#biz_zip' ).val() );
        fd.append( 'state', jQuery( '#biz_state' ).val() );
        if ( jQuery( '#biz_tax_id_no' ).length ) {
            fd.append( 'business_tax_id', jQuery( '#biz_tax_id_no' ).val() );
        }
        fd.append( 'day', jQuery( '#owner_dob_day' ).val() );
        fd.append( 'month', jQuery( '#owner_dob_mon' ).val() );
        fd.append( 'year', jQuery( '#owner_dob_year' ).val() );
        fd.append( 'first_name', jQuery( '#owner_first_name' ).val() );
        fd.append( 'last_name', jQuery( '#owner_last_name' ).val() );
        fd.append( 'ssn_last_4', jQuery( '#owner_personal_id_no' ).val() );
        fd.append( 'type', jQuery( 'input[name=biz_type]:checked' ).val() );
        fd.append( 'routing_number', jQuery( '#routing_number' ).val() );
        fd.append( 'account_number', jQuery( '#account_number' ).val() );
        fd.append( 'idempotency_key', idempotencyKey() );

        nanoStripeConnectUpdate.text( 'Verifying...' );

        if ( 'undefined' === typeof result ) {
            jQuery.ajax({
                type: 'POST',
                dataType: 'json',
                url: nano_stripe.ajaxurl,
                data: fd,
                contentType: false,
                processData: false,
                cache: false,
                success: function( data ) {
                    if ( data.stripeError === 'success' ) {
                        errorElement.text( data.message );
                        errorElement.addClass( 'show' ).removeClass( 'd-none' );
                        nanoStripeConnectUpdate.text( 'Account Updated' ).prop( 'disabled', true );
                        jQuery( 'html,body' ).animate( { scrollTop: 0 }, 500, 'swing' );
                    } else {
                        errorElement.text( data.message );
                        errorElement.addClass( 'show' ).removeClass( 'd-none' );
                        nanoStripeConnectUpdate.text( 'Update Account' );
                        jQuery( 'html,body' ).animate( { scrollTop: 0 }, 500, 'swing' );
                    }
                },
                error: function( data ) {
                    console.log( data );
                }
            });
        } else {
            if ( result.token ) {
                fd.append( 'external_account', result.token.id );
                jQuery.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: nano_stripe.ajaxurl,
                    data: fd,
                    contentType: false,
                    processData: false,
                    cache: false,
                    success: function( data ) {
                        if ( data.stripeError === 'success' ) {
                            errorElement.text( data.message );
                            errorElement.addClass( 'show' ).removeClass( 'd-none' );
                            nanoStripeConnectUpdate.text( 'Account Updated' ).prop( 'disabled', true );
                            jQuery( 'html,body' ).animate( { scrollTop: 0 }, 500, 'swing' );
                        } else {
                            errorElement.text( data.message );
                            errorElement.addClass( 'show' ).removeClass( 'd-none' );
                            nanoStripeConnectUpdate.text( 'Update Account' );
                            jQuery( 'html,body' ).animate( { scrollTop: 0 }, 500, 'swing' );
                        }
                    },
                    error: function( data ) {
                        console.log( data );
                    }
                });
            } else {
                errorElement.text( result.error.message );
                errorElement.addClass( 'show' ).removeClass( 'd-none' );
                nanoStripeConnectUpdate.text( 'Update Account' );
                jQuery( 'html,body' ).animate( { scrollTop: 0 }, 500, 'swing' );
            }
        }

    }

    // Override IDK's ID_Form Case for Number inputs defaulting to 0 if value is null.
    numberFields = stripeFunding.find( 'input[type="number"]' );

    jQuery.each( numberFields, function() {
        if ( '0' === jQuery( this ).val() ) {
            jQuery( this ).val( null );
        }
    });

    // Disable IDK hack on input FILE type
    jQuery( '#identity_document' ).on( 'change', function( e ) {
        e.stopPropagation();
    });

    // Include Business / Non Profit Requirement Details
    jQuery( '.biz_type' ).on( 'change', function() {
        if ( 'company' === jQuery( this ).val() ) {
            console.log( 'here' );
            if ( ! jQuery( '.biz_acct' ).length ) {
                jQuery( '.legal_add' ).before( '<div class="form-group form-row pb-3">' +
                    '<div class="formval col-sm-4 fadein biz_acct">' +
                    '<small class="form-text text-muted d-block d-sm-none">Business Name</small>' +
                    '<label class="sr-only" for="biz_dba">Business Name</label>' +
                    '<input type="text" id="biz_dba" name="biz_dba" class="form-control form-control-sm required" value="" placeholder="Business Name" required="" aria-label="Business Name">' +
                    '<small class="form-text text-muted d-none d-sm-block">Business Name</small>' +
                    '</div>' +
                    '<div class="formval col-sm-4 fadein biz_acct">' +
                    '<small class="form-text text-muted d-block d-sm-none">Business number (Tax ID / EIN)</small>' +
                    '<label class="sr-only" for="biz_tax_id_no">Business number (Tax ID / EIN):</label>' +
                    '<input type="text" id="biz_tax_id_no" name="biz_tax_id_no" class="form-control form-control-sm required " value="" placeholder="" required="" aria-label="Business number (Tax ID / EIN)">' +
                    '<small class="form-text text-muted d-none d-sm-block">Business number (Tax ID / EIN)</small>' +
                    '</div>' +
                    '</div>'
                );
            }
        } else if ( 'individual' === jQuery( this ).val() ) {
            jQuery( '.biz_acct' ).fadeOut( 'slow' ).delay( 400 ).parent().remove();
        }
    });

    trackinElements      = stripeFunding.find( ':input' );
    bankingValuesAccount = jQuery( '#account_number' );
    bankingValuesRouting = jQuery( '#routing_number' );

    // Track required elements in real time and enable update account button
    trackinElements.on( 'keyup change', function() {

        var value = jQuery( this ).val();

        stripeConnectUpdate.prop( 'disabled', false );
        stripeConnectUpdate.text( 'Update Account' );
        if ( '' === value || value === undefined ) {
            jQuery( this ).addClass( 'is-invalid' );
        } else {
            jQuery( this ).removeClass( 'is-invalid' );
        }
    });

    // Track account fields in real time
    function trackingAccountDiff() {
        var status, bothFields;

        if ( jQuery( '#account_number_validate' ).length ) {
            bothFields = jQuery( '#account_number_validate, #account_number' );
            bothFields.on( 'keyup change', function() {

                var accountNumber = jQuery( '#account_number' );
                var accountVerfiy = jQuery( '#account_number_validate' );

                if ( accountNumber.val() === accountVerfiy.val() && '' !== accountNumber.val() && '' !== accountVerfiy.val() ) {
                    bothFields.removeClass( 'is-invalid' );
                } else {
                    bothFields.addClass( 'is-invalid' );
                }

            });
        } else {
            status = true;
        }

        return status;

    }

    trackingAccountDiff();

    stripeConnectUpdate.on( 'click', function( e ) {

        var alertInfo = jQuery( '.alert-info' );

        e.preventDefault();

            accountHolder = jQuery( 'input[name=biz_type]:checked' ).val();
            bankAccountParams = {
                country: 'US',
                currency: 'USD',
                account_number: jQuery( '#account_number' ).val(),
                account_holder_name: jQuery( '#owner_first_name' ).val() + ' ' + jQuery( '#owner_last_name' ).val(),
                account_holder_type: accountHolder,
                routing_number: jQuery( '#routing_number' ).val()
            };

        if ( ( '' !== bankingValuesAccount.val() && bankingValuesAccount.val() !== bankingValuesAccount.data( 'base' ) ) ) {
            stripe.createToken( 'bank_account', bankAccountParams ).then( nanoStripeConnectUpdateAccount );
        } else {
            nanoStripeConnectUpdateAccount();
        }

        if ( alertInfo.length ) {
            //alertInfo.hide();
        }

    });

    // Create Bank Acccount Token for registering user.
    jQuery( '#nano_stripe_connect' ).on( 'click', function( e ) {
        var accountNumberInput = jQuery( '#account_number' );
        e.preventDefault();

            accountHolder = jQuery( 'input[name=biz_type]:checked' ).val();
            bankAccountParams = {
                country: 'US',
                currency: 'USD',
                account_number: accountNumberInput.val(),
                account_holder_name: jQuery( '#owner_first_name' ).val() + ' ' + jQuery( '#owner_last_name' ).val(),
                account_holder_type: accountHolder,
                routing_number: jQuery( '#routing_number' ).val()
            };
            required = jQuery( '.required' );
            mainRequired = jQuery( '.funding .required' );
            bothFields = jQuery( '#account_number_validate, #account_number' );
            accountNumber = accountNumberInput.val();
            accountVerfiy = jQuery( '#account_number_validate' ).val();

        jQuery.each( required, function() {
            var value = jQuery( this ).val();

            if ( '' === value || value === undefined ) {
                jQuery( this ).addClass( 'is-invalid' );
            } else {
                jQuery( this ).removeClass( 'is-invalid' );
            }
        });

        if ( ( accountNumber !== accountVerfiy ) || '' === accountNumber || '' === accountVerfiy ) {
            bothFields.addClass( 'is-invalid' );
        }

        if ( mainRequired.hasClass( 'error' ) ) {

            jQuery( 'html,body' ).animate({ scrollTop: jQuery( '#funding_container' ).offset().top }, 1000 );

        } else {
            required.removeClass( 'is-invalid' );
            stripe.createToken( 'bank_account', bankAccountParams ).then( nanoStripeConnect );
        }

    });

});


