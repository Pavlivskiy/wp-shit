<?php if ( ! defined( 'ABSPATH' ) ) {
	die( 'Direct access forbidden.' );
}

/* ------------------------
 * Adding $_SESSION to the mix for token tracking
 *
------------------------------*/

if ( session_status() === PHP_SESSION_NONE || session_status() === 1 ) {
	session_start();
}


/* ------------------------
 * Adding Token to sussessful order
 *
------------------------------*/



/**
 * If nanosteam default project order is successful then add credits to client, create a new purchase for the client's
 * supported project with credits and add those credits to project
 */
function nanosteam_tokens() {

	$defaut_project_id = function_exists( 'nano_get_default_project_id' ) ? nano_get_default_project_id() : '';

	global $wpdb;
	$table_name               = $wpdb->prefix . 'mdid_assignments';
	$sql                      = "SELECT * FROM $table_name WHERE project_id LIKE %s";
	$sql_prepare              = $wpdb->prepare( $sql, $defaut_project_id );
	$default_project_level_id = $wpdb->get_results( $sql_prepare );
	$default_project_level_id = isset($default_project_level_id[0]) ? $default_project_level_id[0]->level_id : null;

	if ( isset( $_GET['idc_product'] ) && isset( $_GET['paykey'] ) && $default_project_level_id == $_GET['idc_product'] ) {


		$_SESSION['order_type'] = 'creditcard';

		if ( true == ! $_SESSION['loaded'] ) {

			global $wpdb;
			$tz = get_option( 'timezone_string' );
			if ( empty( $tz ) ) {
				$tz = 'UTC';
			}
			date_default_timezone_set( $tz );

			// Project Details
			//ID of the project which was set on the project page.
			$nano_project_id = $_SESSION['nano_project_id'];
			//ID of the projects level (1 less than the project ID)

			// Adjust the asigments if the level ID is incorrect on

			$sql_check             = $wpdb->prepare( $sql, $nano_project_id );
			$nano_product_level_id = $wpdb->get_results( $sql_check );
			$nano_product_level_id = isset( $nano_product_level_id[0] ) ? $nano_product_level_id[0]->level_id : null;

			$nano_product_id = $nano_product_level_id;

			$access_levels = array( $nano_product_id );
			$project       = new ID_Project( $nano_project_id );
			$post_id       = $project->get_project_postid();

			//User Details
			$current_user = wp_get_current_user();
			$user_id      = absint( $current_user->ID );
			$user_meta    = get_user_meta( $user_id );
			$email        = $current_user->user_email;
			$member       = new ID_Member();
			$check_user   = $member->check_user( $email );

			// Token Details
			$credits = $_SESSION['nano_project_price'];

			// Transdaction ID
			// We want to grab the strip transaction ID and use it here for the token ID for tracking.
			$order      = new ID_Member_Order( null, $user_id, ( $default_project_level_id ) );
			$last_order = apply_filters( 'idc_last_order_lightbox', $order->get_last_order() );
			$txn_id     = 'nano-' . $last_order->transaction_id;

			// E Date
			$exp    = strtotime( '+90 days' );
			$e_date = date( 'Y-m-d h:i:s', $exp );

			if ( ! empty( $check_user ) ) {

				// Add tokens to the user account based on purchase amount. use our own function to get away from floats.
				if ( isset( $user_id ) && isset( $credits ) ) {
					//ID_Member::add_credits($user_id, $credits);
					nano_add_credits( $user_id, $credits );
				}

				// Get the user's tokens.
				$member     = new ID_Member( $user_id );
				$md_credits = $member->get_user_credits();

				// Add product order and subtract tokens from user

				if ( ( $md_credits + 1 ) >= $credits ) {
					$match_user = $member->match_user( $user_id );

					if ( ! empty( $match_user ) ) {

						// Adding the order to the backer

						if ( isset( $match_user->access_level ) ) {
							// let's combine levels
							$old_levels = unserialize( $match_user->access_level );
							if ( is_array( $old_levels ) ) {
								$key = array();
								foreach ( $old_levels as $key['val'] ) {
									$access_levels[] = $key['val'];
								}
							}
						}
						if ( isset( $match_user->data ) ) {
							// let's combine data
							$old_data = unserialize( $match_user->data );
							// do we need any data for credit purchases?
							//$old_data[] =  array('customer_id' => $custid);
						} else {
							$old_data = array();
						}
						$paykey = md5( $email . time() );
						// price is 0 because they are using a credit/token
						$order = new ID_Member_Order( null, $user_id, $nano_product_id, null, $txn_id, '', 'active', $e_date, '0' );

						$new_order = $order->add_order();
						MD_Keys::set_licenses( $user_id, $nano_product_id );
						$user = array(
							'user_id' => $user_id,
							'level'   => $access_levels,
							'data'    => $old_data,
						);
						ID_Member::update_user( $user );

						ID_Member_Order::update_order_meta( $new_order, 'pwyw_price', $credits );
						//ID_Member_Credit::use_credits($user_id, $credits);
						nano_use_credits( $user_id, $credits );

						// DB update to expire unused tokens and to also update how many tokens were used.
						nano_user_token_update( '' );

						// don't send receipt for now
						//do_action('memberdeck_credit_receipt', $user_id, $level_data->credit_value);
						do_action( 'memberdeck_payment_success', $user_id, $new_order, $paykey, '', 'credit' );

						// Adding the order to the project as the user order does not automatically apply to the project

						$sql = "INSERT INTO " . $wpdb->prefix . "ign_pay_info
                            (first_name,last_name,email,address,country,state,city,zip,product_id,product_level,prod_price,status,created_at)
                        VALUES (
                            
                            '" . esc_attr( $user_meta['first_name'][0] ) . "',
                            '" . esc_attr( $user_meta['last_name'][0] ) . "',
                            '" . esc_attr( $email ) . "',
                            '" . esc_attr( 'address' ) . "',
                            '" . esc_attr( 'country' ) . "',
                            '" . esc_attr( 'state' ) . "',
                            '" . esc_attr( 'city' ) . "',
                            '" . esc_attr( 'zip' ) . "',
                            '" . absint( $nano_project_id ) . "',
                            '" . absint( '1' ) . "',
                            '" . esc_attr( $credits ) . "',
                            '" . esc_attr( 'C' ) . "',
                            '" . nano_get_formated_date_time_gmt( 'Y-m-d H:i:s' ) . "'
                        )";
						$wpdb->query( $sql );
						$pay_info_id = $wpdb->insert_id;

						do_action( 'id_modify_order', $pay_info_id, 'insert' );
						do_action( 'id_insert_order', $pay_info_id );

						// Update creator when pledge has been successful

						// Update the project's funding and percentage meta data
						$post = get_post( $post_id );
						set_project_meta( $post_id, $post, '' );

						// Changing the ID of this get to change the popup message that appears after the purchase.
						$_GET['idc_product'] = $nano_product_id;

						// Update the Strip transaction meta data
						nano_stripe_payment_status_meta_data( $post_id, $new_order, $user_id );

						// Email the creator that their project has received a pledge of support
						creator_pledge_update_notify( $post_id );

						send_error( 'credit card system fired / tokens created - Post ID:' . $post_id . ' Order ID: ' . $new_order . ' User ID:' . $user_id );

					}
				}
			}
		}

		$_SESSION['loaded'] = true; // Only allow this function to run once.

	} elseif ( ( isset( $_GET['idc_product'] ) && ( $default_project_level_id ) != $_GET['idc_product'] ) && isset( $_GET['paykey'] ) ) {
		$_SESSION['order_type'] = 'token';

		$current_user = wp_get_current_user();
		$user_id      = absint( $current_user->ID );

		$product_id = $_GET['idc_product'];

		$nano_project_id = $_SESSION['nano_project_id'];
		$level_id        = nano_get_level_id( $nano_project_id );

		$order          = new ID_Member_Order( null, $user_id, $level_id );
		$order          = $order->get_last_order();
		$order_id       = $order->id;
		$order_level_id = $order->level_id;
		$post_id        = nano_post_id_by_order_level_id( $level_id );

		$price     = ID_Member_Order::get_order_meta( $order_id, 'pwyw_price' );
		$level_id  = '';
		$source    = '';
		$new_order = $order_id;

		send_error( 'token system fired / Permalink:' . get_permalink( $post_id ) . ' Order ID: ' . $order_id . ' User ID:' . $user_id );

		// DB update to expire unused tokens and to also update how many tokens were used.
		nano_user_token_update( '' );

		// Update the Stripe transaction meta data
		nano_stripe_payment_status_meta_data( $post_id, $order_id, $user_id );

		// Adding the stripe fee to the credit order
		nano_add_stripe_fee_to_credit_order( $order, $current_user, false );

		// Email the creator that their project has received a pledge of support
		creator_pledge_update_notify( $post_id );

		do_action( 'idmember_receipt', $user_id, $price, $level_id, $source, $new_order );

	} else {
		$_SESSION['order_type'] = 'token';
	}

}

add_action( 'init', 'nanosteam_tokens' );

/**
 * Replacing IDC Order Receipt Lightbox
 *
 * @param $notification
 *
 * @return string
 */
function nano_idc_order_lightbox( $notification ) {

	$order_type = false;
	if ( isset($_SESSION['order_type']) && 'creditcard' == $_SESSION['order_type'] ) {
		$_GET['idc_product'] = nano_get_level_id( $_SESSION['nano_project_id'] );
		$order_type          = true;
	}
	global $global_currency;

	if ( isset( $_GET['idc_product'] ) && isset( $_GET['paykey'] ) ) {
		if ( class_exists( 'ID_Project' ) ) {
			$settings = ID_Project::get_id_settings();
		}
		$current_user = wp_get_current_user();
		$order        = new ID_Member_Order( null, $current_user->ID, $_GET['idc_product'] );
		$level        = ID_Member_Level::get_level( $_GET['idc_product'] );
		$levels       = array( $level );
		// Project and order details to be shown on template
		$last_order = apply_filters( 'idc_last_order_lightbox', $order->get_last_order() );

		// First checking if this lightbox is loaded for the 1st time using transients, if not, don't load lighbox and return $notification
		if ( isset( $last_order ) ) {//&& false === ( $is_set = get_transient( 'idc_order_lightbox_'.$last_order->id ) ) ) {
			set_transient( 'idc_order_lightbox_' . $last_order->id, 'value_stored', 0 );
			$status = 'completed';
			// Getting the order price using filter
			if ( false == $order_type ) {
				// $price = apply_filters('idc_order_price', $last_order->price, $last_order->id);
				$price = ID_Member_Order::get_order_meta( $last_order->id, 'pwyw_price' ) . ' Credits';
			} else {
				$price = '$' . ID_Member_Order::get_order_meta( $last_order->id, 'pwyw_price' );
			}
		} elseif ( ! isset( $last_order ) ) {
			// If level is recurring, the check there is a subscription for it
			if ( 'recurring' == $level->level_type ) {
				// We need to check if recurring order is made and not returned by payment gateway yet
				$subscription = new ID_Member_Subscription( null, $current_user->ID, $_GET['idc_product'] );
				$sub_details  = $subscription->find_subscription();
				if ( ! empty( $sub_details ) ) {
					// The subscription exists then it means that order hasn't yet completed, so go on
					$status     = 'pending';
					$last_order = array();
					if ( isset( $_GET['mdid_checkout'] ) ) {
						$price = apply_filters( 'id_price_format', $level->level_price, $sub_details->source );
					} else {
						$price = apply_filters( 'idc_price_format', $level->level_price, $sub_details->source );
					}
				} else {
					return $notification;
				}
			}
		} else {
			return $notification;
		}
		ob_start();
		include_once NANO_STRIPE . '/library/idc_templates/_orderLightbox.php';
		$notification .= ob_get_contents();
		ob_end_clean();
	}

	return $notification;
}

remove_filter( 'the_content', 'idc_order_lightbox' );
add_filter( 'the_content', 'nano_idc_order_lightbox' );


/* ------------------------
 * Redirecting the backer after successful checkout to the project's single page
 *
------------------------------*/

function nano_checkout_redirect() {

	if ( isset( $_GET['paykey'] ) && isset( $_GET['mdid_checkout'] ) && isset( $_GET['idc_product'] ) && isset( $_SESSION['nano_project_id'] ) && ! isset( $_GET['view_receipt'] ) && ! isset( $_GET['nano_id'] ) ) {

		$defaut_project_id = function_exists( 'nano_get_default_project_id' ) ? nano_get_default_project_id() : '';

		// Project Details
		// ID of the project which was set on the project page.
		$nano_project_id = $_SESSION['nano_project_id'];
		$project_id      = 'token' == $_SESSION['order_type'] ? $nano_project_id : $defaut_project_id;

		// ID of the projects level (1 less than the project ID)
		$level_id  = nano_get_level_id( $nano_project_id );
		$post_id   = nano_post_id_by_order_level_id( $level_id );
		$permalink = get_permalink( $post_id );

		// User Details
		$current_user = wp_get_current_user();
		$user_id      = absint( $current_user->ID );

		// Transaction ID
		// We want to grab the strip transaction ID and use it here for the token ID for tracking.
		$order         = new ID_Member_Order( null, $user_id, ( $level_id ) );

		$last_order    = $order->get_last_order();
		$last_order_id = 'token' == $_SESSION['order_type'] ? $last_order->id : $last_order->id + 1;

		$url = $permalink . '?view_receipt=' . $last_order_id . '&nano_id';
		wp_redirect( $url );
		exit;

	}

}

add_action( 'wp_loaded', 'nano_checkout_redirect' );
/* ------------------------
 * Replacing IDC credits (TOKENS) system with our own so we can use decimals for amounts
 *
------------------------------*/

/**
 * This checks the data_type of the table and adjusts if it is not set to decimal
 */
function token_table_data_type() {

	global $wpdb;

	$table_name  = $wpdb->prefix . 'memberdeck_members';
	$column_name = 'credits';

	$sql      = "SELECT data_type FROM information_schema.columns WHERE table_name = '$table_name' AND column_name = %s";
	$sql_prep = $wpdb->prepare( $sql, $column_name );
	$res      = $wpdb->get_var( $sql_prep );

	if ( 'decimal' != $res ) {
		$result = $wpdb->query(
			"ALTER TABLE $table_name MODIFY $column_name DECIMAL(9,2)"
		);

		nano_debug_query( $result, '"SELECT data_type FROM information_schema.columns WHERE table_name = \'$table_name\' AND column_name = %s"', 'token_table_data_type' );
	}

}


/**
 * This replaces ID_Member::add_credits($user_id, $credit_count);
 *
 * @param $user_id
 * @param $credit_count
 */
function nano_add_credits( $user_id, $credit_count ) {
	//ensure data_type is decimal before proceeding
	token_table_data_type();

	global $wpdb;
	$member = new ID_Member();
	$match  = $member->match_user( $user_id );
	if ( ! empty( $match ) ) {
		$user_credits = $match->credits;
		$new_count    = $user_credits + $credit_count;
		$new_count    = round( $new_count, 2 );
		$new_count    = (string) $new_count;
		$id           = $match->id;
		$table_name   = $wpdb->prefix . 'memberdeck_members';
		$sql          = "UPDATE $table_name SET credits = %s WHERE id = %d";
		$sql_prep     = $wpdb->prepare( $sql, $new_count, $id );
		$res          = $wpdb->query( $sql_prep );

		nano_debug_query( $res, $sql, 'nano_add_credits' );
	}
}


/**
 * This replaces ID_Member::use_credits($user_id, $spent);
 *
 * @param $user_id
 * @param $spent
 */
function nano_use_credits( $user_id, $spent ) {
	//ensure data_type is decimal before proceeding
	token_table_data_type();

	$member = new ID_Member();
	$match  = $member->match_user( $user_id );
	if ( isset( $match ) ) {
		$credits = $match->credits;
		$sum     = $credits - $spent;
		$sum     = round( $sum, 2 );
		$sum     = (string) $sum;
		$spend   = nano_set_credits( $user_id, $sum );
	}
}

/**
 * This replaces ID_Member::set_credits($user_id, $sum);
 *
 * @param $user_id
 * @param $sum
 */
function nano_id_number_format( $number, $dec = 0, $dec_point = '.', $sep = ',' ) {
	if ( $number > 0 ) {
		$number = str_replace( ',', '', $number );
		$number = number_format( $number, $dec, $dec_point, $sep );
	}

	return $number;
}


/**
 * This replaces ID_Member::set_credits($user_id, $sum);
 *
 * @param $user_id
 * @param $sum
 */
function nano_set_credits( $user_id, $sum ) {
	global $wpdb;
	$sql = $wpdb->prepare( 'UPDATE ' . $wpdb->prefix . 'memberdeck_members SET credits = %s WHERE user_id = %d', $sum, $user_id );
	$res = $wpdb->query( $sql );

	nano_debug_query( $res, '\'UPDATE \' . $wpdb->prefix . \'memberdeck_members SET credits = %s WHERE user_id = %d\'', 'nano_set_credits' );
}

// Replaces idc_price_format()
add_action( 'init', 'wpse163434_init' );

/**
 * Replacing the built in IDK price formatter which is based in INT
 */
function wpse163434_init() {
	remove_filter( 'idc_price_format', 'idc_price_format', 10 );
	add_filter( 'idc_price_format', 'nano_idc_price_format', 20, 2 );
	remove_filter( 'id_number_format', 'id_number_format', 10 );
	add_filter( 'id_number_format', 'nano_id_number_format', 10, 4 );
}


/**
 * Our new decimal friendly price formatter.
 *
 * @param $amount
 * @param null $gateway
 *
 * @return string
 */
function nano_idc_price_format( $amount, $gateway = null ) {
	if ( 'BTC' == $gateway || 'coinbase' == $gateway ) {
		$amount = sprintf( '%f', (float) $amount );
	} elseif ( 'credit' !== $gateway && 'credits' !== $gateway ) {
		if ( $amount > 0 ) {
			$amount = number_format( preg_replace( '/[^0-9.]+/', '', $amount ), 2, '.', ',' );
		} else {
			$amount = '0.00';
		}
	} else {
		if ( $amount > 0 ) {
			$amount = number_format( $amount, 2 );
		} else {
			$amount = '0';
		}
	}

	return $amount;
}

// Replaces md_use_credit()
remove_action( 'wp_ajax_md_use_credit', 'md_use_credit' );
remove_action( 'wp_ajax_nopriv_md_use_credit', 'md_use_credit' );
add_action( 'wp_ajax_md_use_credit', 'nano_md_use_credit' );
add_action( 'wp_ajax_nopriv_md_use_credit', 'nano_md_use_credit' );

/**
 * Our credit function which uses decimal credits
 */
function nano_md_use_credit() {
	global $crowdfunding, $global_currency;
	$customer_id   = customer_id();
	$md_credits    = md_credits();
	$customer      = $_POST['Customer'];
	$fields        = ( isset( $_POST['Fields'] ) ? $_POST['Fields'] : '' );
	$product_id    = absint( esc_attr( $customer['product_id'] ) );
	$access_levels = array( $product_id );
	$level_data    = ID_Member_Level::get_level( $product_id );
	if ( 'recurring' == $level_data->level_type ) {
		// we need to return false here
		$error = __( 'Cannot use ' . strtolower( apply_filters( 'idc_credits_label', 'credits', true ) ) . ' to purchase recurring products', 'memberdeck' );
		print_r(
			json_encode(
				array(
					'response' => 'failure',
					'message'  => $error,
				)
			)
		);
		exit;
	} elseif ( 'lifetime' == $level_data->level_type ) {
		$e_date = null;
	} else {
		$e_date = idc_set_order_edate( $level_data );
	}
	$fname = esc_attr( $customer['first_name'] );
	$lname = esc_attr( $customer['last_name'] );
	if ( isset( $customer['email'] ) ) {
		$email = esc_attr( $customer['email'] );
	} else {
		// they have used 1cc or some other mechanism and we don't have their email
		if ( is_user_logged_in() ) {
			$current_user = wp_get_current_user();
			$email        = $current_user->user_email;
		}
	}
	$member     = new ID_Member();
	$check_user = $member->check_user( $email );
	if ( ! empty( $check_user ) ) {
		if ( $md_credits >= $level_data->credit_value ) {
			$user_id    = $check_user->ID;
			$match_user = $member->match_user( $user_id );
			if ( ! empty( $match_user ) ) {
				// this user already exists within MemberDeck
				$txn_id = 'credit';
				if ( isset( $match_user->access_level ) ) {
					// let's combine levels
					$old_levels = unserialize( $match_user->access_level );
					if ( is_array( $old_levels ) ) {
						foreach ( $old_levels as $key['val'] ) {
							$access_levels[] = $key['val'];
						}
					}
				}
				if ( isset( $match_user->data ) ) {
					// let's combine data
					$old_data = unserialize( $match_user->data );
					// do we need any data for credit purchases?
					//$old_data[] =  array('customer_id' => $custid);
				} else {
					$old_data = array();
				}
				$paykey = md5( $email . time() );
				// price is 0 because they are using a credit
				/*if ($global_currency == 'credit') {
					$price = $level_data->credit_value;
				}*/
				$order     = new ID_Member_Order( null, $user_id, $product_id, null, $txn_id, '', 'active', $e_date, '0' );
				$new_order = $order->add_order();
				MD_Keys::set_licenses( $user_id, $product_id );
				$user = array(
					'user_id' => $user_id,
					'level'   => $access_levels,
					'data'    => $old_data,
				);
				ID_Member::update_user( $user );
				if ( isset( $_POST['PWYW'] ) ) {
					$pwyw_price = esc_attr( $_POST['PWYW'] );
					if ( $pwyw_price > $level_data->credit_value ) {
						$level_data->credit_value = $pwyw_price;
					}
					ID_Member_Order::update_order_meta( $new_order, 'pwyw_price', $pwyw_price );
				}
				nano_use_credits( $user_id, $level_data->credit_value );

				// don't send receipt for now
				//do_action('memberdeck_credit_receipt', $user_id, $level_data->credit_value);
				do_action( 'memberdeck_payment_success', $user_id, $new_order, $paykey, $fields, 'credit' );

				if ( $crowdfunding ) {
					//echo 'order: '.$new_order;
					if ( isset( $_POST['Fields'] ) ) {
						//echo 'isset post fields';
						$fields = $_POST['Fields'];
						if ( is_array( $fields ) ) {
							foreach ( $fields as $field ) {
								if ( 'project_id' == $field['name'] ) {
									$project_id = $field['value'];
								} elseif ( 'project_level' == $field['name'] ) {
									$proj_level = $field['value'];
								}
							}
						}
						if ( isset( $project_id ) && isset( $proj_level ) ) {
							$price = apply_filters( 'id_idc_pwyw_price', $level_data->level_price, $level_data->credit_value, $pwyw_price );
							//send_error( $price );
							if ( isset( $new_order ) ) {
								//echo $new_order;
								$order = new ID_Member_Order( $new_order );
								//print_r($order);
								//$the_order = $order->get_order();
								$created_at = $order->order_date;
							} else {
								$created_at = nano_get_formated_date_time_gmt( 'Y-m-d H:i:s' );
							}
							$status = 'C';
							// Setting price/credits based on the global currency display
							// if (!empty($global_currency) && $global_currency == "credits") {
							// 	$pay_id = mdid_insert_payinfo($fname, $lname, $email, $project_id, $txn_id, $proj_level, $level_data->credit_value, $status, $created_at);
							// } else {
							$pay_id = mdid_insert_payinfo( $fname, $lname, $email, $project_id, $txn_id, $proj_level, $price, $status, $created_at );

							//send_error( $pay_id );
							// }
							// now need to insert mdid order
							if ( isset( $pay_id ) ) {
								$mdid_id = mdid_insert_order( null, $pay_id, $new_order, null );
								//send_error( $mdid_id );
								do_action( 'id_payment_success', $pay_id );
							}
						}
					}
				}
				print_r(
					json_encode(
						array(
							'response'    => 'success',
							'product'     => $product_id,
							'paykey'      => $paykey,
							'customer_id' => null,
							'user_id'     => $user_id,
							'order_id'    => $new_order,
							'type'        => 'credit',
						)
					)
				);
			} else {
				$error = __( 'This user is not a memberdeck user', 'memberdeck' ) . ' ' . __LINE__;
				print_r(
					json_encode(
						array(
							'response' => 'failure',
							'message'  => $error,
						)
					)
				);
			}
		} else {
			$error = __( 'You do not have enough credits to complete this transaction', 'memberdeck' ) . ' ' . __LINE__;
			print_r(
				json_encode(
					array(
						'response' => 'failure',
						'message'  => $error,
					)
				)
			);
		}
	} else {
		$error = __( 'User was not found', 'memberdeck' ) . ' ' . __LINE__;
		print_r(
			json_encode(
				array(
					'response' => 'failure',
					'message'  => $error,
				)
			)
		);
	}
	exit;
}
