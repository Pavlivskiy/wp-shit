<?php

if ( ! defined( 'ABSPATH' ) ) {
	die( 'Direct access forbidden.' );
}


/**
 * Sanitize all inputs for the form
 *
 * @param string $name The name of the post input.
 *
 * @return string $val The sanitized input.
 */
function nano_sanitize_post_text_field( $name ) {
	$val = '';
	if ( ! empty( $_POST[ $name ] ) ) { // Input var okay.
		$val = sanitize_text_field( wp_unslash( $_POST[ $name ] ) ); // Input var okay.
	}

	return $val;
}


/**
 * Creating a stripe connect custom user & associating them to the website and their website account
 */
function nano_stripe_connect_custom() {

	// First check the nonce, if it fails the function will break.
	check_ajax_referer( 'nano_stripe_connect_secure', 'security' );

	if ( is_user_logged_in() ) {

		if ( ! current_user_can( 'create_edit_projects' ) ) {
			return;
		}

		// Create Stripe Account.
		$settings     = get_option( 'memberdeck_gateways' );
		$current_user = wp_get_current_user();
		$user_id      = $current_user->ID;

		if ( ! empty( $settings ) ) {
			if ( is_array( $settings ) ) {
				$test = $settings['test'];
				if ( 1 === $test ) {
					$key = $settings['tsk'];
				} else {
					$key = $settings['sk'];
				}
			}
		}

		$error = '';

		if ( ! class_exists( 'Stripe\Stripe' ) ) {
			require_once IDC_PATH . 'lib/stripe-php-4.2.0/init.php';
		}

		try {

			$legal_entity_business = ! empty( $_POST['business_name'] ) || ! empty( $_POST['business_tax_id'] ) ? array( // Input var okay.
				'business_name'   => nano_sanitize_post_text_field( 'business_name' ),
				'business_tax_id' => nano_sanitize_post_text_field( 'biz_tax_id_no' ),
			) : array();

			$legal_entity_standard = array(
				'address'    => array(
					'city'        => nano_sanitize_post_text_field( 'city' ),
					'country'     => 'US',
					'line1'       => nano_sanitize_post_text_field( 'line1' ),
					'postal_code' => nano_sanitize_post_text_field( 'postal_code' ),
					'state'       => nano_sanitize_post_text_field( 'state' ),
				),
				'dob'        => array(
					'day'   => nano_sanitize_post_text_field( 'day' ),
					'month' => nano_sanitize_post_text_field( 'month' ),
					'year'  => nano_sanitize_post_text_field( 'year' ),
				),
				'first_name' => nano_sanitize_post_text_field( 'first_name' ),
				'last_name'  => nano_sanitize_post_text_field( 'last_name' ),
				'ssn_last_4' => nano_sanitize_post_text_field( 'ssn_last_4' ),
				'type'       => nano_sanitize_post_text_field( 'type' ),
			);

			$legal_entity = array_merge( $legal_entity_business, $legal_entity_standard );


			// Set the API Version & Key.
			\Stripe\Stripe::setApiVersion( NANO_STRIPE_API_VERSION );
			\Stripe\Stripe::setApiKey( $key );

			$account_array = array(
				'type'             => 'custom',
				'country'          => 'US',
				'default_currency' => 'USD',
				// Input var okay.
				'metadata'         => array(
					'nano_user_id' => $user_id,
				),
				'legal_entity'     => $legal_entity,
				'tos_acceptance'   => array(
					'date' => time(),
					'ip'   => $_SERVER['REMOTE_ADDR'], // Input var okay.
				),
				'payout_schedule'  => array(
					'delay_days' => 14,
					'interval'   => 'daily',
				),
				'external_account' => sanitize_text_field( $_POST['external_account'] ),
			);

			// Add in the business URL if filled in, otherwise ignore
			if ( isset( $_POST['business_url'] ) && '' != $_POST['business_url'] ) {
				$account_array['business_url'] = esc_url_raw( $_POST['business_url'] );
			}

			$account = \Stripe\Account::create(
				$account_array
			);

			$status = true;
			$error  = '';
		} catch ( \Stripe\Error\Card $e ) {
			// Since it's a decline, \Stripe\Error\Card will be caught.
			stripe_error_email_admin_notification( $e, 'nano_stripe_connect_custom' );
		} catch ( \Stripe\Error\RateLimit $e ) {
			// Too many requests made to the API too quickly.
			$body  = $e->getJsonBody();
			$err   = $body['error'];
			$error = 'Status ' . $e->getHttpStatus() . '. ' . $err['message'];
		} catch ( \Stripe\Error\InvalidRequest $e ) {
			// Invalid parameters were supplied to Stripe's API.
			$body  = $e->getJsonBody();
			$err   = $body['error'];
			$error = 'Status ' . $e->getHttpStatus() . '. ' . $err['message'];
		} catch ( \Stripe\Error\Authentication $e ) {
			// Authentication with Stripe's API failed.
			// (maybe you changed API keys recently).
			$body  = $e->getJsonBody();
			$err   = $body['error'];
			$error = 'Status ' . $e->getHttpStatus() . '. ' . $err['message'];
		} catch ( \Stripe\Error\ApiConnection $e ) {
			// Network communication with Stripe failed.
			$body  = $e->getJsonBody();
			$err   = $body['error'];
			$error = 'Status ' . $e->getHttpStatus() . '. ' . $err['message'];
		} catch ( \Stripe\Error\Base $e ) {
			// Display a very generic error to the user, and maybe send.
			// yourself an email.
			$body  = $e->getJsonBody();
			$err   = $body['error'];
			$error = 'Status ' . $e->getHttpStatus() . '. ' . $err['message'];
		} catch ( Exception $e ) {
			// Something else happened, completely unrelated to Stripe.
			$error = 'Exception ' . $e;
		}

		if ( ! isset( $status ) ) {
			echo json_encode(
				array(
					'stripeError' => true,
					'message'     => $error,
				)
			);
			stripe_error_email_admin_notification( $e, 'nano_stripe_connect_custom' );
			exit();
		} else {
			echo json_encode(
				array(
					'stripeError' => false,
					'message'     => $error,
				)
			);
			//Create Connected Account on website.
			$current_user = wp_get_current_user();
			$user_id      = $current_user->ID;
			$check_creds  = md_sc_creds( $user_id );
			if ( empty( $check_creds ) ) {
				$access_token           = $key;
				$refresh_token          = '';
				$stripe_publishable_key = '';
				$stripe_user_id         = $account->id;
				$params                 = array(
					'access_token'           => $access_token,
					'refresh_token'          => $refresh_token,
					'stripe_publishable_key' => $stripe_publishable_key,
					'stripe_user_id'         => $stripe_user_id,
				);
				$insert_id              = nano_save_sc_params( $user_id, $params );
				if ( $insert_id > 0 ) {
					do_action( 'idc_stripe_connect_success', $insert_id, $user_id );
					save_user_geo_location(); // Appending the user's geolocation.
				}
			}
		}
	}

	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		wp_die();
	}

}


/**
 * Updating a user's Stripe account info
 *
 *
 */

function nano_stripe_connect_update() {

	// First check the nonce, if it fails the function will break
	check_ajax_referer( 'nano_stripe_connect_secure', 'security' );

	if ( is_user_logged_in() ) {

		if ( ! current_user_can( 'create_edit_projects' ) ) {
			return;
		}

		$current_user = wp_get_current_user();
		$user_id      = $current_user->ID;
		$params       = get_sc_params( $user_id );

		if ( ! empty( $params ) ) {
			if ( ! class_exists( 'Stripe\Stripe' ) ) {
				require_once IDC_PATH . 'lib/stripe-php-4.2.0/init.php';
			}
			$api_key        = $params->access_token;
			$stripe_user_id = $params->stripe_user_id;

			try {

				$error  = 'Account Updated.';
				$status = 'success';

				$disputed = '';
				if ( isset( $_FILES['identity_document'] ) && $_FILES['identity_document']['size'] > 0 ) {
					$disputed = wp_handle_upload( $_FILES['identity_document'], array( 'test_form' => false ) );
				}

				// Set the API Version & Key
				\Stripe\Stripe::setApiVersion( NANO_STRIPE_API_VERSION );
				\Stripe\Stripe::setApiKey( $api_key );

				if ( isset( $_FILES['identity_document'] ) ) {
					$disputed_filetype = wp_check_filetype( basename( $disputed['file'] ), null );
					if ( strtolower( 'png' ) === $disputed_filetype['ext'] || strtolower( 'jpg' ) === $disputed_filetype['ext'] || strtolower( 'jpeg' ) === $disputed_filetype['ext'] ) {

						$strip_identity_doc = \Stripe\FileUpload::create(
							array(
								'purpose' => 'identity_document',
								'file'    => fopen( $disputed['file'], 'r' ),
							),
							array( 'stripe_account' => $stripe_user_id )
						);
						$error              = 'Your identity document has been successfully sent for review. We will send you an email once Stripe has reviewed your upload or you can check back on this page to check the status.';
						$status             = 'identity_doc_required';
					} else {
						$error  = 'Sorry, you need to upload a PNG or JPG file only';
						$status = 'identity_doc_required';
					}
				}

				// Get the user's account
				$stripe_account = \Stripe\Account::retrieve( $stripe_user_id );

				// Clean up the supplied php array.
				$sc_params = $stripe_account->__toJSON();
				$sc_params = json_decode( $sc_params );

				$bank_base = $sc_params->external_accounts->data;

				// Prepare for bank account updating if required.
				$account_number_old = $sc_params ? $bank_base[0]->last4 : '';
				$account_number_new = strlen( (string) $_POST['account_number'] ) >= 4 ? substr( (string) $_POST['account_number'], - 4 ) : '';

				$stripe_user_account = array(
					'business_name'     => ( isset( $sc_params->legal_entity->business_name ) ? $sc_params->legal_entity->business_name : '' ),
					'business_url'      => ( $sc_params ? $sc_params->business_url : '' ),
					'city'              => ( $sc_params ? $sc_params->legal_entity->address->city : '' ),
					'line1'             => ( $sc_params ? $sc_params->legal_entity->address->line1 : '' ),
					'postal_code'       => ( $sc_params ? $sc_params->legal_entity->address->postal_code : '' ),
					'state'             => ( $sc_params ? $sc_params->legal_entity->address->state : '' ),
					'business_tax_id'   => ( isset( $sc_params->legal_entity->business_tax_id_provided ) ? $sc_params->legal_entity->business_tax_id_provided : '' ),
					'day'               => ( $sc_params ? $sc_params->legal_entity->dob->day : '' ),
					'month'             => ( $sc_params ? $sc_params->legal_entity->dob->month : '' ),
					'year'              => ( $sc_params ? $sc_params->legal_entity->dob->year : '' ),
					'first_name'        => ( $sc_params ? $sc_params->legal_entity->first_name : '' ),
					'last_name'         => ( $sc_params ? $sc_params->legal_entity->last_name : '' ),
					'ssn_last_4'        => '',
					'type'              => ( $sc_params ? $sc_params->legal_entity->type : '' ),
					'routing_number'    => ( $sc_params ? $bank_base[0]->routing_number : '' ),
					'account_number'    => $account_number_old,
					'external_account'  => '',
					'identity_document' => '',
				);

				$updated_account_info = array(
					'city'           => $_POST['city'],
					'line1'          => $_POST['line1'],
					'postal_code'    => $_POST['postal_code'],
					'state'          => $_POST['state'],
					'day'            => $_POST['day'],
					'month'          => $_POST['month'],
					'year'           => $_POST['year'],
					'first_name'     => $_POST['first_name'],
					'last_name'      => $_POST['last_name'],
					'ssn_last_4'     => $_POST['ssn_last_4'],
					'type'           => $_POST['type'],
					'routing_number' => $_POST['routing_number'],
				);

				// Checking for updated values, if there include, if not do not submit
				$updated_account_info['business_name']     = isset( $_POST['business_name'] ) && '' != $_POST['business_name'] ? $_POST['business_name'] : '';
				$updated_account_info['business_url']      = isset( $_POST['business_url'] ) && '' != $_POST['business_url'] ? $_POST['business_url'] : '';
				$updated_account_info['business_tax_id']   = isset( $_POST['business_tax_id'] ) && '' != $_POST['business_tax_id'] ? $_POST['business_tax_id'] : '';
				$updated_account_info['account_number']    = isset( $_POST['account_number'] ) && '' != $_POST['account_number'] ? $_POST['account_number'] : '';
				$updated_account_info['external_account']  = isset( $_POST['external_account'] ) && '' != $_POST['external_account'] ? $_POST['external_account'] : '';
				$updated_account_info['identity_document'] = isset( $strip_identity_doc ) ? $strip_identity_doc->id : '';

				$result = array_diff( $updated_account_info, $stripe_user_account );

				if ( count( $result ) > 0 ) {

					foreach ( $result as $key_val => $val ) {

						if ( 'business_name' == $key_val ) {
							if ( 'undefined' != $val ) {
								$stripe_account->legal_entity->business_name = $val;
							}
						}
						if ( 'business_url' == $key_val ) {
							$stripe_account->business_url = $val;
						}
						if ( 'city' == $key_val ) {
							$stripe_account->legal_entity->address->city = $val;
						}
						if ( 'line1' == $key_val ) {
							$stripe_account->legal_entity->address->line1 = $val;
						}
						if ( 'postal_code' == $key_val ) {
							$stripe_account->legal_entity->address->postal_code = $val;
						}
						if ( 'state' == $key_val ) {
							$stripe_account->legal_entity->address->state = $val;
						}
						if ( 'business_tax_id' == $key_val ) {
							if ( 'undefined' != $val ) {
								$stripe_account->legal_entity->business_tax_id = $val;
							}
						}
						if ( 'external_account' == $key_val ) {
							$stripe_account->external_account = $val;
						}
						if ( 'first_name' == $key_val ) {
							$stripe_account->legal_entity->first_name = $val;
						}
						if ( 'last_name' == $key_val ) {
							$stripe_account->legal_entity->last_name = $val;
						}
						if ( 'ssn_last_4' == $key_val ) {
							//$stripe_account->legal_entity->ssn_last_4 = $val;
							$stripe_account->legal_entity->personal_id_number = $val;
						}
						if ( 'identity_document' == $key_val ) {
							$stripe_account->legal_entity->verification->document = $val;
						}
						if ( 'day' == $key_val ) {
							$stripe_account->legal_entity->dob->day   = $val;
							$stripe_account->legal_entity->dob->month = $updated_account_info['month'];
							$stripe_account->legal_entity->dob->year  = $updated_account_info['year'];
						}
						if ( 'month' == $key_val ) {
							$stripe_account->legal_entity->dob->day   = $updated_account_info['day'];
							$stripe_account->legal_entity->dob->month = $val;
							$stripe_account->legal_entity->dob->year  = $updated_account_info['year'];
						}
						if ( 'year' == $key_val ) {
							$stripe_account->legal_entity->dob->day   = $updated_account_info['day'];
							$stripe_account->legal_entity->dob->month = $updated_account_info['month'];
							$stripe_account->legal_entity->dob->year  = $val;
						}
						if ( 'verified' == $stripe_account->legal_entity->verification->status ) {
							if ( in_array( $key_val, array(
									'day',
									'month',
									'year',
									'ssn_last_4',
									'first_name',
									'last_name'
								) ) == $key_val ) {
								$prop = '';
								if ( in_array( $key_val, array( 'day', 'month', 'year' ) ) ) {
									$prop = 'date of birth';
								}
								if ( 'ssn_last_4' == $key_val ) {
									$prop = 'SSN number';
								}
								if ( in_array( $key_val, array( 'first_name', 'last_name' ) ) ) {
									$prop = 'name';
								}
								if ( 'external_account' == $key_val ) {
									$prop = 'bank account';
								}
								$error  = 'Sorry your ' . $prop . ' has already been verified. These settings cannot be changed without contacting us directly.';
								$status = 'already_verified';
							}
						}
						if ( 'type' == $key_val ) {
							$stripe_account->legal_entity->type = $val;
						}
					}

					$stripe_account->save();
					save_user_geo_location(); // Appending the user's geolocation

				} else {
					$error  = 'No changes detected.';
					$status = 'already_verified';
				}
			} catch ( \Stripe\Error\Card $e ) {
				// Since it's a decline, \Stripe\Error\Card will be caught.
				stripe_error_email_admin_notification( $e, 'nano_stripe_connect_update' );
			} catch ( \Stripe\Error\RateLimit $e ) {
				// Too many requests made to the API too quickly.
				$body  = $e->getJsonBody();
				$err   = $body['error'];
				$error = 'Status ' . $e->getHttpStatus() . '. ' . $err['message'];
			} catch ( \Stripe\Error\InvalidRequest $e ) {
				// Invalid parameters were supplied to Stripe's API.
				$body  = $e->getJsonBody();
				$err   = $body['error'];
				$error = 'Status ' . $e->getHttpStatus() . '. ' . $err['message'];
			} catch ( \Stripe\Error\Authentication $e ) {
				// Authentication with Stripe's API failed.
				// (maybe you changed API keys recently).
				$body  = $e->getJsonBody();
				$err   = $body['error'];
				$error = 'Status ' . $e->getHttpStatus() . '. ' . $err['message'];
			} catch ( \Stripe\Error\ApiConnection $e ) {
				// Network communication with Stripe failed.
				$body  = $e->getJsonBody();
				$err   = $body['error'];
				$error = 'Status ' . $e->getHttpStatus() . '. ' . $err['message'];
			} catch ( \Stripe\Error\Base $e ) {
				// Display a very generic error to the user, and maybe send.
				// yourself an email.
				$body  = $e->getJsonBody();
				$err   = $body['error'];
				$error = 'Status ' . $e->getHttpStatus() . '. ' . $err['message'];
			} catch ( Exception $e ) {
				// Something else happened, completely unrelated to Stripe.
				$error = 'Exception ' . $e;
			}

			echo json_encode(
				array(
					'stripeError' => $status,
					'message'     => $error,
				)
			);

			exit();

		}
	}

	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		wp_die();
	}

}


/**
 * Save new user to DB if successfully added to Stripe
 *
 * @param string $user_id The current user's WP ID
 * @param stdClass $params The platform's Stripe parameters
 *
 * @return string $val The user's insert ID
 *
 */

function nano_save_sc_params( $user_id, $params ) {

	$val = null;

	if ( ! empty( $params ) && $user_id > 0 ) {
		global $wpdb;
		$access_token           = $params['access_token'];
		$refresh_token          = $params['refresh_token'];
		$stripe_publishable_key = $params['stripe_publishable_key'];
		$stripe_user_id         = $params['stripe_user_id'];
		$oldparams              = get_sc_params( $user_id );
		$table_name             = $wpdb->prefix . 'memberdeck_sc_params';
		if ( empty( $oldparams ) ) {
			$original = '';
			if ( empty( $original ) ) {
				//echo '1';
				// send_error( '01' );

				$sql_query = "INSERT INTO $table_name (user_id, access_token, refresh_token, stripe_publishable_key, stripe_user_id) VALUES (%d, %s, %s, %s, %s)";
				$sql       = $wpdb->prepare( $sql_query, $user_id, $access_token, $refresh_token, $stripe_publishable_key, $stripe_user_id );
				$result = $wpdb->query( $sql );

				nano_debug_query( $result, $sql_query, 'nano_save_sc_params' );

				$insert_id = $wpdb->insert_id;
				if ( $insert_id > 0 ) {
					//echo '2';
					// send_error( '02' );
					$val = $insert_id;
				} else {
					//echo '3';
					// send_error( '03' );
					$val = null;
				}
			} else {
				//echo '4';
				// send_error( '04' );
				$val = $original;
			}
		} else {
			$valid_params = validate_sc_params( $user_id );
			if ( $valid_params ) {
				//echo '5';
				// send_error( '05' );
				$val = $params->id;
			} else {
				delete_sc_params( $user_id );
				$sql_query = "INSERT INTO '$table_name' (user_id, access_token, refresh_token, stripe_publishable_key, stripe_user_id) VALUES (%d, %s, %s, %s, %s)";
				$sql       = $wpdb->prepare( $sql_query, $user_id, $access_token, $refresh_token, $stripe_publishable_key, $stripe_user_id );
				$wpdb->query( $sql );
				$insert_id = $wpdb->insert_id;
				if ( $insert_id > 0 ) {
					//echo '6';
					// send_error( '07' );
					$val = $insert_id;
				} else {
					//echo '7';
					//send_error( '07' );
					$val = null;
				}
			}
		}
	}

	// send_error( $val );

	return $val;

}


/**
 * Get / Return creator's Stripe credentials if they exist
 *
 * @param string $user_id
 *
 * @return array
 *
 */

function nano_get_sc_params( $user_id ) {

	$params = get_sc_params( $user_id );

	$sc_params = null;

	if ( ! empty( $params ) ) {

		if ( ! class_exists( 'Stripe\Stripe' ) ) {
			require_once IDC_PATH . 'lib/stripe-php-4.2.0/init.php';
		}

		$api_key        = $params->access_token;
		$stripe_user_id = $params->stripe_user_id;

		// Set the API Version & Key
		\Stripe\Stripe::setApiVersion( NANO_STRIPE_API_VERSION );
		\Stripe\Stripe::setApiKey( $api_key );

		try {
			$stripe_account = \Stripe\Account::retrieve( $stripe_user_id );
			$sc_params      = $stripe_account->__toJSON();
			$sc_params      = json_decode( $sc_params );
		} catch ( Exception $e ) {
			$sc_params = null;
		}
	}

	return $sc_params;

}


function nano_get_sc_params_by_user_account( $stripe_user_account ) {

	$sc_params = null;

		if ( ! class_exists( 'Stripe\Stripe' ) ) {
			require_once IDC_PATH . 'lib/stripe-php-4.2.0/init.php';
		}

		$api_key        = 'sk_test_zZoBVh8OPyutDRUvYTON9alq';

		// Set the API Version & Key
		\Stripe\Stripe::setApiVersion( NANO_STRIPE_API_VERSION );
		\Stripe\Stripe::setApiKey( $api_key );

		try {
			$stripe_account = \Stripe\Account::retrieve( $stripe_user_account );
			$sc_params      = $stripe_account->__toJSON();
			$sc_params      = json_decode( $sc_params );
		} catch ( Exception $e ) {
			$sc_params = null;
		}


	return $sc_params;

}


/**
 * Get / Return platforms' Stripe credentials
 *
 *
 */

function get_master_sc_params() {

	$settings = get_option( 'memberdeck_gateways' );

	if ( ! empty( $settings ) ) {
		if ( is_array( $settings ) ) {
			$test = $settings['test'];
			if ( 1 == $test ) {
				$api_key = $settings['tsk'];
			} else {
				$api_key = $settings['sk'];
			}
		}
	}

	if ( ! class_exists( 'Stripe' ) ) {
		require_once IDC_PATH . 'lib/stripe-php-4.2.0/init.php';
	}
	try {
		// Set the API Version & Key
		\Stripe\Stripe::setApiVersion( NANO_STRIPE_API_VERSION );
		\Stripe\Stripe::setApiKey( $api_key );

		$stripe_account = \Stripe\Account::retrieve();
		$sc_params      = $stripe_account->__toJSON();
		$sc_params      = json_decode( $sc_params );
	} catch ( Exception $e ) {
		$sc_params = $e;
	}

	return $sc_params;
}


/**
 * Get customer's Stripe credentials if they exist
 *
 * @param string $user_id
 *
 * @return array
 *
 */

function nano_get_stripe_customer_params( $user_id ) {

	$settings = get_option( 'memberdeck_gateways' );

	if ( ! empty( $settings ) ) {
		if ( is_array( $settings ) ) {
			$test = $settings['test'];
			if ( 1 == $test ) {
				$api_key = $settings['tsk'];
			} else {
				$api_key = $settings['sk'];
			}
		}
	}

	$customer_id = get_user_meta( $user_id, 'stripe_customer_id', true );

	$sc_params = null;

	if ( ! empty( $customer_id ) ) {
		if ( ! class_exists( 'Stripe\Stripe' ) ) {
			require_once IDC_PATH . 'lib/stripe-php-4.2.0/init.php';
		}

		try {
			\Stripe\Stripe::setApiKey( $api_key );
			$stripe_account = \Stripe\Customer::retrieve( $customer_id );
			$sc_params      = $stripe_account->__toJSON();
			$sc_params      = json_decode( $sc_params );
		} catch ( Exception $e ) {

			$sc_params = null;
		}
	}

	return $sc_params;

}


/**
 * When the admin chooses the accounts to deposit and debit to, we should set them up for negative balances allowance
 *
 * @param string $account_id
 *
 */

function admin_connect_account_update( $account_id, $update ) {

	$settings = get_option( 'memberdeck_gateways' );

	$dev     = false;
	$message = 'There was a problem, please check your account ID again';

	if ( ! empty( $settings ) ) {
		if ( is_array( $settings ) ) {
			$test = $settings['test'];
			if ( 1 == $test ) {
				$key = $settings['tsk'];
				$dev = true;
			} else {
				$key = $settings['sk'];
				$dev = false;
			}
		}
	}

	if ( ! class_exists( 'Stripe\Stripe' ) ) {
		require_once IDC_PATH . 'lib/stripe-php-4.2.0/init.php';
	}

	\Stripe\Stripe::setApiKey( $key );

	try {

		$account = \Stripe\Account::retrieve( $account_id );
		$status  = true;

	} catch ( \Stripe\Error\Card $e ) {
		// Since it's a decline, \Stripe\Error\Card will be caught
		$status = false;
	} catch ( \Stripe\Error\RateLimit $e ) {
		// Too many requests made to the API too quickly
		$status = false;
		$body   = $e->getJsonBody();
		$err    = $body['error'];
		$error  = 'Status ' . $e->getHttpStatus() . '. ' . $err['message'];
	} catch ( \Stripe\Error\InvalidRequest $e ) {
		// Invalid parameters were supplied to Stripe's API
		$status = false;
		$body   = $e->getJsonBody();
		$err    = $body['error'];
		$error  = 'Status ' . $e->getHttpStatus() . '. ' . $err['message'];
	} catch ( \Stripe\Error\Authentication $e ) {
		// Authentication with Stripe's API failed
		// (maybe you changed API keys recently)
		$status = false;
		$body   = $e->getJsonBody();
		$err    = $body['error'];
		$error  = 'Status ' . $e->getHttpStatus() . '. ' . $err['message'];
	} catch ( \Stripe\Error\ApiConnection $e ) {
		// Network communication with Stripe failed
		$status = false;
		$body   = $e->getJsonBody();
		$err    = $body['error'];
		$error  = 'Status ' . $e->getHttpStatus() . '. ' . $err['message'];
	} catch ( \Stripe\Error\Base $e ) {
		// Display a very generic error to the user, and maybe send
		// yourself an email
		$status = false;
		$body   = $e->getJsonBody();
		$err    = $body['error'];
		$error  = 'Status ' . $e->getHttpStatus() . '. ' . $err['message'];
	} catch ( Exception $e ) {
		// Something else happened, completely unrelated to Stripe
		$status = false;
		$error  = 'Exception ' . $e;
	}

	if ( false === $status ) {
		echo json_encode(
			array(
				'stripeError' => true,
				'message'     => $error,
			)
		);
		stripe_error_email_admin_notification( $e, 'admin_connect_account_update' );
		send_error( $error );

	} elseif ( true === $status ) {

		$check_acct   = $account->__toJSON();
		$check_acct   = json_decode( $check_acct );
		$bank_base    = $check_acct->external_accounts->data;
		$test_account = 'STRIPE TEST BANK' === $bank_base[0]->bank_name ? 'test_acct' : 'live_acct';

		if ( true === $dev && 'test_acct' === $test_account ) {

			if ( true === $update ) {

				$account->debit_negative_balances = true;
				$account->save();

			}

			$message = '';

		} elseif ( false === $dev && 'live_acct' === $test_account ) {

			if ( true === $update ) {

				$account->debit_negative_balances = true;
				$account->save();

			}

			$message = '';

		} elseif ( true === $dev && 'live_acct' === $test_account ) {

			$message = 'Sorry, the account number ' . $account_id . ' was not saved. It looks like you were using a LIVE CONNECT account number here while this system is in TEST mode.';

		} elseif ( false === $dev && 'test_acct' === $test_account ) {

			$message = 'Sorry, the account number ' . $account_id . ' was not saved. It looks like you were using a TEST CONNECT account number here while this system is in LIVE mode.';

		}

	}

	return $message;

}


/**
 * Ajax Fund Transfer for successful projects
 * This is step 1 to initiate the payment intended for the creator.
 * Processing button is located in the project's sidebar metabox under "Payout this project"
 *
 */

function nano_stripe_creator_payment_ajax() {

	// First check the nonce, if it fails the function will break
	check_ajax_referer( 'nano_project_payout_meta', 'security' );

	if ( true === nanosteam_privledge_check() ) {

		if ( ! current_user_can( 'administrator' ) ) {
			return;
		}

		$post_id = $_POST['post_id'];

		// For FEES
		$level_id = nano_get_level_id( get_post_meta( $post_id, 'ign_project_id', true ) );
		$orders   = ID_Member_Order::get_orders_by_level( $level_id );

		// FEES & TOTALS
		$fees              = nano_fee_list( $orders, $post_id );
		$total_fund_raised = $fees['total_before_fees'];
		$stripe_fees       = $fees['stripe_fees'];
		$nanosteam_fees    = $fees['nanosteam_fees'];
		$total_fees        = $fees['total_fees'];
		$total_after_fees  = $fees['total_after_fees'];

		// Payout Rule Check :: the last step before we begin the Stripe transfer from the Nanosteam Pool to the Creator.
		$first_transfer_processed_status    = get_post_meta( $post_id, 'first_transfer_transaction_id', true ) != '' ? true : false;
		$final_transfer_processed_status    = get_post_meta( $post_id, 'final_transfer_transaction_id', true ) != '' ? true : false;
		$standard_transfer_processed_status = get_post_meta( $post_id, 'standard_transfer_transaction_id', true ) != '' ? true : false;
		$project_success_status             = get_post_meta( $post_id, 'ign_project_success', true ) == '1' ? true : false;
		$project_fund_goal                  = get_post_meta( $post_id, 'ign_fund_goal', true );
		$project_fund_raised                = $total_fund_raised;
		$project_fund_status                = absint( $project_fund_raised ) >= absint( $project_fund_goal ) ? true : false;
		$matching_grant_project_check       = get_post_meta( $post_id, 'fifty_fifty_grant', true ) == 'approved' ? true : false;
		$second_milestone_check             = nano_second_milsetone_achieved( $post_id );
		$original_author                    = get_user_by( 'login', get_post_meta( $post_id, 'original_author', true ) ) ? get_user_by( 'login', get_post_meta( $post_id, 'original_author', true ) ) : '';
		$creator_id                         = $original_author->ID;

		// Proceed if the project fundraising effors are noted as successful and double check that funds raised are great
		// or equal to the fundraising goal
		if ( true == $project_success_status && true == $project_fund_status ) {

			if ( ! class_exists( 'Stripe\Stripe' ) ) {
				require_once IDC_PATH . 'lib/stripe-php-4.2.0/init.php';
			}

			try {

				//Project Parameters
				$project_id     = get_post_meta( $post_id, 'project_uid', true );
				$ign_project_id = get_post_meta( $post_id, 'ign_project_id', true );
				$project_name   = nano_get_project_name( $ign_project_id );
				$project_url    = get_permalink( $post_id );

				// Creator Parameters
				$user_parameters     = nano_get_sc_params( $creator_id );
				$user_stripe_account = $user_parameters->id;

				// Platform Account Parameters
				$master_parameters     = get_master_sc_params();
				$master_stripe_account = $master_parameters->id;

				// Admin Debit Account
				$stripe_withdrawl_account = get_option( 'nano_debit_account' );

				$amount = true == $matching_grant_project_check ? ( round( $total_fund_raised / 2 ) ) - $total_fees : $total_fund_raised - $total_fees;
				$amount = number_format( $amount, 2, '.', '' ); // Need to make this an 2 decimal number just in case the division a large decimal count

				// Fees and Totals based on whether this is the first or last matching grant
				if ( true != $first_transfer_processed_status && true == $matching_grant_project_check && true != $second_milestone_check ) {

					$total   = (int) ( ( $amount ) * 100 ); //multiplying by 100 for Stripe and converting it an integer
					$message = 'First Transfer for matching grant project ID: ' . $project_id;

				} elseif ( true == $first_transfer_processed_status && true != $final_transfer_processed_status && true == $matching_grant_project_check && true == $second_milestone_check ) {

					$total      = (int) ( ( $amount + $total_fees ) * 100 ); //multiplying by 100 for Stripe and converting it an integer
					$total_fees = 'The entire fee of $' . $total_fees . ' was processed in first transfer';
					$message    = 'Final Transfer for matching grant project ID: ' . $project_id;

				} else {

					$total   = (int) ( ( $amount ) * 100 ); //multiplying by 100 for Stripe and converting it an integer
					$message = 'Transfer for All-In project ID: ' . $project_id;

				}

				$settings = get_option( 'memberdeck_gateways' );

				if ( ! empty( $settings ) ) {
					if ( is_array( $settings ) ) {
						$test = $settings['test'];
						if ( 1 == $test ) {
							$url = '/test';
						} else {
							$url = '';
						}
					}
				}

				//Admin who initiated the transfer
				$admin_id = $_POST['admin_id'];

				\Stripe\Stripe::setApiVersion( NANO_STRIPE_API_VERSION );

				// Transfer from Admin Debit separate account.
				$account = \Stripe\Transfer::create(
					array(
						'amount'      => $total,
						'currency'    => 'usd',
						'destination' => $master_stripe_account, // Master Account
						'description' => $message,
						'metadata'    => array(
							'project_id'                     => $project_id,
							'nano_creator_id'                => $creator_id,
							'total_funded'                   => $total_fund_raised,
							'total_fee'                      => $total_fees,
							'nanosteam_fee'                  => $nanosteam_fees,
							'stripe_fee'                     => $stripe_fees,
							'project_name'                   => $project_name,
							'project_url'                    => $project_url,
							'user_id_who_initiated_transfer' => $admin_id,
						),
					),
					array(
						'stripe_account' => $stripe_withdrawl_account, // Admin Debit Account
					)
				);

				$account = $account->__toJSON();
				$account = json_decode( $account );

				$status = true;
				$error  = 'A transfer of $' . ( $total / 100 ) . ' has now been initiated from Nanosteams\'s connect account <a target="new" href="https://dashboard.stripe.com' . $url . '/applications/users/' . $stripe_withdrawl_account . '">' . $stripe_withdrawl_account . '</a>. Within 15 days the final transfer will take place to the creator. An email has been sent to the Creator. Please review your <strong>Payment</strong> history in your stripe admin dashboard.';

			} catch ( \Stripe\Error\Card $e ) {
				// Since it's a decline, \Stripe\Error\Card will be caught
				$status = false;
				stripe_error_email_admin_notification( $e, 'nano_stripe_creator_payment_ajax' );
			} catch ( \Stripe\Error\RateLimit $e ) {
				// Too many requests made to the API too quickly
				$status = false;
				$body   = $e->getJsonBody();
				$err    = $body['error'];
				$error  = 'Status ' . $e->getHttpStatus() . '. ' . $err['message'];
			} catch ( \Stripe\Error\InvalidRequest $e ) {
				// Invalid parameters were supplied to Stripe's API
				$status = false;
				$body   = $e->getJsonBody();
				$err    = $body['error'];
				$error  = 'Status ' . $e->getHttpStatus() . '. ' . $err['message'];
			} catch ( \Stripe\Error\Authentication $e ) {
				// Authentication with Stripe's API failed
				// (maybe you changed API keys recently)
				$status = false;
				$body   = $e->getJsonBody();
				$err    = $body['error'];
				$error  = 'Status ' . $e->getHttpStatus() . '. ' . $err['message'];
			} catch ( \Stripe\Error\ApiConnection $e ) {
				// Network communication with Stripe failed
				$status = false;
				$body   = $e->getJsonBody();
				$err    = $body['error'];
				$error  = 'Status ' . $e->getHttpStatus() . '. ' . $err['message'];
			} catch ( \Stripe\Error\Base $e ) {
				// Display a very generic error to the user, and maybe send
				// yourself an email
				$status = false;
				$body   = $e->getJsonBody();
				$err    = $body['error'];
				$error  = 'Status ' . $e->getHttpStatus() . '. ' . $err['message'];
			} catch ( Exception $e ) {
				// Something else happened, completely unrelated to Stripe
				$status = false;
				$error  = 'Exception ' . $e;
			}

			if ( true !== $status ) {
				echo json_encode(
					array(
						'stripeError' => true,
						'message'     => $error,
					)
				);
				stripe_error_email_admin_notification( $e, 'nano_stripe_creator_payment_ajax' );
				exit();
			} else {
				// If everything is good from Stripe then do the following

				echo json_encode(
					array(
						'stripeError' => false,
						'message'     => $error,
					)
				);

				//Update the project with the transaction ID from Stripe for future reference if required
				if ( true != $first_transfer_processed_status && true == $matching_grant_project_check ) {
					update_post_meta( $post_id, 'first_transfer_transaction_id', $account->id );
					update_post_meta( $post_id, 'first_transfer_transaction_initiated', nano_get_formated_date_time_gmt( 'm/d/Y' ) );
					update_post_meta( $post_id, 'first_transfer_transaction_delivered', 'PENDING' );
				} elseif ( true == $first_transfer_processed_status && true != $final_transfer_processed_status && true == $matching_grant_project_check ) {
					update_post_meta( $post_id, 'final_transfer_transaction_id', $account->id );
					update_post_meta( $post_id, 'final_transfer_transaction_date_initiated', nano_get_formated_date_time_gmt( 'm/d/Y' ) );
					update_post_meta( $post_id, 'final_transfer_transaction_date_delivered', 'PENDING' );
				} else {
					update_post_meta( $post_id, 'standard_transfer_transaction_id', $account->id );
					update_post_meta( $post_id, 'standard_transfer_transaction_date_initiated', nano_get_formated_date_time_gmt( 'm/d/Y' ) );
					update_post_meta( $post_id, 'standard_transfer_transaction_date_delivered', 'PENDING' );
				}

				// Adding this data here to cross referencing when paid out amounts become available to the platform account
				// from the connected payout account. See function nano_creator_payment_listener()
				// These amounts are precise to 2 decimal places whereas IDK only is precise to 1 decimal place.

				update_post_meta( $post_id, 'nano_total_raised', $total_fund_raised );
				update_post_meta( $post_id, 'nano_fees_total', $total_fees );
				update_post_meta( $post_id, 'nano_fees_stripe', $stripe_fees );
				update_post_meta( $post_id, 'nano_fees_nanosteam', $nanosteam_fees );
				update_post_meta( $post_id, 'nano_fees_after_fees', $total_after_fees );

				// Send email to Admin that the transfer took place
				$subject = 'Transfer initiated successfully for ';
				stripe_transfer_email_admin_notification( $post_id, $project_name, $project_url, $creator_id, $account->id, $user_stripe_account, $project_fund_raised, $total, $nanosteam_fees, $stripe_fees, $admin_id, $subject );

				// Send email to creator
				$recipient = get_user_by( 'login', get_post_meta( $post_id, 'original_author', true ) ) ? get_user_by( 'login', get_post_meta( $post_id, 'original_author', true ) ) : '';
				$recipient = '' != $recipient ? $recipient->user_email : '';
				nano_email_notify( $post_id, $recipient, 'nanosteam_transfer' );

				// Now to close the project to further fundraising
				$date = nano_get_formated_date_time_gmt( 'm/d/Y' );
				$date = date( 'm/d/Y', strtotime( '-1 day', strtotime( $date ) ) );

				if ( '1' !== get_post_meta( $post_id, 'ign_project_closed', true ) ) {
					update_post_meta( $post_id, 'ign_fund_end', $date );
					update_post_meta( $post_id, 'ign_project_closed', '1' );
				}

			}
		} elseif ( true == $project_success_status || true == $project_fund_status && true == $first_transfer_processed_status && true != $final_transfer_processed_status ) {
			echo json_encode( array(
				'stripeError' => true,
				'message'     => 'This matching grant project has already had the first payment transferred but the second has not been transferred.',
			) );
		} elseif ( true == $project_success_status || true == $project_fund_status && true == $first_transfer_processed_status && true == $final_transfer_processed_status ) {
			echo json_encode( array(
				'stripeError' => true,
				'message'     => 'This matching grant project has already had funds transferred.',
			) );
		} elseif ( true == $project_success_status || true == $project_fund_status && true == $standard_transfer_processed_status ) {
			echo json_encode( array(
				'stripeError' => true,
				'message'     => 'This all or nothing project has already had funds transferred.',
			) );
		} else {
			echo json_encode( array(
				'stripeError' => true,
				'message'     => 'The project has not managed to reach it\'s goal and the project end date has not arrived. You should not have seen this button.',
			) );
		}
	} else {

		echo json_encode(
			array(
				'stripeError' => true,
				'message'     => 'Sorry you have insufficent priveldges to proceed.',
			)
		);

	}

	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		wp_die();
	}

}


/**
 * Ties into stripe_listener.php
 * Once a payout.paid hook has been received
 * Processing button is located in the project's sidebar metabox under "Payout this project"
 *
 * @param array $event
 * @param string $stripe_user_id
 *
 * @return int
 *
 */

function nano_creator_payment_listener( $event, $stripe_user_id ) {

	// Admin Debit Account
	$stripe_withdrawl_account = get_option( 'nano_debit_account' );

	$payout_account = $stripe_user_id;
	$payout_amount  = isset( $event->data->object->amount ) ? $event->data->object->amount : 1;

	// These are all transactions which have recently been released to the platforms general funds.
	$reserve_transactions = nano_stripe_last_reserve_transactions();

	$response_code = 200;

	if ( $stripe_withdrawl_account == $payout_account && $payout_amount < 0 ) {

		$response_code = 400;

		// Check for all projects which are awaiting final payment but which the payment process has been initiated.
		$args = array(
			'post_type'      => 'ignition_product',
			'post_status'    => 'publish',
			'posts_per_page' => - 1,
		);

		// Get all published projects
		$query = new WP_Query( $args );

		if ( $query->have_posts() ) {

			while ( $query->have_posts() ) {
				$query->the_post();
				$post_id = get_the_id();

				$is_closed     = get_post_meta( $post_id, 'ign_project_closed', true ) == '1' ? true : false;
				$is_successful = get_post_meta( $post_id, 'ign_project_success', true ) == '1' ? true : false;

				if ( true == $is_closed && true == $is_successful ) {

					// Is a matching grant or not
					$matching_grant_project_check = get_post_meta( $post_id, 'fifty_fifty_grant', true ) == 'approved' ? true : false;

					//Project Parameters
					$project_id     = get_post_meta( $post_id, 'project_uid', true );
					$ign_project_id = get_post_meta( $post_id, 'ign_project_id', true );
					$project_name   = nano_get_project_name( $ign_project_id );
					$project_url    = get_permalink( $post_id );

					// Project Parameters & orders
					$level_id = nano_get_level_id( get_post_meta( $post_id, 'ign_project_id', true ) );
					$orders   = ID_Member_Order::get_orders_by_level( $level_id );

					// Total and Fees
					$fees              = nano_fee_list( $orders, $post_id );
					$total_fund_raised = $fees['total_before_fees'];
					$total_fees        = $fees['total_fees'];
					$stripe_fees       = $fees['stripe_fees'];
					$nanosteam_fees    = $fees['nanosteam_fees'];
					// $after_fees        = get_post_meta( $post_id, 'nano_fees_after_fees', true );

					// Project Payment Statuses
					// $first_transfer_processed_id          = get_post_meta( $post_id, 'first_transfer_transaction_id', true );
					$first_transfer_transaction_initiated = strtotime( get_post_meta( $post_id, 'first_transfer_transaction_initiated', true ) );
					$first_transfer_transaction_delivered = get_post_meta( $post_id, 'first_transfer_transaction_delivered', true );
					$first_status                         = validate_date( $first_transfer_transaction_delivered ) == false ? 'UNPAID' : 'PAID';

					// $final_transfer_processed_id               = get_post_meta( $post_id, 'final_transfer_transaction_id', true );
					$final_transfer_transaction_date_initiated = strtotime( get_post_meta( $post_id, 'final_transfer_transaction_date_initiated', true ) );
					$final_transfer_transaction_date_delivered = get_post_meta( $post_id, 'final_transfer_transaction_date_delivered', true );
					$final_status                              = validate_date( $final_transfer_transaction_date_delivered ) == false ? 'UNPAID' : 'PAID';

					// $standard_transfer_processed_id               = get_post_meta( $post_id, 'standard_transfer_transaction_id', true );
					$standard_transfer_transaction_date_initiated = strtotime( get_post_meta( $post_id, 'standard_transfer_transaction_date_initiated', true ) );
					$standard_transfer_transaction_date_delivered = get_post_meta( $post_id, 'standard_transfer_transaction_date_delivered', true );
					$standard_status                              = validate_date( $standard_transfer_transaction_date_delivered ) == false ? 'UNPAID' : 'PAID';

					$amount = true == $matching_grant_project_check ? ( round( $total_fund_raised / 2 ) ) - $total_fees : $total_fund_raised - $total_fees;

					$payment_status = true;

					// Fees and Totals based on whether this is the first or last matching grant or an all or nothing project
					if ( 'UNPAID' == $first_status && true == $matching_grant_project_check ) {

						$total          = ( $amount ) * 100; //multiplying by 100 for Stripe
						$date_initiated = $first_transfer_transaction_initiated;
						$message        = 'First Matching Grant Payment to Creator. Project ID: ' . $project_id;
						$payment_status = $first_status;

					} elseif ( 'PAID' == $first_status && 'UNPAID' == $final_status && true == $matching_grant_project_check ) {

						$total          = ( $amount + $total_fees ) * 100; //multiplying by 100 for Stripe
						$date_initiated = $final_transfer_transaction_date_initiated;
						$message        = 'Final Matching Grant Payment to Creator. Project ID: ' . $project_id;
						$payment_status = $final_status;

					} elseif ( 'UNPAID' == $standard_status && false == $matching_grant_project_check ) {

						$total          = ( $amount ) * 100; //multiplying by 100 for Stripe
						$date_initiated = $standard_transfer_transaction_date_initiated;
						$message        = 'All or nothing Payment to Creator. Project ID: ' . $project_id;
						$payment_status = $standard_status;

					}

					$total = absint( $total );

					foreach ( $reserve_transactions as $reserve_transaction ) {

						if ( in_array( $total, $reserve_transaction['totals'] ) ) {

							$reserve_released_date = $reserve_transaction['date_available'] > strtotime( '-7 day' ) ? true : false;
							$date_initiated        = $date_initiated > strtotime( '-15 day' ) ? true : false;
							$total_reserves        = get_option( 'nano_stripe_balance' );
							$total_available       = $total_reserves['available'];

							$stripe_reserve = 100 * get_option( 'nano_platform_stripe_amt' ); // This equals to stripe.

							if ( true == $reserve_released_date && true == $date_initiated && ( ( $total_available - $stripe_reserve ) > $total ) && ( 'UNPAID' == $payment_status ) ) {

								// We've confirmed that the total and the release date of the funds have been released into
								// the general platform account at this point. We can now proceed to transfer the funds to
								// the creator. We have a last cross check to confirm that this amount hasn't already been paid
								// As well. We can now proceed with paying the creator.

								// Creator Parameters
								$user               = get_post_meta( $post_id, 'original_author', true );
								$creator_id         = false !== get_user_by( 'login', $user ) ? get_user_by( 'login', $user ) : get_user_by( 'email', $user );
								$creator_id         = $creator_id->ID;
								$user_stripe_params = get_sc_params( $creator_id );
								$user_stripe_id     = $user_stripe_params->stripe_user_id;

								$settings = get_option( 'memberdeck_gateways' );

								if ( ! empty( $settings ) ) {
									if ( is_array( $settings ) ) {
										$test = $settings['test'];
										if ( 1 == $test ) {
											$key         = $settings['tsk'];
											$stripe_acct = '/test';
										} else {
											$key         = $settings['sk'];
											$stripe_acct = '';
										}
									}
								}

								if ( ! class_exists( 'Stripe\Stripe' ) ) {
									require_once IDC_PATH . 'lib/stripe-php-4.2.0/init.php';
								}

								\Stripe\Stripe::setApiKey( $key );

								try {

									$account = \Stripe\Transfer::create(
										array(
											'amount'      => $total,
											'currency'    => 'usd',
											'destination' => $user_stripe_id,
											'description' => $message,
											'metadata'    => array(
												'project_id'      => $project_id,
												'nano_creator_id' => $creator_id,
												'total_funded'    => $total_fund_raised,
												'total_fee'       => $total_fees,
												'nanosteam_fee'   => $nanosteam_fees,
												'stripe_fee'      => $stripe_fees,
												'project_name'    => $project_name,
												'project_url'     => $project_url,
											),
										)
									);

									$status = true;
									$error  = 'A transfer of $' . ( $total / 100 ) . ' has now been initiated from Nanosteams\'s connect debit account <a target="new" href="https://dashboard.stripe.com' . $stripe_acct . '/applications/users/' . $stripe_withdrawl_account . '">' . $stripe_withdrawl_account . '</a>. Within 15 days the final transfer will take place to the creator. An email has been sent to the Creator. Please review your <strong>Payment</strong> history in your stripe admin dashboard.';
									// Send email to creator

								} catch ( \Stripe\Error\Card $e ) {
									// Since it's a decline, \Stripe\Error\Card will be caught
									$status = false;
									$body   = $e->getJsonBody();
									$err    = $body['error'];
									$error  = 'Status ' . $e->getHttpStatus() . '. ' . $err['message'];
								} catch ( \Stripe\Error\RateLimit $e ) {
									// Too many requests made to the API too quickly
									$status = false;
									$body   = $e->getJsonBody();
									$err    = $body['error'];
									$error  = 'Status ' . $e->getHttpStatus() . '. ' . $err['message'];
								} catch ( \Stripe\Error\InvalidRequest $e ) {
									// Invalid parameters were supplied to Stripe's API
									$status = false;
									$body   = $e->getJsonBody();
									$err    = $body['error'];
									$error  = 'Status ' . $e->getHttpStatus() . '. ' . $err['message'];
								} catch ( \Stripe\Error\Authentication $e ) {
									// Authentication with Stripe's API failed
									// (maybe you changed API keys recently)
									$status = false;
									$body   = $e->getJsonBody();
									$err    = $body['error'];
									$error  = 'Status ' . $e->getHttpStatus() . '. ' . $err['message'];
								} catch ( \Stripe\Error\ApiConnection $e ) {
									// Network communication with Stripe failed
									$status = false;
									$body   = $e->getJsonBody();
									$err    = $body['error'];
									$error  = 'Status ' . $e->getHttpStatus() . '. ' . $err['message'];
								} catch ( \Stripe\Error\Base $e ) {
									// Display a very generic error to the user, and maybe send
									// yourself an email
									$status = false;
									$body   = $e->getJsonBody();
									$err    = $body['error'];
									$error  = 'Status ' . $e->getHttpStatus() . '. ' . $err['message'];
								} catch ( Exception $e ) {
									// Something else happened, completely unrelated to Stripe
									$status = false;
									$error  = 'Exception ' . $e;
								}

								if ( true !== $status ) {

									stripe_error_email_admin_notification( $e, 'nano_creator_payment_listener' );
									$response_code = 400;

								} else {

									// Send email to Admin that the transfer took place
									$subject = 'Funds have arrived in the platform\'s account and have successfully been transferred for ';
									stripe_transfer_email_admin_notification( $post_id, $project_name, $project_url, $creator_id, $account->id, $user_stripe_id, $total_fund_raised, $total, $nanosteam_fees, $stripe_fees, 'Not Applicable', $subject );

									// Send email to creator that their funds have been processed and are on the way.
									// $recipient = get_user_by( 'id', $creator_id ) ? get_user_by( 'id', $creator_id ) : '';
									// $recipient = $recipient != '' ? $recipient->user_email : '';
									// nano_email_notify($post_id, $recipient,'nanosteam_transfer_confirmed');

									// Send response code to stripe that this worked.
									$response_code = 200;
									// Adding this to the php error log for archival purposes
									send_error( $subject . $project_name . $error );
									//Update the post meta for the date the transaction was transferred to the creator
									if ( 'UNPAID' == $first_status && true == $matching_grant_project_check ) {

										update_post_meta( $post_id, 'first_transfer_transaction_delivered', nano_get_formated_date_time_gmt( 'm/d/Y' ) );

									} elseif ( 'PAID' == $first_status && 'UNPAID' == $final_status && true == $matching_grant_project_check ) {

										update_post_meta( $post_id, 'final_transfer_transaction_date_delivered', nano_get_formated_date_time_gmt( 'm/d/Y' ) );

									} elseif ( 'UNPAID' == $standard_status && false == $matching_grant_project_check ) {

										update_post_meta( $post_id, 'standard_transfer_transaction_date_delivered', nano_get_formated_date_time_gmt( 'm/d/Y' ) );

									}
								}
							}
						}
					}
				}
			}
		}
	}

	return http_response_code( $response_code );

}

/**
 * Specifically query Stripe on an event to check if it has been sent.
 *
 * @param array $event
 * @param string $stripe_user_id
 *
 * @return int
 *
 */

function nano_ajax_payout_overdue_recheck() {

	// First check the nonce, if it fails the function will break
	check_ajax_referer( 'nano_project_payout_meta', 'security' );

	$ajax_res = array();

	if ( true === nanosteam_privledge_check() ) {

		if ( ! current_user_can( 'administrator' ) ) {
			return;
		}

		if ( isset( $_POST['event_id'] ) && '' !== $_POST['event_id'] ) {

			$stripe_withdrawl_account = get_option( 'nano_debit_account' );
			$post_id                  = $_POST['post_id'];
			$event_id                 = $_POST['event_id'];

			//Project Parameters
			$level_id = nano_get_level_id( get_post_meta( $post_id, 'ign_project_id', true ) );
			$orders   = ID_Member_Order::get_orders_by_level( $level_id );

			// FEES & TOTALS
			$fees              = nano_fee_list( $orders, $post_id );
			$total_fund_raised = $fees['total_before_fees'];
			$stripe_fees       = $fees['stripe_fees'];
			$nanosteam_fees    = $fees['nanosteam_fees'];
			$total_fees        = $fees['total_fees'];
			$total_after_fees  = $fees['total_after_fees'];

			// Stripe
			$settings = get_option( 'memberdeck_gateways' );

			if ( ! empty( $settings ) ) {
				if ( is_array( $settings ) ) {
					$test = $settings['test'];
					if ( 1 == $test ) {
						$key = $settings['tsk'];
					} else {
						$key = $settings['sk'];
					}
				}
			}

			if ( ! class_exists( 'Stripe\Stripe' ) ) {
				require_once IDC_PATH . 'lib/stripe-php-4.2.0/init.php';
			}

			\Stripe\Stripe::setApiVersion( NANO_STRIPE_API_VERSION );
			\Stripe\Stripe::setApiKey( $key );

			try {

				$event = \Stripe\Event::retrieve(
					array( 'id' => $event_id ),
					array( 'stripe_account' => $stripe_withdrawl_account )
				);

				$status = true;

			} catch ( \Stripe\Error\RateLimit $e ) {
				// Too many requests made to the API too quickly
				stripe_error_email_admin_notification( $e, 'RateLimit nano_ajax_payout_overdue_recheck' );

			} catch ( \Stripe\Error\InvalidRequest $e ) {
				// Invalid parameters were supplied to Stripe's API
				stripe_error_email_admin_notification( $e, 'InvalidRequest nano_ajax_payout_overdue_recheck' );

			} catch ( \Stripe\Error\Authentication $e ) {
				// Authentication with Stripe's API failed
				// (maybe you changed API keys recently)
				stripe_error_email_admin_notification( $e, 'Authentication nano_ajax_payout_overdue_recheck' );

			} catch ( \Stripe\Error\ApiConnection $e ) {
				// Network communication with Stripe failed
				stripe_error_email_admin_notification( $e, 'ApiConnection nano_ajax_payout_overdue_recheck' );

			} catch ( \Stripe\Error\Base $e ) {
				// Display a very generic error to the user, and maybe send
				// yourself an email
				stripe_error_email_admin_notification( $e, 'Base nano_ajax_payout_overdue_recheck' );
			} catch ( Exception $e ) {
				// Something else happened, completely unrelated to Stripe
				stripe_error_email_admin_notification( $e, 'Exception nano_ajax_payout_overdue_recheck' );
			}

			if ( isset( $status ) && true === $status ) {

				$results = $event->__toJSON();
				$results = json_decode( $results );

				$payout_amount = isset( $results->data->object->amount ) ? $results->data->object->amount : 1;
				$arrival_date  = isset( $results->data->object->arrival_date ) ? $results->data->object->arrival_date : null;

				$is_closed     = get_post_meta( $post_id, 'ign_project_closed', true ) == '1' ? true : false;
				$is_successful = get_post_meta( $post_id, 'ign_project_success', true ) == '1' ? true : false;

				if ( true == $is_closed && true == $is_successful ) {

					// These are all transactions which have recently been released to the platforms general funds.
					$reserve_transactions = nano_stripe_last_reserve_transactions();

					// Is a matching grant or not
					$matching_grant_project_check = get_post_meta( $post_id, 'fifty_fifty_grant', true ) == 'approved' ? true : false;

					//Project Parameters
					$project_id     = get_post_meta( $post_id, 'project_uid', true );
					$ign_project_id = get_post_meta( $post_id, 'ign_project_id', true );
					$project_name   = nano_get_project_name( $ign_project_id );
					$project_url    = get_permalink( $post_id );

					//Project Parameters & orders
					$level_id = nano_get_level_id( get_post_meta( $post_id, 'ign_project_id', true ) );
					$orders   = ID_Member_Order::get_orders_by_level( $level_id );

					// Total and Fees
					$fees              = nano_fee_list( $orders, $post_id );
					$total_fund_raised = $fees['total_before_fees'];
					$total_fees        = $fees['total_fees'];
					$stripe_fees       = $fees['stripe_fees'];
					$nanosteam_fees    = $fees['nanosteam_fees'];
					// $after_fees        = get_post_meta( $post_id, 'nano_fees_after_fees', true );

					// Project Payment Statuses
					// $first_transfer_processed_id          = get_post_meta( $post_id, 'first_transfer_transaction_id', true );
					$first_transfer_transaction_initiated = strtotime( get_post_meta( $post_id, 'first_transfer_transaction_initiated', true ) );
					$first_transfer_transaction_delivered = get_post_meta( $post_id, 'first_transfer_transaction_delivered', true );
					$first_status                         = validate_date( $first_transfer_transaction_delivered ) == false ? 'UNPAID' : 'PAID';

					// $final_transfer_processed_id               = get_post_meta( $post_id, 'final_transfer_transaction_id', true );
					$final_transfer_transaction_date_initiated = strtotime( get_post_meta( $post_id, 'final_transfer_transaction_date_initiated', true ) );
					$final_transfer_transaction_date_delivered = get_post_meta( $post_id, 'final_transfer_transaction_date_delivered', true );
					$final_status                              = validate_date( $final_transfer_transaction_date_delivered ) == false ? 'UNPAID' : 'PAID';

					// $standard_transfer_processed_id               = get_post_meta( $post_id, 'standard_transfer_transaction_id', true );
					$standard_transfer_transaction_date_initiated = strtotime( get_post_meta( $post_id, 'standard_transfer_transaction_date_initiated', true ) );
					$standard_transfer_transaction_date_delivered = get_post_meta( $post_id, 'standard_transfer_transaction_date_delivered', true );
					$standard_status                              = validate_date( $standard_transfer_transaction_date_delivered ) == false ? 'UNPAID' : 'PAID';

					$amount = true == $matching_grant_project_check ? ( round( $total_fund_raised / 2 ) ) - $total_fees : $total_fund_raised - $total_fees;

					$payment_status = true;

					// Fees and Totals based on whether this is the first or last matching grant or an all or nothing project
					if ( 'UNPAID' == $first_status && true == $matching_grant_project_check ) {

						$total          = ( $amount ) * 100; //multiplying by 100 for Stripe
						$date_initiated = $first_transfer_transaction_initiated;
						$message        = 'First Matching Grant Payment to Creator. Project ID: ' . $project_id;
						$payment_status = validate_date( $first_transfer_transaction_delivered );

					} elseif ( 'PAID' == $first_status && 'UNPAID' == $final_status && true == $matching_grant_project_check ) {

						$total          = ( $amount + $total_fees ) * 100; //multiplying by 100 for Stripe
						$date_initiated = $final_transfer_transaction_date_initiated;
						$message        = 'Final Matching Grant Payment to Creator. Project ID: ' . $project_id;
						$payment_status = validate_date( $final_transfer_transaction_date_delivered );

					} elseif ( 'UNPAID' == $standard_status && false == $matching_grant_project_check ) {

						$total          = ( $amount ) * 100; //multiplying by 100 for Stripe
						$date_initiated = $standard_transfer_transaction_date_initiated;
						$message        = 'All or nothing Payment to Creator. Project ID: ' . $project_id;
						$payment_status = validate_date( $standard_transfer_transaction_date_delivered );

					}

					$total = absint( $total );

					if ( false === $payment_status ) {

						foreach ( $reserve_transactions as $reserve_transaction ) {

							if ( in_array( $total, $reserve_transaction['totals'] ) ) {

								$reserve_released_date = $reserve_transaction['date_available'] > strtotime( '-30 day' ) ? true : false;
								$date_initiated        = $date_initiated > strtotime( '-30 day' ) ? true : false;
								$total_reserves        = get_option( 'nano_stripe_balance' );
								$total_available       = $total_reserves['available'];

								$stripe_reserve = 100 * get_option( 'nano_platform_stripe_amt' ); // This equals to stripe.

								if ( true == $reserve_released_date && true == $date_initiated && ( $total_available - $stripe_reserve ) > $total && false == $payment_status ) {

									// We've confirmed that the total and the release date of the funds have been released into
									// the general platform account at this point. We can now proceed to transfer the funds to
									// the creator. We have a last cross check to confirm that this amount hasn't already been paid
									// As well. We can now proceed with paying the creator.

									// Creator Parameters
									$user               = get_post_meta( $post_id, 'original_author', true );
									$creator_id         = false !== get_user_by( 'login', $user ) ? get_user_by( 'login', $user ) : get_user_by( 'email', $user );
									$creator_id         = $creator_id->ID;
									$user_stripe_params = get_sc_params( $creator_id );
									$user_stripe_id     = $user_stripe_params->stripe_user_id;

									$settings = get_option( 'memberdeck_gateways' );

									if ( ! empty( $settings ) ) {
										if ( is_array( $settings ) ) {
											$test = $settings['test'];
											if ( 1 == $test ) {
												$key         = $settings['tsk'];
												$stripe_acct = '/test';
											} else {
												$key         = $settings['sk'];
												$stripe_acct = '';
											}
										}
									}

									if ( ! class_exists( 'Stripe\Stripe' ) ) {
										require_once IDC_PATH . 'lib/stripe-php-4.2.0/init.php';
									}

									\Stripe\Stripe::setApiKey( $key );

									try {

										$account = \Stripe\Transfer::create(
											array(
												'amount'      => $total,
												'currency'    => 'usd',
												'destination' => $user_stripe_id,
												'description' => $message,
												'metadata'    => array(
													'project_id'      => $project_id,
													'nano_creator_id' => $creator_id,
													'total_funded'    => $total_fund_raised,
													'total_fee'       => $total_fees,
													'nanosteam_fee'   => $nanosteam_fees,
													'stripe_fee'      => $stripe_fees,
													'project_name'    => $project_name,
													'project_url'     => $project_url,
												),
											)
										);

										$status = true;
										$error  = 'A transfer of $' . ( $total / 100 ) . ' has now been initiated from Nanosteams\'s connect debit account <a target="new" href="https://dashboard.stripe.com' . $stripe_acct . '/applications/users/' . $stripe_withdrawl_account . '">' . $stripe_withdrawl_account . '</a>. Within 15 days the final transfer will take place to the creator. An email has been sent to the Creator. Please review your <strong>Payment</strong> history in your stripe admin dashboard.';
										// Send email to creator

									} catch ( \Stripe\Error\Card $e ) {
										// Since it's a decline, \Stripe\Error\Card will be caught
										$status = false;
										$body   = $e->getJsonBody();
										$err    = $body['error'];
										$error  = 'Status ' . $e->getHttpStatus() . '. ' . $err['message'];
									} catch ( \Stripe\Error\RateLimit $e ) {
										// Too many requests made to the API too quickly
										$status = false;
										$body   = $e->getJsonBody();
										$err    = $body['error'];
										$error  = 'Status ' . $e->getHttpStatus() . '. ' . $err['message'];
									} catch ( \Stripe\Error\InvalidRequest $e ) {
										// Invalid parameters were supplied to Stripe's API
										$status = false;
										$body   = $e->getJsonBody();
										$err    = $body['error'];
										$error  = 'Status ' . $e->getHttpStatus() . '. ' . $err['message'];
									} catch ( \Stripe\Error\Authentication $e ) {
										// Authentication with Stripe's API failed
										// (maybe you changed API keys recently)
										$status = false;
										$body   = $e->getJsonBody();
										$err    = $body['error'];
										$error  = 'Status ' . $e->getHttpStatus() . '. ' . $err['message'];
									} catch ( \Stripe\Error\ApiConnection $e ) {
										// Network communication with Stripe failed
										$status = false;
										$body   = $e->getJsonBody();
										$err    = $body['error'];
										$error  = 'Status ' . $e->getHttpStatus() . '. ' . $err['message'];
									} catch ( \Stripe\Error\Base $e ) {
										// Display a very generic error to the user, and maybe send
										// yourself an email
										$status = false;
										$body   = $e->getJsonBody();
										$err    = $body['error'];
										$error  = 'Status ' . $e->getHttpStatus() . '. ' . $err['message'];
									} catch ( Exception $e ) {
										// Something else happened, completely unrelated to Stripe
										$status = false;
										$error  = 'Exception ' . $e;
									}

									if ( true !== $status ) {

										stripe_error_email_admin_notification( $e, 'nano_ajax_payout_overdue_recheck' );
										$response_code = 400;

										$ajax_res = array(
											'stripeError' => true,
											'message'     => 'We found the correct total but there was a problem with the transferring the amount to the creator. You may need to do this transfer manually.',
										);

										break;


									} else {

										// Send email to Admin that the transfer took place
										$subject = 'Funds have arrived in the platform\'s account and have successfully been transferred for ';
										stripe_transfer_email_admin_notification( $post_id, $project_name, $project_url, $creator_id, $account->id, $user_stripe_id, $total_fund_raised, $total, $nanosteam_fees, $stripe_fees, 'Not Applicable', $subject );

										// Send email to creator that their funds have been processed and are on the way.
										//$recipient = get_user_by( 'id', $creator_id ) ? get_user_by( 'id', $creator_id ) : '';
										//$recipient = $recipient != '' ? $recipient->user_email : '';
										//nano_email_notify($post_id, $recipient,'nanosteam_transfer_confirmed');

										// Send response code to stripe that this worked.
										$response_code = 200;
										send_error( $subject . $project_name . $error );
										//Update the post meta for the date the transaction was transferred to the creator
										if ( 'UNPAID' == $first_status && true == $matching_grant_project_check ) {

											update_post_meta( $post_id, 'first_transfer_transaction_delivered', nano_get_formated_date_time_gmt( 'm/d/Y' ) );

										} elseif ( 'PAID' == $first_status && 'UNPAID' == $final_status && true == $matching_grant_project_check ) {

											update_post_meta( $post_id, 'final_transfer_transaction_date_delivered', nano_get_formated_date_time_gmt( 'm/d/Y' ) );

										} elseif ( 'UNPAID' == $standard_status && false == $matching_grant_project_check ) {

											update_post_meta( $post_id, 'standard_transfer_transaction_date_delivered', nano_get_formated_date_time_gmt( 'm/d/Y' ) );

										}

										$ajax_res = array(
											'stripeError' => false,
											'message'     => 'Success! Funds have arrived in the platform\'s account and have successfully been transferred to the creator',
										);

										// Updating the payment status for all successfully paid projects
										update_metadata_cron();

										break;
									}
								} else {

									$ajax_res = array(
										'stripeError' => true,
										'message'     => 'Sorry there was a problem with one or more of the following: Reserve Transaction Date, Date the transfer was initiated, total reserves are insufficient or the payment status is already processed.   ',
									);

								}
							} else {

								$ajax_res = array(
									'stripeError' => true,
									'message'     => 'This amount was not listed in our reserve transactions from Stripe through this event.',
								);

							}
						}
					} else {

						$ajax_res = array(
							'stripeError' => true,
							'message'     => 'This project\'s funds have already been sent to the creator.',
						);

					}
				} else {

					$ajax_res = array(
						'stripeError' => true,
						'message'     => 'This projects status is not listed as successful or closed.',
					);

				}
			} else {

				$ajax_res = array(
					'stripeError' => true,
					'message'     => 'Stripe did not accept the event ID you entered.',
				);

			}
		} else {

			$ajax_res = array(
				'stripeError' => true,
				'message'     => 'Please enter an Event ID.',
			);

		}
	}

	echo json_encode(
		$ajax_res
	);

	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		wp_die();
	}

}

// A basic test fucntion which changes based on needs
function nano_stripe_test() {

	$settings = get_option( 'memberdeck_gateways' );

	if ( ! empty( $settings ) ) {
		if ( is_array( $settings ) ) {
			$test = $settings['test'];
			if ( 1 == $test ) {
				$key = $settings['tsk'];
			} else {
				$key = $settings['sk'];
			}
		}
	}

	if ( ! class_exists( 'Stripe\Stripe' ) ) {
		require_once IDC_PATH . 'lib/stripe-php-4.2.0/init.php';
	}

	\Stripe\Stripe::setApiKey( $key );

	try {

		// Admin Debit Account
		$stripe_withdrawl_account = get_option( 'nano_debit_account' );
		$elissa                   = 'acct_1DDk4sEQ0ARGFZY7';
		$event_id                 = 'evt_1ChZXcGPaLlQVE7hxkF4o2G4';
		$stripe_user_id           = $elissa;

		/*

		$event = \Stripe\Event::retrieve(
			array( 'id' => $event_id ),
			array( 'stripe_account' => $stripe_withdrawl_account )
		);

		*/

		$account = \Stripe\Account::retrieve( $stripe_user_id );
		//$bank_account = $account->external_accounts->retrieve("ba_1DGGvVEQ0ARGFZY7S9McJtOn");
		//$bank_account->default_for_currency = true;
		//$bank_account->save();

		$account = $account->__toJSON();
		$account = json_decode( $account );

	} catch ( \Stripe\Error\RateLimit $e ) {
		// Too many requests made to the API too quickly
		stripe_error_email_admin_notification( $e, 'RateLimit nano_stripe_test' );
		http_response_code( 400 ); // PHP 5.4 or greater

	} catch ( \Stripe\Error\InvalidRequest $e ) {
		// Invalid parameters were supplied to Stripe's API
		stripe_error_email_admin_notification( $e, 'InvalidRequest nano_stripe_test' );
		http_response_code( 400 ); // PHP 5.4 or greater

	} catch ( \Stripe\Error\Authentication $e ) {
		// Authentication with Stripe's API failed
		// (maybe you changed API keys recently)
		stripe_error_email_admin_notification( $e, 'Authentication nano_stripe_test' );
		http_response_code( 400 ); // PHP 5.4 or greater

	} catch ( \Stripe\Error\ApiConnection $e ) {
		// Network communication with Stripe failed
		stripe_error_email_admin_notification( $e, 'ApiConnection nano_stripe_test' );
		http_response_code( 400 ); // PHP 5.4 or greater

	} catch ( \Stripe\Error\Base $e ) {
		// Display a very generic error to the user, and maybe send
		// yourself an email
		stripe_error_email_admin_notification( $e, 'Base nano_stripe_test' );
		http_response_code( 400 ); // PHP 5.4 or greater

	} catch ( Exception $e ) {
		// Something else happened, completely unrelated to Stripe
		stripe_error_email_admin_notification( $e, 'Exception nano_stripe_test' );
		http_response_code( 400 ); // PHP 5.4 or greater
	}

	// nano_creator_payment_listener( $event, $stripe_user_id );

}


/**
 * Pulls together information from the cities and states and organizes them based on the user's selection
 *
 * @param $vals
 * @param $sum
 *
 * @return array
 */
function array_sum_parts( $vals, $sum ) {
	$solutions      = array();
	$pos            = array( 0 => count( $vals ) - 1 );
	$last_pos_index = 0;
	$current_pos    = $pos[0];
	$current_sum    = 0;
	while ( true ) {
		$current_sum += $vals[ $current_pos ];

		if ( $current_sum < $sum && 0 != $current_pos ) {

			$pos[ ++ $last_pos_index ] = -- $current_pos;

		} else {

			if ( $current_sum == $sum ) {
				$solutions[] = array_slice( $pos, 0, $last_pos_index + 1 );

			}

			if ( 0 == $last_pos_index ) {
				break;
			}

			$current_sum -= $vals[ $current_pos ] + $vals[ 1 + $current_pos = -- $pos[ -- $last_pos_index ] ];
		}
	}

	return $solutions;

}


/**
 * Return all reserve transactions within the last 200 transactions that have taken place.
 * Checks the total returned and returns all possible values within the last week which correlate to the total as
 * Sometimes the returned value can be a bundle of transfers.
 *
 */

function nano_stripe_last_reserve_transactions() {

	$nano_stripe_last_reserve_transactions = get_transient( 'nano_stripe_last_reserve_transactions' );
	//$nano_stripe_last_reserve_transactions = false;

	if ( false === $nano_stripe_last_reserve_transactions ) {

		$settings = get_option( 'memberdeck_gateways' );

		if ( ! empty( $settings ) ) {
			if ( is_array( $settings ) ) {
				$test = $settings['test'];
				if ( 1 == $test ) {
					$key = $settings['tsk'];
				} else {
					$key = $settings['sk'];
				}
			}
		}

		if ( ! class_exists( 'Stripe\Stripe' ) ) {
			require_once IDC_PATH . 'lib/stripe-php-4.2.0/init.php';
		}

		\Stripe\Stripe::setApiKey( $key );

		try {

			// We need to find all reserve transactions which have been released and forwad them on.
			$event = \Stripe\BalanceTransaction::all(
				array(
					'limit' => 200,
				)
			);

			$event  = $event->__toJSON();
			$events = json_decode( $event );
		} catch ( \Stripe\Error\RateLimit $e ) {
			// Too many requests made to the API too quickly
			stripe_error_email_admin_notification( $e, 'RateLimit nano_stripe_last_reserve_transactions' );
			http_response_code( 400 ); // PHP 5.4 or greater

		} catch ( \Stripe\Error\InvalidRequest $e ) {
			// Invalid parameters were supplied to Stripe's API
			stripe_error_email_admin_notification( $e, 'InvalidRequest nano_stripe_last_reserve_transactions' );
			http_response_code( 400 ); // PHP 5.4 or greater

		} catch ( \Stripe\Error\Authentication $e ) {
			// Authentication with Stripe's API failed
			// (maybe you changed API keys recently)
			stripe_error_email_admin_notification( $e, 'Authentication nano_stripe_last_reserve_transactions' );
			http_response_code( 400 ); // PHP 5.4 or greater

		} catch ( \Stripe\Error\ApiConnection $e ) {
			// Network communication with Stripe failed
			stripe_error_email_admin_notification( $e, 'ApiConnection nano_stripe_last_reserve_transactions' );
			http_response_code( 400 ); // PHP 5.4 or greater

		} catch ( \Stripe\Error\Base $e ) {
			// Display a very generic error to the user, and maybe send
			// yourself an email
			stripe_error_email_admin_notification( $e, 'Base nano_stripe_last_reserve_transactions' );
			http_response_code( 400 ); // PHP 5.4 or greater

		} catch ( Exception $e ) {
			// Something else happened, completely unrelated to Stripe
			stripe_error_email_admin_notification( $e, 'Exception nano_stripe_last_reserve_transactions' );
			http_response_code( 400 ); // PHP 5.4 or greater
		}

		$nano_stripe_last_reserve_transactions = array();
		if ( $events->data ) {
			foreach ( $events->data as $event ) {
				if ( 'reserve_transaction' == $event->type ) {
					if ( false !== strpos( $event->description, 'Released' ) ) {
						$nano_stripe_last_reserve_transactions['released'][] = $event;
					} else {
						$nano_stripe_last_reserve_transactions['original'][] = $event;
					}
				}
			}
		}

		// Time to find and return the values that make up the transferred amount if there is more than one

		$reserve_transactions_released = $nano_stripe_last_reserve_transactions['released'];
		$reserve_transactions_total    = $nano_stripe_last_reserve_transactions['original'];

		$sum_array = array();

		foreach ( $reserve_transactions_total as $id => $value ) {

			$sub_total = abs( $value->amount );

			if ( array_key_exists( $id, $sum_array ) ) {

				$sum_array[ $id ] += $sub_total;

			} else {

				$sum_array[ $id ] = $sub_total;

			}
		}

		rsort( $sum_array );

		$nano_stripe_last_reserve_transactions = array();
		foreach ( $reserve_transactions_released as $key => $val ) {

			$amount         = $val->amount;
			$date_available = $val->available_on;

			$result = array_sum_parts( $sum_array, $amount );

			foreach ( $result as $element ) {
				$totals = array();
				$s      = 0;

				foreach ( $element as $subkey => $i ) {
					//$totals[$key][]  = $reserve_transactions_total[$i];
					$totals[ $subkey ] = $sum_array[ $i ];
					$s                += $sum_array[ $i ];
				}

				if ( $s == $amount ) {
					$nano_stripe_last_reserve_transactions[ $amount ] = array(
						'totals'         => $totals,
						'date_available' => $date_available,
					);
				}
			}
		}

		set_transient( 'nano_stripe_last_reserve_transactions', $nano_stripe_last_reserve_transactions, 1 * HOUR_IN_SECONDS );

		send_error( 'Reserve Transactions Updated: ' . nano_get_formated_date_time_gmt( 'm/d/Y' ) );

	}

	return $nano_stripe_last_reserve_transactions;

}


/**
 * Date validation to assist in confirmation for payouts
 *
 * @param string $date
 * @param string $format
 *
 * @return true or false based on whether the date in the field is a valid date or not
 *
 */

function validate_date( $date, $format = 'm/d/Y' ) {
	$d = DateTime::createFromFormat( $format, $date );

	// The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
	return $d && $d->format( $format ) === $date;
}
