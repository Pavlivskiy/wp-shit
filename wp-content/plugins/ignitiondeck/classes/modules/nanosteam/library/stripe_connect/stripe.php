<?php if ( ! defined( 'ABSPATH' ) ) {
	die( 'Direct access forbidden.' );
}


// Connect Accounts
include( dirname( __FILE__ ) . '/' . 'includes/stripe_connect.php' );

// Web Hooks
include( dirname( __FILE__ ) . '/' . 'includes/scripts.php' );
include( dirname( __FILE__ ) . '/' . 'includes/stripe_listener.php' );

//Stripe Credit System
include( dirname( __FILE__ ) . '/' . 'includes/stripe_credits.php' );