jQuery( document ).ready( function() {

    var orgProjectName, getUrlParameter, projectID, form, dataProduct, defaultProjectID;

    var paywithCredits = jQuery( '#pay-with-credits' ),
        firstPayoutSubmit = jQuery( '#first_payout_submit' ),
        payWithStripe = jQuery( '#pay-with-stripe' ),
        notificationElement = jQuery( '#first_nano_payout' ),
        overdueElement = jQuery( '#over_due_field' );

    // Check to see if we're on the checkout page
    if ( jQuery( '.checkout-wrapper' ).length ) {
        orgProjectName = jQuery( '.about-to-support span' ).text();
        jQuery( '.checkout-project-title span' ).text( orgProjectName );
        jQuery( '.terms-checkbox-input' ).attr( 'id', 'terms_conditions' );
        jQuery( '.link-terms-conditions' ).parent().attr( 'for', 'terms_conditions' );
    }

    // Payout button ajax function for ADMIN payout the creator
    function nanoStripePayout(buttonName) {

        var notificationElement = jQuery( '#first_nano_payout' );
        var dataToSend = {
            action: 'nano_stripe_creator_payment_ajax',
            security: jQuery( '#nano_project_payout_metabox' ).val(),
            creator_id: jQuery( '#post_author' ).val(),
            post_id: jQuery( '#post_ID' ).val(),
            admin_id: jQuery( '#user-id' ).val()
        };

        jQuery.ajax({
            type: 'POST',
            dataType: 'json',
            url: ajaxurl,
            data: dataToSend,
            success: function( data ) {
                console.log( data );
                if ( data.stripeError === false ) {
                    if ( buttonName === 'first_payout_submit') {
                        firstPayoutSubmit.text( 'Transfer Successful' ).prop( 'disabled', true );
                    } if ( buttonName === 'final_payout_submit' ) {
                        jQuery( '#final_payout_submit' ).text( 'Transfer Successful' ).prop( 'disabled', true );
                    }
                    notificationElement.html( data.message );
                    notificationElement.removeClass( 'hidden' ).addClass( 'fade show' );
                } else {
                    notificationElement.html( data.message );
                    notificationElement.removeClass( 'hidden' ).addClass( 'fade show' );
                }
            },
            error: function( data ) {
                console.log( data );
                notificationElement.text( 'There has been an Ajax error, see console for details.' );
                notificationElement.removeClass( 'hidden' ).addClass( 'fade show' );
            }
        });

    }

    // Overdue Payout Recheck with Stripe
    function nanoOverdueRecheck() {

        var notificationElement = jQuery( '#nano_overdue_alert' );
        var dataToSend = {
            action: 'nano_ajax_payout_overdue_recheck',
            security: jQuery( '#nano_project_payout_metabox' ).val(),
            event_id: jQuery( '#over_due_field' ).val(),
            post_id: jQuery( '#post_ID' ).val(),
        };

        jQuery.ajax({
            type: 'POST',
            dataType: 'json',
            url: ajaxurl,
            data: dataToSend,
            success: function( data ) {
                console.log( 'ajax success' );
                console.log( data.stripeError );
                if ( data.stripeError === false ) {
                    jQuery('#over_due_submit').text( 'Success' ).prop( 'disabled', true );
                    notificationElement.html( data.message );
                    notificationElement.removeClass( 'hidden' ).addClass( 'fade show' );
                } else {
                    jQuery('#over_due_submit').text( 'Recheck Amnount' );
                    notificationElement.html( data.message );
                    notificationElement.removeClass( 'hidden' ).addClass( 'fade show' );
                }
            },
            error: function( data ) {
                console.log( 'error' );
                console.log( data.responseText );
                notificationElement.text( 'There has been an Ajax error, see console for details.' );
                notificationElement.removeClass( 'hidden' ).addClass( 'fade show' );
            }
        });

    }

    // Read $_GET URL parameters
    getUrlParameter = function getUrlParameter( sParam ) {

        var sPageURL = decodeURIComponent( window.location.search.substring( 1 ) ),
            sURLVariables = sPageURL.split( '&' ),
            sParameterName,
            i;

        for ( i = 0; i < sURLVariables.length; i++ ) {
            sParameterName = sURLVariables[ i ].split( '=' );

            if ( sParameterName[ 0 ] === sParam ) {
                return sParameterName[ 1 ] === undefined ? true : sParameterName[ 1 ];
            }
        }
    };

    if ( overdueElement.length ) {

        jQuery('#over_due_submit').on( 'click', function( e ) {
            jQuery( this ).text( 'Processing...' );
            e.preventDefault();
            nanoOverdueRecheck();
        });

    }

    if ( firstPayoutSubmit.length ) {

        if ( ! nano_transfer.first_transfer_status.length || nano_transfer.first_transfer_status === '' ) {
            firstPayoutSubmit.on( 'click', function( e ) {
                var buttonName = jQuery( this ).attr('name');
                jQuery( this ).text( 'Processing...' );
                e.preventDefault();
                nanoStripePayout(buttonName);
            });
        } else {
            notificationElement.text( 'These funds have been transferred to the creator.' );
            notificationElement.removeClass( 'hidden' ).addClass( 'fade show' );
        }

        if ( ! nano_transfer.final_transfer_status.length || nano_transfer.final_transfer_status === '' ) {
            jQuery( '#final_payout_submit' ).on( 'click', function( e ) {
                var buttonName = jQuery( this ).attr('name');
                jQuery( this ).text( 'Processing...' );
                e.preventDefault();
                nanoStripePayout(buttonName);
            });
        } else {
            notificationElement.text( 'These funds have been transferred to the creator.' );
            notificationElement.removeClass( 'hidden' ).addClass( 'fade show' );
        }
    }

    if ( paywithCredits.length || payWithStripe.length ) {
        projectID = getUrlParameter( 'mdid_checkout' );
        form = jQuery( '#payment-form' );
        dataProduct = projectID;
        defaultProjectID = nano_transfer.default_project_id;

        // CHECKOUT PAGE FUNCTIONAL ADJUSTMENTS
        // Change thge product ID in the form data attributed depending
        // on whether the backer chooses to use their credit card or Tokens (if Available)
        paywithCredits.on( 'click', function( e ) {
            form.data( 'product', dataProduct );
            form.attr( 'data-product', dataProduct );
            form.data( 'txn-type', 'preauth' );
            form.attr( 'data-txn-type', 'preauth' );
        });

        payWithStripe.on( 'click', function( e ) {

            form.data( 'product', ( defaultProjectID ) );
            form.attr( 'data-product', ( defaultProjectID ) );
            form.data( 'txn-type', 'capture' );
            form.attr( 'data-txn-type', 'capture' );
            jQuery( 'input[name="project_id"]' ).val( defaultProjectID );

        });

        if ( ! jQuery( '#pay-with-credits' ).length ) {
            form.data( 'product', ( defaultProjectID ) );
            form.attr( 'data-product', ( defaultProjectID ) );
            form.data( 'txn-type', 'capture' );
            form.attr( 'data-txn-type', 'capture' );
            jQuery( 'input[name="project_id"]' ).val( defaultProjectID );
        }

    }

});
