<?php if ( ! defined( 'ABSPATH' ) ) {
	die( 'Direct access forbidden.' );
}

/**
 * Checks whether we're on staging or live and loads the appropriate script.
 * UNUSED
 */
function ab_load_stripe_scripts() {
	global $stripe_options;

	if ( ! empty( $stripe_options ) ) {
		if ( is_array( $stripe_options ) ) {
			$test = $stripe_options['test'];
			if ( 1 == $test ) {
				$publishable = $stripe_options['tpk'];
			} else {
				$publishable = $stripe_options['pk'];
			}
		}
	}
}

add_action( 'wp_enqueue_scripts', 'ab_load_stripe_scripts' );

/**
 * For each backer transaction that is from a credit card we transfer the nanosteam FEE to the platform account and then
 * transfer the remaining amount to the connected nanosteam account tied into the holding account for project payouts.
 *
 * @param $order
 * @param $post_id
 * @param $stripe_charge_id
 */

function nanosteam_fee_payout_transfer( $order, $post_id, $stripe_charge_id ) {

	$sc_settings = get_option( 'md_sc_settings' );
	$sc_settings = maybe_unserialize( $sc_settings );

	$total_before_fee = ID_Member_Order::get_order_meta( $order->id, 'pwyw_price' );
	$stripe_fee       = ( ID_Member_Order::get_order_meta( $order->id, 'stripe_fee', true ) ) / 100;
	$fee_percentage   = $sc_settings['app_fee'] / 100;
	$nanosteam_fee    = number_format( $total_before_fee * $fee_percentage, 2 );
	$total_fee        = $nanosteam_fee + $stripe_fee;
	$final_after_fee  = $total_before_fee - $total_fee;

	// Setting up our stripe transfers

	$settings = get_option( 'memberdeck_gateways' );

	if ( ! empty( $settings ) ) {
		if ( is_array( $settings ) ) {
			$test = $settings['test'];
			if ( 1 == $test ) {
				$key = $settings['tsk'];
				$dev = true;
			} else {
				$key = $settings['sk'];
				$dev = false;
			}
		}
	}

	if ( ! empty( $key ) ) {
		if ( ! class_exists( 'Stripe\Stripe' ) ) {
			require_once IDC_PATH . 'lib/stripe-php-4.2.0/init.php';
		}

		try {

			//Project Parameters
			$project_id     = get_post_meta( $post_id, 'project_uid', true );
			$ign_project_id = get_post_meta( $post_id, 'ign_project_id', true );
			$project_name   = nano_get_project_name( $ign_project_id );
			$project_url    = get_permalink( $post_id );

			// Creator Parameters
			$creator_id      = get_post_meta( $post_id, 'original_author', true );
			$creator_id      = false !== get_user_by( 'login', $creator_id ) ? get_user_by( 'login', $creator_id ) : get_user_by( 'email', $creator_id );
			$creator_id      = $creator_id->ID;
			$user_parameters = nano_get_sc_params( $creator_id );

			// Platform Account Parameters
			$master_parameters     = get_master_sc_params();
			$master_stripe_account = $master_parameters->id;

			// Admin Payment (Deposit) Account
			$stripe_deposit_account = get_option( 'nano_deposit_account' );

			$message = 'Fee transfer for project ID: ' . $project_id;

			// Set the API Version & Key
			\Stripe\Stripe::setApiVersion( NANO_STRIPE_API_VERSION );
			\Stripe\Stripe::setApiKey( $key );

			// Transfer the nanosteam FEE amount to to the platofrm's ACCT.

			$fee_transfer = \Stripe\Payout::create( array(
				'amount'      => $nanosteam_fee * 100, // * 100 for stripe to account properly. No decimals for them.
				'currency'    => 'usd',
				'description' => $message,
				'metadata'    => array(
					'project_id'            => $project_id,
					'stripe_transaction_id' => $stripe_charge_id,
					'nano_creator_id'       => $creator_id,
					'total_funded'          => $total_before_fee,
					'total_fee'             => $total_fee,
					'nanosteam_fee'         => $nanosteam_fee,
					'stripe_fee'            => $stripe_fee,
					'project_name'          => $project_name,
					'project_url'           => $project_url,
				),
			) );

			// Transfer the creator's funds remaining amount to the nanosteam "driver" account.

			$message = 'Creator fund transfer for project ID: ' . $project_id;

			// Set the API Version & Key
			\Stripe\Stripe::setApiVersion( NANO_STRIPE_API_VERSION );
			\Stripe\Stripe::setApiKey( $key );

			$main_transfer = \Stripe\Transfer::create( array(
				'amount'             => $final_after_fee * 100,
				// * 100 for stripe to account properly. No decimals for them.
				'currency'           => 'usd',
				'destination'        => $stripe_deposit_account,
				'description'        => $message,
				'source_transaction' => $stripe_charge_id,
				'metadata'           => array(
					'project_id'            => $project_id,
					'stripe_transaction_id' => $stripe_charge_id,
					'nano_creator_id'       => $creator_id,
					'total_funded'          => $total_before_fee,
					'total_fee'             => $total_fee,
					'nanosteam_fee'         => $nanosteam_fee,
					'stripe_fee'            => $stripe_fee,
					'project_name'          => $project_name,
					'project_url'           => $project_url,
				),
			) );

			//send_error( $fee_transfer );
			//send_error( $main_transfer );

		} catch ( \Stripe\Error\Card $e ) {
			// Since it's a decline, \Stripe\Error\Card will be caught
			send_error( $e );
			stripe_error_email_admin_notification( $e, 'nanosteam_fee_payout_transfer' );

		} catch ( \Stripe\Error\RateLimit $e ) {
			// Too many requests made to the API too quickly
			send_error( $e );
			stripe_error_email_admin_notification( $e, 'nanosteam_fee_payout_transfer' );

		} catch ( \Stripe\Error\InvalidRequest $e ) {
			// Invalid parameters were supplied to Stripe's API
			send_error( $e );
			stripe_error_email_admin_notification( $e, 'nanosteam_fee_payout_transfer' );

		} catch ( \Stripe\Error\Authentication $e ) {
			// Authentication with Stripe's API failed
			// (maybe you changed API keys recently)
			send_error( $e );
			stripe_error_email_admin_notification( $e, 'nanosteam_fee_payout_transfer' );

		} catch ( \Stripe\Error\ApiConnection $e ) {
			// Network communication with Stripe failed
			send_error( $e );
			stripe_error_email_admin_notification( $e, 'nanosteam_fee_payout_transfer' );

		} catch ( \Stripe\Error\Base $e ) {
			// Display a very generic error to the user, and maybe send
			// yourself an email
			send_error( $e );
			stripe_error_email_admin_notification( $e, 'nanosteam_fee_payout_transfer' );

		} catch ( Exception $e ) {
			// Something else happened, completely unrelated to Stripe
			send_error( $e );
			stripe_error_email_admin_notification( $e, 'nanosteam_fee_payout_transfer' );
		}
	}
}


/**
 * Once nano_stripe_payment_status_meta_data($post_id) runs, the Stripe transaction will be sent the project's UID for tracking.
 *
 * @param $order
 * @param $stripe_charge_id
 * @param $project_uid
 * @param $project_state
 */

function nano_stripe_transaction_update( $order, $stripe_charge_id, $project_uid, $project_state ) {

	if ( 'previously refunded credits' === strtolower( $stripe_charge_id ) ) {
		send_error('Previously Refunded Credits when initiating nano_stripe_transaction_update');
		return;
	}

	$settings = get_option( 'memberdeck_gateways' );

	if ( ! empty( $settings ) ) {
		if ( is_array( $settings ) ) {
			$test = $settings['test'];
			if ( 1 == $test ) {
				$key = $settings['tsk'];
			} else {
				$key = $settings['sk'];
			}
		}
	}

	if ( ! empty( $key ) ) {
		if ( ! class_exists( 'Stripe\Stripe' ) ) {
			require_once IDC_PATH . 'lib/stripe-php-4.2.0/init.php';
		}
		$stripe_api_version = '2017-08-15';

		try {

			// Set the API Version & Key
			\Stripe\Stripe::setApiVersion( NANO_STRIPE_API_VERSION );
			\Stripe\Stripe::setApiKey( $key );

			// Get the user's account
			$ch = \Stripe\Charge::retrieve( $stripe_charge_id );

			$balance_transaction = json_encode( $ch );
			$balance_transaction = json_decode( $balance_transaction );

			if ( 'LIVE' == $project_state ) {
				$ch->metadata->project_id = $project_uid;
			}
			if ( 'EXPIRED' == $project_state ) {

				$ch->metadata->project_id = $project_uid . '-t';

				// Getting User Data
				global $wpdb;

				$charset_collate = $wpdb->get_charset_collate();
				$table_name      = $wpdb->prefix . 'nano_token_meta_data';
				if ( $wpdb->get_var( "SHOW TABLES LIKE '$table_name'" ) == $table_name ) {
					$sql                      = "SELECT * FROM $table_name WHERE stripe_txn_id LIKE %s";
					$sql                      = $wpdb->prepare( $sql, $stripe_charge_id );
					$refunded_tokens          = $wpdb->get_results( $sql );
					$ch->metadata->unused_amt = $refunded_tokens[0]->tokens_unused;
				}
			}
			if ( 'REALLOCATED' == $project_state ) {
				$ch->metadata->project_id_new = $project_uid;
			}
			$ch->save();

		} catch ( \Stripe\Error\Card $e ) {
			// Since it's a decline, \Stripe\Error\Card will be caught
			send_error( 'Error From : nano_stripe_transaction_update \ card' );
			send_error( $e );
			stripe_error_email_admin_notification( $e, 'nano_stripe_transaction_update' );

		} catch ( \Stripe\Error\RateLimit $e ) {
			// Too many requests made to the API too quickly
			send_error( 'Error From : nano_stripe_transaction_update \ RateLimit' );
			send_error( $e );
			stripe_error_email_admin_notification( $e, 'nano_stripe_transaction_update' );

		} catch ( \Stripe\Error\InvalidRequest $e ) {
			// Invalid parameters were supplied to Stripe's API
			send_error( 'Error From : nano_stripe_transaction_update \ Invalid Request' );
			send_error( $e );
			stripe_error_email_admin_notification( $e, 'nano_stripe_transaction_update' );

		} catch ( \Stripe\Error\Authentication $e ) {
			// Authentication with Stripe's API failed
			// (maybe you changed API keys recently)
			send_error( 'Error From : nano_stripe_transaction_update \ Authentication' );
			send_error( $e );
			stripe_error_email_admin_notification( $e, 'nano_stripe_transaction_update' );

		} catch ( \Stripe\Error\ApiConnection $e ) {
			// Network communication with Stripe failed
			send_error( $e );
			stripe_error_email_admin_notification( $e, 'nano_stripe_transaction_update' );

		} catch ( \Stripe\Error\Base $e ) {
			// Display a very generic error to the user, and maybe send
			// yourself an email
			send_error( 'Error From : nano_stripe_transaction_update \ Base' );
			send_error( $e );
			stripe_error_email_admin_notification( $e, 'nano_stripe_transaction_update' );

		} catch ( Exception $e ) {
			// Something else happened, completely unrelated to Stripe
			send_error( 'Error From : nano_stripe_transaction_update \ Exception' );
			send_error( $e );
			stripe_error_email_admin_notification( $e, 'nano_stripe_transaction_update' );
		}

		// Once we've updated the Project UID we'll snag the balance transaction stripe fee amount and add it to our order meta.
		if ( isset( $balance_transaction->balance_transaction ) && isset( $order ) && 'EXPIRED' != $project_state ) {

			try {

				// Set the API Version & Key
				\Stripe\Stripe::setApiVersion( NANO_STRIPE_API_VERSION );
				\Stripe\Stripe::setApiKey( $key );

				// retrieve the transaction balance
				$sbtr = \Stripe\BalanceTransaction::retrieve( $balance_transaction->balance_transaction );

			} catch ( \Stripe\Error\Card $e ) {
				// Since it's a decline, \Stripe\Error\Card will be caught
				send_error( 'Error From : nano_stripe_transaction_update \ Balance\Card' );
				send_error( $e );
				stripe_error_email_admin_notification( $e, 'nano_stripe_transaction_update' );

			} catch ( \Stripe\Error\RateLimit $e ) {
				// Too many requests made to the API too quickly
				send_error( 'Error From : nano_stripe_transaction_update \ Balance\Ratelimite' );
				send_error( $e );
				stripe_error_email_admin_notification( $e, 'nano_stripe_transaction_update' );

			} catch ( \Stripe\Error\InvalidRequest $e ) {
				// Invalid parameters were supplied to Stripe's API
				send_error( 'Error From : nano_stripe_transaction_update \ Balance\InvalidReq' );
				send_error( $e );
				stripe_error_email_admin_notification( $e, 'nano_stripe_transaction_update' );

			} catch ( \Stripe\Error\Authentication $e ) {
				// Authentication with Stripe's API failed
				// (maybe you changed API keys recently)
				send_error( 'Error From : nano_stripe_transaction_update \ Balance\Authentication' );
				send_error( $e );
				stripe_error_email_admin_notification( $e, 'nano_stripe_transaction_update' );

			} catch ( \Stripe\Error\ApiConnection $e ) {
				// Network communication with Stripe failed
				send_error( 'Error From : nano_stripe_transaction_update \ Balance\ApiConnection' );
				send_error( $e );
				stripe_error_email_admin_notification( $e, 'nano_stripe_transaction_update' );

			} catch ( \Stripe\Error\Base $e ) {
				// Display a very generic error to the user, and maybe send
				// yourself an email
				send_error( 'Error From : nano_stripe_transaction_update \ Blalance\Base' );
				send_error( $e );
				stripe_error_email_admin_notification( $e, 'nano_stripe_transaction_update' );

			} catch ( Exception $e ) {
				// Something else happened, completely unrelated to Stripe
				send_error( 'Error From : nano_stripe_transaction_update \ Balance\Exception' );
				send_error( $e );
				stripe_error_email_admin_notification( $e, 'nano_stripe_transaction_update' );
			}

			$stripe_fee = json_encode( $sbtr );
			$stripe_fee = json_decode( $stripe_fee );
			$stripe_fee = $stripe_fee->fee;

			// Adding the actual stripe fee to the order for processing later.
			ID_Member_Order::update_order_meta( $order->id, 'stripe_fee', $stripe_fee );

		}
	}
}


/**
 * Once nano_stripe_payment_status_meta_data($post_id) runs, for each transaction that needs to be
 * updated with the project UID, do it here.
 *
 * @param $order
 * @param $stripe_charge_id
 * @param $project_uid
 * @param $project_state
 */

function nano_stripe_expired_amount( $order, $stripe_charge_id, $project_uid, $project_state ) {

	$settings = get_option( 'memberdeck_gateways' );

	if ( ! empty( $settings ) ) {
		if ( is_array( $settings ) ) {
			$test = $settings['test'];
			if ( 1 == $test ) {
				$key = $settings['tsk'];
			} else {
				$key = $settings['sk'];
			}
		}
	}

	if ( ! empty( $key ) ) {
		if ( ! class_exists( 'Stripe\Stripe' ) ) {
			require_once IDC_PATH . 'lib/stripe-php-4.2.0/init.php';
		}
		$stripe_api_version = '2017-08-15';

		try {

			// Set the API Version & Key
			\Stripe\Stripe::setApiVersion( NANO_STRIPE_API_VERSION );
			\Stripe\Stripe::setApiKey( $key );

			// Get the user's account
			$ch = \Stripe\Charge::retrieve( $stripe_charge_id );

			$balance_transaction = json_encode( $ch );
			$balance_transaction = json_decode( $balance_transaction );

			if ( 'LIVE' == $project_state ) {
				$ch->metadata->project_id = $project_uid;
			}
			if ( 'EXPIRED' == $project_state ) {
				$ch->metadata->project_id = $project_uid . '-t';
			}
			if ( 'REALLOCATED' == $project_state ) {
				$ch->metadata->project_id_new = $project_uid;
			}
			$ch->save();

		} catch ( \Stripe\Error\Card $e ) {
			// Since it's a decline, \Stripe\Error\Card will be caught
			send_error( $e );

		} catch ( \Stripe\Error\RateLimit $e ) {
			// Too many requests made to the API too quickly
			send_error( $e );

		} catch ( \Stripe\Error\InvalidRequest $e ) {
			// Invalid parameters were supplied to Stripe's API
			send_error( $e );

		} catch ( \Stripe\Error\Authentication $e ) {
			// Authentication with Stripe's API failed
			// (maybe you changed API keys recently)
			send_error( $e );

		} catch ( \Stripe\Error\ApiConnection $e ) {
			// Network communication with Stripe failed
			send_error( $e );

		} catch ( \Stripe\Error\Base $e ) {
			// Display a very generic error to the user, and maybe send
			// yourself an email
			send_error( $e );

		} catch ( Exception $e ) {
			// Something else happened, completely unrelated to Stripe
			send_error( $e );
		}
	}
}


/**
 * After a project is approved we need to update it's payment status during its lifecycle
 *
 * @param $post_id
 * @param $new_order
 * @param $user_id
 */

function nano_stripe_payment_status_meta_data( $post_id, $new_order, $user_id ) {

	$project_state = nano_funding_status( $post_id );

	// Project Data
	$project_uid = '' !== get_post_meta( $post_id, 'project_uid', true ) ? get_post_meta( $post_id, 'project_uid', true ) : 'No UID for post_id: ' . $post_id;
	$level_id    = nano_get_level_id( get_post_meta( $post_id, 'ign_project_id', true ) );
	$orders      = ID_Member_Order::get_orders_by_level( $level_id );
	$the_order   = new ID_Member_Order( $new_order );
	$order       = $the_order->get_order();
	$order_price = ID_Member_Order::get_order_meta( $new_order, 'pwyw_price' );
	$payments    = array();

	if ( ! empty( $order ) ) {

		// Payment Method
		$payment_method = false !== strpos( $order->transaction_id, 'nano-' ) ? 'cash' : 'no cash';

		if ( 'cash' == $payment_method ) {

			$stripe_charge_id = explode( 'nano-', $order->transaction_id );
			if ( is_array( $stripe_charge_id ) ) {
				$stripe_charge_id = $stripe_charge_id[1];
			}

			// We need to update stripe with ...

			send_error( 'nano_stripe_transaction_update' );
			nano_stripe_transaction_update( $order, $stripe_charge_id, $project_uid, $project_state );

			// nano_stripe_transaction_update updates our orders stripe fee so we need to run the payout and transfer function
			// after. We process each new charge and send the fee & creator amount to their appropriate nanosteam bank accounts
			// for later distribution.

			nanosteam_fee_payout_transfer( $order, $post_id, $stripe_charge_id );

		} elseif ( 'no cash' == $payment_method ) {

			global $wpdb;
			$charset_collate = $wpdb->get_charset_collate();
			$table_name      = $wpdb->prefix . 'nano_token_meta_data';

			$refunded_tokens = $wpdb->get_results( "SELECT * FROM $table_name WHERE user_id = $order->user_id" );

			$total_refunded = 0;
			$total_unused   = 0;
			foreach ( $refunded_tokens as $refunded_token ) {
				if ( isset( $refunded_token->tokens_unused ) && 0 != $refunded_token->tokens_unused && 'expired' != $refunded_token->has_expired ) {
					$refunded_amount = $refunded_token->token_amount;
					$total_refunded  += $refunded_amount;
					$refund_unused   = $refunded_token->tokens_unused;
					$total_unused    += $refund_unused;
				}
			}
			$total_unused = (string) $total_unused;

			// Get the user's tokens.
			$member     = new ID_Member( $user_id );
			$md_credits = $member->get_user_credits();

			$difference = $total_refunded - $md_credits;

			$sub_total_refunded = 0;
			$count              = 0;

			foreach ( $refunded_tokens as $key => $refunded_token ) {

				if ( ( isset( $refunded_token->tokens_unused ) && 0 != $refunded_token->tokens_unused && 'expired' != $refunded_token->has_expired  ) ) {

					$token_used_date = $refunded_token->token_used_date;
					$todays_date     = nano_get_formated_date_time_gmt( 'm/d/Y H:i:s' );
					$post_id         = $refunded_token->post_id;
					$project_state   = nano_funding_status( $post_id );

					$stripe_charge_id = $refunded_token->stripe_txn_id;
					if ( false === strpos( strtoupper( $stripe_charge_id ), 'PREVIOUSLY' ) && false === strpos( strtoupper( $stripe_charge_id ), 'NASNOSTEAM' ) ) {
						nano_stripe_transaction_update( null, $stripe_charge_id, $project_uid, $project_state );
					}

					$user_id             = $refunded_token->user_id;
					$post_id             = $refunded_token->post_id;
					$project_id          = $refunded_token->idk_project_id;
					$nano_project_id     = $refunded_token->nano_project_uid;
					$order_id            = $refunded_token->order_id;
					$refunded_amount     = $refunded_token->token_amount;
					$stripe_fee          = $refunded_token->stripe_fee;
					$token_refund_date   = $refunded_token->token_refund_date;
					$token_expiry_date   = $refunded_token->token_expiry_date;
					$expiry_check        = $refunded_token->has_expired;
					$expiry_check_org    = $refunded_token->has_expired;
					$available           = $refunded_token->tokens_unused;
					$org_token_used_date = $refunded_token->token_used_date;
					$refund_unused       = $refunded_token->tokens_unused;

					$new_project_id_with_order_id = $project_uid . '/' . $new_order;
					$nano_new_project_uid         = array( $new_project_id_with_order_id );
					$nano_org_new_project_uid     = isset( $refunded_token->nano_new_project_uid ) ? $refunded_token->nano_new_project_uid : '';

					// Creating an array for the project UIDs if the backer supports several projects from a single refund
					if ( '' != $nano_org_new_project_uid ) {
						$nano_org_new_project_uid = maybe_unserialize( $nano_org_new_project_uid );
						if ( is_array( $nano_org_new_project_uid ) ) {
							$nano_new_project_uid = array_unique( array_merge( $nano_org_new_project_uid, $nano_new_project_uid ) );
						}
					}

					$nano_new_project_uid     = serialize( $nano_new_project_uid );
					$nano_org_new_project_uid = serialize( $nano_org_new_project_uid );

					// Checking the difference on the order
					if ( $difference > 0 ) {
						if ( $difference >= $refunded_amount ) {
							$available       = 0;
							$difference      = $difference - $refunded_amount;
							$token_used_date = isset( $refunded_token->token_used_date ) && empty( $refunded_token->token_used_date ) == false ? $refunded_token->token_used_date : nano_get_formated_date_time_gmt( 'Y-m-d H:i:s' );
						} elseif ( $difference < $refunded_amount ) {
							$available       = $refunded_amount - $difference;
							$difference      = 0;
							$token_used_date = isset( $refunded_token->token_used_date ) && empty( $refunded_token->token_used_date ) == false ? $refunded_token->token_used_date : nano_get_formated_date_time_gmt( 'Y-m-d H:i:s' );
						}
					}
					if ( ( 0 != $refund_unused && $total_unused != $md_credits ) && $refunded_amount != $available ) {

						nano_update_user_token_meta(
							$user_id, $post_id, $project_id, $nano_project_id, $stripe_charge_id, $order_id,
							$refunded_amount, $token_refund_date, $token_expiry_date, $token_used_date,
							$nano_new_project_uid, $nano_org_new_project_uid, $expiry_check, $expiry_check_org,
							$available, $org_token_used_date, $refund_unused, $stripe_fee
						);

					}
				}
			}
		}
	}
}


/**
 * Add Meta Data to a creators project on submit for accounting purposes
 * https://docs.google.com/document/d/1NTGpkb5Vx1VGCyS1Hv9ul7fiQYa8IOq54Tihi-8uJ7I/edit?usp=sharing
 * Each time a project is approved we add it to the nano_meta_data table for Project tracking
 *
 * @param $post_id
 * @param $post_after
 * @param $post_before
 */

function nano_save_accounting_meta_data( $post_id, $post_after, $post_before ) {

	$status = 'publish' == $post_after->post_status && 'pending' == $post_before->post_status ? true : false;

	if ( get_post_type( $post_id ) == 'ignition_product' ) {
		if ( true == $status ) {
			if ( current_user_can( 'administrator' ) ) {

				// Create the table if it doesn't exist
				global $wpdb;

				$charset_collate = $wpdb->get_charset_collate();
				$table_name      = $wpdb->prefix . 'nano_meta_data';

				$sql = "CREATE TABLE $table_name (
                id smallint(9) NOT NULL AUTO_INCREMENT,
                time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
                type varchar(255) NOT NULL,
                post_id bigint(20) NOT NULL,
                type_id varchar(255) NOT NULL,
                creator_stripe_id varchar(255) NOT NULL,
                creator_id bigint(20) NOT NULL,
                project_uid varchar(255) NOT NULL,
                project_status longtext NOT NULL,
                payments longtext NOT NULL,
                UNIQUE KEY id (id) ) $charset_collate;";

				require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

				maybe_create_table( $table_name, $sql );

				// Determine whether the project is challenge, matching or "all or nothing".

				$type                   = '';
				$nano_fifty_grant_apply = get_post_meta( $post_id, 'nano_fifty_grant_apply', true );
				$challenge_grant        = get_post_meta( $post_id, 'nano_challenge_grant', true );
				$original_author        = get_post_meta( $post_id, 'original_author', true );
				$project_creator        = false !== get_user_by( 'login', $original_author ) ? get_user_by( 'login', $original_author ) : get_user_by( 'email', $original_author );
				$user_id                = $project_creator->ID;
				$stripe_parameters      = get_sc_params( $user_id );
				$creator_stripe_id      = isset( $stripe_parameters->stripe_user_id ) ? $stripe_parameters->stripe_user_id : '';
				$datetime               = nano_get_formated_date_time_gmt( 'Y-m-d h:i:sa' );
				$post_date              = get_the_date( 'mdy', $post_id );
				$date_expiration        = get_post_meta( $post_id, 'ign_fund_end', true );
				$date_posted            = get_the_date( 'm/d/Y', $post_id );

				if ( 'no' == $nano_fifty_grant_apply || '' == $nano_fifty_grant_apply ) {
					$type = 'A';
				}

				if ( 'yes' == $nano_fifty_grant_apply ) {
					$type = 'M';
				}

				if ( 'yes' == $challenge_grant ) {
					$type = 'C';
				}

				// Adding an order number to each type starting at 000
				$sql     = "SELECT * FROM $table_name WHERE type LIKE %s";
				$sql     = $wpdb->prepare( $sql, $type );
				$results = $wpdb->get_results( $sql );

				if ( isset( $results ) ) {
					$type_id = count( $results ) + 1;
				} else {
					$type_id = '0001';
				}

				$num_length = strlen( (string) $type_id );

				if ( 1 == $num_length ) {
					$type_id = '000' . $type_id;
				} elseif ( 2 == $num_length ) {
					$type_id = '00' . $type_id;
				} elseif ( 3 == $num_length ) {
					$type_id = '0' . $type_id;
				}

				$project_uid = $post_date . $type . $type_id;

				$old_project_uid = get_post_meta( $post_id, 'project_uid', true );

				if ( '' == $old_project_uid ) {

					delete_post_meta( $post_id, 'project_uid' );
					add_post_meta( $post_id, 'project_uid', $project_uid );

					// Save the data to the DB

					$project_status = serialize(
						array(
							'funding_status'  => 'LIVE',  // Completed, Expired, Live, or Reallocated
							'date_posted'     => $date_posted,
							'date_completed'  => '',
							'date_of_2nd_lab' => '',
							'date_expiration' => $date_expiration,
						)
					);
					$payments       = serialize(
						array(
							'payment_id'      => '',
							'transaction_id'  => '', // Stripe or Token
							'payment_method'  => '', // Can be CASH or
							'donor_id'        => '', // Local user ID
							'donor_stripe_id' => '', // Stripe customer ID
						)
					);

					$wpdb->insert( $table_name, array(
						'time'              => $datetime,
						'type'              => $type,
						'post_id'           => $post_id,
						'creator_id'        => $project_creator->ID,
						'creator_stripe_id' => $creator_stripe_id,
						'project_uid'       => $project_uid,
						'project_status'    => $project_status,
						'payments'          => $payments,
						'type_id'           => $type_id,
					) );

				}

				// Need to get the DB again now that we've added this project.
				$new_results = $wpdb->get_results( $sql );

				// Connecting to stripe now

				if ( ! empty( $stripe_parameters ) ) {
					if ( ! class_exists( 'Stripe\Stripe' ) ) {
						require_once IDC_PATH . 'lib/stripe-php-4.2.0/init.php';
					}
					$stripe_api_version = '2017-08-15';
					$api_key            = $stripe_parameters->access_token;
					$stripe_user_id     = $stripe_parameters->stripe_user_id;

					try {

						$error  = 'Account Updated.';
						$status = 'success';

						$projects = '';
						//$results = unique_multidim_array($results, 'project_uid');
						foreach ( $new_results as $result ) {
							if ( $user_id == $result->creator_id ) {
								$projects .= $result->project_uid . ( $type_id > 1 ? ', ' : ' ' );
							}
						}

						// Edge case but cropped up when testing during development
						if ( 500 < strlen( $projects ) ) {
							$projects = 'This user has created too many projects for Stripe to hold the projects here. Please refer to nano_meta_data table in DB and search for creator_id : ' . $result->creator_id;
						}

						// Set the API key
						\Stripe\Stripe::setApiVersion( NANO_STRIPE_API_VERSION );
						\Stripe\Stripe::setApiKey( $api_key );

						// Get the user's account
						$stripe_account = \Stripe\Account::retrieve( $stripe_user_id );

						// Clean up the supplied php array.
						$sc_params = $stripe_account->__toJSON();
						$sc_params = json_decode( $sc_params );

						if ( 'A' == $type ) {
							$stripe_account->metadata->all_in_projects = $projects;
						}
						if ( 'M' == $type ) {
							$stripe_account->metadata->matching_projects = $projects;
						}
						if ( 'C' == $type ) {
							$stripe_account->metadata->challenge_projects = $projects;
						}

						$stripe_account->save();

					} catch ( \Stripe\Error\Card $e ) {
						// Since it's a decline, \Stripe\Error\Card will be caught
						stripe_error_email_admin_notification( $e, 'Card nano_save_accounting_meta_data' );

					} catch ( \Stripe\Error\RateLimit $e ) {
						// Too many requests made to the API too quickly
						stripe_error_email_admin_notification( $e, 'RateLimit nano_save_accounting_meta_data' );

					} catch ( \Stripe\Error\InvalidRequest $e ) {
						// Invalid parameters were supplied to Stripe's API
						stripe_error_email_admin_notification( $e, 'InvalidRequest nano_save_accounting_meta_data' );

					} catch ( \Stripe\Error\Authentication $e ) {
						// Authentication with Stripe's API failed
						// (maybe you changed API keys recently)
						stripe_error_email_admin_notification( $e, 'Authentication nano_save_accounting_meta_data' );

					} catch ( \Stripe\Error\ApiConnection $e ) {
						// Network communication with Stripe failed
						stripe_error_email_admin_notification( $e, 'ApiConnection nano_save_accounting_meta_data' );

					} catch ( \Stripe\Error\Base $e ) {
						// Display a very generic error to the user, and maybe send
						// yourself an email
						stripe_error_email_admin_notification( $e, 'Base nano_save_accounting_meta_data' );

					} catch ( Exception $e ) {
						// Something else happened, completely unrelated to Stripe
						stripe_error_email_admin_notification( $e, 'Exception nano_save_accounting_meta_data' );

					}
				} else {
					// If somehow there was a project created but the creator has not created a stripe connect account note it here.
					$user_link = get_author_posts_url( $user_id );
					$user_name = get_user_by( 'id', $user_id );

					$subject  = 'Stripe meta data unable to update for creator.';
					$message  = ( 'The creator:' . $user_name->first_name . ' ' . $user_name->last_name . ' has created a project without an accompanying Stripe connect account setup.' . "\n" );
					$message .= ( 'User ID: ' . $user_id . "\n" );
					$message .= ( 'User URL: ' . $user_link . "\n" );
					$message .= ( 'Project URL: ' . get_permalink( $post_id ) . "\n" );
					$message .= ( 'Error from function: nano_save_accounting_meta_data( $post_id, $post_after, $post_before )' . "\n" );

					email_general_admin_notification( $subject, $message );
				}
			}
		}
	}
}

add_action( 'post_updated', 'nano_save_accounting_meta_data', 10, 3 );
