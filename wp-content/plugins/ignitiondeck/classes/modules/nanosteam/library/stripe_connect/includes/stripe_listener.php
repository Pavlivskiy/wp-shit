<?php if ( ! defined( 'ABSPATH' ) ) {
	die( 'Direct access forbidden.' );
}

/**
 * This is fired when we receive webhooks from Stripe
 * Webhooks we currently listen for.
 *  - account.external_account.created - needs to be setup in "Endpoints receiving events from Connect applications"
 *  - account.updated - needs to be setup in "Endpoints receiving events from Connect applications"
 *  - payout.paid - needs to be setup in "Endpoints receiving events from Connect applications"
 *  - balance.available - needs to be setup in "Endpoints receiving events from your account"
 */
function nano_stripe_event_listener() {

	if ( isset( $_GET['nano_stripe'] ) && 'stripe' == $_GET['nano_stripe'] ) {

		$stripe_options = get_option( 'memberdeck_gateways' );

		if ( ! class_exists( 'Stripe\Stripe' ) ) {
			require_once IDC_PATH . 'lib/stripe-php-4.2.0/init.php';
		}

		if ( ! empty( $stripe_options ) ) {
			if ( is_array( $stripe_options ) ) {
				$test = $stripe_options['test'];
				// If in test env
				if ( 1 == $test ) {
					$secret_key = $stripe_options['tsk'];
					$whsec_key  = '';
					if ( 'nanosteam.site' == $_SERVER['HTTP_HOST'] ) {

						$whsec_key = 'whsec_k56xsMos1uMEx71HfYEEuHgnvj8agnZx';

						if ( isset( $_GET['account'] ) ) {
							$whsec_key = 'whsec_ebDB3y1kDTbD7Iz5PG2wPzgNpMqZm4OR';
						}

					} else {

						$whsec_key = 'whsec_D9QqUVrCLQDGiMohs3b9Fjayg6Tax01y';

						if ( isset( $_GET['account'] ) ) {
							$whsec_key = 'whsec_ymrTPPhfNG2CdU8hKrLvZa2LkqKiFXO0';
						}

					}
				} else {
					$secret_key = $stripe_options['sk'];
					$whsec_key  = '';
					if ( 'nanosteam.staging.wpengine.com' == $_SERVER['HTTP_HOST'] ) {

						$whsec_key = 'whsec_bOmKMOQ0r1weXQXPwcZj51UhQLS3kNqw';

						if ( isset( $_GET['account'] ) ) {
							$whsec_key = 'whsec_IQS6BxkL7zOU2oeGKiRBPST8owOXwclI';
						}

					} else {

						$whsec_key = 'whsec_c9RFdk1rICoG9MTQ4qPST1urSXzaYrZ9';

						if ( isset( $_GET['account'] ) ) {
							$whsec_key = 'whsec_IQS6BxkL7zOU2oeGKiRBPST8owOXwclI';
						}

					}
				}
			}
		}

		/*
		 * https://dashboard.stripe.com/account/webhooks  WEBHOOK SETTINGS
		 * https://github.com/amratab/WPStripeWebhook
		 * https://stripe.com/docs/webhooks
		 *
		 */

		// First we need to verify we're talking to stripe.
		\Stripe\Stripe::setApiKey( $secret_key );

		// You can find your endpoint's secret in your API > webhook settings
		$endpoint_secret = $whsec_key;
		$payload         = @file_get_contents( 'php://input' );

		if ( isset( $_SERVER['HTTP_STRIPE_SIGNATURE'] ) ) {

			$sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];
			$event_test = null;

			try {
				$event_test = \Stripe\Webhook::constructEvent(
					$payload, $sig_header, $endpoint_secret
				);
			} catch ( \UnexpectedValueException $e ) {
				// Invalid payload
				send_error( 'Invalid Payload from Stripe Webhook' );
				send_error( $e );
				http_response_code( 400 ); // PHP 5.4 or greater
				exit();
			} catch ( \Stripe\Error\SignatureVerification $e ) {
				// Invalid signature
				send_error( 'Invalid Signature from Stripe Webhook' );
				send_error( $e );
				http_response_code( 400 ); // PHP 5.4 or greater
				exit();
			}

			// Do something with $event
			// grab the event information
			$event_json = json_decode( $payload );

			if ( isset( $test ) && 1 == $test ) {
				wp_mail( get_option( 'admin_email' ), 'Stripe API Hook Listener', print_r( $event_json, true ) );
			}

			$previous_attr    = isset( $event_json->data->previous_attributes ) ? ( count( (array) $event_json->data->previous_attributes ) > 0 ? true : false ) : true;
			$verification_req = isset( $event_json->data->object->verification->fields_needed ) ? ( count( (array) $event_json->data->object->verification->fields_needed ) > 0 ? true : false ) : true;

			// Platform Account Parameters
			$master_parameters     = get_master_sc_params();
			$master_stripe_account = $master_parameters->id;

			$stripe_debit_account   = get_option( 'nano_debit_account' );
			$stripe_deposit_account = get_option( 'nano_deposit_account' );

			// Admin Debit Account
			$stripe_withdrawl_account = get_option( 'nano_debit_account' );

			try {

				$event_id       = 'evt_00000000000000' != $event_json->id ? $event_json->id : 'evt_1Ch790GPaLlQVE7hquKrC8we';
				$stripe_user_id = isset( $event_json->account ) && 'acct_00000000000000' != $event_json->account ? $event_json->account : $stripe_withdrawl_account;

				if ( ! isset( $event_json->account ) ) {

					$stripe_user_id = $master_stripe_account;

					$event = \Stripe\Event::retrieve(
						array( 'id' => $event_id )
					);

				}

				if ( isset( $event_json->account ) ) {

					$event = \Stripe\Event::retrieve(
						array( 'id' => $event_id ),
						array( 'stripe_account' => $stripe_user_id )
					);

				}
			} catch ( \Stripe\Error\RateLimit $e ) {
				// Too many requests made to the API too quickly
				stripe_error_email_admin_notification( $e, 'RateLimit nano_stripe_event_listener' );
				http_response_code( 400 ); // PHP 5.4 or greater

			} catch ( \Stripe\Error\InvalidRequest $e ) {
				// Invalid parameters were supplied to Stripe's API
				stripe_error_email_admin_notification( $e, 'InvalidRequest nano_stripe_event_listener' );
				http_response_code( 400 ); // PHP 5.4 or greater

			} catch ( \Stripe\Error\Authentication $e ) {
				// Authentication with Stripe's API failed
				// (maybe you changed API keys recently)
				stripe_error_email_admin_notification( $e, 'Authentication nano_stripe_event_listener' );
				http_response_code( 400 ); // PHP 5.4 or greater

			} catch ( \Stripe\Error\ApiConnection $e ) {
				// Network communication with Stripe failed
				stripe_error_email_admin_notification( $e, 'ApiConnection nano_stripe_event_listener' );
				http_response_code( 400 ); // PHP 5.4 or greater

			} catch ( \Stripe\Error\Base $e ) {
				// Display a very generic error to the user, and maybe send
				// yourself an email
				stripe_error_email_admin_notification( $e, 'Base nano_stripe_event_listener' );
				http_response_code( 400 ); // PHP 5.4 or greater

			} catch ( Exception $e ) {
				// Something else happened, completely unrelated to Stripe
				stripe_error_email_admin_notification( $e, 'Exception nano_stripe_event_listener' );
				http_response_code( 400 ); // PHP 5.4 or greater

			}

			if ( 'account.external_account.created' == $event_json->type || 'account.updated' == $event_json->type ) {

				// Set the API Version & Key
				\Stripe\Stripe::setApiVersion( NANO_STRIPE_API_VERSION );

				// Getting User Details from Stripe
				$stripe_customer = \Stripe\Account::retrieve( $stripe_user_id );

				// Get Local User Details
				$local_user_id = $stripe_customer->metadata->nano_user_id;
				$email         = get_user_by( 'id', $local_user_id )->user_email;
				$name          = $stripe_customer->legal_entity->first_name;
				$lname         = $stripe_customer->legal_entity->last_name;
				$from          = get_option( 'admin_email' );

				$settings = get_option( 'md_receipt_settings' );
				if ( ! empty( $settings ) ) {
					$settings = maybe_unserialize( $settings );
					$coname   = apply_filters( 'idc_company_name', $settings['coname'] );
					$coemail  = $settings['coemail'];
				} else {
					$coname  = '';
					$coemail = get_option( 'admin_email', null );
				}
			}

			if ( 'account.external_account.created' == $event_json->type ) {

				$subject = __( 'Nanosteam - Funding account banking notification', 'nanosteam_8n' );

				$message = __( 'Hello', 'nanosteam_8n' ) . ' ' . $name . ', <br /><br />
  
                            ' . __( 'We\'ve recently updated your account information from some recent activity from you on Nanosteam.org.', 'nanosteam_8n' ) . '<br /><br />
                            ' . __( 'If you did not make any changes:', 'nanosteam_8n' ) . '<br /><br />
                            <ol>
                                <li>' . __( 'You should change your password as soon as possible from your account page at', 'nanosteam_8n' ) . ' <a href="' . get_site_url() . '/dashboard/?edit-profile">' . get_site_url() . '/dashboard/?edit-profile</a></li>
                                <li>' . __( 'Outstanding requirements will be highlighted in your <strong>Payment Methods.</strong> You can access your account at', 'nanosteam_8n' ) . ' <a href="' . get_site_url() . '/dashboard/?payment_settings=1">' . get_site_url() . '/dashboard/?payment_settings=1</a></li>
                                <li>' . __( 'If you have any questions, please contact us at ', 'nanosteam_8n' ) . '<a href="mailto:' . $coemail . '">' . $coemail . '</a></li>
                            </ol>
                            ' . __( 'Thank you!', 'nanosteam_8n' ) . '<br />
                            ' . __( 'The', 'nanosteam_8n' ) . ' ' . $coname . ' ' . __( 'team', 'nanosteam_8n' ) . '<br />

                       ' . $coname . '<br />';

				nano_stripe_email( $subject, $message, $email, $name, $lname );

				http_response_code( 200 ); // PHP 5.4 or greater

			} elseif ( 'account.updated' == $event_json->type ) {

				$verification = $event->data->object->verification;

				$date                = $verification->due_by ? ' This is due by: ' . date( 'm/d/Y', $verification->due_by ) . '. ' : '';
				$verification_needed = $verification->fields_needed ? $verification->fields_needed : array();
				$verification_type   = '';
				$verification_total  = count( $verification_needed );

				// Check if verification has already been through our system and if so to stop emailing the user multiple times.
				if ( isset( $verification_needed ) && get_user_meta( $local_user_id, 'nano_verification_required' ) == $verification_needed ) {
					exit();
				}

				if ( $verification_total > 0 ) {
					foreach ( $verification_needed as $needed ) {
						if ( '' != $needed ) {
							update_user_meta( $local_user_id, 'nano_verification_required', $needed );
						}
						$verification_type .= '<li>';
						$verification_type .= 'legal_entity.address.city' == $needed ? 'City' : '';
						$verification_type .= 'legal_entity.address.line1' == $needed ? 'Street' : '';
						$verification_type .= 'legal_entity.address.postal_code' == $needed ? 'Postal Code' : '';
						$verification_type .= 'legal_entity.address.state' == $needed ? 'State' : '';
						$verification_type .= 'legal_entity.business_name' == $needed ? 'Business Name' : '';
						$verification_type .= 'legal_entity.business_tax_id' == $needed ? 'Business Tax ID' : '';
						$verification_type .= 'legal_entity.dob.day' == $needed ? 'Birth Day' : '';
						$verification_type .= 'legal_entity.dob.month' == $needed ? 'Birth Month' : '';
						$verification_type .= 'legal_entity.dob.year' == $needed ? 'Birth Year' : '';
						$verification_type .= 'legal_entity.first_name' == $needed ? 'First Name' : '';
						$verification_type .= 'legal_entity.last_name' == $needed ? 'Last Name' : '';
						$verification_type .= 'legal_entity.personal_id_number' == $needed ? 'Last 4 digits of your SSN number did not process, we will need your full SSN number.' : '';
						$verification_type .= 'legal_entity.verification.document' == $needed ? 'A scan of a legal ID document' : '';
						$verification_type .= '</li>';
					}

					$subject = __( 'Nanosteam - Funding account verification', 'nanosteam_8n' );

					$message = __( 'Hello', 'nanosteam_8n' ) . ' ' . $name . ' ' . $lname . ', <br /><br />
                                ' . __( 'Your Funding Account has been setup but there are still some requirements before you can begin accepting payments for your projects.', 'nanosteam_8n' ) . '<br /><br />
                                ' . __( 'We have recieved the following as still outstanding:', 'nanosteam_8n' ) . '<br /><br />
                                <ul>' . $verification_type . '</ul>
                                <br /><br />
                                ' . $date . '<br /><br>
                                ' . __( 'To update your account information please login at', 'nanosteam_8n' ) . ' <a href="' . get_site_url() . '">' . get_site_url() . '</a><br />
                                ' .  __( 'Outstanding requirements will be highlighted in your <strong>Payment Methods.</strong> You can access your account at', 'nanosteam_8n' ) . ' <a href="' . get_site_url() . '/dashboard/?payment_settings=1">' . get_site_url() . '/dashboard/?payment_settings=1</a><br />
                                ' . __( 'If you have any questions, please contact us at ', 'nanosteam_8n' ) . '<a href="mailto:' . $coemail . '">' . $coemail . '</a><br /><br />
                                ' . __( 'Thank you!', 'nanosteam_8n' ) . '<br />
                                ' . __( 'The', 'nanosteam_8n' ) . ' ' . $coname . ' ' . __( 'team', 'nanosteam_8n' ) . '<br /><br />
                                ' . $coname . '<br />';

				} else {

					if ( true != $previous_attr ) {
						exit();
					}

					$subject = __( 'Nanosteam - Funding account update notification', 'nanosteam_8n' );

					$message = '<h2>' . __( 'Funding account update notification', 'nanosteam_8n' ) . '</h2>
   
                                ' . __( 'Hello', 'nanosteam_8n' ) . ' ' . $name . ' ' . $lname . ', <br /><br />         
                                ' . __( 'We\'ve recently updated your account information from some recent activity from you on Nanosteam.org.', 'nanosteam_8n' ) . '<br /><br />            
                                ' . __( 'If you did not make these changes:', 'nanosteam_8n' ) . '<br /><br />
                                <ol>
                                    <li>' . __( 'You should change your password as soon as possible from your account page at', 'nanosteam_8n' ) . ' <a href="' . get_site_url() . '/dashboard/?edit-profile">' . get_site_url() . '/dashboard/?edit-profile</a></li>
                                    <li>' . __( 'You should confirm your payment information from any project you\'ve created under <strong>Payment Methords.</strong> You can access your account at', 'nanosteam_8n' ) . ' <a href="' . get_site_url() . '/dashboard/?payment_settings=1">' . get_site_url() . '/dashboard/?payment_settings=1</a></li>
                                    <li>' . __( 'If you have any questions, please contact us at ', 'nanosteam_8n' ) . '<a href="mailto:' . $coemail . '">' . $coemail . '</a></li>
                                </ol>
                                ' . __( 'Thank you!', 'nanosteam_8n' ) . '<br />
                                ' . __( 'The', 'nanosteam_8n' ) . ' ' . $coname . ' ' . __( 'team', 'nanosteam_8n' ) . '<br />
                           ' . $coname . '<br />';
				}

				nano_stripe_email( $subject, $message, $email, $name, $lname );

				http_response_code( 200 ); // PHP 5.4 or greater

			} elseif ( 'payout.paid' == $event_json->type ) {

				// This is the automated payout transfer to the creator once the payout has been initiated by the administrator

				if ( $stripe_user_id == $stripe_debit_account ) {

					nano_creator_payment_listener( $event_json, $stripe_user_id );

					send_error( 'nano_creator_payment_listener' );

					// Response code is handled in nano_creator_payment_listener();

				} else {

					http_response_code( 400 );

				}

			} elseif ( 'balance.available' == $event_json->type ) {

				if ( $stripe_user_id == $master_stripe_account ) {

					$event_base = $event_json->data->object;

					$stripe_balance              = array();
					$available                   = isset( $event_base->available ) ? $event_base->available : '';
					$reserve                     = isset( $event_base->connect_reserved ) ? $event_base->connect_reserved : '';
					$pending                     = isset( $event_base->pending ) ? $event_base->pending : '';
					$stripe_balance['available'] = isset( $available[0]->amount ) ? $available[0]->amount : '';
					$stripe_balance['reserve']   = isset( $reserve[0]->amount ) ? $reserve[0]->amount : '';
					$stripe_balance['pending']   = isset( $pending[0]->amount ) ? $pending[0]->amount : '';

					update_option( 'nano_stripe_balance', $stripe_balance, true );
					send_error( 'BALANCE AVAILABLE' );
					send_error( $stripe_balance );

					http_response_code( 200 ); // PHP 5.4 or greater

				} else {

					http_response_code( 400 ); // PHP 5.4 or greater

				}

			} else {

				http_response_code( 400 ); // PHP 5.4 or greater

			}
		}
	}
}

add_action( 'init', 'nano_stripe_event_listener' );

/**
 * A more rhobust emailed than what's built into IDK
 * Can be overridden if we use Mandrill
 *
 * @param $subject_inc
 * @param $message_inc
 * @param $creator_email
 * @param $creator_fname
 * @param $creator_lname
 */
function nano_stripe_email( $subject_inc, $message_inc, $creator_email, $creator_fname, $creator_lname ) {
	//error_reporting(0);

	$settings = get_option( 'md_receipt_settings' );
	if ( ! empty( $settings ) ) {
		$settings = maybe_unserialize( $settings );
		$coname   = apply_filters( 'idc_company_name', $settings['coname'] );
		$coemail  = $settings['coemail'];
	} else {
		$coname  = '';
		$coemail = get_option( 'admin_email', null );
	}

	/*
	** Check CRM Settings
	*/

	$crm_settings = get_option( 'crm_settings' );

	if ( ! empty( $crm_settings ) ) {
		$enable_sendgrid = ( isset( $crm_settings['enable_sendgrid'] ) ? $crm_settings['enable_sendgrid'] : 0 );
		if ( $enable_sendgrid ) {
			$sendgrid_api_key = $crm_settings['sendgrid_api_key'];
		}
		$enable_mandrill = ( isset( $crm_settings['enable_mandrill'] ) ? $crm_settings['enable_mandrill'] : 0 );
		if ( $enable_mandrill ) {
			$mandrill_key = $crm_settings['mandrill_key'];
		}
	}

	/*
	** Mail Function
	*/
	if ( ! empty( $coemail ) ) {
		// Notify project owner that level has been funded
		$subject  = $subject_inc;
		$headers  = 'From: ' . $coname . ' <' . $coemail . '>' . "\n";
		$headers .= 'Reply-To: ' . $coemail . "\n";
		$headers .= "MIME-Version: 1.0\n";
		$headers .= "Content-Type: text/html; charset=UTF-8\n";
		$message  = $enable_mandrill && 0 == $enable_mandrill ? nano_email_header( $subject, '' ) : '';
		$message .= $message_inc;
		$message .= 0 == $enable_mandrill ? nano_email_footer() : '';
		if ( isset( $enable_sendgrid ) && $enable_sendgrid ) {
			require_once IDC_PATH . 'lib/sendgrid-php/sendgrid-php.php';
			$sendgrid = new SendGrid( $sendgrid_api_key );
			$mail     = new SendGrid\Email();
			$mail->
			addTo( $creator_email )->
			setReplyTo( $coemail )->
			setFrom( $coemail )->
			setFromName( $coname )->
			setSubject( $subject )->
			setText( null )->
			setHtml( $message );
			try {
				$go = $sendgrid->send( $mail );
			} catch ( Exception $e ) {
				$error = $e->getErrors();
				print_r( json_encode( $error ) );
				exit;
			}
		} elseif ( isset( $enable_mandrill ) && $enable_mandrill ) {
			try {
				require_once IDC_PATH . 'lib/mandrill-php-master/src/Mandrill.php';
				$mandrill = new Mandrill( $mandrill_key );
				$msgarray = array(
					'html'       => $message,
					'text'       => '',
					'subject'    => $subject,
					'from_email' => $coemail,
					'from_name'  => $coname,
					'to'         => array(
						array(
							'email' => $creator_email,
							'name'  => $creator_fname . ' ' . $creator_lname,
							'type'  => 'to',
						),
					),
					'headers'    => array(
						'MIME-Version' => '1.0',
						'Content-Type' => 'text/html',
						'charset'      => 'UTF-8',
						'Reply-To'     => $coemail,
					),
				);
				$async    = false;
				$ip_pool  = null;
				$send_at  = null;
				$go       = $mandrill->messages->send( $msgarray, $async, $ip_pool, $send_at );
			} catch ( Mandrill_Error $e ) {
				// Mandrill errors are thrown as exceptions
				echo 'A mandrill error occurred: ' . get_class( $e ) . ' - ' . $e->getMessage();
				// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
				send_error( $e );
			}
		} else {
			wp_mail( $creator_email, $subject, $message, $headers );
		}
	}
}
