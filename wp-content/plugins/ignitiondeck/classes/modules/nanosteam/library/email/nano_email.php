<?php if ( ! defined( 'ABSPATH' ) ) {
	die( 'Direct access forbidden.' );
}

// Adding some additional filters to the base WP_MAIL system
add_filter( 'wp_mail_from_name', function( $name ) {
	return 'nanosteam Foundation';
});

add_filter( 'wp_mail_from', function( $email ) {
	return 'donotreply@nanosteam.org';
});

add_filter( 'wp_mail', function( $args ) {

	if ( ( !is_array(  $args['headers'] ) && false !== strpos( $args['headers'], 'text/html' ) ) || ( is_array(  $args['headers'] ) && false !== strpos( $args['headers'][1], 'text/html' ) )  ) {

		$html_header = nano_email_header( $args['subject'], '' );
		$html_footer = nano_email_footer();

		$crm_settings    = get_option( 'crm_settings' );
		$enable_mandrill = '';
		if ( ! empty( $crm_settings ) ) {
			$enable_mandrill = $crm_settings['enable_mandrill'];
		}

		$text = $args['message'];

		$args['message']  = 0 == $enable_mandrill ? $html_header : '';
		$args['message'] .= preg_replace( '/[^[:print:]]/', ' ', $text );
		$args['message'] .= 0 == $enable_mandrill ? $html_footer : '';

	}
	//else {

		//$args['message'] = preg_replace( '/[^[:print:]]/', ' ', $args['message'] );

	//}


	return $args;
});

// Disabling some IDK auto emails

remove_action('idfu_update_create', 'idc_ide_update_notification', 10, 2);


// define the wp_mail_failed callback
/**
 * If WP Mail fails for whatever reason log it and mail the admin
 * @param $wp_error
 *
 * @return bool
 */
function nano_action_wp_mail_failed( $wp_error ) {

	$error_d = '';

	if ( isset( $wp_error->errors['wp_mail_failed'] ) && is_array( $wp_error->errors['wp_mail_failed'] ) ) {

		$errors = $wp_error->errors['wp_mail_failed'];

		foreach ( $errors as $key => $error ) {
			$error_d .= 'Error #' . $key . ': ' . $error . "\n";
			$error_d .= 'To: ' . $wp_error->error_data['wp_mail_failed']['to'][0] . "\n";
			$error_d .= 'Subject: ' . $wp_error->error_data['wp_mail_failed']['subject'] . "\n";
		}

	}

	$to       = get_option( 'admin_email' );
	$subject  = 'WordPress Mail Failed To Send';
	$message  = "This was also logged wp_content/debug.log\n";
	$message .= "There is a high chance that the message has been corrupted. Please goto WP Admin > IDC > Email and 
	test send the email listed here. If you get another failure email like this then clear the editor field and 
	re-write your email. Once done save then resend a test. If you get the test email everything is fine, if you get 
	another error email, try again. \n";

	$message .= "Errors:\n";
	$message .= $error_d;
	//wp_mail( $to, $subject, $message );
	return error_log( print_r( $error_d, true ) );

}

// add the action
add_action( 'wp_mail_failed', 'nano_action_wp_mail_failed', 10, 1 );

/**
 * The list of Nanosteam's email IDs and their respective titles to select from
 * key = Email ID
 * val = Title
 *
 */
function nano_base_email_templates() {

	$array = array(
		'nanosteam_project_approved'        => __( 'Project Approved Notification (Creator)', 'nanosteam_8n' ),
		'nanosteam_fifty_approved'          => __( 'Nanosteam Matching Grant Approved', 'nanosteam_8n' ),
		'nanosteam_second_milsetone_check'  => __( 'Nanosteam Matching Grant 2nd milestone reminder', 'nanosteam_8n' ),
		'nanosteam_fifty_declined'          => __( 'Nanosteam Matching Grant Declined', 'nanosteam_8n' ),
		'nanosteam_backer_refund'           => __( 'Backer Credit', 'nanosteam_8n' ),
		'nanosteam_backer_refund_reminder'  => __( 'Backer Credit Reminder', 'nanosteam_8n' ),
		'backer_notification_updates'       => __( 'Backer notification of Project updates', 'nanosteam_8n' ),
		'nanosteam_transfer'                => __( 'Payout Funds to Creator initiated notification', 'nanosteam_8n' ),
		//'nanosteam_transfer_confirmed'		=> __( 'Payout Funds have arrived and are being sent to Creator notification', 'nanosteam_8n' ),
		'creator_notification_pledge'       => __( 'New Pledge Activity', 'nanosteam_8n' ),
		'nanosteam_teammember'              => __( 'Team Member Addition to Project', 'nanosteam_8n' ),
		'nano_endorsement_member'           => __( 'Endorsement Invitation', 'nanosteam_8n' ),
		'nano_endorsement_member_reminder'  => __( 'Endorsement Invitation Reminder', 'nanosteam_8n' ),
		'nano_endorsement_notification'     => __( 'Endorsement Received Notification', 'nanosteam_8n' ),
		'nano_submission_reminder'          => __( 'Project Submission Reminder', 'nanosteam_8n' ),
		'nano_backer_ceo_note'              => __( 'Backer Day 0 CEO Note', 'nanosteam_8n' ),
		'nano_backer_day_3_creator_letter'  => __( 'Backer Day 3 Creator Letter', 'nanosteam_8n' ),
		'nano_backer_day_10_give_creator'   => __( 'Backer Day 10 Creator Give', 'nanosteam_8n' ),
		'nano_backer_day_12_give_creator'   => __( 'Backer Day 12 Creator Give', 'nanosteam_8n' ),
		'nano_backer_day_21_creator_letter' => __( 'Backer Day 21 Creator Give', 'nanosteam_8n' ),
		'nano_backer_day_57_give_creator'   => __( 'Backer Day 57 Creator Give', 'nanosteam_8n' ),
		'nano_backer_day_58_give_creator'   => __( 'Backer Day 58 Creator Give', 'nanosteam_8n' ),
	);

	return $array;
}

/**
 * Add email templates that auto send based on approved or declined in the admin.
 * Available Merge swaps are: {{NAME}},{{PROJECT_NAME}},{{PROJECT_DESCRIPTION}},{{PROJECT_GOAL}},
 * {{EDIT_LINK}},{{COMPANY_NAME}},{{COMPANY_EMAIL}}
 *
 */

function nano_email_templates() {

	$template_arr = nano_base_email_templates();

	foreach ( $template_arr as $key => $val ) {
		echo '<option name="' . $key . '">' . $val . '</option>';

	}

	/**
	 * Adding all the custom Nanosteam email fields to the IDK emails
	 *
	 */

	function nano_add_email_fields() {

		$current_user = wp_get_current_user();

		$template_arr = nano_base_email_templates();

		$template_array = apply_filters( 'idc_email_template_array', $template_arr );

		foreach ( $template_array as $k => $v ) {
			$content = stripslashes( get_option( $k ) );
			if ( empty( $content ) ) {
				$default = stripslashes( get_option( $k . '_default' ) );
				if ( ! empty( $default ) ) {
					$content = $default;
				}
			}
			$template_array[ $k ] = $content;

		}

		foreach ( $template_arr as $key => $val ) {

			if ( isset( $_POST['edit_template'] ) ) {
				foreach ( $_POST as $k => $v ) {
					$key     = str_replace( '_text', '', $k );
					$subject = isset( $_POST[ $key . '_subject' ] ) ? $_POST[ $key . '_subject' ] : '';
					if ( ( array_key_exists( $key, $template_array ) && $v !== $template_array[ $key ] ) || get_option( $key . '_subject' ) != $subject ) {
						$template_array[ $key ] = wp_kses_post( balanceTags( $v ) );
						update_option( $key, $template_array[ $key ] );
						update_option( $key . '_subject', $subject );
					}
				}
			} elseif ( isset( $_POST[ 'restore_default_' . $key . '_email' ] ) ) {
				$nano_fifty_approved_email = get_option( $key . '_default' );
				update_option( $key, $nano_fifty_approved_email );
			} else {
				do_action( 'idc_email_template_test', $_POST );

				if ( isset( $_POST[ 'send_test_' . $key ] ) ) {
					//send test to default email

					$spaced_key = str_replace( '_', ' ', $key );
					$capped_key = ucwords( $spaced_key );
					$to         = get_option( 'admin_email' );
					$subject    = apply_filters( 'idc_email_test_subject', $capped_key . ' ' . __( 'Test', 'memberdeck' ) );
					//$message    = nano_email_header( $val, '' );
					$message   = wpautop( stripslashes( wp_kses_post( $template_array[ $key ] ) ) );
					//$message   .= nano_email_footer();
					$mail       = new ID_Member_Email( $to, $subject, $message, $current_user->ID );
					$mail->send_mail();
				}
			}
		}

		$editor_settings = array(
			'textarea_rows' => 80,
			'editor_height' => 400,
		);

		foreach ( $template_arr as $key => $val ) {
			$subject = get_option( $key . '_subject' );
			?>
			<div class="form-row <?php echo $key; ?> email_text" style="display: none">

				<div class="small-text test-field" style="display: block; margin-bottom: 10px;">
					<label for="<?php echo $key; ?>_subject">Subject</label>
					<input style="font-size: 13px; line-height: 13px; color: #333" class="small-text"
							placeholder="Subject" type="text" name="<?php echo $key; ?>_subject"
							id="<?php echo $key; ?>_subject" value="<?php echo $subject; ?>">
				</div>

				<?php wp_editor( ( isset( $template_array[ $key ] ) ? $template_array[ $key ] : '' ), $key . '_text', $editor_settings ); ?>
			</div>
			<?php
		}

	}

	add_action( 'idc_email_template', 'nano_add_email_fields' );

}

add_filter( 'idc_email_template_option', 'nano_email_templates' );


/**
 * Sending an email to the creator or backer depending on the trigger elements.
 *
 * @param string $post_ID
 * @param string $recipient
 * @param string $type
 *
 * @throws
 *
 */

function nano_email_notify( $post_ID, $recipient, $type ) {

	global $permalink_structure;

	$post = get_post( $post_ID );

	if ( empty( $post ) ) {
		return;
	}

	$post_id                  = $post_ID;
	$standard_email_templates = nano_base_email_templates();

	foreach ( $standard_email_templates as $key => $val ) {
		if ( $type == $key ) {
			$text    = stripslashes( get_option( $key ) );
			$subject = get_option( $key . '_subject' );
		}
	}

	// If there are special conditions for the email type put them below otherwise use the standard email foreach above.
	// Matching Grant Approval / Declined

	if ( 'fifty_fifty_grant' == $type ) {

		$grant_status = get_post_meta( $post_id, 'fifty_fifty_grant', true );

		$subject = __( 'Grant Application Review', 'nanosteam_8n' );

		if ( 'approved' == $grant_status ) {
			// create approval message
			$text = stripslashes( get_option( 'nanosteam_fifty_approved' ) );
		} elseif ( 'declined' == $grant_status ) {
			// create declined message
			$text = stripslashes( get_option( 'nanosteam_fifty_declined' ) );
		} else {
			$text = 'No Message';
		}
	}

	if ( 'success_notification' == $type || 'success_notification_default' == $type ) {
		$subject = __( 'Successful Project Notification', 'nanosteam_8n' );
		$text = stripslashes(get_option('success_notification'));
		if (empty($text)) {
			$text = stripslashes(get_option('success_notification_default'));
		}
	}

	// Project Needs additional requirements
	if ( 'nanosteam_project_needs_more_requirements' == $type ) {
		$last_subject = array_values( array_slice( get_post_meta( $post_id, 'project_review_subject' ), - 1 ) )[0];
		$last_text    = array_values( array_slice( get_post_meta( $post_id, 'project_review_message' ), - 1 ) )[0];

		$subject = $last_subject;
		$text    = $last_text;
	}
	$project_id = get_post_meta( $post_id, 'ign_project_id', true );
	$user       = get_user_by( 'email', $recipient );
	if ( ! empty( $user ) ) {
		if ( isset( $user->user_firstname ) ) {
			$fname = $user->user_firstname;
			$lname = $user->user_lastname;
			$name  = $fname . ' ' . $lname;
		} else {
			$name = $user->display_name;
		}
	} else {
		$name = $recipient;
	}

	$to = isset( $user->user_email ) ? $user->user_email : '';
	$settings = get_option( 'md_receipt_settings' );

	if ( '' == $to ) {
		send_error( 'nano_email_notify() Failed to send. Email does not exist for the following. Post ID: ' . $post_id . '. Email Type: ' . $type );
		die();
	}

	if ( ! empty( $settings ) ) {
		if ( ! is_array( $settings ) ) {
			$settings = unserialize( $settings );
		}
		$coname  = apply_filters( 'idc_company_name', $settings['coname'] );
		$coemail = $settings['coemail'];
	} else {
		$coname  = '';
		$coemail = '';
	}
	if ( empty( $permalink_structure ) ) {
		$prefix = '&';
	} else {
		$prefix = '?';
	}
	if ( isset( $post_id ) ) {

		$project      = isset( $project_id ) ? new ID_Project( $project_id ) : null;
		$the_project  = null != $project ? $project->the_project() : '';
		$description  = '' !== get_post_meta( $post_id, 'nano_project_long_description', true ) ? get_post_meta( $post_id, 'nano_project_long_description', true ) : get_post_meta( $post_id, 'ign_project_description', true );
		$preview_link = get_site_url() . '?post_type=ignition_product&p=' . $post_id . '&preview=1';
		$edit_link    = isset( $project_id ) ? get_site_url() . '/dashboard/?edit_project=' . $post_id : $preview_link;
		$public_link  = get_permalink( $post_id );
		$end          = get_post_meta( $post_id, 'ign_fund_end', true );
		$update       = get_post( $post_id ); // This is to snag the Labnotes info

		// Update the user's email notifications receipts
		add_user_meta( $user->ID, 'nano_email_reciepts', $type . '_' . $post_id );

		/*
		** Mail Function
		*/

		// Sending email to customer on the completion of order
		$subject     = isset( $subject ) ? $subject : __( 'Email from Nanosteam', 'nanosteam_8n' );
		$headers     = 'From: ' . $coname . ' <donotreply@nanosteam.org>' . "\n";
		$headers    .= 'Reply-To: donotreply@nanosteam.org ' . "\n";
		$headers    .= "MIME-Version: 1.0\n";
		$headers    .= "Content-Type: text/html; charset=UTF-8\n";
		$html_header = nano_email_header( $subject, '' );
		$html_footer = nano_email_footer();

		$merge_swap = array(
			array(
				'tag'  => '{{NAME}}',
				'swap' => $name,
			),
			array(
				'tag'  => '{{PROJECT_NAME}}',
				'swap' => get_the_title( $post_id ),
			),
			array(
				'tag'  => '{{PROJECT_DESCRIPTION}}',
				'swap' => $description,
			),
			array(
				'tag'  => '{{PROJECT_GOAL}}',
				'swap' => isset( $project_id ) && isset( $the_project->goal ) ? apply_filters( 'id_project_goal', $the_project->goal, $post_id ) : '',
			),
			array(
				'tag'  => '{{EDIT_LINK}}',
				'swap' => '<a href="' . $edit_link . '">' . $edit_link . '</a>',
			),
			array(
				'tag'  => '{{PREVIEW_LINK}}',
				'swap' => '<a href="' . $preview_link . '">' . $preview_link . '</a>',
			),
			array(
				'tag'  => '{{PUBLIC_LINK}}',
				'swap' => '<a href="' . $public_link . '">' . $public_link . '</a>',
			),
			array(
				'tag'  => '{{COMPANY_NAME}}',
				'swap' => $coname,
			),
			array(
				'tag'  => '{{COMPANY_EMAIL}}',
				'swap' => '<a href="mailto:' . $coemail . '">' . $coemail . '</a>',
			),
			array(
				'tag'  => '{{END_DATE}}',
				'swap' => $end,
			),
			array(
				'tag'  => '{{UPDATE_TITLE}}',
				'swap' => $update->post_title,
			),
			array(
				'tag'  => '{{UPDATE_CONTENT}}',
				'swap' => $update->post_content,
			),
		);
		foreach ( $merge_swap as $swap ) {
			$text = str_replace( $swap['tag'], $swap['swap'], $text );
		}

		$text = preg_replace( '/[^[:print:]]/', ' ', $text );

		/*
		** Check CRM Settings
		*/

		$crm_settings    = get_option( 'crm_settings' );
		$enable_mandrill = '';
		if ( ! empty( $crm_settings ) ) {
			$enable_mandrill = $crm_settings['enable_mandrill'];
		}

		//$message  = 0 == $enable_mandrill ? $html_header : '';
		$message = wpautop( $text );
		//$message .= 0 == $enable_mandrill ? $html_footer : '';

		//md_send_mail( $to, $headers, $subject, $message );

		$mail = new ID_Member_Email( $to, $subject, $message, '' );
		$mail->send_mail();

	}
}


/**
 * We need to override some IDK email functions as they don't play well with our credit toekn system
 *
 */

// Project success notification to backers replacement.
function nano_idc_success_notification( $post_id, $project_id ) {

	$project_id  = get_post_meta( $post_id, 'ign_project_id', true );
	$level_id    = nano_get_level_id( $project_id );
	$idcf_orders = ID_Member_Order::get_orders_by_level( $level_id );
	$text        = stripslashes( get_option( 'success_notification' ) );
	$type        = 'success_notification';
	if ( empty( $text ) ) {
		$text = stripslashes( get_option( 'success_notification_default' ) );
		$type = 'success_notification_default';
	}

	if ( ! empty( $idcf_orders ) ) {
		$user_id_array = array();
		foreach ( $idcf_orders as $idcf_order ) {
			$user_id = $idcf_order->user_id;

			if ( ! in_array( $user_id, $user_id_array ) ) {

				$user = get_user_by( 'id', $user_id );
				if ( ! empty( $user ) ) {
					$email = $user->user_email;
				}

				nano_email_notify( $post_id, $email, $type );

				$user_id_array[] = $user_id;

			}
		}
	}
}

add_action( 'idmember_receipt', 'remove_idk_project_success_action', 9 );
function remove_idk_project_success_action(){
	remove_action('idcf_project_success', 'idc_success_notification', 10, 2);
}
add_action('idcf_project_success', 'nano_idc_success_notification', 10, 2);


/**
 * Replaces idmember_purchase_receipt() as our token system makes the default purchase receipt send the
 * inappropriate product description and ID to the backer. We need to find the project they supported and
 * send them that data instead.
 *
 * @param $user_id
 * @param $price
 * @param $level_id
 * @param $source
 * @param $new_order
 *
 * @throws Mandrill_Error
 */
// We do this as the default payment reciept does not include the proper project name for payment.

function nano_idmember_purchase_receipt($user_id, $price, $level_id, $source, $new_order) {

	send_error('purchase receipt');

	$level_id = isset( $_SESSION['nano_project_id'] ) ? nano_get_level_id( $_SESSION['nano_project_id'] ) : $level_id;

	//error_reporting(0);
	$settings = get_option( 'md_receipt_settings' );
	if ( ! empty( $settings ) ) {
		$settings = maybe_unserialize( $settings );
		$coname   = apply_filters( 'idc_company_name', $settings['coname'] );
		$coemail  = $settings['coemail'];
	} else {
		$coname  = apply_filters( 'idc_company_name', '' );
		$coemail = get_option( 'admin_email', null );
	}
	$price = apply_filters( 'idc_order_price', $price, $new_order );
	$user  = get_userdata( $user_id );
	if ( ! empty( $user ) ) {
		$email = $user->user_email;
		$fname = stripslashes( $user->first_name );
		$lname = stripslashes( $user->last_name );
	} else {
		$user = (object) ID_Member_Order::get_order_meta( $new_order, 'guest_data', true );
		// #dev (should we re-map the email key so it matches WP default?)
		$email = $user->email;
		$fname = stripslashes( $user->first_name );
		$lname = stripslashes( $user->last_name );
	}

	/*
	** Check CRM Settings
	*/

	$crm_settings = get_option( 'crm_settings' );
	if ( ! empty( $crm_settings ) ) {
		$enable_sendgrid = ( isset( $crm_settings['enable_sendgrid'] ) ? $crm_settings['enable_sendgrid'] : 0 );
		if ( $enable_sendgrid ) {
			$sendgrid_api_key = $crm_settings['sendgrid_api_key'];
		}
		$enable_mandrill = ( isset( $crm_settings['enable_mandrill'] ) ? $crm_settings['enable_mandrill'] : 0 );
		if ( $enable_mandrill ) {
			$mandrill_key = $crm_settings['mandrill_key'];
		}
	}

	$level      = ID_Member_Level::get_level( $level_id );
	$level_name = stripslashes( $level->level_name );

	$order     = new ID_Member_Order( $new_order );
	$the_order = $order->get_order();

	if ( ! empty( $the_order ) ) {
		$txn_id = $the_order->transaction_id;
	} else {
		$txn_id = '';
	}

	// Sending email to customer on the completion of order

	/*
	** Mail Function
	*/
	if ( ! empty( $coemail ) ) {
		// Sending email to customer on the completion of order
		$subject     = __( 'Payment Receipt', 'memberdeck' );
		$headers     = 'From: ' . $coname . ' <donotreply@nanosteam.org>' . "\n";
		$headers    .= 'Reply-To: donotreply@nanosteam.org ' . "\n";
		$headers    .= "MIME-Version: 1.0\n";
		$headers    .= "Content-Type: text/html; charset=UTF-8\n";
		$html_header = nano_email_header( $subject, '' );
		$html_footer = nano_email_footer();
		$text        = stripslashes( get_option( 'purchase_receipt' ) );
		if ( empty( $text ) ) {
			$text = stripslashes( get_option( 'purchase_receipt_default' ) );
		}
		if ( empty( $text ) ) {
			$message .= '<div style="padding:10px;background-color:#f2f2f2;">
						<div style="padding:10px;border:1px solid #eee;background-color:#fff;">
						<h2>' . $coname . ' ' . __( 'Payment Receipt', 'memberdeck' ) . '</h2>

							<div style="margin:10px;">

	 							' . __( 'Hello', 'memberdeck' ) . ' ' . $fname . ' ' . $lname . ', <br /><br />
	  
	  							' . __( 'You have successfully made a payment of ', 'memberdeck' ) . $price . '<br /><br />
	    
	    						' . __( 'This transaction should appear on your Credit Card statement as', 'memberdeck' ) . ' ' . $coname . '<br /><br />
	    						<div style="border: 1px solid #333333; width: 600px;">
	    							<table width="600" border="0" cellspacing="0" cellpadding="5">
	          							<tr bgcolor="#333333" style="color: white">
					                        <td width="150">' . __( 'DATE', 'memberdeck' ) . '</td>
					                        <td width="150">' . __( 'PRODUCT', 'memberdeck' ) . '</td>
					                        <td width="150">' . __( 'AMOUNT', 'memberdeck' ) . '</td>
					                        <td width="150">' . __( 'ORDER ID', 'memberdeck' ) . '</td>
					                    </tr>
				                        <tr>
				                        	<td width="150">' . nano_get_formated_date_time_gmt( 'D, M j' ) . '</td>
				                           	<td width="150">' . $level_name . '</td>
				                           	<td width="150">' . $price . '</td>
				                      		<td width="150">' . $txn_id . '</td>
				                      	</tr>
	    							</table>
	    						</div>
	    						<br /><br />
	    						' . __( 'Thank you for your support!', 'memberdeck' ) . '<br />
	    						' . __( 'The', 'memberdeck' ) . ' ' . $coname . ' ' . __( 'team', 'memberdeck' ) . '
							</div>

							<table rules="all" style="border-color:#666;width:80%;margin:20px auto;" cellpadding="10">

	    					<!--table rows-->

							</table>

			               ---------------------------------<br />
			               ' . $coname . '<br />
			               <a href="mailto:' . $coemail . '">' . $coemail . '</a>
			           

			            </div>
			        </div>';
		} else {
			$merge_swap = array(
				array(
					'tag'  => '{{COMPANY_NAME}}',
					'swap' => $coname,
				),
				array(
					'tag'  => '{{NAME}}',
					'swap' => $fname . ' ' . $lname,
				),
				array(
					'tag'  => '{{AMOUNT}}',
					'swap' => $price,
				),
				array(
					'tag'  => '{{DATE}}',
					'swap' => nano_get_formated_date_time_gmt( 'D, M j' ),
				),
				array(
					'tag'  => '{{PRODUCT_NAME}}',
					'swap' => $level_name,
				),
				array(
					'tag'  => '{{TXN_ID}}',
					'swap' => $txn_id,
				),
				array(
					'tag'  => '{{COMPANY_EMAIL}}',
					'swap' => $coemail,
				),
			);
			foreach ( $merge_swap as $swap ) {
				$text = str_replace( $swap['tag'], $swap['swap'], $text );
			}
			$message = wpautop( $text );
		}
		if ( isset( $enable_sendgrid ) && $enable_sendgrid ) {
			require_once IDC_PATH . 'lib/sendgrid-php/sendgrid-php.php';
			$sendgrid = new SendGrid( $sendgrid_api_key );
			$mail     = new SendGrid\Email();
			$mail->
			addTo( $email )->
			setReplyTo( $coemail )->
			setFrom( $coemail )->
			setFromName( $coname )->
			setSubject( $subject )->
			setText( null )->
			setHtml( $message );
			try {
				$go = $sendgrid->send( $mail );
			} catch ( Exception $e ) {
				$error = $e->getErrors();
				print_r( json_encode( $error ) );
				exit;
			}
		} else if ( isset( $enable_mandrill ) && $enable_mandrill ) {
			try {
				require_once IDC_PATH . 'lib/mandrill-php-master/src/Mandrill.php';
				$mandrill = new Mandrill( $mandrill_key );
				$msgarray = array(
					'html'       => $message,
					'text'       => '',
					'subject'    => $subject,
					'from_email' => $coemail,
					'from_name'  => $coname,
					'to'         => array(
						array(
							'email' => $email,
							'name'  => $fname . ' ' . $lname,
							'type'  => 'to'
						)
					),
					'headers'    => array(
						'MIME-Version' => '1.0',
						'Content-Type' => 'text/html',
						'charset'      => 'UTF-8',
						'Reply-To'     => $coemail
					)
				);
				$async    = false;
				$ip_pool  = null;
				$send_at  = null;
				$go       = $mandrill->messages->send( $msgarray, $async, $ip_pool, $send_at );
			} catch ( Mandrill_Error $e ) {
				// Mandrill errors are thrown as exceptions
				echo 'A mandrill error occurred: ' . get_class( $e ) . ' - ' . $e->getMessage();
				// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
				throw $e;
			}
		} else {
			send_error('sending now');
			//echo $email."<br>".$subject."<br>".$message;
			//$html_message  = $html_header;
			$html_message = $message;
			//$html_message .= $html_footer;


			//$sending_mail = mail( $email, $subject, $html_message, $headers );
			wp_mail( $email, $subject, $html_message, $headers );

		}
	}
}

add_action( 'idmember_receipt', 'remove_idk_email_action', 0 );
function remove_idk_email_action(){
	remove_action( 'idmember_receipt', 'idmember_purchase_receipt', 1, 5 );
}
add_action( 'idmember_receipt', 'nano_idmember_purchase_receipt', 1, 5 );

/**
 * Has user been emailed already.
 *
 * @param string $user_id
 * @param string $type
 *
 * @return string
 */

function nano_user_emailed_receipt( $user_id, $type ) {
	$receipt = get_user_meta( $user_id, 'nano_email_reciepts' );
	$receipt = in_array( $type, $receipt );

	return $receipt;
}

/**
 * Engagment triggers after a donor's first donation.
 */

function nano_donor_engagement_after_first_donation() {

	$args = array(
		'post_type'      => 'ignition_product',
		'post_status'    => 'publish',
		'posts_per_page' => - 1,
	);

	$query = new WP_Query( $args );

	if ( $query->have_posts() ) {

		while ( $query->have_posts() ) {

			//need to get orders
			//need to see if ther is a filter to add in subject line to
			$query->the_post();

			$post_id                   = get_the_id();
			$is_project_live           = get_post_meta( $post_id, 'ign_project_closed', true ) != '1' ? true : false;
			$project_raised_percentage = get_post_meta( $post_id, 'ign_percent_raised', true ) != '' && get_post_meta( $post_id, 'ign_percent_raised', true ) >= 100 ? true : false;
			$start_date                = get_post_meta( $post_id, 'ign_start_date', true );
			$level_id                  = nano_get_level_id( get_post_meta( $post_id, 'ign_project_id', true ) );

			// Get project orders
			$orders = class_exists( 'ID_Member_Order' ) ? ID_Member_Order::get_orders_by_level( $level_id ) : '';

			if ( true == $is_project_live ) {

				if ( isset( $orders ) ) {

					foreach ( $orders as $order ) {

						$user_id = $order->user_id;

						// Date different calculation
						$today          = nano_get_formated_date_time_gmt( get_option( 'date_format' ) );
						$datetime_today = new DateTime( $today );
						$ordered_date   = $order->order_date;

						// Difference in days between ordered date and today.
						$datetime_ordered = new DateTime( $ordered_date );
						$interval_ordered = $datetime_today->diff( $datetime_ordered );
						$diff_ordered     = $interval_ordered->format( '%a' );

						// Difference in days between project close date and today.
						$datetime_closed = new DateTime( $start_date );
						$interval_closed = $datetime_today->diff( $datetime_closed );
						$diff_closed     = $interval_closed->format( '%a' );

						$recipient_params         = get_user_by( 'id', $order->user_id );
						$recipient                = $recipient_params->data->user_email;
						$recipient_meta           = get_user_meta( $order->user_id );
						$recipient_email_receipts = get_user_meta( $order->user_id, 'nano_email_reciepts' );

						if ( 0 == $diff_ordered && nano_user_emailed_receipt( $user_id, 'nano_backer_ceo_note_' . $post_id ) == false ) {
							nano_email_notify( $post_id, $recipient, 'nano_backer_ceo_note' );
						}

						if ( 3 == $diff_ordered && nano_user_emailed_receipt( $user_id, 'nano_backer_day_3_creator_letter_' . $post_id ) == false ) {
							nano_email_notify( $post_id, $recipient, 'nano_backer_day_3_creator_letter' );
						}

						if ( 10 == $diff_ordered && nano_user_emailed_receipt( $user_id, 'nano_backer_day_10_give_creator_' . $post_id ) == false ) {
							nano_email_notify( $post_id, $recipient, 'nano_backer_day_10_give_creator' );
						}

						if ( 12 == $diff_ordered && nano_user_emailed_receipt( $user_id, 'nano_backer_day_12_give_creator_' . $post_id ) == false ) {
							nano_email_notify( $post_id, $recipient, 'nano_backer_day_12_give_creator' );
						}

						if ( 21 == $diff_ordered && nano_user_emailed_receipt( $user_id, 'nano_backer_day_21_creator_letter_' . $post_id ) == false ) {
							nano_email_notify( $post_id, $recipient, 'nano_backer_day_21_creator_letter' );
						}

						if ( 57 == $diff_ordered && nano_user_emailed_receipt( $user_id, 'nano_backer_day_57_give_creator_' . $post_id ) == false ) {
							nano_email_notify( $post_id, $recipient, 'nano_backer_day_57_give_creator' );
						}

						if ( 58 == $diff_ordered && nano_user_emailed_receipt( $user_id, 'nano_backer_day_58_give_creator_' . $post_id ) == false ) {
							nano_email_notify( $post_id, $recipient, 'nano_backer_day_58_give_creator' );
						}

						if ( false == $is_project_live && true == $project_raised_percentage && nano_user_emailed_receipt( $user_id, 'nano_backer_project_closed_' . $post_id ) == false ) {
							nano_email_notify( $post_id, $recipient, 'nano_backer_project_closed' );
						}
					}
				}
			}
		}

		/* Restore original Post Data */
		wp_reset_postdata();
	}

}

/**
 * Reminder emails for project creator to finish and submit their project for review.
 */

function project_submission_reminder() {


	$args = array(
		'post_type'      => 'ignition_product',
		'post_status'    => 'draft',
		'posts_per_page' => - 1,

	);

	$query = new WP_Query( $args );

	if ( $query->have_posts() ) {

		while ( $query->have_posts() ) {
			$query->the_post();

			$post_id = get_the_id();

			$original_author = get_post_meta( $post_id, 'original_author', true );
			$today           = nano_get_formated_date_time_gmt( get_option( 'date_format' ) );
			$sumbit_date     = get_the_date( '', $post_id );
			$datetime1       = new DateTime( $today );
			$datetime2       = new DateTime( $sumbit_date );
			$interval        = $datetime1->diff( $datetime2 );
			$diff            = $interval->format( '%a' );
			$recipient       = false !== get_user_by( 'login', $original_author ) ? get_user_by( 'login', $original_author ) : get_user_by( 'email', $original_author );
			$recipient       = $recipient->user_email;

			// We'll send out the first reminder at 2 days then there after every 2 days until day 8
			if ( $diff >= 2 && $diff <= 8 ) {
				if ( 0 == $diff % 2 ) {
					nano_email_notify( $post_id, $recipient, 'nano_submission_reminder' );
				}
			}

			// We'll send out the subsequent reminders at 7 day intervals until day 60
			if ( $diff >= 9 && $diff <= 60 ) {
				if ( 0 == $diff % 7 ) {
					nano_email_notify( $post_id, $recipient, 'nano_submission_reminder' );
				}
			}
		}

		/* Restore original Post Data */
		wp_reset_postdata();
	}

}


/**
 * Create a new filtering function that will add our where clause to the query
 *
 * @param string $where
 * @return string
 *
 */

function filter_where( $where = '' ) {

	$todays_date = nano_get_formated_date_time_gmt( 'Y-m-d' );
	$date        = date( 'Y-m-d', strtotime( $todays_date . '-90 days' ) );
	$where      .= " AND post_date >= '$date'";

	return $where;

}

/**
 * Reminder email for Matching Grant project creators to work towards their 2nd milestone for final payout.
 * This function is run once per week from the weekly cron system
 */

function nano_matching_grant_2nd_milestone_email_reminder() {

	$args = array(
		'post_type'      => 'ignition_product',
		'post_status'    => 'publish',
		'posts_per_page' => - 1,
	);

	add_filter( 'posts_where', 'filter_where' );
	$query = new WP_Query( $args );
	remove_filter( 'posts_where', 'filter_where' );

	if ( $query->have_posts() ) {

		while ( $query->have_posts() ) {

			$query->the_post();
			$post_id = get_the_id();

			$is_grant_approved = get_post_meta( $post_id, 'fifty_fifty_grant', true ) == 'approved' ? true : false;
			$is_project_live   = get_post_meta( $post_id, 'ign_project_closed', true ) == '1' ? true : false;

			if ( true == $is_grant_approved && true == $is_project_live ) {

				$first_transfer_processed_status = get_post_meta( $post_id, 'first_transfer_transaction_id', true ) != '' ? true : false;
				$final_transfer_processed_status = get_post_meta( $post_id, 'final_transfer_transaction_id', true ) != '' ? true : false;
				$second_lab_posted               = nano_second_milsetone_achieved( $post_id );

				if ( true != $second_lab_posted && true != $final_transfer_processed_status && true == $first_transfer_processed_status ) {

					$recipient = get_post_meta( $post_id, 'original_author', true );
					$type      = 'nanosteam_second_milsetone_check';
					nano_email_notify( $post_id, $recipient, $type );

				}
			}
		}

		/* Restore original Post Data */
		wp_reset_postdata();

	}

}




if ( ! function_exists( 'wp_new_user_notification' ) ) {

	/**
	 * Redefine user notification function
	 *
	 * @param $user_id
	 * @param null $deprecated
	 * @param string $notify
	 */
	function wp_new_user_notification( $user_id, $deprecated = null, $notify = '' ) {

		if ( null !== $deprecated ) {
			_deprecated_argument( __FUNCTION__, '4.3.1' );
		}

		global $wpdb, $wp_hasher;
		$user = get_userdata( $user_id );

		// The blogname option is escaped with esc_html on the way into the database in sanitize_option
		// we want to reverse this for the plain text arena of emails.
		$blogname = wp_specialchars_decode( get_option( 'blogname' ), ENT_QUOTES );

		$settings = get_option( 'md_receipt_settings' );
		if ( ! empty( $settings ) ) {
			if ( ! is_array( $settings ) ) {
				$settings = unserialize( $settings );
			}
			$coname  = apply_filters( 'idc_company_name', $settings['coname'] );
			$coemail = $settings['coemail'];
		} else {
			$coname  = '';
			$coemail = '';
		}

		$headers[] = 'From: ' . $coname . ' <donotreply@nanosteam.org>';
		$headers[] = 'Content-Type: text/html; charset=UTF-8';

		if ( 'user' !== $notify ) {
			$switched_locale = switch_to_locale( get_locale() );
			//$message         = nano_email_header( '','New User Registration' );
			$message = '<p>New User Registration</p>';
			/* translators: %s is replaced with "string" */
			$message .= '<p>' . sprintf( __( 'New user registration on your site %s:' ), $blogname ) . '</p>';
			/* translators: %s is replaced with "string" */
			$message .= '<p>' . sprintf( __( 'Username: %s' ), $user->user_login ) . '</p>';
			/* translators: %s is replaced with "string" */
			$message .= '<p>' . sprintf( __( 'Email: %s' ), $user->user_email ) . '</p>';
			//$message .= nano_email_footer();

			/* translators: %s is replaced with "string" */
			@wp_mail( get_option( 'admin_email' ), sprintf( __( '[%s] New User Registration' ), $blogname ), $message, $headers );

			if ( $switched_locale ) {
				restore_previous_locale();
			}
		}

		// `$deprecated was pre-4.3 `$plaintext_pass`. An empty `$plaintext_pass` didn't sent a user notification.
		if ( 'admin' === $notify || ( empty( $deprecated ) && empty( $notify ) ) ) {
			return;
		}

		// Generate something random for a password reset key.
		$key = wp_generate_password( 20, false );

		/** This action is documented in wp-login.php */
		do_action( 'retrieve_password_key', $user->user_login, $key );

		// Now insert the key, hashed, into the DB.
		if ( empty( $wp_hasher ) ) {
			require_once ABSPATH . WPINC . '/class-phpass.php';
			$wp_hasher = new PasswordHash( 8, true );
		}
		$hashed = time() . ':' . $wp_hasher->HashPassword( $key );
		$wpdb->update( $wpdb->users, array( 'user_activation_key' => $hashed ), array( 'user_login' => $user->user_login ) );

		$switched_locale = switch_to_locale( get_user_locale( $user ) );

		//$message = nano_email_header( '', 'To set your password' );
		$message = '<p>To set your password</p>';
		/* translators: %s is replaced with "string" */
		$message .= '<p>' . sprintf( __( 'Username: %s' ), $user->user_login ) . '</p>';
		$message .= '<p>' . __( 'To set your password, visit the following address:' ) . '</p>';
		$message .= '<p><a href="' . network_site_url( "member-password-reset/?key=$key&login=" . rawurlencode( $user->user_login ), 'login' ) . '">' . network_site_url( "member-password-reset/?key=$key&login=" . rawurlencode( $user->user_login ), 'login' ) . '</a></p>';
		$message .= '<p>' . wp_login_url() . '</p>';
		//$message .= nano_email_footer();

		/* translators: %s is replaced with "string" */
		wp_mail( $user->user_email, sprintf( __( 'Your username and password info on %s' ), $blogname ), $message, $headers );

		if ( $switched_locale ) {
			restore_previous_locale();
		}

	}
}





/**
 * Change the content message for when a user has forgotten their password
 *
 * @param $pass_change_mail
 * @param $user
 * @param $userdata
 *
 * @return mixed
 */
function nano_change_password_mail_message( $pass_change_mail, $user, $userdata ) {

	$settings = get_option( 'md_receipt_settings' );

	if ( ! empty( $settings ) ) {
		if ( ! is_array( $settings ) ) {
			$settings = unserialize( $settings );
		}
		$coname  = apply_filters( 'idc_company_name', $settings['coname'] );
		$coemail = $settings['coemail'];
	} else {
		$coname  = '';
		$coemail = '';
	}

	$new_message_txt = __( 'Hi ###DISPLAYNAME###,

This notice confirms that your password was changed on ###SITENAME###.

If you did not change your password, please contact us at admin@nanosteam.org

This email has been sent to ###EMAIL###

<p>Regards,<br>
All at ###SITENAME###<br>
###SITEURL###</p>' );

	$headers  = 'From: ' . $coname . ' <admin@nanosteam.org>' . "\n";
	$headers .= 'Reply-To: admin@nanosteam.org ' . "\n";
	$headers .= "MIME-Version: 1.0\n";
	$headers .= "Content-Type: text/html; charset=UTF-8\n";

	/* translators: %s is replaced with "string" */
	$pass_change_mail['subject'] = __( 'Notice of Password Change on %s' );
	$pass_change_mail['headers'] = $headers;

	//$pass_change_mail['message']  = nano_email_header( $pass_change_mail['subject'], 'This notice confirms that your password was changed' );
	$pass_change_mail['message']  = '<p>This notice confirms that your password was changed</p>';
	$pass_change_mail['message'] .= wpautop( $new_message_txt );
	//$pass_change_mail['message'] .= nano_email_footer();

	$pass_change_mail['message'] = str_replace( '###DISPLAYNAME###', $user['display_name'], $pass_change_mail['message'] );

	return $pass_change_mail;
}

add_filter( 'password_change_email', 'nano_change_password_mail_message', 10, 3 );


/**
 * Change the content message for when a user has changed their email
 *
 * @param $pass_change_mail
 * @param $user
 * @param $userdata
 *
 * @return mixed
 */
function nano_change_email_mail_message( $pass_change_mail, $user, $userdata ) {

	$settings = get_option( 'md_receipt_settings' );

	if ( ! empty( $settings ) ) {
		if ( ! is_array( $settings ) ) {
			$settings = unserialize( $settings );
		}
		$coname  = apply_filters( 'idc_company_name', $settings['coname'] );
		$coemail = $settings['coemail'];
	} else {
		$coname  = '';
		$coemail = '';
	}

	$new_message_txt = __( 'Hi ###DISPLAYNAME###,

This notice confirms that your email was changed on ###SITENAME###.

If you did not change your password, please contact us at admin@nanosteam.org

This email has been sent to ###EMAIL###

Regards,
All at ###SITENAME###
###SITEURL###' );

	$headers  = 'From: ' . $coname . ' <admin@nanosteam.org>' . "\n";
	$headers .= 'Reply-To: admin@nanosteam.org ' . "\n";
	$headers .= "MIME-Version: 1.0\n";
	$headers .= "Content-Type: text/html; charset=UTF-8\n";

	/* translators: %s is replaced with "string" */
	$pass_change_mail['subject'] = __( 'Notice of Email Change on %s' );
	$pass_change_mail['headers'] = $headers;

	//$pass_change_mail['message']  = nano_email_header( $pass_change_mail['subject'], 'This notice confirms that your email was changed' );
	$pass_change_mail['message']  = '<p>This notice confirms that your email was changed</p>';
	$pass_change_mail['message'] .= wpautop( $new_message_txt );
	//$pass_change_mail['message'] .= nano_email_footer();

	$pass_change_mail['message'] = str_replace( '###DISPLAYNAME###', $user['display_name'], $pass_change_mail['message'] );

	return $pass_change_mail;

}

add_filter( 'email_change_email', 'nano_change_email_mail_message', 10, 3 );

/**
 * Update a backer based on the user's choice when a lab note for a project they are supporting has been posted.
 *
 * @param string $post_id
 * @param string $project_id
 */

function backer_project_update_notify( $post_id, $project_id ) {

	$level_id       = nano_get_level_id( $project_id );
	$project_orders = ID_Member_Order::get_orders_by_level( $level_id );

	// If a backer donated more than once, strip them from here so they only get a single update.
	$unique_orders = unique_multidim_array( $project_orders, 'user_id' );

	foreach ( $unique_orders as $project_order ) {

		$user_id                       = $project_order->user_id;
		$user_notification_preferences = get_user_meta( $user_id, 'nano_user_email_settings', true );
		if ( isset( $user_notification_preferences['backer_notification_updates'] ) && 'backer_notification_updates' == $user_notification_preferences['backer_notification_updates'] ) {
			$recipient = get_user_by( 'id', $user_id );
			$recipient = $recipient->user_email;
			$type      = 'backer_notification_updates';
			nano_email_notify( $post_id, $recipient, $type );
		}
	}
}


/**
 * Update a creator based on the user's choice when a pledge has happened on one of their projects.
 *
 * @param string $post_id
 *
 */

function creator_pledge_update_notify( $post_id ) {
	// creator_notification_pledge
	$original_author = get_post_meta( $post_id, 'original_author', true );
	$user_id         = false !== get_user_by( 'login', $original_author ) ? get_user_by( 'login', $original_author ) : get_user_by( 'email', $original_author );
	$user_id         = $user_id->ID;

	$user_notification_preferences = get_user_meta( $user_id, 'nano_user_email_settings', true );
	if ( isset( $user_notification_preferences['creator_notification_pledge'] ) && 'creator_notification_pledge' == $user_notification_preferences['creator_notification_pledge'] ) {
		$recipient = get_user_by( 'id', $user_id );
		$recipient = $recipient->user_email;
		$type      = 'creator_notification_pledge';
		nano_email_notify( $post_id, $recipient, $type );
	}

}


/**
 * Update the creator if their project is published.
 *
 * @param $new_status
 * @param $old_status
 * @param $post
 */

function nano_notify_creator_publish( $new_status, $old_status, $post ) {

	if ( 'publish' == $new_status && 'pending' == $old_status ) {

		if ( get_post_type( $post->ID ) == 'ignition_product' ) {

			$post_id         = $post->ID;
			$original_author = get_post_meta( $post_id, 'original_author', true );
			$user_id         = false !== get_user_by( 'login', $original_author ) ? get_user_by( 'login', $original_author ) : get_user_by( 'email', $original_author );
			$recipient       = $user_id->user_email;
			$type            = 'nanosteam_project_approved';

			nano_email_notify( $post_id, $recipient, $type );
		}
	}
}

add_action( 'transition_post_status', 'nano_notify_creator_publish', 10, 3 );

/**
 * Send to Error Log if error with API
 * Send mail to admin if error
 *
 * @param array $e
 * @param string $type
 */

function stripe_error_email_admin_notification( $e, $type ) {

	if ( $type === 'overdue_payout' ) {

		$to      = get_option( 'admin_email' );
		$message = $e;
		$subject = 'A project is 7 days past the day it\'s payout was initiated and is overdue. We have not heard from Stripe.';
		wp_mail( $to, $subject, $message );

	} else {

		if ( method_exists( $e, 'getJsonBody' ) ) {

			$body = $e->getJsonBody();
			$err  = $body['error'];

			$current_user = wp_get_current_user();
			$user_id      = $current_user->ID;

			$error_msg = ( 'Status is: ' . $e->getHttpStatus() . "\n" );
			$error_msg .= ( 'Type is: ' . $err['type'] . "\n" );
			$error_msg .= isset( $err['code'] ) ? ( 'Code is: ' . $err['code'] . "\n" ) : '';
			$error_msg .= isset( $err['param'] ) ? ( 'Param is: ' . $err['param'] . "\n" ) : '';
			$error_msg .= ( 'Message is: ' . $err['message'] . "\n" );

			error_log( print_r( $error_msg . ' : ' . $type, true ) );

			$to      = get_option( 'admin_email' );
			$subject = '' != $current_user ? 'Stripe Connect API Error' : 'Stripe Webhook Error Received';
			$message = $error_msg;
			$message .= '<br><br>To disable admin emails goto plugins/ignitiondeck/classes/modules/nanosteam/stripe_connect/includes/stripe_connect.php and comment out line 594';
			wp_mail( $to, $subject, $message );

		} else {

			$current_user = wp_get_current_user();
			$to           = get_option( 'admin_email' );
			$subject      = '' != $current_user ? 'No JSON Stripe Connect API Error' : 'No JSON Stripe Webhook Error Received';
			$message      = 'There was a Stripe API or Webhook Connection error but no JSON was included to parse in their error. Included below is the error.' . "\n";
			$message      .= 'Function Name: ' . $type . "\n";
			$message      .= $e;

			wp_mail( $to, $subject, $message );
			error_log( print_r( 'No json error pack - sending whats was included: ' . $e . ' : ' . $type, true ) );
		}

	}

}


/**
 * Send message to admin when fund transfer is completed successfully
 *
 * @param string $post_id
 * @param string $project_name
 * @param string $project_url
 * @param string $creator_id
 * @param string $transaction_id
 * @param string $stripe_account
 * @param string $raised
 * @param string $transferred
 * @param string $nanosteam_fee
 * @param string $stripe_fee
 * @param string $admin_id
 * @param string $subject
 *
 */

function stripe_transfer_email_admin_notification( $post_id, $project_name, $project_url, $creator_id, $transaction_id, $stripe_account, $raised, $transferred, $nanosteam_fee, $stripe_fee, $admin_id, $subject ) {

	$admin_first = get_user_meta( $admin_id, 'first_name', true );
	$admin_last  = get_user_meta( $admin_id, 'last_name', true );

	$msg  = ( 'Project Name: ' . $project_name . "\n" );
	$msg .= ( 'Project URL: ' . $project_url . "\n" );
	$msg .= ( 'Creator: ' . get_author_posts_url( $creator_id ) . ' / ' . get_user_meta( $creator_id, 'first_name', true ) . ' ' . get_user_meta( $creator_id, 'last_name', true ) . "\n" );
	$msg .= ( 'Creator\'s Stripe Account ID: ' . $stripe_account . "\n" );
	$msg .= ( 'Stripe\'s Transaction ID: ' . $transaction_id . "\n" );
	$msg .= ( 'Funds Raised: ' . $raised . "\n" );
	$msg .= ( 'Funds Transferred: ' . ( $transferred / 100 ) . "\n" );
	$msg .= ( 'Nanosteam Fee: ' . $nanosteam_fee . "\n" );
	$msg .= ( 'Stripe Fee: ' . $stripe_fee . "\n" );
	$msg .= ( 'Transfer Initiated By: ' . $admin_first . ' ' . $admin_last . "\n" );
	$msg .= ( 'This message has also been logged in the general php error logs' . "\n" );

	error_log( print_r( $msg, true ) );

	$to      = get_option( 'admin_email' );
	$subject = $subject . $project_name;
	$message = wpautop( $msg );
	wp_mail( $to, $subject, $message );

}


/**
 * Used to notify the admin of any general errors or updates
 *
 * @param $subject
 * @param $message
 */

function email_general_admin_notification( $subject, $message ) {

	$msg  = ( 'Automated Messge: ' . "\n" );
	$msg .= $message;
	$msg .= ( 'This message has also been logged in the general php error logs' . "\n" );

	error_log( print_r( $msg, true ) );

	$to      = get_option( 'admin_email' );
	$message = wpautop( $msg );
	wp_mail( $to, $subject, $message );

}


/**
 * Send message to admin when its time to send funds for the second milestone
 *
 * @param string $post_id
 */

function stripe_matching_grant_second_milestone_email_admin( $post_id ) {

	$project_id     = get_post_meta( $post_id, 'project_uid', true );
	$transient_name = 'admin_emailed_milestone_complete_' . $project_id;


	if ( false === ( $transient_name = get_transient( $transient_name ) ) ) {
		// Transient missing, fire.

		//Project Parameters

		$ign_project_id = get_post_meta( $post_id, 'ign_project_id', true );
		$project_name   = nano_get_project_name( $ign_project_id );
		$project_url    = get_permalink( $post_id );

		$msg  = ( 'Project Name: ' . $project_name . "\n" );
		$msg .= ( 'Project URL: ' . $project_url . "\n" );
		$msg .= ( 'Notification: This matching project has now published their second labnote and their final transfer is ready to be paid.' . "\n" );
		$msg .= ( 'This email will be sent again in approximately 12 hours.' . "\n" );
		$msg .= ( 'This message has also been logged in the general php error logs' . "\n" );

		error_log( print_r( $msg, true ) );

		$to      = get_option( 'admin_email' );
		$subject = 'Second milestone has been reached for ' . $project_name . ' and is now ready for you to transfer the final payout.';
		$message = wpautop( $msg );
		wp_mail( $to, $subject, $message );

		set_transient( 'admin_emailed_milestone_complete_' . $project_id, 'emailed', 12 * HOUR_IN_SECONDS );

	}



}


function nano_admin_notify_macthing_grant_milestone() {

	$args = array(
		'post_type'      => 'ignition_product',
		'post_status'    => 'publish',
		'posts_per_page' => - 1,
	);

	add_filter( 'posts_where', 'filter_where' );
	$query = new WP_Query( $args );
	remove_filter( 'posts_where', 'filter_where' );

	if ( $query->have_posts() ) {

		while ( $query->have_posts() ) {

			$query->the_post();
			$post_id = get_the_id();

			$is_grant_approved = get_post_meta( $post_id, 'fifty_fifty_grant', true ) == 'approved' ? true : false;
			$is_project_closed   = get_post_meta( $post_id, 'ign_project_closed', true ) == '1' ? true : false;

			if ( true == $is_grant_approved && true == $is_project_closed ) {

				$first_transfer_processed_status = get_post_meta( $post_id, 'first_transfer_transaction_id', true ) != '' ? true : false;
				$final_transfer_processed_status = get_post_meta( $post_id, 'final_transfer_transaction_id', true ) != '' ? true : false;
				$second_lab_posted               = nano_second_milsetone_achieved( $post_id );

				if ( true == $second_lab_posted && true != $final_transfer_processed_status ) {

					stripe_matching_grant_second_milestone_email_admin( $post_id );

				}
			}
		}

		/* Restore original Post Data */
		wp_reset_postdata();

	}

}


/**
 * Email template headers and footers if NOT using Mandrill
 * Called through the retrieve_password_message filter.
 *
 * @param string $title
 *
 * @return string
 */

function nano_email_header( $title, $summary ) {

	$header = '
    <!doctype html>
    <html>
    <head>
        <meta name="viewport" content="width=device-width">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>' . $title . '</title>
        <style>
            @media only screen and (max-width: 620px) {
                table[class=body] h1 {
                    font-size: 28px !important;
                    margin-bottom: 10px !important;
                }
                table[class=body] p,
                table[class=body] ul,
                table[class=body] ol,
                table[class=body] td,
                table[class=body] span,
                table[class=body] a {
                    font-size: 16px !important;
                }
                table[class=body] .wrapper,
                table[class=body] .article {
                    padding: 10px !important;
                }
                table[class=body] .content {
                    padding: 0 !important;
                }
                table[class=body] .container {
                    padding: 0 !important;
                    width: 100% !important;
                }
                table[class=body] .main {
                    border-left-width: 0 !important;
                    border-radius: 0 !important;
                    border-right-width: 0 !important;
                }
                table[class=body] .btn table {
                    width: 100% !important;
                }
                table[class=body] .btn a {
                    width: 100% !important;
                }
                table[class=body] .img-responsive {
                    height: auto !important;
                    max-width: 100% !important;
                    width: auto !important;
                }
            }
            /* -------------------------------------
                PRESERVE THESE STYLES IN THE HEAD
            ------------------------------------- */
            @media all {
                .ExternalClass {
                    width: 100%;
                }
                .ExternalClass,
                .ExternalClass p,
                .ExternalClass span,
                .ExternalClass font,
                .ExternalClass td,
                .ExternalClass div {
                    line-height: 100%;
                }
                .apple-link a {
                    color: inherit !important;
                    font-family: inherit !important;
                    font-size: inherit !important;
                    font-weight: inherit !important;
                    line-height: inherit !important;
                    text-decoration: none !important;
                }
                .btn-primary table td:hover {
                    background-color: #34495e !important;
                }
                .btn-primary a:hover {
                    background-color: #34495e !important;
                    border-color: #34495e !important;
                }
            }
        </style>
    </head>
    <body class="" style="background-color: #f6f6f6; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
    <table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #f6f6f6;">
        <tr>
            <td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; text-align: center; display: block; margin: 0 auto; max-width: 580px; padding: 10px; padding-top: 50px; width: 580px;">
                <img width="250" style="max-width: 100%; height: auto;" src="' . get_site_url() . '/wp-content/themes/nanosteam_child/images/ns_logo_hz.png">
            </td>
        </tr>
        <tr>
            <td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; Margin: 0 auto; max-width: 580px; padding: 10px; width: 580px;">
                <div class="content" style="box-sizing: border-box; display: block; Margin: 0 auto; max-width: 580px; padding: 10px;">

                    <!-- START CENTERED WHITE CONTAINER -->
                    <span class="preheader" style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;">'. $summary .'</span>
                    <table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #ffffff; border-radius: 3px;">

                        <!-- START MAIN CONTENT AREA -->
                        <tr>
                            <td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;">
                                <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                                    <tr>
                                        <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">';

	return $header;
}

function nano_email_footer() {
	$footer = '
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <!-- END MAIN CONTENT AREA -->
                    </table>

                    <!-- START FOOTER -->
                    <div class="footer" style="clear: both; Margin-top: 10px; text-align: center; width: 100%;">
                        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                            <tr>
                                <td class="content-block" style="font-family: sans-serif; vertical-align: top; padding-bottom: 10px; padding-top: 10px; font-size: 12px; color: #999999; text-align: center;">
                                    <span class="apple-link" style="color: #999999; font-size: 12px; text-align: center;"><a href="https://nanosteam.org">nanosteam.org</a></span>   
                                </td>
                            </tr>
                            <tr>
                                <td class="content-block powered-by" style="font-family: sans-serif; vertical-align: top; padding-bottom: 10px; padding-top: 10px; font-size: 12px; color: #999999; text-align: center;">
                                </td>
                            </tr>
                        </table>
                    </div>
                    <!-- END FOOTER -->

                    <!-- END CENTERED WHITE CONTAINER -->
                </div>
            </td>
            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
        </tr>
    </table>
    </body>
    </html>';

	return $footer;

}