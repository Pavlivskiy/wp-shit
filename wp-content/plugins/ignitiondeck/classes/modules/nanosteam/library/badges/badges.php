<?php if ( ! defined( 'ABSPATH' ) ) {
	die( 'Direct access forbidden.' );
}


/**
 * Rewriting the Author base to "profile"
 */

function nano_author_base() {
	global $wp_rewrite;
	$author_slug             = 'profile'; // change slug name
	$wp_rewrite->author_base = $author_slug;
}
add_action( 'init', 'nano_author_base' );

/**
 * Add checked to whichever badge the user selected
 *
 * @param $user_id
 * @param $nano_badge
 *
 * @return string
 */

function nano_checked( $user_id, $nano_badge ) {
	$val = '';
	if ( get_user_meta( $user_id, 'nano_badge', true ) == $nano_badge ) {
		$val = ' checked';
	}
	return $val;
}


/**
 * Show badges on user's account page
 */

function nano_personal_badges() {

	$current_user = wp_get_current_user();
	$user_id      = $current_user->ID;
	$usermeta     = get_user_meta( $user_id );
	$nano_badge   = isset( $_POST['nano_badge'] ) ? $_POST['nano_badge'] : '';

	if ( isset( $_POST['edit-profile-submit'] ) ) {
		if ( isset( $_POST['nano_badge'] ) ) {
			$sanitized_val = filter_var( $nano_badge, FILTER_SANITIZE_STRING );
			update_user_meta( $user_id, 'nano_badge', $sanitized_val );
		}
	}

	$user_badge = array(
		'philanthropist',
		'hero_of_steam',
		'futurist',
		'visionary',
		'innovator',
		'designer',
		'scientist',
		'technologist',
		'engineer',
		'artist',
		'mathematician',
	);

	?>

	<div class="form-row form-group mb-2">
		<div class="col-12">
			<h4><?php _e( 'Select Your Badge', 'nanosteam_8n' ); ?></h4>
		</div>
		<div class="col-12">
			<?php
			foreach ( $user_badge as $item ) {
				echo '<div class="custom-control custom-radio custom-control-inline">';
				echo '<input type="radio" id="' . $item . '" class="custom-control-input" name="nano_badge" value="' . $item . '" ' . nano_checked( $user_id, $item ) . '>';
				echo '<label for="' . $item . '" class="custom-control-label" data-toggle="tooltip" data-placement="top" data-original-title="' . ucfirst( str_replace( '_', ' ', $item ) ) . '"><i class="fa-2x icon icon-biology fa-2x icon-' . $item . '"></i></label>';
				echo '</div>';
			}
			?>
		</div>
	</div>
	<?php

}

add_action( 'idc_profile_social_after', 'nano_personal_badges', 10 );

/**
 * Displaying the backer profile
 */

function nano_ide_backer_profile() {
	if ( isset( $_GET[ apply_filters( 'idc_backer_profile_slug', 'backer_profile' ) ] ) ) {
		$profile = absint( $_GET[ apply_filters( 'idc_backer_profile_slug', 'backer_profile' ) ] );
		if ( isset( $profile ) && $profile > 0 ) {
			add_filter( 'the_content', 'nano_ide_backer_profile_display' );
			add_filter( 'the_title', 'ide_backer_profile_title', 10, 2 );
			add_action( 'wp_head', 'ide_backer_profile_og' );
			add_filter( 'wp_title', 'ide_backer_profile_tab_title', 10, 2 );
		}
	}
}

/**
 * Getting the user's badges
 */

function nano_profile_badges() {

	$profile            = get_userdata( get_query_var( 'author' ) );
	$profile_user_login = $profile->user_login;
	$profile            = $profile->ID;

	$content = '';
	$user    = get_user_by( 'id', $profile );

	if ( class_exists( 'ID_Member_Order' ) ) {

		// Backer Stats
		$orders = ID_Member_Order::get_orders_by_user( $profile );

		if ( ! empty( $orders ) ) {

			$stats_backer      = array();
			$total_funded      = 0;
			$total_projects    = array();
			$post_ids          = array();
			$listed            = array();
			$anonymous_post_id = array();

			// Creating array of anon donations
			foreach ( $orders as $id_order ) {

				$project_id          = nano_project_id_by_order_level( $id_order->level_id );
				$project             = new ID_Project( $project_id );
				$post_id             = $project->get_project_postid();
				$anonymous           = ID_Member_Order::get_order_meta( ( $id_order->id - 1 ), 'anonymous_checkout', true );
				$anonymous_post_id[] = null !== $anonymous ? $post_id : '';

			}

			foreach ( $orders as $id_order ) {

				$default_project_id = nano_get_default_project_id();
				$project_id         = nano_project_id_by_order_level( $id_order->level_id );
				$project            = new ID_Project( $project_id );
				$post_id            = $project->get_project_postid();
				$the_project        = $project->the_project();
				$order_id           = $id_order->id;
				$token_price        = ID_Member_Order::get_order_meta( $id_order->id, 'pwyw_price' );

				if ( isset( $post_id ) && $default_project_id !== $project_id && null !== $token_price ) {

					$anonymous = ID_Member_Order::get_order_meta( ( $id_order->id - 1 ), 'anonymous_checkout', true );

					if ( 'PUBLISH' == strtoupper( get_post_status( $post_id ) ) && ! in_array( $post_id, $listed ) && ! in_array( $post_id, $anonymous_post_id ) ) {

						if ( null == $anonymous ) {

							$credit_backing = strpos( $id_order->transaction_id, 'credit' ) !== false ? 'credit' : 'stripe';
							$backed_amount  = null !== $token_price && $credit_backing === 'stripe' ? $token_price : 0;

							//Project funding
							$project_backed_date      = date( 'Ymd', strtotime( $id_order->order_date ) );
							$project_started          = date( 'Ymd', strtotime( get_post_meta( $post_id, 'ign_start_date', true ) ) );
							$project_ends             = date( 'Ymd', strtotime( get_post_meta( $post_id, 'ign_fund_end', true ) ) );
							$project_funded_first_day = $project_backed_date - $project_started <= 3 ? true : false;
							$project_funded_last_day  = $project_ends - $project_backed_date <= 3 ? true : false;

							$total_funded    += $backed_amount;
							$total_projects[] = $project_id;

							//Categories funded
							$categories_supported = ( get_the_terms( $post_id, 'project_category' ) );
							if ( is_array( $categories_supported ) ) {
								foreach ( $categories_supported as $category_supported ) {
									$stats_backer['category_funded'][] = $category_supported->name;
								}
							}

							// Disciplines
							// Output the category disciplines
							$disciplines = ( get_the_terms( $post_id, 'project_category' ) );
							$result      = array();

							if ( is_array( $disciplines ) ) {
								foreach ( $disciplines as $key => $discipline ) {
									$backer_disciplined    = get_term_meta( $discipline->term_id, null, true );
									$backer_disciplined    = isset( $backer_disciplined['nano_steam_disciplines'] ) ? $backer_disciplined['nano_steam_disciplines'] : null;
									$backer_result[ $key ] = isset( $backer_disciplined ) ? maybe_unserialize( $backer_disciplined[0] ) : '';
								}
							}

							$backer_discipline_cat_terms = isset( $backer_result[0] ) && count( array_unique( array_merge( $backer_result[0], $backer_result[1] ) ) ) >= 2 ? true : null;

							$stats_backer['total_funded']                    = $total_funded;
							$stats_backer['categories_supported'][]          = is_array( $categories_supported ) ? count( $categories_supported ) : 0;
							$stats_backer['disciplines_supported'][]         = $backer_discipline_cat_terms;
							$stats_backer['project_funded_within_last_3'][]  = $project_funded_last_day;
							$stats_backer['project_funded_within_first_3'][] = $project_funded_first_day;
							$stats_backer['project_successfully_funded'][]   = $post_id;

							$listed[]   = $post_id;
							$post_ids[] = $post_id;

						}
					}
				}
			}


			$stats_backer['total_projects_funded'] = isset( $stats_backer['project_successfully_funded'] ) ? count( $stats_backer['project_successfully_funded'] ) : '';

		}

		// Creator Stats
		$creator_args     = array(
			'post_type'      => 'ignition_product',
			'post_status'    => 'publish',
			'posts_per_page' => - 1,
			'tax_query'      => array(
				array(
					'taxonomy'         => 'author',
					'field'            => 'name',
					'terms'            => $profile_user_login,
					'include_children' => false,
				),
			),
		);

		$created_projects = get_posts( $creator_args );

		$stats_creator                                  = array();
		$stats_creator['total_funded']                  = '';
		$stats_creator['single_funded'][]               = '';
		$stats_creator['project_successfully_funded'][] = '';
		$stats_creator['total_categories'][]            = '';
		$stats_creator['total_disciplines'][]           = '';
		$stats_creator['total_projects_created']        = '';
		$stats_creator['total_team_members'][]          = '';
		$stats_creator['total_labnotes'][]              = '';

		if ( ! empty( $created_projects ) ) {

			$stats_creator        = array();
			$total_funded_creator = 0;

			foreach ( $created_projects as $created_project ) {

				$project_id  = get_post_meta( $created_project->ID, 'ign_project_id', true );
				$project     = new ID_Project( $project_id );
				$the_project = $project->the_project();

				if ( ! empty( $the_project ) ) {
					$post_id            = $created_project->ID;
					$successful_project = get_post_meta( $post_id, 'ign_percent_raised', true ) >= 100 ? true : false;

					$successful_project = true;
					// List projects that require successful funding to be listed
					if ( true == $successful_project ) {

						$successful_creator    = $post_id;
						$project_id            = get_post_meta( $post_id, 'ign_project_id', true );
						$project               = new ID_Project( $project_id );
						$single_funded_creator = str_replace( ',', '', nano_orders_raised( $post_id ) );
						$total_funded_creator += $single_funded_creator;

						// Output the category disciplines
						$disciplines = ( get_the_terms( $created_project->ID, 'project_category' ) );
						$result      = array();

						if ( is_array($disciplines) ) {
							foreach ( $disciplines as $key => $discipline ) {
								$disciplined    = get_term_meta( $discipline->term_id, null, true );
								$disciplined    = isset( $disciplined['nano_steam_disciplines'] ) ? $disciplined['nano_steam_disciplines'] : null;
								$result[ $key ] = isset( $disciplined ) ? maybe_unserialize( $disciplined[0] ) : '';
							}
						}

						$discipline_cat_terms = isset( $result[0] ) && isset( $result[1] ) && is_array( $result[0] ) && is_array( $result[1] ) ? array_merge( $result[0], $result[1] ) : array();

						//Stats Array
						$stats_creator['total_funded']                    = $total_funded_creator;
						$stats_creator['single_funded'][]                 = $single_funded_creator;
						$stats_creator['project_successfully_funded'][]   = $successful_creator;
						$stats_creator['total_categories'][]              = is_array( get_the_terms( $created_project->ID, 'project_category' ) ) ? count( get_the_terms( $created_project->ID, 'project_category' ) ) : 0;
						$stats_creator['total_disciplines'][]             = $discipline_cat_terms;
						$stats_creator['total_disciplines_per_project'][] = count( array_unique( $discipline_cat_terms ) );

					}
					// List projects that do not require successful funding

					//Count the number of team members for the project

					$total_team_members = get_post_meta( $post_id, 'nano_team_members', true );

					$total_team_members = is_array( $total_team_members ) ? count( get_post_meta( $post_id, 'nano_team_members', true ) ) : 0;

					if ( $total_team_members >= 2 && $total_team_members <= 4 ) {
						$stats_creator['total_team_members'][] = 'two';
					}
					if ( $total_team_members >= 5 ) {
						$stats_creator['total_team_members'][] = 'five';
					}
					$stats_creator['total_labnotes'][] = count( idfu_get_updates( $post_id ) );
				}
			}

			//Count unique disciplines from all projects
			$discipline_counts                             = isset( $stats_creator['total_disciplines'] ) ? count( array_unique( array_merge( ... $stats_creator['total_disciplines'] ) ) ) : array();
			$stats_creator['total_discipline_count']       = $discipline_counts;
			$stats_creator['total_discipline_per_project'] = isset( $stats_creator['total_disciplines_per_project'] ) ? in_array( 3, $stats_creator['total_disciplines_per_project'] ) : null;

			$stats_creator['total_projects_created'] = isset( $stats_creator['project_successfully_funded'] ) ? count( $stats_creator['project_successfully_funded'] ) : '';
			delete_user_meta( $profile, 'nano_projects_created' );
			add_user_meta( $profile, 'nano_projects_created', count( $created_projects ) );

		}
	}

	$usermeta = get_user_meta( $profile );

	echo '<div class="row pt-3 pb-5">';

	// Personal Badges
	//
	//
	if ( isset( $usermeta['nano_badge'] ) ) {

		$description = '';
		$title = 'Backer';
		if ( 'philanthropist' == $usermeta['nano_badge'][0] ) {
			$description = __( 'Philanthropist', 'nanosteam_8n' );
		}
		if ( 'hero_of_steam' == $usermeta['nano_badge'][0] ) {
			$description = __( 'Hero of STEAM', 'nanosteam_8n' );
		}
		if ( 'futurist' == $usermeta['nano_badge'][0] ) {
			$description = __( 'Futurist', 'nanosteam_8n' );
		}
		if ( 'visionary' == $usermeta['nano_badge'][0] ) {
			$description = __( 'Visionary', 'nanosteam_8n' );
		}
		if ( 'designer' == $usermeta['nano_badge'][0] ) {
			$description = __( 'Designer, creative thinker, tinker, incessant creator', 'nanosteam_8n' );
			$title = 'Creator';
		}
		if ( 'scientist' == $usermeta['nano_badge'][0] ) {
			$description = __( 'Future scientist, academic scientist, industry scientist, independent scientist', 'nanosteam_8n' );
			$title = 'Creator';
		}
		if ( 'technologist' == $usermeta['nano_badge'][0] ) {
			$description = __( 'Future technologist, academic technologist, industry technologist, independent technologist', 'nanosteam_8n' );
			$title = 'Creator';
		}
		if ( 'engineer' == $usermeta['nano_badge'][0] ) {
			$description = __( 'Future engineer, academic engineer, industry engineer, independent engineer', 'nanosteam_8n' );
			$title = 'Creator';
		}
		if ( 'artist' == $usermeta['nano_badge'][0] ) {
			$description = __( 'Future artist, artist, industry artist, independent artist', 'nanosteam_8n' );
			$title = 'Creator';
		}
		if ( 'mathematician' == $usermeta['nano_badge'][0] ) {
			$description = __( 'Future mathematician, industry mathematician, independent mathematician', 'nanosteam_8n' );
			$title = 'Creator';
		}

		echo '<div class="text-center col-sm-2" data-toggle="tooltip" data-placement="top" title="' . $description . '"><h5 class="d-block">'. $title .'</h5><i class="icon icon-physics fa-2x d-block icon-' . $usermeta['nano_badge'][0] . '"></i></div>';
		echo '<div class="col-sm-10"><h5 class="d-block">Badges</h5>';

	} else {
		echo '<div class="col"><h5 class="d-block">Badges</h5>';
	}

	// Backer Badges
	//
	//

	$base_badge_a = '<div class="mr-2 d-inline-block text-center" data-toggle="tooltip" data-placement="top" title="';
	$base_badge_b = '"><i class="icon icon-biology fa-2x icon_backer_';
	$base_badge_c = '"></i></div>';

	// Total projects created badges at 2, 5, 10

	if ( isset( $stats_backer ) && $stats_backer['total_projects_funded'] >= '2' ) {
		$variable = '2';
		if ( $stats_backer['total_projects_funded'] >= '5' ) {
			$variable = '5';
		}
		if ( $stats_backer['total_projects_funded'] >= '10' ) {
			$variable = '10';
		}
		if ( $stats_backer['total_projects_funded'] >= '100' ) {
			$variable = '100';
		}
		$message = sprintf( esc_html__( '%d+ Projects Backed', 'nanosteam_8n' ), $variable );
		$icon    = 'total_projects_funded' . $variable;
		echo $base_badge_a . $message . $base_badge_b . $icon . $base_badge_c;
	}

	// Categories Funded
	if ( isset( $stats_backer['category_funded'] ) ) {
		foreach ( array_unique( $stats_backer['category_funded'] ) as $cat_funded ) {
			echo '<div class="mr-2 d-inline-block text-center" data-toggle="tooltip" data-placement="top" title="' . $cat_funded . ' Supported"><i class="icon icon-biology fa-2x icon_category_funded_' . $cat_funded . '"></i></div>';
		}
	}

	//If Project has more than 1 discipline get super STEAM badge
	if ( isset( $stats_backer['disciplines_supported'] ) && in_array( true, $stats_backer['disciplines_supported'] ) == true ) {
		$variable = '';
		$message  = sprintf( esc_html__( 'Project supported which integrated at least 2 disciplines', 'nanosteam_8n' ), $variable );
		$icon     = 'disciplines_supported';
		echo $base_badge_a . $message . $base_badge_b . $icon . $base_badge_c;
	}

	//If the user has endorsed a project
	$endorsements       = endorsements_user_profile();
	$endorsements_total = isset( $endorsements['total'] ) && $endorsements['total'] > 0 ? true : false;
	if ( true === $endorsements_total ) {
		$variable = '';
		$message  = sprintf( esc_html__( 'Has endorsed a project', 'nanosteam_8n' ), $variable );
		$icon     = 'endorsement_total';
		echo $base_badge_a . $message . $base_badge_b . $icon . $base_badge_c;
	}

	//IF total funded over all projects exceeds defined amount
	$total_funded_badge = '';
	if ( isset( $stats_backer['total_funded'] ) && $stats_backer['total_funded'] >= '500' ) {
		$variable = '500';
		if ( $stats_backer['total_funded'] >= '1000' ) {
			$variable = '1000';
		}
		if ( $stats_backer['total_funded'] >= '5000' ) {
			$variable = '5000';
		}
		if ( $stats_backer['total_funded'] >= '10000' ) {
			$variable = '10,000';
		}
		$message = sprintf( esc_html__( '%d+ Total Backed', 'nanosteam_8n' ), $variable );
		$icon    = 'total_funded_' . $variable;
		echo $base_badge_a . $message . $base_badge_b . $icon . $base_badge_c;
		}

	echo $total_funded_badge;

	//Project funded within 3 days of opening
	if ( isset( $stats_backer['project_funded_within_first_3'] ) && in_array( true, $stats_backer['project_funded_within_first_3'] ) ) {
		$variable = '';
		$message  = sprintf( esc_html__( 'Funded within 3 days of opening', 'nanosteam_8n' ), $variable );
		$icon     = 'project_funded_within_first_3';
		if ( count( $stats_backer['project_funded_within_first_3'] ) > 1 ) {
			$message  = sprintf( esc_html__( 'Backed more than 1 project in first 3 days', 'nanosteam_8n' ), $variable );
			$icon     = 'project_funded_within_first_3_more';
		}
		echo $base_badge_a . $message . $base_badge_b . $icon . $base_badge_c;
	}

	//Project funded within 3 days of closing
	if ( isset( $stats_backer['project_funded_within_last_3'] ) && in_array( true, $stats_backer['project_funded_within_last_3'] ) ) {
		$variable = '';
		$message  = sprintf( esc_html__( 'Backed a project within 3 days of closing', 'nanosteam_8n' ), $variable );
		$icon     = 'project_funded_within_last_3';
		echo $base_badge_a . $message . $base_badge_b . $icon . $base_badge_c;
	}


	// Creator Badges
	//
	//
	$base_badge_a = '<div class="mr-2 d-inline-block text-center" data-toggle="tooltip" data-placement="top" title="';
	$base_badge_b = '"><i class="icon icon-biology fa-2x icon_creator_';
	$base_badge_c = '"></i></div>';

	// Total projects created badges at 2, 5, 10
	$projects_created_total = get_user_meta( $profile, 'nano_projects_created', true );
	if ( $projects_created_total >= '2' ) {
		$variable = '2';
		if ( $projects_created_total >= '5' ) {
			$variable = '5';
		}
		if ( $projects_created_total >= '10' ) {
			$variable = '10';
		}
		$message = sprintf( esc_html__( '%d+ Projects Created', 'nanosteam_8n' ), $variable );
		$icon    = 'nano_projects_created_' . $variable;
		echo $base_badge_a . $message . $base_badge_b . $icon . $base_badge_c;
	}

	// Project successfully funded badge
	if ( isset( $stats_creator['project_successfully_funded'] ) && in_array( true, $stats_creator['project_successfully_funded'] ) ) {
		$variable = '';
		$message  = sprintf( esc_html__( 'A project was successfully backed', 'nanosteam_8n' ), $variable );
		$icon     = 'project_successfully_funded';
		echo $base_badge_a . $message . $base_badge_b . $icon . $base_badge_c;
	}

	// If total disciplines used in all project created is 5 SUPER STEAM
	if ( isset( $stats_creator['total_discipline_count'] ) && 5 == $stats_creator['total_discipline_count'] ) {
		$variable = '';
		$message  = sprintf( esc_html__( 'All STEAM disciplines have been used', 'nanosteam_8n' ), $variable );
		$icon     = 'total_discipline_count';
		echo $base_badge_a . $message . $base_badge_b . $icon . $base_badge_c;
	}

	// If single project uses 3 or more disciplines
	if ( isset( $stats_creator['total_disciplines_per_project'] ) && in_array( 3, $stats_creator['total_disciplines_per_project'] ) == true ) {
		$variable = '';
		$message  = sprintf( esc_html__( 'At least 3 disciplines were used in a single project', 'nanosteam_8n' ), $variable );
		$icon     = 'total_disciplines_per_project';
		echo $base_badge_a . $message . $base_badge_b . $icon . $base_badge_c;
	}

	// Check to see all projects have more than 3 lab notes published combined
	if ( isset( $stats_creator['total_labnotes'] ) && array_sum( $stats_creator['total_labnotes'] ) >= 3 ) {
		$variable = '';
		$message  = sprintf( esc_html__( 'Has projects with at least 3 lab notes posted combined', 'nanosteam_8n' ), $variable );
		$icon     = 'total_labnotes';
		echo $base_badge_a . $message . $base_badge_b . $icon . $base_badge_c;
	}

	// Count the total number of team members
	if ( isset( $stats_creator['total_team_members'] ) ) {
		if ( in_array( 'two', $stats_creator['total_team_members'] ) || in_array( 'five', $stats_creator['total_team_members'] ) ) {
			$variable = '2';
			if ( in_array( 'five', $stats_creator['total_team_members'] ) ) {
				$variable = '5';
			}
			$message = sprintf( esc_html__( 'Created a project with at least %d+ team members', 'nanosteam_8n' ), $variable );
			$icon    = 'total_team_members_' . $variable;
			echo $base_badge_a . $message . $base_badge_b . $icon . $base_badge_c;
		}
	}

	// Singular Funding Achievements
	if ( isset( $stats_creator['single_funded'] ) ) {
		foreach ( $stats_creator['single_funded'] as $single_funding ) {
			$single_fund = '';
			if ( $single_funding >= 2000 ) {
				$single_fund = '2';
				if ( $single_funding >= 3000 ) {
					$single_fund = '3';
				}
				if ( $single_funding >= 5000 ) {
					$single_fund = '5';
				}
				$message = sprintf( esc_html__( 'A project received $%d000+ in backing.', 'nanosteam_8n' ), $single_fund );
				$icon = 'single_funded_' . $single_fund;
				echo $base_badge_a . $message . $base_badge_b . $icon . $base_badge_c;
			}
		}
	}

	echo '</div></div>';

}

/**
 * Getting the user's profile details
 */

// Only use this function for profile details below.
function nano_parse_website( $url ) {
	$parsed_url = parse_url( $url );

	if ( empty( $parsed_url['scheme'] ) ) {
		$url = 'http://' . ltrim( $url, '/' );
	}
	// Twitter specific. If a user has only added their uername instead of the whole URL
	if ( false !== strpos( $url, '@' ) ) {
		$url = 'http://twitter.com/' . ltrim( strstr( $url, '@' ), '@' );
	}

	return $url;
}

function nano_profile_details() {
	$profile         = get_userdata( get_query_var( 'author' ) );
	$profile         = $profile->ID;
	$content         = '';
	$user            = get_user_by( 'id', $profile );
	$name            = apply_filters( 'ide_profile_name', $user->display_name, $user );
	$nano_avatar     = get_user_meta( $profile, 'nano_idc_avatar', true );
	$nano_avatar_img = isset( $nano_avatar[0] ) && is_array( $nano_avatar ) ? $nano_avatar[0] : $nano_avatar;
	$nano_avatar_msg = '<img class="img-fluid rounded-circle" src="' . $nano_avatar_img . '">';

	$twitter_link = nano_parse_website( apply_filters( 'ide_profile_twitter_url', get_user_meta( $profile, 'twitter', true ), $user ) );
	$fb_link      = nano_parse_website( apply_filters( 'ide_profile_fb_url', get_user_meta( $profile, 'facebook', true ), $user ) );
	//$google_link  = apply_filters( 'ide_profile_google_url', get_user_meta( $profile, 'google', true ), $user );
	$website_link = nano_parse_website( apply_filters( 'ide_profile_website_url', $user->user_url, $user ) );

	// Endorsement Count
	$endorsed_projects = get_user_meta( $profile, 'project_endorsement_connection', true ) != '' ? get_user_meta( $profile, 'project_endorsement_connection', true ) : '';
	$current_user      = wp_get_current_user();
	$user_login        = $user->data->user_login;
	$counter           = 0;
	if ( $endorsed_projects ) {
		foreach ( $endorsed_projects as $endorsed_project ) {

			if ( strtoupper( get_post_status( $endorsed_project ) ) == 'PUBLISH' || $current_user->ID == $profile ) {

				$endorsements = get_post_meta( $endorsed_project, 'nano_endorsements', true );

				if ( isset( $endorsements['endorsement'][ $user_login ] ) && '' != $endorsements['endorsement'][ $user_login ] ) {
					$counter ++;
				}
			}
		}
	}
	$endorsed_projects = 0 != $counter ? $counter . ' Endorsed project' . ( $counter > 1 ? 's' : '' ) . ' <br>' : '';

	$location = get_user_meta( $profile, 'location', true ) != '' ? get_user_meta( $profile, 'location', true ) . '<br>' : '';

	$content .= '<div class="row">';
	$content .= '<div class="col-sm-2 text-center">';
	$content .= '<div class="backer_avatar">' . $nano_avatar_msg . '</div>';
	$content .= '</div>';
	$content .= '<div class="col-sm-3">';
	$content .= '<h5>' . apply_filters( 'ide_backer_name', $name, $user ) . '</h5>';
	$content .= '<p class="small muted">';
	$content .= $location;
	$content .= $endorsed_projects;
	$content .= __( 'Joined', 'nanosteam_8n' ) . ' ' . date( 'Y', strtotime( $user->user_registered ) ) . '</p>';
	$content .= '<div class="small">' . ( ! empty( $website_link ) && 'http://' !== $website_link ? '<a href="' . $website_link . '" class="website text-dark mr-2"><i class="fa fa-home" aria-hidden="true"></i></a>' : '' );
	$content .= ( ! empty( $twitter_link )  && 'http://' !== $twitter_link ? '<a href="' . $twitter_link . '" class="twitter text-dark mr-2"><i class="fa fa-twitter"></i></a>' : '' );
	$content .= ( ! empty( $fb_link )  && 'http://' !== $fb_link ? '<a  href="' . $fb_link . '" class=" text-dark mr-2 facebook"><i class="fa fa-facebook"></i></a>' : '' );
	$content .= do_action( 'ide_after_backer_data' ) . '</div>';
	$content .= '</div>';
	$content .= '<div class="col-sm-7">';
	$content .= '<h5>Bio</h5>';
	$content .= wpautop( wp_trim_words( apply_filters( 'ide_profile_description', $user->description, $user ), 100 ) );
	$content .= '</div></div>';
	echo $content;
}

/**
 * Dispalying the user's backer profile details
 *
 * @return array
 */

function nano_ide_backer_profile_display() {

	$profile = get_userdata( get_query_var( 'author' ) );
	$profile = $profile->ID;
	$content = '';
	$user    = get_user_by( 'id', $profile );

	if ( class_exists( 'ID_Member_Order' ) ) {
		// Backer Stats
		$orders      = ID_Member_Order::get_orders_by_user( $profile );
		$orders      = array_reverse( $orders );
		$order_count = '';

		if ( ! empty( $orders ) ) {

			$order_content = '<div class="backer_projects nano-style row pb-5">';

			$listed            = array();
			$anonymous_post_id = array();

			// Creating array of anon donations
			foreach ( $orders as $id_order ) {

				$project_id          = nano_project_id_by_order_level( $id_order->level_id );
				$project             = new ID_Project( $project_id );
				$post_id             = $project->get_project_postid();
				$anonymous           = ID_Member_Order::get_order_meta( ( $id_order->id - 1 ), 'anonymous_checkout', true );
				$anonymous_post_id[] = null !== $anonymous ? $post_id : '';

			}

			foreach ( $orders as $order ) {

				$order_level        = $order->level_id;
				$project_id         = nano_project_id_by_order_level( $order_level );
				$default_project_id = nano_get_default_project_id();
				$project            = new ID_Project( $project_id );
				$post_id            = $project->get_project_postid();
				$token_price        = ID_Member_Order::get_order_meta( $order->id, 'pwyw_price' );

				if ( isset( $post_id ) && $default_project_id !== $project_id && null !== $token_price ) {

					$anonymous           = ID_Member_Order::get_order_meta( ( $order->id - 1 ), 'anonymous_checkout', true );

					if ( 'PUBLISH' == strtoupper( get_post_status( $post_id ) ) && ! in_array( $post_id, $listed ) && ! in_array( $post_id, $anonymous_post_id ) ) {

						if ( null == $anonymous ) {

							ob_start();
							include( locate_template( 'content-ignition_product.php' ) );
							$order_content .= ob_get_contents();
							ob_end_clean();

							$listed[] = $post_id;

						}
					}
				}
			}

			$order_content .= '</div>';
			$order_count    = count( $listed );

		}

		$content .= '<div>';
		$content .= ( isset( $order_content ) ? $order_content : '' );
		$content .= '</div>';
	}
	$values            = array();
	$values['total']   = $order_count;
	$values['content'] = $content;

	return $values;
}

/**
 * Dispalying the user's creator profile details
 *
 * @return array
 */

function nano_ide_creator_profile_display() {
	$profile  = get_userdata( get_query_var( 'author' ) );
	$profile  = $profile->ID;
	$profile2 = get_userdata( get_query_var( 'author' ) );
	$profile2 = $profile2->user_login;

	$content = '';
	$user    = get_user_by( 'id', $profile );

	$creator_args = array(
		'post_type'      => 'ignition_product',
		'post_status'    => 'publish',
		'posts_per_page' => - 1,
		'tax_query'      => array(
			array(
				'taxonomy'         => 'author',
				'field'            => 'name',
				'terms'            => $profile2,
				'include_children' => false,
			),
		),
	);

	$created_projects = get_posts( $creator_args );
	if ( ! empty( $created_projects ) ) {
		$order_content = '<div class="backer_projects nano-style row pb-5">';
		foreach ( $created_projects as $created_project ) {
			$project_id = get_post_meta( $created_project->ID, 'ign_project_id', true );
			$project    = new ID_Project( $project_id );
			$post_id    = $project->get_project_postid();

			if ( isset( $post_id ) ) {

				if ( strtoupper( get_post_status( $post_id ) ) == 'PUBLISH' ) {

					ob_start();
					include( locate_template( 'content-ignition_product.php' ) );
					$order_content .= ob_get_contents();
					ob_end_clean();

				}
			}
		}
		$order_content .= '</div>';
		$order_count    = count( $created_projects );

		$content .= '<div>';
		$content .= ( isset( $order_content ) ? $order_content : '' );
		$content .= '</div>';
	}
	$value            = array();
	$value['content'] = $content;
	$value['total']   = isset( $order_count ) ? $order_count : 0;

	return $value;
}

/**
 * Displaying the user's labnotes from their created projects
 *
 * @return array
 */

function labnotes() {

	$profile    = get_userdata( get_query_var( 'author' ) );
	$profile_id = $profile->ID;

	$args = array(
		'author'         => $profile_id,
		'orderby'        => 'post_date',
		'order'          => 'ASC',
		'post_type'      => 'ignition_update',
		'posts_per_page' => - 1, // no limit
	);

	$user_posts = get_posts( $args );

	$content = '<div class="row">';
	foreach ( $user_posts as $post ) {
		setup_postdata( $post );
		$post_status       = get_post_meta( $post->ID, 'nano_update_backer_' . $post->ID, true );
		$parent_project_id = get_post_meta( $post->ID, 'idfu_project_update', true );
		$parent_post_id    = getPostbyProductID( $parent_project_id );
		$card_link         = '';
		$post_title        = '';
		$post_excerpt      = wp_trim_words( $post->post_content, 20, '...' );

		if ( 'backer' == $post_status ) {

			$user = wp_get_current_user()->ID;

			if ( class_exists( 'ID_Member_Order' ) ) {

				// Get current post's meta
				$project_id = $parent_project_id;

				// Current user's backed projects
				$user_backed_projects = get_user_meta( $user, 'backed_projects', true ) ? get_user_meta( $user, 'backed_projects', true ) : array();

				// The project's team members always see all posts
				$project_team = wp_get_post_terms( $parent_post_id, 'author' );

				$team_member_id = array();
				foreach ( $project_team as $team_member ) {
					$team_member_name    = $team_member->name;
					$team_member_details = get_user_by( 'login', $team_member_name );
					$team_member_id[]    = $team_member_details->ID;
				}

				if ( ! in_array( $project_id, $user_backed_projects ) ) {
					if ( ! in_array( $user, $team_member_id ) ) {
						$post_title .= '<h6 class="title text-uppercase heading-normal">' . get_the_title( $post->ID ) . ' <small>(Backers Only)</small></h6>';
						$card_link   = '';
					}
				}

				if ( in_array( $user, $team_member_id ) || in_array( $project_id, $user_backed_projects ) ) {
					$post_title .= '<a href="' . get_the_permalink( $post->ID ) . '" class="update-link" alt="' . get_the_title( $post->ID ) . '" title="' . get_the_title( $post->ID ) . '">';
					$post_title .= '<h6 class="title text-uppercase heading-normal">' . get_the_title( $post->ID ) . '</h6>';
					$post_title .= '</a>';
					$card_link   = '<a class="card-link" href="' . get_permalink( $post->ID ) . '"></a>';
				}
			}
		} else {
			$post_title .= '<a href="' . get_the_permalink( $post->ID ) . '" class="update-link" alt="' . get_the_title( $post->ID ) . '" title="' . get_the_title( $post->ID ) . '">';
			$post_title .= '<h6 class="title text-uppercase heading-normal">' . get_the_title( $post->ID ) . '</h6>';
			$post_title .= '</a>';
			$card_link   = '<a class="card-link" href="' . get_permalink( $post->ID ) . '"></a>';
		}

		$content .= '<div class="col-sm-12">';
		$content .= '<div class="card">';
		$content .= $card_link;
		$content .= '<div class="card-body">';
		$content .= $post_title;
		$content .= '<p class="card-text small mb-2">' . $post_excerpt . '<br>';
		$content .= '<p class="small muted">Posted: ' . date( get_option( 'date_format' ), strtotime( $post->post_modified ) ) . '</p></p>';
		$content .= '</div></div></div>';
	}
	$content .= '</div>';
	wp_reset_postdata();

	$value            = array();
	$value['total']   = count( $user_posts );
	$value['content'] = $content;

	return $value;
}
