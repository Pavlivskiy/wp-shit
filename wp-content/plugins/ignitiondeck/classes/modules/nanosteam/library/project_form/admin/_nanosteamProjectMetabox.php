<?php if ( ! defined( 'ABSPATH' ) ) {
	die( 'Direct access forbidden.' );
}

$vars = get_post_meta( $post->ID );

function nano_goal_milestones( $vars ) {

	$milestones = unserialize( $vars['nano_goal_milestones'][0] );

	$field = array(
		'label'  => '<strong>' . __( 'Milestone', 'nanosteam_8n' ) . '</strong>',
		'value'  => ( isset( $milestones[0] ) ? $milestones[0] : '' ),
		'name'   => 'nano_goal_milestones[]',
		'id'     => 'nano_goal_milestones',
		'type'   => 'text',
		'misc'   => 'placeholder="Milestone"',
		'class'  => '',
		'wclass' => 'nano-left',
		'before' => '<h3>Milestones</h3>',
	);

	return $field;

}

function nano_goal_milestones_extra( $vars ) {

	$field = array(
		'label'  => '<strong>' . __( 'Milestone', 'nanosteam_8n' ) . '</strong>',
		'value'  => ( isset( $vars ) ? $vars : '' ),
		'name'   => 'nano_goal_milestones[]',
		'id'     => 'nano_goal_milestones' . $vars,
		'type'   => 'text',
		'misc'   => 'placeholder="Milestone"',
		'class'  => 'ign_projectmeta_third',
		'wclass' => 'nano-left',
	);

	return $field;

}

function nano_goal_milestones_date( $vars ) {

	$date = isset( $vars['nano_goal_milestone_date'][0] ) ? unserialize( $vars['nano_goal_milestone_date'][0] ) : $vars;

	$date_format = get_option( 'date_format' );

	$date_alt = ! isset( $vars['fund_types'] ) && ! isset( $vars['nano_goal_milestone_date'] ) ? date( $date_format, strtotime( $vars ) ) : '';

	$field = array(
		'label'  => '<strong>' . __( 'Milestone Date', 'nanosteam_8n' ) . '</strong>',
		'value'  => ( isset( $vars['nano_goal_milestone_date'] ) ? date( $date_format, strtotime( isset( $date[0] ) ? $date[0] : '' ) ) : $date_alt ),
		'name'   => 'nano_goal_milestone_date[]',
		'id'     => '',
		'type'   => 'date',
		'misc'   => 'placeholder="Date"',
		'class'  => 'cmb_datepicker',
		'wclass' => 'nano-left nano-clear',
	);

	return $field;
}

function milestones( $vars ) {
	$milestones = unserialize( $vars['nano_goal_milestones'][0] );
	$dates      = unserialize( $vars['nano_goal_milestone_date'][0] );
	$i          = 1;
	$form       = array();
	$form[]     = nano_goal_milestones( $vars );
	$form[]     = nano_goal_milestones_date( $vars );

	if ( $milestones ) {
		foreach ( $milestones as $k => $v ) {

			if ( $k < 1 ) {
				continue;
			}

			$form[] = nano_goal_milestones_extra( $v );
			$form[] = nano_goal_milestones_date( isset( $dates[ $i ] ) ? $dates[ $i ] : '' );
			$i ++;

		}
	}

	return $form;

}

function nano_team_members( $vars ) {

	$field = array(
		'label'  => '<strong>' . __( 'Team Members', 'nanosteam_8n' ) . '</strong>',
		'value'  => ( isset( $vars ) ? $vars : '' ),
		'name'   => 'nano_team_members[]',
		'id'     => 'nano_team_members',
		'type'   => 'text',
		'misc'   => 'placeholder="Enter the email of a team member"',
		'class'  => '',
		'wclass' => 'nano-left',
	);

	return $field;
}

function nano_team_members_extra( $vars ) {

	$teammembers = unserialize( $vars['nano_team_members'][0] );

	$form = array();

	if ( $teammembers ) {

		$form[] = array(
			'before' => '<h3>Team Member Emails</h3>',
		);

		foreach ( $teammembers as $k => $v ) {
			$form[] = nano_team_members( $v );
		}
	}

	return $form;

}

$team_leader = isset( $vars['original_author'][0] ) ? $vars['original_author'][0] : '';
$team_leader = '' != $team_leader ? ( false !== get_user_by( 'login', $team_leader ) ? get_user_by( 'login', $team_leader ) : get_user_by( 'email', $team_leader ) ) : '';

$team_leader_image = get_user_meta( $team_leader->ID, 'nano_idc_avatar', true );

$nano_custom_fields = array(
	array(
		'label' => '<strong>' . __( 'Project Significance', 'nanosteam_8n' ) . '</strong>',
		'misc'  => 'rows="6"',
		'value' => ( isset( $nano_project_significance ) ? $nano_project_significance : '' ),
		'id'    => 'nano_project_significance',
		'name'  => 'nano_project_significance',
		'type'  => 'textarea',
		'class' => 'nano-full-width',
	),
	array(
		'label'  => '<strong>' . __( 'Location', 'nanosteam_8n' ) . '</strong>',
		'value'  => ( isset( $vars['nano_location_city'][0] ) ? $vars['nano_location_city'][0] : '' ),
		'name'   => 'nano_location_city',
		'id'     => 'nano_location_city',
		'type'   => 'text',
		'class'  => '',
		'wclass' => 'nano-left',
		'misc'   => 'placeholder="City"',
	),
	array(
		'label'  => '<strong>' . __( 'Country', 'nanosteam_8n' ) . '</strong>',
		'value'  => ( isset( $vars['nano_location_country'][0] ) ? $vars['nano_location_country'][0] : '' ),
		'name'   => 'nano_location_country',
		'id'     => 'nano_location_country',
		'type'   => 'text',
		'class'  => '',
		'wclass' => 'nano-left',
		'misc'   => 'placeholder="Country"',
	),
	array(
		'label'  => '<strong>' . __( 'State', 'nanosteam_8n' ) . '</strong>',
		'value'  => ( isset( $vars['nano_location_state'][0] ) ? $vars['nano_location_state'][0] : '' ),
		'name'   => 'nano_location_state',
		'id'     => 'nano_location_state',
		'type'   => ( isset( $vars['nano_location_country'][0] ) && 'United States of America' == $vars['nano_location_country'][0] ? 'text' : 'hidden' ),
		'class'  => '',
		'wclass' => 'nano-left',
		'misc'   => 'placeholder="State"',
	),
	array(
		'label'  => '<strong>' . __( 'Timeline', 'nanosteam_8n' ) . '</strong>',
		'value'  => ( isset( $vars['nano_goal_timeline'][0] ) ? $vars['nano_goal_timeline'][0] : '' ),
		'name'   => 'nano_goal_timeline',
		'id'     => 'nano_goal_timeline',
		'type'   => 'textarea',
		'class'  => 'nano-full-width',
		'wclass' => 'form-row',
		'misc'   => 'placeholder="Please provide an overview of the timeline for your project." rows=6 ',
	),
	array(
		'label'  => '<strong>' . __( 'Team Leader Image', 'nanosteam_8n' ) . '</strong>',
		'value'  => ( '' != $team_leader_image ? $team_leader_image : '' ),
		'misc'   => ( '' != $team_leader_image ? 'data-url="' . $team_leader_image . '" accept="image/*"' : 'accept="image/*"' ),
		'name'   => 'nano_team_leader',
		'id'     => 'nano_team_leader',
		'type'   => 'text',
		'wclass' => 'form-row onethird',
		'after'  => '<img src=' . ( '' != $team_leader_image ? $team_leader_image : '' ) . '>',
	),
	array(
		'label'  => '<strong>' . __( 'Team Leader Bio', 'nanosteam_8n' ) . '</strong>',
		'value'  => ( isset( $vars['nano_team_leader_bio'][0] ) ? $vars['nano_team_leader_bio'][0] : '' ),
		'name'   => 'nano_team_leader_bio',
		'id'     => 'nano_team_leader_bio',
		'type'   => 'textarea',
		'misc'   => 'placeholder="Team Leader Bio" rows=6 ',
		'class'  => 'nano-full-width',
		'wclass' => 'nano-full-width',
	),
);


$form_nano       = new ID_Form( $nano_custom_fields );
$form_milestones = new ID_Form( milestones( $vars ) );
$form_team       = new ID_Form( nano_team_members_extra( $vars ) );

echo apply_filters( 'nano_fifty_form', $form_nano->build_form() );
echo apply_filters( 'nano_fifty_form', $form_team->build_form() );
echo apply_filters( 'nano_fifty_form', $form_milestones->build_form() );
