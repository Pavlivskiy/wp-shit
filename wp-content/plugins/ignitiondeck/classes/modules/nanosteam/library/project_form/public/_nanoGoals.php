<?php if ( ! defined( 'ABSPATH' ) ) {
	die( 'Direct access forbidden.' );
}

if ( isset( $_GET['edit_project'] ) || isset( $_GET['create_project'] ) ) {

	if ( isset( $vars['fund_types'] ) ) {
		$fund_type_args            = array(
			'label'  => __( 'Level Fund Type', 'ignitiondeck' ),
			'name'   => 'project_fund_type[]',
			'id'     => 'level_project_fund_type_1',
			'type'   => 'hidden',
			'wclass' => 'd-none',
			'value'  => $vars['fund_types'],
		);
		$fund_type_args['options'] = array();
		// Pushing both the options, removing on checks then
		$option = array(
			'value' => 'capture',
			'title' => 'Immediately Deliver Funds',
		);
		array_push( $fund_type_args['options'], $option );
		$option = array(
			'value' => 'preauth',
			'title' => '100% Threshold',
		);
		array_push( $fund_type_args['options'], $option );

		if ( 'capture' == $vars['fund_types'] || 'c_sub' == $vars['fund_types'] ) {
			// Remove 'preauth' (100% Threshold) option
			for ( $i = 0; $i < count( $fund_type_args['options'] ); $i ++ ) {
				if ( 'preauth' == $fund_type_args['options'][ $i ]['value'] ) {
					$removal_index = $i;
				}
			}
			if ( isset( $removal_index ) ) {
				unset( $fund_type_args['options'][ $removal_index ] );
				unset( $removal_index );
			}
		}
		if ( 'preauth' == $vars['fund_types'] ) {
			// Remove the 'capture' (Immediately Deliver Funds) option
			for ( $i = 0; $i < count( $fund_type_args['options'] ); $i ++ ) {
				if ( 'capture' == $fund_type_args['options'][ $i ]['value'] ) {
					$removal_index = $i;
				}
			}
			if ( isset( $removal_index ) ) {
				unset( $fund_type_args['options'][ $removal_index ] );
				unset( $removal_index );
			}
		}
		$fund_type_args['options'] = apply_filters( 'ide_fund_options', $fund_type_args['options'] );
		$form[]                    = $fund_type_args;
	}
	$form[] = array(
		'heading'     => ( true == team_lead_only_goals() ? __( 'Goals', 'nanosteam_8n' ) : null ),
		'help'        => nano_help_field( 'goals' ),
		'wclass'      => ( team_lead_only_goals() == true ? '' : 'd-none' ),
		'description' => nano_description_field( 'goals' ),
	);

	$form[] = nano_goals_long_description( $vars );
	$form[] = nano_goals_abstract( $vars );

	if ( true == self::team_lead_only() || isset( $_GET['create_project'] ) ) {

		$form[] = nano_goals_project_significance( $vars );
		$form[] = nano_goal_timeline( $vars );

		$form[] = array(
			'heading'     => __( 'Milestones', 'nanosteam_8n' ),
			'help'        => nano_help_field( 'milestones' ),
			'description' => nano_description_field( 'milestones' ),
			'before'      => '<div class="select-wrapper">',
		);

		$milestone_data = isset( $vars['post_id'] ) ? get_post_meta( $vars['post_id'] ) : false;
		$milestones     = isset( $milestone_data ) ? unserialize( $milestone_data['nano_goal_milestones'][0] ) : '';
		$dates          = isset( $milestone_data ) ? unserialize( $milestone_data['nano_goal_milestone_date'][0] ) : '';
		$i              = 1;
		$len            = is_array( $milestones ) ? count( $milestones ) : 0;


		if ( $milestones ) {
			foreach ( $milestones as $k => $v ) {
				$form[] = nano_goal_milestones( $vars, $k );
				$form[] = nano_goal_milestones_date( $vars, $k );
				$i ++;
			}
		} else {
			$form[] = nano_goal_milestones( $vars, 0 );
			$form[] = nano_goal_milestones_date( $vars, 0 );
			$form[] = nano_goal_milestones( $vars, 1 );
			$form[] = nano_goal_milestones_date( $vars, 1 );
		}

		$form[] = array( 'after' => '</div>' );

		// If the project is submitted or published no further milestones may be added
		if ( isset( $_GET['create_project'] ) || ( isset( $_GET['edit_project'] ) && get_post( $_GET['edit_project'] )->post_status == 'draft' ) ) {
			$form[] = array( 'after' => '<div class="pb-3"><button class="add_field_button btn btn-sm btn-primary">Add Milestone</button></div>' );
		}
	}

	$form[] = nano_goal_faq( $vars );

}


function team_lead_only_goals() {
	$team_lead_only = true;
	if ( isset( $_GET['edit_project'] ) ) {
		$team_leader    = wp_get_current_user()->user_login == get_post_meta( $_GET['edit_project'], 'original_author', true ) ? true : false;
		$team_lead_only = true != $team_leader ? false : true;
	}

	return $team_lead_only;
}


// GOALS

function nano_goals_long_description( $vars ) {

	$post_id = isset( $vars['post_id'] ) ? $vars['post_id'] : '';
	$value = isset( $vars['post_id'] ) ? ( '' !== get_post_meta( $post_id, 'nano_project_long_description', true ) ? get_post_meta( $post_id, 'nano_project_long_description', true ) : get_post_meta( $post_id, 'project_long_description', true ) ) : '';

	$field = array(
		'label'  => __( 'Goals', 'nanosteam_8n' ),
		'value'  => $value,
		'name'   => 'nano_project_long_description',
		'id'     => 'project_long_description_goals',
		'type'   => ( team_lead_only_goals() == true ? 'textarea' : 'hidden' ),
		'class'  => 'required js-auto-size',
		'wclass' => 'col',
		'misc'   => 'maxlength=750 placeholder="What are the goals of this project" required',
		'before' => '<div class="form-group form-row pb-3 ' . ( team_lead_only_goals() == true ? '' : 'd-none' ) . '">',
		'after'  => '</div>',
	);

	return $field;
}

function nano_goals_abstract( $vars ) {

	$post_id = isset( $vars['post_id'] ) ? $vars['post_id'] : '';
	$value = isset( $vars['post_id'] ) ? ( '' !== get_post_meta( $post_id, 'nano_project_short_description', true ) ? get_post_meta( $post_id, 'nano_project_short_description', true ) : get_post_meta( $post_id, 'ign_project_description', true ) ) : '';

	$field = array(
		'label'       => __( 'Abstract', 'nanosteam_8n' ),
		'value'       => $value,
		'name'        => 'nano_project_short_description',
		'id'          => 'nano_project_short_description',
		'type'        => ( team_lead_only_goals() == true ? 'textarea' : 'hidden' ),
		'class'       => 'required js-auto-size',
		'wclass'      => 'col',
		'misc'        => 'maxlength=750 rows="2" placeholder="Write an abstract or summary of your project. Include why your project is important and what you want to accomplish." required',
		'heading'     => ( true == team_lead_only_goals() ? __( 'Abstract', 'nanosteam_8n' ) : null ),
		'help'        => nano_help_field( 'abstract' ),
		'description' => nano_description_field( 'abstract' ),
		'before'      => '<div class="form-group form-row pb-3 ' . ( team_lead_only_goals() == true ? '' : 'd-none' ) . '">',
		'after'       => '</div>',
	);

	return $field;
}

function nano_goals_project_significance( $vars ) {

	//view_var(get_post_meta( $vars['post_id'] ));

	$value = isset( $vars['post_id'] ) ? get_post_meta( $vars['post_id'], 'nano_project_significance', true ) : '';

	$field = array(
		'label'       => __( 'Project Significance', 'nanosteam_8n' ),
		'value'       => $value,
		'name'        => 'nano_project_significance',
		'id'          => 'nano_project_significance',
		'type'        => 'textarea',
		'class'       => 'required js-auto-size',
		'wclass'      => 'col',
		'misc'        => 'maxlength=750 rows="2" placeholder="Write an abstract or summary of your project. Include why your project is important and what you want to accomplish." required',
		'heading'     => __( 'Project Significance', 'nanosteam_8n' ),
		'help'        => nano_help_field( 'project_significance' ),
		'description' => nano_description_field( 'project_significance' ),
		'before'      => '<div class="form-group form-row pb-3">',
		'after'       => '</div>',
	);

	return $field;
}

function nano_goal_timeline( $vars ) {

	$vars = isset( $vars['post_id'] ) ? get_post_meta( $vars['post_id'] ) : '';

	$field = array(
		'label'       => __( 'Timeline', 'nanosteam_8n' ),
		'value'       => ( isset( $vars['nano_goal_timeline'][0] ) ? $vars['nano_goal_timeline'][0] : '' ),
		'name'        => 'nano_goal_timeline',
		'id'          => 'nano_goal_timeline',
		'type'        => 'textarea',
		'class'       => 'required js-auto-size',
		'wclass'      => 'col',
		'misc'        => 'maxlength=500 placeholder="Please provide an overview of the timeline for your project." required',
		'heading'     => __( 'Timeline', 'nanosteam_8n' ),
		'help'        => nano_help_field( 'timeline' ),
		'description' => nano_description_field( 'timeline' ),
		'before'      => '<div class="form-group form-row pb-3">',
		'after'       => '</div>',

	);

	return $field;
}

function nano_goal_milestones( $vars, $key ) {

	$vars = isset( $vars['post_id'] ) ? get_post_meta( $vars['post_id'] ) : '';

	$milestones = isset( $vars['nano_goal_milestones'] ) ? maybe_unserialize( $vars['nano_goal_milestones'][0] ) : '';

	$field = array(
		'label'  => __( 'Milestone Date', 'nanosteam_8n' ),
		'value'  => ( isset( $milestones[ $key ] ) ? $milestones[ $key ] : '' ),
		'name'   => 'nano_goal_milestones[]',
		'id'     => '',
		'type'   => 'text',
		'misc'   => 'maxlength=200 placeholder="Milestone" ' . ( $key < 2 ? 'required' : '' ),
		'class'  => 'required',
		'wclass' => 'col-sm-9 col-7',
		'before' => '<div class="form-group form-row">',
	);

	return $field;

}

function nano_goal_milestones_date( $vars, $key ) {

	$vars = isset( $vars['post_id'] ) ? get_post_meta( $vars['post_id'] ) : $vars;

	$date = isset( $vars['nano_goal_milestone_date'][0] ) ? maybe_unserialize( $vars['nano_goal_milestone_date'][0] ) : $vars;

	$date_format = get_option( 'date_format' );

	$field = array(
		'label'       => __( 'Milestone Date', 'nanosteam_8n' ),
		'value'       => ( isset( $vars['nano_goal_milestone_date'] ) && isset( $date[ $key ] ) ? $date[ $key ] : '' ),
		'name'        => 'nano_goal_milestone_date[]',
		'id'          => '',
		'type'        => 'text',
		'misc'        => 'placeholder="Date select"',
		'class'       => '',
		'wclass'      => 'col-sm-3 col-5',
		'input_addon' => ( $key <= 1 ? null : array(
			'<a href="#" class="remove_field btn btn-sm btn-primary px-3"><i class="fa fa-times" aria-hidden="true"></i></a>',
			'right',
		) ),
		'after'       => '</div>',
	);

	return $field;

}

function nano_goal_faq( $vars ) {

	$field = array(
		'label'       => __( 'Project FAQ', 'nanosteam_8n' ),
		'value'       => ( isset( $vars['project_faq'] ) ? $vars['project_faq'] : '' ),
		'name'        => 'project_faq',
		'id'          => 'project_faq',
		'type'        => ( team_lead_only_goals() == true ? 'wpeditor' : 'hidden' ),
		'wclass'      => 'col wpeditor',
		'heading'     => ( team_lead_only_goals() == true ? __( 'Frequently Asked Questions', 'nanosteam_8n' ) : null ),
		'help'        => nano_help_field( 'frequently_asked_questions' ),
		'description' => nano_description_field( 'frequently_asked_questions' ),
		'before'      => '<div class="form-group form-row pb-3 ' . ( true == team_lead_only_goals() ? '' : 'd-none' ) . '">',
		'after'       => '</div>',
	);

	return $field;
}
