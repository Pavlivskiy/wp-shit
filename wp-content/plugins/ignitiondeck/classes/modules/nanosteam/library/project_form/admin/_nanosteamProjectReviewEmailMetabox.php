<?php if ( ! defined( 'ABSPATH' ) ) {
	die( 'Direct access forbidden.' );
}


function nano_project_email( $post_id ) {

	$project_subjects = get_post_meta( $post_id, 'project_review_subject' );
	$project_messages = get_post_meta( $post_id, 'project_review_message' );

	$field[] = '';

	if ( isset( $project_subjects ) && is_array( $project_subjects ) && count( $project_subjects ) > 0 ) {

		$project_subjects = array_reverse( $project_subjects, true );

		foreach ( $project_subjects as $key => $value ) {

			$subject = $value;
			$message = $project_messages[ $key ];

			$field[] = nano_project_subject( $subject, $key );
			$field[] = nano_project_message( $message, $key );

		}
	}

	return $field;

}

function nano_project_subject( $value, $key ) {

	$key = $key + 1;

	$field = array(
		'value'  => $value,
		'name'   => 'project_review_subject_email_' . $key,
		'id'     => 'project_review_subject_email_' . $key,
		'type'   => 'text',
		'class'  => ' nano-full-width',
		'wclass' => 'form-row project_review_subject',
		'misc'   => 'rows=6 style="margin-top:20px" placeholder="Subject" readonly',
		'after'  => '',
		'before' => '<p><strong>Message ' . $key . '</strong></p>',
	);

	return $field;
}

function nano_project_message( $value, $key ) {

	$field = array(
		'value'  => $value,
		'name'   => 'project_review_message_email_' . $key,
		'id'     => 'project_review_message_email_' . $key,
		'type'   => 'textarea',
		'class'  => ' nano-full-width',
		'wclass' => 'form-row project_review_message',
		'misc'   => 'rows=6 style="margin-top:20px" placeholder="Message" readonly',
		'after'  => '',
	);

	return $field;
}

$post_id = $_GET['post'];

if (!function_exists('nano_project_email')) {
	$form = new NANO_Form( nano_project_email( $post_id ) );
	echo apply_filters( 'nano_project_review', $form->build_form() );
}
