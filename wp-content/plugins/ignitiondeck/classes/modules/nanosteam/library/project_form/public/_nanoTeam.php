<?php if ( ! defined( 'ABSPATH' ) ) {
	die( 'Direct access forbidden.' );
}

if ( ( isset( $_GET['edit_project'] ) && self::team_lead_only() == true ) || isset( $_GET['create_project'] ) ) {

	$form[] = array(
		'before' => wp_nonce_field( 'nano_teammember_secure', 'nano_teammember_security' ),
	);

	$form[] = nano_team( $vars );
	$form[] = nano_team_leader_image( $vars );
	$form[] = nano_team_leader_image_hidden( $vars );
	$form[] = nano_team_leader_image_check( $vars );
	$form[] = nano_team_leader_name( $vars );
	$form[] = nano_team_leader_title( $vars );
	$form[] = nano_team_leader_bio( $vars );

	$form[] = array(
		'before'  => '<div class="teammember-wrapper">',
		'heading' => __( 'Team Members', 'nanosteam_8n' ),
		'help' => nano_help_field( 'team_members' ),
		'description' => nano_description_field( 'team_members' ),
	);

	$teammember  = isset( $vars['post_id'] ) ? get_post_meta( $vars['post_id'] ) : '';
	$teammembers = isset( $teammember['nano_team_members'] ) ? unserialize( $teammember['nano_team_members'][0] ) : false;

	$form[] = array( 'before' => '<div class="form-group form-row teammember">' );

	if ( $teammembers ) {
		$teammembers = array_unique( $teammembers );
		foreach ( $teammembers as $k => $v ) {
			$form[] = nano_team_members( $vars, $k );
		}
	} else {
		$form[] = nano_team_members( $vars, 0 );
	}

	$form[] = array( 'before' => '</div>' ); // Closing Team Member Email Columns

	$form[] = array( 'after' => '<button class="mb-3 add_field_button_members btn btn-sm btn-primary' . ( false == $teammembers ? ' fade ' : '' ) . '">Add Another Team Member</button></div>' );

	// Endorsements

	$form[] = array(
		'before' => wp_nonce_field( 'nano_endorsement_secure', 'nano_endorsement_security' ),
	);

	$form[] = nano_endorsement_title( $vars );

	$form[] = array(
		'before' => '<div class="endorser-wrapper">',
	);

	$endorser  = isset( $vars['post_id'] ) ? get_post_meta( $vars['post_id'] ) : '';
	$endorsers = isset( $endorser['nano_endorsement_members'] ) ? unserialize( $endorser['nano_endorsement_members'][0] ) : false;

	$form[] = array( 'before' => '<div class="form-group form-row teammember">' );

	if ( $endorsers ) {
		$endorsers = array_unique( $endorsers );
		foreach ( $endorsers as $k => $v ) {
			$form[] = nano_endorsement_members( $vars, $k );
		}
	} else {
		$form[] = nano_endorsement_members( $vars, 0 );
	}

	$form[] = nano_save_continue( $vars );

	$form[] = array( 'before' => '</div>' ); // Closing Endorsement Email Columns

	$form[] = array( 'after' => '<button class="add_field_button_endorsement btn btn-sm btn-primary' . ( false == $endorsers ? ' fade ' : '' ) . '">Add Another Endorser</button></div>' );

}

// TEAM

function nano_team() {

	$field = array(
		'heading'     => __( 'Team', 'nanosteam_8n' ),
		'help'        => nano_help_field('team'),
		'description' => nano_description_field( 'team' ),
		'wclass'      => 'pb-3',
	);

	return $field;
}

function nano_team_leader_image( $vars ) {

	$vars            = isset( $vars['post_id'] ) ? get_post_meta( $vars['post_id'] ) : '';
	$current_user    = wp_get_current_user();
	$user_id         = $current_user->ID;
	$team_leader     = isset( $vars['original_author'][0] ) ? $vars['original_author'][0] : '';
	$team_leader     = isset( $vars['original_author'][0] ) ? get_user_by( 'login', $team_leader ) : null;
	$team_leader_id  = isset( $vars['original_author'][0] ) ? $team_leader->ID : $user_id;
	$nano_avatar     = get_user_meta( $team_leader_id, 'nano_idc_avatar', true );
	$nano_avatar_img = isset( $nano_avatar[0] ) && is_array( $nano_avatar ) ? $nano_avatar[0] : $nano_avatar;
	$image           = $nano_avatar_img;

	$field = array(
		'label'   => __( 'Team Leader Photo', 'nanosteam_8n' ),
		'value'   => ( '' != $image ? $image : '' ),
		'misc'    => ( '' != $image ? 'data-url="' . $image . '" accept="image/*"' : 'accept="image/*"' ) . ' required',
		'name'    => 'nano_team_leader',
		'id'      => 'nano_team_leader',
		'type'    => 'file',
		'class'   => 'nano_team_leader nano-image-file',
		'wclass'  => 'col-sm-4',
		'heading' => __( 'Team Leader', 'nanosteam_8n' ),
		'help'    => nano_help_field('nano_team_leader'),
		'before'  => '<div class="nano-team-leader-image nano-upload form-row pb-3">',
		'after'   => '<div class="w-100"></div><div id="count" class="d-none">2</div>
                    <div class="d-none hidden_crop"></div><div class="croppie-container pb-3 pt-4 col-sm-4 mb-4">' . ( '' == $image ? '
                    <div class="upload-msg text-secondary teamlead_img">' . __( 'Select your project photo', 'nanosteam_8n' ) . '</div>' : '' ) . '
                    <div class="nano-upload-wrap"><div class="upload-container"></div>
                    <div class="d-flex justify-content-between">
                    <button class="vanilla-rotate btn btn-sm btn-primary px-2 py-0" data-deg="-90">Rotate Left</button>
                    <button class="vanilla-rotate btn btn-sm btn-primary px-2 py-0" data-deg="90">Rotate Right</button>
                    </div>
                    </div></div></div>',
	);

	return $field;
}

function nano_team_leader_image_hidden( $vars ) {

	$vars = isset( $vars['post_id'] ) ? get_post_meta( $vars['post_id'] ) : '';

	$current_user = wp_get_current_user();
	$user_id      = $current_user->ID;

	$team_leader    = isset( $vars['original_author'][0] ) ? $vars['original_author'][0] : '';
	$team_leader    = isset( $vars['original_author'][0] ) ? get_user_by( 'login', $team_leader ) : '';
	$team_leader_id = isset( $vars['original_author'][0] ) ? $team_leader->ID : $user_id;

	$nano_avatar     = get_user_meta( $team_leader_id, 'nano_idc_avatar', true );
	$nano_avatar_img = isset( $nano_avatar[0] ) && is_array( $nano_avatar ) ? $nano_avatar[0] : $nano_avatar;
	$image           = $nano_avatar_img;

	$field = array(
		'label'  => __( 'Team Leader Photo', 'nanosteam_8n' ),
		'value'  => ( '' != $image ? $image : '' ),
		'misc'   => ( '' != $image ? 'data-url="' . $image . '" accept="image/*"' : 'accept="image/*"' ),
		'name'   => 'nano_team_leader_hidden',
		'id'     => 'nano_team_leader_hidden',
		'type'   => 'hidden',
		'class'  => 'nano_team_leader_hidden',
		'wclass' => '',
	);

	return $field;
}

function nano_team_leader_image_check() {

	$field = array(
		'value'  => 'no',
		'name'   => 'nano_team_leader_removed',
		'id'     => 'nano_team_leader_removed',
		'type'   => 'hidden',
		'wclass' => 'hide',
	);

	return $field;
}

function nano_team_leader_name() {

	$post_id      = isset( $_GET['edit_project'] ) ? $_GET['edit_project'] : '';
	$current_user = wp_get_current_user();
	$org_author   = get_post_meta( $post_id, 'original_author', true );
	$org_author   = '' != $org_author ? ( false !== get_user_by( 'login', $org_author ) ? get_user_by( 'login', $org_author ) : get_user_by( 'email', $org_author ) ) : '';
	$name         = '' != $org_author ? $org_author->data->display_name : $current_user->data->display_name;

	$field = array(
		'label'       => __( 'Bio', 'nanosteam_8n' ),
		'value'       => $name,
		'name'        => 'nano_team_leader_name',
		'id'          => 'nano_team_leader_name',
		'type'        => 'text',
		'misc'        => 'maxlength=150 placeholder="Team Leader Name" required',
		'class'       => 'required',
		'wclass'      => 'col-sm-6',
		'heading'     => __( 'Biography', 'nanosteam_8n' ),
		'help'        => nano_help_field( 'bio' ),
		'description' => nano_description_field( 'bio' ),
		'before'      => '<div class="form-group form-row pb-3">',
		'after'       => '',
	);

	return $field;
}

function nano_team_leader_title() {

	$post_id      = isset( $_GET['edit_project'] ) ? $_GET['edit_project'] : '';
	$current_user = wp_get_current_user();
	$org_author   = get_post_meta( $post_id, 'original_author', true );
	$org_author   = '' != $org_author ? ( false !== get_user_by( 'login', $org_author ) ? get_user_by( 'login', $org_author ) : get_user_by( 'email', $org_author ) ) : '';
	$user_id      = '' != $org_author ? $org_author->ID : $current_user->ID;
	$title        = get_user_meta( $user_id, 'nano_user_title', true );

	$field = array(
		'label'  => __( 'Bio', 'nanosteam_8n' ),
		'value'  => $title,
		'name'   => 'nano_team_leader_title',
		'id'     => 'nano_team_leader_title',
		'type'   => 'text',
		'misc'   => 'maxlength=200 placeholder="Team Leader Title" required',
		'class'  => 'required',
		'wclass' => 'col-sm-6',
		'before' => '',
		'after'  => '</div>',
	);

	return $field;
}

function nano_team_leader_bio( $vars ) {

	//vars
	$vars = isset( $vars['post_id'] ) ? get_post_meta( $vars['post_id'] ) : '';

	$field = array(
		'label'  => __( 'Bio', 'nanosteam_8n' ),
		'value'  => ( isset( $vars['nano_team_leader_bio'][0] ) ? $vars['nano_team_leader_bio'][0] : '' ),
		'name'   => 'nano_team_leader_bio',
		'id'     => 'nano_team_leader_bio',
		'type'   => 'textarea',
		'misc'   => 'maxlength=500 placeholder="Team Leader Bio" required',
		'class'  => 'required js-auto-size',
		'wclass' => 'col',
		'before' => '<div class="form-group form-row pb-3">',
		'after'  => '</div>',
	);

	return $field;
}

function nano_team_members( $vars, $key ) {

	$vars       = isset( $vars['post_id'] ) ? get_post_meta( $vars['post_id'] ) : '';
	$teammember = isset( $vars['nano_team_members'] ) ? unserialize( $vars['nano_team_members'][0] ) : '';

	$field = array(
		'label'       => __( 'Team Members', 'nanosteam_8n' ),
		'value'       => ( isset( $teammember[ $key ] ) ? $teammember[ $key ] : '' ),
		'name'        => 'nano_team_members[]',
		'id'          => 'nano_team_members',
		'type'        => 'email',
		'misc'        => 'maxlength=254 placeholder="Enter the email of a team member" ' . ( isset( $teammember[ $key ] ) ? 'data-email-org="' . $teammember[ $key ] . '"' : '' ),
		'class'       => 'team_email_va',
		'wclass'      => 'col-sm-6 form-group' . ( 0 == $key ? ' org-field' : '' ),
		'input_addon' => array(
			'<a data-container="body" data-toggle="popover" data-placement="right" data-content="test" href="#" class="invite_user btn btn-sm btn-primary px-3 d-none">Invite</a>',
			'right',
		),
		'before'      => '',
		'after'       => '<div class="w-100"></div>',
	);

	return $field;
}

// Endorsements

function nano_endorsement_title() {

	$field = array(
		'heading'     => __( 'Endorsements', 'nanosteam_8n' ),
		'help'        => nano_help_field( 'endorsements' ),
		'description' => nano_description_field( 'endorsements' ),
		'wclass'      => 'pb-3',
	);

	return $field;
}

function nano_endorsement_members( $vars, $key ) {

	$post_id                = isset( $vars['post_id'] ) ? $vars['post_id'] : '';
	$vars                   = isset( $vars['post_id'] ) ? get_post_meta( $post_id ) : '';
	$teammember             = isset( $vars['nano_endorsement_members'] ) ? unserialize( $vars['nano_endorsement_members'][0] ) : '';
	$nano_match_grant_check = isset( $vars['post_id'] ) && get_post_meta( $vars['post_id'], 'nano_fifty_grant_apply', true ) == 'yes' ? 'required' : '';

	// Endorsements

	$team_member_key = isset( $teammember[ $key ] ) ? $teammember[ $key ] : '';
	$user            = get_user_by( 'email', $team_member_key );
	$user_login      = isset( $user->user_login ) ? $user->user_login : '';
	$endorsements    = get_post_meta( $post_id, 'nano_endorsements', true );
	$endorsement     = '';
	if ( isset( $user->user_login ) ) {
		$endorsement  = '<div class="w-100"></div><div class="mb-2"><textarea class="form-control small js-auto-size" disabled>';
		$endorsement .= '' != $endorsements && isset( $endorsements['endorsement'][ $user_login ] ) ? $endorsements['endorsement'][ $user_login ] : 'Pending Endorsement';
		$endorsement .= '</textarea></div>';
	}

	//$test = isset($teammember[$key]) && $teammember[$key] =! '' ? '<a href="#" class="remove_field btn btn-sm btn-primary px-3 '. $hidden .'"><i class="fa fa-times" aria-hidden="true"></i></a> here' : '';

	$field = array(
		'label'       => __( 'Endorsers', 'nanosteam_8n' ),
		'value'       => ( isset( $teammember[ $key ] ) ? $teammember[ $key ] : '' ),
		'name'        => 'nano_endorsement_members[]',
		'id'          => 'nano_endorsement_members',
		'type'        => 'email',
		'misc'        => 'maxlength=254 placeholder="Enter the email of an endorser" ' . $nano_match_grant_check . ( isset( $teammember[ $key ] ) ? ' data-email-org="' . $teammember[ $key ] . '"' : '' ),
		'class'       => 'endorser_email_va',
		'wclass'      => 'mb-2',
		'input_addon' => array(
			'<a data-container="body" data-toggle="popover" data-placement="right" data-content="test" href="#" class="invite_endorser btn btn-sm btn-primary px-3 d-none">Invite</a>',
			'right',
		),
		'before'      => '<div class="form-group col-sm-6 ' . ( 0 == $key ? 'org-field' : '' ) . '">',
		'after'       => $endorsement . '</div><div class="w-100"></div>',
	);

	return $field;
}

function nano_project_endorsements( $post_id, $team_member_key ) {

	$endorsements = get_post_meta( $post_id, 'nano_endorsements', true );

	$field = '';

	if ( '' != $endorsements ) {
		foreach ( $endorsements['endorsement'] as $key => $endorsement ) {
			$user       = get_user_by( 'email', $team_member_key );
			$user_login = $user->user_login;

			if ( $user_login == $key ) {
				$field = '<div class="w-100"></div><div class="mb-2"><textarea class="form-control js-auto-size" readonly>' . $endorsement . '</textarea></div>';
			} else {
				$field = '<div class="w-100"></div><div class="mb-2"><textarea class="form-control js-auto-size" readonly>Pending endorsement.</textarea></div>';
			}
		}
	}

	return $field;

}

function nano_save_continue( $vars ) {

	$save_continue = isset( $vars['post_id'] ) ? get_post_meta( $vars['post_id'], 'save_continue', true ) : '';

	$field = array(
		'value'  => $save_continue,
		'id'     => 'save_continue',
		'name'   => 'save_continue',
		'type'   => 'hidden',
		'wclass' => 'd-none',
	);

	return $field;
}
