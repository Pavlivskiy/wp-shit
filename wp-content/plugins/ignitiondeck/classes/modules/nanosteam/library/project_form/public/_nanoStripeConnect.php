<?php if ( ! defined( 'ABSPATH' ) ) {
	die( 'Direct access forbidden.' );
}


$current_user           = wp_get_current_user();
$user_id                = $current_user->ID;
// $check_creds            = md_sc_creds( $user_id ); // This may have deleted a user's account. Keeping for future review.
$sc_params              = nano_get_sc_params( $user_id );
$check_creds            = $sc_params;
$bank_base              = isset( $sc_params->external_accounts->data ) ? $sc_params->external_accounts->data : false;
$biz_type               = isset( $sc_params->legal_entity->type ) ? $sc_params->legal_entity->type : '';
$biz_tax_id_no_provided = isset( $sc_params->legal_entity->business_tax_id_provided ) && true === $sc_params->legal_entity->business_tax_id_provided ? 'Verified' : '';
$biz_street             = isset( $sc_params->legal_entity->address->line1 ) ? $sc_params->legal_entity->address->line1 : '';
$biz_zip                = isset( $sc_params->legal_entity->address->postal_code ) ? $sc_params->legal_entity->address->postal_code : '';
$biz_city               = isset( $sc_params->legal_entity->address->city ) ? $sc_params->legal_entity->address->city : '';
$biz_state              = isset( $sc_params->legal_entity->address->state ) ? $sc_params->legal_entity->address->state : '';
$owner_first_name       = isset( $sc_params->legal_entity->first_name ) ? $sc_params->legal_entity->first_name : '';
$owner_last_name        = isset( $sc_params->legal_entity->last_name ) ? $sc_params->legal_entity->last_name : '';
$owner_dob_mon          = isset( $sc_params->legal_entity->dob->month ) ? $sc_params->legal_entity->dob->month : '';
$owner_dob_day          = isset( $sc_params->legal_entity->dob->day ) ? $sc_params->legal_entity->dob->day : '';
$owner_dob_year         = isset( $sc_params->legal_entity->dob->year ) ? $sc_params->legal_entity->dob->year : '';
$last_4_ssn             = isset( $sc_params->legal_entity->ssn_last_4_provided ) || isset( $sc_params->legal_entity->personal_id_number_provided ) ? ( true === $sc_params->legal_entity->ssn_last_4_provided || true === $sc_params->legal_entity->personal_id_number_provided ? 'Verified' : 'Unverified' ) : '';
$biz_dba                = isset( $sc_params->business_name ) ? $sc_params->business_name : '';
$url                    = isset( $sc_params->business_url ) ? $sc_params->business_url : '';
$routing_number         = isset( $bank_base[0]->routing_number ) ? $bank_base[0]->routing_number : '';
$account_number         = isset( $bank_base[0]->last4 ) && '' !== $bank_base[0]->last4 ? '********' . $bank_base[0]->last4 : '';
$account_number_inc     = isset( $bank_base[0]->last4 ) && '' !== $bank_base[0]->last4 ? $bank_base[0]->last4 : '';
$tos_confirm            = isset( $sc_params->tos_acceptance->date ) && is_int( $sc_params->tos_acceptance->date ) ? 'checked="checked"' : '';
$register_btn           = isset( $sc_params ) ? true : false;
$verification           = isset( $sc_params ) ? verification_notify_type( $sc_params ) : '';


if ( isset( $sc_params ) ) {

	$data              = isset( $sc_params->verification ) ? $sc_params->verification : '';
	$verification_type = isset( $data->fields_needed ) ? ( $data->fields_needed ? $data->fields_needed : array() ) : array();

	if ( '' == $verification ) {
		$message = '<span class="ml-2 small text-success">Connected</span>';
	} elseif ( is_array( $verification ) && 'rejected' == $verification[1] ) {
		$message = '<span class="ml-2 small text-danger">Rejected</span>';
	} elseif ( is_array( $verification ) && 'rejected' !== $verification[1] || count( $verification_type > 0 ) ) {
		$message = '<span class="ml-2 small text-info">Pending</span>';
	} elseif ( is_string( $verification ) ) {
		$message = '<span class="ml-2 small text-info">Under Review</span>';
	} else {
		$message = '';
	}
}


// Business Type


if ( ( ! isset( $sc_params ) && ! isset( $_GET['payment_settings'] ) ) || ( isset( $_GET['payment_settings'] ) && isset( $sc_params ) ) ) {

	$form[] = array(
		'before' => wp_nonce_field( 'nano_stripe_connect_secure', 'nano_stripe_connect_security' ),
	);

	$form[] = array(
		'before' => '<div class="funding" id="funding_container">' . ( isset( $_GET['payment_settings'] ) ? '<h2 class="pb-3">' . __( 'Funding Type', 'nanosteam_8n' ) . $message . '</h2>' : '' ),
	);


	if ( is_array( $verification ) && 'rejected' !== $verification[1] ) {


		$details = isset( $sc_params->verification->details_code ) ? $sc_params->verification->details_code : '';
		$status  = isset( $sc_params->verification->status ) ? '<div class="alert alert-info" role="alert">Review status: ' . $sc_params->verification->status . '. Details: ' . $details . '.</div>' : '';

		$form[] = array(
			'label'       => __( 'Identity document', 'nanosteam_8n' ),
			'value'       => '',
			'misc'        => 'accept="image/*" required',
			'name'        => 'identity_document',
			'id'          => 'identity_document',
			'type'        => 'file',
			'class'       => 'required is-invalid form-control',
			'wclass'      => 'col-sm-4',
			'help'        => __( 'Stripe has some additional requirements before we can setup your account. Please provide a scanned JPG or PNG copy of a government issued ID', 'nanosteam_8n' ),
			'description' => __( 'Stripe has some additional requirements before we can setup your account. Please upload a color scanned JPG or PNG copy of a Passport, government-issued ID, or driver\'s license', 'nanosteam_8n' ),
			'heading'     => __( 'Identity Document Requirment', 'nanosteam_8n' ),
			'before'      => $status . '<div class="form-group form-row pb-3">',
			'after'       => '</div>',
		);
	}

	$form[] = array(
		'label'   => __( 'Individual', 'nanosteam_8n' ),
		'name'    => 'biz_type',
		'id'      => 'individual',
		'type'    => 'radio',
		'value'   => 'individual',
		'wclass'  => 'required custom-control custom-radio custom-control-inline',
		'class'   => 'custom-control-input biz_type',
		'misc'    => ( 'individual' == $biz_type ? 'checked' : ( 'company' != $biz_type ? 'checked' : '' ) ) . ' required',
		'heading' => __( 'Bank Account Holder', 'nanosteam_8n' ),
		'help'    => nano_help_field( 'biz_type' ),
		'before'  => '<div class="form-group form-row  pb-3"><div class="col-sm">',
	);

	$form[] = array(
		'label'  => __( 'Business / Non-Profit', 'nanosteam_8n' ),
		'name'   => 'biz_type',
		'id'     => 'company',
		'type'   => 'radio',
		'value'  => 'company',
		'wclass' => 'required custom-control custom-radio custom-control-inline',
		'class'  => 'custom-control-input biz_type',
		'misc'   => ( 'company' == $biz_type ? 'checked' : '' ) . ' required',
		'after'  => '</div></div>',
	);

// Company or Non-Profit

	if ( 'company' == $biz_type ) {

		//Business Name
		$form[] = array(
			'label'       => __( 'Business name', 'nanosteam_8n' ),
			'name'        => 'biz_dba',
			'id'          => 'biz_dba',
			'type'        => 'text',
			'value'       => $biz_dba,
			'class'       => ( verification_needed( $sc_params, 'legal_entity.business_name', '' )[1] ),
			'wclass'      => 'col-sm-3 biz_acct',
			'misc'        => ' placeholder="' . ( verification_needed( $sc_params, 'legal_entity.business_name', 'Business Name' )[0] ) . '" required',
			'description' => __( 'Business Name', 'nanosteam_8n' ),
			'before'      => '<div class="form-group form-row  pb-3">',
		);

		//Tax ID Number
		$form[] = array(
			'label'       => __( 'Business number (Tax ID / EIN):', 'nanosteam_8n' ),
			'name'        => 'biz_tax_id_no',
			'id'          => 'biz_tax_id_no',
			'type'        => 'text',
			'value'       => '',
			'class'       => ( verification_needed( $sc_params, 'legal_entity.business_tax_id', '' )[1] ),
			'wclass'      => 'col-sm-3 biz_acct',
			'misc'        => ' placeholder="' . ( verification_needed( $sc_params, 'legal_entity.business_tax_id', $biz_tax_id_no_provided )[0] ) . '" required',
			'description' => __( 'Business number (Tax ID / EIN):', 'nanosteam_8n' ),
			'after'       => '</div>',
		);

	}

// Account Details

	// First Name
	$form[] = array(
		'label'       => __( 'First name', 'nanosteam_8n' ),
		'name'        => 'owner_first_name',
		'id'          => 'owner_first_name',
		'type'        => 'text',
		'value'       => $owner_first_name,
		'class'       => 'nan_check ' . ( verification_needed( $sc_params, 'legal_entity.first_name', '' )[1] ),
		'wclass'      => 'col-sm-4',
		'misc'        => ' placeholder="' . ( verification_needed( $sc_params, 'legal_entity.first_name', 'First Name' )[0] ) . '" required',
		//'heading' => __('Your personal details', 'nanosteam_8n'),
		'help'        => nano_help_field( 'owner_first_name' ),
		'description' => __( 'Legal Name', 'nanosteam_8n' ),
		'before'      => '<div class="form-group form-row pb-3 legal_add">',
	);

	// Last Name
	$form[] = array(
		'label'  => __( 'Last Name', 'nanosteam_8n' ),
		'name'   => 'owner_last_name',
		'id'     => 'owner_last_name',
		'type'   => 'text',
		'value'  => $owner_last_name,
		'class'  => 'nan_check required' . ( verification_needed( $sc_params, 'legal_entity.last_name', '' )[1] ),
		'wclass' => 'col-sm-4',
		'misc'   => ' placeholder="' . ( verification_needed( $sc_params, 'legal_entity.last_name', 'Last Name' )[0] ) . '" required',
		'after'  => '</div>',
	);

	//DOB Month
	$form[] = nano_entity_dob_month( $owner_dob_mon, $sc_params );

	//DOB Day
	$form[] = nano_entity_dob_day( $owner_dob_day, $sc_params );

	//DOB Year
	$form[] = nano_entity_dob_year( $owner_dob_year, $sc_params );

	//SIN
	$form[] = array(
		'label'       => __( 'SIN', 'nanosteam_8n' ),
		'name'        => 'owner_personal_id_no',
		'id'          => 'owner_personal_id_no',
		'type'        => 'text',
		'value'       => null,
		'class'       => 'nan_check ' . ( verification_needed( $sc_params, 'legal_entity.personal_id_number', '' )[1] ),
		'wclass'      => 'col-sm-6',
		'misc'        => ' placeholder="' . ( verification_needed( $sc_params, 'legal_entity.personal_id_number', $last_4_ssn )[0] ) . '" ' . ( 'Verified' == verification_needed( $sc_params, 'legal_entity.personal_id_number', $last_4_ssn )[0] ? '' : 'required' ) . '',
		'description' => ( 'Required' == verification_needed( $sc_params, 'legal_entity.personal_id_number', $last_4_ssn )[0] ? __( 'Stripe requires your full 9 digit SSN', 'nanosteam_8n' ) : __( 'Last 4 digits of SSN', 'nanosteam_8n' ) ),
		'help'        => nano_help_field( 'owner_personal_id_no' ),
		'before'      => '<div class="form-group form-row pb-3">',
		'after'       => '</div>',
	);


// Individual & Company

	//Street
	$form[] = array(
		'label'  => __( 'Street', 'nanosteam_8n' ),
		'name'   => 'biz_street',
		'id'     => 'biz_street',
		'type'   => 'text',
		'value'  => $biz_street,
		'class'  => 'nan_check required' . ( verification_needed( $sc_params, 'legal_entity.address.line1', '' )[1] ),
		'wclass' => 'col-sm-6 biz_street',
		'misc'   => ' placeholder="' . ( verification_needed( $sc_params, 'legal_entity.address.line1', 'Street' )[0] ) . '" required',
		//'description' => __('Your Address', 'nanosteam_8n'),
		'before' => '<div class="form-group form-row  pb-3">',
		'after'  => '</div>',
	);

	//City
	$form[] = array(
		'label'  => __( 'City', 'nanosteam_8n' ),
		'name'   => 'biz_city',
		'id'     => 'biz_city',
		'type'   => 'text',
		'value'  => $biz_city,
		'class'  => 'nan_check required' . ( verification_needed( $sc_params, 'legal_entity.address.city', '' )[1] ),
		'wclass' => 'col-sm-6',
		'misc'   => ' placeholder="' . ( verification_needed( $sc_params, 'legal_entity.address.city', 'City' )[0] ) . '" required',
		'before' => '<div class="form-group form-row  pb-3">',
		'after'  => '</div>',
	);

	//State
	$form[] = nano_entity_state( $biz_state, $sc_params );

	//Postal
	$form[] = array(
		'label'  => __( 'Zip', 'nanosteam_8n' ),
		'name'   => 'biz_zip',
		'id'     => 'biz_zip',
		'type'   => 'text',
		'value'  => $biz_zip,
		'class'  => 'nan_check ' . ( verification_needed( $sc_params, 'legal_entity.address.postal_code', '' )[1] ),
		'wclass' => 'col-sm-2',
		'misc'   => ' placeholder="' . ( verification_needed( $sc_params, 'legal_entity.address.postal_code', 'ZIP Code' )[0] ) . '" required',
		//'before' => '<div class="form-group form-row  pb-3">',
		'after'  => '</div>',
	);

	//Website URL
	$form[] = array(
		'label'       => __( 'Your website:', 'nanosteam_8n' ),
		'name'        => 'url',
		'id'          => 'url',
		'type'        => 'text',
		'value'       => $url,
		'class'       => '',
		'wclass'      => 'col-sm-6',
		'misc'        => ' placeholder="mycompany.com" ',
		'description' => __( 'Your Website', 'nanosteam_8n' ),
		'before'      => '<div class="form-group form-row  pb-3">',
		'after'       => '</div>',
	);


// Banking Information

	$form[] = array(
		'label'       => __( 'Account number', 'nanosteam_8n' ),
		'name'        => 'account_number',
		'id'          => 'account_number',
		'type'        => 'text',
		'value'       => '',
		'class'       => 'nan_check',
		'wclass'      => 'col-sm-6',
		'misc'        => ' placeholder="' . $account_number . '"  data-base="' . $account_number_inc . '" ' . ( $sc_params ? '' : 'required' ) . '',
		'heading'     => __( 'Account Details', 'nanosteam_8n' ),
		'help'        => nano_help_field( 'routing_number' ),
		'description' => __( 'Account Number', 'nanosteam_8n' ),
		'before'      => '<div class="form-group form-row  pb-3">',
		'after'       => '</div>',
	);


	$form[] = array(
		'label'       => __( 'Routing number', 'nanosteam_8n' ),
		'name'        => 'routing_number',
		'id'          => 'routing_number',
		'type'        => 'text',
		'value'       => $routing_number,
		'class'       => 'nan_check',
		'wclass'      => ( ! $sc_params ? 'col-sm-3' : 'col-sm-6' ),
		'misc'        => ' min="000000000" max="999999999" placeholder="123456789" data-base="' . $routing_number . '" required',
		'description' => __( 'Routing Number', 'nanosteam_8n' ),
		'before'      => '<div class="form-group form-row  pb-3">',
		'after'       => ( $sc_params ? '</div>' : null ),
	);
	if ( ! $sc_params ) {
		$form[] = array(
			'label'       => __( 'Routing number', 'nanosteam_8n' ),
			'name'        => 'routing_number_validate',
			'id'          => 'routing_number_validate',
			'type'        => 'text',
			'value'       => $routing_number,
			'class'       => 'nan_check',
			'wclass'      => 'col-sm-3',
			'misc'        => ' placeholder="' . __( 'Confirm Routing Number', 'nanosteam_8n' ) . '" required',
			'description' => null,
			'after'       => '</div>',
		);
	}


	// Returned bank account token for client creation.

	$form[] = array(
		'label'  => __( 'Token', 'nanosteam_8n' ),
		'name'   => 'bk_token',
		'id'     => 'bk_token',
		'type'   => 'hidden',
		'value'  => '',
		'wclass' => 'd-none',
	);

	$form[] = array(
		'value'       => ( true == $register_btn ? __( 'Update account', 'nanosteam_8n' ) : __( 'Register your account', 'nanosteam_8n' ) ),
		'name'        => ( ! $sc_params ? 'funding_submit' : 'account_update' ),
		'type'        => 'submit',
		'id'          => ( ! $sc_params ? 'nano_stripe_connect' : 'nano_stripe_connect_update' ),
		'class'       => 'btn-primary' . ( true == $register_btn ? ' disabled' : '' ),
		'wclass'      => 'col-sm-6' . ( true == $register_btn ? ' disabled' : '' ),
		'misc'        =>  ' formnovalidate' . ( true == $register_btn || ! $sc_params ? ' disabled' : '' ),
		'description' => ( ! $sc_params ? __( 'By registering you agree to the', 'nanosteam_8n' ) . ' <a data-toggle="modal" data-target="#nano-terms-conditions" target="_blank" href="">terms of service</a>.' : '<a data-toggle="modal" data-target="#nano-terms-conditions" target="_blank" href="">Terms of service</a>' ),
		'before'      => '<div class="form-group form-row pb-3">',
		'after'       => '</div>',
	);

	$form[] = array( 'before' => '</div>' ); // Closing funding container
	$form[] = array(
		'before' => '
        <div class="modal fade" tabindex="-1" role="dialog" id="nano-terms-conditions">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        ' . nano_modal_page_load( 'terms-of-service' ) . '
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn-sm btn btn-secondary" data-dismiss="modal"><i class="fa fa-close"></i></button>
                    </div>
                </div>
            </div>
        </div>',
	);

} else {
	if ( isset( $verification ) && '' != $verification ) {
		$form[] = array(
			'before' => '<div class="account_details mt-3"><h4>' . __( 'Account Details', 'nanosteam_8n' ) . '</h4>' . __( 'Your Funding Account has been setup but there are still some requirements before you can begin accepting payments for your projects.', 'nanosteam_8n' ) . ' <br><a class="mt-3 btn btn-sm btn-primary" href="' . md_get_durl() . '?payment_settings=1">Finish funding account setup</a></div>',
		);
	} elseif ( isset( $sc_params ) ) {
		$form[] = array(
			'before' => '<div class="account_details mt-3"><h4>' . __( 'Account Details', 'nanosteam_8n' ) . '</h4>' . __( 'Your account has been verified and connected. You can review account details and make changes', 'nanosteam_8n' ) . ' <br><a class="mt-3 btn btn-sm btn-primary" href="' . md_get_durl() . '?payment_settings=1">Funding Account</a>.</div>',
		);
	}
}

if ( isset( $_GET['payment_settings'] ) && isset( $sc_params ) ) {

	$form_nano = new NANO_Form( $form );
	echo '<hr>';
	echo $form_nano->build_form();
}

/**
 * Add highlihghts to required fields for verification needs.
 *
 * @param $sc_params
 * @param $parameter
 * @param $message
 *
 * @return array
 */

function verification_needed( $sc_params, $parameter, $message ) {
	$verification = isset( $sc_params->verification->fields_needed ) ? $sc_params->verification->fields_needed : array();

	$result = array( $message, '' );

	if ( count( $verification ) > 0 ) {
		foreach ( $verification as $needed ) {
			if ( $needed === $parameter ) {
				$result = array( 'Required', ' is-invalid ' );
			} else {
				$result = array( $message, '' );
			}
		}
	} else {
		if ( 'legal_entity.personal_id_number' === $parameter ) {
			$result = array( '****', '' );
		}
	}

	return $result;
}


/**
 * DOB Month
 *
 * @param $owner_dob_mon
 * @param $sc_params
 *
 * @return array
 */
function nano_entity_dob_month( $owner_dob_mon, $sc_params ) {

	$form = array(
		'label'       => __( 'Month', 'nanosteam_8n' ),
		'value'       => $owner_dob_mon,
		'name'        => 'owner_dob_mon',
		'id'          => 'owner_dob_mon',
		'type'        => 'select',
		'class'       => 'nan_check required' . ( verification_needed( $sc_params, 'legal_entity.dob.month', '' )[1] ),
		'wclass'      => 'col-sm-4 ',
		'description' => __( 'Date of birth', 'nanosteam_8n' ),
		'before'      => '<div class="form-group w-50 form-row pb-3">',
		'misc'        => 'required',
	);

	$cat_options   = array();
	$cat_options[] = array(
		'value' => '',
		'title' => 'Month',
	);
	for ( $i = 01; $i <= 12; $i ++ ) {
		$cat_options[] = array(
			'value' => "$i",
			'title' => "$i",
		);
	}

	$form['options'] = $cat_options;

	return $form;
}


/**
 * DOB day
 *
 * @param $owner_dob_day
 * @param $sc_params
 *
 * @return array
 */

function nano_entity_dob_day( $owner_dob_day, $sc_params ) {

	$form = array(
		'label'  => __( 'Day', 'nanosteam_8n' ),
		'value'  => $owner_dob_day,
		'name'   => 'owner_dob_day',
		'id'     => 'owner_dob_day',
		'type'   => 'select',
		'class'  => 'nan_check required' . ( verification_needed( $sc_params, 'legal_entity.dob.day', '' )[1] ),
		'wclass' => 'col-sm-4 ',
		'misc'   => 'required',
	);

	$cat_options   = array();
	$cat_options[] = array(
		'value' => '',
		'title' => 'Day',
	);
	for ( $i = 01; $i <= 31; $i ++ ) {
		$cat_options[] = array(
			'value' => "$i",
			'title' => "$i",
		);
	}

	$form['options'] = $cat_options;

	return $form;
}


/**
 * DOB year
 *
 * @param $owner_dob_year
 * @param $sc_params
 *
 * @return array
 */

function nano_entity_dob_year( $owner_dob_year, $sc_params ) {

	$form = array(
		'label'  => __( 'Year', 'nanosteam_8n' ),
		'value'  => $owner_dob_year,
		'name'   => 'owner_dob_year',
		'id'     => 'owner_dob_year',
		'type'   => 'select',
		'class'  => 'nan_check required' . ( verification_needed( $sc_params, 'legal_entity.dob.year', '' )[1] ),
		'wclass' => 'col-sm-4 ',
		'after'  => '</div>',
		'misc'   => 'required',
	);

	$cat_options   = array();
	$cat_options[] = array(
		'value' => '',
		'title' => 'Year',
	);
	for ( $i = ( nano_get_formated_date_time_gmt( 'Y' ) - 10 ); $i >= ( nano_get_formated_date_time_gmt( 'Y' ) - 150 ); $i -- ) {
		$cat_options[] = array(
			'value' => "$i",
			'title' => "$i",
		);
	}

	$form['options'] = $cat_options;

	return $form;
}

/**
 * State select form
 *
 * @param $biz_state
 * @param $sc_params
 *
 * @return array
 */

function nano_entity_state( $biz_state, $sc_params ) {

	$form = array(
		'label'  => __( 'State', 'nanosteam_8n' ),
		'value'  => $biz_state,
		'name'   => 'biz_state',
		'id'     => 'biz_state',
		'type'   => 'select',
		'class'  => 'required' . ( verification_needed( $sc_params, 'legal_entity.address.state', '' )[1] ),
		'wclass' => 'col-sm-4',
		'misc'   => 'required',
		'before' => '<div class="form-group form-row  pb-3">',
		'after'  => null,

	);

	$cat_options   = array();
	$cat_options[] = array( 'value' => '', 'title' => 'State...' );
	$cat_options[] = array( 'value' => 'AL', 'title' => 'Alabama' );
	$cat_options[] = array( 'value' => 'AK', 'title' => 'Alaska' );
	$cat_options[] = array( 'value' => 'AZ', 'title' => 'Arizona' );
	$cat_options[] = array( 'value' => 'AR', 'title' => 'Arkansas' );
	$cat_options[] = array( 'value' => 'CA', 'title' => 'California' );
	$cat_options[] = array( 'value' => 'CO', 'title' => 'Colorado' );
	$cat_options[] = array( 'value' => 'CT', 'title' => 'Connecticut' );
	$cat_options[] = array( 'value' => 'DE', 'title' => 'Delaware' );
	$cat_options[] = array( 'value' => 'DC', 'title' => 'District Of Columbia' );
	$cat_options[] = array( 'value' => 'FL', 'title' => 'Florida' );
	$cat_options[] = array( 'value' => 'GA', 'title' => 'Georgia' );
	$cat_options[] = array( 'value' => 'HI', 'title' => 'Hawaii' );
	$cat_options[] = array( 'value' => 'ID', 'title' => 'Idaho' );
	$cat_options[] = array( 'value' => 'IL', 'title' => 'Illinois' );
	$cat_options[] = array( 'value' => 'IN', 'title' => 'Indiana' );
	$cat_options[] = array( 'value' => 'IA', 'title' => 'Iowa' );
	$cat_options[] = array( 'value' => 'KS', 'title' => 'Kansas' );
	$cat_options[] = array( 'value' => 'KY', 'title' => 'Kentucky' );
	$cat_options[] = array( 'value' => 'LA', 'title' => 'Louisiana' );
	$cat_options[] = array( 'value' => 'ME', 'title' => 'Maine' );
	$cat_options[] = array( 'value' => 'MD', 'title' => 'Maryland' );
	$cat_options[] = array( 'value' => 'MA', 'title' => 'Massachusetts' );
	$cat_options[] = array( 'value' => 'MI', 'title' => 'Michigan' );
	$cat_options[] = array( 'value' => 'MN', 'title' => 'Minnesota' );
	$cat_options[] = array( 'value' => 'MS', 'title' => 'Mississippi' );
	$cat_options[] = array( 'value' => 'MO', 'title' => 'Missouri' );
	$cat_options[] = array( 'value' => 'MT', 'title' => 'Montana' );
	$cat_options[] = array( 'value' => 'NE', 'title' => 'Nebraska' );
	$cat_options[] = array( 'value' => 'NV', 'title' => 'Nevada' );
	$cat_options[] = array( 'value' => 'NH', 'title' => 'New Hampshire' );
	$cat_options[] = array( 'value' => 'NJ', 'title' => 'New Jersey' );
	$cat_options[] = array( 'value' => 'NM', 'title' => 'New Mexico' );
	$cat_options[] = array( 'value' => 'NY', 'title' => 'New York' );
	$cat_options[] = array( 'value' => 'NC', 'title' => 'North Carolina' );
	$cat_options[] = array( 'value' => 'ND', 'title' => 'North Dakota' );
	$cat_options[] = array( 'value' => 'OH', 'title' => 'Ohio' );
	$cat_options[] = array( 'value' => 'OK', 'title' => 'Oklahoma' );
	$cat_options[] = array( 'value' => 'OR', 'title' => 'Oregon' );
	$cat_options[] = array( 'value' => 'PA', 'title' => 'Pennsylvania' );
	$cat_options[] = array( 'value' => 'RI', 'title' => 'Rhode Island' );
	$cat_options[] = array( 'value' => 'SC', 'title' => 'South Carolina' );
	$cat_options[] = array( 'value' => 'SD', 'title' => 'South Dakota' );
	$cat_options[] = array( 'value' => 'TN', 'title' => 'Tennessee' );
	$cat_options[] = array( 'value' => 'TX', 'title' => 'Texas' );
	$cat_options[] = array( 'value' => 'UT', 'title' => 'Utah' );
	$cat_options[] = array( 'value' => 'VT', 'title' => 'Vermont' );
	$cat_options[] = array( 'value' => 'VA', 'title' => 'Virginia' );
	$cat_options[] = array( 'value' => 'WA', 'title' => 'Washington' );
	$cat_options[] = array( 'value' => 'WV', 'title' => 'West Virginia' );
	$cat_options[] = array( 'value' => 'WI', 'title' => 'Wisconsin' );
	$cat_options[] = array( 'value' => 'WY', 'title' => 'Wyoming' );

	$form['options'] = $cat_options;

	return $form;
}

