<?php if ( ! defined( 'ABSPATH' ) ) {
	die( 'Direct access forbidden.' );
}

$post_id = $_GET['post'];

// Status Checks
$first_transfer_processed_status = get_post_meta( $post_id, 'first_transfer_transaction_id', true ) != '' ? true : false;
$final_transfer_processed_status = get_post_meta( $post_id, 'final_transfer_transaction_id', true ) != '' ? true : false;
$standard_transfer               = get_post_meta( $post_id, 'standard_transfer_transaction_id', true ) != '' ? true : false;

$project_success_status = get_post_meta( $post_id, 'ign_project_success', true ) == '1' ? true : false;
$second_milestone_check = nano_second_milsetone_achieved( $post_id );
$nano_fifty_review      = get_post_meta( $post_id, 'fifty_fifty_grant', true );

//$level_id_01 = get_post_meta( $post_id, 'ign_project_id', true ) - 1;
$level_id = nano_get_level_id( get_post_meta( $post_id, 'ign_project_id', true ) );
$orders   = ID_Member_Order::get_orders_by_level( $level_id );

// FEES & TOTALS
$fees              = nano_fee_list( $orders, $post_id );
$total_fund_raised = $fees['total_before_fees'];
$stripe_fees       = $fees['stripe_fees'];
$nanosteam_fees    = $fees['nanosteam_fees'];
$total_fees        = $fees['total_fees'];
$total_after_fees  = $fees['total_after_fees'];



$amount        = number_format( 'approved' == $nano_fifty_review ? ( $total_fund_raised / 2 ) - $total_fees : $total_fund_raised - $total_fees, 2 );
$raw_amount    = number_format( 'approved' == $nano_fifty_review ? ( $total_fund_raised / 2 ) - $total_fees : $total_fund_raised - $total_fees, 2, '.', '' );
$amount_w_fees = $raw_amount + $total_fees;


$total_reserves  = get_option( 'nano_stripe_balance' );
$total_available = $total_reserves['available'];
$stripe_reserve  = 100 * get_option( 'nano_platform_stripe_amt' ); // This equals to stripe.

$reserve_check         = ( ( $total_available - $stripe_reserve ) / 100 ) > $total_after_fees ? true : false;
$reserve_check_message = false === $reserve_check ? '<div class="alert alert-info" style="color: red">You cannot initiate your payment transfer process as your Stripe account reserves are insufficient. We require a minimum of $' . ( $stripe_reserve / 100 ) . ' to be in the Platform\'s Account. To adjust this please go <a href="/wp-admin/admin.php?page=nanosteam-admin-options">here</a></div>' : '';
$alert                 = payout_problem_alert( $post_id );
$overdue_alert         = isset( $alert['message'] ) ? true : false;


// Final cross check to ensure the nanosteam admin debit account is not a stagin account or vise versa depending on where we are testing from.
$admin_debit_account             = get_option( 'nano_debit_account' );
$cross_check_admin_debit_account = admin_connect_account_update( $admin_debit_account, false );
$cross_checked_admin_debit       = '' === $cross_check_admin_debit_account ? true : false;

$nano_payout_button = array(
	array(
		'value'  => ( true == $first_transfer_processed_status || true == $standard_transfer ? __( 'Already Paid $', 'nanosteam_8n' ) : __( 'Transfer to creator for $', 'nanosteam_8n' ) ) . $amount,
		'name'   => 'first_payout_submit',
		'id'     => 'first_payout_submit',
		'type'   => 'submit',
		'class'  => 'first_payout_submit button button-primary button-large',
		'wclass' => 'form-row nano-btn btn-left',
		'misc'   => ( ( true == $first_transfer_processed_status || true == $standard_transfer || false === $reserve_check || false === $cross_checked_admin_debit ) ? ' disabled ' : '' ),
		'before' => '<p>' . ( false === $cross_checked_admin_debit ? '<span style="color:red">Nanosteam Admin DEBIT Account Problem: ' . $cross_check_admin_debit_account . '</span><br>' : '' ) . '
    Project Fund Total: $' . $total_fund_raised . '<br>
    Total Fees: $' . $total_fees . '<br> 
    Nanosteam Fee: $' . $nanosteam_fees . '<br>
    Stripe Fee : $' . $stripe_fees . '<br>
    ' . ( 'approved' == $nano_fifty_review ? 'First transfer amount: $ ' : 'Transfer: $' ) . $amount . '<br>
    </p>',
		'after'  => '<div id="first_nano_payout" class="hidden alert alert-info"></div><br>' . $reserve_check_message,
	),
	( true === $overdue_alert && isset( $alert['type'] ) && ( 'STANDARD' === $alert['type'] || 'FIRST' === $alert['type'] ) ? nano_overdue_check_field( $overdue_alert, $amount ) : array() ),
	( true === $overdue_alert && isset( $alert['type'] ) && ( 'STANDARD' === $alert['type'] || 'FIRST' === $alert['type'] ) ?  nano_overdue_check_button( $overdue_alert ) : array() ),
	final_payout( $first_transfer_processed_status, $final_transfer_processed_status, $nano_fifty_review, $amount, $total_fees, $second_milestone_check, $reserve_check, $reserve_check_message, $cross_checked_admin_debit, $overdue_alert ),
	( true === $overdue_alert && 'FINAL' === $alert['type'] ? nano_overdue_check_field( $overdue_alert,  $amount_w_fees ) : array() ),
	( true === $overdue_alert && 'FINAL' === $alert['type'] ? nano_overdue_check_button( $overdue_alert ) : array() ),
);

function final_payout( $first_transfer_processed_status, $final_transfer_processed_status, $nano_fifty_review, $amount, $total_fees, $second_milestone_check, $reserve_check, $reserve_check_message, $cross_checked_admin_debit, $overdue_alert ) {


	if ( 'approved' == $nano_fifty_review ) {

		$field = array(
			'value'  => ( true == $final_transfer_processed_status ? __( 'Already Paid', 'nanosteam_8n' ) . ' $' . ( $amount + $total_fees ) : ( true != $first_transfer_processed_status || true != $second_milestone_check ? 'Pending of $' . ( $amount + $total_fees ) : __( 'Transfer final amount for : $', 'nanosteam_8n' ) . ( $amount + $total_fees ) ) ),
			'name'   => 'final_payout_submit',
			'id'     => 'final_payout_submit',
			'type'   => 'submit',
			'class'  => 'final_payout_submit button button-primary button-large',
			'wclass' => 'form-row nano-btn btn-left',
			'misc'   => ( ( ( true != $final_transfer_processed_status && true == $second_milestone_check && true == $first_transfer_processed_status ) || false === $reserve_check || false === $cross_checked_admin_debit ) && false === $overdue_alert ? '' : ' disabled ' ),
			'before' => '<p>Final Matching Grant Payment.' . ( true != $second_milestone_check ? '<br>The creator has not reached their second milestone yet' : '' ) . '</p>',
			'after'  => '<div id="final_nano_payout" class="hidden alert alert-info"></div>' . $reserve_check_message,
		);

	} else {

		$field = array();

	}

	return $field;

}

function nano_overdue_check_field( $overdue, $amount ) {

	$field = array();

	if ( true === $overdue ) {

		$settings = get_option( 'memberdeck_gateways' );

		if ( ! empty( $settings ) ) {
			if ( is_array( $settings ) ) {
				$test = $settings['test'];
				if ( 1 == $test ) {
					$url = '/test';
				} else {
					$url = '';
				}
			}
		}

		// Admin Debit Account
		$stripe_withdrawl_account = get_option( 'nano_debit_account' );

		$creator_link = '<a href="https://dashboard.stripe.com/' . $stripe_withdrawl_account . $url . '/payouts" target="new">' . $stripe_withdrawl_account . '</a>';

		$field = array(
			'label'  => __( 'This is overdue. Please enter the event ID for this transation.', 'nanosteam_8n' ),
			'value'  => '',
			'name'   => 'over_due_field',
			'id'     => 'over_due_field',
			'type'   => 'text',
			'class'  => 'required',
			'wclass' => 'form-row mb-1',
			'misc'   => 'maxlength=500 placeholder="Event ID" required',
			'before' => '<p>This is overdue. Please goto: ' . $creator_link . ' and look for recent negative amount <strong>withdrawn</strong> within the last 7 days.</p>
<p>Click on it and you will see a summary listing. Look for "Retried Payouts". Cop the code that starts with tr_  and paste it into the search field. If you see a amount of $' . $amount . ' then scroll further to the events at the bottom.</p>
<p>Click the Event where it says "A payout of ($XXX.XX USD) should now appear on your bank account statement".</p>
<p>Copy the event ID in the Event Details at the top of the new page that opens and paste in the field below".</p>',
			'after'  => '<div id="nano_overdue_alert" class="hidden alert alert-info"></div><br>',

		);

	}

	return $field;
}

function nano_overdue_check_button( $overdue ) {

	$field = array();

	if ( true === $overdue ) {

		$field = array(
			'value'  => __( 'Recheck Amount', 'nanosteam_8n' ),
			'name'   => 'over_due_submit',
			'id'     => 'over_due_submit',
			'type'   => 'submit',
			'class'  => 'over_due_submit button button-primary button-large',
			'wclass' => 'form-row nano-btn btn-left',
			'misc'   => '',
			'before' => '',
			'after'  => '',
		);

	}

	return $field;

}

$form_payout = new NANO_Form( $nano_payout_button );

echo apply_filters( 'nano_payout', $form_payout->build_form() );


