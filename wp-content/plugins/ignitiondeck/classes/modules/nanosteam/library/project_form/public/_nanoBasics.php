<?php if ( ! defined( 'ABSPATH' ) ) {
	die( 'Direct access forbidden.' );
}

// When a project has been submitted, check to see if user is the team leader or not
if ( ( isset( $_GET['edit_project'] ) && self::team_lead_only() == true ) || isset( $_GET['create_project'] ) ) {

	$form[] = array( 'before' => ( isset( $_GET['create_project'] ) ? '<p>' . __( 'Get more attention to your project with an interesting title, compelling image, and other information that engages prospective backers.', 'nanosteam_8n' ) . '</p>' : '' ) );
	$form[] = id_fes_project_name_field( $vars );
	$form[] = id_fes_featured_image_field( $vars );
	$form[] = nano_featured_image_hidden( $vars );
	$form[] = id_fes_featured_image_check_field( $vars );
	$form[] = id_fes_video_field( $vars );
	$form[] = nano_cat_form( $vars, 0 );
	$form[] = nano_cat_form( $vars, 1 );
	$form[] = nano_location_city_aj( $vars );
	$form[] = nano_location_city( $vars );
	$form[] = nano_location_state( $vars );

} // Hide all content from users who are not the team lead except for labnotes.

// BASICS
function id_fes_project_name_field( $vars ) {

	$field = array(
		'label'       => __( 'Project Title', 'nanosteam_8n' ),
		'value'       => ( isset( $vars['project_name'] ) ? $vars['project_name'] : '' ),
		'name'        => 'project_name',
		'id'          => 'project_name',
		'type'        => 'text',
		'class'       => 'required',
		'wclass'      => 'col',
		'misc'        => 'maxlength=140 placeholder="' . __( 'My sample fabulous Nanosteam Project Title goes here.', 'nanosteam_8n' ) . '" required',
		'heading'     => __( 'Title', 'nanosteam_8n' ),
		'help'        => nano_help_field( 'project_title' ),
		'description' => nano_description_field( 'project_title' ),
		'before'      => '<div class="form-group form-row pb-3">',
		'after'       => '</div>',

	);

	return $field;
}

function id_fes_featured_image_field( $vars ) {

	$post_id       = isset( $vars['post_id'] ) ? $vars['post_id'] : '';
	$image_id      = get_post_meta( $post_id, 'project_main', true ) != '' ? get_post_meta( $post_id, 'project_main', true ) : get_post_thumbnail_id( $post_id );
	$thumbnail_url = '' != $image_id ? wp_get_attachment_image_src( $image_id, 'nano-crop-16-9-sm', true )[0] : '';

	if ( 'PUBLISH' !== strtoupper( get_post_status( $post_id ) ) || '' == $post_id ) {

		$field = array(
			'label'   => __( 'Featured Image', 'nanosteam_8n' ),
			'value'   => $thumbnail_url,
			'misc'    => ( '' != $image_id ? 'data-url="' . $thumbnail_url . '" accept="image/*"' : 'accept="image/*"' ) . ' required',
			'name'    => 'project_main',
			'id'      => 'project_main',
			'type'    => 'file',
			'class'   => 'required nano-image-file',
			'wclass'  => 'col-sm',
			'heading' => __( 'Project Image or Video', 'nanosteam_8n' ),
			'help'    => nano_help_field( 'project_main' ),
			'before'  => '<div class="nano-project-image nano-upload form-group form-row pb-3">' . wp_nonce_field( 'nano_image_secure', 'nano_image_security' ),
		);

	}

	if ( 'PUBLISH' == strtoupper( get_post_status( $post_id ) ) && '' !== $post_id  ) {

		$img_obj = nano_project_image( $post_id, 'nano-crop-16-9-xs' );

		$field = array(
			'label'   => __( 'Featured Image', 'nanosteam_8n' ),
			'value'   => '',
			'misc'    => '',
			'name'    => '',
			'id'      => '',
			'type'    => '',
			'class'   => '',
			'wclass'  => '',
			'heading' => __( 'Project Image or Video', 'nanosteam_8n' ),
			'help'    => nano_help_field( 'project_main' ),
			'before'  => '<div class="nano-project-image nano-upload form-group form-row pb-3"><div class="col-sm-6">' . $img_obj . '</div>' .  wp_nonce_field( 'nano_image_secure', 'nano_image_security' ),
		);

	}

	return $field;
}

function nano_featured_image_hidden( $vars ) {

	$post_id       = isset( $vars['post_id'] ) ? $vars['post_id'] : '';
	$image_id      = get_post_meta( $post_id, 'project_main', true ) != '' ? get_post_meta( $post_id, 'project_main', true ) : get_post_thumbnail_id( $post_id );
	$thumbnail_url = '' != $image_id ? wp_get_attachment_image_src( $image_id, 'nano-crop-16-9-sm', true )[0] : '';

	$field = array(
		'label'  => __( 'Featured Image', 'nanosteam_8n' ),
		'value'  => $thumbnail_url,
		'misc'   => ( '' != $image_id ? 'data-url="' . $thumbnail_url . '" accept="image/*"' : 'accept="image/*"' ) . ' required',
		'name'   => 'nano_featured_image_hidden',
		'id'     => 'nano_featured_image_hidden',
		'type'   => 'hidden',
		'class'  => 'nano_featured_image_hidden',
		'wclass' => 'd-none',
	);

	return $field;
}

function id_fes_featured_image_check_field( $vars ) {

	$field = array(
		'value'  => 'no',
		'name'   => 'project_main_removed',
		'id'     => 'project_main_removed',
		'wclass' => 'd-none',
		'type'   => 'hidden',
	);

	return $field;
}

function id_fes_video_field( $vars ) {

	$post_id  = isset( $vars['post_id'] ) ? $vars['post_id'] : '';
	$image_id = '' != get_post_meta( $post_id, 'project_main', true ) ? get_post_meta( $post_id, 'project_main', true ) : get_post_thumbnail_id( $post_id );
	$after = '<div class="w-100"></div><div id="count" class="d-none">2</div>
                    <div class="hidden_crop"></div>
                    <div class="pb-3 pt-4 col-sm-6 mb-4 croppie-container">' . ( '' == $image_id ? '
                    <div class="upload-msg px-5 text-secondary project_img"><span>' . __( 'Select your project photo', 'nanosteam_8n' ) . '</span></div>' : '' ) . '
                    <div class="nano-upload-wrap"><div class="upload-container"></div>
                    <div class="d-flex justify-content-between">
                    <button class="vanilla-rotate btn btn-sm btn-primary px-2 py-0" data-deg="-90">Rotate Left</button>
                    <button class="vanilla-rotate btn btn-sm btn-primary px-2 py-0" data-deg="90">Rotate Right</button>
                    </div>
                    </div></div></div>';
	$read_only = '';

	if ( 'PUBLISH' == strtoupper( get_post_status( $post_id ) ) && '' !== $post_id ) {
		$after = '</div>';
		$read_only = ' readonly';
	}

	$field = array(
		'label'  => __( 'Project Video', 'nanosteam_8n' ),
		'value'  => ( isset( $vars['project_video'] ) ? $vars['project_video'] : '' ),
		'name'   => 'project_video',
		'id'     => 'project_video',
		'type'   => 'text',
		'wclass' => 'col-sm video_link',
		'misc'   => 'placeholder="Youtube or Vimeo link here"' . $read_only,
		'after'  => $after,
	);

	return $field;
}

function nano_cat_form( $vars, $key ) {

	$vars = isset( $vars['post_id'] ) ? get_post_meta( $vars['post_id'] ) : '';

	$args          = array(
		'type'       => 'ignition_product',
		'taxonomy'   => 'project_category',
		'hide_empty' => false,
	);
	$categories    = get_categories( $args );
	$cat_form      = array(
		'label'       => __( 'Project Categories', 'nanosteam_8n' ),
		'value'       => ( isset( $vars['project_category'][ $key ] ) ? $vars['project_category'][ $key ] : '' ),
		'name'        => 'project_category_' . $key,
		'id'          => 'project_category_' . $key,
		'type'        => 'select',
		'class'       => 'required catterm',
		'wclass'      => 'col-sm',
		'misc'        => 'required',
		'heading'     => ( 0 == $key ? __( 'Select Your Categories', 'nanosteam_8n' ) : null ),
		'help'        => ( 0 == $key ? nano_help_field( 'select_your_categories' ) : null ),
		'description' => ( 0 == $key ? nano_description_field( 'select_your_categories' ) : null ),
		'before'      => ( 0 == $key ? '<div class="form-group form-row pb-3">' : null ),
		'after'       => ( 1 == $key ? '</div>' : null ),
	);
	$cat_options   = array();
	$cat_options[] = array(
		'value' => '',
		'title' => 'Category',
		'misc'  => '',
	);
	foreach ( $categories as $category ) {
		$selected      = isset( $vars['project_category'][ $key ] ) && $vars['project_category'][ $key ] == $category->slug ? 'selected' : '';
		$cat_options[] = array(
			'value' => $category->slug,
			'title' => $category->name,
			'misc'  => $selected,
		);
	}
	$cat_form['options'] = $cat_options;

	return $cat_form;
}

function nano_location_city_aj( $vars ) {

	$vars = isset( $vars['post_id'] ) ? get_post_meta( $vars['post_id'] ) : '';

	$field = array(
		'label'       => __( 'City', 'nanosteam_8n' ),
		'value'       => ( isset( $vars['nano_location_city'][0] ) ? $vars['nano_location_city'][0] : '' ) . ( isset( $vars['nano_location_state'][0] ) && '' != $vars['nano_location_state'][0] ? ', ' . $vars['nano_location_state'][0] : '' ),
		'name'        => 'nano_location_city_aj',
		'id'          => 'nano_location_city_aj',
		'type'        => 'text',
		'class'       => 'required',
		'misc'        => 'maxlength=140 placeholder="City, State" required',
		'wclass'      => 'col-sm-12',
		'description' => 'Enter the first three letters of your city.',
		'description' => nano_description_field( 'city' ),
		'heading'     => __( 'Location', 'nanosteam_8n' ),
		'help'        => nano_help_field( 'city' ),
		'before'      => '<div class="form-group form-row pb-3">',
	);

	return $field;
}

function nano_location_city( $vars ) {

	$vars = isset( $vars['post_id'] ) ? get_post_meta( $vars['post_id'] ) : '';

	$field = array(
		'label'  => __( 'City', 'nanosteam_8n' ),
		'value'  => ( isset( $vars['nano_location_city'][0] ) ? $vars['nano_location_city'][0] : '' ),
		'name'   => 'nano_location_city',
		'id'     => 'nano_location_city',
		'type'   => 'hidden',
		'class'  => 'required',
		'misc'   => 'maxlength=140 placeholder="City" required',
		'wclass' => 'col-sm',
	);

	return $field;
}

function nano_location_state( $vars ) {

	$vars = isset( $vars['post_id'] ) ? get_post_meta( $vars['post_id'] ) : '';

	$form = array(
		'label'  => __( 'Country', 'nanosteam_8n' ),
		'value'  => ( isset( $vars['nano_location_state'][0] ) ? $vars['nano_location_state'][0] : '' ),
		'name'   => 'nano_location_state',
		'id'     => 'nano_location_state',
		'type'   => 'hidden',
		'class'  => 'state_select ' . ( isset( $vars['nano_location_state'][0] ) ? '' : 'fade' ),
		'misc'   => 'required',
		'wclass' => 'col-sm',
		'after'  => '</div>',
	);

	$cat_options   = array();
	$cat_options[] = array( 'value' => '', 'title' => 'State...' );
	$cat_options[] = array( 'value' => 'AL', 'title' => 'Alabama' );
	$cat_options[] = array( 'value' => 'AK', 'title' => 'Alaska' );
	$cat_options[] = array( 'value' => 'AZ', 'title' => 'Arizona' );
	$cat_options[] = array( 'value' => 'AR', 'title' => 'Arkansas' );
	$cat_options[] = array( 'value' => 'CA', 'title' => 'California' );
	$cat_options[] = array( 'value' => 'CO', 'title' => 'Colorado' );
	$cat_options[] = array( 'value' => 'CT', 'title' => 'Connecticut' );
	$cat_options[] = array( 'value' => 'DE', 'title' => 'Delaware' );
	$cat_options[] = array( 'value' => 'DC', 'title' => 'District Of Columbia' );
	$cat_options[] = array( 'value' => 'FL', 'title' => 'Florida' );
	$cat_options[] = array( 'value' => 'GA', 'title' => 'Georgia' );
	$cat_options[] = array( 'value' => 'HI', 'title' => 'Hawaii' );
	$cat_options[] = array( 'value' => 'ID', 'title' => 'Idaho' );
	$cat_options[] = array( 'value' => 'IL', 'title' => 'Illinois' );
	$cat_options[] = array( 'value' => 'IN', 'title' => 'Indiana' );
	$cat_options[] = array( 'value' => 'IA', 'title' => 'Iowa' );
	$cat_options[] = array( 'value' => 'KS', 'title' => 'Kansas' );
	$cat_options[] = array( 'value' => 'KY', 'title' => 'Kentucky' );
	$cat_options[] = array( 'value' => 'LA', 'title' => 'Louisiana' );
	$cat_options[] = array( 'value' => 'ME', 'title' => 'Maine' );
	$cat_options[] = array( 'value' => 'MD', 'title' => 'Maryland' );
	$cat_options[] = array( 'value' => 'MA', 'title' => 'Massachusetts' );
	$cat_options[] = array( 'value' => 'MI', 'title' => 'Michigan' );
	$cat_options[] = array( 'value' => 'MN', 'title' => 'Minnesota' );
	$cat_options[] = array( 'value' => 'MS', 'title' => 'Mississippi' );
	$cat_options[] = array( 'value' => 'MO', 'title' => 'Missouri' );
	$cat_options[] = array( 'value' => 'MT', 'title' => 'Montana' );
	$cat_options[] = array( 'value' => 'NE', 'title' => 'Nebraska' );
	$cat_options[] = array( 'value' => 'NV', 'title' => 'Nevada' );
	$cat_options[] = array( 'value' => 'NH', 'title' => 'New Hampshire' );
	$cat_options[] = array( 'value' => 'NJ', 'title' => 'New Jersey' );
	$cat_options[] = array( 'value' => 'NM', 'title' => 'New Mexico' );
	$cat_options[] = array( 'value' => 'NY', 'title' => 'New York' );
	$cat_options[] = array( 'value' => 'NC', 'title' => 'North Carolina' );
	$cat_options[] = array( 'value' => 'ND', 'title' => 'North Dakota' );
	$cat_options[] = array( 'value' => 'OH', 'title' => 'Ohio' );
	$cat_options[] = array( 'value' => 'OK', 'title' => 'Oklahoma' );
	$cat_options[] = array( 'value' => 'OR', 'title' => 'Oregon' );
	$cat_options[] = array( 'value' => 'PA', 'title' => 'Pennsylvania' );
	$cat_options[] = array( 'value' => 'RI', 'title' => 'Rhode Island' );
	$cat_options[] = array( 'value' => 'SC', 'title' => 'South Carolina' );
	$cat_options[] = array( 'value' => 'SD', 'title' => 'South Dakota' );
	$cat_options[] = array( 'value' => 'TN', 'title' => 'Tennessee' );
	$cat_options[] = array( 'value' => 'TX', 'title' => 'Texas' );
	$cat_options[] = array( 'value' => 'UT', 'title' => 'Utah' );
	$cat_options[] = array( 'value' => 'VT', 'title' => 'Vermont' );
	$cat_options[] = array( 'value' => 'VA', 'title' => 'Virginia' );
	$cat_options[] = array( 'value' => 'WA', 'title' => 'Washington' );
	$cat_options[] = array( 'value' => 'WV', 'title' => 'West Virginia' );
	$cat_options[] = array( 'value' => 'WI', 'title' => 'Wisconsin' );
	$cat_options[] = array( 'value' => 'WY', 'title' => 'Wyoming' );

	$form['options'] = $cat_options;

	return $form;
}

/*
function nano_location_country( $vars ) {

	$vars = isset( $vars['post_id'] ) ? get_post_meta( $vars['post_id'] ) : '';

	$form          = array(
		'label'  => __( 'Country', 'nanosteam_8n' ),
		'value'  => ( isset( $vars['nano_location_country'][0] ) ? $vars['nano_location_country'][0] : '' ),
		'name'   => 'nano_location_country',
		'id'     => 'nano_location_country',
		'type'   => 'select',
		'class'  => 'country_select',
		'misc'   => 'required',
		'wclass' => 'col-sm',
	);
	$cat_options   = array();
	$cat_options[] = array( 'value' => '', 'title' => 'Country...' );
	$cat_options[] = array( 'value' => 'United States of America', 'title' => 'United States of America' );
	$cat_options[] = array( 'value' => 'Afganistan', 'title' => 'Afghanistan' );
	$cat_options[] = array( 'value' => 'Albania', 'title' => 'Albania' );
	$cat_options[] = array( 'value' => 'Algeria', 'title' => 'Algeria' );
	$cat_options[] = array( 'value' => 'American Samoa', 'title' => 'American Samoa' );
	$cat_options[] = array( 'value' => 'Andorra', 'title' => 'Andorra' );
	$cat_options[] = array( 'value' => 'Angola', 'title' => 'Angola' );
	$cat_options[] = array( 'value' => 'Anguilla', 'title' => 'Anguilla' );
	$cat_options[] = array( 'value' => 'Antigua &amp; Barbuda', 'title' => 'Antigua &amp; Barbuda' );
	$cat_options[] = array( 'value' => 'Argentina', 'title' => 'Argentina' );
	$cat_options[] = array( 'value' => 'Armenia', 'title' => 'Armenia' );
	$cat_options[] = array( 'value' => 'Aruba', 'title' => 'Aruba' );
	$cat_options[] = array( 'value' => 'Australia', 'title' => 'Australia' );
	$cat_options[] = array( 'value' => 'Austria', 'title' => 'Austria' );
	$cat_options[] = array( 'value' => 'Azerbaijan', 'title' => 'Azerbaijan' );
	$cat_options[] = array( 'value' => 'Bahamas', 'title' => 'Bahamas' );
	$cat_options[] = array( 'value' => 'Bahrain', 'title' => 'Bahrain' );
	$cat_options[] = array( 'value' => 'Bangladesh', 'title' => 'Bangladesh' );
	$cat_options[] = array( 'value' => 'Barbados', 'title' => 'Barbados' );
	$cat_options[] = array( 'value' => 'Belarus', 'title' => 'Belarus' );
	$cat_options[] = array( 'value' => 'Belgium', 'title' => 'Belgium' );
	$cat_options[] = array( 'value' => 'Belize', 'title' => 'Belize' );
	$cat_options[] = array( 'value' => 'Benin', 'title' => 'Benin' );
	$cat_options[] = array( 'value' => 'Bermuda', 'title' => 'Bermuda' );
	$cat_options[] = array( 'value' => 'Bhutan', 'title' => 'Bhutan' );
	$cat_options[] = array( 'value' => 'Bolivia', 'title' => 'Bolivia' );
	$cat_options[] = array( 'value' => 'Bonaire', 'title' => 'Bonaire' );
	$cat_options[] = array( 'value' => 'Bosnia &amp; Herzegovina', 'title' => 'Bosnia &amp; Herzegovina' );
	$cat_options[] = array( 'value' => 'Botswana', 'title' => 'Botswana' );
	$cat_options[] = array( 'value' => 'Brazil', 'title' => 'Brazil' );
	$cat_options[] = array( 'value' => 'British Indian Ocean Ter', 'title' => 'British Indian Ocean Ter' );
	$cat_options[] = array( 'value' => 'Brunei', 'title' => 'Brunei' );
	$cat_options[] = array( 'value' => 'Bulgaria', 'title' => 'Bulgaria' );
	$cat_options[] = array( 'value' => 'Burkina Faso', 'title' => 'Burkina Faso' );
	$cat_options[] = array( 'value' => 'Burundi', 'title' => 'Burundi' );
	$cat_options[] = array( 'value' => 'Cambodia', 'title' => 'Cambodia' );
	$cat_options[] = array( 'value' => 'Cameroon', 'title' => 'Cameroon' );
	$cat_options[] = array( 'value' => 'Canada', 'title' => 'Canada' );
	$cat_options[] = array( 'value' => 'Canary Islands', 'title' => 'Canary Islands' );
	$cat_options[] = array( 'value' => 'Cape Verde', 'title' => 'Cape Verde' );
	$cat_options[] = array( 'value' => 'Cayman Islands', 'title' => 'Cayman Islands' );
	$cat_options[] = array( 'value' => 'Central African Republic', 'title' => 'Central African Republic' );
	$cat_options[] = array( 'value' => 'Chad', 'title' => 'Chad' );
	$cat_options[] = array( 'value' => 'Channel Islands', 'title' => 'Channel Islands' );
	$cat_options[] = array( 'value' => 'Chile', 'title' => 'Chile' );
	$cat_options[] = array( 'value' => 'China', 'title' => 'China' );
	$cat_options[] = array( 'value' => 'Christmas Island', 'title' => 'Christmas Island' );
	$cat_options[] = array( 'value' => 'Cocos Island', 'title' => 'Cocos Island' );
	$cat_options[] = array( 'value' => 'Colombia', 'title' => 'Colombia' );
	$cat_options[] = array( 'value' => 'Comoros', 'title' => 'Comoros' );
	$cat_options[] = array( 'value' => 'Congo', 'title' => 'Congo' );
	$cat_options[] = array( 'value' => 'Cook Islands', 'title' => 'Cook Islands' );
	$cat_options[] = array( 'value' => 'Costa Rica', 'title' => 'Costa Rica' );
	$cat_options[] = array( 'value' => 'Cote DIvoire', 'title' => "Cote D'Ivoire" );
	$cat_options[] = array( 'value' => 'Croatia', 'title' => 'Croatia' );
	$cat_options[] = array( 'value' => 'Cuba', 'title' => 'Cuba' );
	$cat_options[] = array( 'value' => 'Curaco', 'title' => 'Curacao' );
	$cat_options[] = array( 'value' => 'Cyprus', 'title' => 'Cyprus' );
	$cat_options[] = array( 'value' => 'Czech Republic', 'title' => 'Czech Republic' );
	$cat_options[] = array( 'value' => 'Denmark', 'title' => 'Denmark' );
	$cat_options[] = array( 'value' => 'Djibouti', 'title' => 'Djibouti' );
	$cat_options[] = array( 'value' => 'Dominica', 'title' => 'Dominica' );
	$cat_options[] = array( 'value' => 'Dominican Republic', 'title' => 'Dominican Republic' );
	$cat_options[] = array( 'value' => 'East Timor', 'title' => 'East Timor' );
	$cat_options[] = array( 'value' => 'Ecuador', 'title' => 'Ecuador' );
	$cat_options[] = array( 'value' => 'Egypt', 'title' => 'Egypt' );
	$cat_options[] = array( 'value' => 'El Salvador', 'title' => 'El Salvador' );
	$cat_options[] = array( 'value' => 'Equatorial Guinea', 'title' => 'Equatorial Guinea' );
	$cat_options[] = array( 'value' => 'Eritrea', 'title' => 'Eritrea' );
	$cat_options[] = array( 'value' => 'Estonia', 'title' => 'Estonia' );
	$cat_options[] = array( 'value' => 'Ethiopia', 'title' => 'Ethiopia' );
	$cat_options[] = array( 'value' => 'Falkland Islands', 'title' => 'Falkland Islands' );
	$cat_options[] = array( 'value' => 'Faroe Islands', 'title' => 'Faroe Islands' );
	$cat_options[] = array( 'value' => 'Fiji', 'title' => 'Fiji' );
	$cat_options[] = array( 'value' => 'Finland', 'title' => 'Finland' );
	$cat_options[] = array( 'value' => 'France', 'title' => 'France' );
	$cat_options[] = array( 'value' => 'French Guiana', 'title' => 'French Guiana' );
	$cat_options[] = array( 'value' => 'French Polynesia', 'title' => 'French Polynesia' );
	$cat_options[] = array( 'value' => 'French Southern Ter', 'title' => 'French Southern Ter' );
	$cat_options[] = array( 'value' => 'Gabon', 'title' => 'Gabon' );
	$cat_options[] = array( 'value' => 'Gambia', 'title' => 'Gambia' );
	$cat_options[] = array( 'value' => 'Georgia', 'title' => 'Georgia' );
	$cat_options[] = array( 'value' => 'Germany', 'title' => 'Germany' );
	$cat_options[] = array( 'value' => 'Ghana', 'title' => 'Ghana' );
	$cat_options[] = array( 'value' => 'Gibraltar', 'title' => 'Gibraltar' );
	$cat_options[] = array( 'value' => 'Great Britain', 'title' => 'Great Britain' );
	$cat_options[] = array( 'value' => 'Greece', 'title' => 'Greece' );
	$cat_options[] = array( 'value' => 'Greenland', 'title' => 'Greenland' );
	$cat_options[] = array( 'value' => 'Grenada', 'title' => 'Grenada' );
	$cat_options[] = array( 'value' => 'Guadeloupe', 'title' => 'Guadeloupe' );
	$cat_options[] = array( 'value' => 'Guam', 'title' => 'Guam' );
	$cat_options[] = array( 'value' => 'Guatemala', 'title' => 'Guatemala' );
	$cat_options[] = array( 'value' => 'Guinea', 'title' => 'Guinea' );
	$cat_options[] = array( 'value' => 'Guyana', 'title' => 'Guyana' );
	$cat_options[] = array( 'value' => 'Haiti', 'title' => 'Haiti' );
	$cat_options[] = array( 'value' => 'Hawaii', 'title' => 'Hawaii' );
	$cat_options[] = array( 'value' => 'Honduras', 'title' => 'Honduras' );
	$cat_options[] = array( 'value' => 'Hong Kong', 'title' => 'Hong Kong' );
	$cat_options[] = array( 'value' => 'Hungary', 'title' => 'Hungary' );
	$cat_options[] = array( 'value' => 'Iceland', 'title' => 'Iceland' );
	$cat_options[] = array( 'value' => 'India', 'title' => 'India' );
	$cat_options[] = array( 'value' => 'Indonesia', 'title' => 'Indonesia' );
	$cat_options[] = array( 'value' => 'Iran', 'title' => 'Iran' );
	$cat_options[] = array( 'value' => 'Iraq', 'title' => 'Iraq' );
	$cat_options[] = array( 'value' => 'Ireland', 'title' => 'Ireland' );
	$cat_options[] = array( 'value' => 'Isle of Man', 'title' => 'Isle of Man' );
	$cat_options[] = array( 'value' => 'Israel', 'title' => 'Israel' );
	$cat_options[] = array( 'value' => 'Italy', 'title' => 'Italy' );
	$cat_options[] = array( 'value' => 'Jamaica', 'title' => 'Jamaica' );
	$cat_options[] = array( 'value' => 'Japan', 'title' => 'Japan' );
	$cat_options[] = array( 'value' => 'Jordan', 'title' => 'Jordan' );
	$cat_options[] = array( 'value' => 'Kazakhstan', 'title' => 'Kazakhstan' );
	$cat_options[] = array( 'value' => 'Kenya', 'title' => 'Kenya' );
	$cat_options[] = array( 'value' => 'Kiribati', 'title' => 'Kiribati' );
	$cat_options[] = array( 'value' => 'Korea North', 'title' => 'Korea North' );
	$cat_options[] = array( 'value' => 'Korea Sout', 'title' => 'Korea South' );
	$cat_options[] = array( 'value' => 'Kuwait', 'title' => 'Kuwait' );
	$cat_options[] = array( 'value' => 'Kyrgyzstan', 'title' => 'Kyrgyzstan' );
	$cat_options[] = array( 'value' => 'Laos', 'title' => 'Laos' );
	$cat_options[] = array( 'value' => 'Latvia', 'title' => 'Latvia' );
	$cat_options[] = array( 'value' => 'Lebanon', 'title' => 'Lebanon' );
	$cat_options[] = array( 'value' => 'Lesotho', 'title' => 'Lesotho' );
	$cat_options[] = array( 'value' => 'Liberia', 'title' => 'Liberia' );
	$cat_options[] = array( 'value' => 'Libya', 'title' => 'Libya' );
	$cat_options[] = array( 'value' => 'Liechtenstein', 'title' => 'Liechtenstein' );
	$cat_options[] = array( 'value' => 'Lithuania', 'title' => 'Lithuania' );
	$cat_options[] = array( 'value' => 'Luxembourg', 'title' => 'Luxembourg' );
	$cat_options[] = array( 'value' => 'Macau', 'title' => 'Macau' );
	$cat_options[] = array( 'value' => 'Macedonia', 'title' => 'Macedonia' );
	$cat_options[] = array( 'value' => 'Madagascar', 'title' => 'Madagascar' );
	$cat_options[] = array( 'value' => 'Malaysia', 'title' => 'Malaysia' );
	$cat_options[] = array( 'value' => 'Malawi', 'title' => 'Malawi' );
	$cat_options[] = array( 'value' => 'Maldives', 'title' => 'Maldives' );
	$cat_options[] = array( 'value' => 'Mali', 'title' => 'Mali' );
	$cat_options[] = array( 'value' => 'Malta', 'title' => 'Malta' );
	$cat_options[] = array( 'value' => 'Marshall Islands', 'title' => 'Marshall Islands' );
	$cat_options[] = array( 'value' => 'Martinique', 'title' => 'Martinique' );
	$cat_options[] = array( 'value' => 'Mauritania', 'title' => 'Mauritania' );
	$cat_options[] = array( 'value' => 'Mauritius', 'title' => 'Mauritius' );
	$cat_options[] = array( 'value' => 'Mayotte', 'title' => 'Mayotte' );
	$cat_options[] = array( 'value' => 'Mexico', 'title' => 'Mexico' );
	$cat_options[] = array( 'value' => 'Midway Islands', 'title' => 'Midway Islands' );
	$cat_options[] = array( 'value' => 'Moldova', 'title' => 'Moldova' );
	$cat_options[] = array( 'value' => 'Monaco', 'title' => 'Monaco' );
	$cat_options[] = array( 'value' => 'Mongolia', 'title' => 'Mongolia' );
	$cat_options[] = array( 'value' => 'Montserrat', 'title' => 'Montserrat' );
	$cat_options[] = array( 'value' => 'Morocco', 'title' => 'Morocco' );
	$cat_options[] = array( 'value' => 'Mozambique', 'title' => 'Mozambique' );
	$cat_options[] = array( 'value' => 'Myanmar', 'title' => 'Myanmar' );
	$cat_options[] = array( 'value' => 'Nambia', 'title' => 'Nambia' );
	$cat_options[] = array( 'value' => 'Nauru', 'title' => 'Nauru' );
	$cat_options[] = array( 'value' => 'Nepal', 'title' => 'Nepal' );
	$cat_options[] = array( 'value' => 'Netherland Antilles', 'title' => 'Netherland Antilles' );
	$cat_options[] = array( 'value' => 'Netherlands', 'title' => 'Netherlands (Holland, Europe)' );
	$cat_options[] = array( 'value' => 'Nevis', 'title' => 'Nevis' );
	$cat_options[] = array( 'value' => 'New Caledonia', 'title' => 'New Caledonia' );
	$cat_options[] = array( 'value' => 'New Zealand', 'title' => 'New Zealand' );
	$cat_options[] = array( 'value' => 'Nicaragua', 'title' => 'Nicaragua' );
	$cat_options[] = array( 'value' => 'Niger', 'title' => 'Niger' );
	$cat_options[] = array( 'value' => 'Nigeria', 'title' => 'Nigeria' );
	$cat_options[] = array( 'value' => 'Niue', 'title' => 'Niue' );
	$cat_options[] = array( 'value' => 'Norfolk Island', 'title' => 'Norfolk Island' );
	$cat_options[] = array( 'value' => 'Norway', 'title' => 'Norway' );
	$cat_options[] = array( 'value' => 'Oman', 'title' => 'Oman' );
	$cat_options[] = array( 'value' => 'Pakistan', 'title' => 'Pakistan' );
	$cat_options[] = array( 'value' => 'Palau Island', 'title' => 'Palau Island' );
	$cat_options[] = array( 'value' => 'Palestine', 'title' => 'Palestine' );
	$cat_options[] = array( 'value' => 'Panama', 'title' => 'Panama' );
	$cat_options[] = array( 'value' => 'Papua New Guinea', 'title' => 'Papua New Guinea' );
	$cat_options[] = array( 'value' => 'Paraguay', 'title' => 'Paraguay' );
	$cat_options[] = array( 'value' => 'Peru', 'title' => 'Peru' );
	$cat_options[] = array( 'value' => 'Phillipines', 'title' => 'Philippines' );
	$cat_options[] = array( 'value' => 'Pitcairn Island', 'title' => 'Pitcairn Island' );
	$cat_options[] = array( 'value' => 'Poland', 'title' => 'Poland' );
	$cat_options[] = array( 'value' => 'Portugal', 'title' => 'Portugal' );
	$cat_options[] = array( 'value' => 'Puerto Rico', 'title' => 'Puerto Rico' );
	$cat_options[] = array( 'value' => 'Qatar', 'title' => 'Qatar' );
	$cat_options[] = array( 'value' => 'Republic of Montenegro', 'title' => 'Republic of Montenegro' );
	$cat_options[] = array( 'value' => 'Republic of Serbia', 'title' => 'Republic of Serbia' );
	$cat_options[] = array( 'value' => 'Reunion', 'title' => 'Reunion' );
	$cat_options[] = array( 'value' => 'Romania', 'title' => 'Romania' );
	$cat_options[] = array( 'value' => 'Russia', 'title' => 'Russia' );
	$cat_options[] = array( 'value' => 'Rwanda', 'title' => 'Rwanda' );
	$cat_options[] = array( 'value' => 'St Barthelemy', 'title' => 'St Barthelemy' );
	$cat_options[] = array( 'value' => 'St Eustatius', 'title' => 'St Eustatius' );
	$cat_options[] = array( 'value' => 'St Helena', 'title' => 'St Helena' );
	$cat_options[] = array( 'value' => 'St Kitts-Nevis', 'title' => 'St Kitts-Nevis' );
	$cat_options[] = array( 'value' => 'St Lucia', 'title' => 'St Lucia' );
	$cat_options[] = array( 'value' => 'St Maarten', 'title' => 'St Maarten' );
	$cat_options[] = array( 'value' => 'St Pierre &amp; Miquelon', 'title' => 'St Pierre &amp; Miquelon' );
	$cat_options[] = array( 'value' => 'St Vincent &amp; Grenadines', 'title' => 'St Vincent &amp; Grenadines' );
	$cat_options[] = array( 'value' => 'Saipan', 'title' => 'Saipan' );
	$cat_options[] = array( 'value' => 'Samoa', 'title' => 'Samoa' );
	$cat_options[] = array( 'value' => 'Samoa American', 'title' => 'Samoa American' );
	$cat_options[] = array( 'value' => 'San Marino', 'title' => 'San Marino' );
	$cat_options[] = array( 'value' => 'Sao Tome &amp; Principe', 'title' => 'Sao Tome &amp; Principe' );
	$cat_options[] = array( 'value' => 'Saudi Arabia', 'title' => 'Saudi Arabia' );
	$cat_options[] = array( 'value' => 'Senegal', 'title' => 'Senegal' );
	$cat_options[] = array( 'value' => 'Serbia', 'title' => 'Serbia' );
	$cat_options[] = array( 'value' => 'Seychelles', 'title' => 'Seychelles' );
	$cat_options[] = array( 'value' => 'Sierra Leone', 'title' => 'Sierra Leone' );
	$cat_options[] = array( 'value' => 'Singapore', 'title' => 'Singapore' );
	$cat_options[] = array( 'value' => 'Slovakia', 'title' => 'Slovakia' );
	$cat_options[] = array( 'value' => 'Slovenia', 'title' => 'Slovenia' );
	$cat_options[] = array( 'value' => 'Solomon Islands', 'title' => 'Solomon Islands' );
	$cat_options[] = array( 'value' => 'Somalia', 'title' => 'Somalia' );
	$cat_options[] = array( 'value' => 'South Africa', 'title' => 'South Africa' );
	$cat_options[] = array( 'value' => 'Spain', 'title' => 'Spain' );
	$cat_options[] = array( 'value' => 'Sri Lanka', 'title' => 'Sri Lanka' );
	$cat_options[] = array( 'value' => 'Sudan', 'title' => 'Sudan' );
	$cat_options[] = array( 'value' => 'Suriname', 'title' => 'Suriname' );
	$cat_options[] = array( 'value' => 'Swaziland', 'title' => 'Swaziland' );
	$cat_options[] = array( 'value' => 'Sweden', 'title' => 'Sweden' );
	$cat_options[] = array( 'value' => 'Switzerland', 'title' => 'Switzerland' );
	$cat_options[] = array( 'value' => 'Syria', 'title' => 'Syria' );
	$cat_options[] = array( 'value' => 'Tahiti', 'title' => 'Tahiti' );
	$cat_options[] = array( 'value' => 'Taiwan', 'title' => 'Taiwan' );
	$cat_options[] = array( 'value' => 'Tajikistan', 'title' => 'Tajikistan' );
	$cat_options[] = array( 'value' => 'Tanzania', 'title' => 'Tanzania' );
	$cat_options[] = array( 'value' => 'Thailand', 'title' => 'Thailand' );
	$cat_options[] = array( 'value' => 'Togo', 'title' => 'Togo' );
	$cat_options[] = array( 'value' => 'Tokelau', 'title' => 'Tokelau' );
	$cat_options[] = array( 'value' => 'Tonga', 'title' => 'Tonga' );
	$cat_options[] = array( 'value' => 'Trinidad &amp; Tobago', 'title' => 'Trinidad &amp; Tobago' );
	$cat_options[] = array( 'value' => 'Tunisia', 'title' => 'Tunisia' );
	$cat_options[] = array( 'value' => 'Turkey', 'title' => 'Turkey' );
	$cat_options[] = array( 'value' => 'Turkmenistan', 'title' => 'Turkmenistan' );
	$cat_options[] = array( 'value' => 'Turks &amp; Caicos Is', 'title' => 'Turks &amp; Caicos Is' );
	$cat_options[] = array( 'value' => 'Tuvalu', 'title' => 'Tuvalu' );
	$cat_options[] = array( 'value' => 'Uganda', 'title' => 'Uganda' );
	$cat_options[] = array( 'value' => 'Ukraine', 'title' => 'Ukraine' );
	$cat_options[] = array( 'value' => 'United Arab Erimates', 'title' => 'United Arab Emirates' );
	$cat_options[] = array( 'value' => 'United Kingdom', 'title' => 'United Kingdom' );
	$cat_options[] = array( 'value' => 'Uraguay', 'title' => 'Uruguay' );
	$cat_options[] = array( 'value' => 'Uzbekistan', 'title' => 'Uzbekistan' );
	$cat_options[] = array( 'value' => 'Vanuatu', 'title' => 'Vanuatu' );
	$cat_options[] = array( 'value' => 'Vatican City State', 'title' => 'Vatican City State' );
	$cat_options[] = array( 'value' => 'Venezuela', 'title' => 'Venezuela' );
	$cat_options[] = array( 'value' => 'Vietnam', 'title' => 'Vietnam' );
	$cat_options[] = array( 'value' => 'Virgin Islands (Brit)', 'title' => 'Virgin Islands (Brit)' );
	$cat_options[] = array( 'value' => 'Virgin Islands (USA)', 'title' => 'Virgin Islands (USA)' );
	$cat_options[] = array( 'value' => 'Wake Island', 'title' => 'Wake Island' );
	$cat_options[] = array( 'value' => 'Wallis &amp; Futana Is', 'title' => 'Wallis &amp; Futana Is' );
	$cat_options[] = array( 'value' => 'Yemen', 'title' => 'Yemen' );
	$cat_options[] = array( 'value' => 'Zaire', 'title' => 'Zaire' );
	$cat_options[] = array( 'value' => 'Zambia', 'title' => 'Zambia' );
	$cat_options[] = array( 'value' => 'Zimbabwe', 'title' => 'Zimbabwe' );

	$form['options'] = $cat_options;

	return $form;
}
*/

