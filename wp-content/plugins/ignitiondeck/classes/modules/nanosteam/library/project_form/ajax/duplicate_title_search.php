<?php

/**
 * Checking the porject title as the user enters it to ensure there is no duplicates
 * Moved the ajax function here as using admin-ajax.php is very slow to respond.
 */

ini_set( 'html_errors', 0 );
define( 'SHORTINIT', true );

require( $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php' );
require( ABSPATH . WPINC . '/formatting.php' );
wp_plugin_directory_constants();

function send_error( $i ) {
	error_log( print_r( $i, true ) );
}

$project_name = sanitize_text_field( $_POST['project_name'] );
$project_name = str_replace( array( ':', ' ', '.', "'" ), '', strtolower( $project_name ) );

$current_post_id = $_POST['post_id'];

global $wpdb;

$custom_post_type     = 'ignition_product'; // define your custom post type slug here
$custom_post_status_a = 'pending';
$custom_post_status_b = 'publish';

// A sql query to return all post titles
$prepare = $wpdb->prepare( "SELECT ID, post_title FROM {$wpdb->posts} WHERE post_status = %s OR post_status = %s  AND post_type = %s", $custom_post_status_a, $custom_post_status_b, $custom_post_type );
$results = $wpdb->get_results( $prepare, ARRAY_A );

// Adding all project titles to an array.
$output = array();

if ( isset( $results ) & is_array( $results ) & 1 < count( $results ) ) {
	foreach ( $results as $index => $post ) {

		$projects_name = sanitize_text_field( $post['post_title'] );
		$projects_name = str_replace( array( ':', ' ', '.', "'" ), '', strtolower( $projects_name ) );
		$project_post_id = $post['ID'];

		// If this project is the exisiting project with the name then let it pass.
		if ( ( $project_name === $projects_name ) && ( $current_post_id === $project_post_id ) ) {
			$project_name = '00pass00';
		}

		$output[] = $projects_name;
	}
}


if ( 10 > strlen( $project_name ) ) {
	echo json_encode(
		array(
			'valid' => true,
		)
	);
} elseif ( 10 < strlen( $project_name ) && in_array( $project_name, $output ) !== false ) {
	echo json_encode(
		array(
			'valid' => false,
		)
	);
} else {
	echo json_encode(
		array(
			'valid' => true,
		)
	);
}

die(); //stop "0" from being output