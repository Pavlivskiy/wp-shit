<?php if ( ! defined( 'ABSPATH' ) ) {
	die( 'Direct access forbidden.' );
}

$nano_fifty_review         = get_post_meta( $post->ID, 'fifty_fifty_grant', true );
$nano_fifty_admin_note     = get_post_meta( $post->ID, 'fifty_comments', true );
$nano_fifty_grant          = get_post_meta( $post->ID, 'nano_fifty_grant_apply', true );
$nano_project_significance = get_post_meta( $post->ID, 'nano_project_significance', true );

$nano_fifty_fields = array(
	array(
		'before' => '<p>' . __( 'Selecting either Approved or Declined will send an email to the project creator when project is updated. You can modify your email messaging', 'nanosteam_8n' ) . ' <a href="/wp-admin/admin.php?page=idc-email">here</a>. </p>',
		'misc'   => ( 'approved' == $nano_fifty_review ? 'checked="checked"' : '' ),
		'label'  => __( 'Approved', 'nanosteam_8n' ),
		'value'  => 'approved',
		'class'  => 'radio',
		'id'     => 'matching_approved',
		'name'   => 'fifty_fifty_grant',
		'type'   => 'radio',
	),
	array(
		'misc'  => ( 'declined' == $nano_fifty_review ? 'checked="checked"' : '' ),
		'label' => __( 'Declined', 'nanosteam_8n' ),
		'value' => 'declined',
		'class' => 'radio',
		'id'    => 'matching_declined',
		'name'  => 'fifty_fifty_grant',
		'type'  => 'radio',
	),
	array(
		'misc'  => ( 'pending' == $nano_fifty_review ? 'checked="checked"' : '' ),
		'label' => __( 'Pending Review', 'nanosteam_8n' ),
		'value' => 'pending',
		'class' => 'radio',
		'id'    => 'matching_pending_review',
		'name'  => 'fifty_fifty_grant',
		'type'  => 'radio',
	),
	array(
		'before' => '<p><strong>' . __( 'Grant Administrative Note', 'nanosteam_8n' ) . '</strong><em> (for internal use - will not be seen by project creator) </em></p>',
		'misc'   => 'rows="6"',
		'value'  => ( isset( $nano_fifty_admin_note ) ? $nano_fifty_admin_note : '' ),
		'name'   => 'fifty_comments',
		'type'   => 'textarea',
		'class'  => 'nano-full-width',
	),
);

$form_fifty = new ID_Form( $nano_fifty_fields );

if ( 'yes' == $nano_fifty_grant ) {
	echo apply_filters( 'nano_fifty_form', $form_fifty->build_form() );
} else {
	echo '<h4>This project has not been submitted for a Matching Grant review.</h4>';
}

