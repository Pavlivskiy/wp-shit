<?php if ( ! defined( 'ABSPATH' ) ) {
	die( 'Direct access forbidden.' );
}

// live post
$form[] = array( 'before' => '<div class="fes_section">' );
$form[] = apply_filters( 'fes_updates_form', array(
	'label'   => __( 'Project Updates', 'nanosteam_8n' ),
	'value'   => ( isset( $vars['project_updates'] ) ? $vars['project_updates'] : '' ),
	'name'    => 'project_updates',
	'id'      => 'project_updates',
	'type'    => 'wpeditor',
	'wclass'  => 'col wpeditor',
	'heading' => __( 'Project Updates', 'nanosteam_8n' ),
) );
$form[] = array(
	'description' => __( 'Who can see this post?', 'nanosteam_8n' ),
	'help'        => nano_help_field( 'project_updates' ),
);
$form[] = nano_labnotes_backers( $vars );
$form[] = nano_labnotes_public( $vars );
$form[] = apply_filters( 'fes_updates_after', $vars );
$form[] = array( 'after' => '</div>' );



function nano_labnotes_backers( $vars ) {

	$post_id       = $_GET['edit_project'];
	$backer_public = get_post_meta( $post_id, 'nano_public_backer', true );

	$field = array(
		'label'  => apply_filters( 'idfu_update_backer', __( 'Visible to Backers Only', 'idfu' ) ),
		'name'   => 'nano_public_backer',
		'id'     => 'new_update_backer',
		'type'   => 'radio',
		'value'  => 'backer',
		'wclass' => 'custom-control custom-radio custom-control-inline',
		'class'  => 'custom-control-input',
		'misc'   => ( 'backers' == $backer_public ? 'checked="checked"' : '' ),
		'before' => '<div class="form-group form-row pb-3"><div class="col-sm">',
	);

	return $field;
}

function nano_labnotes_public( $vars ) {

	$post_id       = $_GET['edit_project'];
	$backer_public = get_post_meta( $post_id, 'nano_public_backer', true );

	$field = array(
		'label'  => apply_filters( 'idfu_update_public', __( 'Visible to Public', 'idfu' ) ),
		'name'   => 'nano_public_backer',
		'id'     => 'new_update_public',
		'type'   => 'radio',
		'value'  => 'public',
		'wclass' => 'custom-control custom-radio custom-control-inline',
		'class'  => 'custom-control-input',
		'misc'   => ( 'public' == $backer_public || '' == $backer_public ? 'checked="checked"' : '' ),
		'after'  => '</div></div>',
	);

	return $field;
}
