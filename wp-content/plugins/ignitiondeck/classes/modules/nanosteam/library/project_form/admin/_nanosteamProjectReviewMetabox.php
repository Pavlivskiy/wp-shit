<?php if ( ! defined( 'ABSPATH' ) ) {
	die( 'Direct access forbidden.' );
}


$project_review_subject      = get_post_meta( $_GET['post'], 'project_review_subject', true ) != '' ? array_values( array_slice( get_post_meta( $_GET['post'], 'project_review_subject' ), - 1 ) )[0] : '';
$project_review_message      = get_post_meta( $_GET['post'], 'project_review_message', true ) != '' ? array_values( array_slice( get_post_meta( $_GET['post'], 'project_review_message' ), - 1 ) )[0] : '';
$project_review_status_radio = get_post_meta( $_GET['post'], 'project_review_status_radio', true );
$project_review_status_email = get_post_meta( $_GET['post'], 'project_review_status_email', true );
$nano_review_admin_note      = get_post_meta( $_GET['post'], 'project_admin_notes', true );


$fields = array(
	array(
		'label'  => __( 'Approved', 'nanosteam_8n' ),
		'name'   => 'project_review_status_radio',
		'id'     => 'approved',
		'type'   => 'radio',
		'value'  => 'approved',
		//'wclass' => '',
		//'class'  => 'project_review_status_radio',
		'misc'   => ( 'approved' == $project_review_status_radio ? 'checked="checked"' : '' ) . ' required',
		'before' => '<p>Please select whether this project is approved or needs further updates.</p><div class="form-group form-row pb-3">',
		'wclass' => 'custom-control custom-radio custom-control-inline',
		'class'  => 'custom-control-input project_review_status_radio',

	),
	array(
		'label'  => __( 'Required to re-submit', 'nanosteam_8n' ),
		'name'   => 'project_review_status_radio',
		'id'     => 'resubmit',
		'type'   => 'radio',
		'value'  => 'resubmit',
		//'wclass' => '',
		//'class'  => 'project_review_status_radio',
		'wclass' => 'custom-control custom-radio custom-control-inline',
		'class'  => 'custom-control-input project_review_status_radio',
		'misc'   => ( 'resubmit' == $project_review_status_radio ? 'checked="checked"' : '' ) . ' required',
		'before' => '',
		'after'  => '</div>',
	),
	array(
		'value'  => ( 'pending' == $project_review_status_email ? $project_review_subject : '' ),
		'name'   => 'project_review_subject',
		'id'     => 'project_review_subject',
		'type'   => 'text',
		'class'  => ' nano-full-width',
		'wclass' => 'form-row project_review_subject' . ( '' == $project_review_subject ? ' fade d-none' : '' ),
		'misc'   => 'rows=6 style="margin-top:20px" placeholder="Subject"',
		'after'  => '',
	),
	array(
		'value'  => ( 'pending' == $project_review_status_email ? $project_review_message : '' ),
		'name'   => 'project_review_message',
		'id'     => 'project_review_message',
		'type'   => 'textarea',
		'class'  => ' nano-full-width',
		'wclass' => 'form-row project_review_message' . ( '' == $project_review_message ? ' fade d-none' : '' ),
		'misc'   => 'rows=6 style="margin-top:20px" placeholder="Message"',
		'after'  => '',
	),
	array(
		'value'  => $project_review_status_email,
		'name'   => 'project_review_status_email',
		'id'     => 'project_review_status_email',
		'type'   => 'hidden',
		'class'  => 'project_review_status_email',
		'wclass' => 'form-row',
		'misc'   => null,
		'after'  => '',
	),
	array(
		'value'  => ( 'sent' == $project_review_status_email ? __( 'Email sent', 'nanosteam_8n' ) : ( 'pending' == $project_review_status_email ? __( 'Pending email to be sent', 'nanosteam_8n' ) : __( 'Send email', 'nanosteam_8n' ) ) ),
		'name'   => 'project_review_submit',
		'id'     => 'project_review_submit',
		'type'   => 'submit',
		'class'  => 'button button-primary button-large',
		'wclass' => 'form-row nano-btn btn-left project_review_submit' . ( '' == $project_review_message ? ' fade d-none' : '' ),
		'misc'   => ( 'sent' == $project_review_status_email ? ' disabled ' : '' ),
		'after'  => '',
	),
	array(
		'before' => '<p><strong>' . __( 'Project Administrative Note', 'nanosteam_8n' ) . '</strong><em> (for internal use - will not be seen by project creator) </em></p>',
		'misc'   => 'rows="6"',
		'value'  => ( isset( $nano_review_admin_note ) ? $nano_review_admin_note : '' ),
		'name'   => 'project_admin_notes',
		'type'   => 'textarea',
		'class'  => 'nano-full-width',
	),
);

$form = new NANO_Form( $fields );

echo apply_filters( 'nano_project_review', $form->build_form() );
