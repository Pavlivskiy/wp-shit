<?php if ( ! defined( 'ABSPATH' ) ) {
	die( 'Direct access forbidden.' );
}

if ( ( isset( $_GET['edit_project'] ) && self::team_lead_only() == true ) || isset( $_GET['create_project'] ) ) {

	$form[] = array(
		'heading'     => __( 'Budget', 'nanosteam_8n' ),
		'help'        => nano_help_field( 'budget' ),
		'description' => nano_description_field( 'budget' ),
	);

	$form[] = nano_budget_description( $vars );

	$form[] = array(
		'before' => '<div class="row"><div class="col-sm-9"><div class="budget-wrapper">',
	);

	$budget_data   = isset( $vars['post_id'] ) ? get_post_meta( $vars['post_id'] ) : false;
	$budget_titles = isset( $budget_data['nano_budget_title'] ) ? maybe_unserialize( $budget_data['nano_budget_title'][0] ) : '';
	$budget_values = isset( $budget_data['nano_budget_value'] ) ? maybe_unserialize( $budget_data['nano_budget_value'][0] ) : '';
	$i             = 1;
	$len           = is_array( $budget_titles ) ? count( $budget_titles ) : 0;

	$budget_list  = '';
	$budget_total = 0;

	if ( is_array( $budget_titles ) && isset($budget_titles[0]) ) {

		foreach ( $budget_titles as $k => $v ) {

			$title = '' != $v ? $v : 'Title';
			$value = isset( $budget_values[ $k ] ) && '' != $budget_values[ $k ] ? $budget_values[ $k ] : '50';

			$budget_list .= '<li data-nantitle="' . $title . '" data-value="' . $value . '"><i class="fa fa-circle"></i> ' . $title . '</li>';
			if ( isset( $budget_values[ $k ] ) && '' != $budget_values[ $k ] ) {
				$budget_total += $budget_values[ $k ];
			}

			$form[] = nano_budget_title( $vars, $k );
			$form[] = nano_budget_value( $vars, $k );

			$i ++;
		}
	} else {
		$form[] = nano_budget_title( $vars, 0 );
		$form[] = nano_budget_value( $vars, 0 );
		$form[] = nano_budget_title( $vars, 1 );
		$form[] = nano_budget_value( $vars, 1 );

		$budget_list .= '<li data-value="50"><i class="fa fa-circle"></i> Title</li>';
		$budget_list .= '<li data-value="50"><i class="fa fa-circle"></i> Title</li>';
	}
	$goal_amount  = isset( $vars['project_goal'] ) ? $vars['project_goal'] : '';
	$budget_tally = isset( $vars['project_goal'] ) && '' !== $vars['project_goal'] ? $vars['project_goal'] - $budget_total : '';

	$form[] = array(
		'before' => '</div><div class="row budget_tally fade"><div class="col-sm-9 col-7 text-sm-right"><p>' . __( 'Remaining from goal', 'nanosteam_8n' ) . '</p></div><div class="col-sm-3 col-5 dataval" data-budget="' . $goal_amount . '">' . $budget_tally . '</div></div>',
		'after'  => '<div class="pb-3"><button class="add_budget_button btn btn-sm btn-primary">Add budget item</button></div></div><div class="col-sm-3"><canvas id="budget_chart" width="400" height="600"></canvas></div></div>',
	);

}

function nano_budget_description( $vars ) {

	$text = isset( $vars['post_id'] ) ? get_post_meta( $vars['post_id'], 'nano_budget_description', true ) : '';

	$field = array(
		'label'  => __( 'Budget Description', 'nanosteam_8n' ),
		'value'  => $text,
		'name'   => 'nano_budget_description',
		'id'     => 'nano_budget_description',
		'type'   => 'textarea',
		'class'  => 'required js-auto-size',
		'wclass' => 'col',
		'misc'   => 'maxlength=500 placeholder="Outline the details of how you will use your budget here." required',
		'before' => '<div class="form-group form-row pb-3">',
		'after'  => '</div>',

	);

	return $field;
}

function nano_budget_title( $vars, $key ) {

	$vars = isset( $vars['post_id'] ) ? get_post_meta( $vars['post_id'] ) : '';

	$nano_budget_title = isset( $vars['nano_budget_title'] ) ? unserialize( $vars['nano_budget_title'][0] ) : '';

	$field = array(
		'label'  => __( 'Budget Title', 'nanosteam_8n' ),
		'value'  => ( isset( $nano_budget_title[ $key ] ) ? $nano_budget_title[ $key ] : '' ),
		'name'   => 'nano_budget_title[]',
		'id'     => '',
		'type'   => 'text',
		'misc'   => 'maxlength=140 placeholder="Title" ' . ( $key < 2 ? 'required' : '' ) . ' data-title="'.( isset( $nano_budget_title[ $key ] ) ? $nano_budget_title[ $key ] : '' ).'"',
		'class'  => 'required budget_title',
		'wclass' => 'col-sm-9 col-7',
		'before' => '<div class="form-group form-row">',
	);

	return $field;

}

function nano_budget_value( $vars, $key ) {

	$vars  = isset( $vars['post_id'] ) ? get_post_meta( $vars['post_id'] ) : $vars;
	$value = isset( $vars['nano_budget_value'][0] ) ? unserialize( $vars['nano_budget_value'][0] ) : '';

	$field = array(
		'label'  => __( 'Budget Value', 'nanosteam_8n' ),
		'value'  => ( isset( $vars['nano_budget_value'][0] ) && isset( $value[ $key ] ) && '' != $value[ $key ] ? $value[ $key ] : '' ),
		'name'   => 'nano_budget_value[]',
		'id'     => '',
		'type'   => 'text',
		'misc'   => 'placeholder="$" ' . ( $key < 2 ? 'required' : '' ),
		'class'  => 'svg_val ',
		'wclass' => 'col-sm-3 col-5',
		'after'  => '</div>',
	);

	return $field;
}
