<?php if ( ! defined( 'ABSPATH' ) ) {
	die( 'Direct access forbidden.' );
}


class Class_Nanosteam_Form_Container {
	var $form;
	var $vars;

	function __construct( $form = null, $vars = null ) {

	}

	public static function nano_steam_create_form( $form = null, $vars = null ) {

		$author_check = true;

		if ( isset( $_GET['edit_project'] ) ) {
			$post_id            = $_GET['edit_project'];
			$post_status        = get_post_meta( $post_id, 'ign_project_success', true );
			$current_user       = wp_get_current_user()->ID;
			$original_author    = get_user_by( 'login', get_post_meta( $post_id, 'original_author', true ) ) != false ? get_user_by( 'login', get_post_meta( $post_id, 'original_author', true ) ) : get_user_by( 'id', get_post( $post_id )->post_author );
			$original_author_id = $original_author->ID;
			$author_check       = $current_user == $original_author_id ? true : false;
		}

		$form = apply_filters( 'id_fes_form_init', $form, $vars );

		$form[] = self::nano_security_check();

		$form[] = array(
			'before' => '<div class="form-row justify-content-end sticky-top stick-margin-top mb-3 bg-sm-white"><div class="col-md-3 col-sm-5"><div class="form-row justify-content-end bg-white pt-1">', // Buttons
		);


		$submit_button = array(
			'value'  => ( isset( $vars['status'] ) && strtoupper( $vars['status'] ) == 'PUBLISH' ? __( 'Update', 'nanosteam_8n' ) : __( 'Update', 'nanosteam_8n' ) ),
			'name'   => 'project_fesubmit',
			'type'   => 'submit',
			'class'  => 'project_fesubmit btn btn-primary ' . ( isset( $vars['status'] ) && strtoupper( $vars['status'] ) == 'PUBLISH' ? 'project_published' : '' ),
			'wclass' => 'col-6',
		);

		$preview_button = array(
			'value'  => __( 'Preview', 'nanosteam_8n' ),
			'name'   => 'project_fesave',
			'class'  => 'project_fesave btn-outline-primary btn-light ' . ( isset( $vars['status'] ) && strtoupper( $vars['status'] ) == 'PUBLISH' ? 'project_published' : '' ) . ( isset( $vars['status'] ) && strtoupper( $vars['status'] ) == 'PENDING' ? ' project_pending' : '' ),
			'type'   => 'submit',
			'wclass' => 'col-6',
		);

		if ( empty( $vars['status'] ) || strtoupper( $vars['status'] ) == 'DRAFT' ) {
			$preview_button['misc']  = '';
			$preview_button['name']  = 'project_fesave';
			$preview_button['class'] = 'project_fesave btn-outline-primary';
			$submit_button['value']  = __( 'Submit', 'nanosteam_8n' );
			$submit_button['wclass'] = 'col-6';
			$submit_button['misc']   = ( isset( $_GET['create_project'] ) ? '' : '' );
		}

		if ( isset( $vars['status'] ) && strtoupper( $vars['status'] ) == 'PUBLISH' ) {
			$preview_button['name']  = 'project_fesubmit';
			$preview_button['class'] = 'project_fesubmit btn-outline-primary';
		}



		$form[] = $preview_button;
		$form[] = $submit_button;

		$form[] = array(
			'before' => '</div></div></div>', // End Buttons
		);

		$form[] = array(
			'before' => '<div class="row"><div class="col-lg-9 '. ( 'desktop' === is_mobile() ? 'zindex-fixed' : '' ) .'"><div id="notify" class="stripe-error alert alert-info d-none fade"></div>' . self::matching_alert() . self::nano_project_emails( ( isset( $vars['post_id'] ) ? $vars['post_id'] : null ) ),
			// Contain the form with col-lg-9
		);

		//$form[] = array('before' => self::nano_fifty_grant($vars));
		$form[] = array( 'before' => self::nav_pills( $vars ) );
		$form[] = array( 'after' => '<div class="tab-content" id="projectContent">' ); // Tabs

		$form[] = array( 'before' => '<div class="tab-pane fade ' . ( true == $author_check ? 'show active' : '' ) . '" id="basics" role="tabpanel" aria-labelledby="basics-tab">' );// Basics

		if ( ( isset( $_GET['edit_project'] ) && self::team_lead_only() == true ) || isset( $_GET['create_project'] ) ) {
			include_once '_nanoBasics.php';
		}

		$form[] = array( 'before' => '</div>' ); // End Basics

		$form[] = array( 'before' => '<div class="tab-pane fade" id="goals" role="tabpanel" aria-labelledby="goals-tab">' ); // Goals

		if ( ( isset( $_GET['edit_project'] ) && self::team_lead_only() == true ) || isset( $_GET['create_project'] ) ) {
			include_once '_nanoGoals.php';
		}

		$form[] = array( 'before' => '</div>' ); // End Goals

		$form[] = array( 'before' => '<div class="tab-pane fade" id="funding" role="tabpanel" aria-labelledby="funding-tab">' );  // Funding

		if ( ( isset( $_GET['edit_project'] ) && self::team_lead_only() == true ) || isset( $_GET['create_project'] ) ) {
			include_once '_nanoFunding.php';
		}

		$form[] = array( 'before' => '</div>' );  // End Funding

		$form[] = array( 'before' => '<div class="tab-pane fade" id="budget" role="tabpanel" aria-labelledby="budget-tab">' );  // Budget

		if ( ( isset( $_GET['edit_project'] ) && self::team_lead_only() == true ) || isset( $_GET['create_project'] ) ) {
			include_once '_nanoBudget.php';
		}

		$form[] = array( 'before' => '</div>' );// End Budget

		$form[] = array( 'before' => '<div class="tab-pane fade" id="team" role="tabpanel" aria-labelledby="team-tab">' );  // Team

		if ( ( isset( $_GET['edit_project'] ) && self::team_lead_only() == true ) || isset( $_GET['create_project'] ) ) {
			include_once '_nanoTeam.php';
		}

		$form[] = array( 'before' => '</div>' );  // End Team

		if ( isset( $vars['post_id'] ) && get_post_meta( $vars['post_id'], 'ign_days_left', true ) == '0' && get_post_meta( $vars['post_id'], 'ign_percent_raised', true ) >= '100' ) {

			$form[] = array( 'before' => '<div class="tab-pane fade ' . ( false == $author_check ? 'show active' : '' ) . '" id="labnotes" role="tabpanel" aria-labelledby="labnotes-tab">' );  // Labnotes

			include_once '_nanoLabnotes.php';

			$form[] = array( 'before' => '</div>' ); // End Labnotes

		}

		$form[] = array( 'before' => '</div>' );  // End Tabs

		$form[] = array(
			'label'  => '',
			'value'  => '1',
			'name'   => 'project_levels',
			'id'     => 'project_levels',
			'type'   => 'hidden',
			'wclass' => 'd-none',
			'class'  => '',
		);

		$form[] = array(
			'before' => '<div class="form-row justify-content-sm-end">', // Buttons
		);

		if ( empty( $vars['status'] ) || strtoupper( $vars['status'] ) == 'DRAFT' ) {

			$form[] = array(
				'value'  => ( empty( $vars['status'] ) || 'DRAFT' == strtoupper( $vars['status'] ) ? __( 'Preview', 'nanosteam_8n' ) : __( 'Update', 'nanosteam_8n' ) ),
				'name'   => 'project_fesave',
				'class'  => 'project_fesave btn btn-outline-primary',
				'type'   => 'submit',
				'wclass' => 'col-md-2 col-sm-3 col-6',
				'misc'   => '',
			);

			$form[] = array(
				'value'  => __( 'Continue', 'nanosteam_8n' ),
				'name'   => 'project_continue',
				'id'     => 'project_continue',
				'class'  => 'btn btn-primary',
				'type'   => 'submit',
				'wclass' => 'col-md-2 col-sm-3 col-6 continue-btn',
				'misc'   => 'formnovalidate',
			);

			$form[] = array(
				'value'  => ( isset( $vars['status'] ) && strtoupper( $vars['status'] ) == 'PUBLISH' ? __( 'Update', 'nanosteam_8n' ) : ( empty( $vars['status'] ) || strtoupper( $vars['status'] ) == 'DRAFT' ) ? __( 'Submit', 'nanosteam_8n' ) : __( 'Update Submission', 'nanosteam_8n' ) ),
				'name'   => 'project_fesubmit',
				'type'   => 'submit',
				'class'  => 'project_fesubmit btn btn-primary ' . ( isset( $vars['status'] ) && strtoupper( $vars['status'] ) == 'PUBLISH' ? 'project_published' : '' ),
				'wclass' => 'col-md-3 col-6 d-none submit-btn',
				'misc'   => '',
			);

		}

		$form[] = array(
			'before' => '</div>', // end Buttons
		);

		$form[] = array(
			'before' => '</div></div>', // End col-md-9
		);

		if ( isset( $vars['post_id'] ) && $vars['post_id'] > 0 ) {
			$form[] = array(
				'value'  => $vars['post_id'],
				'name'   => 'project_post_id',
				'type'   => 'hidden',
				'wclass' => 'd-none',
			);
		}

		$form[] = self::nano_creator_id();

		$form[] = array(
			'value'  => ( empty( $vars['status'] ) ? __( 'Save and Continue', 'nanosteam_8n' ) : __( 'Save and Continue', 'nanosteam_8n' ) ),
			'name'   => 'project_fesave',
			'id'     => 'project_save',
			'class'  => 'project_fesave btn btn-outline-primary btn-block',
			'type'   => 'submit',
			'wclass' => 'col-sm-6',
			'misc'   => 'formnovalidate',
			'before' => '<div class="modal fade" id="confirmExit" tabindex="-1" role="dialog" aria-labelledby="confirmExitTitle" aria-hidden="true"><div class="modal-dialog modal-center" style="max-width:500px" role="document"><div class="modal-content"><div class="modal-header"><h5 class="modal-title" id="confirmExitTitle">Exit without saving?</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="p-3"><div class="row"><div class="col-sm-6 mb-2 mb-sm-0"><a id="continue" href="#" class="btn btn-primary btn-block px-3">Continue without saving</a></div>',
			'after'  => '</div></div></div></div></div>',
		);
		// END MODAL CONFIRM

		$form[] = array(
			'before' => '
<div class="modal fade" id="FormErrors" tabindex="-1" role="dialog" aria-labelledby="FormErrorsTitle" aria-hidden="true">
<div class="modal-dialog modal-center modal" role="document">
<div class="modal-content">
	<div class="modal-header border-bottom-0">
		<alert id="error-list" class="alert alert-danger w-100" role="alert"></alert>
	</div>
<div class="px-3">
<div class="row">
	<div class="col-sm mb-2 mb-sm-0">
	<p class="px-5 pb-3">Please finish filling out the required information on each of the following pages below.</p>
	<ul style="border-radius: 5px" class="border pb-3" id="valerrors"></ul></div>',
			'after'  => '</div></div></div></div></div>' . self::confirm_instructions_modal(),
		);

		return $form;
	}

	// GENERAL FUNCTIONS

	public static function nav_pills( $vars ) {

		$published = isset( $vars['status'] ) && strtoupper( $vars['status'] ) == 'PUBLISH' && isset( $_GET['edit_project'] ) ? true : false;

		$author_check = true;
		if ( isset( $_GET['edit_project'] ) ) {
			$post_id            = $_GET['edit_project'];
			$current_user       = wp_get_current_user()->ID;
			$original_author    = get_user_by( 'login', get_post_meta( $post_id, 'original_author', true ) ) != false ? get_user_by( 'login', get_post_meta( $post_id, 'original_author', true ) ) : get_user_by( 'id', get_post( $post_id )->post_author );
			$original_author_id = $original_author->ID;
			$author_check       = $current_user == $original_author_id ? true : false;
		}
		$pills = '<div><nav class="nav nav-pills nav-fill mb-4 bg-light">';
		if ( true == $author_check && ! isset( $_GET['labnotes'] ) ) {
			$pills .= '<a class="nav-item nav-link text-sm-center px-3 ' . ( true == $author_check ? 'active' : '' ) . '" id="basics-tab" href="#basics" data-toggle="tab" role="tab" aria-controls="basics" ><span class="project_section d-block">1</span><span class="d-none d-sm-inline">Basics <i class="fa"></i></span></a>';
			$pills .= '<a class="nav-item nav-link text-sm-center px-3" id="goals-tab" href="#goals" data-toggle="tab" role="tab" aria-controls="goals" ><span class="project_section d-block">2</span> <span class="d-none d-sm-inline">Goals <i class="fa"></i></span></a>';
			$pills .= '<a class="nav-item nav-link text-sm-center px-3" id="funding-tab" href="#funding" data-toggle="tab" role="tab" aria-controls="funding" ><span class="project_section d-block">3</span> <span class="d-none d-sm-inline">Funding <i class="fa"></i></span></a>';
			$pills .= '<a class="nav-item nav-link text-sm-center px-3" id="budget-tab" href="#budget" data-toggle="tab" role="tab" aria-controls="budget" ><span class="project_section d-block">4</span> <span class="d-none d-sm-inline">Budget <i class="fa"></i></span></a>';
			$pills .= '<a class="nav-item nav-link text-sm-center px-3" id="team-tab" href="#team" data-toggle="tab" role="tab" aria-controls="team" ><span class="project_section d-block">5</span> <span class="d-none d-sm-inline">Team <i class="fa"></i></span></a>';
		}
		if ( isset( $vars['post_id'] ) && get_post_meta( $vars['post_id'], 'ign_days_left', true ) == '0' && get_post_meta( $vars['post_id'], 'ign_percent_raised', true ) >= '100' ) {
			if ( ( isset( $_GET['edit_project'] ) && self::team_lead_only() == true ) ) {
				$pills .= ( true == $published ? '<a class="d-none nav-item nav-link text-sm-center px-3 ' . ( false == $author_check ? 'active' : '' ) . '" id="labnotes-tab" href="#labnotes" data-toggle="tab" role="tab" aria-controls="labnotes" >' . ( true == $author_check ? '<span class="project_section d-block">6</span> <span class="d-none d-sm-inline">Labnotes <i class="fa"></i></span>' : 'Labnotes' ) . '</a>' : '' );
			}
		}
		$pills .= '</nav></div> ';

		return $pills;

	}

	// Security Check to ensure users are allowed to access the project page.

	public static function nano_security_check() {
		if ( ( isset( $_GET['edit_project'] ) || isset( $_GET['create_project'] ) ) && ! is_user_logged_in() ) {
			die();
		} elseif ( isset( $_GET['edit_project'] ) && is_user_logged_in() ) {

			$post_id = $_GET['edit_project'];

			// getting everyone who is allowed to have access to this project
			$team_members    = get_post_meta( $post_id, 'nano_team_members', true ) != '' ? get_post_meta( $post_id, 'nano_team_members', true ) : array();
			$original_author = get_user_by( 'login', get_post_meta( $post_id, 'original_author', true ) ) != false ? get_user_by( 'login', get_post_meta( $post_id, 'original_author', true ) ) : get_user_by( 'id', get_post( $post_id )->post_author );
			$original_author = array( $original_author->user_login );

			// Getting current user ID
			$current_user       = wp_get_current_user();
			$current_user_email = $current_user->user_login;

			$team = array_merge( $team_members, $original_author );
			if ( ! in_array( $current_user_email, $team ) ) {
				die();
			}
		}
	}

	public static function team_lead_only() {
		$team_lead_only = true;
		if ( isset( $_GET['edit_project'] ) ) {
			$team_leader    = wp_get_current_user()->user_login == get_post_meta( $_GET['edit_project'], 'original_author', true ) ? true : false;
			$team_lead_only = true != $team_leader ? false : true;
		}

		return $team_lead_only;
	}

	public static function matching_modal() {
		$content  = '<div class="modal fade" tabindex="-1" role="dialog" id="nano-matching">';
		$content .= '<div class="modal-dialog" role="document">';
		$content .= '<div class="modal-content">';
		$content .= '<div class="modal-body">';
		$content .= nano_modal_page_load( 'matching-grant-requirements' );
		$content .= '</div>';
		$content .= '<div class="modal-footer">';
		$content .= '<button type="button" class="btn-sm btn btn-secondary" data-dismiss="modal"><i class="fa fa-close"></i></button>';
		$content .= '</div></div></div></div>';

		return $content;
	}

	public static function matching_alert() {

		$link = get_permalink( get_page_by_title( 'Matching Grant Overview' ) );

		$content  = '<div id="nano-matching" class="alert alert-info alert-dismissible d-none fade" role="alert">';
		$content .= '<strong>' . __( 'Important.', 'nanosteam_8n' ) . '</strong><br>' . __( 'Matching Grants have requirements that must be met to qualify for a grant from Nanosteam.', 'nanosteam_8n' );
		$content .= ' <a class="alert-link d-block" href="' . $link . '" target="_blank">Click here for more details.</a>';
		$content .= '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
		$content .= '<span aria-hidden="true">&times;</span>';
		$content .= '</button>';
		$content .= '</div>';

		return $content;
	}

	public static function confirm_instructions_modal() {
		$content  = '<div class="modal fade" tabindex="-1" role="dialog" id="nano-instructions">';
		$content .= '<div class="modal-dialog" role="document">';
		$content .= '<div class="modal-content">';
		$content .= '<div class="modal-body">';
		if ( isset( $_GET['labnotes'] ) ) {
			$content .= nano_modal_page_load( 'final-information-labnote-form' );
		} else {
			$content .= nano_modal_page_load( 'final-information-project-form' );
		}
		$content .= '</div>';
		$content .= '<div class="modal-footer">';
		$content .= '<button type="button" id="agree-continue" class="btn-sm btn btn-primary" data-dismiss="modal">Agree and continue</button><button type="button" class="btn-sm btn btn-secondary" data-dismiss="modal">Cancel</button>';
		$content .= '</div></div></div></div>';

		return $content;
	}

	// FUNDING

	public static function nano_fifty_grant() {

		$post_id           = isset( $_GET['edit_project'] ) ? $_GET['edit_project'] : '';
		$nano_fifty_grant  = isset( $post_id ) ? get_post_meta( $post_id, 'nano_fifty_grant_apply', true ) : '';
		$nano_fifty_review = isset( $post_id ) ? get_post_meta( $post_id, 'fifty_fifty_grant', true ) : '';
		$status            = '';
		if ( 'yes' == $nano_fifty_grant ) {
			$status .= '<div class="alert alert-success"><h4>' . __( 'Matching Grant Application Status', 'nanosteam_8n' ) . '</h4>';
			if ( 'approved' == $nano_fifty_review ) {
				$status .= '<p>' . __( 'Review Status:', 'nanosteam_8n' ) . '<strong> Approved</strong>, please check your email for details.</p>';
			} elseif ( 'declined' == $nano_fifty_review ) {
				$status .= '<p>' . __( 'Review Status:', 'nanosteam_8n' ) . '<strong> Declined</strong>, please check your email for details.</p>';
			} elseif ( 'pending' == $nano_fifty_review || null == $nano_fifty_review ) {
				$status .= '<p>' . __( 'Review Status:', 'nanosteam_8n' ) . '<strong> Pending Review </strong></p>';
			}
			$status .= '</div>';
		}
		if ( 'no' == $nano_fifty_grant ) {
			$status = '';
		}

		return $status;
	}


	public static function nano_creator_id() {

		$current_user = wp_get_current_user();
		$user_email   = $current_user->data->user_email;

		$field = array(
			'value'  => $user_email,
			'id'     => 'creator_id',
			'name'   => 'creator_id',
			'type'   => 'hidden',
			'wclass' => 'd-none',
		);

		return $field;
	}

	public static function nano_project_emails( $post_id ) {
		$project_subjects = get_post_meta( $post_id, 'project_review_subject' );
		$project_messages = get_post_meta( $post_id, 'project_review_message' );

		$content = '';

		if ( isset( $post_id ) && strtoupper( get_post_status( $post_id ) ) !== 'PUBLISH' ) {
			if ( isset( $project_subjects ) && is_array( $project_subjects ) && count( $project_subjects ) > 0 ) {
				$content          = '<div class="alert alert-info alert-dismissible fade show" role="alert">';
				$project_subjects = array_reverse( $project_subjects, true );

				$count    = 1;
				$last_key = count( $project_subjects );
				$content .= '<h4>Further requirements before we can approved your project.</h4>';
				foreach ( $project_subjects as $key => $value ) {

					$subject = $value;
					$message = $project_messages[ $key ];

					$content .= '<h6>' . $subject . ' <sup>' . $count . '</sup></h6>';
					$content .= '<small class="mb-0">' . $message . '</small>';
					if ( $count !== $last_key ) {
						$content .= '<hr>';
					}

					$count ++;
				}

				$content .= '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
				$content .= '<span aria-hidden="true">&times;</span>';
				$content .= '</button>';
				$content .= '</div>';
			}
		}

		return $content;
	}


	public static function nanosteam_form( $form = '', $vars = null ) {
		return self::nano_steam_create_form( $form, $vars );
	}

}
