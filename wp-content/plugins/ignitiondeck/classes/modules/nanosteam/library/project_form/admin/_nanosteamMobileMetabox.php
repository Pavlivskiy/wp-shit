<?php if ( ! defined( 'ABSPATH' ) ) {
	die( 'Direct access forbidden.' );
}

$nano_mobile_content_val = isset( $_GET['post'] ) ? get_post_meta( $_GET['post'], 'nano_mobile_content', true ) : '';

$nano_mobile_content = array(
	array(
		'label'   => __( 'Mobile', 'nanosteam_8n' ),
		'value'   => $nano_mobile_content_val,
		'name'    => 'nano_mobile_content',
		'id'      => 'nano_mobile_content',
		'type'    => 'wpeditor',
		'class'   => '',
		'wclass'  => 'col',
		'misc'    => 'placeholder="create the mobile version here if needed"',
		'heading' => __( 'Mobile version here if required.', 'nanosteam_8n' ),
	),
);


$nano_mobile = new NANO_Form( $nano_mobile_content );

echo apply_filters( 'nano_mobile_form', $nano_mobile->build_form() );
