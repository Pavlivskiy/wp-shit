<?php if ( ! defined( 'ABSPATH' ) ) {
	die( 'Direct access forbidden.' );
}
// self is here as it's referenced in nanosteamProjectFields.php

if ( ( isset( $_GET['edit_project'] ) && self::team_lead_only() == true ) || isset( $_GET['create_project'] ) ) {

	if ( empty( $vars['status'] ) || strtoupper( $vars['status'] ) !== 'PUBLISH' ) {
		// draft or pending review
		$form[] = array(
			'heading'     => __( 'Funding Type', 'nanosteam_8n' ),
			'description' => nano_description_field( 'funding_type' ),
			'before'      => '<div class="pb-3">',
			'after'       => '</div>',
			'help'        => nano_help_field( 'funding_type' ),
		);

		// Add in 50/50 form question
		$form[] = nano_fifty_grant_apply_yes();
		$form[] = nano_fifty_grant_apply_no();
	}

	// Custom date selection tool
	$form[] = array( 'before' => '<div class="row"><div class="col-sm-6">' );
	$form[] = nano_funding_amount( $vars );
	$form[] = array( 'before' => '</div>' );
	$form[] = array( 'before' => '<div class="col-sm-6">' );
	$form[] = nano_funding_date_select( $vars );
	$form[] = nano_funding_start_date_field( $vars );
	$form[] = nano_funding_end_date_field( $vars );
	$form[] = array( 'before' => '</div></div>' );
	// Add in 50/50 form question
	$form[] = id_funding_close_option_field( $vars );
	// Stripe Account Setup
	include_once '_nanoStripeConnect.php';

}

// FUNDING

function nano_fifty_grant_apply_yes() {

	$post_id          = isset( $_GET['edit_project'] ) ? $_GET['edit_project'] : '';
	$nano_fifty_grant = isset( $post_id ) ? get_post_meta( $post_id, 'nano_fifty_grant_apply', true ) : '';
	$field            = array(
		'label'  => __( 'Matching', 'nanosteam_8n' ),
		'name'   => 'nano_fifty_grant_apply',
		'id'     => 'yes',
		'type'   => 'radio',
		'value'  => 'yes',
		'wclass' => 'custom-control custom-radio',
		'class'  => 'custom-control-input',
		'misc'   => ( 'yes' == $nano_fifty_grant ? 'checked="checked"' : '' ) . ' required',
		'before' => '<div class="form-group form-row pb-3"><div class="col-sm">',
	);

	return $field;
}

function nano_fifty_grant_apply_no() {

	$post_id          = isset( $_GET['edit_project'] ) ? $_GET['edit_project'] : '';
	$nano_fifty_grant = isset( $post_id ) ? get_post_meta( $post_id, 'nano_fifty_grant_apply', true ) : '';
	$field            = array(
		'label'  => __( 'All-or-nothing', 'nanosteam_8n' ),
		'name'   => 'nano_fifty_grant_apply',
		'id'     => 'no',
		'type'   => 'radio',
		'value'  => 'no',
		'class'  => 'custom-control-input',
		'wclass' => 'custom-control custom-radio',
		'misc'   => ( 'no' == $nano_fifty_grant || '' == $nano_fifty_grant ? 'checked="checked"' : '' ) . ' required',
		'after'  => '</div></div>',
	);

	return $field;
}

function nano_funding_amount( $vars ) {

	$field = array(
		'label'       => __( 'Funding Goal Amount', 'nanosteam_8n' ),
		'value'       => ( isset( $vars['project_goal'] ) && '' != $vars['project_goal'] ? number_format( $vars['project_goal'], 0, '.', '' ) : '' ),
		'name'        => 'project_goal',
		'id'          => 'project_goal',
		'type'        => 'text',
		'class'       => 'required',
		'wclass'      => 'col',
		'misc'        => ( isset( $_GET['edit_project'] ) && '' == ( isset( $vars['status'] ) && 'PUBLISH' == strtoupper( $vars['status'] ) ? '' : 'PUBLISH' ) ? 'disabled' : '' ) . ' placeholder="500" required',
		'heading'     => __( 'Funding Goal Amount', 'nanosteam_8n' ),
		'help'        => nano_help_field( 'funding_goal_amount' ),
		'description' => nano_description_field( 'funding_goal_amount' ),
		'before'      => '<div class="form-group form-row pb-3">',
		'after'       => '</div>',
	);

	return $field;
}

function nano_funding_date_select( $vars ) {

	$date_format = get_option( 'date_format' );
	$start_date  = isset( $vars['project_start'] ) ? date( $date_format, strtotime( $vars['project_start'] ) ) : '';
	$end_date    = isset( $vars['project_start'] ) ? ' - ' . date( $date_format, strtotime( $vars['project_end'] ) ) : '';

	$field = array(
		'label'       => __( 'Date Select', 'nanosteam_8n' ),
		'value'       => $start_date . $end_date,
		'name'        => 'project_date_range',
		'id'          => 'project_date_range',
		'type'        => 'text',
		'class'       => 'required',
		'wclass'      => 'col input-group',
		'misc'        => ( isset( $_GET['edit_project'] ) && ( isset( $vars['status'] ) && ( strtoupper( $vars['status'] ) == 'DRAFT' || strtoupper( $vars['status'] ) == 'PENDING' ) ? 'draft' : '' ) == '' ? 'disabled' : '' ) . ' required',
		'heading'     => __( 'Funding Duration', 'nanosteam_8n' ),
		'help'        => nano_help_field( 'funding_duration' ),
		'description' => nano_description_field( 'funding_duration' ),
		'input_addon' => array( '<span class="input-group-text"><i class="fa fa-calendar"></i></span>', 'right' ),
		'before'      => '<div class="form-group form-row pb-3">',
		'after'       => '</div>',
	);

	return $field;
}

function nano_funding_start_date_field( $vars ) {

	$date_format = get_option( 'date_format' );

	$field = array(
		'label'  => __( 'Start Date', 'nanosteam_8n' ),
		'value'  => ( isset( $vars['project_start'] ) ? date( $date_format, strtotime( $vars['project_start'] ) ) : '' ),
		'name'   => 'project_start',
		'id'     => 'project_start',
		'type'   => 'hidden',
		'class'  => 'required',
		'wclass' => 'col-sm-4',
		'misc'   => '',
		'before' => '<div class="form-group form-row pb-3 d-none">',
	);

	return $field;
}

function nano_funding_end_date_field( $vars ) {

	$date_format = get_option( 'date_format' );

	$field = array(
		'label'  => __( 'End Date', 'nanosteam_8n' ),
		'value'  => ( isset( $vars['project_end'] ) ? date( $date_format, strtotime( $vars['project_end'] ) ) : '' ),
		'name'   => 'project_end',
		'id'     => 'project_end',
		'type'   => ( isset( $_GET['create_project'] ) || isset( $vars['status'] ) && strtoupper( $vars['status'] ) == 'DRAFT' ? 'hidden' : 'hidden' ),
		'class'  => 'required',
		'wclass' => 'col-sm-4',
		'misc'   => '',
		'after'  => '</div>',
	);

	return $field;
}

function id_funding_close_option_field( $vars ) {

	$close = array(
		'label'  => __( 'Close on End', 'nanosteam_8n' ),
		'name'   => 'project_end_type',
		'id'     => 'closed',
		'type'   => 'hidden',
		'value'  => 'closed',
		'wclass' => 'd-none',
		'misc'   => ( ( isset( $vars['project_end_type'] ) && 'closed' == $vars['project_end_type'] ) || ! isset( $vars['project_end_type'] ) ? 'checked="checked"' : '' ),
	);

	return $close;
}

function id_funding_open_option_field( $vars ) {

	$open = array(
		'label'  => __( 'Leave Open', 'nanosteam_8n' ),
		'name'   => 'project_end_type',
		'id'     => 'open',
		'type'   => 'hidden',
		'value'  => 'open',
		'wclass' => 'd-none',
		'misc'   => ( isset( $vars['project_end_type'] ) && 'open' == $vars['project_end_type'] ? 'checked="checked"' : '' ),
		'after'  => '</div></div>',
	);

	return $open;
}
