<?php if ( ! defined( 'ABSPATH' ) ) {
	die( 'Direct access forbidden.' );
}

$revolution_slider = isset( $_GET['post'] ) ? get_post_meta( $_GET['post'], 'nano_revolution_slider_alias', true ) : '';

if ( class_exists( 'RevSlider' ) ) {
	$rev_slider = new RevSlider();
	$sliders    = $rev_slider->getAllSliderAliases();
} else {
	$sliders = array();
}

$options = array( array ('value' => '', 'title' => 'Slider...', 'misc' => '' ) );
foreach ($sliders as $key => $slider) {
	if ( false === strpos( $slider,'tablet' ) && false === strpos( $slider,'mobile' ) ) {
		$options[$key + 1]['value'] = $slider;
		$options[$key + 1]['title'] = $slider;
		$options[$key + 1]['misc'] = $slider === $revolution_slider ? ' selected' : '';
	}
}

$nano_revolution_slider = array(
	array(
		'label'   => __( 'Revolution Slider', 'nanosteam_8n' ),
		'options' => $options,
		'name'    => 'nano_revolution_slider_alias',
		'id'      => 'nano_revolution_slider_alias',
		'type'    => 'select',
		'class'   => '',
		'wclass'  => 'col',
		'misc'    => 'placeholder="alias-name"',
		'heading' => __( 'If you would like to position a slider at the top of the page and for it to be full width select it here', 'nanosteam_8n' ),
	),
);


$form_payout = new NANO_Form( $nano_revolution_slider );

echo apply_filters( 'nano_revolution_slider', $form_payout->build_form() );
