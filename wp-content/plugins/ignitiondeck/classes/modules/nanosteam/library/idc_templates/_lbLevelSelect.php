<?php

$project_id = get_post_meta( $post->ID, 'ign_project_id', true );
$deck       = new Deck( $project_id );
$the_deck   = $deck->the_deck();

global $pwyw;
if ( 'default' == $the_deck->deck_type ) {
	$post_id = $the_deck->post_id;
	$image   = idc_checkout_image( $post_id );
	if ( isset( $the_deck->level_data ) ) {
		$level_data = $the_deck->level_data;
	} else {
		$level_data = null;
	}
	if ( ! empty( $level_data ) ) {
		$purchase_url = getPurchaseURLfromType( $project_id, 'purchaseform' );
		$action       = apply_filters( 'idcf_purchase_url', $purchase_url, $project_id );
		//$content = ob_get_contents();
		//ob_end_flush();
		//echo $content;
	}
}

// we need to hide/invalidate sold out levels
if ( isset( $level ) ) {
	$level_invalid = getLevelLimitReached( $project_id, $post_id, $level );
	if ( $level_invalid ) {
		$level = 0;
	}
}
$level_data = apply_filters( 'idcf_dropdown_level', $level_data, $project_id );
?>

<div class="modal idc_lightbox fade ignitiondeck" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Thank you for supporting "<?php the_title(); ?>"!</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form action="<?php echo( isset( $action ) ? $action : '' ); ?>" method="POST" name="idcf_level_select">
				<div class="modal-body pb-1">
					<img class="img-fluid" src="<?php echo $image; ?>">
					<div class="form-row inline left twothird">
						<label class="d-none"
								for="level_select"><?php _e( 'Contribution Level', 'ignitiondeck' ); ?></label>
						<span class="idc-dropdown">
						<select title="Level Select" id="idc-dropdown__select" name="level_select"
								class="idc-dropdown__select level_select d-none">
							<?php
							// would like to move this to the Deck class
							$i = 0;
							foreach ( $level_data as $level ) {
								if ( empty( $level->level_invalid ) || ! $level->level_invalid ) {
									echo '<option value="' . $level->id . '" data-price="' . ( isset( $level->meta_price ) ? $level->meta_price : '' ) . '" data-desc="' . $level->meta_short_desc . '" data-order="' . ( ! empty( $level->meta_order ) ? $level->meta_order : $i ) . '" ' . apply_filters( 'idcf_dropdown_option_attributes', '', $level ) . '>' . $level->meta_title . '</option>';
								}
								$i ++;
							}
							?>
						</select>
					</span>
					</div>
					<div class="form-row form-group total pt-3">
						<div class="col">
							<label for="total"><?php _e( 'How much would you like to contribute?', 'ignitiondeck' ); ?></label>
							<!-- <div class="id-currency-symbol"><?php echo apply_filters( 'id_lightbox_currency_symbol', $the_deck->cCode, $post_id, $the_deck ); ?></div> -->
							<?php if ( isset( $pwyw ) && $pwyw ) { ?>
								<div class="input-group mb-2 mb-sm-0 mt-1">
									<div class="input-group-prepend"><i class="fa fa-dollar input-group-text"></i></div>
									<input type="text" class="value form-control-lg form-control px-3"
											name="value" id="value" value=""
											placeholder="<?php echo apply_filters( 'id_price_format', ( isset( $level_data[0] ) ? $level_data[0]->meta_price : 0 ), $post_id ); ?>"/>
									<input type="hidden" class="total form-control-lg form-control"
											name="total" id="total" value=""/>
								</div>
								<div id="errmsg" class="small text-danger strong"></div>
							<?php } else { ?>
								<span class="total" data-value=""></span>
							<?php } ?>
						</div>
					</div>
					<div class="form-hidden">
						<input type="hidden" name="project_id" value="<?php echo $project_id; ?>"/>
					</div>
					<div id="extra_fields" class="form-row">
						<?php echo do_action( 'md_purchase_extrafields' ); ?>
					</div>
				</div>
				<div class="modal-footer submit">
					<input type="submit" name="lb_level_submit" class="btn btn-primary lb_level_submit"
							value="<?php _e( 'Next Step', 'ignitiondeck' ); ?>"/>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>

<?php
