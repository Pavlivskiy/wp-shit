<div class="memberdeck checkout-wrapper row">
	<div class="col-sm-12 mb-3 d-none d-lg-block">
		<div class="card-deck">
			<div class="card col-lg-7 m-0">
				<div class="card-body border border-primary ">
					<div class="row">
						<div class="col-6">
							<p class="lead"><?php _e( 'Nanosteam Project', 'memberdeck' ); ?></p>
						</div>
						<div class="col-6">
							<?php
							// Original project ID
							$supported_project_id = isset( $_SESSION['nano_project_id'] ) ? $_SESSION['nano_project_id'] : ( isset( $_GET['mdid_checkout'] ) ? $_GET['mdid_checkout'] : '' );
							$project              = new ID_Project( $supported_project_id );
							$post_id              = $project->get_project_postid();
							$project_title        = get_the_title( $post_id );

							if ( isset( $project_title ) && '' != $project_title && isset( $_GET['purchaseform'] ) ) {
								echo '<p class="lead">' . $project_title . '</p>';
							}
							?>
						</div>
					</div>
					<div class="row">
						<div class="col-6">
							<p class="lead"><?php _e( 'Donation', 'memberdeck' ); ?></p>
						</div>
						<div class="col-6">
							<p class="lead"><span class="currency-symbol"><?php echo $global_currency_symbol; ?><span
											class=""><?php echo $info_price; ?></span></span></p>
						</div>
					</div>
					<div class="row">
						<div class="col-6">
							<p class="lead mb-0"><?php _e( 'Total Amount', 'memberdeck' ); ?></p>
						</div>
						<div class="col-6">
							<p class="lead mb-0"><span class="total-symbol"><?php echo $global_currency_symbol; ?><span
											class="total-price"><?php echo $info_price; ?></span></span></p>
						</div>
					</div>
				</div>
			</div>
			<div class="card col-lg-5 m-0">
				<div class="card-body bg-light border">
					<h5 class="strong"><span><i class="fa fa-lock mr-2"></i></span>Payment Method</h5>
					<p>Your payment information is securely and safely stored using Stripe, our encrypted payment provider.</p>
					<p class="mb-0">Your donation is tax deductible.</p>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-12 mb-3 d-block d-lg-none">
			<div class="border border-primary col-12 mb-3">
				<div class="p-2 ps-5">
					<div class="row">
						<div class="col-6">
							<p class="lead"><?php _e( 'Nanosteam Project', 'memberdeck' ); ?></p>
						</div>
						<div class="col-6">
							<?php
							// Original project ID
							$supported_project_id = isset( $_SESSION['nano_project_id'] ) ? $_SESSION['nano_project_id'] : ( isset( $_GET['mdid_checkout'] ) ? $_GET['mdid_checkout'] : '' );
							$project              = new ID_Project( $supported_project_id );
							$post_id              = $project->get_project_postid();
							$project_title        = get_the_title( $post_id );

							if ( isset( $project_title ) && '' != $project_title && isset( $_GET['purchaseform'] ) ) {
								echo '<p class="lead">' . $project_title . '</p>';
							}
							?>
						</div>
					</div>
					<div class="row">
						<div class="col-6">
							<p class="lead"><?php _e( 'Donation', 'memberdeck' ); ?></p>
						</div>
						<div class="col-6">
							<p class="lead"><span class="currency-symbol"><?php echo $global_currency_symbol; ?><span
											class=""><?php echo $info_price; ?></span></span></p>
						</div>
					</div>
					<div class="row">
						<div class="col-6">
							<p class="lead mb-0"><?php _e( 'Total Amount', 'memberdeck' ); ?></p>
						</div>
						<div class="col-6">
							<p class="lead mb-0"><span class="total-symbol"><?php echo $global_currency_symbol; ?><span
											class="total-price"><?php echo $info_price; ?></span></span></p>
						</div>
					</div>
				</div>
			</div>
			<div class="bg-light border col-lg-5 col-12">
				<div class="p-2 ps-5">
					<h5 class="strong"><span><i class="fa fa-lock mr-2"></i></span>Payment Method</h5>
					<p>Your payment information is securely and safely stored using Stripe, our encrypted payment provider.</p>
					<p class="mb-0">Your donation is tax deductible.</p>
				</div>
			</div>
	</div>
	<div class="col-lg-7">
		<div class="tooltip-text">
			<?php //TODO : Changed path ?>
			<?php include_once( IDC_PATH . 'templates/_checkoutTooltip.php' ); ?>
		</div>
		<form action="" method="POST" id="payment-form" data-currency-code="<?php echo $pp_currency; ?>"
				data-product="<?php echo( isset( $product_id ) ? $product_id : '' ); ?>"
				data-type="<?php echo( isset( $type ) ? $type : '' ); ?>" <?php echo( isset( $type ) && 'recurring' == $type ? 'data-recurring="' . $recurring . '"' : '' ); ?>
				data-free="<?php echo( 0 == $level_price ? 'free' : 'premium' ); ?>"
				data-txn-type="<?php echo( isset( $txn_type ) ? $txn_type : 'capture' ); ?>"
				data-renewable="<?php echo( isset( $renewable ) ? $renewable : 0 ); ?>"
				data-limit-term="<?php echo( isset( $type ) && 'recurring' == $type ? $limit_term : 0 ); ?>"
				data-term-limit="<?php echo( isset( $limit_term ) && $limit_term ? $term_length : '' ); ?>"
				data-scpk="<?php echo( isset( $sc_pubkey ) ? apply_filters( 'idc_sc_pubkey', $sc_pubkey ) : '' ); ?>"
				data-claimedpp="<?php echo( isset( $claimed_paypal ) ? apply_filters( 'idc_claimed_paypal', $claimed_paypal ) : '' ); ?>" <?php echo( ( isset( $es ) && 1 == $es && ! is_idc_free() ) || isset( $_GET['login_failure'] ) ? 'style="display: none;"' : '' ); ?>
				data-pay-by-credits="<?php echo( ( isset( $paybycrd ) && 1 == $paybycrd ) ? '1' : '' ); ?>"
				data-guest-checkout="<?php echo( $guest_checkout ); ?>">
			<h3 class="checkout-header"><?php /* echo (isset($level_name) ? $level_name : ''); ?> <?php _e('Checkout', 'memberdeck'); */ ?>
				<?php _e( 'Select Payment Method', 'memberdeck' ); ?></h3>
			<?php if ( '' !== $level_price && $level_price > 0 ) { ?>
				<div class="payment-type-selector row">
					<?php if ( isset( $epp ) && 1 == $epp ) { ?>
						<div class="col"><a id="pay-with-paypal" class="pay_selector btn btn-block px-4" href="#">
								<i class="fa fa-paypal"></i>
								<span><?php _e( 'Paypal', 'memberdeck' ); ?></span>
							</a></div>
					<?php } ?>
					<?php if ( isset( $eppadap ) && 1 == $eppadap && ! is_idc_free() ) { ?>
						<div><a id="pay-with-ppadaptive" class="pay_selector" href="#">
								<i class="fa fa-paypal"></i>
								<span><?php _e( 'PayPal', 'memberdeck' ); ?></span>
							</a></div>
					<?php } ?>
					<?php if ( isset( $es ) && 1 == $es && ! is_idc_free() ) { ?>
						<div class="col"><a id="pay-with-stripe" class="pay_selector btn btn-block px-4" href="#">
								<i class="fa fa-credit-card"></i>
								<span><?php _e( 'Credit Card', 'memberdeck' ); ?></span>
							</a></div>
					<?php } ?>
					<?php if ( isset( $efd ) && 1 == $efd && ! is_idc_free() ) { ?>
						<div><a id="pay-with-fd" class="pay_selector" href="#">
								<i class="fa fa-credit-card"></i>
								<span><?php _e( 'Credit Card', 'memberdeck' ); ?></span>
							</a></div>
					<?php } ?>
					<?php if ( isset( $eauthnet ) && 1 == $eauthnet && ! is_idc_free() && ! $guest_checkout ) { ?>
						<div><a id="pay-with-authorize" class="pay_selector" href="#">
								<i class="fa fa-credit-card"></i>
								<span><?php _e( 'Credit Card', 'memberdeck' ); ?></span>
							</a></div>
					<?php } ?>
					<?php do_action( 'idc_after_credit_card_selectors', $gateways ); ?>

					<?php if ( isset( $mc ) && 1 == $mc && ! is_idc_free() ) { ?>
						<div><a id="pay-with-mc" class="pay_selector" href="#">
								<i class="fa fa-power-off"></i>
								<span><?php _e( 'Offline Checkout', 'memberdeck' ); ?></span>
							</a></div>
					<?php } ?>
					<?php if ( isset( $paybycrd ) && 1 == $paybycrd && ! is_idc_free() ) { ?>
						<div class="col"><a id="pay-with-credits" class="pay_selector btn btn-block px-4" href="#">
								<i class="fa fa-usd"></i>
								<span><?php _e( ucwords( apply_filters( 'idc_credits_label', 'Credits', true ) ), 'memberdeck' ); ?></span>
							</a></div>
					<?php } ?>
					<?php if ( isset( $ecb ) && 1 == $ecb && ! is_idc_free() ) { ?>
						<div><a id="pay-with-coinbase" class="pay_selector" href="#">
								<i class="fa fa-btc"></i>
								<span><?php _e( 'Bitcoin', 'memberdeck' ); ?></span>
							</a></div>
					<?php } ?>
				</div>
			<?php } ?>
			<div class="confirm-screen" style="display:none;">
				<?php if ( ! is_user_logged_in() ) { ?>
					<span class="login-help"><a href="#"
								class="reveal-login"><?php _e( 'Already have an account?', 'memberdeck' ); ?></a></span>
					<div id="logged-input" class="no mt-3">
						<div class="row">
							<div class="col-sm-6 form-group formval">
								<label for="first-name"><?php _e( 'First Name', 'memberdeck' ); ?> <span
											class="starred">*</span></label>
								<input id="first-name" type="text" size="20" class="first-name form-control required"
										name="first-name"/>
							</div>
							<div class="col-sm-6 form-group formval">
								<label for="last-name"><?php _e( 'Last Name', 'memberdeck' ); ?> <span
											class="starred">*</span></label>
								<input id="last-name" type="text" size="20" class="last-name form-control required"
										name="last-name"/>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4 col-sm-12 form-group formval">
								<label for="email"><?php _e( 'Email Address', 'memberdeck' ); ?> <span
											class="starred">*</span></label>
								<input id="email" type="email" pattern="[^ @]*@[^ @]*" size="20"
										class="email form-control required" name="email" autocomplete="username email"/>
							</div>

							<?php if ( ! $guest_checkout ) { ?>
							<div class="col-md-4 col-6 form-group formval">
								<label for="pw"><?php _e( 'Password', 'memberdeck' ); ?> <span class="starred">*</span></label>
								<input id="pw" type="password" size="20" class="pw form-control required" name="pw" autocomplete="new-password"/>
							</div>
							<div class="col-md-4 col-6 form-group formval">
								<label for="cpw"><?php _e( 'Password', 'memberdeck' ); ?> <span
											class="starred">*</span></label>
								<input id="cpw" type="password" size="20" class="cpw form-control required" name="cpw" autocomplete="new-password"/>
							</div>
						</div>
						<?php } else { ?>
							<a href="#" class="reveal-account"><?php _e( 'Create an account', 'memberdeck' ); ?></a>
							<div id="create_account" class="row" style="display: none">
								<div class="col-sm-6 form-group formval">
									<label for="pw"><?php _e( 'Password', 'memberdeck' ); ?> <span
												class="starred">*</span></label>
									<input id="pw" type="password" size="20" class="pw form-control required"
											name="pw"/>
								</div>
								<div class="col-sm-6 form-group formval">
									<label for="cpw"><?php _e( 'Re-enter Password', 'memberdeck' ); ?> <span
												class="starred">*</span></label>
									<input id="cpw" type="password" size="20" class="cpw form-control required"
											name="cpw"/>
								</div>
							</div>
						<?php } ?>
					</div>
				<?php } else { ?>
					<div id="logged-input" class="yes row">
						<div class="col-sm-6 form-group formval" style="display: none;">
							<label for="first-name"><?php _e( 'First Name', 'memberdeck' ); ?> <span
										class="starred">*</span></label>
							<input id="first-name" type="text" size="20" class="first-name required" name="first-name"
									value="<?php echo( isset( $fname ) ? $fname : '' ); ?>"/>
						</div>
						<div class="col-sm-6 form-group formval" style="display: none;">
							<label for="last-name"><?php _e( 'Last Name', 'memberdeck' ); ?> <span
										class="starred">*</span></label>
							<input id="last-name" type="text" size="20" class="last-name required" name="last-name"
									value="<?php echo( isset( $lname ) ? $lname : '' ); ?>"/>
						</div>
						<div class="col-sm-12 form-group formval" style="display: none;">
							<label for="email"><?php _e( 'Email Address', 'memberdeck' ); ?> <span
										class="starred">*</span></label>
							<input id="email" type="email" pattern="[^ @]*@[^ @]*" size="20" class="email required"
									name="email" autocomplete="username email"
									value="<?php echo( isset( $email ) ? $email : '' ); ?>"/>
						</div>
					</div>
				<?php } ?>
			</div> <!-- confirm screen -->
			<div id="stripe-input" class="row mt-4 fadein"
					data-idset="<?php echo( isset( $instant_checkout ) && true == $instant_checkout ? true : false ); ?>"
					data-symbol="<?php echo( isset( $stripe_symbol ) ? $stripe_symbol : '' ); ?>"
					data-customer-id="<?php echo( ( isset( $customer_id ) && ! empty( $customer_id ) ) ? $customer_id : '' ); ?>"
					style="display:none;">
				<div class="col-sm-12">
					<h3 class="checkout-header"><?php _e( 'Credit Card Info', 'memberdeck' ); ?></h3>
				</div>

				<div class="form-group formval col-sm-12">
					<label for="card-number" class="w-100"><?php _e( 'Card Number', 'memberdeck' ); ?> <span
								class="starred">*</span>
						<span class="cards pull-right"><img
									src="<?php echo get_stylesheet_directory_uri(); ?>/images/creditcards-full2.png"
									alt="<?php _e( 'Credit Cards Accepted', 'memberdeck' ); ?>"/></span></label>
					<input id="card-number" type="text" size="20" name="nano_cc" autocomplete="off"
							class="card-number required form-control"/><span
							class="error-info"
							style="display:none;"><?php _e( 'Incorrect Number', 'memberdeck' ); ?></span>
				</div>

				<div class="form-group formval col-md-6 col-12 date mb-3">
					<div class="row">
						<div class="col-sm-12">
							<label class="clearfix"><?php _e( 'Expiration', 'memberdeck' ); ?><span
										class="starred">*</span> <small>(MM / YYYY)</small></label>
						</div>
						<div class="col-5">
							<input id="month" type="text" size="2" placeholder="MM" maxlength="2" name="nano_mm"
									class="card-expiry-month required form-control px-3"/>
						</div>
						<div class="col-1 p-2">
							<span>/</span>
						</div>
						<div class="col col-6">
							<input id="year" type="text" size="4" placeholder="YYYY" maxlength="4" name="nano_yy"
									class="card-expiry-year required form-control px-3"/>
						</div>
					</div>
				</div>

				<div class="form-group formval col">
					<label for="card-cvc"><?php _e( 'CVC', 'memberdeck' ); ?> <span class="starred">*</span></label>
					<input id="card-cvc" type="text" size="4" maxlength="4" autocomplete="off" name="nano_cvc"
							class="card-cvc required form-control"/><span class="error-info"
							style="display:none;"><?php _e( 'CVC number required', 'memberdeck' ); ?></span>
				</div>

				<div class="form-group formval col">
					<?php if ( 1 == $es ) { ?>
						<label for="card-error"><?php _e( 'Zip Code', 'memberdeck' ); ?> <span class="starred">*</span></label>
						<input id="card-error" type="text" size="20" autocomplete="off" name="nano_zip"
								class="zip-code required form-control"/><span
								class="error-info"
								style="display:none;"><?php _e( 'Invalid Zip code', 'memberdeck' ); ?></span>
					<?php } ?>
				</div>

			</div>

			<div class="row mt-2 nano_instant_checkout d-none">
				<?php if ( is_user_logged_in() ) { ?>
				<div class="col-sm-12">
					<?php
					$user_id = wp_get_current_user()->ID;
					$user_credentials = nano_get_stripe_customer_params( $user_id );
					$card_number = isset($user_credentials->sources->data{0}->last4) ? $user_credentials->sources->data{0}->last4 : '****';
					wp_nonce_field( 'nano_instant_checkout_secure', 'nano_instant_checkout_security' );
					$show_icc         = allow_instant_checkout();
					$instant_checkout = instant_checkout();
						?>
					<div class="input-group">
						<div id="instant" class="custom-control custom-checkbox custom-control-inline">
							<input type="checkbox" class="instant_checkout custom-control-input"
									id="instant_checkout"
									name="instant_checkout" <?php echo( isset( $instant_checkout ) && 1 == $instant_checkout ? 'checked="checked"' : '' ); ?>
									value="1"/>
							<label class="custom-control-label <?php echo( isset( $instant_checkout ) && 0 == $instant_checkout ? ' new' : '' )?>"
									for="instant_checkout">
								<?php
								if ( '0' == $instant_checkout ) {
									printf(  esc_html__( 'Remember this card for future donations', 'nanosteam_8n' ));
								} else {
									printf( esc_html__( 'Use the same card ending in %s I used last time', 'nanosteam_8n' ), $card_number );
								}
								?>
							</label>
						</div>
					</div>
				</div>
				<?php } ?>
				<div class="col-sm-12">
					<?php nano_nonce(); ?>
				</div>
			</div>

			<div id="extra_fields" class="row">
				<div class="col">
					<?php echo mdid_project_fields(); ?>
					<input title="Anonymous Checkout" type="checkbox" class="d-none" name="anonymous_checkout"
							id="anonymous_checkout"
							value="1" <?php echo( isset( $_POST['anonymous_checkout'] ) ? 'checked' : '' ); ?>>
					<input title="Checkout Comments" type="text" size="20" maxlength="140" class="d-none"
							name="idc_checkout_comments"
							id="idc_checkout_comments">
				</div>
			</div>

			<div><?php echo apply_filters( 'md_purchase_footer', '' ); ?></div>
			<input id="reg-price" type="hidden" name="reg-price"
					value="<?php echo( isset( $return->level_price ) ? $return->level_price : '' ); ?>"/>
			<input id="pwyw-proice" type="hidden" name="pwyw-price"
					value="<?php echo( isset( $pwyw_price ) && $pwyw_price > 0 ? $pwyw_price : '' ); ?>"/>
			<?php if ( isset( $upgrade_level ) && $upgrade_level ) { ?>
				<input id="upgrade-level-price" type="hidden" name="upgrade-level-price"
						value="<?php echo( isset( $level_price ) && $level_price > 0 ? $level_price : '' ); ?>"/>
			<?php } ?>
			<div class="checkout-terms-wrapper row mt-3">
				<div class="col-sm-12 mb-2">
					<div class="main-submit-wrapper" style="display:none;">
						<button type="submit" id="id-main-submit"
								class="submit-button btn-block btn btn-primary"><?php _e( 'Give', 'nanosteam_8n' ); ?></button>
					</div>
				</div>
				<?php if ( 1 == $general['show_terms'] && ( isset( $terms_content->post_title ) || isset( $privacy_content->post_title ) ) ) { ?>
					<div class="idc-terms-checkbox col-sm-12" style="display:none;">
						<div class="checklist">
							<input title="Terms" id="terms-checkbox-input" type="checkbox"
									class="terms-checkbox-input required d-none" checked>
							<label><?php _e( 'By giving you agree to the Nanosteam ', 'memberdeck' ); ?>
								<?php if ( isset( $terms_content->post_title ) ) { ?>
									<span class="link-terms-conditions"><a
												href="#"><?php echo $terms_content->post_title; ?></a></span>
								<?php } ?>
								<?php if ( isset( $privacy_content->post_title ) ) { ?>
									<?php echo( ( isset( $terms_content->post_title ) ) ? '&amp;' : '' ); ?>
									<span class="link-privacy-policy"><a
												href="#"><?php echo $privacy_content->post_title; ?></a></span>
								<?php } ?>
							</label>
							<input type="hidden" id="idc-hdn-error-terms-privacy"
									value="<?php echo( isset( $terms_content ) ? $terms_content->post_title : '' ); ?> &amp; <?php echo( isset( $privacy_content ) ? $privacy_content->post_title : '' ); ?>"/>
						</div>
					</div>
				<?php } ?>
				<div class="col-sm-12">
					<?php echo apply_filters( 'idc_checkout_descriptions', '', $return, $level_price, ( isset( $user_data ) ? $user_data : '' ), $gateways, $general, $credit_value ); ?>
				</div>
			</div>
		</form>


		<?php if ( 1 == $general['show_terms'] ) { ?>
			<div class="idc-terms-conditions idc_lightbox mfp-hide">
				<div class="idc_lightbox_wrapper">
					<?php echo( isset( $terms_content ) ? wpautop( $terms_content->post_content ) : '' ); ?>
				</div>
			</div>
			<div class="idc-privacy-policy idc_lightbox mfp-hide">
				<div class="idc_lightbox_wrapper">
					<?php echo( isset( $privacy_content ) ? wpautop( $privacy_content->post_content ) : '' ); ?>
				</div>
			</div>
		<?php } ?>
	</div>
</div>
<?php if ( ! isset( $_GET['login_failure'] ) ) { ?>
	<!--
		The easiest way to indicate that the form requires JavaScript is to show
		the form with JavaScript (otherwise it will not render). You can add a
		helpful message in a noscript to indicate that users should enable JS.
	-->
	<script>
		if (window.Stripe) jQuery("#payment-form").show();
	</script>
	<noscript><p><?php _e( 'JavaScript is required for the purchase form', 'memberdeck' ); ?>.</p></noscript>
<?php } ?>
<div id="ppload"></div>
<?php if ( isset( $ecb ) && 1 == $ecb && ! is_idc_free() ) { ?>
	<div id="coinbaseload" data-button-loaded="no" style="display:none;">
		<input type="hidden" name="id_coinbase_button_code" id="id_coinbase_button_code" value=""/>
	</div>
<?php } ?>
<?php
if ( isset( $eppadap ) && 1 == $eppadap && ! is_idc_free() ) {
	// For lightbox
	echo '<script src="https://www.paypalobjects.com/js/external/dg.js"></script>';
	// For mini browser
	echo '<script src="https://www.paypalobjects.com/js/external/apdg.js"></script>';
}
