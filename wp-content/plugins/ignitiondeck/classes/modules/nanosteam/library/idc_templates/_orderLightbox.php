<?php
$general = get_option( 'md_receipt_settings' );
$general = maybe_unserialize( $general );

if ( ( isset( $status ) && 'completed' == $status ) || ! isset( $status ) ) {
	$name = apply_filters( 'idc_order_level_title', ( isset( $order_level_key ) ? $levels[ $order_level_key ]->level_name : $level->level_name ), $last_order );
} else {
	$name = ( isset( $order_level_key ) ? $levels[ $order_level_key ]->level_name : $level->level_name ) ;
}

$sc_settings = get_option( 'md_sc_settings' );
$order_amt   = idc_get_order_meta( $last_order->id, 'pwyw_price', true );
$stripe_amt  = idc_get_order_meta( $last_order->id, 'stripe_fee', true );
$stripe_amt  = $stripe_amt / 100; // Converting the stripe raw amt back to decimals.

if ( ! empty( $sc_settings ) ) {

	$fee = null;

	if ( ! is_array( $sc_settings ) ) {
		$sc_settings = unserialize( $sc_settings );
	}
	if ( is_array( $sc_settings ) ) {
		$app_fee  = apply_filters( 'idc_app_fee', $sc_settings['app_fee'], '' );
		$fee_type = apply_filters( 'idc_fee_type', $sc_settings['fee_type'], '' );
		$fee      = apply_filters( 'nano_idc_fee_amount', $app_fee, $order_amt, $fee_type, 'stripe' );
	}

}


if ( 'credit' === $last_order->transaction_id ) {

	if ( null === idc_get_order_meta( $last_order->id, 'stripe_fee', true ) ) {

		$stripe_amt = nano_add_stripe_fee_to_credit_order( $last_order, $current_user, true );

	}

}


$nanosteam_fee   = '$' . number_format( $fee, 2, '.', '' );
$stripe_fee      = '$' . number_format( $stripe_amt, 2, '.', '' );
$order_with_fees = ( 'credit' !== $last_order->transaction_id ? '$' : 'Credits ' ) . number_format( ( $order_amt - $stripe_amt - $fee ), 2, '.', '' );
$price           = ( 'credit' !== $last_order->transaction_id ? '$' : 'Credits ' ) . number_format( $order_amt, 2, '.', '' );

?>

<?php do_action( 'idc_order_lightbox_before', $last_order ); ?>
	<!-- Modal -->
	<div class="modal fade d-print-none nano-receipt"
			id="nano_order_<?php echo( ( isset( $last_order->id ) ) ? $last_order->id : 0 ); ?>"
			tabindex="-1" role="dialog" aria-labelledby="LongTitle" aria-hidden="true">
		<div class="modal-dialog" style="max-width: 800px;" role="document">
			<div class="modal-content">
				<div class="modal-header border-bottom-0 pb-0">
					<h5><?php _e( 'Thank you for supporting', 'memberdeck' ); ?> "<?php echo $name; ?>"</h5>
					<button type="button" class="close printNone" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body pt-0">
					<hr>
					<p><?php echo apply_filters( 'idc_company_name', $general['coname'] ); ?><br><?php echo home_url(); ?></p>
					<p><?php echo $current_user->user_firstname . ' ' . $current_user->user_lastname; ?></p>
					<div class="px-sm-5 px-1">
						<table class="table table-sm">
							<tr>
								<td colspan="2" class="border-top-0"><h5><?php _e( 'Your order details', 'memberdeck' ); ?></h5></td>
							</tr>
							<tr>
								<td class="pb-5"><span class="order"><?php _e( 'Order', 'memberdeck' ); ?></span>
									#100<?php echo( ( isset( $last_order->id ) ) ? $last_order->id : 0 ); ?></td>
								<td class="pb-5 text-right">
									<?php if ( isset( $status ) && 'pending' == $status ) { ?>
										<h2><?php _e( 'Pending', 'memberdeck' ); ?></h2>
									<?php } ?>
									<?php echo( ( isset( $last_order->order_date ) ) ? date( 'm/d/Y', strtotime( $last_order->order_date ) ) : nano_get_formated_date_time_gmt( 'Y-m-d H:i:s' ) ); ?>
								</td>
							</tr>
							<tr>
								<td class="border-top-0"><h5><?php _e( 'Project', 'memberdeck' ); ?></h5></td>
								<td class="border-top-0 text-right"><h5><?php _e( 'Amount', 'memberdeck' ); ?></h5></td>
							</tr>
							<tr>
								<td><?php echo $name; ?></td>
								<td class="text-right"><?php echo $order_with_fees; ?></td>
							</tr>

							<tr>
								<td class="border-top-0"><?php _e( 'Credit Card Fees', 'memberdeck' ); ?></td>
								<td class="text-right border-top-0"><?php echo $stripe_fee; ?></td>
							</tr>

							<tr>
								<td class="border-top-0 pb-2"><?php _e( 'nanosteam Fees', 'memberdeck' ); ?></td>
								<td class="text-right border-top-0 pb-2"><?php echo $nanosteam_fee; ?></td>
							</tr>
							<tr>
								<td colspan="2"  class="text-right"><?php _e( 'TOTAL', 'memberdeck' ); ?>:<strong class="pl-2"><?php echo $price; ?></strong></td>
							</tr>
						<tr>
							<td colspan="2" class="border-top-0 pt-3"><?php _e( 'Email confirmation will be sent to your Inbox soon. Details about our fees can be found', 'memberdeck' ); ?> <a href="/fees">here</a>.</td>
						</tr>
					</table>
					</div>
					<div class="social-sharing-options-wrapper d-print-none">
						<?php do_action( 'idc_order_sharing_before', $last_order, $levels ); ?>
						<?php if ( ! empty( $last_order ) ) { ?>
							<div class="row pb-2">
								<div class="col-12 col-sm-8">
									<h5><?php _e( 'Tell your friends about it', 'memberdeck' ); ?>!</h5>
								</div>
								<div class="col text-sm-right">
									<?php krown_share_buttons( $post_id ); ?>
								</div>
							</div>
							<div class="friendlink">
								<?php echo $thumbnail;  ?>
								<?php if ( ( isset( $status ) && 'completed' == $status ) || ! isset( $status ) ) { ?>
									<div class="text"><?php _e( 'I just backed', 'memberdeck' ); ?> <?php echo apply_filters( 'idc_order_level_title', ( isset( $order_level_key ) ? $levels[ $order_level_key ]->level_name : $level->level_name ), $last_order ); ?>
										.<br/>
										<a href="<?php echo apply_filters( 'idc_order_level_url', home_url(), $last_order ); ?>"><?php echo apply_filters( 'idc_order_level_url', home_url(), $last_order ); ?></a>
									</div>
								<?php } ?>
							</div>
						<?php } ?>
						<?php do_action( 'idc_order_sharing_after', $last_order, $levels ); ?>
					</div>
					<?php do_action( 'idc_order_lightbox_after', $last_order ); ?>
					<?php save_user_geo_location(); ?>
				</div>
				<div class="modal-footer d-print-none">
					<?php echo ( !isset( $_GET['idc_orders'] ) ? '<a href="/dashboard/?idc_orders=1" class="btn btn-secondary px-3 px-sm-4">All payments</a>' : '' ) ?>
					<button type="button" class="btn btn-secondary px-3 px-sm-4" data-dismiss="modal">Close</button>
					<a href="javascript:window.print()"
							class="btn btn-primary px-3 px-sm-4"><?php _e( 'Print', 'memberdeck' ); ?></a>
				</div>
			</div>
		</div>
	</div>

	<div class="d-none d-print-block printSection pt-5 mt-5">
		<h5><?php _e( 'Thank you for supporting', 'memberdeck' ); ?> "<?php echo $name; ?>"</h5>
		<table class="table table-sm">
			<tr>
				<td><?php echo apply_filters( 'idc_company_name', $general['coname'] ); ?></td>
				<td class="right"><span class="order"><?php _e( 'Order', 'memberdeck' ); ?></span>
					#100<?php echo( ( isset( $last_order->id ) ) ? $last_order->id : 0 ); ?></td>
			</tr>
			<tr>
				<td class="detailtitle"><?php echo home_url(); ?></td>
				<td class="right dates">
					<?php if ( isset( $status ) && 'pending' == $status ) { ?>
						<h2><?php _e( 'Pending', 'memberdeck' ); ?></h2>
					<?php } ?>
					<?php echo( ( isset( $last_order->order_date ) ) ? date( 'm/d/Y', strtotime( $last_order->order_date ) ) : nano_get_formated_date_time_gmt( 'Y-m-d H:i:s' ) ); ?>
				</td>
			</tr>
			<tr>
				<td class="detailtitle"><?php echo $general['coemail']; ?></td>
				<td class="right dates"></td>
			</tr>
			<tr>
				<td class="customername"><?php echo $current_user->user_firstname . ' ' . $current_user->user_lastname; ?></td>
				<td class="right dates"></td>
			</tr>
			<tr>
				<td class="detailtitle"><?php _e( 'Your order details:', 'memberdeck' ); ?></td>
				<td></td>
			</tr>
			<tr>
				<td class="h5"><?php _e( 'Project', 'memberdeck' ); ?></td>
				<td class="h5"><?php _e( 'Amount', 'memberdeck' ); ?></td>
			</tr>
			<tr>
				<?php if ( ( isset( $status ) && 'completed' == $status ) || ! isset( $status ) ) { ?>
					<td>
						<?php echo apply_filters( 'idc_order_level_title', ( isset( $order_level_key ) ? $levels[ $order_level_key ]->level_name : $level->level_name ), $last_order ); ?></td>
				<?php } else { ?>
					<td><?php echo( isset( $order_level_key ) ? $levels[ $order_level_key ]->level_name : $level->level_name ); ?></td>
				<?php } ?>
				<td><?php echo $price; ?></td>
			</tr>
			<tr>
				<td class="text-right"><?php _e( 'TOTAL', 'memberdeck' ); ?>:</td>
				<td>
					<span class="currency"><strong><?php echo $price; ?></strong></span>
				</td>
			</tr>
			<tr>
				<td class="email"><?php _e( 'Email confirmation will be sent to your Inbox soon', 'memberdeck' ); ?>.</td>
				<td></td>
			</tr>
		</table>
	</div>

	<script>
		jQuery(document).ready(function ($) {
			$('#nano_order_<?php echo( ( isset( $last_order->id ) ) ? $last_order->id : 0 ); ?>').modal('show')
		});
	</script>

<?php
