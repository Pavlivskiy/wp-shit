<?php if ( ! defined( 'ABSPATH' ) ) {
	die( 'Direct access forbidden.' );
}

/* ------------------------
 * Class for administration tables
 *
------------------------------*/

if ( is_admin() ) {
	new Paulund_Wp_List_Table();
}

/**
 * Paulund_Wp_List_Table class will create the page to load the table
 */
class Paulund_Wp_List_Table {
	/**
	 * Constructor will create the menu item
	 */
	public function __construct() {
		add_action( 'admin_menu', array( $this, 'add_menu_example_list_table_page' ) );
	}

	/**
	 * Menu item will allow us to load the page to display the table
	 */
	public function add_menu_example_list_table_page() {
		add_menu_page( null, 'Nanosteam', 'manage_options', 'project_tracking', '' );
		//add_submenu_page( null, 'Nanosteam', 'Nanosteam', 'manage_options', 'nanosteam', '');
		add_submenu_page( 'project_tracking', 'Project Tracking', 'Project Tracking', 'manage_options', 'project_tracking', array(
			$this,
			'list_project_table_page',
		) );
		add_submenu_page( 'project_tracking', 'Credits Tracking', 'Credits Tracking', 'manage_options', 'token_tracking', array(
			$this,
			'list_table_page',
		) );
		add_submenu_page( 'project_tracking', 'Payouts', 'Payouts', 'manage_options', 'pending_payouts', array(
			$this,
			'list_pending_table_page',
		) );
		add_submenu_page( 'project_tracking', 'All Donations Received', 'Donations Received', 'manage_options', 'donations_received', array(
			$this,
			'all_donations_r_table_page',
		) );
		add_submenu_page( 'project_tracking', 'All Donations Paid-Out', 'Donations Paid', 'manage_options', 'donations_paid', array(
			$this,
			'all_donations_p_table_page',
		) );
		add_submenu_page( 'project_tracking', 'Orders', 'Orders', 'manage_options', 'nano_idc_orders', array(
			$this,
			'nano_idc_orders_table',
		) );


	}

	/**
	 * Display the list table page
	 *
	 * @return Void
	 */
	public function list_table_page() {
		$token_tracking = new Token_List_Table();
		$token_tracking->prepare_items();
		?>
		<div class="wrap responsive">
			<div id="icon-users" class="icon32"></div>
			<h2>Refunded Credits Tracker</h2>
			<?php $token_tracking->display(); ?>
		</div>
		<?php
	}

	/**
	 * Display the list table page
	 *
	 * @return Void
	 */
	public function list_project_table_page() {
		$token_tracking = new Token_List_Table();
		$token_tracking->prepare_items();
		?>
		<div class="wrap responsive">
			<div id="icon-users" class="icon32"></div>
			<h2>Project Tracking</h2>
			<?php $token_tracking->display(); ?>
		</div>
		<?php
	}

	/**
	 * Display the list table page
	 *
	 * @return Void
	 */
	public function all_donations_r_table_page() {
		$token_tracking = new Token_List_Table();
		$token_tracking->prepare_items();
		?>
		<div class="wrap responsive">
			<div id="icon-users" class="icon32"></div>
			<h2>All Donations Received</h2>
			<?php $token_tracking->display(); ?>
		</div>
		<?php
	}

	/**
	 * Display the list table page
	 *
	 * @return Void
	 */
	public function all_donations_p_table_page() {
		$token_tracking = new Token_List_Table();
		$token_tracking->prepare_items();
		?>
		<div class="wrap responsive">
			<div id="icon-users" class="icon32"></div>
			<h2>All Donations Paid-Out</h2>
			<?php $token_tracking->display(); ?>
		</div>
		<?php
	}

	/**
	 * Display the list table page
	 *
	 * @return Void
	 */
	public function list_pending_table_page() {
		$token_tracking = new Token_List_Table();
		$token_tracking->prepare_items();
		$title = isset( $_POST['payout_status'] ) && 'paid' == $_POST['payout_status'] ? 'Paid Payouts' : 'Pending Payouts';
		?>
		<div class="wrap responsive">
			<div id="icon-users" class="icon32"></div>
			<h2><?php echo $title; ?></h2>
			<?php $token_tracking->display(); ?>
		</div>
		<?php
	}


	/**
	 * Display our own orders page
	 *
	 * @return Void
	 */
	public function nano_idc_orders_table() {
		$token_tracking = new Token_List_Table();
		$token_tracking->prepare_items();
		?>
		<div class="wrap responsive">
			<div id="icon-users" class="icon32"></div>
			<h2>Orders</h2>
			<?php $token_tracking->display(); ?>
		</div>
		<?php
	}
}

// WP_List_Table is not loaded automatically so we need to load it in our application
if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

/**
 * Create a new table class that will extend the WP_List_Table
 */
class Token_List_Table extends WP_List_Table {
	/**
	 * Prepare the items for the table to process
	 *
	 * @return Void
	 */
	public function prepare_items() {
		$columns  = $this->get_columns();
		$hidden   = $this->get_hidden_columns();
		$sortable = $this->get_sortable_columns();
		$data     = $this->table_data();
		usort( $data, array( &$this, 'sort_data' ) );
		$perPage     = count( $data );
		$currentPage = $this->get_pagenum();
		$totalItems  = count( $data );
		$this->set_pagination_args( array(
			'total_items' => $totalItems,
			'per_page'    => $totalItems
		) );
		$data                  = array_slice( $data, ( ( $currentPage - 1 ) * $perPage ), $perPage );
		$this->_column_headers = array( $columns, $hidden, $sortable );
		$this->items           = $data;
	}

	/**
	 * Add extra markup in the toolbars before or after the list
	 *
	 * @param string $which , helps you decide if you add the markup after (bottom) or before (top) the list
	 */
	function extra_tablenav( $which ) {

		if ( isset( $_GET['page'] ) && 'token_tracking' == $_GET['page'] ) {

			$matching_check  = '';
			$all_in_check    = '';
			$challenge_check = '';

			// Tokens
			if ( isset( $_POST['submit'] ) && 'filter' == $_POST['submit'] ) {
				$today           = $_POST['date_to'];
				$back            = $_POST['date_from'];
				$matching_check  = 'M' == $_POST['project_type'] ? 'selected="selected"' : '';
				$all_in_check    = 'A' == $_POST['project_type'] ? 'selected="selected"' : '';
				$challenge_check = 'C' == $_POST['project_type'] ? 'selected="selected"' : '';
				set_transient( 'token_tracking_back', $back, 60 * 10 );
				set_transient( 'token_tracking_today', $today, 60 * 10 );
				set_transient( 'token_tracking_type', $_POST['project_type'], 60 * 10 );
			} elseif ( isset( $_GET['orderby'] ) ) {
				$today           = '' !== get_transient( 'token_tracking_today' ) && false !== get_transient( 'token_tracking_today' ) ? get_transient( 'token_tracking_today' ) : nano_get_formated_date_time_gmt( 'm/d/Y' );
				$back            = '' !== get_transient( 'token_tracking_back' ) && false !== get_transient( 'token_tracking_back' ) ? get_transient( 'token_tracking_back' ) : date( 'm/d/Y', strtotime( '-30 days', strtotime( $today ) ) );
				$matching_check  = '' !== get_transient( 'token_tracking_type' ) && 'M' === get_transient( 'token_tracking_type' ) ? 'selected="selected"' : '';
				$all_in_check    = '' !== get_transient( 'token_tracking_type' ) && 'A' === get_transient( 'token_tracking_type' ) ? 'selected="selected"' : '';
				$challenge_check = '' !== get_transient( 'token_tracking_type' ) && 'C' === get_transient( 'token_tracking_type' ) ? 'selected="selected"' : '';
			} else {
				$today          = nano_get_formated_date_time_gmt( 'm/d/Y' );
				$back           = date( 'm/d/Y', strtotime( '-30 days', strtotime( $today ) ) );
				$matching_check = '';
				$all_in_check   = 'selected="selected"';
			}

			$search = @$_POST['s'] ? esc_attr( $_POST['s'] ) : "";
			if ( 'top' == $which ) {
				?>
				<form method="post" id="token_data">
					<div class="actions alignleft curtime" style="margin-left: -8px">
				<span>
					<strong>Refunded Date</strong>
					<input type="text" id="token_date_range"
						name="token_date_range"
						class="form-control form-control-sm"
						value="<?php echo $back; ?> - <?php echo $today; ?>"
						required="" aria-label="Date Select"
						data-fv-field="token_date_range">
					<input type="hidden" name="date_from" id="token_start" placeholder="Date From"
						value="<?php echo $back; ?>">
					<input type="hidden" name="date_to" id="token_end" placeholder="Date To"
						value="<?php echo $today; ?>">
				</span>
						<span>
					<strong>Project Type: </strong>
						<select name="project_type">
							<option value="A" <?php echo $all_in_check ?>>All or nothing</option>
							<option value="M" <?php echo $matching_check ?>>Matching</option>
							<option value="C" <?php echo $challenge_check ?>>Challenge</option>
						</select>
				</span>
						<span>
					<input type="submit" name="submit" id="submit" class="button button-primary" value="filter">
					<input type="submit" name="submit" id="token_export" class="button button-cancel"
						value="Export CSV">
				</span>
					</div>
				</form>
				<?php
			};
		} elseif ( isset( $_GET['page'] ) && 'project_tracking' == $_GET['page'] ) {

			$matching_check  = '';
			$all_in_check    = '';
			$challenge_check = '';

			// Tokens
			if ( isset( $_POST['submit'] ) && 'filter' == $_POST['submit'] ) {
				$today           = $_POST['date_to'];
				$back            = $_POST['date_from'];
				$matching_check  = 'M' == $_POST['project_type'] ? 'selected="selected"' : '';
				$all_in_check    = 'A' == $_POST['project_type'] ? 'selected="selected"' : '';
				$challenge_check = 'C' == $_POST['project_type'] ? 'selected="selected"' : '';
				set_transient( 'token_tracking_back', $back, 60 * 10 );
				set_transient( 'token_tracking_today', $today, 60 * 10 );
				set_transient( 'token_tracking_type', $_POST['project_type'], 60 * 10 );
			} elseif ( isset( $_GET['orderby'] ) ) {
				$today           = '' !== get_transient( 'token_tracking_today' ) && false !== get_transient( 'token_tracking_today' ) ? get_transient( 'token_tracking_today' ) : nano_get_formated_date_time_gmt( 'm/d/Y' );
				$back            = '' !== get_transient( 'token_tracking_back' ) && false !== get_transient( 'token_tracking_back' ) ? get_transient( 'token_tracking_back' ) : date( 'm/d/Y', strtotime( '-30 days', strtotime( $today ) ) );
				$matching_check  = '' !== get_transient( 'token_tracking_type' ) && 'M' === get_transient( 'token_tracking_type' ) ? 'selected="selected"' : '';
				$all_in_check    = '' !== get_transient( 'token_tracking_type' ) && 'A' === get_transient( 'token_tracking_type' ) ? 'selected="selected"' : '';
				$challenge_check = '' !== get_transient( 'token_tracking_type' ) && 'C' === get_transient( 'token_tracking_type' ) ? 'selected="selected"' : '';
			} else {
				$today          = nano_get_formated_date_time_gmt( 'm/d/Y' );
				$back           = date( 'm/d/Y', strtotime( '-30 days', strtotime( $today ) ) );
				$matching_check = '';
				$all_in_check   = 'selected="selected"';
			}

			if ( 'top' == $which ) { ?>
				<form method="post" id="token_data">
					<div class="actions alignleft curtime" style="margin-left: -8px">
						<span>
							<strong>Date Select</strong>
							<input type="text" id="token_date_range"
								name="token_date_range"
								class="form-control form-control-sm"
								value="<?php echo $back; ?> - <?php echo $today; ?>"
								required="" aria-label="Date Select"
								data-fv-field="token_date_range">
							<input type="hidden" name="date_from" id="token_start" placeholder="Date From"
								value="<?php echo $back; ?>">
							<input type="hidden" name="date_to" id="token_end" placeholder="Date To"
								value="<?php echo $today; ?>">
						</span>
						<span>
					<strong>Project Type: </strong>
						<select name="project_type">
							<option value="A" <?php echo $all_in_check ?>>All or nothing</option>
							<option value="M" <?php echo $matching_check ?>>Matching</option>
							<option value="C" <?php echo $challenge_check ?>>Challenge</option>
						</select>
				</span>
						<span>
							<input type="submit" name="submit" id="submit" class="button button-primary" value="filter">
							<input type="submit" name="submit" id="token_export" class="button button-cancel"
								value="Export CSV">
						</span>
					</div>
				</form>
				<?php
			}
		} elseif ( isset( $_GET['page'] ) && 'pending_payouts' == $_GET['page'] ) {

			// Tokens
			if ( isset( $_POST['submit'] ) && 'filter' == $_POST['submit'] ) {
				$project_status_paid    = 'paid' == $_POST['payout_status'] ? 'checked="checked"' : '';
				$project_status_pending = 'pending' == $_POST['payout_status'] ? 'checked="checked"' : '';
			} else {
				$project_status_paid    = '';
				$project_status_pending = 'checked="checked"';
			}

			if ( 'top' == $which ) { ?>
				<form method="post" id="token_data">
					<div class="actions alignleft curtime" style="margin-left: -8px">
						<span>
							<strong>Project Status: </strong>
							<span class="">Pending
								<input name="payout_status" id="nano_pending" type="radio" value="pending"
									class="tog" <?php echo $project_status_pending; ?>>
							</span>
							<span class="">Paid
								<input name="payout_status" id="matching" type="radio" value="paid"
									class="tog" <?php echo $project_status_paid; ?>>
							</span>
						</span>
						<span>
							<input type="submit" name="submit" id="submit" class="button button-primary" value="filter">
							<input type="submit" name="submit" id="token_export" class="button button-cancel"
								value="Export CSV">
						</span>
					</div>
				</form>
				<?php
			}
		} elseif ( isset( $_GET['page'] ) && ( 'donations_paid' == $_GET['page'] || 'donations_received' == $_GET['page'] ) ) {

			$matching_check  = '';
			$all_in_check    = '';
			$challenge_check = '';

			// Tokens
			if ( isset( $_POST['submit'] ) && 'filter' == $_POST['submit'] ) {
				$back            = $_POST['date_from'];
				set_transient( 'token_tracking_back', $back, 60 * 10 );
			} elseif ( isset( $_GET['orderby'] ) ) {
				$today           = '' !== get_transient( 'token_tracking_today' ) && false !== get_transient( 'token_tracking_today' ) ? get_transient( 'token_tracking_today' ) : nano_get_formated_date_time_gmt( 'm/d/Y' );
				$back            = '' !== get_transient( 'token_tracking_back' ) && false !== get_transient( 'token_tracking_back' ) ? get_transient( 'token_tracking_back' ) : date( 'm/d/Y', strtotime( '-30 days', strtotime( $today ) ) );
				$matching_check  = '' !== get_transient( 'token_tracking_type' ) && 'M' === get_transient( 'token_tracking_type' ) ? 'selected="selected"' : '';
				$all_in_check    = '' !== get_transient( 'token_tracking_type' ) && 'A' === get_transient( 'token_tracking_type' ) ? 'selected="selected"' : '';
				$challenge_check = '' !== get_transient( 'token_tracking_type' ) && 'C' === get_transient( 'token_tracking_type' ) ? 'selected="selected"' : '';
			} else {
				$today          = nano_get_formated_date_time_gmt( 'm/d/Y' );
				$back           = '2019';
				$matching_check = '';
				$all_in_check   = 'selected="selected"';
			}

			if ( 'top' == $which ) { ?>
				<form method="post" id="token_data">
					<div class="actions alignleft curtime" style="margin-left: -8px">
						<span>
							<select name="date_from">
							<?php
							$firstYear = 2017;
							$lastYear = (int)date('Y');
							for ( $i = $firstYear; $i <= $lastYear; $i ++ ) {
								echo '<option' . ( $i == $back ? ' selected ' : '' ) . ' value=' . $i . '>' . $i . '</option>';
							}
							?>
							</select>
						</span>
						<span>
							<input type="submit" name="submit" id="submit" class="button button-primary" value="filter">
							<input type="submit" name="submit" id="token_export" class="button button-cancel"
								value="Export CSV">
						</span>
					</div>
				</form>
				<?php
			}
		}
	}

	/**
	 * Override the parent columns method. Defines the columns to use in your listing table
	 *
	 * @return Array
	 */
	public function get_columns() {
		$columns = '';
		if ( isset( $_GET['page'] ) && 'token_tracking' == $_GET['page'] ) {
			$columns = array(
				'todays_date'     => 'Today\'s Date',
				'user_link'       => 'Donor ID',
				'donation_date'   => 'Org Order Date',
				'tokens_issued'   => 'Refunded Date',
				'tokens_expiry'   => 'Refunded Expiry',
				'order_id'        => 'Order ID',
				'transaction_id'  => 'Transaction ID',
				'project_uid'     => 'Project ID',
				'project_status'  => 'Project Status',
				'stripe_fee'      => 'Stripe Fees',
				'nano_fee'        => 'Nano Fees',
				'total_fee'       => 'Total Fees',
				'donation_amount' => 'Donation Amount',
				'credit_status'   => 'Credits Status',
				'tokens_spent'    => 'Credits Spent',
				'tokens_unused'   => 'Unused Credits',
				'expired_credits' => 'Expired Credits',
				'sub_donation_date'   => 'Single Order Date',
				'sub_order_id'        => 'Single Order ID',
				'sub_transaction_id'  => 'Single Transaction ID',
				'sub_project_uid'     => 'Single Project UID',
				'sub_project_status'  => 'Single Project Status',
				'sub_stripe_fee'      => 'Single Stripe Fee',
				'sub_nano_fee'        => 'Single nano Fee',
				'sub_total_fee'       => 'Single Total Fee',
				'sub_donation_amount' => 'Single Donation Amt',
			);
		} elseif ( isset( $_GET['page'] ) && 'project_tracking' == $_GET['page'] ) {
			$columns = array(
				'date_posted'          => 'Date Posted',
				'pt_date_expires'         => 'Date Expires',
				'pt_creator_paid'      => 'Creator Paid',
				'pt_project_uid'       => 'Project UID',
				'pt_creator_id'        => 'Creator IDK ID / Stripe ID',
				'pt_funding_status'    => 'Status',
				'pt_target_goal'       => 'Target Grant Amount',
				'pt_total_stripe_fees' => 'Stripe Fees',
				'pt_total_nano_fees'   => 'nano Fees',
				'pt_payment_id'        => 'Payment ID',
				'pt_transaction_id'    => 'Transaction ID',
				'pt_payment_method'    => 'Payment Method',
				'pt_donor_id'          => 'Donor ID',
				'pt_stripe_fee'        => 'Stripe Fee',
				'pt_nano_fee'          => 'nano Fee',
				'pt_order_amt'         => '$ Order',
			);
		} elseif ( isset( $_GET['page'] ) && 'pending_payouts' == $_GET['page'] ) {
			$columns = array(
				'date_posted'    => 'Date Posted',
				'project_uid'    => 'Project UID',
				'creator_id'     => 'Creator IDK ID / Stripe ID',
				'funding_status' => 'Status',
				'project_goal'   => 'Goal',
				'total_funded'   => 'Raised',
				'total_fees'     => 'Fees',
				'nano_fees'      => 'nanosteam Fees',
				'stripe_fees'    => 'Stripe Fees',
				'after_fees'     => 'After Fees',
				'payment_status' => 'Notes',
				'payment_link'   => 'Project Link',
			);
		} elseif ( isset( $_GET['page'] ) && 'donations_received' == $_GET['page'] ) {
			$columns = array(
				'month'          => 'Month',
				'type'           => 'Type',
				'd_total_funded' => 'Total Received',
				'd_total_fees'   => 'All Fees',
				'd_nano_fees'    => 'Nano Fees',
				'd_stripe_fees'  => 'Stripe Fees',
				'd_after_fees'   => 'After Fees',
			);
		} elseif ( isset( $_GET['page'] ) && 'donations_paid' == $_GET['page']  ) {
			$columns = array(
				'month'        => 'Month',
				'type'         => 'Type',
				'p_total_funded' => 'Donations Paid',
				'p_total_first'  => 'First Payment',
				'p_total_final'  => 'Final Payment',
			);
		} elseif ( isset( $_GET['page'] ) && 'nano_idc_orders' == $_GET['page']  ) {
			$columns = array(
				'order_id'       => 'Order ID',
				'order_date'     => 'Order Date',
				'user_id'        => 'User ID',
				'user_email'     => 'User Email',
				'product_name'   => 'Product',
				'price'          => 'Price',
				'transaction_id' => 'Transaction ID',
				'order_status'   => 'Order Status',
			);
		}

		return $columns;
	}

	/**
	 * Define which columns are hidden
	 *
	 * @return Array
	 */
	public function get_hidden_columns() {
		$columns = '';
		if ( isset( $_GET['page'] ) && 'token_tracking' == $_GET['page'] ) {
			$columns = array();
		} else {
			$columns = array(
				'date_posted_h' => 'Ordering Hidden',
			);
		}

		return $columns;
	}

	/**
	 * Define the sortable columns
	 *
	 * @return Array
	 */
	public function get_sortable_columns() {

		$sortable_columns = '';
		if ( isset( $_GET['page'] ) && 'token_tracking_null' == $_GET['page'] ) {
			$sortable_columns = array(
				'tokens_spent'    => array( 'tokens_spent', false ),
				'tokens_unused'   => array( 'tokens_unused', false ),
				'tokens_expiry'   => array( 'tokens_expiry', false ),
				'tokens_issued'   => array( 'tokens_issued', false ),
				'donation_date'   => array( 'donation_date', false ),
				'stripe_fee'      => array( 'stripe_fee', false ),
				'nano_fee'        => array( 'nano_fee', false ),
				'total_fee'       => array( 'total_fee', false ),
				'donation_amount' => array( 'donation_amount', false ),
			);
		} else {
			$sortable_columns = array();
		}

		return $sortable_columns;

	}

	/**
	 * Get the table data
	 *
	 * @return Array
	 */
	private function table_data() {
		$data = '';
		if ( isset( $_GET['page'] ) && 'token_tracking' == $_GET['page'] ) {
			$data = nano_token_tracking(){'data'};
		} elseif ( isset( $_GET['page'] ) && 'project_tracking' == $_GET['page'] ) {
			$data = nano_project_tracking(){'table_data'};
		} elseif ( isset( $_GET['page'] ) && 'pending_payouts' == $_GET['page'] ) {
			$data = nano_pending_payouts(){'table_data'};
		} elseif ( isset( $_GET['page'] ) && 'donations_received' == $_GET['page'] ) {
			$data = nano_donations_received(){'table_data'};
		} elseif ( isset( $_GET['page'] ) && 'donations_paid' == $_GET['page'] ) {
			$data = nano_donations_paid(){'table_data'};
		}  elseif ( isset( $_GET['page'] ) && 'nano_idc_orders' == $_GET['page']  ) {
			$data = nano_idc_orders(){'table_data'};
		}

		return $data;
	}

	/**
	 * Define what data to show on each column of the table
	 *
	 * @param  Array $item Data
	 * @param  String $column_name - Current column name
	 *
	 * @return Mixed
	 */
	public function column_default( $item, $column_name ) {
		if ( isset( $_GET['page'] ) && 'token_tracking' == $_GET['page'] ) {
			switch ( $column_name ) {
				case 'todays_date':
				case 'date_posted_h':
				case 'user_link':
				case 'donation_date':
				case 'tokens_issued':
				case 'tokens_expiry':
				case 'creator_paid':
				case 'order_id':
				case 'transaction_id':
				case 'project_uid':
				case 'project_status':
				case 'stripe_fee':
				case 'nano_fee':
				case 'total_fee':
				case 'donation_amount':
				case 'tokens_spent':
				case 'tokens_unused':
				case 'expired_credits':
				case 'sub_donation_date':
				case 'sub_order_id':
				case 'sub_transaction_id':
				case 'sub_project_uid':
				case 'sub_project_status':
				case 'sub_stripe_fee':
				case 'sub_nano_fee':
				case 'sub_total_fee':
				case 'sub_donation_amount':
				case 'credit_status':
					return $item[ $column_name ];
				default:
					return print_r( $item, true );
			}
		} elseif ( isset( $_GET['page'] ) && 'project_tracking' == $_GET['page'] ) {

			switch ( $column_name ) {
				case 'date_posted':
				case 'date_posted_h':
				case 'pt_date_expires':
				case 'pt_creator_paid':
				case 'pt_project_uid':
				case 'pt_creator_id':
				case 'pt_funding_status':
				case 'pt_target_goal':
				case 'pt_total_stripe_fees':
				case 'pt_total_nano_fees':
				case 'pt_payment_id':
				case 'pt_transaction_id':
				case 'pt_payment_method':
				case 'pt_donor_id':
				case 'pt_stripe_fee':
				case 'pt_nano_fee':
				case 'pt_order_amt':
					return $item[ $column_name ];
				default:
					return print_r( $item, true );
			}
		} elseif ( isset( $_GET['page'] ) && 'pending_payouts' == $_GET['page'] ) {

			switch ( $column_name ) {
				case 'date_posted':
				case 'date_posted_h':
				case 'project_uid':
				case 'creator_id':
				case 'funding_status':
				case 'project_goal':
				case 'total_funded':
				case 'total_fees':
				case 'nano_fees':
				case 'stripe_fees':
				case 'after_fees':
				case 'target_goal':
				case 'payment_status':
				case 'payment_link':
					return $item[ $column_name ];
				default:
					return print_r( $item, true );
			}

		} elseif ( isset( $_GET['page'] ) && 'donations_received' == $_GET['page'] ) {

			switch ( $column_name ) {
				case 'month':
				case 'date_posted_h':
				case 'type':
				case 'd_total_funded':
				case 'd_total_fees':
				case 'd_nano_fees':
				case 'd_stripe_fees':
				case 'd_after_fees':
					return $item[ $column_name ];
				default:
					return print_r( $item, true );
			}

		} elseif ( isset( $_GET['page'] ) && 'donations_paid' == $_GET['page']  ) {

			switch ( $column_name ) {
				case 'month':
				case 'date_posted_h':
				case 'type':
				case 'p_total_funded':
				case 'p_total_first':
				case 'p_total_final':
					return $item[ $column_name ];
				default:
					return print_r( $item, true );
			}

		} elseif ( isset( $_GET['page'] ) && 'nano_idc_orders' == $_GET['page']  ) {

			switch ( $column_name ) {
				case 'month':
				case 'order_id':
				case 'order_date':
				case 'user_id':
				case 'user_email':
				case 'product_name':
				case 'price':
				case 'transaction_id':
				case 'order_status':
					return $item[ $column_name ];
				default:
					return print_r( $item, true );
			}

		}

	}


	/**
	 * Allows you to sort the data by the variables set in the $_GET
	 *
	 * @return Mixed
	 */
	private function sort_data( $a, $b ) {
		// Set defaults
		if ( isset( $_GET['page'] ) && 'token_tracking' == $_GET['page'] ) {
			$orderby = 'date_posted_h';
			$order   = 'asc';
		} elseif ( isset( $_GET['page'] ) && ( 'donations_received' == $_GET['page'] || 'donations_paid' == $_GET['page'] ) ) {
			$orderby = 'date_posted_h';
			$order   = 'asc';
		} elseif ( isset( $_GET['page'] ) && ( 'nano_idc_orders' == $_GET['page'] || 'project_tracking' == $_GET['page'] ) ) {
			$orderby = 'date_posted_h';
			$order   = 'desc';
		} else {
			$orderby = 'date_posted_h';
			$order   = 'asc';
		}


		// If orderby is set, use this as the sort column
		if ( ! empty( $_GET['orderby'] ) ) {
			$orderby = $_GET['orderby'];
		}
		// If order is set use this as the order
		if ( ! empty( $_GET['order'] ) ) {
			$order = $_GET['order'];
		}
		$result = strcmp( $a[ $orderby ], $b[ $orderby ] );

		if ( 'asc' === $order ) {
			return $result;
		}

		return - $result;
	}

	/**
	 * Display the table
	 *
	 * @since 3.1.0
	 */
	public function display() {
		$singular = $this->_args['singular'];

		$this->display_tablenav( 'top' );

		$this->screen->render_screen_reader_content( 'heading_list' );
		?>
		<table class="wp-list-table <?php echo implode( ' ', $this->get_table_classes() ); ?>">
			<thead>
			<tr>
				<?php $this->print_column_headers(); ?>
			</tr>
			</thead>

			<tbody id="the-list"<?php
			if ( $singular ) {
				echo " data-wp-lists='list:$singular'";
			} ?>>
			<?php $this->display_rows_or_placeholder(); ?>
			</tbody>

			<tfoot>
			<tr>
				<?php $this->print_column_footers(); ?>
			</tr>
			</tfoot>

		</table>
		<?php
		$this->display_tablenav( 'bottom' );
	}


	/**
	 * Print column headers, accounting for hidden and sortable columns.
	 *
	 * @since 3.1.0
	 * @access protected
	 *
	 * @param bool $with_id Whether to set the id attribute or not
	 */
	function print_column_footers( $with_id = true ) {
		list( $columns, $hidden, $sortable ) = $this->get_column_info();

		$current_url = set_url_scheme( 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] );
		$current_url = remove_query_arg( 'paged', $current_url );

		if ( isset( $_GET['orderby'] ) ) {
			$current_orderby = $_GET['orderby'];
		} else {
			$current_orderby = '';
		}

		if ( isset( $_GET['order'] ) && 'desc' == $_GET['order'] ) {
			$current_order = 'desc';
		} else {
			$current_order = 'asc';
		}

		if ( ! empty( $columns['cb'] ) ) {
			static $cb_counter = 1;
			$columns['cb'] = '<label class="screen-reader-text" for="cb-select-all-' . $cb_counter . '">' . __( 'Select All' ) . '</label>'
			                 . '<input id="cb-select-all-' . $cb_counter . '" type="checkbox" />';
			$cb_counter ++;
		}

		$function_type = '';


		if ( isset( $_GET['page']) && $_GET['page'] === 'donations_received' ) {
			//$function_type = nano_donations_received();
		} elseif ( isset($_GET['page']) && $_GET['page'] === 'donations_paid') {
			$function_type = nano_donations_paid();
		} elseif ( isset($_GET['page']) && $_GET['page'] === 'pending_payouts') {
			$function_type = nano_pending_payouts();
		} elseif ( isset($_GET['page']) && $_GET['page'] === 'token_tracking') {
			$function_type = nano_token_tracking();
		} elseif ( isset($_GET['page']) && $_GET['page'] === 'project_tracking') {
			$function_type = nano_project_tracking();
		}

		foreach ( $columns as $column_key => $column_display_name ) {
			$class = array( 'manage-column', "column-$column_key" );

			$style = '';
			if ( in_array( $column_key, $hidden ) ) {
				$style = 'display:none;';
			}

			$style = ' style="' . $style . '"';

			if ( 'cb' == $column_key ) {
				$class[] = 'check-column';
			} elseif ( in_array( $column_key, array( 'posts', 'comments', 'links' ) ) ) {
				$class[] = 'num';
			}

			if ( isset( $sortable[ $column_key ] ) ) {
				list( $orderby, $desc_first ) = $sortable[ $column_key ];

				if ( $current_orderby == $orderby ) {
					$order   = 'asc' == $current_order ? 'desc' : 'asc';
					$class[] = 'sorted';
					$class[] = $current_order;
				} else {
					$order   = $desc_first ? 'desc' : 'asc';
					$class[] = 'sortable';
					$class[] = $desc_first ? 'asc' : 'desc';
				}

				$column_display_name = '<a href="' . esc_url( add_query_arg( compact( 'orderby', 'order' ), $current_url ) ) . '"><span>' . $column_display_name . '</span><span class="sorting-indicator"></span></a>';
			}

			$id = $with_id ? "id='$column_key'" : '';

			if ( ! empty( $class ) ) {
				$class = "class='" . join( ' ', $class ) . "'";
			}

			$column_display_name = '';
			// Token Credits Trackimng
			if ( $column_key == 'todays_date' || $column_key == 'date_posted' || $column_key == 'month' ) {
				$column_display_name = 'Total';
			} elseif ( $column_key == 'stripe_fee' ) {
				$column_display_name = $function_type['total_stripe'];
			} elseif ( $column_key == 'expired_credits' ) {
				$column_display_name = $function_type['total_expired'];
			} elseif ( $column_key == 'nano_fee' ) {
				$column_display_name = $function_type['total_nano_fees'];
			} elseif ( $column_key == 'total_fee' ) {
				$column_display_name = $function_type['total_fees'];
			} elseif ( $column_key == 'donation_amount' ) {
				$column_display_name = $function_type['total_donations'];
			} elseif ( $column_key == 'tokens_spent' ) {
				$column_display_name = $function_type['total_spent'];
			} elseif ( $column_key == 'tokens_unused' ) {
				$column_display_name = $function_type['total_unused'];
			} elseif ( $column_key == 'sub_donation_amount' ) {
				$column_display_name = $function_type['tr_sub_total'];
			} elseif ( $column_key == 'sub_stripe_fee' ) {
				$column_display_name = $function_type['tr_sub_tripe'];
			} elseif ( $column_key == 'sub_nano_fee' ) {
				$column_display_name = $function_type['tr_sub_nano'];
			} elseif ( $column_key == 'sub_total_fee' ) {
				$column_display_name = $function_type['tr_sub_total_fee'];
			} // Project Tracking
			elseif ( $column_key == 'target_goal' ) {
				$column_display_name = $function_type['pt_total_goal'];
			} elseif ( $column_key == 'order_amt' ) {
				$column_display_name = $function_type['pt_total_paid'];
			} elseif ( $column_key == 'pt_total_stripe_fees' ) {
				$column_display_name = $function_type['pt_total_stripe_fees'];
			} elseif ( $column_key == 'pt_total_nano_fees' ) {
				$column_display_name = $function_type['pt_total_nano_fees'];
			} elseif ( $column_key == 'pt_stripe_fee' ) {
				$column_display_name = $function_type['pt_total_stripe_sub_fees'];
			} elseif ( $column_key == 'pt_nano_fee' ) {
				$column_display_name = $function_type['pt_total_nano_sub_fees'];
			} elseif ( $column_key == 'pt_order_amt' ) {
				$column_display_name = $function_type['pt_total_order'];
			} // Payment Tracking ?? Maybe
			elseif ( $column_key == 'project_goal' ) {
				$column_display_name = $function_type['t_total_goals'];
			} elseif ( $column_key == 'total_funded' ) {
				$column_display_name = $function_type['t_total_funding'];
			} elseif ( $column_key == 'total_fees' ) {
				$column_display_name = $function_type['t_total_fees'];
			} elseif ( $column_key == 'nano_fees' ) {
				$column_display_name = $function_type['t_nano_fees'];
			} elseif ( $column_key == 'stripe_fees' ) {
				$column_display_name = $function_type['t_stripe_fees'];
			} elseif ( $column_key == 'after_fees' ) {
				$column_display_name = $function_type['t_after_fees'];
			} elseif ( $column_key == 'd_total_funded' ) {
				$column_display_name = $_SESSION['d_total_funded'];
			} elseif ( $column_key == 'd_total_fees' ) {
				$column_display_name = $_SESSION['d_total_fees'];
			} elseif ( $column_key == 'd_nano_fees' ) {
				$column_display_name = $_SESSION['d_nano_fees'];
			} elseif ( $column_key == 'd_stripe_fees' ) {
				$column_display_name = $_SESSION['d_stripe_fees'];
			} elseif ( $column_key == 'd_after_fees' ) {
				$column_display_name = $_SESSION['d_after_fees'];
			} elseif ( $column_key == 'p_total_funded' ) {
				$column_display_name = $_SESSION['p_total_funded'];
			} elseif ( $column_key == 'p_total_first' ) {
				$column_display_name = $_SESSION['p_total_first'];
			} elseif ( $column_key == 'p_total_final' ) {
				$column_display_name = $_SESSION['p_total_final'];
			}

			echo "<th scope='col' $id $class $style>$column_display_name</th>";
		}
	}

}