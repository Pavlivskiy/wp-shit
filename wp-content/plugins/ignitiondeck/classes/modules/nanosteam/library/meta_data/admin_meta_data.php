<?php if ( ! defined( 'ABSPATH' ) ) {
	die( 'Direct access forbidden.' );
}


/**
 * List all projects which have pending payouts
 */
function nano_pending_payouts() {

	$test_env = false;

	$date_from_org = isset( $_POST['date_from'] ) ? $_POST['date_from'] : '';
	$date_to       = isset( $_POST['date_to'] ) ? $_POST['date_to'] : '';

	// Tokens
	$today = nano_get_formated_date_time_gmt( 'm/d/Y' );
	$back  = date( 'm/d/Y', strtotime( '-30 days', strtotime( $today ) ) );

	if ( true == $test_env || ! isset( $_POST['submit'] ) ) {
		$date_from_org = $back;
		$date_to       = $today;
	}

	$payout_status = isset( $_POST['payout_status'] ) ? $_POST['payout_status'] : 'pending';

	if ( is_user_logged_in() ) {

		if ( ! current_user_can( 'administrator' ) ) {
			return;
		}

		$settings = get_option( 'memberdeck_gateways' );

		if ( ! empty( $settings ) ) {
			if ( is_array( $settings ) ) {
				$test = $settings['test'];
				if ( 1 == $test ) {
					$url = '/test';
				} else {
					$url = '';
				}
			}
		}

		$date_from_org = date( 'Y-m-d', strtotime( $date_from_org ) );
		$date_to       = date( 'Y-m-d', strtotime( $date_to ) );

		// Collect all regular orders with actual stripe transaction IDs for processing

		$range = " WHERE time >= '" . $date_from_org . " 00:00:00' AND time <= '" . $date_to . " 23:59:59' ";

		// Tabular Data

		global $wpdb;
		// this adds the prefix which is set by the user upon instillation of WP
		$table_name = $wpdb->prefix . 'nano_meta_data';
		// this will get the data from your table

		$sql           = "SELECT * FROM $table_name";
		$retrieve_data = $wpdb->get_results( $sql );

		$meta_data  = '';
		$table_data = array();
		$count      = 0;

		$total_goal = 0;
		$total_paid = 0;

		$t_total_goals   = 0;
		$t_total_funding = 0;
		$t_total_fees    = 0;
		$t_nano_fees     = 0;
		$t_stripe_fees   = 0;
		$t_after_fees    = 0;


		foreach ( $retrieve_data as $data ) {

			$date_posted       = date( 'm/d/Y', strtotime( $data->time ) );
			$project_uid       = $data->project_uid;
			$creator_id        = $data->creator_id;
			$creator_stripe_id = $data->creator_stripe_id;
			$post_id           = $data->post_id;
			$project_status    = unserialize( $data->project_status );
			$payments          = unserialize( $data->payments );
			$target_goal       = get_post_meta( $post_id, 'ign_fund_goal', true );
			$date_posted_h     = strtotime( $data->time );


			$pending                = false;
			$project_success_status = false;
			$project_fund_status    = false;

			// Fees and Totals based on whether this is the first or last matching grant or an all or nothing project

			if ( ! in_array( $project_status['funding_status'], array( 'EXPIRED', 'REALLOCATED' ) ) ) {

				$creator_link = '<a href="' . get_author_posts_url( $creator_id ) . '">' . $creator_id . '</a> / <a href="https://dashboard.stripe.com' . $url . '/applications/users/' . $creator_stripe_id . '">' . $creator_stripe_id . '</a>';
				$project_link = '<a href="' . get_permalink( $post_id ) . '">' . $project_uid . '</a>';

				//Project Parameters
				$level_id = nano_get_level_id( get_post_meta( $post_id, 'ign_project_id', true ) );
				$orders   = ID_Member_Order::get_orders_by_level( $level_id );

				// FEES & TOTALS
				$fees              = nano_fee_list( $orders, $post_id );
				$total_fund_raised = $fees['total_before_fees'];
				$stripe_fees       = $fees['stripe_fees'];
				$nanosteam_fees    = $fees['nanosteam_fees'];
				$total_fees        = $fees['total_fees'];
				$total_after_fees  = $fees['total_after_fees'];

				// Matching Grant Breakdown in totals
				$matching_first_amount = number_format( ( $total_fund_raised / 2 ) - $total_fees, 2, '.', '' );
				$matching_last_amount  = number_format( ( $total_fund_raised / 2 ), 2, '.', '' );

				// Payout Status
				$project_success_status       = get_post_meta( $post_id, 'ign_project_success', true ) == '1' ? true : false;
				$project_fund_goal            = get_post_meta( $post_id, 'ign_fund_goal', true );
				$project_fund_raised          = $total_fund_raised;
				$project_fund_status          = absint( $project_fund_raised ) >= absint( $project_fund_goal ) ? true : false;
				$matching_grant_project_check = get_post_meta( $post_id, 'fifty_fifty_grant', true ) == 'approved' ? true : false;
				$second_milestone_check       = nano_second_milsetone_achieved( $post_id );

				// Project Payment Statuses
				// Matching Grant
				$first_transfer_processed_id = get_post_meta( $post_id, 'first_transfer_transaction_id', true );
				$first_transfer_initiated    = get_post_meta( $post_id, 'first_transfer_transaction_initiated', true );
				$first_transfer_delivered    = get_post_meta( $post_id, 'first_transfer_transaction_delivered', true );

				$final_transfer_processed_id = get_post_meta( $post_id, 'final_transfer_transaction_id', true );
				$final_transfer_initiated    = get_post_meta( $post_id, 'final_transfer_transaction_date_initiated', true );
				$final_transfer_delivered    = get_post_meta( $post_id, 'final_transfer_transaction_date_delivered', true );

				// All In
				$standard_transfer_processed_id = get_post_meta( $post_id, 'standard_transfer_transaction_id', true );
				$standard_transfer_initiated    = get_post_meta( $post_id, 'standard_transfer_transaction_date_initiated', true );
				$standard_transfer_delivered    = get_post_meta( $post_id, 'standard_transfer_transaction_date_delivered', true );

				if ( 'pending' === $payout_status ) {

					$alert  = payout_problem_alert( $post_id );
					$status = isset( $alert['message'] ) ? $alert['message'] : '';

					if ( false == $matching_grant_project_check ) {

						if ( '' == $standard_transfer_processed_id ) {

							$status  .= '<p>This all in project is now ready to be paid out.</p>';
							$message = 'Administrate Project';
							$pending = true;

						} elseif ( '' !== $standard_transfer_processed_id && 'PENDING' === $standard_transfer_delivered ) {

							$stripe_withdrawl_account = get_option( 'nano_debit_account' );
							$transfer_id              = $standard_transfer_processed_id;
							$link_url                 = 'https://dashboard.stripe.com/' . $stripe_withdrawl_account . $url . '/connect/transfers/' . $transfer_id;
							$link                     = '<a href="' . $link_url . '">' . $transfer_id . '</a>';

							$status  .= '<p>This payment has been processed but has not been delivered yet.</p>';
							$status  .= '<p>Transfer Date Initiated: ' . $standard_transfer_initiated . '</p>';
							$status  .= '<p>Transfer Date Delivered: ' . $standard_transfer_delivered . '</p>';
							$status  .= '<p>Stripe Transfer ID : ' . $link . '</p>';
							$message = 'Administrate Project';
							$pending = true;

						}
					} elseif ( true === $matching_grant_project_check ) {

						if ( '' == $first_transfer_processed_id && true !== $second_milestone_check ) {

							$message = 'Administrate Project';
							$status  = '<p>Payout 1 of 2 for matching grant project is now ready to transfer.</p>';
							$status  .= '<p>Final matching grant Payout is still pending.</p>';
							$pending = true;

						} elseif ( ! '' == $first_transfer_processed_id && '' == $final_transfer_processed_id && true !== $second_milestone_check ) {

							$stripe_withdrawl_account = get_option( 'nano_debit_account' );
							$transfer_id              = $first_transfer_processed_id;
							$link_url                 = 'https://dashboard.stripe.com/' . $stripe_withdrawl_account . $url . '/connect/transfers/' . $transfer_id;
							$link                     = '<a href="' . $link_url . '">' . $transfer_id . '</a>';

							if ( 'PENDING' === $first_transfer_delivered ) {

								$status  .= '<p>This First payment has been processed but has not been delivered yet.</p>';
								$status  .= '<p>The Creator has NOT posted their second labnote for the final transfer to be processed.</p>';
								$status  .= '<p>Transfer Date Initiated: ' . $first_transfer_initiated . '</p>';
								$status  .= '<p>Transfer Date Delivered: ' . $first_transfer_delivered . '</p>';
								$status  .= '<p>Stripe Transfer ID : ' . $link . '</p>';
								$message = 'Administrate Project';
								$pending = true;

							} else {

								$message = 'Administrate Project';
								$status  .= '<p>Creator has NOT posted their second labnote, final payment can\'t be processed yet.</p>';
								$status  .= '<p>Transfer Date Initiated: ' . $first_transfer_initiated . '</p>';
								$status  .= '<p>Transfer Date Delivered: ' . $first_transfer_delivered . '</p>';
								$status  .= '<p>Stripe Transfer ID : ' . $link . '</p>';
								$pending = true;

							}

						} elseif ( ! '' == $first_transfer_processed_id && '' == $final_transfer_processed_id && true === $second_milestone_check ) {

							if ( 'PENDING' === $first_transfer_delivered ) {

								$stripe_withdrawl_account = get_option( 'nano_debit_account' );
								$transfer_id              = $first_transfer_processed_id;
								$link_url                 = 'https://dashboard.stripe.com/' . $stripe_withdrawl_account . $url . '/connect/transfers/' . $transfer_id;
								$link                     = '<a href="' . $link_url . '">' . $transfer_id . '</a>';

								$status  .= '<p>This first payment has been processed but has not been delivered yet.</p>';
								$status  .= '<p>The Creator has <strong>POSTED</strong> their second labnote for the final transfer to be processed.</p>';
								$status  .= '<p>Transfer Date Initiated: ' . $first_transfer_initiated . '</p>';
								$status  .= '<p>Transfer Date Delivered: ' . $first_transfer_delivered . '</p>';
								$status  .= '<p>Stripe Transfer ID : ' . $link . '</p>';
								$message = 'Administrate Project';
								$pending = true;

							} else {

								$message = 'Administrate Project';
								$status  .= '<p>Creator has posted their second labnote, this can now be paid</p>';
								$pending = true;

							}

						} elseif ( ! '' == $first_transfer_processed_id && '' !== $final_transfer_processed_id && true === $second_milestone_check && 'PENDING' === $final_transfer_delivered ) {

							$stripe_withdrawl_account = get_option( 'nano_debit_account' );
							$transfer_id              = $final_transfer_processed_id;
							$link_url                 = 'https://dashboard.stripe.com/' . $stripe_withdrawl_account . $url . '/connect/transfers/' . $transfer_id;
							$link                     = '<a href="' . $link_url . '">' . $transfer_id . '</a>';

							$status  .= '<p>This final payment of $' . $matching_last_amount . ' has been processed but has not been delivered yet.</p>';
							$status  .= '<p>Transfer Date Initiated: ' . $final_transfer_initiated . '</p>';
							$status  .= '<p>Transfer Date Delivered: ' . $final_transfer_delivered . '</p>';
							$status  .= '<p>Stripe Transfer ID : ' . $link . '</p>';
							$message = 'Administrate Project';
							$pending = true;

						}

					}

				} elseif ( 'paid' === $payout_status ) {

					if ( 'COMPLETED' === $project_status['funding_status'] ) {

						if ( false == $matching_grant_project_check ) {

							if ( '' !== $standard_transfer_processed_id && 'PENDING' !== $standard_transfer_delivered ) {

								$stripe_withdrawl_account = get_option( 'nano_debit_account' );
								$transfer_id              = $standard_transfer_processed_id;
								$link_url                 = 'https://dashboard.stripe.com/' . $stripe_withdrawl_account . $url . '/connect/transfers/' . $transfer_id;
								$link                     = '<a href="' . $link_url . '">' . $transfer_id . '</a>';

								$status  = '<p>This payment has been paid.</p>';
								$status  .= '<p>Transfer Date Initiated: ' . $standard_transfer_initiated . '</p>';
								$status  .= '<p>Transfer Date Delivered: ' . $standard_transfer_delivered . '</p>';
								$status  .= '<p>Stripe Transfer ID : ' . $link . '</p>';
								$message = 'Administrate Project';
								$pending = true;

							}

						} elseif ( true === $matching_grant_project_check ) {

							if ( ! '' == $first_transfer_processed_id && '' !== $final_transfer_processed_id && 'PENDING' !== $first_transfer_delivered && 'PENDING' !== $final_transfer_delivered ) {

								$stripe_withdrawl_account = get_option( 'nano_debit_account' );
								//First
								$transfer_id = $first_transfer_processed_id;
								$link_url    = 'https://dashboard.stripe.com/' . $stripe_withdrawl_account . $url . '/connect/transfers/' . $transfer_id;
								$link        = '<a href="' . $link_url . '">' . $transfer_id . '</a>';

								//Final;
								$final_transfer_id = $final_transfer_processed_id;
								$final_link_url    = 'https://dashboard.stripe.com/' . $url . $stripe_withdrawl_account . '/connect/transfers/' . $final_transfer_id;
								$final_link        = '<a href="' . $final_link_url . '">' . $final_transfer_id . '</a>';

								$status  = '<p>This matching grant payment has been processed and paid.</p>';
								$status  .= '<p>First Transfer Date Initiated: ' . $first_transfer_initiated . '</p>';
								$status  .= '<p>Transfer Date Delivered: ' . $first_transfer_delivered . '</p>';
								$status  .= '<p>First Stripe Transfer ID : ' . $link . '</p>';
								$status  .= '<p>Final Transfer Date Initiated: ' . $final_transfer_initiated . '</p>';
								$status  .= '<p>Transfer Date Delivered: ' . $final_transfer_delivered . '</p>';
								$status  .= '<p>Final Stripe Transfer ID : ' . $final_link . '</p>';
								$message = 'Administrate Project';
								$pending = true;

							}

						}

					}

				}

			}


			// Proceed if the project fundraising effors are noted as successful and double check that funds raised are great
			// or equal to the fundraising goal
			if ( ( true == $project_success_status || true == $project_fund_status ) && true === $pending ) {

				if ( true !== $matching_grant_project_check ) {

				} else {

					$status .= '<p>First Transfer Amount: $' . $matching_first_amount . '</p>';
					$status .= '<p>Final Transfer Amount: $' . $matching_last_amount . '</p>';

				}

				// Fees and Totals based on whether this is the first or last matching grant

				$transfer_button = '<a href="/wp-admin/post.php?post=' . $post_id . '&action=edit" class="button button-primary button-large">' . $message . '</a>';

				$table_data[ $count ] = array(
					'date_posted'    => $date_posted,
					'date_posted_h'  => $date_posted_h,
					'target_goal'    => $target_goal,
					'creator_id'     => $creator_link,
					'project_uid'    => $project_link,
					'funding_status' => $project_status['funding_status'],
					'project_goal'   => $project_fund_goal,
					'total_funded'   => $total_fund_raised,
					'total_fees'     => $total_fees,
					'nano_fees'      => $nanosteam_fees,
					'stripe_fees'    => $stripe_fees,
					'after_fees'     => $total_after_fees,
					'payment_status' => $status,
					'payment_link'   => $transfer_button,
				);

				$count ++;

				$t_total_goals   += $project_fund_goal;
				$t_total_funding += $total_fund_raised;
				$t_total_fees    += $total_fees;
				$t_nano_fees     += $nanosteam_fees;
				$t_stripe_fees   += $stripe_fees;
				$t_after_fees    += $total_after_fees;


			}
		}

		$data                    = array();
		$data['table_data']      = $table_data;
		$data['t_total_goals']   = $t_total_goals;
		$data['t_total_funding'] = $t_total_funding;
		$data['t_total_fees']    = $t_total_fees;
		$data['t_nano_fees']     = $t_nano_fees;
		$data['t_stripe_fees']   = $t_stripe_fees;
		$data['t_after_fees']    = $t_after_fees;

		return $data;

	}
}


/**
 * List all metadata for tracking purposes
 */
function nano_project_tracking() {

	$test_env = false;

	$date_from_org = isset( $_POST['date_from'] ) ? str_replace( '.', '/', $_POST['date_from'] ) : '';
	$date_to       = isset( $_POST['date_to'] ) ? str_replace( '.', '/', $_POST['date_to'] ) : '';
	$project_class = isset( $_POST['project_type'] ) ? $_POST['project_type'] : 'A';

	// Tokens
	$today = nano_get_formated_date_time_gmt( 'm/d/Y' );
	$back  = date( 'm/d/Y', strtotime( '-30 days', strtotime( $today ) ) );

	if ( true == $test_env || ! isset( $_POST['submit'] ) ) {
		$date_from_org = $back;
		$date_to       = $today;
	}

	if ( is_user_logged_in() ) {

		if ( ! current_user_can( 'administrator' ) ) {
			return;
		}

		$settings = get_option( 'memberdeck_gateways' );

		if ( ! empty( $settings ) ) {
			if ( is_array( $settings ) ) {
				$test = $settings['test'];
				if ( 1 == $test ) {
					$url = '/test';
				} else {
					$url = '';
				}
			}
		}

		$date_from_org = date( 'Y-m-d', strtotime( $date_from_org ) );
		$date_to       = date( 'Y-m-d', strtotime( $date_to ) );

		// Collect all regular orders with actual stripe transaction IDs for processing

		$range = " WHERE time >= '" . $date_from_org . " 00:00:00' AND time <= '" . $date_to . " 23:59:59' ";

		// Tabular Data

		global $wpdb;
		// this adds the prefix which is set by the user upon instillation of WP
		$table_name = $wpdb->prefix . 'nano_meta_data' . $range;
		// this will get the data from your table

		$sql           = "SELECT * FROM $table_name";
		$retrieve_data = $wpdb->get_results( $sql );

		$meta_data  = '';
		$table_data = array();
		$count      = 0;

		$total_goal          = 0;
		$total_paid          = 0;
		$total_stripe_fees   = 0;
		$t_total_stripe_fees = 0;
		$total_nano_fees     = 0;
		$t_total_nano_fees   = 0;

		foreach ( $retrieve_data as $data ) {


			$project_uid       = $data->project_uid;
			$creator_id        = $data->creator_id;
			$creator_stripe_id = $data->creator_stripe_id;
			$post_id           = $data->post_id;
			$project_status    = unserialize( $data->project_status );
			$payments          = unserialize( $data->payments );
			$target_goal       = '' !== get_post_meta( $post_id, 'ign_fund_goal', true ) ? get_post_meta( $post_id, 'ign_fund_goal', true ) : 0;
			$table_sub_data    = '';
			$order_amt         = '';
			$date_posted       = $project_status['date_posted'];
			$date_posted_h     = strtotime( $data->time );

			// We will do a quick update of all projects in the project list to see if we need to update thier statuses or any new orders
			nano_update_project_status_meta_data( $post_id );

			if ( strpos( $project_uid, $project_class ) !== false ) {

				//Project Parameters
				$level_id = nano_get_level_id( get_post_meta( $post_id, 'ign_project_id', true ) );
				$orders   = ID_Member_Order::get_orders_by_level( $level_id );

				// FEES & TOTALS
				$fees              = nano_fee_list( $orders, $post_id );
				$total_fund_raised = $fees['total_before_fees'];
				$stripe_fees       = $fees['stripe_fees'];
				$nanosteam_fees    = $fees['nanosteam_fees'];
				$total_fees        = $fees['total_fees'];
				$total_after_fees  = $fees['total_after_fees'];

				$total_stripe_fees += $stripe_fees;
				$total_nano_fees   += $nanosteam_fees;

				$creator_link = '<a href="' . get_author_posts_url( $creator_id ) . '">' . $creator_id . '</a> / <a href="https://dashboard.stripe.com' . $url . '/applications/users/' . $creator_stripe_id . '">' . $creator_stripe_id . '</a>';
				$project_link = '<a href="' . get_permalink( $post_id ) . '">' . $project_uid . '</a>';


				$table_sub_data = '';
				$order_amt      = '';


				if ( isset( $payments ) && count( $payments ) > 0 ) {

					$total_paid_sub        = 0;
					$total_nano_sub_fees   = 0;
					$total_stripe_sub_fees = 0;

					foreach ( $payments as $payment ) {

						if ( '' != $payment ) {

							$payment_id     = isset( $payment['payment_id'] ) ? $payment['payment_id'] : '';
							$transaction_id = isset( $payment['transaction_id'] ) ? $payment['transaction_id'] : '';

							if ( false !== strpos( $transaction_id, 'nano' ) && 'nanosteam Matching Grant' != $transaction_id ) {
								$transaction_id = explode( 'nano-', $transaction_id );
								if ( is_array( $transaction_id ) ) {
									$transaction_id = '<a href="https://dashboard.stripe.com' . $url . '/payments/' . $transaction_id[1] . '">' . $transaction_id[1] . '</a>';
								}
							}

							$payment_method  = isset( $payment['payment_method'] ) ? $payment['payment_method'] : '';
							$donor_id        = isset( $payment['donor_id'] ) ? $payment['donor_id'] : '';
							$donor_stripe_id = isset( $payment['donor_stripe_id'] ) ? $payment['donor_stripe_id'] : '';

							// Get the order amount
							$order_amt      = convert_to_numeric( ID_Member_Order::get_order_meta( $payment_id, 'pwyw_price', true ) );
							$nano_fee_amt   = nano_single_order_fee( $order_amt, $transaction_id );
							$nano_fee       = convert_to_numeric( $nano_fee_amt );
							$stripe_fee_amt = ID_Member_Order::get_order_meta( $payment_id, 'stripe_fee', true );
							$stripe_fee     = convert_to_numeric( $stripe_fee_amt / 100 );


							// Links
							$nano_author_link = '<a href="' . get_author_posts_url( $donor_id ) . '">' . $donor_id . '</a> / <a href="https://dashboard.stripe.com' . $url . '/customers/' . $donor_stripe_id . '">' . $donor_stripe_id . '</a>';
							$nano_stripe_link = false === strpos( $transaction_id, 'Previously' ) ? $transaction_id : 'FROM TEST CREDITS';
							$nano_order_link  = '<a href="admin.php?page=nano_idc_orders&order_id=' . $payment_id . '">' . $payment_id . '</a>';

							$table_data[ $count ] = array(
								'date_posted'          => '',
								'date_posted_h'        => $date_posted_h,
								'pt_date_expires'      => '',
								'pt_creator_paid'      => '',
								'pt_project_uid'       => '',
								'pt_creator_id'        => '',
								'pt_funding_status'    => '',
								'pt_target_goal'       => '',
								'pt_total_stripe_fees' => '',
								'pt_total_nano_fees'   => '',
								'pt_payment_id'        => $nano_order_link,
								'pt_transaction_id'    => $nano_stripe_link,
								'pt_payment_method'    => $payment_method,
								'pt_donor_id'          => $nano_author_link,
								'pt_stripe_fee'        => $stripe_fee,
								'pt_nano_fee'          => $nano_fee,
								'pt_order_amt'         => $order_amt,
							);

							$total_paid_sub        += $order_amt;
							$total_nano_sub_fees   += $nano_fee;
							$total_stripe_sub_fees += $stripe_fee;

							$count ++;
						}
					}

					$total_paid          += $total_paid_sub;
					$t_total_nano_fees   += $total_nano_sub_fees;
					$t_total_stripe_fees += $total_stripe_sub_fees;
				}

				$total_goal += $target_goal;

				$date_expires = isset( $project_status['date_expiration'] ) ? $project_status['date_expiration'] : '';
				$creator_paid = isset( $project_status['date_completed'] ) ? $project_status['date_completed'] : '';

				$table_data[ $count ] = array(
					'date_posted'          => $date_posted,
					'date_posted_h'        => $date_posted_h + 1,
					'pt_date_expires'      => $date_expires,
					'pt_creator_paid'      => $creator_paid,
					'pt_project_uid'       => $project_link,
					'pt_creator_id'        => $creator_link,
					'pt_funding_status'    => $project_status['funding_status'],
					'pt_target_goal'       => $target_goal,
					'pt_total_stripe_fees' => $stripe_fees,
					'pt_total_nano_fees'   => $nanosteam_fees,
					'pt_payment_id'        => '',
					'pt_transaction_id'    => '',
					'pt_payment_method'    => '',
					'pt_donor_id'          => '',
					'pt_stripe_fee'        => '',
					'pt_nano_fee'          => '',
					'pt_order_amt'         => '',
				);

				$count ++;

			}

		}

		$data                             = array();
		$data['table_data']               = $table_data;
		$data['pt_total_goal']            = convert_to_numeric( $total_goal );
		$data['pt_total_stripe_fees']     = convert_to_numeric( $total_stripe_fees );
		$data['pt_total_nano_fees']       = convert_to_numeric( $total_nano_fees );
		$data['pt_total_nano_sub_fees']   = convert_to_numeric( $t_total_nano_fees );
		$data['pt_total_stripe_sub_fees'] = convert_to_numeric( $t_total_stripe_fees );
		$data['pt_total_order']           = convert_to_numeric( $total_paid );

		return $data;

	}
}


/**
 *
 * This is to update nano_token_tracking for each order. Specifically to update new refunded token order's stripe fee
 * and their parent's stripe transaction ID for reference
 *
 */
function nano_token_tracking_update() {

	$_POST['submit']    = 'w';
	$_POST['date_to']   = nano_get_formated_date_time_gmt( 'm/d/Y' );
	$_POST['date_from'] = date( 'm/d/Y', strtotime( '-90 days', strtotime( $_POST['date_to'] ) ) );
	$project_classes    = array( 'A', 'M', 'C' );

	foreach ( $project_classes as $project_class ) {

		$_POST['project_type'] = $project_class;
		nano_token_tracking();

	}
}


/**
 * Token tracking
 * This is for use in populating the token data table which nanosteam uses to track financials each month
 */

function nano_token_tracking() {

	$date_from_org = isset( $_POST['date_from'] ) ? str_replace( '.', '/', $_POST['date_from'] ) : '';
	$date_to       = isset( $_POST['date_to'] ) ? str_replace( '.', '/', $_POST['date_to'] ) : '';
	$project_class = isset( $_POST['project_type'] ) ? $_POST['project_type'] : '';

	// Tokens
	$today          = '' !== get_transient( 'token_tracking_today' ) && false !== get_transient( 'token_tracking_today' ) ? get_transient( 'token_tracking_today' ) : nano_get_formated_date_time_gmt( 'm/d/Y' );
	$back           = '' !== get_transient( 'token_tracking_back' ) && false !== get_transient( 'token_tracking_back' ) ? get_transient( 'token_tracking_back' ) : date( 'm/d/Y', strtotime( '-30 days', strtotime( $today ) ) );
	$matching_check = '' !== get_transient( 'token_tracking_type' ) && 'M' === get_transient( 'token_tracking_type' ) ? 'selected="selected"' : '';

	if ( ! isset( $_POST['submit'] ) ) {
		$date_from_org = $back;
		$date_to       = $today;
		$project_class = '' !== $matching_check ? $matching_check : 'A';
	}

	if ( is_user_logged_in() || ( isset( $_POST['submit'] ) && 'w' === $_POST['submit'] ) ) {

		if ( ( isset( $_POST['submit'] ) && 'w' !== $_POST['submit'] ) || ! isset( $_POST['submit'] ) ) {
			if ( ! current_user_can( 'administrator' ) ) {
				return;
			}
		}

		$settings = get_option( 'memberdeck_gateways' );

		if ( ! empty( $settings ) ) {
			if ( is_array( $settings ) ) {
				$test = $settings['test'];
				if ( 1 == $test ) {
					$url = '/test';
				} else {
					$url = '';
				}
			}
		}

		$token_data = array();

		$date_from_org = date( 'Y-m-d', strtotime( $date_from_org ) ) . ' 00:00:00';
		$date_to       = date( 'Y-m-d', strtotime( $date_to ) ) . ' 23:59:59';

		$project_type = '%' . $project_class . '%';

		// Prepare the table data
		global $wpdb;
		$table_name              = $wpdb->prefix . 'nano_token_meta_data';
		$sql                     = "SELECT * FROM $table_name WHERE time >= %s AND time <= %s AND nano_project_uid LIKE %s";
		$sql_prepare             = $wpdb->prepare( $sql, $date_from_org, $date_to, $project_type );
		$nano_token_meta_results = $wpdb->get_results( $sql_prepare );


		// Total counters
		$total_donations = 0;
		$total_stripe    = 0;
		$total_nano      = 0;
		$total_fees      = 0;
		$total_spent     = 0;
		$total_unused    = 0;
		$total_expired   = 0;

		// Subs
		$s_sub_order_amount = 0;
		$s_sub_nano_fee     = 0;
		$s_sub_stripe_fee   = 0;
		$s_sub_total_fee    = 0;

		$sub_sub_total_spent = 0;
		$tokens_spent_total  = 0;

		if ( is_array( $nano_token_meta_results ) && 0 < count( $nano_token_meta_results ) ) {

			$counter = 0;

			foreach ( $nano_token_meta_results as $key => $nano_token_result ) {

				$order_id               = $nano_token_result->order_id;
				$user_id                = $nano_token_result->user_id;
				$idk_project_id         = $nano_token_result->idk_project_id;
				$post_id                = $nano_token_result->post_id;
				$nano_project_uid       = $nano_token_result->nano_project_uid;
				$project_status         = nano_funding_status( $post_id );
				$stripe_cust_id         = get_user_meta( $user_id, 'stripe_customer_id', true );
				$stripe_txn_id          = $nano_token_result->stripe_txn_id;
				$original_order_id      = $nano_token_result->order_id;
				$total_token_refund     = convert_to_numeric( $nano_token_result->token_amount );
				$token_refund_date      = $nano_token_result->token_refund_date;
				$token_expiry_date      = $nano_token_result->token_expiry_date;
				$tokens_first_used_date = $nano_token_result->token_used_date;
				$new_project_ids        = $nano_token_result->nano_new_project_uid;
				$tokens_have_expired    = 'expired' == $nano_token_result->has_expired ? 'EXPIRED' : 'ACTIVE';
				$unused_tokens          = 'ACTIVE' == $tokens_have_expired ? convert_to_numeric( $nano_token_result->tokens_unused ) : '0.00';
				$expired_tokens         = 'EXPIRED' == $tokens_have_expired ? convert_to_numeric( $nano_token_result->tokens_unused ) : '0.00';
				$tot_unused             = '' == $unused_tokens ? 0 : $unused_tokens;
				$tot_expired            = '' == $expired_tokens ? 0 : $expired_tokens;
				$stripe_fee             = convert_to_numeric( $nano_token_result->stripe_fee ) / 100;
				if ( 'EXPIRED' == $tokens_have_expired ) {
					if ( $total_expired <= $total_token_refund ) {
						$tokens_spent = $total_token_refund - $tot_expired;
					} else {
						$tokens_spent = $total_token_refund;
					}
				} else if ( 'ACTIVE' == $tokens_have_expired ) {
					if ( $tot_unused <= $total_token_refund ) {
						$tokens_spent = $total_token_refund - $tot_unused;
					} else {
						$tokens_spent = $total_token_refund;
					}
				}
				$tokens_spent           = convert_to_numeric( $tokens_spent );
				$nano_fee               = convert_to_numeric( nano_single_order_fee( $total_token_refund, $stripe_txn_id ) );
				$total_fee              = convert_to_numeric( $nano_fee + $stripe_fee );

				// Total counters
				$total_donations += $total_token_refund;
				$total_stripe    += $stripe_fee;
				$total_nano      += $nano_fee;
				$total_fees      += $total_fee;
				$total_spent     += $tokens_spent;
				$total_unused    += $tot_unused;
				$total_expired   += $tot_expired;


				// Original Order Details
				$order             = new ID_Member_Order( $original_order_id );
				$donation_date     = $order->get_order()->order_date;
				$org_donation_date = date( get_option( 'date_format' ), strtotime( $donation_date ) );

				// Links
				$nano_project_link = '<a href="' . get_permalink( get_post_id_by_uid( $nano_project_uid ) ) . '">' . $nano_project_uid . '</a>';
				$nano_author_link  = '<a href="' . get_author_posts_url( $user_id ) . '">' . $user_id . '</a> / <a href="https://dashboard.stripe.com' . $url . '/customers/' . $stripe_cust_id . '">' . $stripe_cust_id . '</a>';
				$nano_stripe_link  = false === strpos( $stripe_txn_id, 'Previously' ) ? '<a href="https://dashboard.stripe.com' . $url . '/payments/' . $stripe_txn_id . '">' . $stripe_txn_id . '</a>' : 'FROM TEST CREDITS';
				$nano_order_link   = '<a href="admin.php?page=nano_idc_orders&order_id=' . $order_id . '">' . $order_id . '</a>';

				// Tokens applied to the following projects (if applicable)

				$newly_supported_projects = maybe_unserialize( $new_project_ids );

				$token_data[ $counter ] = array(
					'todays_date'         => nano_get_formated_date_time_gmt( get_option( 'date_format' ) ),
					'date_posted_h'       => str_pad( $counter, 4, '0', STR_PAD_LEFT ),
					'user_link'           => $nano_author_link,
					'donation_date'       => $org_donation_date,
					'order_id'            => $nano_order_link,
					'transaction_id'      => $nano_stripe_link,
					'project_uid'         => $nano_project_link,
					'project_status'      => $project_status,
					'stripe_fee'          => $stripe_fee,
					'nano_fee'            => $nano_fee,
					'total_fee'           => $total_fee,
					'donation_amount'     => $total_token_refund,
					'tokens_spent'        => $tokens_spent,
					'tokens_issued'       => $token_refund_date,
					'tokens_expiry'       => $token_expiry_date,
					'tokens_unused'       => $unused_tokens,
					'expired_credits'     => $expired_tokens,
					'second_projects'     => ( is_array( $newly_supported_projects ) && 0 < count( $newly_supported_projects ) ? 'See Below' : '' ),
					'sub_donation_date'   => '',
					'sub_order_id'        => '',
					'sub_transaction_id'  => '',
					'sub_project_uid'     => '',
					'sub_project_status'  => '',
					'sub_stripe_fee'      => '',
					'sub_nano_fee'        => '',
					'sub_total_fee'       => '',
					'sub_donation_amount' => '',
					'credit_status'       => $tokens_have_expired,
				);

				if ( is_array( $newly_supported_projects ) && 0 < count( $newly_supported_projects ) ) {

					$sub_order_amount = 0;
					$sub_order_total  = 0;
					$sub_nano_fee     = 0;
					$sub_stripe_fee   = 0;
					$sub_total_fee    = 0;

					foreach ( $newly_supported_projects as $newly_supported_project ) {

						if ( strpos( $newly_supported_project, '/' ) !== false ) {

							$project_uids_order_ids = explode( '/', $newly_supported_project );

							$new_project_uid = $project_uids_order_ids[0];
							$new_order_id    = $project_uids_order_ids[1];

							if ( '' !== $new_project_uid && '' !== $new_order_id ) {

								$counter ++;

								// Original Order Details
								$new_order         = new ID_Member_Order( $new_order_id );
								$new_order_details = $new_order->get_order();

								//$new_order_id         = $new_order_details->id;
								$new_user_id          = $new_order_details->user_id;
								$new_post_id          = get_post_id_by_uid( $new_project_uid );
								$new_project_status   = nano_funding_status( $new_post_id );
								$new_order_date       = date( get_option( 'date_format' ), strtotime( $new_order_details->order_date ) );
								$new_stripe_txn_id    = false === strpos( $stripe_txn_id, 'Previously' ) ? 'used-from-' . $stripe_txn_id : 'refunded-from-TEST CREDITS';
								$stripe_unaltered_fee = ID_Member_Order::get_order_meta( $new_order_id, 'stripe_fee', true );
								$new_stripe_fee       = convert_to_numeric( $stripe_unaltered_fee / 100 );
								$new_order_total      = ID_Member_Order::get_order_meta( $new_order_id, 'pwyw_price' );
								$new_order_amount     = $new_order_total >= $total_token_refund ? $total_token_refund : $new_order_total;
								$new_order_amount     = convert_to_numeric( $new_order_amount );
								$new_nano_fee         = nano_single_order_fee( $new_order_amount, $stripe_txn_id );

								// Calculating Stripe Fee and Adding to order:
								$credit_stripe_percentage = $new_order_amount / $total_token_refund;

								if ( 0 < $stripe_fee ) {

									if ( 1 < $credit_stripe_percentage ) {

										$new_stripe_fee = $stripe_fee;

									} else {

										$new_stripe_fee = convert_to_numeric( $stripe_fee * $credit_stripe_percentage );

									}

								}

								$parent_stripe_txn = idc_get_order_meta( $new_order_details->id, 'parent_stripe_txn_id', true );

								if ( '' == $parent_stripe_txn || null == $parent_stripe_txn ) {
									// If the project has achieved its fundraising status then lets give the order the corrected stripe fee
									// Convert stripe fee back to raw stripe fee without decimals for consistency
									$raw_stripe_fee = $new_stripe_fee * 100;
									ID_Member_Order::update_order_meta( $new_order_details->id, 'stripe_fee', $raw_stripe_fee );
									ID_Member_Order::update_order_meta( $new_order_details->id, 'parent_stripe_txn_id', ( 'used-from-' . $stripe_txn_id ) );
								}


								$new_total_fee = $new_nano_fee + $new_stripe_fee;

								// Links
								$new_project_link = '<a href="' . get_permalink( get_post_id_by_uid( $new_project_uid ) ) . '">' . $new_project_uid . '</a>';
								$new_author_link  = '<a href="' . get_author_posts_url( $new_user_id ) . '">' . $new_user_id . '</a> / <a href="https://dashboard.stripe.com' . $url . '/customers/' . $stripe_cust_id . '">' . $stripe_cust_id . '</a>';
								$new_stripe_link  = false === strpos( $stripe_txn_id, 'Previously' ) ? '<a href="https://dashboard.stripe.com' . $url . '/payments/' . $stripe_txn_id . '">' . $stripe_txn_id . '</a>' : $stripe_txn_id;
								$new_order_link   = '<a href="admin.php?page=nano_idc_orders&order_id=' . $new_order_id . '">' . $new_order_id . '</a>';

								$token_data[ $counter ] = array(
									'todays_date'         => '',
									'date_posted_h'       => str_pad( $counter, 4, '0', STR_PAD_LEFT ),
									'user_link'           => 'CREDIT ORDER',
									'donation_date'       => '',
									'order_id'            => '',
									'transaction_id'      => '',
									'project_uid'         => '',
									'project_status'      => '',
									'stripe_fee'          => '',
									'nano_fee'            => '',
									'total_fee'           => '',
									'donation_amount'     => '',
									'tokens_spent'        => '',
									'tokens_issued'       => '',
									'tokens_expiry'       => '',
									'tokens_unused'       => '',
									'second_projects'     => '',
									'expired_credits'     => '',
									'sub_donation_date'   => $new_order_date,
									'sub_order_id'        => $new_order_link,
									'sub_transaction_id'  => $new_stripe_txn_id,
									'sub_project_uid'     => $new_project_link,
									'sub_project_status'  => $new_project_status,
									'sub_stripe_fee'      => $new_stripe_fee,
									'sub_nano_fee'        => convert_to_numeric( $new_nano_fee ),
									'sub_total_fee'       => convert_to_numeric( $new_total_fee ),
									'sub_donation_amount' => ( convert_to_numeric( $new_order_total ) ),
									'credit_status'       => $tokens_have_expired,
								);

								$sub_order_amount += $new_order_amount;
								$sub_order_total  += $new_order_amount;
								$sub_nano_fee     += $new_nano_fee;
								$sub_stripe_fee   += $new_stripe_fee;
								$sub_total_fee    += $new_total_fee;

							}

						}

					}

					// Subs
					$s_sub_order_amount += $sub_order_total;
					$s_sub_nano_fee     += $sub_nano_fee;
					$s_sub_stripe_fee   += $sub_stripe_fee;
					$s_sub_total_fee    += $sub_total_fee;

					// We should do a last cross check of the unsused amounts here as sub order tallies are always correct
					$check_used_tokens      = convert_to_numeric( $nano_token_result->token_amount - $nano_token_result->tokens_unused );
					$check_sub_total_orders = convert_to_numeric( $sub_order_amount );

					if ( $check_used_tokens !== $check_sub_total_orders ) {

						if ( $nano_token_result->token_amount >= $check_sub_total_orders ) {

							$these_unused_tokens = $nano_token_result->token_amount - $check_sub_total_orders;

						} else if ( $nano_token_result->token_amount < $check_sub_total_orders ) {

							$these_unused_tokens = 0;

						}

						global $wpdb;
						$charset_collate = $wpdb->get_charset_collate();
						$table_name      = $wpdb->prefix . 'nano_token_meta_data';

						if ( $wpdb->get_var( "SHOW TABLES LIKE '$table_name'" ) == $table_name ) {

							$result = $wpdb->query( $wpdb->prepare( "UPDATE $table_name SET tokens_unused = %s WHERE order_id = %s", $these_unused_tokens, $order_id ) );

							nano_debug_query( $result, '"UPDATE $table_name SET tokens_unused = %s WHERE order_id = %s"', 'nano_token_tracking' );

						}
					}
				}

				$counter ++;

			}
		}

		// Total Everything Up

		$fin_donations = convert_to_numeric( $total_donations );
		$fin_stripe    = convert_to_numeric( $total_stripe );
		$fin_nano      = convert_to_numeric( $total_nano );
		$fin_total_fee = convert_to_numeric( $total_fees );
		$fin_spent     = convert_to_numeric( $total_spent );
		$fin_unused    = convert_to_numeric( $total_unused );
		$fin_expired   = convert_to_numeric( $total_expired );

		$totals = array(
			'total_donations'  => $fin_donations,
			'total_stripe'     => $fin_stripe,
			'total_nano_fees'  => $fin_nano,
			'total_fees'       => $fin_total_fee,
			'total_spent'      => $fin_spent,
			'total_unused'     => $fin_unused,
			'total_expired'    => $fin_expired,
			'tr_sub_total'     => convert_to_numeric( $s_sub_order_amount ),
			'tr_sub_tripe'     => convert_to_numeric( $s_sub_stripe_fee ),
			'tr_sub_nano'      => convert_to_numeric( $s_sub_nano_fee ),
			'tr_sub_total_fee' => convert_to_numeric( $s_sub_total_fee ),
			'data'             => $token_data,
		);

		return $totals;

	}
}

function nano_token_tracking_original() {

	$test_env = false;

	$date_from_org = isset( $_POST['date_from'] ) ? $_POST['date_from'] : '';
	$date_to       = isset( $_POST['date_to'] ) ? $_POST['date_to'] : '';
	$project_class = isset( $_POST['project_type'] ) ? $_POST['project_type'] : '';

	// Tokens
	$today          = '' !== get_transient( 'token_tracking_today' ) && false !== get_transient( 'token_tracking_today' ) ? get_transient( 'token_tracking_today' ) : nano_get_formated_date_time_gmt( 'm/d/Y' );
	$back           = '' !== get_transient( 'token_tracking_back' ) && false !== get_transient( 'token_tracking_back' ) ? get_transient( 'token_tracking_back' ) : date( 'm/d/Y', strtotime( '-30 days', strtotime( $today ) ) );
	$matching_check = '' !== get_transient( 'token_tracking_type' ) && 'M' === get_transient( 'token_tracking_type' ) ? 'selected="selected"' : '';

	if ( true == $test_env || ! isset( $_POST['submit'] ) ) {
		$date_from_org = $back;
		$date_to       = $today;
		$project_class = '' !== $matching_check ? $matching_check : 'A';
	}

	// First check the nonce, if it fails the function will break
	//check_ajax_referer( 'nano_token_secure', 'security' );

	if ( is_user_logged_in() ) {

		if ( ! current_user_can( 'administrator' ) ) {
			return;
		}

		$settings = get_option( 'memberdeck_gateways' );

		if ( ! empty( $settings ) ) {
			if ( is_array( $settings ) ) {
				$test = $settings['test'];
				if ( 1 == $test ) {
					$url = '/test';
				} else {
					$url = '';
				}
			}
		}

		$token_data = array();

		$date_from_org = date( 'Y-m-d', strtotime( $date_from_org ) );
		$date_to       = date( 'Y-m-d', strtotime( $date_to ) );

		// Collect all regular orders with actual stripe transaction IDs for processing

		$misc = " WHERE order_date >= '" . $date_from_org . " 00:00:00' AND order_date <= '" . $date_to . " 23:59:59' ";

		$orders = ID_Member_Order::get_orders( null, null, ( isset( $misc ) ? $misc : '' ), 'DESC' );

		$count = 0;

		// Total counters
		$total_donations = 0;
		$total_stripe    = 0;
		$total_nano      = 0;
		$total_fees      = 0;
		$total_spent     = 0;
		$total_unused    = 0;

		$sub_sub_total_spent = 0;
		$tokens_spent_total  = 0;

		if ( isset( $orders ) ) {

			$content = '<h4>' . __( 'Credits Meta Data Tracking', 'nanosteam8n' ) . '</h4>';
			$content .= '<div class="table-responsive-sm">';
			$content .= '<table class="table">';
			$content .= '<thead><tr>';
			$content .= '<th>Today\s Date</th><th>Donor ID</th><th>Donation Date</th><th>Transaction ID</th><th>Project ID</th><th>Status</th><th>Stripe fee</th><th>Donation Amount</th><th>Tokens Spent</th><th>Tokens Issued</th><th>Tokens Expiry</th><th>Unused Tokens</th><th>Tokens used on</th>';
			$content .= '</tr></thead>';

			$sub_total_spent_01 = 0;

			foreach ( $orders as $order ) {

				if ( strpos( $order->transaction_id, 'nano-' ) !== false || strpos( $order->transaction_id, 'credit' ) !== false ) {

					$order_id         = $order->id;
					$user_id          = $order->user_id;
					$idk_project_id   = nano_get_project_id( $order->level_id );
					$project          = new ID_Project( $idk_project_id );
					$post_id          = $project->get_project_postid();
					$nano_project_uid = get_post_meta( $post_id, 'project_uid', true );

					// Get only All or nothing or Matching Grant based on user input.
					if ( strpos( $nano_project_uid, $project_class ) !== false ) {

						// Check the transaction ID whether it is a stripe purchase or token
						$transaction_id = $order->transaction_id;

						$project_status       = nano_funding_status( $post_id );
						$stripe_user_id       = get_user_meta( $user_id, 'stripe_customer_id', true );
						$nano_project_id      = '<a href="' . get_permalink( get_post_id_by_uid( $nano_project_uid ) ) . '">' . $nano_project_uid . '</a>';
						$donation_date        = date( get_option( 'date_format' ), strtotime( $order->order_date ) );
						$stripe_unaltered_fee = ID_Member_Order::get_order_meta( $order_id, 'stripe_fee', true ) / 100;
						$stripe_fee           = number_format( $stripe_unaltered_fee, 2, '.', '' );
						$stripe_total         = ID_Member_Order::get_order_meta( $order_id, 'stripe_fee', true );
						$donation_amount      = ID_Member_Order::get_order_meta( $order_id, 'pwyw_price' );
						$donation_amount      = convert_to_numeric( $donation_amount );
						$token_refund_date    = '';
						$token_expiry         = '';
						$tokens_unused        = '';
						$new_project_id       = '';
						$tokens_expired       = '';
						$tokens_spent         = '';

						// Check to see the if the project was unsuccessful
						global $wpdb;
						$table_name = $wpdb->prefix . 'nano_token_meta_data';

						//Totals Setup
						$sub_total_donations = 0;
						$sub_total_stripe    = 0;
						$sub_total_spent     = 0;
						$sub_total_unused    = 0;

						// If transaction is token token based rather than stripe based
						if ( 'credit' == $order->transaction_id ) {

							$sql           = "SELECT * FROM $table_name WHERE user_id LIKE %s";
							$sql           = $wpdb->prepare( $sql, $user_id );
							$token_refunds = $wpdb->get_results( $sql );

							$sub_transaction_id = '';
							$donation_amount    = '';
							$stripe_fee         = '0' == $stripe_fee ? '' : $stripe_fee;
							$sub_total_spent_01 += ID_Member_Order::get_order_meta( $order_id, 'pwyw_price' );
							$sub_total          = ID_Member_Order::get_order_meta( $order_id, 'pwyw_price' );
							$tokens_spent       = number_format( $sub_total, 2, '.', '' );

							foreach ( $token_refunds as $key => $token_ref ) {

								if ( isset( $token_ref->nano_new_project_uid ) && '' != $token_ref->nano_new_project_uid ) {
									$sub_new_project_ids = maybe_unserialize( $token_ref->nano_new_project_uid );

									if ( is_array( $sub_new_project_ids ) ) {

										foreach ( $sub_new_project_ids as $sub_new_project ) {

											$project_uids = $sub_new_project;

											if ( strpos( $project_uids, '/' ) !== false ) {
												$project_uids = explode( '/', $project_uids );
												if ( is_array( $project_uids ) ) {
													$project_uid  = $project_uids[0];
													$sub_order_id = $project_uids[1];
													if ( $sub_order_id == $order_id ) {

														$order_sub      = new ID_Member_Order( $token_ref->order_id );
														$idc_order      = $order_sub->get_order();
														$new_project_id = date( get_option( 'date_format' ), strtotime( $idc_order->order_date ) );

														// get all orders that related to this project to display as references
														$result = '';
														foreach ( $token_refunds as $trans_ids ) {

															if ( isset( $trans_ids->nano_project_uid ) ) {
																$txn_id = $trans_ids->stripe_txn_id;
																if ( $trans_ids->nano_project_uid == $token_ref->nano_project_uid ) {
																	$result .= false === strpos( $txn_id, 'Previously' ) ? '<a href="/wp-admin/admin.php?s=' . $txn_id . '&page=idc-orders&p=1">100' . $trans_ids->order_id . '</a> ' : '100' . $trans_ids->order_id . ' ';
																}
															}

														}

														$trans_id           = $token_ref->stripe_txn_id;
														$link               = false === strpos( $trans_id, 'Previously' ) ? '<a href="https://dashboard.stripe.com' . $url . '/payments/' . $trans_id . '">' . $trans_id . '</a>' : $trans_id;
														$orderid            = '<a href="/wp-admin/admin.php?s=' . $trans_id . '&page=idc-orders&p=1">100' . $order_id . '</a>';
														$orderlinkid        = 'Ref: ' . $result;
														$sub_transaction_id = $orderid . ' / ' . $link;
														$token_expiry       = $token_ref->token_expiry_date;
														$token_refund_date  = $token_ref->token_refund_date;
														$tokens_unused      = $orderlinkid;
														$tokens_expired     = 'expired' == $token_ref->has_expired ? ' / EXPIRED' : ' / LIVE';

														$sub_sub_total_spent += $tokens_spent;

													}
												}
											}
										}
									}
								}

								$transaction_id = $sub_transaction_id;

							}

							$project_status = $project_status . $tokens_expired;

						}

						// If this is a stripe transaction proceed

						if ( false !== strpos( $transaction_id, 'nano-' ) && 'nanosteam Matching Grant' != $transaction_id ) {
							$transaction_id = explode( 'nano-', $transaction_id );
							if ( is_array( $transaction_id ) ) {
								$orderid        = '<a href="/wp-admin/admin.php?s=' . $transaction_id[1] . '&page=idc-orders&p=1">100' . $order_id . '</a>  / ';
								$transaction_id = $orderid . '<a href="https://dashboard.stripe.com' . $url . '/payments/' . $transaction_id[1] . '">' . $transaction_id[1] . '</a>';
							}
						}

						$sql           = "SELECT * FROM $table_name WHERE order_id LIKE %s";
						$sql           = $wpdb->prepare( $sql, $order_id );
						$token_refunds = $wpdb->get_results( $sql );
						$token_refund  = isset( $token_refunds[0] ) ? $token_refunds[0] : null;

						if ( isset( $token_refund ) ) {

							$orderid           = '<a href="/wp-admin/admin.php?s=' . $token_refund->stripe_txn_id . '&page=idc-orders&p=1">100' . $order_id . '</a>  / ';
							$transaction_id    = $orderid . ' ' . ( false === strpos( $token_refund->stripe_txn_id, 'Previously' ) ? ' <a href="https://dashboard.stripe.com' . $url . '/payments/' . $token_refund->stripe_txn_id . '">' . $token_refund->stripe_txn_id . '</a>' : $token_refund->stripe_txn_id );
							$token_refund_date = $token_refund->token_refund_date;
							$token_expiry      = $token_refund->token_expiry_date;
							$tokens_expired    = 'expired' == $token_refund->has_expired ? ' / EXPIRED' : ' / LIVE';
							$tokens_unused     = number_format( $token_refund->tokens_unused, 2, '.', '' );

							if ( isset( $token_refund->nano_new_project_uid ) && '' != $token_refund->nano_new_project_uid ) {

								$new_project_ids = maybe_unserialize( $token_refund->nano_new_project_uid );
								$new_project_id  = '<small>';
								$new_project_id  .= '<p><strong>ID :: Order Amount :: Order Date :: Order #</strong></p>';

								if ( is_array( $new_project_ids ) ) {

									foreach ( $new_project_ids as $new_project ) {

										$project_uid = $new_project;
										if ( strpos( $new_project, '/' ) !== false ) {
											$new_project = explode( '/', $new_project );
											if ( is_array( $new_project ) ) {
												$project_uid = $new_project[0];
												$order_id    = $new_project[1];

											}
										}


										$post_id        = get_post_id_by_uid( $project_uid );
										$project_id     = get_post_meta( $post_id, 'ign_project_id', true );
										$order_sub      = new ID_Member_Order( $order_id );
										$idc_order      = $order_sub->get_order();
										$order_date     = date( get_option( 'date_format' ), strtotime( $idc_order->order_date ) );
										$amount         = number_format( ID_Member_Order::get_order_meta( $order_id, 'pwyw_price' ), 2, '.', '' );
										$order_id       = '<a href="/wp-admin/admin.php?s=' . $idc_order->transaction_id . '&page=idc-orders&p=1">100' . $order_id . '</a>';
										$new_project_id .= '<p><a href="' . get_permalink( $post_id ) . '">' . $project_uid . '</a> :: ' . $amount . ' :: ' . $order_date . ' :: ' . $order_id . '</p>';

									}

								} else {

									$post_id    = get_post_id_by_uid( $new_project_ids );
									$project_id = get_post_meta( $post_id, 'ign_project_id', true );
									$level_id   = nano_get_level_id( $project_id );
									$new_orders = ID_Member_Order::get_orders_by_level( $level_id );

									foreach ( $new_orders as $new_order ) {
										if ( $new_order->user_id == $user_id ) {
											$amount = number_format( ID_Member_Order::get_order_meta( $new_order->id, 'pwyw_price' ), 2, '.', '' );
											if ( $donation_amount > $amount ) {
												$order_date     = date( get_option( 'date_format' ), strtotime( $new_order->order_date ) );
												$order_id       = $new_order->id;
												$new_project_id .= '<p><a href="' . get_permalink( $post_id ) . '">' . $new_project_ids . '</a> / ' . $project_status . ' / ' . $amount . ' / ' . $order_date . ' / ' . $order_id . '</p>';
											}
										}
									}

								}

								$new_project_id .= '</small>';

							}

							//$post_id = get_post_id_by_uid( $project_uid );

							if ( 'REALLOCATED' == $project_status ) {
								//$project_status = 'REFUNDED';
							}

							$project_status   = $project_status . $tokens_expired;
							$sub_total_unused += $tokens_unused;

						}

						/*
						$token_data[ $count ]['todays_date']     = date( get_option( 'date_format' ), time() );
						$token_data[ $count ]['user_link']       = '<a href="' . get_author_posts_url( $user_id ) . '">' . $user_id . '</a> / <a href="https://dashboard.stripe.com' . $url . '/customers/' . $stripe_user_id . '">' . $stripe_user_id . '</a>';
						$token_data[ $count ]['donation_date']   = $donation_date;
						$token_data[ $count ]['transaction_id']  = $transaction_id;
						$token_data[ $count ]['project_status']  = $project_status;
						$token_data[ $count ]['project_uid']     = $nano_project_id;
						$token_data[ $count ]['stripe_fee']      = $stripe_fee;
						$token_data[ $count ]['donation_amount'] = $donation_amount;
						$token_data[ $count ]['tokens_spent']    = $tokens_spent;
						$token_data[ $count ]['tokens_issued']   = $token_refund_date;
						$token_data[ $count ]['tokens_expiry']   = $token_expiry;
						$token_data[ $count ]['tokens_unused']   = $tokens_unused;
						$token_data[ $count ]['second_projects'] = $new_project_id;
						*/

						$other_fees = project_fees( $stripe_fee, $donation_amount );
						$nano_fee   = $other_fees['nanosteam'];
						$nano_fee   = '0' == $nano_fee ? '' : $nano_fee;
						$total_fee  = $other_fees['total'];
						$total_fee  = '0' == $total_fee ? '' : $total_fee;

						$token_data[ $count ] = array(
							'todays_date'     => nano_get_formated_date_time_gmt( get_option( 'date_format' ) ),
							'user_link'       => '<a href="' . get_author_posts_url( $user_id ) . '">' . $user_id . '</a> / <a href="https://dashboard.stripe.com' . $url . '/customers/' . $stripe_user_id . '">' . $stripe_user_id . '</a>',
							'donation_date'   => $donation_date,
							'transaction_id'  => $transaction_id,
							'project_uid'     => $nano_project_id,
							'project_status'  => $project_status,
							'stripe_fee'      => $stripe_fee,
							'nano_fee'        => $nano_fee,
							'total_fee'       => $total_fee,
							'donation_amount' => $donation_amount,
							'tokens_spent'    => $tokens_spent,
							'tokens_issued'   => $token_refund_date,
							'tokens_expiry'   => $token_expiry,
							'tokens_unused'   => $tokens_unused,
							'second_projects' => $new_project_id,
						);

						/*

						function nano_output_to_admin (
							$count, $user_id, $url, $stripe_user_id, $donation_date, $transaction_id,
							$project_status, $nano_project_id, $stripe_fee, $donation_amount, $tokens_spent,
							$token_refund_date, $token_expiry, $tokens_unused, $new_project_id ) {

							$data[$count] = array (
								'todays_date' => date(get_option('date_format'), time()),
								'user_link' => '<a href="' . get_author_posts_url($user_id) . '">' . $user_id . '</a> / <a href="https://dashboard.stripe.com' . $url . '/customers/' . $stripe_user_id . '">' . $stripe_user_id . '</a>',
								'donation_date' => $donation_date,
								'transaction_id' => $transaction_id,
								'project_status' => $project_status,
								'project_uid' => $nano_project_id,
								'stripe_fee' => $stripe_fee,
								'donation_amount' => $donation_amount,
								'tokens_spent' => $tokens_spent,
								'tokens_issued' => $token_refund_date,
								'tokens_expiry' => $token_expiry,
								'tokens_unused' => $tokens_unused,
								'second_projects' => $new_project_id,
							);
						}

						*/

						$content .= '<tr></tr><td>' . nano_get_formated_date_time_gmt( get_option( 'date_format' ) );
						$content .= '<td><a href="' . get_author_posts_url( $user_id ) . '">' . $user_id . '</a> / <a href="https://dashboard.stripe.com' . $url . '/customers/' . $stripe_user_id . '">' . $stripe_user_id . '</a></td>';
						$content .= '<td>' . $donation_date . '</td>';
						$content .= '<td>' . $transaction_id . '</td>';
						$content .= '<td>' . $project_status . '</td>';
						$content .= '<td>' . $nano_project_id . '</td>';
						$content .= '<td>' . $stripe_fee . '</td>';
						$content .= '<td>' . $donation_amount . '</td>';
						$content .= '<td>' . $tokens_spent . '</td>';
						$content .= '<td>' . $token_refund_date . '</td>';
						$content .= '<td>' . $token_expiry . '</td>';
						$content .= '<td>' . $tokens_unused . '</td>';
						$content .= '<td>' . $new_project_id . '</td></tr>';

						$count ++;

						$total_donations += convert_to_numeric( $donation_amount );
						$total_stripe    += convert_to_numeric( $stripe_fee );
						$total_nano      += convert_to_numeric( $nano_fee );
						$total_fees      += convert_to_numeric( $total_fee );
						$total_unused    += convert_to_numeric( $sub_total_unused );

					}
				}
			}

			$content .= '</div>';
			$content .= '</table>';

		}

		// Total Everything Up

		$fin_donations = convert_to_numeric( $total_donations );
		$fin_stripe    = convert_to_numeric( $total_stripe );
		$fin_nano      = convert_to_numeric( $total_nano );
		$fin_total_fee = convert_to_numeric( $total_fees );
		$fin_spent     = convert_to_numeric( $sub_total_spent_01 );
		$fin_unused    = convert_to_numeric( $total_unused );

		$totals = array(
			'total_donations' => $fin_donations,
			'total_stripe'    => $fin_stripe,
			'total_nano_fees' => $fin_nano,
			'total_fees'      => $fin_total_fee,
			'total_spent'     => $fin_spent,
			'total_unused'    => $fin_unused,
			'data'            => $token_data,
		);

		return $totals;

	}
}


function nano_idc_orders() {
	#devnote timestamps should be stored in UTC
	$default_tz = get_option( 'timezone_string' );
	if ( empty( $default_tz ) ) {
		$default_tz = "UTC";
	}
	$tz = new DateTimeZone( $default_tz );
	// number of results to show per page
	$row_count = 20;
	// what's on the page now?
	$query = $_SERVER['QUERY_STRING'];
	// quick query to get number of orders
	$order_count = ID_Member_Order::get_order_count();
	if ( ! empty( $order_count ) ) {
		$order_count = $order_count->count;
	} else {
		$order_count = 0;
	}
	// handle search query
	if ( isset( $_GET['s'] ) ) {
		$search = sanitize_text_field( $_GET['s'] );
	} else {
		$search = null;
	}
	// what page are we on?
	if ( isset( $_GET['p'] ) ) {
		$page = absint( $_GET['p'] );
	} else {
		$page = 1;
	}
	// calculate number of total pages
	$pages = ceil( ( $order_count ) / $row_count );
	// now handle where our next page arrows should point to
	if ( $pages == 0 ) {
		$pages = 1;
	}
	if ( $page < $pages ) {
		$nextp = $page + 1;
	} else {
		$nextp = $page;
	}
	if ( $page == 1 ) {
		$limit = '0, ' . ( $row_count - 1 );
		// back arrow
		$lastp = 1;
	} else {
		// calculate limit based on current page number and row count
		$start = ( $page * 20 ) - $row_count;
		$end   = ( $row_count * $page ) - 1;
		$limit = $start . ', ' . $end;
		// back arrow
		$lastp = $page - 1;
	}
	parse_str( $query, $query_array );
	$query_last       = $query_array;
	$query_first      = $query_array;
	$query_next       = $query_array;
	$query_prev       = $query_array;
	$query_last['p']  = $pages;
	$query_last       = http_build_query( $query_last );
	$query_first['p'] = 1;
	$query_first      = http_build_query( $query_first );
	$query_next['p']  = $nextp;
	$query_next       = http_build_query( $query_next );
	$query_prev['p']  = $lastp;
	$query_prev       = http_build_query( $query_prev );

	$column = 'transaction_id';
	$search = null;
	$limit  = null;
	$misc   = null;
	$sort   = 'ASC';

	if ( isset( $_GET['user_id'] ) ) {
		$column = 'user_id';
		$search = absint( $_GET['user_id'] );
	}

	if ( isset( $_GET['order_id'] ) ) {
		$column = 'id';
		$search = absint( $_GET['order_id'] );
	}

	global $wpdb;
	$sql     = 'SELECT * FROM ' . $wpdb->prefix . 'memberdeck_orders' . $misc . ( ! empty( $search ) ? ' WHERE ' . $column . ' LIKE "%' . $search . '%"' : '' ) . ' ORDER BY id ' . $sort . ( ! empty( $limit ) ? ' LIMIT ' . $limit : '' );
	$results = $wpdb->get_results( $sql );

	$count = 0;

	foreach ( $results as $result ) {

		$order_id       = $result->id;
		$order_date     = $result->order_date;
		$user_id        = $result->user_id;
		$user_email     = false !== get_user_by( 'id', $user_id ) ? get_user_by( 'id', $user_id )->user_email : '';
		$product_id     = '' !== $result->level_id ? (string) nano_project_id_by_order_level( $result->level_id ) : '';
		$product_name   = 0 < $product_id ? ( mb_strlen( nano_get_project_name( $product_id ) ) > 40 ? substr( nano_get_project_name( $product_id ), 0, 40 ) . '...' : nano_get_project_name( $product_id ) ) : '';
		$transaction_id = $result->transaction_id;
		$order_status   = $result->status;

		$price_check = 0 < ID_Member_Order::get_order_meta( $order_id, 'pwyw_price' ) ? ID_Member_Order::get_order_meta( $order_id, 'pwyw_price' ) : $result->price;
		$price       = ( false !== strpos( $transaction_id, 'nano' ) || false !== strpos( $transaction_id, 'credit' ) ? 'CR ' : '$' ) . convert_to_numeric( $price_check );


		$totals[ $count ] = array(
			'order_id'       => $order_id,
			'date_posted_h'  => $order_date,
			'order_date'     => $order_date,
			'user_id'        => $user_id,
			'user_email'     => $user_email,
			'product_name'   => $product_name,
			'price'          => $price,
			'transaction_id' => $transaction_id,
			'order_status'   => $order_status,
		);

		$count ++;
	}

	$data['table_data'] = $totals;

	return $data;

}


/**
 *
 * List all donations received in the calendar year broken down by month
 */

function nano_get_orders( $search = null, $limit = null, $misc = null, $sort = 'ASC' ) {
	global $wpdb;
	$sql = 'SELECT * FROM ' . $wpdb->prefix . 'memberdeck_orders' . $misc . ( ! empty( $search ) ? ' WHERE order_date LIKE "%' . $search . '%"' : '' ) . ' ORDER BY id ' . $sort . ( ! empty( $limit ) ? ' LIMIT ' . $limit : '' );
	$res = $wpdb->get_results( $sql );

	return $res;
}

function nano_donations_received() {

	if ( is_user_logged_in() ) {

		if ( ! current_user_can( 'administrator' ) ) {
			return;
		}

		$project_types      = array( 'A', 'M', 'C', 'O' );
		$project_type_names = array( 'All or northing', 'Matching Grant', 'Challenge', 'Other' );
		$date_from_year     = isset( $_POST['date_from'] ) ? $_POST['date_from'] : '2019';
		$all_orders         = nano_get_orders( $date_from_year );

		$total_received = 0;
		$total_fee      = 0;
		$total_stripe   = 0;
		$total_nano     = 0;
		$total_after    = 0;
		$month          = 01;

		$sum = 0;
		// Breaking this down per month
		$count = 0000;

		for ( $i = 1; $i <= 12; $i ++ ) {

			$sub_total  = 0;
			$sub_stripe = 0;
			$sub_nano   = 0;
			$sub_fee    = 0;
			$sub_after  = 0;

			$dateObj   = DateTime::createFromFormat( '!m', $i );
			$monthName = $dateObj->format( 'F' ); // March

			// Breaking this down by project type
			foreach ( $project_types as $project_type_key => $project_type ) {

				$total_sub_received = 0;
				$total_sub_fee      = 0;
				$total_sub_stripe   = 0;
				$total_sub_nano     = 0;
				$total_sub_after    = 0;

				foreach ( $all_orders as $key => $order ) {

					if ( strpos( $order->transaction_id, 'nano-' ) !== false ) {

						$month_number = date( 'm', strtotime( $order->order_date ) );
						$year_number  = date( 'Y', strtotime( $order->order_date ) );


						$post_id     = nano_post_id_by_order_level_id( $order->level_id );
						$project_uid = get_post_meta( $post_id, 'project_uid', true );

						if ( $month_number == $i && false !== strpos( $project_uid, $project_type ) && false !== strpos( $year_number, $date_from_year ) ) {

							$project_id = nano_get_project_id( $order->level_id );

							// Order Amount
							$amount = idc_get_order_meta( $order->id, 'pwyw_price', true );

							// Stripe Fee
							$stripe_raw_fee = idc_get_order_meta( $order->id, 'stripe_fee', true );
							$stripe_fee     = null !== $stripe_raw_fee || '' !== $stripe_raw_fee ? $stripe_raw_fee / 100 : 0.00;

							// Nanosteam Fee
							$nano_fee = nano_single_order_fee( $amount, $order->transaction_id );

							$total_sub_received   += $amount;
							$total_sub_fee        += ( $nano_fee + $stripe_fee );
							$total_sub_nano       += $nano_fee;
							$total_sub_stripe     += $stripe_fee;
							$total_sub_after      += $amount - ( $nano_fee + $stripe_fee );
							$month_name[ $count ] = date( 'F', strtotime( $order->order_date ) );


						}

					}

				}

				$type       = $project_type_names[ $project_type_key ];
				$month      = 0 == $project_type_key ? $monthName : '';
				$sub_total  += $total_sub_received;
				$sub_fee    += $total_sub_fee;
				$sub_nano   += $total_sub_nano;
				$sub_stripe += $total_sub_stripe;
				$sub_after  += $total_sub_after;


				$totals[ $count ] = array(
					'month'          => $month,
					'date_posted_h'  => str_pad( $count, 4, '0', STR_PAD_LEFT ),
					'type'           => $project_type,
					'd_total_funded' => number_format( $total_sub_received, '2', '.', '' ),
					'd_total_fees'   => number_format( $total_sub_fee, '2', '.', '' ),
					'd_nano_fees'    => number_format( $total_sub_nano, '2', '.', '' ),
					'd_stripe_fees'  => number_format( $total_sub_stripe, '2', '.', '' ),
					'd_after_fees'   => number_format( $total_sub_after, '2', '.', '' ),
				);

				$count ++;

			}

			$total_received += $sub_total;
			$total_fee      += $sub_fee;
			$total_stripe   += $sub_nano;
			$total_nano     += $sub_stripe;
			$total_after    += $sub_after;

		}

		$data                       = array();
		$data['table_data']         = $totals;
		$_SESSION['d_total_funded'] = $total_received;
		$_SESSION['d_total_fees']   = $total_fee;
		$_SESSION['d_nano_fees']    = $total_stripe;
		$_SESSION['d_stripe_fees']  = $total_stripe;
		$_SESSION['d_after_fees']   = $total_after;


		return $data;

	}

}

function nano_donations_paid() {

	if ( is_user_logged_in() ) {

		if ( ! current_user_can( 'administrator' ) ) {
			return;
		}

		$project_types      = array( 'A', 'M', 'C', 'O' );
		$project_type_names = array( 'All or northing', 'Matching Grant', 'Challenge', 'Other' );
		$date_from_year     = isset( $_POST['date_from'] ) ? $_POST['date_from'] : '2019';
		$all_orders         = nano_get_orders( $date_from_year );

		$total_received = 0;
		$matching_first = 0;
		$matching_final = 0;
		$month          = 01;

		// Breaking this down per month
		$count = 0000;

		for ( $i = 1; $i <= 12; $i ++ ) {

			$sub_total = 0;
			$sub_first = 0;
			$sub_final = 0;

			$dateObj   = DateTime::createFromFormat( '!m', $i );
			$monthName = $dateObj->format( 'F' ); // March

			// Breaking this down by project type
			foreach ( $project_types as $project_type_key => $project_type ) {

				$total_sub_received = 0;
				$total_sub_first    = 0;
				$total_sub_final    = 0;


				// Collect all project of a certain type
				$completed = 'COMPLETED';
				$type      = " WHERE type = '" . $project_type . "' AND BINARY project_status LIKE '%" . $completed . "%' ";

				// Tabular Data

				global $wpdb;
				// this adds the prefix which is set by the user upon instillation of WP
				$table_name = $wpdb->prefix . 'nano_meta_data' . $type;
				// this will get the data from your table

				$sql            = "SELECT * FROM $table_name";
				$retrieved_data = $wpdb->get_results( $sql );

				foreach ( $retrieved_data as $data ) {

					// From the table
					$date_posted       = date( 'm/d/Y', strtotime( $data->time ) );
					$project_uid       = $data->project_uid;
					$creator_id        = $data->creator_id;
					$creator_stripe_id = $data->creator_stripe_id;
					$post_id           = $data->post_id;
					$project_status    = unserialize( $data->project_status );
					$payments          = unserialize( $data->payments );
					$target_goal       = get_post_meta( $post_id, 'ign_fund_goal', true );
					$date_posted_h     = strtotime( $data->time );

					//Project Parameters
					$level_id = nano_get_level_id( get_post_meta( $post_id, 'ign_project_id', true ) );
					$orders   = ID_Member_Order::get_orders_by_level( $level_id );

					// FEES & TOTALS
					$fees              = nano_fee_list( $orders, $post_id );
					$total_fund_raised = $fees['total_before_fees'];
					$stripe_fees       = $fees['stripe_fees'];
					$nanosteam_fees    = $fees['nanosteam_fees'];
					$total_fees        = $fees['total_fees'];
					$total_after_fees  = $fees['total_after_fees'];

					// Payout Status
					$project_success_status       = get_post_meta( $post_id, 'ign_project_success', true ) == '1' ? true : false;
					$project_fund_goal            = get_post_meta( $post_id, 'ign_fund_goal', true );
					$project_fund_raised          = $total_fund_raised;
					$project_fund_status          = absint( $project_fund_raised ) >= absint( $project_fund_goal ) ? true : false;
					$matching_grant_project_check = get_post_meta( $post_id, 'fifty_fifty_grant', true ) == 'approved' ? true : false;
					$second_milestone_check       = nano_second_milsetone_achieved( $post_id );

					// Project Payment Statuses
					// Matching Grant
					$first_transfer_processed_id = get_post_meta( $post_id, 'first_transfer_transaction_id', true );
					$first_transfer_initiated    = get_post_meta( $post_id, 'first_transfer_transaction_initiated', true );
					$first_transfer_delivered    = get_post_meta( $post_id, 'first_transfer_transaction_delivered', true );

					$final_transfer_processed_id = get_post_meta( $post_id, 'final_transfer_transaction_id', true );
					$final_transfer_initiated    = get_post_meta( $post_id, 'final_transfer_transaction_date_initiated', true );
					$final_transfer_delivered    = get_post_meta( $post_id, 'final_transfer_transaction_date_delivered', true );

					// All In
					$standard_transfer_processed_id = get_post_meta( $post_id, 'standard_transfer_transaction_id', true );
					$standard_transfer_initiated    = get_post_meta( $post_id, 'standard_transfer_transaction_date_initiated', true );
					$standard_transfer_delivered    = get_post_meta( $post_id, 'standard_transfer_transaction_date_delivered', true );

					if ( false == $matching_grant_project_check ) {

						if ( $i === (int) date( 'm', strtotime( $standard_transfer_delivered ) ) && $date_from_year == date( 'Y', strtotime( $standard_transfer_delivered ) ) ) {

							$total_sub_received += $total_after_fees;

						}

					} elseif ( true === $matching_grant_project_check ) {

						if ( $i === (int) date( 'm', strtotime( $first_transfer_delivered ) ) && strpos( $first_transfer_delivered, $date_from_year ) !== false ) {

							$total_sub_received += $total_after_fees;
							$total_sub_first    += ( $total_fund_raised / 2 ) - $total_fees;

						}

						if ( $i === (int) date( 'm', strtotime( $final_transfer_delivered ) ) && strpos( $final_transfer_delivered, $date_from_year ) !== false ) {

							$total_sub_final += ( $total_fund_raised / 2 );

						}

					}

				}

				$type      = $project_type_names[ $project_type_key ];
				$month     = 0 == $project_type_key ? $monthName : '';
				$sub_total += $total_sub_received;
				$sub_first += $total_sub_first;
				$sub_final += $total_sub_final;

				$totals[ $count ] = array(
					'month'          => $month,
					'date_posted_h'  => str_pad( $count, 4, '0', STR_PAD_LEFT ),
					'type'           => $project_type,
					'p_total_funded' => number_format( $total_sub_received, '2', '.', '' ),
					'p_total_first'  => number_format( $total_sub_first, '2', '.', '' ),
					'p_total_final'  => number_format( $total_sub_final, '2', '.', '' ),
				);

				$count ++;

			}

			$total_received += $sub_total;
			$matching_first += $sub_first;
			$matching_final += $sub_final;

		}

		$data                       = array();
		$data['table_data']         = $totals;
		$_SESSION['p_total_funded'] = number_format( $total_received, '2', '.', '' );
		$_SESSION['p_total_first']  = number_format( $matching_first, '2', '.', '' );
		$_SESSION['p_total_final']  = number_format( $matching_final, '2', '.', '' );

		return $data;

	}

}