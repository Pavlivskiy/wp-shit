jQuery( document ).ready( function( $ ) {
	let values = [],
		totals = [],
		budgetValue = $( '.svg_val' ),
		budx = 1, //initial text box count
		position = 'bottom';
	const budMaxFields = 10, //maximum input boxes allowed
		budWrapper = $( '.budget-wrapper' ), //Fields wrapper
		addButton = $( '.add_budget_button' ), //Add button ID;
		svgContainer = $( '#budget_chart' );
	// Additional Fields Function
	if ( svgContainer.length ) {
		if ( ! svgContainer.hasClass( 'pub' ) ) {
			// This is the project creation flow form fields
			budgetValue.each( function() {
				values.push( $( this ).closest( '.form-row' ).find( 'input[name=\'nano_budget_title[]\']' ).val() );
				totals.push( $( this ).val() );
			} );
		} else {
			// This is the public facing data
			budgetValue = php_budget;
			values = budgetValue.label;
			totals = budgetValue.val;
			position = 'left';
		}

		const ctx = document.getElementById( 'budget_chart' ).getContext( '2d' );
		const myChart = new Chart( ctx, {
			type: 'doughnut',
			data: {
				labels: values,
				datasets: [ {
					label: 'Budget',
					data: totals,
					backgroundColor: [
						'#FFB400',
						'#ffe900',
						'#3dc644',
						'#017574',
						'#001d79',
						'#005dcb',
						'#00bdd9',
						'#7f2ca2',
						'#e83e8c',
						'#d72229',
					],
					borderColor: [
						'#ffffff',
					],
					borderWidth: 1,
				} ],
			},
			options: {
				cutoutPercentage: 20,
				tooltips: {
					enabled: true,
					mode: 'single',
					callbacks: {
						label: function( tooltipItems, data ) {
							return tooltipItems.yLabel + ' $' + data.datasets[ 0 ].data[ tooltipItems.index ];
						},
					},
				},
				legend: {
					display: false,
					position: position, // top, left, bottom, right
					fullWidth: true,
					responsive: true,
					maintainAspectRatio: true,
					labels: {
						boxWidth: 10,
					},
				},
				legendCallback: function( chart ) {
					// Return the HTML string here.

					if ( chart.data.datasets[ 0 ].data.length > 0 ) {
						const text = [];
						const longest = chart.data.datasets[ 0 ].data.reduce( function( a, b ) {
							return a.length > b.length ? a : b;
						} );
						let width = '40px'; // Good for $30 (2 characters)
						text.push( '<ul class="' + chart.id + '-legend list-unstyled">' );

						width = ( longest.length + 2 ) + '0px';

						for ( let i = 0; i < chart.data.datasets[ 0 ].data.length; i++ ) {
							const colorVal = chart.data.datasets[ 0 ].backgroundColor[ i ];
							text.push( '<li class="row mb-1">' +
                                '<div class="col" style="color:' + colorVal + '; max-width: 15px"><i class="fa fa-circle"></i></div>' +
                                '<div class="col pr-0" style="color:' + colorVal + '; max-width: ' + width + '"><strong>$' + chart.data.datasets[ 0 ].data[ i ] + '</strong></div>' );
							if ( chart.data.labels[ i ] ) {
								text.push( '<div class="col-9 col-xl-10">' + chart.data.labels[ i ] + '</div>' );
							}
							text.push( '</li>' );
						}
						text.push( '</ul>' );
						return text.join( '' );
					}
				},
			},
		} );

		$( '#legend' ).html( myChart.generateLegend() );

		function updateData( chart, label, data ) {
			chart.data.labels = label;
			chart.data.datasets.forEach( ( dataset ) => {
				dataset.data = data;
			} );
			chart.update();
		}

		function removeData( chart ) {
			chart.data.labels.pop();
			chart.data.datasets.forEach( ( dataset ) => {
				dataset.data.pop();
			} );
			chart.update();
		}

		// Add budget field
		addButton.click( function( e ) { //on add input button click
			e.preventDefault();
			if ( budx < budMaxFields ) { //max input box allowed
				budx++; //text box increment
				budWrapper.append( '<div class="form-group fadein form-row"><div class="formval col-sm-9 col-7"><label class="sr-only" for="">Budget Title</label><input type="text" id="" name="nano_budget_title[]" class="form-control form-control-sm budget_title" value="" maxlength="140" placeholder="Title" aria-label="Budget Title" data-fv-field="nano_budget_title[]"></div><div class="mb-1 mb-sm-0 formval col-sm-3 col-5"><label class="sr-only" for="nano_budget_value_">Budget Value</label><div class="input-group"><input type="text" name="nano_budget_value[]" class="form-control form-control-sm budgetValue svg_val" value="" placeholder="$" aria-label="Budget Value"></div></div>' ); //add input box
				piechart();
			}
		} );

		//Destroy budget field (not used but for reference for when we had buttons
		budWrapper.on( 'click', '.remove_field', function( e ) { //user click on remove text
			e.preventDefault();
			jQuery( this ).closest( '.form-row' ).remove();
			budx--;
		} );

		// Checking to ensure only numbers are used
		budWrapper.on( 'keyup', '.svg_val', function( e ) {
			var charCode = ( 'undefined' === typeof e.which ) ? e.keyCode : e.which,
				budgetValue = $( this ),
				budgetVal = budgetValue.val(),
				e = e || event;

			if ( ( 1 === this.value.length || 0 === this.value.length ) && ( 48 === charCode || 96 === charCode ) ) {
				if ( 48 === charCode || 96 === charCode ) {
					$( this ).val( '' );
				}

				return false;
			}

			if ( this.value === '00' || this.value === '0' ) {
				$( this ).val( '' );
			}

			if ( -1 !== $.inArray( charCode, [ 46, 8, 9, 27, 13, 110 ] ) ||

                // Allow: Ctrl+A, Command+A
                ( 65 === charCode && ( true === e.ctrlKey || true === charCode ) ) ||

                // Allow: home, end, left, right, down, up
                ( 35 <= charCode && 40 >= charCode ) ) {
				// let it happen, don't do anything
				return;
			}

			// Ensure that it is a number and stop the keypress
			if ( ( e.shiftKey || ( 48 > charCode || 57 < charCode ) ) && ( 96 > charCode || 105 < charCode ) ) {
				if ( charCode === 190 || charCode === 110 ) {
					budgetValue.popover( 'hide' );
					$( this ).attr( 'data-content', 'Sorry, only whole number, no decimals allowed.' );
					$( this ).attr( 'data-toggle', 'popover' );
					$( this ).attr( 'data-html', 'true' );
					$( this ).popover( 'show' );
					$( this ).on( 'blur click', function() {
						$( this ).popover( 'hide' );
					} );
				}

				const stringedUp = budgetVal.toString();
				const adjusted = stringedUp.slice( 0, -1 );

				$( this ).val( adjusted );
			}
		} );

		//Destroy budget fields if all content removed from the fields
		budWrapper.on( 'change', '.svg_val, .budget_title', function( ) {
			const ulparent = jQuery( '.svg_container li' );
			let title = jQuery( this ).closest( '.form-group' ).find( '.budget_title' ).val();
			let value = jQuery( this ).closest( '.form-group' ).find( '.svg_val' ).val();
			const fields = jQuery( '.budget-wrapper .form-group' ).length;

			// Remove if field is emptied
			if ( undefined === title ) {
				title = 0;
			}
			if ( undefined === value ) {
				value = 0;
			}

			if ( 0 === title.length && 0 === value.length && fields >= 3 ) {
				jQuery( this ).closest( '.form-group' ).fadeOut( 'slow', function() {
					jQuery( this ).remove();

					if ( jQuery( '.budget_title' ).first().val() ) {
						jQuery( '.budget_title' ).first().click();
					} else {
						jQuery( '.budget_title' ).last().click();
					}
				} );
			}

			return false;
		} );

		function piechart() {
			// Lets check the values of the inputs
			let projectGoalContainer = $( '#project_goal' ),
				dataValContainer = $( '.dataval' ),
				goalInt, celTotal,
				totalInt,
				singlecelTotal,
				i,
				fields = jQuery( '.budget-wrapper .form-group' ).length;

			budWrapper.on( 'change keyup click', '.svg_val, .budget_title', function( ) {
				// Modifying
				let total = 0,
					goal = projectGoalContainer.length ? projectGoalContainer.val() : dataValContainer.data( 'budget' ),
					sumContainer = dataValContainer,
					values = [],
					totals = [],
					budgetValue = $( '.svg_val' ),
					label = $( this ).closest( '.form-row' ).find( 'input[name=\'nano_budget_title[]\']' ).val(),
					val = $( this ).val();

				budgetValue.each( function() {
					values.push( $( this ).closest( '.form-row' ).find( 'input[name=\'nano_budget_title[]\']' ).val() );
					totals.push( $( this ).val() );
				} );

				for ( i = 0; i < totals.length; i++ ) {
					singlecelTotal = '' !== totals[ i ] ? parseInt( totals[ i ] ) : 0;

					total += singlecelTotal;

					goalInt = parseInt( goal );
					totalInt = parseInt( total );

					if ( 0 !== goal && totals[ i ] > ( goalInt - total ) && 0 > ( goalInt - total ) ) {
						budgetValue.popover( 'hide' );
						celTotal = parseInt( $( this ).val() );
						$( this ).attr( 'data-content', 'Your entered amount exceeded the remaining from your budget. We have adjusted this amount to be in line with the total. Please check your amounts to be sure.' );
						$( this ).attr( 'data-toggle', 'popover' );
						$( this ).popover( 'show' );
						if ( celTotal + ( goalInt - total ) !== 0 ) {
							$( this ).val( celTotal + ( goalInt - total ) );
						} else if ( celTotal + ( goalInt - total ) === 0 ) {
							$( this ).val( '0' );
						}
						total = goalInt;
						$( this ).on( 'blur click', function() {
							$( this ).popover( 'hide' );
						} );
					}
				}

				if ( 0 !== goal && 0 < goal ) {
					sumContainer.text( ( goalInt - total ) );
					$( '.budget_tally' ).addClass( 'show' );
				}

				updateData( myChart, values, totals );
			} );

			budWrapper.on( 'change keyup click', '.svg_val', function( ) {
				// Modifying
				const goal = projectGoalContainer.length ? projectGoalContainer.val() : dataValContainer.data( 'budget' ),
					budgetValue = $( this );

				if ( '' === goal ) {
					budgetValue.popover( 'hide' );
					$( this ).attr( 'data-content', 'You can\'t enter your budget values until you have a <a id="funding-lnk" href="">funding goal amount</a> added.' );
					$( this ).attr( 'data-toggle', 'popover' );
					$( this ).attr( 'data-html', 'true' );
					$( this ).popover( 'show' );
					$( this ).val( '' );
					$( this ).on( 'blur click', function() {
						$( this ).popover( 'hide' );
					} );
					$( '#funding-lnk' ).on( 'click', function( e ) {
						e.preventDefault();
						// Get plugin instance
						const fv = jQuery( '#fes' ).data( 'formValidation' );
						fv.validateField( 'project_goal' );
						$( '#funding-tab' ).tab( 'show' );
					} );

					return false;
				}
			} );
		}

		piechart();
	}
} );
