jQuery( document ).ready( function() {

    var projectImage = jQuery( '.nano-project-image' ),
    projectImageUrl = jQuery( '.image_url' );

    projectImage.on( 'mouseup', '.remove_image', function() {
        jQuery( '.nano_team_leader, input[name="project_main"]' ).addClass( 'custom-file-input' );
    });

    jQuery( '.project_img' ).on( 'click', function() {
        jQuery( 'input[name="project_main"]' ).click();
    });

    jQuery( '.nano-team-leader-image' ).on( 'click', '.teamlead_img', function() {
        jQuery( 'input[name="nano_team_leader"]' ).click();
    });

    jQuery( '.avatar_img' ).on( 'click', function() {
        jQuery( '#nano_idc_avatar' ).click();
    });

    if ( projectImageUrl.length ) {
        projectImageUrl.hide();
    }

    /* ------------------------
     *  Image Cropping for Profile Image
     *
     ------------------------------*/

    function nanoCropPhoto( wrapper, hidden, type, postFile ) {

        var $uploadCrop,
            $hiddenUpload,
            $size,
            $imageFormat,
            $uploadSize,
            $postFile = postFile,
            hiddenCropContainer = jQuery( '.hidden_crop', wrapper ),
            croppieContainer = jQuery( '.upload-container', wrapper ),
            nanoUpload = jQuery( wrapper ),
            nanoFileInput = jQuery( 'input[type="file"]', wrapper ),
            mainHidden = jQuery( hidden ),
            saveButton = jQuery( 'button[type="submit"]' );

        nanoFileInput.addClass( 'custom-file-input' );
        croppieContainer.croppie( 'destroy' );
        hiddenCropContainer.croppie( 'destroy' );
        nanoUpload.removeClass( 'ready' );
        saveButton.attr( 'disabled', true );

        if ( 'circle' === type ) {

        $hiddenUpload = hiddenCropContainer.croppie({
            type: 'blob',
            enableExif: true,
            enableOrientation: true,
            showZoomer: false,
            enableZoom: false,
            viewport: {
                width: 150,
                height: 150,
                type: 'circle'
            }
        });

        $uploadCrop = croppieContainer.croppie({
            type: 'blob',
            enableExif: true,
            enableOrientation: true,
            mouseWheelZoom: false,
            viewport: {
                width: 150,
                height: 150,
                type: 'circle'
            }
        });

        $imageFormat = 'png';
        $size = 150;
        $uploadSize = { width: 150, height: 150 };

        } else {

            $hiddenUpload = hiddenCropContainer.croppie({
                type: 'blob',
                enableExif: true,
                enableOrientation: true,
                showZoomer: false,
                enableZoom: false,
                viewport: {
                    width: 835,
                    height: 446,
                    type: 'square'
                }
            });

            $uploadCrop = croppieContainer.croppie({
                type: 'blob',
                enableExif: true,
                enableOrientation: true,
                mouseWheelZoom: false,
                viewport: {
                    width: 250,
                    height: 134,
                    type: 'square'
                }
            });

            $imageFormat = 'jpeg';
            $size = 800;
            $uploadSize = { width: 835, height: 446 };

        }

        function readFile( input ) {

            var reader = new FileReader(),
                container = croppieContainer.parent().parent();

            if ( input.files && input.files[ 0 ]) {

                reader.onload = function( e ) {

                    var image = new Image();
                    image.src = window.URL.createObjectURL( input.files[ 0 ]);

                    image.onload = function() {

                        var width = image.naturalWidth;

                        if ( width < $size && 0 !== width ) {

                            if ( jQuery( '.upload-msg', wrapper ).length ) {
                                nanoUpload.removeClass( 'ready' );
                                jQuery( '.upload-msg', wrapper ).html( '<p class="text-danger" style="padding: 20px;">Your image is too small, please upload a larger image with a pixel width of at least ' + $size + '</p>' );
                            } else {
                                nanoUpload.removeClass( 'ready' );
                                nanoFileInput.append( '<div class="upload-msg text-danger project_img"><p style="padding: 20px;">Your image is too small, please upload a larger image with a pixel width of at least ' + $size + '</p></div>' );
                            }

                        } else {

                            nanoUpload.addClass( 'ready' );
                            croppieContainer.addClass( 'show active' );
                            reloadStylesheets();

                            if ( croppieContainer.hasClass( 'active' ) ) {

                                $uploadCrop.croppie( 'bind', {
                                    url: e.target.result
                                }).then( function() {
                                    //reloadStylesheets();
                                    //saveButton.attr( 'disabled', true );
                                });

                            }

                        }
                    };

                };

                reader.readAsDataURL( input.files[ 0 ]);


            } else {
                swal( 'Sorry - you\'re browser doesn\'t support the FileReader API' );
            }
        }

        function reloadStylesheets() {

            // Doing this because on first load the CSS does not load for the added elements causing misplaced elements
            jQuery( '<style>' +
                '.croppie-container {' +
                'width: 100%;' +
                'height: 100%;' +
                '}' +
                '.croppie-container .cr-image { z-index: -1;' +
                'position: absolute;' +
                'top: 0;' +
                'left: 0;' +
                'transform-origin: 0 0;' +
                'max-height: none;' +
                'max-width: none; }' +
                ' .croppie-container .cr-boundary {' +
                'position: relative;' +
                'overflow: hidden;' +
                'margin: 0 auto;' +
                'z-index: 1;' +
                'width: 100%;' +
                'height: 100%;' +
                '}' +
                '.croppie-container .cr-slider-wrap {' +
                'width: 75%;' +
                'margin: 15px auto;' +
                'text-align: center;' +
                '}.croppie-container .cr-viewport, .croppie-container .cr-resizer {\n' +
                '\n' +
                '    position: absolute;\n' +
                '    border: 2px solid #fff;\n' +
                '    margin: auto;\n' +
                '    top: 0;\n' +
                '    bottom: 0;\n' +
                '    right: 0;\n' +
                '    left: 0;\n' +
                '    box-shadow: 0 0 2000px 2000px rgba(0,0,0,.5);\n' +
                '    z-index: 0;\n' +
                '\n' +
                '}.croppie-container .cr-overlay {\n' +
                '\n' +
                '    z-index: 1;\n' +
                '    position: absolute;\n' +
                '    cursor: move;\n' +
                '    touch-action: none;\n' +
                '\n' +
                '}</style>' ).prependTo( 'body' );
        }



        croppieContainer.on( 'click, mouseup', function( ) {

            cropImages();

        });

        jQuery(function() {
            jQuery("input:file").change(function (){
                setTimeout(function(){
                    cropImages();
                }, 2000);
            });
        });

        function cropImages() {

            saveButton.attr( 'disabled', true );
            jQuery( '#notify' ).after( '<div class="alert alert-info fade show fixed-top img_crop_alert text-center" role="alert">Saving Image.</div>' );

            $uploadCrop.croppie( 'result', {
                type: 'canvas',
                size: $uploadSize,
                quality: 0.75,
                format: $imageFormat,
            }).then( function( resp ) {

                $hiddenUpload.croppie( 'bind', {

                    url: resp

                }).then( function( ) {

                    if ( jQuery( '.main_crop' ).length ) {

                        mainHidden.val( resp );
                        saveButton.attr( 'disabled', false );

                    } else {
                        $hiddenUpload.croppie( 'result', {
                            type: 'canvas',
                            size: $uploadSize,
                            quality: 1,
                            format: $imageFormat
                        }).then( function( response ) {

                            var security = jQuery( '#nano_image_security' ).val(),
                                action = 'crop_image_ajax';

                            jQuery.ajax({
                                type: 'POST',
                                dataType: 'json',
                                url: id_ajaxurl,
                                data: {
                                    'action': action,
                                    'hidden_post_file': resp,
                                    'post_file': postFile,
                                    'security': security,
                                    'aspect' : type
                                },
                                success: function( data ) {

                                    if ( 'complete' === data.results ) {

                                        mainHidden.val( data.url );
                                        saveButton.attr( 'disabled', false );
                                        jQuery( '.img_crop_alert' ).text('Save Complete').removeClass('show');

                                    }

                                },
                                error: function( data ) {
                                    console.log(data);
                                    jQuery('.project_fesave').attr( 'disabled', false );
                                    jQuery( '#notify' ).after( '<div class="alert alert-warning" role="alert">There was a problem saving the image.</div>' );
                                }
                            });

                        });

                    }

                });

            });

        }

        // Update the image data when the crop area has been adjusted


        nanoFileInput.on( 'change', function() {
            readFile( this );
        });

        jQuery( '.vanilla-rotate' ).on( 'click', function( ev ) {
            var degrees = parseInt( jQuery( this ).data( 'deg' ) );
            ev.preventDefault();
            //$uploadCrop.croppie( 'rotate', degrees );
            $uploadCrop.croppie( 'rotate', parseInt( jQuery( this ).data( 'deg' ) ) );
        });

    }

    // Team Leader Image
    jQuery( '.nano-team-leader-image' ).on( 'click', '.nano_team_leader', function() {
        nanoCropPhoto( '.nano-team-leader-image', '#nano_team_leader_hidden', 'circle', 'nano_team_leader' );
    });

    // Project Image
    projectImage.on( 'click', 'input[name="project_main"]', function() {
        //jQuery( '.croppie-container, #hidden_crop' ).croppie( 'destroy' );
        nanoCropPhoto( '.nano-project-image', '#nano_featured_image_hidden', 'square', 'project_main' );
    });

    // User Avatar
    jQuery( '.nano-avatar-image' ).on( 'click', '#nano_idc_avatar', function() {
        nanoCropPhoto( '.nano-avatar-image', '#nano_idc_avatar_hidden', 'circle', 'nano_team_leader' );
        jQuery( '.img-fluid' ).fadeOut( 'slow', function() {
            jQuery( this ).remove();
        });
    });

});
