jQuery( document ).ready( function( $ ) {
	const maxFields = 15; //maximum input boxes allowed
	const dateRangeInput = $( 'input[name="project_date_range"]' );
	const projectStart = $( '#project_start' );
	const projectEnd = $( '#project_end' );
	const destroy = $( '.milestone' ); //Fields wrapper
	const wrapper = $( '.select-wrapper' ); //Fields wrapper
	const addButton = $( '.add_field_button' ); //Add button ID
	let startDate;
	let milestoneDate = $( 'input[name="nano_goal_milestone_date[]"]' );
	const todaysDate = moment().format( 'MM/DD/YYYY' );
	let x;
	let keyID;
	let $_GET;

	if ( 0 < dateRangeInput.length ) {
		startDate = projectStart.val().length ? projectStart.val() : moment().format( 'MM/DD/YYYY' );

		// Adding Extra Fields for the Milstones under the goals area in the project creation form

		dateRangeInput.daterangepicker( {
			buttonClasses: 'btn btn-sm',
			applyClass: 'btn-primary',
			cancelClass: 'btn-primary-outline',
			autoApply: true,
			dateLimit: {
				days: 60,
			},
			startDate: ( projectStart.val().length ? projectStart.val() : moment().add( 3, 'days' ).calendar() ),
			endDate: ( projectEnd.val().length ? projectEnd.val() : moment().add( 33, 'days' ).calendar() ),
			minDate: todaysDate,
		}, function( start, end, label ) {
			$( '#project_start' ).val( start.format( 'MM/DD/YYYY' ) );
			$( '#project_end' ).val( end.format( 'MM/DD/YYYY' ) );
		} );

		milestoneDate.on( 'click focus', function( ) {

		} );

		milestoneDate.daterangepicker( {
			singleDatePicker: true,
			autoUpdateInput: true,
			minDate: todaysDate,
		} );

		milestoneDate.change( function() {
			// If this is the first milestone then update the second's min date as its minimum date
			$( milestoneDate[ 0 ] ).on( 'apply.daterangepicker', function( ev, picker ) {
				const minDate2 = picker.minDate;
				$( milestoneDate[ 1 ] ).data( 'daterangepicker' ).minDate = minDate2;
			} );
		} );

		$_GET = {};

		document.location.search.replace( /\??(?:([^=]+)=([^&]*)&?)/g, function() {
			function decode( s ) {
				return decodeURIComponent( s.split( '+' ).join( ' ' ) );
			}

			$_GET[ decode( arguments[ 1 ] ) ] = decode( arguments[ 2 ] );
		} );

		if ( $_GET[ 'create_project' ] ) {
			milestoneDate.val( '' );
		}

		x = 2; //initial text box count
		jQuery( addButton ).click( function( e ) { //on add input button click
			// Assign a minimum date to each newly added milestone based on the previous date selection.
			const keyID = ( $( '.select-wrapper > div' ).length ) - 1;
			e.preventDefault();
			x = $( '.select-wrapper > div' ).length;

			milestoneDate = $( 'input[name="nano_goal_milestone_date[]"]' );
			startDate = '' !== $( milestoneDate[ keyID ] ).val() ? $( milestoneDate[ keyID ] ).val() : startDate;

			if ( x < maxFields ) { //max input box allowed
				x++; //text box increment
				jQuery( wrapper ).append( '<div class="form-row fadein milestone"><div class="form-group col-sm-9 col-7"> <label class="sr-only"  for="">Milestone Date</label> <input type="text" id="" name="nano_goal_milestones[]" class="form-control form-control-sm required milestone_added" value="" maxlength=140 placeholder="Milestone" required aria-label="Milestone Date"> </div><div class="form-group col-sm-3 col-5"> <label class="sr-only"  for="">Milestone Date</label><div class="input-group"><input type="text" id="" name="nano_goal_milestone_date[]" class="form-control form-control-sm newDate_' + keyID + '" value="" placeholder="Date select" aria-label="Milestone Date"></div></div>' ); //add input box
				jQuery( '.newDate_' + keyID ).daterangepicker( {
					singleDatePicker: true,
					autoUpdateInput: true,
					minDate: todaysDate,
				} );
				jQuery( '.newDate_' + keyID ).val( '' );
			}
			if ( x === maxFields ) {
				jQuery( this ).hide();
			}
		} );

		//Destroy for milestone
		jQuery( wrapper ).on( 'click', '.remove_field', function( e ) { //user click on remove text
			e.preventDefault();
			jQuery( this ).closest( '.form-row' ).remove();
			x--;
		} );

		// Delete milestone if deleting content in text field
		wrapper.on( 'click', '.milestone_added', function() {
			let wto;

			jQuery( this ).on( 'keyup change', function() {
				const title = jQuery( this ).val();
				const item = jQuery( this );

				// If typing starts within 1 second then stop deletion
				if ( 0 < title.length ) {
					clearTimeout( wto );
				}

				// Remove if field is emptied
				if ( 0 === title.length ) {
					wto = setTimeout( function() {
						item.closest( '.form-row' ).fadeOut( 'slow', function() {
							item.remove();
						} );
					}, 1000 );
				}
			} );
		} );
	}
} );
