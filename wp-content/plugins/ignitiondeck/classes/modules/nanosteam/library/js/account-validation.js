jQuery( document ).ready( function() {
	/* -------------------------------
         -----   validation   -----
         ---------------------------------*/

	let passwordCheck = jQuery( 'input[name="cpw"]' ),
		form = jQuery( '#edit-profile' ),
		fv = '';

	if ( form.length ) {
		// When the user is going to submit the form for review validate the entire form
		form.formValidation( {
			fields: {
				nicename: {
					validators: {
						notEmpty: {
							message: 'Display Name cannot be empty ',
						},
					},
				},
				'first-name': {
					validators: {
						notEmpty: {
							message: 'First Name cannot be empty ',
						},
					},
				},
				'last-name': {
					validators: {
						notEmpty: {
							message: 'Last Name cannot be empty ',
						},
					},
				},
				description: {
					validators: {
						notEmpty: {
							message: 'Biography cannot be empty ',
						},
					},
				},
				email: {
					validators: {
						notEmpty: {
							message: 'Email cannot be empty ',
						},
						emailAddress: {
							message: 'The is not a valid email address',
						},
					},
				},
				pw: {
					validators: {
						notEmpty: {
							enabled: false,
							message: 'A password is required ',
						},
						identical: {
							compare: function() {
								return form.find( 'input[name="cpw"]' ).val();
							},
							message: 'The passwords do not match',
						},
					},
				},
				cpw: {
					validators: {
						identical: {
							compare: function() {
								return form.find( 'input[name="pw"]' ).val();
							},
							message: 'The passwords do not match',
						},
					},
				},

			},
			plugins: {
				bootstrap: new FormValidation.plugins.Bootstrap( {

					rowSelector: function( field, ele ) {
						// field is the field name
						// ele is the field element
						switch ( field ) {
							default:
								return '.formval';
						}
					},

				} ),
				trigger: new FormValidation.plugins.Trigger( {
					event: 'keyup change focus blur',
				} ),
				submitButton: new FormValidation.plugins.SubmitButton(),
				defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
				sequence: new FormValidation.plugins.Sequence(),
			},
		} );

		fv = form.data( 'formValidation' );

		// Crosschecking password fields as being typed
		form.on( 'keyup', passwordCheck, function() {
			if ( 0 < passwordCheck.val().length ) {
				fv.enableValidator( 'pw', 'notEmpty' );
				fv.validateField( 'pw', 'notEmpty' );
			} else {
				fv.disableValidator( 'pw', 'notEmpty' );
				fv.revalidateField( 'cpw', 'identical' );
			}
		} );

		fv.on( 'core.form.valid', function( e ) {
			console.log( 'valid' );

			jQuery( '#about-you' ).append( '<input type="hidden" id="edit-profile-submit-3" name="edit-profile-submit" class="nano_unique_fesubmit" value="edit-profile-submit">' );

			form.submit();
		} );

		fv.on( 'core.form.invalid', function( e ) {
			console.log( 'invalid' );

			// Reset the errors inside the modal first
			jQuery( '#valerrors' ).html( '' );

			form

			// Find all the error messages
				.find( '.is-invalid' ).closest( '.formval' ).children( '.fv-plugins-message-container' )

			// And update the error inside the modal body
				.clone().appendTo( '#valerrors' );

			// Show the message modal
			jQuery( '#FormErrors' ).modal( 'show' );
		} );

		// When the user is only saving a draft destroy the form validation.
		jQuery( '.mt-2.ml-2.px-3.submit-button.btn.btn-sm.btn-primary.float-right' ).on( 'click', function() {
			console.log('fired');
			form.formValidation( 'destroy' );
			jQuery( '#about-you' ).append( '<input type="hidden" name="' + jQuery( this ).attr( 'name' ) + '" class="edit-endorsement">' );
			form.submit();
		} );
	}
} );
