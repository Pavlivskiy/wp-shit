jQuery( document ).ready( function( $ ) {
	const tokenStart = $( '#token_start' );
	const tokenEnd = $( '#token_end' );
	const tokenDateRange = $( '#token_date_range' );

	// Date Selection
	if ( 0 < tokenDateRange.length ) {
		tokenDateRange.daterangepicker( {
			buttonClasses: 'btn btn-sm',
			applyClass: 'btn-primary',
			cancelClass: 'btn-primary-outline',
			autoApply: true,
			startDate: ( tokenStart.val().length ? tokenStart.val() : moment().subtract( 30, 'days' ).calendar() ),
			endDate: ( tokenEnd.val().length ? tokenEnd.val() : moment().format( 'MM/DD/YYYY' ) ),
			linkedCalendars: false,
		}, function( start, end, label ) {
			console.log( 'here' );
			tokenStart.val( start.format( 'MM/DD/YYYY' ) );
			tokenEnd.val( end.format( 'MM/DD/YYYY' ) );
			jQuery( '.button.button-primary' ).click();
		} );

		$( '.daterangepicker.dropdown-menu' ).addClass( 'hidden' );

		// Need to add in enter key control for when its used on the input fields in the range picker
		tokenDateRange.on( 'showCalendar.daterangepicker', function( ev, picker ) {
			jQuery( '.daterangepicker' ).on( 'keydown', 'input[name=daterangepicker_start], input[name=daterangepicker_end]', function( events ) {
				let inputField = jQuery( '#token_start' );
				const code = ( events.keyCode ? events.keyCode : events.which );

				if ( jQuery( this ).attr( 'name' ) === 'daterangepicker_end' ) {
					inputField = jQuery( '#token_end' );
				}
				if ( ( code === 9 ) || ( code === 13 ) ) {
					inputField.val( jQuery( this ).val() );
					jQuery( '.button.button-primary' ).click();
				}
			} );
		} );

		tokenDateRange.on( 'apply.daterangepicker', function( ev, picker ) {
			//do something, like clearing an input
			console.log( '' );
		} );

		jQuery( '.calendar.right' ).on( 'mouseout', 'thead', function( e ) {
			if ( jQuery( '.calendar.left .input-mini.form-control' ).hasClass( 'active' ) ) {
				jQuery( '.calendar.right .input-mini.form-control' ).focus().select().addClass( 'active' );
			}
		} );

		jQuery( '.calendar.left' ).on( 'mouseout', 'thead', function( e ) {
			if ( jQuery( '.calendar.right .input-mini.form-control' ).hasClass( 'active' ) ) {
				jQuery( '.calendar.left .input-mini.form-control' ).focus().select().addClass( 'active' );
			}
		} );

		// Perform AJAX (NO LONGER USED, Kept for reference if ever moved to front end)
		jQuery( '#token_refresh' ).on( 'click', function( e ) {
			const action = 'nano_ajax_token_tracking',
				security = jQuery( '#nano_token_secure_security' ).val(),
				dateFrom = jQuery( '#token_start' ).val(),
				dateTo = jQuery( '#token_end' ).val(),
				projectType = jQuery( 'input[name=project_type]:checked' ).val();

			e.preventDefault();

			$( '#tokens tbody' ).find( 'tr' ).remove();

			/*
                jQuery('#tokens').dataTable({
                    "processing": true,
                    "serverSide": true,
                    "ajax": id_ajaxurl + '?action=nano_ajax_token_tracking&security='+security+'&date_from='+date_from+'&date_to='+date_to+'&project_type='+project_type,
                });
                */

			jQuery.ajax( {
				type: 'POST',
				dataType: 'json',
				url: id_ajaxurl,
				data: {
					action: action,
					date_from: dateFrom,
					date_to: dateTo,
					project_type: projectType,
					security: security,
				},
				success: function( data ) {
					const response = data.data;

					$( function() {
						$.each( response, function( i, item ) {
							const $tr = $( '<tr>' ).append(
								$( '<td>' ).text( item.todays_date ),
								$( '<td>' ).html( item.user_link ),
								$( '<td>' ).text( item.donation_date ),
								$( '<td>' ).html( item.transaction_id ),
								$( '<td>' ).html( item.project_uid ),
								$( '<td>' ).html( item.project_status ),
								$( '<td>' ).html( item.stripe_fee ),
								$( '<td>' ).text( item.donation_amount ),
								$( '<td>' ).text( item.tokens_spent ),
								$( '<td>' ).html( item.tokens_unused ),
								$( '<td>' ).text( item.tokens_issued ),
								$( '<td>' ).text( item.tokens_expiry ),
								$( '<td>' ).html( item.second_projects )
							);

							$tr.appendTo( '#tokens tbody' );
						} );
					} );

					$( '#total_donations' ).text( data.total_donations );
					$( '#total_stripe' ).text( data.total_stripe );
					$( '#total_spent' ).text( data.total_spent );
					$( '#total_unused' ).text( data.total_unused );
				},
				error: function( data ) {
					console.log( data );
				},
			} );
		} );
	}

	jQuery( '#token_export' ).on( 'click', function( e ) {
		const titles = [];
		const data = [];
		const footer = [];
		let CSVString, downloadLink, blob, url;

		e.preventDefault();

		/*
         * Get the table headers, this will be CSV headers
         * The count of headers will be CSV string separator
         */
		jQuery( '.wp-list-table thead th' ).each( function() {
			titles.push( $( this ).text() );
		} );

		/*
         * Get the actual data, this will contain all the data, in 1 array
         */
		jQuery( '.wp-list-table tbody td' ).each( function() {
			const tdVar = jQuery( this );
			if ( $( this ).find( '.toggle-row' ).length ) {
				$( this ).find( '.toggle-row' ).remove();
			}
			data.push( $( this ).text() );
		} );

		/*
         * Get the footers
         */
		jQuery( '.wp-list-table tfoot th' ).each( function() {
			data.push( $( this ).text() );
		} );

		/*
         * Convert our data to CSV string
         */

		CSVString = prepCSVRow( titles, titles.length, footer, '' );
		CSVString = prepCSVRow( data, titles.length, CSVString );
		downloadLink = document.createElement( 'a' );
		blob = new Blob( [ '\ufeff', CSVString ] );
		url = URL.createObjectURL( blob );

		/*
         * Make CSV downloadable
         */
		downloadLink.href = url;
		downloadLink.download = 'nanosteam_data.csv';

		/*
         * Actually download CSV
         */
		document.body.appendChild( downloadLink );
		downloadLink.click();
		document.body.removeChild( downloadLink );
	} );

	/*
     * Convert data array to CSV string
     * @param arr {Array} - the actual data
     * @param columnCount {Number} - the amount to split the data into columns
     * @param initial {String} - initial string to append to CSV string
     * return {String} - ready CSV string
     */
	function prepCSVRow( arr, columnCount, initial ) {
		let row = ''; // this will hold data
		const delimeter = ','; // data slice separator, in excel it's `;`, in usual CSv it's `,`
		const newLine = '\r\n'; // newline separator for CSV row
		let plainArr;

		/*
         * Convert [1,2,3,4] into [[1,2], [3,4]] while count is 2
         * @param _arr {Array} - the actual array to split
         * @param _count {Number} - the amount to split
         * return {Array} - splitted array
         */
		function splitArray( _arr, _count ) {
			let splitted = [];
			const result = [];
			_arr.forEach( function( item, idx ) {
				if ( 0 === ( idx + 1 ) % _count ) {
					splitted.push( item );
					result.push( splitted );
					splitted = [];
				} else {
					splitted.push( item );
				}
			} );
			return result;
		}

		plainArr = splitArray( arr, columnCount );

		// don't know how to explain this
		// you just have to like follow the code
		// and you understand, it's pretty simple
		// it converts `['a', 'b', 'c']` to `a,b,c` string
		plainArr.forEach( function( arrItem ) {
			arrItem.forEach( function( item, idx ) {
				row += item + ( ( idx + 1 ) === arrItem.length ? '' : delimeter );
			} );
			row += newLine;
		} );
		return initial + row;
	}
} );
