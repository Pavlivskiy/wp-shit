jQuery( document ).ready( function() {
	'use strict';

	//
	// Check to see if total funding goal is applicable for the 50/50 grant.
	//
	//

	const projectTotal = jQuery( '#project_goal' );
	const nanoFiftyApply = jQuery( '#yes, #no' );
	const nanoFiftyNo = jQuery( '#no' );
	const nanoFiftyLabel = jQuery( '.grant label' );
	const nanoFiftyNotify = jQuery( '#notify' );
	const nanoGrantAmount = parseInt( php_variables.nano_grant_amt ) + 1;
	const nanoGrantTxt = php_variables.nano_grant_amt;
	const instantCheckout = jQuery( '#instant_checkout' );
	const paymentErrors = jQuery( '.payment-errors' );
	let observer, editButtonVisibility;

	// Check initial load values
	if ( projectTotal.val() >= nanoGrantAmount ) {
		nanoFiftyApply.attr( 'disabled', true );
		nanoFiftyLabel.addClass( 'disabled' );
		if ( false === nanoFiftyNo.attr( 'checked' ) ) {
			nanoFiftyNotify.after( '<div class="alert alert-warning fadein" role="alert">Projects can be for any amount but only those with goal amounts up to $' + nanoGrantTxt + ' are eligible for Matching Grants.</div>' );
		}
	}

	//Payment Flow adding alert borders to error field when triggered

	if ( 1 <= paymentErrors.length ) {
		editButtonVisibility = function() {
			//paymentErrors.removeClass( 'd-none' ).addClass( 'd-block alert alert-danger fadein' );
			jQuery( '.nano_status_checkout' ).html( paymentErrors.html() ).show().addClass( 'show' );
			jQuery( '.nano_status a' ).on( 'click', '', function( e ) {
				e.preventDefault();
				jQuery( '#login-form' ).modal( 'show' );
			} );
			if ( ! jQuery( '.nano_status_checkout' ).hasClass( 'show' ) ) {
				jQuery( '#login-form' ).modal( 'show' );
			}
		};

		paymentErrors.on( 'change', function() {

		} );

		observer = new MutationObserver( function( e ) {
			editButtonVisibility();
			if ( 0 === paymentErrors.html().length ) {
				paymentErrors.removeClass( 'd-block' ).addClass( 'd-none' );
			}
		} );

		observer.observe( paymentErrors[ 0 ], {
			characterData: true,
			childList: true,
		} );
	}

	// Changing the button text by overriding IDK's function doing the same thing.
	if ( 1 <= jQuery( '#stripe-input' ).length ) {
		jQuery( '#payment-form #id-main-submit' ).text( 'Give' );
	}

	// Save current value of element
	projectTotal.data( 'oldVal', projectTotal.val() );

	// Look for changes in the value
	// On change of input determine if project can be applied to
	projectTotal.bind( 'propertychange change click keyup input paste', function( event ) {
		const noGrant = jQuery( '.nogrant' );

		// If value has changed...
		if ( projectTotal.data( 'oldVal' ) !== projectTotal.val() ) {
			// Updated stored value
			projectTotal.data( 'oldVal', projectTotal.val() );

			// Do action
			if ( projectTotal.val() >= nanoGrantAmount ) {
				if ( ! nanoFiftyNo.is( ':checked' ) ) {
					nanoFiftyApply.attr( 'disabled', true );
					nanoFiftyLabel.addClass( 'disabled' );
					noGrant.remove();
					if ( ! noGrant.length ) {
						nanoFiftyNotify.after( '<div class="alert alert-warning fadein nogrant" role="alert">Only projects with Goal Amounts up to $' + nanoGrantTxt + ' are eligble for Matching grants.</div>' );
					}
					nanoFiftyNo.attr( 'checked', true );
				}
			} else {
				nanoFiftyApply.attr( 'disabled', false );
				nanoFiftyLabel.removeClass( 'disabled' );
				noGrant.remove();
			}
		}
	} );

	nanoFiftyApply.on( 'change', function() {
		const noGrant = jQuery( '.nogrant' );

		// Do action
		if ( projectTotal.val() >= nanoGrantAmount ) {
			if ( ! nanoFiftyNo.is( ':checked' ) ) {
				nanoFiftyApply.attr( 'disabled', true );
				nanoFiftyLabel.addClass( 'disabled' );
				noGrant.remove();
				if ( ! noGrant.length ) {
					nanoFiftyNotify.after( '<div class="alert alert-warning fadein nogrant" role="alert">Only projects with Goal Amounts up to $' + nanoGrantTxt + ' are eligble for Matching grants.</div>' );
				}
				nanoFiftyNo.attr( 'checked', true );
			}
		} else {
			nanoFiftyApply.attr( 'disabled', false );
			nanoFiftyLabel.removeClass( 'disabled' );
			noGrant.remove();
		}
	} );
} );
