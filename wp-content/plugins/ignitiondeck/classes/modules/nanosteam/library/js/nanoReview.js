jQuery( document ).ready( function() {

    var emailButton = jQuery( '#project_review_submit' ),
        projectContainer = jQuery( '#nano_project_review' ),
        projectRadio = jQuery( 'input[name="project_review_status_radio"]' ),
        projectMessage = jQuery( '.project_review_message' ),
        projectSubject = jQuery( '.project_review_subject' ),
        projectSubmit = jQuery( '.project_review_submit' ),
        projectReviewStatusEmail = jQuery( '#project_review_status_email' ),
        projectRadioChecked = jQuery( 'input[name="project_review_status_radio"]:checked' ),
        publishButton = jQuery(' .editor-post-publish-panel__toggle ');

    if ( emailButton.length ) {

        projectRadio.on( 'change', function() {

            var radioCheck = jQuery(this).val();

            if ( 'resubmit' === radioCheck ) {
                projectMessage.removeClass( 'd-none' ).addClass( 'show' );
                projectSubject.removeClass( 'd-none' ).addClass( 'show' );
                projectSubmit.removeClass( 'd-none' ).addClass( 'show' );
                publishButton.prop( 'disabled', true );
            } else {
                projectMessage.removeClass( 'show' ).addClass( 'd-none' );
                projectSubject.removeClass( 'show' ).addClass( 'd-none' );
                projectSubmit.removeClass( 'show' ).addClass( 'd-none' );
                publishButton.prop( 'disabled', false );
            }
            
        });

        if ( 'resubmit' === projectRadioChecked.val() ) {
            publishButton.prop( 'disabled', true );
        } else {
            publishButton.prop( 'disabled', false );
        }

        emailButton.on( 'click', function( e ) {
            var projectMessageInput = jQuery( '#project_review_message' ),
                projectSubjectInput = jQuery( '#project_review_subject' );

            if ( 1 < projectMessageInput.val().length && 1 < projectSubjectInput.val().length ) {
                nanoProjectReview();
            } else {
                alert( 'Please include both a subject and message.' );
            }
            e.preventDefault();
        });

        projectContainer
            .on( 'keyup change', '#project_review_subject,#project_review_message,input[name="project_review_status_radio"]', function( e ) {
                emailButton.prop( 'disabled', false ).text( 'Send Email' );
                projectReviewStatusEmail.val( 'pending' );
            });

        // Project submission review function
        function nanoProjectReview() {

            var split = location.search.replace( '?', '' ).split( '&' ).map( function( val ) {
                    return val.split( '=' );
                }),
                ajaxurl = id_ajaxurl,
                button = jQuery( '#project_review_submit' );

            var dataToSend = {
                action: 'ajax_nano_project_review',
                security: jQuery( '#nano_project_review_metabox' ).val(),
                project_review_status_radio: jQuery( 'input[name="project_review_status_radio"]:checked' ).val(),
                project_review_subject: jQuery( '#project_review_subject' ).val(),
                project_review_message: jQuery( '#project_review_message' ).val(),
                project_review_status_email: jQuery( '#project_review_status_email' ).val(),
                id: split[ 0 ][ 1 ]
            };

            jQuery.ajax({
                type: 'POST',
                dataType: 'json',
                url: ajaxurl,
                data: dataToSend,
                success: function( data ) {

                    button.prop( 'disabled', true ).text( data.message );
                    jQuery( '#nano_project_review_email .inside' ).prepend( '<input type="hidden" id="nano_project_review_metabox" name="nano_project_review_metabox" value="36596c52e0">' +
                        '<input type="hidden" name="_wp_http_referer" value="/wp-admin/post.php?post=715&amp;action=edit">' +
                        '<p><strong>New Message</strong></p>' +
                        '<div class="formval form-row project_review_subject">' +
                        '<label class="sr-only" for="project_review_subject_email"></label>' +
                        '<input type="text" id="project_review_subject_email" name="project_review_subject_email" class="form-control form-control-sm  nano-full-width" value="' + dataToSend.project_review_subject + '" rows="6" style="margin-top:20px" placeholder="Subject" readonly="" aria-label="">' +
                        '<div class="invalid-feedback"></div> </div>' +
                        '<div class="formval form-row project_review_message">' +
                        '<label class="sr-only" for="project_review_message_email"></label>' +
                        '<textarea id="project_review_message_email" name="project_review_message_email" class="form-control form-control-sm  nano-full-width" rows="6" style="margin-top:20px" placeholder="Message" readonly="">' + dataToSend.project_review_message + '</textarea>' +
                        '<div class="invalid-feedback"></div> </div>' );
                },
                error: function( data ) {
                    button.text( data );
                }
            });

        }

    }

});
