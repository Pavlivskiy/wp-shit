jQuery( document ).ready( function() {
	const maxFieldsTm = 10; //maximum input boxes allowed
	const wrapperTm = jQuery( '.teammember-wrapper' ); //Fields wrapper
	const addButtonTm = jQuery( '.add_field_button_members' ); //Add button ID
	const inputFieldNan = jQuery( '.team_email_va' );
	const wrapperEndorser = jQuery( '.endorser-wrapper' ); //Fields wrapper
	const addButtonEndorser = jQuery( '.add_field_button_endorsement' ); //Add button ID
	const inputFieldEndorser = jQuery( '.endorser_email_va' );
	let x = 1; //initlal text box count

	function validateEmail( email ) {
		const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test( email );
	}

	jQuery( '#project_continue' ).click( function( e ) {
		const $activeTab = jQuery( '.nav.nav-pills' ).find( '.active' );

		e.preventDefault();

		// If a popover is open, hide it before page change
		jQuery( '[data-toggle="popover"]' ).popover( 'hide' );
		$activeTab.next( 'a.nav-link' ).tab( 'show' );

		jQuery( '.nav.nav-pills a' ).on( 'shown.bs.tab', function( e ) {
			const $previous = jQuery( e.relatedTarget ).attr( 'href' );
			const $current = jQuery( e.target ).attr( 'href' );

			jQuery( $current ).addClass( 'fade show' );
			jQuery( $previous ).removeClass( 'active show' ).addClass( 'fade' );
			jQuery( 'html,body' ).animate( { scrollTop: 0 }, 500, 'swing' );
		} );
	} );

	// For milestones see wp-content/plugins/ignitiondeck/classes/modules/nanosteam/js/nanoDateSelect.js

	//Destroy for teammember
	jQuery( wrapperTm ).on( 'click', '.remove_field', function( e ) { //user click on remove text
		e.preventDefault();
		jQuery( this ).closest( '.formval' ).remove();
		x--;
	} );

	// Team Member Email Validation & Registration

	if ( inputFieldNan.length ) {
		wrapperTm.on( 'click mouseover', '.team_email_va', function() {
			let email, invitedEmail, popOverIDBase;

			jQuery( this ).on( 'keyup change', function() {
				email = jQuery( this ).val();
				invitedEmail = email;

				if ( jQuery( this ).is( '[data-email-org]' ) ) {
					invitedEmail = '';
				}

				if ( jQuery( this ).is( '[data-email-org]' ) && 'undefined' === jQuery( this ).attr( 'data-email-org' ) ) {
					invitedEmail = email;
				}

				if ( validateEmail( email ) && email === invitedEmail ) {
					jQuery( this ).closest( 'div' ).find( '.invite_user, .remove_field' ).removeClass( 'd-none fadeout fade' ).addClass( 'fadein' );
					addButtonTm.removeClass( 'fadein' ).addClass( 'show' );
					jQuery( this ).removeClass( 'is-invalid' );
				} else {
					if ( validateEmail( email ) && jQuery( this ).is( '[data-email-org]' ) ) {
						jQuery( this ).attr( 'data-content', 'Pending' );
						jQuery( this ).popover( 'show' );
						popOverIDBase = jQuery( this ).attr( 'aria-describedby' );
						jQuery( '#' + popOverIDBase + ' .popover-body' ).html( 'You can delete this field by clearing it but if you would like to add another please click the "Add Another Team Member" button below' );
					}
					if ( 0 === email.length ) {
						jQuery( this ).popover( 'dispose' );
					}
					jQuery( this ).addClass( 'is-invalid' );
					jQuery( this ).closest( '.form-group' ).find( '.invite_user, .remove_field' ).removeClass( 'fadein' ).addClass( 'fadeout d-none' );
					addButtonTm.removeClass( 'fadein' ).addClass( 'fadeout' ).addClass( 'hide' );
				}

				// Remove if field is emptied
				if ( 0 === email.length ) {
					if ( ! jQuery( this ).closest( '.form-group' ).hasClass( 'org-field' ) ) {
						jQuery( this ).closest( '.form-group' ).fadeOut( 'slow', function() {
							jQuery( this ).remove();
						} );
					}
				}
				return false;
			} );

			// Perform AJAX invite & register team members
			jQuery( '.invite_user' ).unbind( 'click' );
			jQuery( '.invite_user' ).on( 'click', function( e ) {
				let popOverID,
					action = 'ajax_register_team_members',
					security = jQuery( '#nano_teammember_security' ).val(),
					email = jQuery( this ).closest( '.input-group' ).find( 'input' ).val(),
					inputContainer = jQuery( this ).closest( '.input-group' ).find( 'input' ),
					id = jQuery( 'input[name=\'project_post_id\']' ),
					ctrl = jQuery( this );

				jQuery( this ).attr( 'data-content', team_member_object.loadingmessage );
				jQuery( this ).popover( 'show' );

				if ( id.length ) {
					id = id.val();
				} else {
					id = '';
				}

				jQuery.ajax( {
					type: 'POST',
					dataType: 'json',
					url: team_member_object.ajaxurl,
					data: {
						action: action,
						email: email,
						id: id,
						security: security,
					},
					success: function( data ) {
						ctrl.attr( 'data-content', data.message );
						popOverID = ctrl.attr( 'aria-describedby' );
						jQuery( '#' + popOverID + ' .popover-body' ).html( data.message );
						if ( 'Successfully Sent' === data.message ) {
							inputContainer.addClass( 'disabled' ).prop( 'readonly', true );
							ctrl.addClass( 'disabled' ).html( '<i class="fa fa-check"></i>' );
						}
					},
					error: function( data ) {
						console.log( data );
					},
				} );
				e.preventDefault();
			} );
		} );

		jQuery( addButtonTm ).click( function( e ) { //on add input button click
			e.preventDefault();
			x = jQuery( '.team_email_va' ).length;
			if ( x < maxFieldsTm ) { //max input box allowed
				x++; //text box increment
				wrapperTm.find( '.form-row' ).append( '<div class="form-group col-sm-6 fadein"><label class="sr-only" for="nano_team_members">Team Members</label><div class="input-group"><input type="email" id="nano_team_members" name="nano_team_members[]" class="form-control form-control-sm team_email_va" value="" maxlength="254" placeholder="Enter the email of a team member" aria-label="Team Members"><div class="input-group-btn"><a href="#" class="invite_user btn btn-sm btn-primary px-3 d-none">Invite</a></div></div></div><div class="w-100"></div>' ); //add input box
			}
		} );
	}

	// Adding Extra Fields for the Endorsement Email Addresses under the team area in the project creation form

	//Destroy for endorser
	jQuery( wrapperEndorser ).on( 'click', '.remove_field', function( e ) { //user click on remove text
		e.preventDefault();
		jQuery( this ).closest( '.formval' ).remove();
		x--;
	} );

	// Endorser Email Validation & Registration

	if ( inputFieldEndorser.length ) {
		wrapperEndorser.on( 'click mouseover', '.endorser_email_va', function() {
			jQuery( this ).on( 'keyup change', function() {
				let email = jQuery( this ).val(),
					popOverIDBase = jQuery( this ).attr( 'aria-describedby' ),
					invitedEmail;

				if ( jQuery( this ).is( '[data-email-org]' ) ) {
					invitedEmail = '';
				} else {
					invitedEmail = email;
				}

				if ( validateEmail( email ) && email === invitedEmail ) {
					jQuery( this ).closest( 'div' ).find( '.invite_endorser, .remove_field' ).removeClass( 'd-none fadeout fade' ).addClass( 'fadein' );
					addButtonEndorser.removeClass( 'fadein' ).addClass( 'show' );
					jQuery( this ).removeClass( 'is-invalid' );
					if ( email === jQuery( '#creator_id' ).val() ) {
						jQuery( this ).attr( 'data-content', 'Sorry you cannot invite yourself as an endorser' );
						jQuery( this ).popover( 'show' );
						jQuery( this ).addClass( 'is-invalid' );
						jQuery( this ).closest( 'div' ).find( '.invite_endorser, .remove_field' ).addClass( 'd-none fadeout fade' ).removeClass( 'fadein' );
					} else {
						jQuery( this ).popover( 'dispose' );
						jQuery( this ).closest( 'div' ).find( '.invite_endorser, .remove_field' ).removeClass( 'd-none fadeout fade' ).addClass( 'fadein' );
					}
				} else {
					if ( validateEmail( email ) && jQuery( this ).is( '[data-email-org]' ) ) {
						jQuery( this ).attr( 'data-content', 'Pending' );
						jQuery( this ).popover( 'show' );
						jQuery( '#' + popOverIDBase + ' .popover-body' ).html( 'You can delete this field by clearing it but if you would like to add another please click the "Add Another Endorser" button below' );
					}
					if ( 0 === email.length ) {
						jQuery( this ).popover( 'dispose' );
					}
					jQuery( this ).addClass( 'is-invalid' );
					jQuery( this ).closest( '.form-group' ).find( '.invite_endorser, .remove_field' ).removeClass( 'fadein' ).addClass( 'fadeout d-none' );
					addButtonEndorser.removeClass( 'fadein' ).addClass( 'fadeout' ).addClass( 'hide' );
				}

				// Remove if field is emptied
				if ( 0 === email.length ) {
					if ( ! jQuery( this ).closest( '.form-group' ).hasClass( 'org-field' ) ) {
						jQuery( this ).closest( '.form-group' ).fadeOut( 'slow', function() {
							jQuery( this ).closest( '.form-group' ).remove();
						} );
					}
				}
				return false;
			} );

			// Perform AJAX invite & register team members
			jQuery( '.invite_endorser' ).unbind( 'click' );
			jQuery( '.invite_endorser' ).on( 'click', function( e ) {
				let popOverID,
					action = 'register_endorsers',
					security = jQuery( '#nano_endorsement_security' ).val(),
					email = jQuery( this ).closest( '.input-group' ).find( 'input' ).val(),
					inputContainer = jQuery( this ).closest( '.input-group' ).find( 'input' ),
					id = jQuery( 'input[name=\'project_post_id\']' );

				jQuery( this ).attr( 'data-content', team_member_object.loadingmessage );
				jQuery( this ).popover( 'show' );

				if ( id.length ) {
					id = id.val();
				} else {
					id = '';
				}
				const ctrl = jQuery( this );
				jQuery.ajax( {
					type: 'POST',
					dataType: 'json',
					url: team_member_object.ajaxurl,
					data: {
						action: action,
						email: email,
						id: id,
						security: security,
					},
					success: function( data ) {
						ctrl.attr( 'data-content', data.message );
						popOverID = ctrl.attr( 'aria-describedby' );
						jQuery( '#' + popOverID + ' .popover-body' ).html( data.message );
						if ( 'Successfully Sent' === data.message ) {
							inputContainer.addClass( 'disabled' ).prop( 'readonly', true );
							ctrl.addClass( 'disabled' ).html( '<i class="fa fa-check"></i>' );
						}
					},
					error: function( data ) {
						console.log( data );
					},
				} );
				e.preventDefault();
			} );
		} );

		jQuery( addButtonEndorser ).click( function( e ) { //on add input button click
			e.preventDefault();
			x = jQuery( '.endorser_email_va' ).length;
			if ( x < maxFieldsTm ) { //max input box allowed
				x++; //text box increment
				wrapperEndorser.find( '.form-row' ).append( '<div class="form-group col-sm-6 fadein"><label class="sr-only" for="nano_endorsement_members">Endorsers</label><div class="input-group"><input type="email" id="nano_endorsement_members" name="nano_endorsement_members[]" class="form-control form-control-sm endorser_email_va" value="" maxlength="254" placeholder="Enter the email of an endorser" aria-label="Endorsers"><div class="input-group-btn"><a href="#" class="invite_endorser btn btn-sm btn-primary px-3 d-none">Invite</a></div></div></div><div class="w-100"></div>' ); //add input box
			}
		} );
	}

	// If the matching grant is applied for ensure the endorser email field is required.
	jQuery( 'input[name=nano_fifty_grant_apply]' ).on( 'change', function() {
		const selected = jQuery( 'input[name=nano_fifty_grant_apply]:checked' ).val();
		if ( 'yes' === selected ) {
			jQuery( '#nano-matching' ).removeClass( 'd-none' ).addClass( 'show fadein' );
			fv.enableValidator( 'nano_endorsement_members[]', 'notEmpty' );
			// Revalidate the field now
			fv.revalidateField( 'nano_endorsement_members[]' );
		}
		if ( 'no' === selected ) {
			fv.disableValidator( 'nano_endorsement_members[]', 'notEmpty' );
		}
	} );

	// We need to enable or disable the labnotes fields based on whether one of the other fields has content.
	jQuery( document ).on( 'tinymce-editor-init', function( event, editor ) {
		const ed = tinyMCE.get( 'new_update_content' );

		if ( null !== ed ) {
			ed.on( 'focus keyup', function( editor ) {
				const edSelected = tinyMCE.get( 'new_update_content' ).getContent( {
					format: 'text',
				} );

				if ( edSelected.length > 1 ) {
					fv.enableValidator( 'new_update_subject', 'notEmpty' );
					fv.enableValidator( 'new_update_content', 'callback' );

					// Revalidate the field now
					fv.revalidateField( 'new_update_subject' );
					fv.revalidateField( 'new_update_content' );
				}
				if ( jQuery( 'input[name=new_update_subject]' ).val().length === 0 && edSelected.length < 2 ) {
					fv.disableValidator( 'new_update_subject', 'notEmpty' );
					fv.disableValidator( 'new_update_content', 'callback' );
				}
			} );

			jQuery( 'input[name=new_update_subject]' ).on( 'focus keyup', function() {
				const selected = jQuery( this ).val();
				const edSelected = tinyMCE.get( 'new_update_content' ).getContent( {
					format: 'text',
				} );

				if ( selected.length > 0 ) {
					fv.enableValidator( 'new_update_subject', 'notEmpty' );
					fv.enableValidator( 'new_update_content', 'callback' );

					// Revalidate the field now
					fv.revalidateField( 'new_update_subject' );
					fv.revalidateField( 'new_update_content' );
				}
				if ( jQuery( 'input[name=new_update_subject]' ).val().length === 0 && edSelected.length < 2 ) {
					fv.disableValidator( 'new_update_subject', 'notEmpty' );
					fv.disableValidator( 'new_update_content', 'callback' );
				}
			} );
		}
	} );

	//Ajax Autocomplete for cities to ensure spelling is correct for the database.
	jQuery( '#nano_location_city_aj' ).autoComplete( {
		source: function( name, response ) {
			const action = 'geo_locate_by_city_state_ajax';
			jQuery.ajax( {
				type: 'POST',
				dataType: 'json',
				url: team_member_object.direct_url,
				timeout: 3000,
				data: {
					action: action,
					name: name,
				},
				beforeSend: function( data ) {
					jQuery( '#nano_location_city_aj' ).addClass( 'spinner' );
				},
				success: function( data ) {
					response( data );

					jQuery( '#nano_location_city_aj' ).keydown( function( e ) {
						const code = ( e.keyCode ? e.keyCode : e.which );

						if ( 1 === data.length ) {
							if ( 13 === code || 9 === code ) { //Enter keycode
								jQuery( '.autocomplete-suggestions div' ).click();
							}
						} else if ( 1 < data.length ) {
							if ( 13 === code || 9 === code ) { //Enter keycode
								jQuery( '.autocomplete-suggestions .selected' ).click();
							}
						}
					} );
				},
				error: function( xmlhttprequest, textstatus, message ) {
					if ( 'timeout' === textstatus ) {
						alert( 'Sorry this timed out, please try again.' );
					} else {
						alert( textstatus );
					}
				},
			} );
		},
		renderItem: function( item ) {
			const thisInput = jQuery( '#nano_location_city_aj' );
			thisInput.removeClass( 'spinner' );
			if ( 'undefined' === item.combined ) {
				thisInput.attr( 'data-content', 'Sorry no matches, please try again.' );
				thisInput.attr( 'data-toggle', 'popover' );
				thisInput.attr( 'data-placement', 'top' );
				thisInput.popover( 'show' );
				jQuery( '.autocomplete-suggestion' ).addClass( 'd-none' );
			} else {
				thisInput.popover( 'hide' );
				jQuery( '.autocomplete-suggestion' ).removeClass( 'd-none' ).show();
				return '<div class="autocomplete-suggestion" data-city="' + item.city + '" data-state="' + item.state + '" data-val="' + item.combined + '">' + item.combined + '</div>';
			}
		},
		onSelect: function( e, term, item ) {
			jQuery( '#nano_location_city_aj' ).removeClass( 'spinner' );
			jQuery( '#nano_location_city' ).val( item.data( 'city' ) );
			jQuery( '#nano_location_state' ).val( item.data( 'state' ) );
			fv.revalidateField( 'nano_location_city', 'notEmpty' );
			fv.revalidateField( 'nano_location_state', 'notEmpty' );
		},
	} );

	// If the project is just being created confirm with the user that they want to leave without saving

	jQuery( '#save_continue' ).val( '' );

	if ( jQuery( '#project_goal_test' ).length ) {
		jQuery( '.navbar.navbar-light' ).on( 'click', '.nav-item > a, .dropdown-menu a, .navbar-brand', function( e ) {
			if ( ! jQuery( this ).hasClass( 'dropdown-toggle' ) ) {
				const confirmExit = jQuery( '#confirmExit' );
				let startProject = 'no',
					navlinkDestination = jQuery( this ).attr( 'href' );
				if ( jQuery( this ).hasClass( 'nano-logged-in' ) ) {
					startProject = 'yes';
					console.log( '01' );
				}

				e.preventDefault();
				confirmExit.modal( 'toggle' );

				confirmExit.on( 'shown.bs.modal', function( e ) {
					console.log( '0000' );
					console.log( php_vars );
					console.log( startProject );
				    let checkStart = 'no';

					if ( startProject === 'yes' && php_vars.skip_start !== 'nano_start_project_check' ) {
						navlinkDestination = '/dashboard/?create_project=1';
						jQuery( '#project_save' ).removeClass( 'project_fesave' );
						jQuery( '.create_project' ).addClass( 'project_fesave' );
						console.log( '02' );
					}

					jQuery( '#confirmExit' ).on( 'click', '#project_save, a#continue', function( e ) {
						if ( jQuery( this ).attr( 'id' ) === 'save_continue' ) {
							jQuery( '#save_continue' ).val( navlinkDestination );
						}
						if ( startProject === 'yes' && php_vars.skip_start !== 'nano_start_project_check' && checkStart === 'no' ) {
							e.preventDefault();
							confirmExit.modal( 'toggle' );
							jQuery( '#start-a-project-m' ).modal( 'show' );
							checkStart = 'yes';
							jQuery( 'create_project' ).addClass( 'project_fesave' );
						}
					} );
					jQuery( 'a#continue' ).prop( 'href', navlinkDestination );
				} );
			}
		} );
	}

	if ( jQuery( '#project_goal' ).length ) {
		jQuery( '.nav-item > a, .dropdown-menu a, .navbar-brand' ).click( function( e ) {
			if ( ! jQuery( this ).hasClass( 'dropdown-toggle' ) ) {
				const confirmExit = jQuery( '#confirmExit' );
				let startProject = 'no',
					navlinkDestination = jQuery( this ).attr( 'href' );

				if ( jQuery( this ).hasClass( 'nano-logged-in' ) ) {
					startProject = 'yes';
				}

				if ( startProject === 'yes' && php_vars.skip_start !== 'nano_start_project_check' ) {
					navlinkDestination = '/dashboard/?create_project=1';
				}

				e.preventDefault();

				jQuery( 'a#continue' ).prop( 'href', navlinkDestination );
				confirmExit.modal( 'toggle' );

				confirmExit.on( 'shown.bs.modal', function( e ) {
					jQuery( '#confirmExit #project_save' ).click( function( e ) {
						jQuery( '#save_continue' ).val( navlinkDestination );
					} );
				} );
			}
		} );
	}

	// Hide Duplicate Selection of Categories

	//Run once
	hideOptions();

	jQuery( '.catterm' ).change( function() {
		jQuery( '.catterm option' ).show(); //enable everything
		hideOptions(); //disable selected values
	} );

	function hideOptions() {
		const arr = [];
		jQuery( '.catterm option:selected' ).each( function() {
			arr.push( jQuery( this ).val() );
		} );

		jQuery( '.catterm option' ).filter( function() {
			return -1 < jQuery.inArray( jQuery( this ).val(), arr );
		} ).hide();
	}

	// Country Select - show state if USA
	jQuery( '#nano_location_country' ).change( function() {
		if ( 'United States of America' === jQuery( this ).val() ) {
			jQuery( '#nano_location_state' ).addClass( 'active show' );
		} else {
			jQuery( '#nano_location_state' ).removeClass( 'active show' );
		}
	} );

	/* -------------------------------
     -----   validation   -----
     ---------------------------------*/

	const form = jQuery( '#fes' ),
		action = 'duplicate_title_check_ajax',
		security = jQuery( '#nano_endorsement_security' ).val(),
		dupeInputContainer = jQuery( this ).closest( '.input-group' ).find( 'input' ),
		post_id = jQuery( 'input[name="project_post_id"]' ).val();

	const formFields = {

		//Basics
		project_name: {
			validators: {
				notEmpty: {
					message: 'Project name is required. ',
				},
				stringLength: {
					min: 10,
					max: 140,
					message: ' The project name must be more than 10 and less than 140 characters long. ',
				},
				regexp: {
					regexp: /^[a-zA-Z0-9\' ]+$/,
					message: ' The project name can only consist of alphabetical, apostrophes, numbers and spaces  ',
				},
				remote: {
					message: 'Sorry this title has already been used, please try another.',
					method: 'POST',
					url: team_member_object.dupe_url,
					data: {
						post_id: post_id,
					},
				},
			},
		},
		project_main: {
			validators: {
				notEmpty: {
					message: 'Project image is required.',
				},
			},
		},
		nano_featured_image_hidden: {
			validators: {
				notEmpty: {
					enabled: false,
					message: 'Project image is required.',
				},
			},
		},
		project_video: {
			validators: {
				notEmpty: {
					enabled: false,
					message: 'Video field required.',
				},
				regexp: {
					regexp: /(http[s]:\/\/)?(www\.)?(vimeo\.com|youtube\.com|youtu\.be)/i,
					message: 'The URL must either be from youtube, youtu.be or vimeo',
				},
				uri: {
					message: 'The website address is not valid, don\'t forget to use http:// or https://',
				},
			},
		},
		project_category_0: {
			validators: {
				notEmpty: {
					message: 'Project Category A is required.',
				},
			},
		},
		project_category_1: {
			validators: {
				notEmpty: {
					message: 'Project Category B is required.',
				},
			},
		},
		nano_location_city_aj: {
			validators: {
				notEmpty: {
					message: 'Location is required.',
				},
			},
		},
		nano_location_city: {
			validators: {
				notEmpty: {
					message: 'City is required.',
				},
			},
		},
		nano_location_state: {
			excluded: false,
			validators: {
				notEmpty: {
					message: 'State is required.',
				},
			},
		},

		// Goals
		nano_project_long_description: {
			validators: {
				notEmpty: {
					message: 'Goals are required. ',
				},
				stringLength: {
					max: 750,
					message: 'This field cannot exceed 750 characters. ',
				},
			},
		},
		nano_project_short_description: {
			validators: {
				notEmpty: {
					message: 'Abstract is required. ',
				},
				stringLength: {
					max: 750,
					message: 'This field cannot exceed 750 characters. ',
				},
			},
		},
		nano_project_significance: {
			validators: {
				notEmpty: {
					message: 'Project Significance is required. ',
				},
				stringLength: {
					max: 750,
					message: 'This field cannot exceed 750 characters. ',
				},
			},
		},
		nano_goal_timeline: {
			validators: {
				notEmpty: {
					message: 'Timeline is required. ',
				},
				stringLength: {
					max: 500,
					message: 'This field cannot exceed 500 characters. ',
				},
			},
		},
		'nano_goal_milestones[]': {
			validators: {
				notEmpty: {
					message: 'Milestones need to be added.',
				},
				stringLength: {
					max: 140,
					message: 'Milestone fields cannot exceed 140 characters. ',
				},
			},
		},

		// Funding
		project_goal: {
			validators: {
				notEmpty: {
					message: 'Funding Amount is required. ',
				},
				digits: {
					message: 'This field can contain digits only. ',
				},
			},
		},
		nano_fifty_grant_apply: {
			validators: {
				notEmpty: {
					message: 'Funding Type is required. ',
				},
			},
		},
		biz_dba: {
			validators: {
				notEmpty: {
					message: 'Business Name is required. ',
				},
			},
		},
		biz_tax_id_no: {
			validators: {
				notEmpty: {
					message: 'Business Name is required. ',
				},
				ein: {
					message: 'The value is not a valid EIN. ',
				},
			},
		},
		biz_street: {
			validators: {
				notEmpty: {
					message: 'Street is required. ',
				},
			},
		},
		biz_zip: {
			validators: {
				notEmpty: {
					message: 'Zip code is required. ',
				},
				zipCode: {
					country: 'US',
					message: 'The value is not a valid %s zip code.',
				},
			},
		},
		url: {
			validators: {
				enable: false,
				notEmpty: {
					enable: false,
					message: 'URL is required. ',
				},
			},
		},
		biz_city: {
			validators: {
				notEmpty: {
					message: 'City is required. ',
				},
			},
		},
		biz_state: {
			validators: {
				notEmpty: {
					message: 'State is required. ',
				},
			},
		},
		owner_first_name: {
			validators: {
				notEmpty: {
					message: 'First Name is required. ',
				},
			},
		},
		owner_last_name: {
			validators: {
				notEmpty: {
					message: 'Last Name is required. ',
				},
			},
		},
		owner_dob_mon: {
			validators: {
				notEmpty: {
					message: 'DOB Month is required. ',
				},
				digits: {
					message: 'DOB Month is required. ',
				},
			},
		},
		owner_dob_day: {
			validators: {
				notEmpty: {
					message: 'DOB Day is required. ',
				},
				digits: {
					message: 'DOB Day is required. ',
				},
			},
		},
		owner_dob_year: {
			validators: {
				notEmpty: {
					message: 'DOB Year is required. ',
				},
				digits: {
					message: 'DOB Year is required. ',
				},
			},
		},
		owner_personal_id_no: {
			validators: {
				notEmpty: {
					message: 'The last 4 digits of your SSN is required. ',
				},
				digits: {
					message: 'This field can contain digits only. ',
				},
			},
		},
		account_number: {
			validators: {
				notEmpty: {
					message: 'Accounting Number is required. ',
				},
				digits: {
					message: 'This field can contain digits only. ',
				},
			},
		},
		routing_number: {
			validators: {
				notEmpty: {
					message: 'Routing Number is required. ',
				},
				digits: {
					message: 'This field can contain digits only. ',
				},
				stringLength: {
					min: 9,
					max: 9,
					message: 'Routing Number must be 9 digits.',
				},
			},
		},
		routing_number_validate: {
			validators: {
				notEmpty: {
					message: 'This value must match your Routing Number. ',
				},
				digits: {
					message: 'This field can contain digits only. ',
				},
				stringLength: {
					min: 9,
					max: 9,
					message: 'Routing Number must be 9 digits.',
				},
				identical: {
					compare: function() {
						return form.find( 'input[name="routing_number"]' ).val();
					},
					message: 'Please re-check your Routing numbers.',
				},
			},
		},

		//Budget
		nano_budget_description: {
			validators: {
				notEmpty: {
					message: 'Budget overview is required. ',
				},
			},
		},
		'nano_budget_title[]': {
			validators: {
				notEmpty: {
					message: 'Budget title is required. ',
				},
			},
		},
		'nano_budget_value[]': {
			validators: {
				notEmpty: {
					message: 'Budget value is required. ',
				},
				digits: {
					message: 'This field can contain digits only. ',
				},
			},
		},

		// Team
		nano_team_leader: {
			validators: {
				notEmpty: {
					message: 'Team leader photo is required. ',
				},
			},
		},
		nano_team_leader_hidden: {
			validators: {
				notEmpty: {
					enable: false,
					message: 'Team leader photo is required. ',
				},
			},
		},
		nano_team_leader_bio: {
			validators: {
				notEmpty: {
					message: 'Team leader biography is required. ',
				},
			},
		},
		nano_team_leader_name: {
			validators: {
				notEmpty: {
					message: 'Team leader name is required. ',
				},
			},
		},
		nano_team_leader_title: {
			validators: {
				notEmpty: {
					message: 'Team leader title is required. ',
				},
			},
		},
		'nano_team_members[]': {
			validators: {
				emailAddress: {
					message: 'The value is not a valid email address.',
				},
			},
		},
		'nano_endorsement_members[]': {
			validators: {
				notEmpty: {
					enabled: false,
					message: 'An endorsement is required. ',
				},
				emailAddress: {
					message: 'The value is not a valid email address.',
				},
			},
		},
		new_update_subject: {
			validators: {
				notEmpty: {
					enabled: false,
					message: 'A subject is required. ',
				},
			},
		},
		new_update_content: {
			validators: {
				callback: {
					enabled: false,
					message: 'Content is required and must be at least 5 characters',
					callback: function( value ) {
						// Get the plain text without HTML
						const text = tinyMCE.get( 'new_update_content' ).getContent( {
							format: 'text',
						} );
						return text.length <= 200 && text.length >= 5;
					},
				},
			},
		},

	};

	// When the user is going to submit the form for review validate the entire form
	form.formValidation( {
		fields: formFields,
		plugins: {
			bootstrap: new FormValidation.plugins.Bootstrap( {

				rowSelector: function( field, ele ) {
					// field is the field name
					// ele is the field element
					switch ( field ) {
						default:
							return '.formval';
					}
				},

			} ),
			trigger: new FormValidation.plugins.Trigger( {
				event: 'keyup change focus blur',
			} ),
			submitButton: new FormValidation.plugins.SubmitButton( {

			} ),
			sequence: new FormValidation.plugins.Sequence(),
		},
	} );

	// Get plugin instance
	var fv = form.data( 'formValidation' );

	// Disable Enter to submit the form
	jQuery( '#fes input' ).on( 'keyup keypress', function( e ) {
		const keyCode = e.keyCode || e.which;
		if ( 13 === keyCode ) {
			e.preventDefault();
			return false;
		}
	} );

	// Disable Initial State Of Stripe Registration Button
	form.on( 'keyup keypress mouseover', function( e ) {
		let stripeContainer = jQuery( '#funding_container' ),
			stripeRegisterButton = jQuery( '#nano_stripe_connect' ),
			checkA = false,
			checkB = false;

		// If the account information is being filled out, disable until true..
		stripeRegisterButton.prop( 'disabled', true );

		jQuery( '.nan_check' ).each( function() {
			if ( jQuery( this ).hasClass( 'is-valid' ) ) {
				checkA = true;
				checkB = false;
			}
			if ( jQuery( this ).hasClass( 'is-invalid' ) || ( ! jQuery( this ).hasClass( 'is-valid' ) && ! jQuery( this ).hasClass( 'is-invalid' ) ) ) {
				checkB = true;
				return false;
			}
		} );

		if ( checkA && ! checkB ) {
			stripeRegisterButton.prop( 'disabled', false );
		} else {
			stripeRegisterButton.prop( 'disabled', true );
		}
	} );

	// If the matching grant is applied for then require the endorsement field
	fv.on( 'change', '[name="nano_fifty_grant_apply"]', function() {
		const matchingGrantYes = jQuery( this ).val();

		if ( 'yes' === matchingGrantYes ) {
			fv.enableValidator( 'nano_endorsement_members[]', 'notEmpty' );

			// Revalidate the field now
			fv.validateField( 'nano_endorsement_members[]', 'notEmpty' );
		} else {
			fv.disableValidator( 'nano_endorsement_members[]', 'notEmpty' );
		}
	} );

	// If the project preview button has been clicked we've disabled any empty input validator. If there is now a change we need to revalidate
	form.on( 'change keyup', 'input:not(:button), textarea, select', function() {
		if ( jQuery( '.project_fesave' ).hasClass( 'project_preview' ) ) {
			const fieldsName = jQuery( this ).attr( 'name' );

			if ( formFields[ fieldsName ] ) {
				const fieldsRev = formFields[ fieldsName ].validators;
				const emptyfields = '';

				jQuery.each( fieldsRev, function( key, value ) {
					fv.enableValidator( fieldsName, key );
				} );
			}
		}
	} );

	fv.on( 'core.form.invalid', invalidatedForm );

	fv.on( 'core.form.valid', validatedForm );

	function validatedForm( arg ) {
		const agreeCont = jQuery( '#agree-continue' );
		const notLabnotesUrl = getUrlParam( 'labnotes' );

		console.log( 'valid' );

		// If the account information is being filled out, disable until true..
		jQuery( '#nano_stripe_connect' ).prop( 'disabled', false );

		if ( ! agreeCont.hasClass( 'clicked' ) && ( team_member_object.post_status !== 'draft' || arg === 'project_fesubmit' || ! jQuery( '.project_fesave' ).hasClass( 'project_preview' ) ) ) {
			jQuery( '#nano-instructions' ).modal( 'show' );
		}

		agreeCont.on( 'click', function() {
			jQuery( '#projectContent' ).append( '<input type="hidden" id="project_fesubmit" name="project_fesubmit" class="nano_unique_fesubmit" value="project_fesubmit">' );
			jQuery( this ).addClass( 'clicked' );
			jQuery( '.project_fesubmit' ).click();
			jQuery( '#fes' ).submit();
		} );
	}

	function invalidatedForm() {
		if ( ! jQuery( '.nano_unique_fesubmit' ).length ) {
			console.log( 'invalid' );

			formErrors();
		}
	}

	// Read $_GET URL parameters
	function getUrlParam( sParam ) {
		let sPageURL = decodeURIComponent( window.location.search.substring( 1 ) ),
			sURLVariables = sPageURL.split( '&' ),
			sParameterName,
			i;

		for ( i = 0; i < sURLVariables.length; i++ ) {
			sParameterName = sURLVariables[ i ].split( '=' );

			if ( sParameterName[ 0 ] === sParam ) {
				return sParameterName[ 1 ] === undefined ? true : sParameterName[ 1 ];
			}
		}
	}

	// When the user is only saving a draft destroy the form validation.
	jQuery( '.project_fesave' ).on( 'click', function( e ) {
		if ( team_member_object.post_status === 'draft' ) {
			const emptyVals = returnEmptyFields( 'empty' );
			const allFields = returnEmptyFields();

			jQuery( '#FormErrors #error-list' ).html( '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> We need a bit more information to preview your project!' );

			form.formValidation( 'disableValidator', 'nano_team_leader' );
			form.formValidation( 'disableValidator', 'project_main' );

			jQuery.each( emptyVals, function( key, value ) {
				if ( value !== 'project_faq' && value !== 'save_continue' && value !== 'url' && value !== 'bk_token' && value !== 'nano_goal_milestone_date[]' ) {
					fv.disableValidator( value );
				}
			} );

			if ( jQuery( this ).hasClass( 'project_preview' ) ) {
				formErrors();
			}

			jQuery( '#projectContent' ).append( '<input type="hidden" id="project_fesave" name="project_fesave" class="nano_project_fesave" value="project_fesave">' );

			jQuery( this ).addClass( 'project_preview' );

			fv.validate().then( function( status ) {
				if ( 'Valid' === status ) {
					form.submit();
				}
			} );
		} else if ( ! jQuery( '#project_name' ).length ) {
			fv.validate().then( function( status ) {
				if ( 'Valid' === status ) {
					form.submit();
				}
			} );
		} else {
			jQuery( '#projectContent' ).append( '<input type="hidden" id="nano_project_continue" name="nano_project_continue" class="nano_project_continue" value="nano_project_continue">' );
			nano_project_image_check();
		}
	} );

	// We need to confirm that the project image is always present.
	jQuery( '.project_fesubmit' ).on( 'click', function() {
		jQuery( '#FormErrors #error-list' ).html( '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> We need a bit more information to sumit your project!' );
		nano_project_image_check();

		const selected = jQuery( 'input[name=nano_fifty_grant_apply]:checked' ).val();
		if ( 'yes' === selected ) {
			jQuery( '#nano-matching' ).removeClass( 'd-none' ).addClass( 'show fadein' );
			fv.enableValidator( 'nano_endorsement_members[]', 'notEmpty' );

			// Revalidate the field now
			fv.revalidateField( 'nano_endorsement_members[]' );
		}
		if ( 'no' === selected ) {
			fv.disableValidator( 'nano_endorsement_members[]', 'notEmpty' );
		}

		if ( jQuery( '.project_fesave' ).hasClass( 'project_preview' ) ) {
			const emptyFields = returnEmptyFields( 'empty' );
			const count = emptyFields.length;

			jQuery.each( emptyFields, function( key, value ) {
				if ( formFields[ value ] ) {
					const fieldsName = value;
					const fieldsRev = formFields[ fieldsName ].validators;

					jQuery.each( fieldsRev, function( key, value ) {
						fv.enableValidator( fieldsName, key );
					} );

					fv.revalidateField( fieldsName );
				}
			} );

			fv.validate().then( function( status ) {
				if ( status === 'Valid' ) {
					validatedForm( 'project_fesubmit' );
				} else if ( status === 'Invalid' ) {
					formErrors();
				}
			} );
		}
	} );

	function formErrors() {
		const valErrors = '#valerrors';

		// Reset the errors inside the modal first
		jQuery( valErrors ).children( 'li' ).remove();

		let count = 0;
		jQuery( '.tab-pane' ).each( function( index ) {
			const thisId = jQuery( this ).attr( 'id' ),
				thisName = thisId.substr( 0, 1 ).toUpperCase() + thisId.substr( 1 ),
				thisLink = '#' + thisId + '-link',
				tabId = '#' + thisId,
				errorFields = jQuery( this ).find( '.is-invalid' ).closest( '.formval' ).children( '.fv-plugins-message-container' ),
				foundDupe = {},
				button = '<strong>' + thisName + '</strong>',
				idName = '<li class="mt-3" style="list-style: none; margin-left: -20px;">' + button + '</li>';

			if ( count === index && jQuery( this ).find( '.is-invalid' ).length !== 0 ) {
				jQuery( idName ).appendTo( valErrors );
			}

			jQuery( errorFields ).each( function() {
				const $this = jQuery( this );
				if ( foundDupe[ $this.children( '.fv-help-block' ).data( 'field' ) ] ) {
					$this.remove();
				} else {
					// If the error is not a duplicate then create a field for the modal
					foundDupe[ $this.children( '.fv-help-block' ).data( 'field' ) ] = true;
					jQuery( this ).clone().appendTo( valErrors ).wrap( '<li></li>' );
				}
			} );

			//console.log(foundDupe);

			count++;
		} );

		// Now show the message modal
		if ( jQuery( valErrors ).html().length ) {
			jQuery( '#FormErrors' ).modal( 'show' );
		}
	}

	function returnEmptyFields( args ) {
		const emptyVals = [];
		let duplicatesRemoved = '';

		jQuery( '#fes :input:not(:button), #fes textarea, #fes select' ).each( function() {
			const valName = jQuery( this ).attr( 'name' );
			if ( jQuery.inArray( valName, [ 'project_faq', 'save_continue', 'url', 'bk_token', 'project_video', 'project_date_range' ] ) === -1 ) {
				if ( args === 'empty' ) {
					if ( jQuery( this ).val() === '' ) {
						emptyVals.push( valName );
					}
				} else {
					emptyVals.push( valName );
				}
			}
		} );

		duplicatesRemoved = jQuery.unique( emptyVals );

		return duplicatesRemoved;
	}

	// Check whether the project image is there or a video link in it's place. Also checks the team leader's image.
	function nano_project_image_check() {
		// Checking the project image
		if ( jQuery( '#nano_featured_image_hidden' ).length ) {
			fv.enableValidator( 'nano_featured_image_hidden', 'notEmpty' );
			fv.validateField( 'nano_featured_image_hidden', 'notEmpty' ).then( function( status ) {
				if ( 'Valid' === status ) {
					if ( jQuery( 'input[name=project_main]' ).length || jQuery( '.nano-project-image .remove_image' ).length ) {
						fv.updateFieldStatus( 'project_main', 'Valid', 'notEmpty' );
						fv.disableValidator( 'project_main', 'notEmpty' );
					}
				} else {
					fv.enableValidator( 'project_video', 'notEmpty' );
					fv.validateField( 'project_video', 'notEmpty' ).then( function( status ) {
						if ( 'Valid' === status ) {
							if ( jQuery( 'input[name=nano_fifty_grant_apply]' ).length ) {
								fv.disableValidator( 'project_main', 'notEmpty' );
							}
							fv.disableValidator( 'nano_featured_image_hidden', 'notEmpty' );
						} else {
							if ( jQuery( 'input[name=nano_fifty_grant_apply]' ).length ) {
								fv.revalidateField( 'project_main', 'notEmpty' );
							}
							fv.disableValidator( 'project_video', 'notEmpty' );
						}
					} );
				}

				// status can be one of the following value
				// 'NotValidated' (FormValidation.Status.NotValidated): The field is not yet validated
				// 'Valid' (FormValidation.Status.Valid): The field is valid
				// 'Invalid' (FormValidation.Status.Invalid): The field is invalid
			} );

			// Checking the team leader image
			fv.enableValidator( 'nano_team_leader_hidden', 'notEmpty' );
			fv.validateField( 'nano_team_leader_hidden', 'notEmpty' ).then( function( status ) {
				if ( 'Valid' === status ) {
					fv.disableValidator( 'nano_team_leader', 'notEmpty' );
				} else {
					fv.enableValidator( 'nano_team_leader', 'notEmpty' );
				}

				// status can be one of the following value
				// 'NotValidated' (FormValidation.Status.NotValidated): The field is not yet validated
				// 'Valid' (FormValidation.Status.Valid): The field is valid
				// 'Invalid' (FormValidation.Status.Invalid): The field is invalid
			} );
		}
	}

	jQuery( '.nano-project-image' ).on( 'mouseup', '.custom-file-label', function( e ) {
		jQuery( '.nano-project-image .remove_image' ).click();
		removeImage( 'nano_featured_image_hidden', 'nano-project-image', 'project_main' );
		jQuery( '.project_main' ).click();
	} );

	// Ensure that if a project image is removed after a project is published that the user must replace it with another
	jQuery( '.nano-project-image' ).on( 'mouseup click', '.remove_image', function( e ) {
		removeImage( 'nano_featured_image_hidden', 'nano-project-image', 'project_main' );
	} );

	// Adding click element to the select image field
	jQuery( '#basics' ).on( 'mouseover', '.nano-project-image', function() {
		jQuery( '.custom-file-label' ).css( 'cursor', 'pointer' );
	} );

	jQuery( '.nano-project-image' ).bind( 'DOMNodeInserted', function( event ) {
		parent;
		if ( event.target.nodeName === 'INPUT' ) {
			jQuery( '.nano-project-image .project_img' ).on( 'click', function() {
				jQuery( '.project_main' ).click();
			} );
		}
	} );

	function removeImage( hiddenVar, mainVar, imageVar ) {
		const hiddenClass = '#' + hiddenVar,
			hidden = jQuery( hiddenClass ),
			main = jQuery( '.' + mainVar ),
			image = jQuery( '.' + imageVar ),
			updateButton = jQuery( '.project_fesubmit' ),
			previewButton = jQuery( '.project_fesave' );

		if ( hiddenVar === 'nano_team_leader_hidden' ) {
			const croppie = jQuery( '.' + mainVar + ' .croppie-container' );
			croppie.prepend( '<div class="upload-msg text-secondary teamlead_img"><span>Select your photo</span></div>' );
		} else {
			const videoLink = jQuery( '.video_link' );
			videoLink.after( '<div class="w-100"></div><div id="count" class="d-none">2</div>\n' +
                '<div class="hidden_crop"></div>\n' +
                '<div class="croppie-container message_holder pb-3 pt-4 col-sm-6 mb-4">\n' +
                '<div class="upload-msg px-5 text-secondary project_img"><span>Select your project photo</span></div>' );
		}
		// Clearing old attributes
		hidden.val( '' );
		hidden.attr( 'data-url', '' );
		image.addClass( 'is-invalid' );
		main.find( '.image_url' ).remove();
		image.on( 'change', function( e ) {
			fv.addField( imageVar );
			updateButton.prop( 'disabled', false );
			previewButton.prop( 'disabled', false );
			fv.enableValidator( hiddenVar, 'notEmpty' );
			fv.validateField( imageVar ).then( function( status ) {
				if ( 'Valid' === status ) {
					fv.updateFieldStatus( imageVar, 'Valid' );
					image.closest( '.formval' ).children( '.fv-plugins-message-container' ).html( '' );
				}
			} );
		} );
	}

	jQuery( '.nano-team-leader-image' ).on( 'mouseup', '.custom-file-label', function( e ) {
		jQuery( '.nano-team-leader-image .remove_image' ).click();
		removeImage( 'nano_team_leader_hidden', 'nano-team-leader-image', 'nano_team_leader' );
		jQuery( '.nano_team_leader' ).click();
	} );

	jQuery( '.nano-team-leader-image' ).on( 'mouseup click', '.remove_image', function( e ) {
		removeImage( 'nano_team_leader_hidden', 'nano-team-leader-image', 'nano_team_leader' );
	} );

	// Adding click element to the select image field
	jQuery( '#team' ).on( 'mouseover', '.nano-team-leader-image', function() {
		jQuery( '.custom-file-label' ).css( 'cursor', 'pointer' );
		jQuery( '.nano-team-leader-image .project_img' ).on( 'click', function() {
			jQuery( '.nano_team_leader' ).click();
		} );
	} );

	// If last tab is open hide the continue button
	jQuery( 'a[data-toggle="tab"]' ).on( 'shown.bs.tab', function( e ) {
		const continueBtn = jQuery( '.continue-btn' ),
			submitBtn = jQuery( '.submit-btn' );

		continueBtn.removeClass( 'd-none' );
		submitBtn.addClass( 'd-none' );
		if ( jQuery( '#team-tab' ).hasClass( 'active' ) ) {
			continueBtn.addClass( 'd-none' );
			submitBtn.removeClass( 'd-none' );
		}
	} );

	jQuery( '.endorsements-tab' ).on( 'click', function( e ) {
		const target = this.href.split( '#' );
		jQuery( '#nano-titles a' ).filter( '[href="#' + target[ 1 ] + '"]' ).tab( 'show' );
	} );
} );
