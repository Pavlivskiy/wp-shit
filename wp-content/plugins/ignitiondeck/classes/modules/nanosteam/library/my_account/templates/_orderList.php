<div class="md-box-wrapper full-width cf">
	<?php myaccount_tab_nav( $success, $error ); ?>

	<div class="md-box">
		<div class="md-profile">
			<div class="pt-5" id="paymentHistory">

				<?php

				nano_user_token_update( '' );

				if ( ! isset( $_GET['idc_orders'] ) ) {
					$levels           = '';
					$default_timezone = '';
					$orders           = '';
				}

				nano_stripe_orders( $orders, $crowdfunding, $levels, $default_timezone );

				?>
			</div>
		</div>
	</div>
</div>

