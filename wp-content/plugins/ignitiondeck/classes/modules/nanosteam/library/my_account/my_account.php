<?php if ( ! defined( 'ABSPATH' ) ) {
	die( 'Direct access forbidden.' );
}

// Check and replace all instances of the IDK profile tabs

add_action( 'ide_before_fes_display', 'nano_md_ide_fes_tabs' );
add_action( 'md_ide_before_file_upload', 'nano_md_ide_fes_tabs' );
add_action( 'wp', function () {
	require( ABSPATH . WPINC . '/pluggable.php' );
	if ( is_user_logged_in() ) {
		add_action( 'ide_before_backer_profile', 'nano_md_ide_fes_tabs' );
		add_action( 'ide_before_creator_profile', 'nano_md_ide_fes_tabs' );
	}
} );




// Need to fire these removes before IDK renders them.

function remove_idk_actions(){
	remove_action( 'init', 'memberdeck_profile_check' );
	remove_action( 'init', 'idc_init_checks' );
	remove_action( 'init', 'md_ide_check_payment_settings' );
	remove_action( 'ide_before_fes_display', 'md_ide_fes_tabs' );
	remove_action( 'md_ide_before_file_upload', 'md_ide_fes_tabs' );
	remove_action( 'ide_before_backer_profile', 'md_ide_fes_tabs' );
	remove_action( 'ide_before_creator_profile', 'md_ide_fes_tabs' );
	remove_action( 'md_profile_extratabs', 'ide_backer_profile_tab', 1 );
	remove_action( 'md_profile_extratabs', 'ide_creator_profile_tab', 1 );
	remove_action( 'init', 'md_check_show_sc' ); // Disable IDK Stripe Basic Connect Form
}

add_action( 'init', 'remove_idk_actions', 0 );


function nano_md_ide_fes_tabs( $content ) {
	ob_start();
	echo '<div class="memberdeck">';
	include_once NANO_STRIPE . '/library/my_account/templates/_mdProfileTabs.php';
	echo '</div>';
	$buffer = ob_get_contents();
	ob_end_clean();
	echo $buffer;

	return;
}


/**
 * For elements not within the standard IDK user profile
 */
function nano_profile_save() {
	if ( is_user_logged_in() ) {
		$current_user = wp_get_current_user();
		$user_id      = $current_user->ID;
		if ( isset( $_GET['edit-profile'] ) && $_GET['edit-profile'] == $user_id ) {
			if ( isset( $_POST['edit-profile-submit'] ) ) {
				// User's title
				if ( isset( $_POST['nano_user_title'] ) ) {
					$title = sanitize_text_field( $_POST['nano_user_title'] );
					update_user_meta( $user_id, 'nano_user_title', $title );
				}
				// New cropped avatar
				nano_user_avatar( $user_id );
				// Users email notification profile settings
				$user_email_settings = array();
				if ( isset( $_POST['creator_notification_pledge'] ) || isset( $_POST['creator_notification_backers'] ) || isset( $_POST['backer_notification_updates'] ) ) {
					$user_email_settings['creator_notification_pledge']  = isset( $_POST['creator_notification_pledge'] ) && '' != $_POST['creator_notification_pledge'] ? sanitize_text_field( $_POST['creator_notification_pledge'] ) : '';
					$user_email_settings['creator_notification_backers'] = isset( $_POST['creator_notification_backers'] ) && '' != $_POST['creator_notification_backers'] ? sanitize_text_field( $_POST['creator_notification_backers'] ) : '';
					$user_email_settings['backer_notification_updates']  = isset( $_POST['backer_notification_updates'] ) && '' != $_POST['backer_notification_updates'] ? sanitize_text_field( $_POST['backer_notification_updates'] ) : '';
				}
				update_user_meta( $user_id, 'nano_user_email_settings', $user_email_settings );
			}
		}
	}
}

add_action( 'init', 'nano_profile_save' );

/**
 * Handling the User Avatar Image
 *
 * @param $user_id
 */
function nano_user_avatar( $user_id ) {
	// Handle the User Avatar Image
	$wp_upload_dir = wp_upload_dir();
	if ( isset( $_FILES['nano_idc_avatar'] ) && $_FILES['nano_idc_avatar']['size'] > 0 ) {

		$image_resize      = crop_user_avatar( $user_id, 'nano_idc_avatar', 'nano_idc_avatar_hidden', 'thumbnail' );
		$results           = $image_resize['results'];
		$resized_image_url = $image_resize['image'];
		$hero_posted       = $image_resize['posted'];

	} else {
		$hero_posted = false;
		if ( empty( $vars['nano_idc_avatar'] ) ) {
			$team_hero = null;
		} else {
			$team_hero = $vars['nano_idc_avatar'];
		}
		// Check if the already present image is removed
		if ( isset( $_POST['nano_idc_avatar_removed'] ) && 'yes' == $_POST['nano_idc_avatar_removed'] ) {
			$team_hero_removed = true;
		}
	}
}

/**
 * Cropping the user's avatar to 150x150
 *
 * @param $user_id
 * @param $post_file
 * @param $hidden_post_file
 * @param $size
 *
 * @return mixed
 */
function crop_user_avatar( $user_id, $post_file, $hidden_post_file, $size ) {
	$nano_image        = $_FILES[ $post_file ];
	$upload_dir        = wp_upload_dir();
	$attach_id         = '';
	$resized_image_url = '';
	$results           = '';
	$hero_posted       = '';

	if ( isset( $_POST[ $hidden_post_file ] ) && '' != $_POST[ $hidden_post_file ] ) {

		$data  = $_POST[ $hidden_post_file ];
		$data  = explode( ',', $data );
		$data  = base64_decode( $data[1] );
		$image = $data;

		// @new
		$upload_path = str_replace( '/', DIRECTORY_SEPARATOR, $upload_dir['path'] ) . DIRECTORY_SEPARATOR;

		$decoded  = $image;
		$filename = wp_get_current_user()->ID . '.png';

		$hashed_filename = md5( $filename . microtime() ) . '_' . $filename;

		// @new
		$image_upload = file_put_contents( $upload_path . $hashed_filename, $decoded );

		//HANDLE UPLOADED FILE
		if ( ! function_exists( 'wp_handle_sideload' ) ) {
			require_once( ABSPATH . 'wp-admin/includes/file.php' );
		}

		// Without that I'm getting a debug error!?
		if ( ! function_exists( 'wp_get_current_user' ) ) {
			require_once( ABSPATH . 'wp-includes/pluggable.php' );
		}

		// @new
		$file             = array();
		$file['error']    = '';
		$file['tmp_name'] = $upload_path . $hashed_filename;
		$file['name']     = $hashed_filename;
		$file['type']     = 'image/png';
		$file['size']     = filesize( $upload_path . $hashed_filename );

		// upload file to server
		// @new use $file instead of $image_upload
		$results = wp_handle_sideload( $file, array( 'test_form' => false ) );

	} else {
		$results = wp_handle_upload( $nano_image, array( 'test_form' => false ) );
	}

	$image_filetype = wp_check_filetype( basename( $results['file'] ), null );

	if ( strtolower( 'png' ) == $image_filetype['ext'] || strtolower( 'jpg' ) == $image_filetype['ext'] || strtolower( 'gif' ) == $image_filetype['ext'] || strtolower( 'jpeg' ) == $image_filetype['ext'] ) {
		$hero_attachment = array(
			'guid'           => $upload_dir['url'] . '/' . basename( $results['file'] ),
			'post_mime_type' => $image_filetype['type'],
			'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $results['file'] ) ),
			'post_content'   => '',
			'post_status'    => 'inherit',
		);
		$hero_posted     = true;

		// Insert the attachment.
		$attach_id = wp_insert_attachment( $hero_attachment, $results['url'], 0 );

		// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
		require_once( ABSPATH . 'wp-admin/includes/image.php' );

		// Generate the metadata for the attachment, and update the database record.
		$attach_data = wp_generate_attachment_metadata( $attach_id, $results['file'] );
		wp_update_attachment_metadata( $attach_id, $attach_data );

		$resized_image_url = wp_get_attachment_image_src( $attach_id, $size );

		update_user_meta( $user_id, 'nano_idc_avatar', $resized_image_url[0] );

	} else {
		$hero_posted = false;
	}

	$value['results']       = $results;
	$value['attachment_id'] = $attach_id;
	$value['image']         = $resized_image_url;
	$value['posted']        = $hero_posted;

	return $value;

}

/**
 * New Tabs template for the My Accounts section
 *
 */

function myaccount_tab_nav( $success, $error ) {

	global $permalink_structure;
	if ( empty( $permalink_structure ) ) {
		$prefix = '&amp;';
	} else {
		$prefix = '?';
	}

	if ( is_user_logged_in() && ! isset( $current_user ) ) {
		$current_user = wp_get_current_user();
	}
	if ( isset( $current_user ) ) {
		$backer_orders = ID_Member_Order::get_orders_by_user( $current_user->ID );
		$backer_orders = array_reverse( $backer_orders );
		//$creator_orders = nano_ide_creator_order_list() != '' || nano_ide_creator_preorder_list() !='' ? true : false;
	}

	if ( isset( $_POST['edit-profile-submit'] ) ) {
		if ( isset( $_POST['pw'] ) ) {
			$pw = sanitize_text_field( $_POST['pw'] );
		}
		if ( isset( $_POST['cpw'] ) ) {
			$cpw = sanitize_text_field( $_POST['cpw'] );
		}
	} else {
		$pw  = '';
		$cpw = '';
	}

	// ALERTS

	$current_user = wp_get_current_user();
	$user_id      = $current_user->ID;
	$sc_params    = nano_get_sc_params( $user_id );
	$verification = isset( $sc_params ) ? verification_notify_type( $sc_params ) : '';

	// Get the user's tokens.
	$member     = new ID_Member( $user_id );
	$md_credits = $member->get_user_credits();

	echo '<div id="add_alert" class="stripe-error alert alert-info d-none"></div>';
	if ( isset( $_GET['edit-profile'] ) ) { endorsement_notify_pending( 'pending' ); }
	echo ( is_array( $verification ) ? $verification[0] : $verification );

	echo( isset( $error ) ? '<div class="alert alert-danger">' . $error . '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>' : '' );
	echo( isset( $success ) && ! isset( $_POST['edit-endorsement'] ) ? '<div class="alert alert-success">' . $success . ' <a href="' . get_author_posts_url( $current_user->ID ) . '">View Profile</a><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>' : '' );
	echo( isset( $success ) && isset( $_POST['edit-endorsement'] ) ? '<div class="alert alert-success">Endorsement Saved!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>' : '' );
	echo( isset( $md_credits ) && 0 < $md_credits ? '<div class="alert alert-info">You have refunded credits to use. To use your credits, you can support a project up to $'. $md_credits .'.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>' : '' );


	// END ALERTS

	$durl = md_get_durl();

	$pidna                 = isset( $_GET['pidna'] ) ? $_GET['pidna'] : '';
	$page_check            = isset( $_POST['nano_page'] ) ? $_POST['nano_page'] : '';
	$about_tab_check       = 'about-you' == $page_check || 'about-you' == $pidna || ( '' == $pidna && '' == $page_check && ! isset( $_GET['payment_settings'] ) && ! isset( $_GET['idc_orders'] ) ) || ( '' == $pidna && 'default' == $page_check && isset($_GET['edited']) ) ? true : '';
	$settings_tab_check    = 'settings' == $page_check || 'settings' == $pidna ? true : '';
	$endorsement_tab_check = 'endorsements' == $page_check || 'endorsements' == $pidna ? true : '';
	$administer_tab_check  = 'administration' == $page_check || 'administration' == $pidna ? true : '';
	$page_name             = '' != $pidna ? $pidna : $page_check;
	$about_nav             = isset( $_GET['edit-profile'] ) ? 'data-toggle="tab" href="#about-you" role="tab" aria-controls="about-you" aria-selected="' . ( true == $about_tab_check ? 'true' : '' ) . '"' : 'href="' . ( isset( $current_user ) ? $durl . $prefix . 'edit-profile=' . $current_user->ID . '&pidna=about-you" ' : '' );
	$settings_nav          = isset( $_GET['edit-profile'] ) ? 'data-toggle="tab" href="#settings" role="tab" aria-controls="settings" aria-selected="' . ( true == $settings_tab_check ? 'true' : '' ) . '"' : 'href="' . ( isset( $current_user ) ? $durl . $prefix . 'edit-profile=' . $current_user->ID . '&pidna=settings" ' : '' );
	$endorsement_nav       = isset( $_GET['edit-profile'] ) ? 'data-toggle="tab" href="#endorsements" role="tab" aria-controls="endorsements" aria-selected="' . ( true == $endorsement_tab_check ? 'true' : '' ) . '"' : 'href="' . ( isset( $current_user ) ? $durl . $prefix . 'edit-profile=' . $current_user->ID . '&pidna=endorsements" ' : '' );

	$content  = '<div class="mb-2 mb-sm-5'. ( is_mobile() === 'mobile' ? ' pt-2' : '' ) .'">';
	$content .= '<nav id="nano-titles" class="nav nav-pills flex-column flex-sm-row">';
	$content .= isset( $current_user ) ? '<a id="about-you-tab" ' . $about_nav . ' class="nav-link flex-sm-fill text-sm-center ' . ( true == $about_tab_check ? 'active' : '' ) . '">' . __( 'About You', 'nanosteam_8n' ) . '</a>' : '';
	$content .= '<a id="settings-tab" ' . $settings_nav . ' class="nav-link flex-sm-fill text-sm-center ' . ( true == $settings_tab_check ? 'active' : '' ) . '" href="">' . __( 'Settings', 'nanosteam_8n' ) . '</a>';
	$content .= current_user_can( 'create_edit_projects' ) && idc_creator_settings_enabled() ? '<a class="nav-link flex-sm-fill text-sm-center ' . ( isset( $_GET['payment_settings'] ) ? 'active' : '' ) . '" href="' . $durl . $prefix . 'payment_settings=1">' . __( 'Payment Methods', 'nanosteam_8n' ) . '</a>' : '';
	$content .= '<a class="nav-link flex-sm-fill text-sm-center ' . ( isset( $_GET['idc_orders'] ) || ( isset( $_GET['idc_product'] ) && isset( $_GET['paykey'] ) ) ? 'active' : '' ) . '" href="' . $durl . $prefix . 'idc_orders=1">' . __( 'Payment History', 'nanosteam_8n' ) . '</a>';
	$content .= '<a id="endorsements-tab" ' . $endorsement_nav . ' class="d-none nav-link flex-sm-fill text-sm-center ' . ( true == $endorsement_tab_check ? 'active' : '' ) . '">' . __( 'Endorsements', 'nanosteam_8n' ) . '</a>';
	$content .= '</nav>';
	$content .= '</div>';

	echo $content;

}

/**
 * Shortcode called from memberdeck_dashboard() wp-content/plugins/idcommerce/idcommerce-shortcodes.php (Original)
 * Now called from nano_memberdeck_dashboard() in this file.
 *
 */
function nano_memberdeck_dashboard_OLD_2018_08_29() {
	ob_start();
	global $crowdfunding;
	if ( function_exists( 'idf_get_querystring_prefix' ) ) {
		$prefix = idf_get_querystring_prefix();
	} else {
		$prefix = '?';
	}
	$instant_checkout = instant_checkout();
	/* Mange Dashboard Visibility */
	if ( is_user_logged_in() ) {
		//global $customer_id; --> will trigger 1cc notice

		$current_user = wp_get_current_user();
		$user_id      = $current_user->ID;
		$fname        = $current_user->user_firstname;
		$lname        = $current_user->user_lastname;
		$registered   = $current_user->user_registered;
		$key          = md5( $registered . $current_user->ID );
		// expire any levels that they have not renewed
		$level_check = memberdeck_exp_checkondash( $user_id );
		// this is an array user options
		$user_levels = ID_Member::user_levels( $user_id );
	}

	if ( isset( $user_levels ) ) {
		// this is an array of levels a user has access to
		$access_levels = unserialize( $user_levels->access_level );
		if ( is_array( $access_levels ) ) {
			$unique_levels = array_unique( $access_levels );
		}
	}

	$downloads = ID_Member_Download::get_downloads();
	// we have a list of downloads, but we need to get to the levels by unserializing and then restoring as an array
	if ( ! empty( $downloads ) ) {
		// this will be a new array of downloads with array of levels
		$download_array = array();
		foreach ( $downloads as $download ) {
			$new_levels = unserialize( $download->download_levels );
			unset( $download->download_levels );
			// lets loop through each level of each download to see if it matches
			$pass = false;
			if ( ! empty( $new_levels ) ) {
				foreach ( $new_levels as $single_level ) {
					if ( isset( $unique_levels ) && in_array( $single_level, $unique_levels ) ) {
						// if this download belongs to our list of user levels, add it to array
						//$download->download_levels = $new_levels;
						$pass = true;
					}
				}
			}
			// Putting image URL on image_link according to new changes, as attachment_id might be stored in that field instead of URL
			if ( ! empty( $download->image_link ) && false === stristr( $download->image_link, 'http' ) ) {
				$download_thumb = wp_get_attachment_image_src( $download->image_link, 'idc_dashboard_download_image_size' );
				if ( ! empty( $download_thumb ) ) {
					$download->image_link = $download_thumb[0];
					$width                = $download_thumb[1];
					$height               = $download_thumb[2];
					if ( function_exists( 'idf_image_layout_by_dimensions' ) ) {
						$image_layout = idf_image_layout_by_dimensions( $width, $height );
					} else {
						$image_layout = 'landscape';
					}
					$download->image_width  = $width;
					$download->image_height = $height;
					$download->image_layout = $image_layout;
				}
			} elseif ( empty( $download->image_link ) ) {
				$download->image_link   = get_stylesheet_directory_uri() . '/images/dashboard-download-placeholder.jpg';
				$download->image_layout = 'landscape';
			} else {
				$download->image_layout = 'landscape';
			}
			if ( $pass ) {
				$e_date    = ID_Member_Order::get_expiration_data( $user_id, $single_level );
				$sub       = new ID_Member_Subscription( null, $user_id, $single_level );
				$renewable = $sub->is_subscription_renewable();
				if ( $renewable ) {
					$days_left           = idmember_e_date_format( $e_date );
					$download->days_left = $days_left;
				}
				$license_key                 = MD_Keys::get_license( $user_id, $download->id );
				$download->key               = $license_key;
				$download_array['visible'][] = $download;
			} else {
				$download_array['invisible'][] = $download;
			}
		}
		// we should now have an array of downloads that this user has accces to
	}
	if ( is_user_logged_in() ) {
		$dash    = get_option( 'md_dash_settings' );
		$general = maybe_unserialize( get_option( 'md_receipt_settings' ) );
		if ( ! empty( $dash ) ) {
			if ( ! is_array( $dash ) ) {
				$dash = unserialize( $dash );
			}
			if ( isset( $dash['layout'] ) ) {
				$layout = $dash['layout'];
			} else {
				$layout = 1;
			}
			if ( isset( $dash['alayout'] ) ) {
				$alayout = $dash['alayout'];
			} else {
				$alayout = 'md-featured';
			}
			$aname = $dash['aname'];
			if ( isset( $dash['blayout'] ) ) {
				$blayout = $dash['blayout'];
			} else {
				$blayout = 'md-featured';
			}
			$bname = $dash['bname'];
			if ( isset( $dash['clayout'] ) ) {
				$clayout = $dash['clayout'];
			} else {
				$clayout = 'md-featured';
			}
			$cname = $dash['cname'];
			if ( 1 == $layout ) {
				$p_width = 'half';
				$a_width = 'half';
				$b_width = 'half';
				$c_width = 'half';
			} elseif ( 2 == $layout ) {
				$p_width = 'half';
				$a_width = 'half';
				$b_width = 'full';
				$c_width = 'full';
			} elseif ( 3 == $layout ) {
				$p_width = 'full';
				$a_width = 'full';
				$b_width = 'full';
				$c_width = 'full';
			} elseif ( 4 == $layout ) {
				$p_width = 'half';
				$a_width = 'half-tall';
				$b_width = 'half';
				$c_width = 'hidden';
			}
			if ( isset( $dash['powered_by'] ) ) {
				$powered_by = $dash['powered_by'];
			} else {
				$powered_by = 1;
			}
		}

		// If credits are enabled from settings, then get available credits, else set them to 0
		if ( isset( $general['enable_credits'] ) && 1 == $general['enable_credits'] ) {
			$md_credits = md_credits();
		} else {
			$md_credits = 0;
		}
		$settings = get_option( 'memberdeck_gateways', true );
		if ( isset( $settings ) ) {
			$es       = ( isset( $settings['es'] ) ? $settings['es'] : 0 );
			$efd      = ( isset( $settings['efd'] ) ? $settings['efd'] : 0 );
			$eauthnet = ( isset( $settings['eauthnet'] ) ? $settings['eauthnet'] : 0 );
			if ( 1 == $es ) {
				$customer_id = customer_id();
			} elseif ( 1 == $efd ) {
				$fd_card_details = fd_customer_id();
				if ( ! empty( $fd_card_details ) ) {
					$fd_token    = $fd_card_details['fd_token'];
					$customer_id = $fd_card_details;
				}
			} elseif ( 1 == $eauthnet ) {
				$authorize_customer_id = authnet_customer_id();
				if ( ! empty( $authorize_customer_id ) ) {
					$customer_id = $authorize_customer_id['authorizenet_payment_profile_id'];
				} else {
					$customer_id = '';
				}
			}
			$customer_id = apply_filters( 'idc_checkout_form_customer_id', ( isset( $customer_id ) ? $customer_id : '' ), '', $settings );
		}
		if ( $md_credits > 0 || ! empty( $customer_id ) ) {
			$show_occ = true;
		} else {
			$show_occ = false;
		}
		// TODO : Changed
		include_once 'templates/_orderList.php';

		$content = ob_get_contents();
		ob_end_clean();

		return $content;
	} else {
		// TODO : Changed
		include_once NANO_STRIPE . '/library/idc_templates/_protectedPage.php';
		$content = ob_get_contents();
		ob_end_clean();

		return $content;
	}
}

function nano_memberdeck_dashboard() {
	ob_start();

	if ( is_user_logged_in() ) {

		global $first_data;
		global $stripe_api_version;
		$current_user   = wp_get_current_user();
		$user_id        = $current_user->ID;
		$nicename       = $current_user->display_name;
		$user_firstname = $current_user->user_firstname;
		$user_lastname  = $current_user->user_lastname;
		$email          = $current_user->user_email;
		$usermeta       = get_user_meta( $user_id );
		$url            = $current_user->user_url;
		$idc_avatar     = ( isset( $usermeta['idc_avatar'][0] ) ? $usermeta['idc_avatar'][0] : '' );
		if ( ! empty( $idc_avatar ) ) {
			$idc_avatar_data = wp_get_attachment_image_src( $usermeta['idc_avatar'][0] );
			$idc_avatar_url  = $idc_avatar_data[0];
		} else {
			$idc_avatar_data = get_avatar( $user_id );
			$idc_avatar_url  = get_avatar_url( $user_id );
		}
		if ( isset( $usermeta['description'][0] ) ) {
			$description = $usermeta['description'][0];
		}
		$location = ( isset( $usermeta['location'][0] ) ? $usermeta['location'][0] : '' );
		$url      = $current_user->user_url;
		if ( isset( $usermeta['twitter'][0] ) ) {
			$twitter = $usermeta['twitter'][0];
		}
		if ( isset( $usermeta['facebook'] ) ) {
			$facebook = $usermeta['facebook'][0];
		}
		$show_subscriptions = false;
		$settings           = get_option( 'memberdeck_gateways' );
		if ( isset( $settings ) && ! is_idc_free() ) {
			$es       = ( isset( $settings['es'] ) ? $settings['es'] : 0 );
			$ecb      = ( isset( $settings['ecb'] ) ? $settings['ecb'] : 0 );
			$eauthnet = ( isset( $settings['eauthnet'] ) ? $settings['eauthnet'] : '0' );
			if ( isset( $first_data ) && $first_data ) {
				$efd = ( isset( $settings['efd'] ) ? $settings['efd'] : 0 );
			}
			if ( $es ) {
				$customer_id = customer_id();
				if ( ! empty( $customer_id ) ) {
					$has_subscription = ID_Member_Subscription::has_subscription( $user_id );
					if ( ! empty( $has_subscription ) ) {
						$show_subscriptions = true;
					}
				}
			} elseif ( isset( $efd ) && $efd ) {
				$fd_card_details = fd_customer_id();
				if ( ! empty( $fd_card_details ) ) {
					$customer_id = $fd_card_details['fd_token'];
				}
			} elseif ( $eauthnet ) {
				$authnet_customer_ids = authnet_customer_id();
				if ( ! empty( $authnet_customer_ids ) ) {
					$authorizenet_payment_profile_id = $authnet_customer_ids['authorizenet_payment_profile_id'];
					$authorizenet_profile_id         = $authnet_customer_ids['authorizenet_profile_id'];
					$customer_id                     = $authorizenet_payment_profile_id;
					if ( ! empty( $authorizenet_profile_id ) && ! empty( $authorizenet_payment_profile_id ) ) {
						if ( empty( $has_subscription ) ) {
							$has_subscription = ID_Member_Subscription::has_subscription( $user_id );
							if ( ! $show_subscriptions ) {
								$show_subscriptions = true;
							}
						}
					}
				}
			}
		}

		$general = get_option( 'md_receipt_settings' );
		if ( $show_subscriptions ) {
			if ( $eauthnet ) {
				$plans = array();

				// Requiring the library of Authorize.Net
				// TODO : Changed
				require IDC_PATH . 'lib/AuthorizeNet/vendor/authorizenet/authorizenet/AuthorizeNet.php';
				define( 'AUTHORIZENET_API_LOGIN_ID', $settings['auth_login_id'] );
				define( 'AUTHORIZENET_TRANSACTION_KEY', $settings['auth_transaction_key'] );
				if ( '1' == $settings['test'] ) {
					define( 'AUTHORIZENET_SANDBOX', true );
				} else {
					define( 'AUTHORIZENET_SANDBOX', false );
				}

				$subscriptions = $has_subscription;
				foreach ( $subscriptions as $subscription ) {
					if ( 'authorize.net' == $subscription->source ) {
						// Checking if this is a Auth.Net subscription by using API
						$subscription_arb      = new AuthorizeNetARB;
						$response_subscription = $subscription_arb->getSubscriptionStatus( $subscription->subscription_id );
						if ( $response_subscription->isOk() ) {
							// This means that subscription exists so it's of Auth.Net
							// Check it's status, whether it's cancelled or not
							if ( 'canceled' != $response_subscription->xml->status ) {
								$plan       = array();
								$plan['id'] = $subscription->subscription_id;

								// Getting level name to be used as Subscription Name
								$level           = ID_Member_Level::get_level( $subscription->level_id );
								$plan['plan_id'] = $level->level_name;
								$plan['gateway'] = 'authorize.net';
								$plans[]         = $plan;
							}
						}
					}
				}
			} else {
				$sk = stripe_sk();
				if ( ! class_exists( 'Stripe' ) ) {
					// TODO : Changed
					require_once IDC_PATH . 'lib/stripe-php-4.2.0/init.php';
				}
				\Stripe\Stripe::setApiKey( $sk );

				try {
					$subscriptions = \Stripe\Customer::retrieve( $customer_id )->subscriptions->all();
				} catch ( \Stripe\Error\InvalidRequest $e ) {
					//
				} catch ( Exception $e ) {
					//
				}
				if ( ! empty( $subscriptions ) ) {
					$plans = array();
					foreach ( $subscriptions->data as $sub ) {
						if ( 'active' == $sub->status ) {
							$plan            = array();
							$plan_id         = $sub->plan->id;
							$plan['id']      = $sub->id;
							$plan['plan_id'] = $plan_id;
							$plan['gateway'] = 'stripe';
							$plans[]         = $plan;
						}
					}
				}
			}

			// If coinbase is active, then get its subscriptions
			//if ( 1 == $ecb ) {

			//}
		}

		$instant_checkout = instant_checkout();
		// $show_icc = get_user_meta($user_id, 'customer_id', true);
		$show_icc = allow_instant_checkout();
		//$instant_checkout = get_user_meta($user_id, 'instant_checkout', true);
		// Getting the shortcode button lightbox image
		if ( isset( $_POST['edit-profile-submit'] ) ) {
			$idc_avatar      = sanitize_text_field( $_POST['idc_avatar'] );
			$idc_avatar_data = wp_get_attachment_image_src( $idc_avatar );
			$idc_avatar_url  = $idc_avatar_data[0];
			$user_firstname  = sanitize_text_field( $_POST['first-name'] );
			$user_lastname   = sanitize_text_field( $_POST['last-name'] );
			$email           = sanitize_email( $_POST['email'] );
			$nicename        = sanitize_text_field( $_POST['nicename'] );
			$description     = sanitize_text_field( $_POST['description'] );
			$location        = sanitize_text_field( $_POST['location'] );
			$url             = sanitize_text_field( $_POST['url'] );
			$twitter         = sanitize_text_field( $_POST['twitter'] );
			$facebook        = sanitize_text_field( $_POST['facebook'] );
			if ( isset( $_POST['pw'] ) ) {
				$pw = sanitize_text_field( $_POST['pw'] );
			}
			if ( isset( $_POST['cpw'] ) ) {
				$cpw = sanitize_text_field( $_POST['cpw'] );
			}
			$description = esc_attr( $_POST['description'] );
			if ( isset( $_POST['instant_checkout'] ) ) {
				$instant_checkout = absint( $_POST['instant_checkout'] );
			} else {
				$instant_checkout = 0;
			}
			// Storing checkout image for Button shortcode
			if ( isset( $_FILES['button_checkout_image'] ) && $_FILES['button_checkout_image']['size'] > 0 ) {
				$checkout_image_name = sanitize_text_field( $_FILES['button_checkout_image']['name'] );
				$wp_upload_dir       = wp_upload_dir();
				if ( ! function_exists( 'wp_handle_upload' ) ) {
					require_once( ABSPATH . 'wp-admin/includes/file.php' );
				}
				$file       = wp_handle_upload( $_FILES['button_checkout_image'], array( 'test_form' => false ) );
				$filetype   = wp_check_filetype( basename( $file['file'] ), null );
				$title      = preg_replace( '/\.[^.]+$/', '', basename( $file['file'] ) );
				$attachment = array(
					'guid'           => $wp_upload_dir['url'] . '/' . basename( $file['file'] ),
					'post_mime_type' => $filetype['type'],
					'post_title'     => $checkout_image_name,
					'post_content'   => '',
					'post_status'    => 'inherit',
					'post_author'    => $user_id,
				);
				$insert     = wp_insert_attachment( $attachment, $file['file'] );
				// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
				require_once( ABSPATH . 'wp-admin/includes/image.php' );
				// Generate the metadata for the attachment, and update the database record.
				$attach_data = wp_generate_attachment_metadata( $insert, $file['file'] );
				wp_update_attachment_metadata( $insert, $attach_data );
				update_user_meta( $user_id, 'idc_button_checkout_image', $insert );
			}
		}
		$idc_button_checkout_image = get_user_meta( $user_id, 'idc_button_checkout_image', true );

		$error   = null;
		$success = null;

		if ( isset( $pw ) && $pw !== $cpw ) {
			$error = __( 'Passwords do not match', 'nanosteam_8n' );
		} elseif ( isset( $_GET['edited'] ) ) {
			$success = __( 'Profile Updated!', 'nanosteam_8n' );
		}

		// Setting error if there is one
		if ( isset( $_GET['error_msg'] ) ) {
			$error = $_GET['error_msg'];
		}

		// TODO : Changed
		include 'templates/_editProfile.php';
		$content = ob_get_contents();
		ob_end_clean();

		return $content;

	} else {

		include_once NANO_STRIPE . '/library/idc_templates/_protectedPage.php';
		$content = ob_get_contents();
		ob_end_clean();

		return $content;
	}

}

remove_shortcode( 'idc_dashboard', 'memberdeck_dashboard' );
remove_shortcode('memberdeck_dashboard', 'memberdeck_dashboard');

add_action('init', 'add_nano_idc_shortcode', 99);
function add_nano_idc_shortcode() {
	add_shortcode( 'idc_dashboard', 'nano_memberdeck_dashboard' );
	add_shortcode('memberdeck_dashboard', 'nano_memberdeck_dashboard');
}


/**
 * Filter called from memberdeck_profile_check() in idcommerce-ide.php (Original)
 * Now called from nano_memberdeck_profile_check() in this file.
 *
 * @param $content
 *
 * @return string
 */
function nano_memberdeck_profile_form( $content ) {
	ob_start();
	global $first_data;
	global $stripe_api_version;
	$current_user   = wp_get_current_user();
	$user_id        = $current_user->ID;
	$nicename       = $current_user->display_name;
	$user_firstname = $current_user->user_firstname;
	$user_lastname  = $current_user->user_lastname;
	$email          = $current_user->user_email;
	$usermeta       = get_user_meta( $user_id );
	$url            = $current_user->user_url;
	$idc_avatar     = ( isset( $usermeta['idc_avatar'][0] ) ? $usermeta['idc_avatar'][0] : '' );
	if ( ! empty( $idc_avatar ) ) {
		$idc_avatar_data = wp_get_attachment_image_src( $usermeta['idc_avatar'][0] );
		$idc_avatar_url  = $idc_avatar_data[0];
	} else {
		$idc_avatar_data = get_avatar( $user_id );
		$idc_avatar_url  = get_avatar_url( $user_id );
	}
	if ( isset( $usermeta['description'][0] ) ) {
		$description = $usermeta['description'][0];
	}
	$location = ( isset( $usermeta['location'][0] ) ? $usermeta['location'][0] : '' );
	$url      = $current_user->user_url;
	if ( isset( $usermeta['twitter'][0] ) ) {
		$twitter = $usermeta['twitter'][0];
	}
	if ( isset( $usermeta['facebook'] ) ) {
		$facebook = $usermeta['facebook'][0];
	}
	$show_subscriptions = false;
	$settings           = get_option( 'memberdeck_gateways' );
	if ( isset( $settings ) && ! is_idc_free() ) {
		$es       = ( isset( $settings['es'] ) ? $settings['es'] : 0 );
		$ecb      = ( isset( $settings['ecb'] ) ? $settings['ecb'] : 0 );
		$eauthnet = ( isset( $settings['eauthnet'] ) ? $settings['eauthnet'] : '0' );
		if ( isset( $first_data ) && $first_data ) {
			$efd = ( isset( $settings['efd'] ) ? $settings['efd'] : 0 );
		}
		if ( $es ) {
			$customer_id = customer_id();
			if ( ! empty( $customer_id ) ) {
				$has_subscription = ID_Member_Subscription::has_subscription( $user_id );
				if ( ! empty( $has_subscription ) ) {
					$show_subscriptions = true;
				}
			}
		} elseif ( isset( $efd ) && $efd ) {
			$fd_card_details = fd_customer_id();
			if ( ! empty( $fd_card_details ) ) {
				$customer_id = $fd_card_details['fd_token'];
			}
		} elseif ( $eauthnet ) {
			$authnet_customer_ids = authnet_customer_id();
			if ( ! empty( $authnet_customer_ids ) ) {
				$authorizenet_payment_profile_id = $authnet_customer_ids['authorizenet_payment_profile_id'];
				$authorizenet_profile_id         = $authnet_customer_ids['authorizenet_profile_id'];
				$customer_id                     = $authorizenet_payment_profile_id;
				if ( ! empty( $authorizenet_profile_id ) && ! empty( $authorizenet_payment_profile_id ) ) {
					if ( empty( $has_subscription ) ) {
						$has_subscription = ID_Member_Subscription::has_subscription( $user_id );
						if ( ! $show_subscriptions ) {
							$show_subscriptions = true;
						}
					}
				}
			}
		}
	}

	$general = get_option( 'md_receipt_settings' );
	if ( $show_subscriptions ) {
		if ( $eauthnet ) {
			$plans = array();

			// Requiring the library of Authorize.Net
			// TODO : Changed
			require IDC_PATH . 'lib/AuthorizeNet/vendor/authorizenet/authorizenet/AuthorizeNet.php';
			define( 'AUTHORIZENET_API_LOGIN_ID', $settings['auth_login_id'] );
			define( 'AUTHORIZENET_TRANSACTION_KEY', $settings['auth_transaction_key'] );
			if ( '1' == $settings['test'] ) {
				define( 'AUTHORIZENET_SANDBOX', true );
			} else {
				define( 'AUTHORIZENET_SANDBOX', false );
			}

			$subscriptions = $has_subscription;
			foreach ( $subscriptions as $subscription ) {
				if ( 'authorize.net' == $subscription->source ) {
					// Checking if this is a Auth.Net subscription by using API
					$subscription_arb      = new AuthorizeNetARB;
					$response_subscription = $subscription_arb->getSubscriptionStatus( $subscription->subscription_id );
					if ( $response_subscription->isOk() ) {
						// This means that subscription exists so it's of Auth.Net
						// Check it's status, whether it's cancelled or not
						if ( 'canceled' != $response_subscription->xml->status ) {
							$plan       = array();
							$plan['id'] = $subscription->subscription_id;

							// Getting level name to be used as Subscription Name
							$level           = ID_Member_Level::get_level( $subscription->level_id );
							$plan['plan_id'] = $level->level_name;
							$plan['gateway'] = 'authorize.net';
							$plans[]         = $plan;
						}
					}
				}
			}
		} else {
			$sk = stripe_sk();
			if ( ! class_exists( 'Stripe' ) ) {
				// TODO : Changed
				require_once IDC_PATH . 'lib/stripe-php-4.2.0/init.php';
			}
			\Stripe\Stripe::setApiKey( $sk );

			try {
				$subscriptions = \Stripe\Customer::retrieve( $customer_id )->subscriptions->all();
			} catch ( \Stripe\Error\InvalidRequest $e ) {
				//
			} catch ( Exception $e ) {
				//
			}
			if ( ! empty( $subscriptions ) ) {
				$plans = array();
				foreach ( $subscriptions->data as $sub ) {
					if ( 'active' == $sub->status ) {
						$plan            = array();
						$plan_id         = $sub->plan->id;
						$plan['id']      = $sub->id;
						$plan['plan_id'] = $plan_id;
						$plan['gateway'] = 'stripe';
						$plans[]         = $plan;
					}
				}
			}
		}

		// If coinbase is active, then get its subscriptions
		//if ( 1 == $ecb ) {

		//}
	}

	$instant_checkout = instant_checkout();
	// $show_icc = get_user_meta($user_id, 'customer_id', true);
	$show_icc = allow_instant_checkout();
	//$instant_checkout = get_user_meta($user_id, 'instant_checkout', true);
	// Getting the shortcode button lightbox image
	if ( isset( $_POST['edit-profile-submit'] ) ) {
		$idc_avatar      = sanitize_text_field( $_POST['idc_avatar'] );
		$idc_avatar_data = wp_get_attachment_image_src( $idc_avatar );
		$idc_avatar_url  = $idc_avatar_data[0];
		$user_firstname  = sanitize_text_field( $_POST['first-name'] );
		$user_lastname   = sanitize_text_field( $_POST['last-name'] );
		$email           = sanitize_email( $_POST['email'] );
		$nicename        = sanitize_text_field( $_POST['nicename'] );
		$description     = sanitize_text_field( $_POST['description'] );
		$location        = sanitize_text_field( $_POST['location'] );
		$url             = sanitize_text_field( $_POST['url'] );
		$twitter         = sanitize_text_field( $_POST['twitter'] );
		$facebook        = sanitize_text_field( $_POST['facebook'] );
		if ( isset( $_POST['pw'] ) ) {
			$pw = sanitize_text_field( $_POST['pw'] );
		}
		if ( isset( $_POST['cpw'] ) ) {
			$cpw = sanitize_text_field( $_POST['cpw'] );
		}
		$description = esc_attr( $_POST['description'] );
		if ( isset( $_POST['instant_checkout'] ) ) {
			$instant_checkout = absint( $_POST['instant_checkout'] );
		} else {
			$instant_checkout = 0;
		}
		// Storing checkout image for Button shortcode
		if ( isset( $_FILES['button_checkout_image'] ) && $_FILES['button_checkout_image']['size'] > 0 ) {
			$checkout_image_name = sanitize_text_field( $_FILES['button_checkout_image']['name'] );
			$wp_upload_dir       = wp_upload_dir();
			if ( ! function_exists( 'wp_handle_upload' ) ) {
				require_once( ABSPATH . 'wp-admin/includes/file.php' );
			}
			$file       = wp_handle_upload( $_FILES['button_checkout_image'], array( 'test_form' => false ) );
			$filetype   = wp_check_filetype( basename( $file['file'] ), null );
			$title      = preg_replace( '/\.[^.]+$/', '', basename( $file['file'] ) );
			$attachment = array(
				'guid'           => $wp_upload_dir['url'] . '/' . basename( $file['file'] ),
				'post_mime_type' => $filetype['type'],
				'post_title'     => $checkout_image_name,
				'post_content'   => '',
				'post_status'    => 'inherit',
				'post_author'    => $user_id,
			);
			$insert     = wp_insert_attachment( $attachment, $file['file'] );
			// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
			require_once( ABSPATH . 'wp-admin/includes/image.php' );
			// Generate the metadata for the attachment, and update the database record.
			$attach_data = wp_generate_attachment_metadata( $insert, $file['file'] );
			wp_update_attachment_metadata( $insert, $attach_data );
			update_user_meta( $user_id, 'idc_button_checkout_image', $insert );
		}
	}
	$idc_button_checkout_image = get_user_meta( $user_id, 'idc_button_checkout_image', true );

	$error   = null;
	$success = null;

	if ( isset( $pw ) && $pw !== $cpw ) {
		$error = __( 'Passwords do not match', 'nanosteam_8n' );
	} elseif ( isset( $_GET['edited'] ) ) {
		$success = __( 'Profile Updated!', 'nanosteam_8n' );
	}

	// Setting error if there is one
	if ( isset( $_GET['error_msg'] ) ) {
		$error = $_GET['error_msg'];
	}

	// TODO : Changed
	include 'templates/_editProfile.php';
	$content = ob_get_contents();
	ob_end_clean();

	return $content;
}



/**
 * Cross checking the user's member profile
 */
function nano_memberdeck_profile_check() {

	if ( is_user_logged_in() ) {
		$current_user     = wp_get_current_user();
		$user_id          = $current_user->ID;
		$nicename         = $current_user->display_name;
		$user_firstname   = $current_user->user_firstname;
		$user_lastname    = $current_user->user_lastname;
		$email            = $current_user->user_email;
		$customer_id      = function_exists('customer_id') ? customer_id() : '';
		$instant_checkout = function_exists('instant_checkout') ? instant_checkout() : '';
		if ( isset( $_GET['payment_settings'] ) ) {
			if ( isset( $_POST['creator_settings_submit'] ) ) {
				if ( isset( $_POST['instant_checkout'] ) ) {
					$instant_checkout = absint( $_POST['instant_checkout'] );
					update_user_meta( $user_id, 'instant_checkout', $instant_checkout );
				} else {
					$instant_checkout = 0;
					update_user_meta( $user_id, 'instant_checkout', $instant_checkout );
				}
			}
		}
		if ( isset( $_GET['edit-profile'] ) && $_GET['edit-profile'] == $user_id ) {
			if ( isset( $_POST['edit-profile-submit'] ) ) {
				$idc_avatar     = sanitize_text_field( $_POST['idc_avatar'] );
				$user_firstname = sanitize_text_field( $_POST['first-name'] );
				$user_lastname  = sanitize_text_field( $_POST['last-name'] );
				$posted_email   = sanitize_email( $_POST['email'] );
				$nicename       = sanitize_text_field( $_POST['nicename'] );
				$location       = sanitize_text_field( $_POST['location'] );
				$url            = sanitize_text_field( $_POST['url'] );
				$description    = wp_kses_post( $_POST['description'] );
				$url            = sanitize_text_field( $_POST['url'] );
				$twitter        = sanitize_text_field( $_POST['twitter'] );
				$facebook       = sanitize_text_field( $_POST['facebook'] );
				if ( isset( $_POST['instant_checkout'] ) ) {
					$instant_checkout = absint( $_POST['instant_checkout'] );
				} else {
					$instant_checkout = 0;
				}

				$pw  = sanitize_text_field( $_POST['pw'] );
				$cpw = sanitize_text_field( $_POST['cpw'] );

				// Check that email doesn't already exists if it's modified
				if ( $email != $posted_email ) {
					// Email is changed, so check that it doesn't exists in db already
					$check_email = get_user_by( 'email', $posted_email );
					if ( ! empty( $check_email ) ) {
						// That means user with this email already exists, give an error
						if ( function_exists( 'idf_get_querystring_prefix' ) ) {
							$prefix = idf_get_querystring_prefix();
						} else {
							$prefix = '?';
						}
						$dash_url = md_get_durl( is_ssl() );
						echo '<script>location.href="' . $dash_url . $prefix . 'edit-profile=' . $user_id . '&error_msg=' . __( 'User already exists with email ', 'nanosteam_8n' ) . urlencode( $posted_email ) . '";</script>';
						$posted_email = $email;
						// exit();
					}
				}

				if ( $pw == $cpw ) {
					if ( ! empty( $pw ) ) {
						wp_update_user( array(
							'ID'           => $user_id,
							'user_email'   => $posted_email,
							'user_pass'    => $pw,
							'first_name'   => $user_firstname,
							'last_name'    => $user_lastname,
							'display_name' => $nicename,
							'description'  => $description,
							'user_url'     => $url,
						) );
					} else {
						wp_update_user( array(
							'ID'           => $user_id,
							'user_email'   => $posted_email,
							'first_name'   => $user_firstname,
							'last_name'    => $user_lastname,
							'display_name' => $nicename,
							'description'  => $description,
							'user_url'     => $url,
						) );
					}
				}
				update_user_meta( $user_id, 'idc_avatar', $idc_avatar );
				update_user_meta( $user_id, 'instant_checkout', $instant_checkout );
				update_user_meta( $user_id, 'location', $location );
				update_user_meta( $user_id, 'twitter', $twitter );
				update_user_meta( $user_id, 'facebook', $facebook );
			}
			// TODO : Changed
			add_filter( 'the_content', 'nano_memberdeck_profile_form' );
		} elseif ( isset( $_GET['edit-profile'] ) && '' == $_GET['edit-profile'] ) {
			// TODO testing alternate script to avoid redirecting. Keeping this here for future referecne if needed
			// echo '<script>location.href="?edit-profile=' . $user_id . '";</script>';
		}
	}
}

add_action( 'init', 'nano_memberdeck_profile_check' );


/**
 * Replacing IDK / IDC filter for thumbnail on the order lightbox receipt
 *
 * @param $thumbnail
 * @param $last_order
 *
 * @return string
 */

function nano_ide_add_project_order_thumbnail( $thumbnail, $last_order ) {

	$order_level = $last_order->level_id;
	$post_id     = nano_post_id_by_order_level_id( $order_level );

	if ( ! empty( $post_id ) ) {
		if ( $post_id > 0 ) {
			if ( has_post_thumbnail( $post_id ) || get_post_meta( $post_id, 'project_main', true ) != '' ) {
				$thumbnail_url = get_post_meta( $post_id, 'project_main', true ) != '' ? wp_get_attachment_image_src( get_post_meta( $post_id, 'project_main', true ), 'nano-crop-16-9-sm', true ) : wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'nano-crop-16-9-sm', true );
				$img_obj       = $thumbnail_url[0];
			} elseif ( get_post_meta( $post_id, 'ign_product_image1', true ) != '' ) {
				$image   = aq_resize( get_post_meta( $post_id, 'ign_product_image1', true ), 255, null, false, false );
				$img_obj = $image[0];
			} elseif ( get_post_meta( $post_id, 'ign_product_video', true ) != '' ) {
				$video   = get_post_meta( $post_id, 'ign_product_video', true );
				$video   = '' . check_video_url( $video ) . '';
				$img_obj = $video;
			} else {
				$img_obj = get_stylesheet_directory_uri() . '/images/dashboard-download-placeholder.jpg';
			}
		}
	}

	return $img_obj;
}

remove_filter( 'idc_order_level_thumbnail', 'ide_add_project_order_thumbnail', 10, 2 );
add_filter( 'idc_order_level_thumbnail', 'nano_ide_add_project_order_thumbnail', 10, 2 );


/**
 * Filter called from idc_init_checks() in idcommerce-ide.php (Original)
 * Now called from nano_idc_init_checks() in this file.
 *
 * @replaces idc_init_checks();
 * See TODOs for major changes to ICK code
 */

function nano_idc_init_checks() {
	if ( isset( $_GET['idc_renew'] ) ) {
		add_filter( 'the_content', 'idc_renew' );
	}
	if ( isset( $_GET['idc_orders'] ) || isset( $_GET['view_receipt'] ) ) {
		// TODO : Changed
		add_filter( 'the_content', 'nano_idc_orders_list' );
	}
}

/**
 * Retriveing a user's orders
 *
 * @replaces idc_renew();
 * See TODOs for major changes to ICK code
 *
 * @param $content
 *
 * @return string
 */
function nano_idc_orders_list( $content ) {

	global $crowdfunding, $global_currency;
	$permalink_structure = get_option( 'permalink_structure' );
	if ( empty( $permalink_structure ) ) {
		$prefix = '&';
	} else {
		$prefix = '?';
	}
	$default_timezone = get_option( 'timezone_string' );
	if ( empty( $default_timezone ) ) {
		$default_timezone = 'UTC';
	}
	// The call for cancelling the order/pledge
	if ( isset( $_GET['cancel_pledge'] ) ) {
		// Restoring the credits, getting level details for the credits
		// nano_token_refund_cancelled_pledge();
	}
	ob_start();

	$success = null;
	$error   = null;

	$levels = ID_Member_Level::get_levels();

	if ( isset( $_GET['idc_orders'] ) ) {
		echo '<div class="memberdeck ignitiondeck">';
		// TODO : Changed
		include_once 'templates/_mdProfileTabs.php';
		//include_once IDC_PATH.'templates/_mdProfileTabs.php';
		// TODO : Changed
		include_once 'templates/_orderList.php';
		//include_once IDC_PATH.'templates/_orderList.php';
	}
	if ( isset( $_GET['view_receipt'] ) ) {

		$current_user = wp_get_current_user();

		if ( isset( $_GET['nano_id'] ) ) {

			$post_id     = get_the_ID();
			$project_id  = get_post_meta( $post_id, 'ign_project_id', true );
			$level_id    = nano_get_level_id( $project_id );
			$orders      = ID_Member_Order::get_orders_by_level( $level_id );
			$user_orders = multi_dimen_array_search_all_keys( $orders, 'user_id', $current_user->ID );
			$last_order  = array_values( array_slice( $user_orders, - 1 ) )[0];
			$order_id    = $last_order->id;

		} else {

			$order_id = $_GET['view_receipt'];

		}

		$order      = new ID_Member_Order( $order_id );
		$last_order = $order->get_order();

		if ( $last_order->user_id == $current_user->ID ) {
			$i = 0;
			foreach ( $levels as $level ) {
				$level_id = $level->id;
				if ( $last_order->level_id == $level_id ) {
					$order_level_key = $i;
					break;
				}
				$i ++;
			}

			//$thumbnail = apply_filters( 'idc_order_level_thumbnail', null, $last_order );

			$post_id = nano_post_id_by_order_level_id( $last_order->level_id );
			$thumbnail = nano_project_image( $post_id, 'nano-crop-16-9-sm' );

			//$img_1     = get_post_meta( $post_id, 'project_main', true ) != '' ? get_post_meta( $post_id, 'project_main', true ) : ( has_post_thumbnail( $post_id ) ? wp_get_attachment_url( get_post_thumbnail_id(), 'full' ) : get_post_meta( $post_id, 'ign_product_image1', true ) );
			// $currency_symbol = ID_Member_Order::get_order_currency_sym($order_id);
			// TODO : Changed
			$price = stripos( $order->transaction_id, 'nano' ) == 0 ? '$' . ID_Member_Order::get_order_meta( $last_order->id, 'pwyw_price' ) : apply_filters( 'idc_order_price', $last_order->price, $last_order->id );
			// TODO : Changed

			if ( stripos( $last_order->transaction_id, 'nano' ) === 0 || $last_order->transaction_id === 'credit' ) {
				include_once NANO_STRIPE . '/library/idc_templates/_orderLightbox.php';
			}

		}
	}
	if ( isset( $_GET['idc_orders'] ) ) {
		echo '</div>';
	}
	$content = ob_get_contents();
	ob_end_clean();

	return $content;
}


add_action( 'init', 'nano_idc_init_checks' );

/**
 * Nano Payment Settings from md_ide_check_payment_settings() in idcommerce-ide.php (Original)
 * Now called from nano_md_ide_check_payment_settings() in this file.
 */

function nano_md_ide_check_payment_settings() {
	if ( isset( $_GET['payment_settings'] ) && $_GET['payment_settings'] && is_user_logged_in() && current_user_can( 'create_edit_projects' ) ) {
		if ( idc_creator_settings_enabled() ) {
			// TODO : Changed
			add_filter( 'the_content', 'nano_md_ide_payment_settings' );
		}
	}
}

function nano_md_ide_payment_settings( $content ) {
	ob_start();
	$current_user = wp_get_current_user();
	$user_id      = $current_user->ID;
	$content      = null;
	$settings     = get_option( 'memberdeck_gateways' );
	$crm_settings = get_option( 'crm_settings' );        //Getting CRM settings for mailchimp (show/hide)

	if ( ! empty( $settings ) ) {
		if ( is_array( $settings ) ) {
			$epp_fes = $settings['epp_fes'];
			//$esc = $settings['esc'];
		}
	}

	// Checking if mailchimp settings are enabled in Admin and setting a flog then
	if ( ! empty( $crm_settings ) ) {
		$enable_mailchimp = $crm_settings['enable_mailchimp'];
	} else {
		$enable_mailchimp = 0;
	}

	$paypal_email     = get_user_meta( $user_id, 'md_paypal_email', true ); // deprecated
	$payment_settings = get_user_meta( $user_id, 'md_payment_settings', true );
	// For ease of use, assigning the mail settings to another $var
	$mail_settings = ( isset( $payment_settings['mail_settings'] ) ? $payment_settings['mail_settings'] : array() );

	if ( isset( $_POST['creator_settings_submit'] ) ) {
		if ( empty( $payment_settings ) ) {
			$payment_settings = array();
		}
		do_action( 'md_payment_settings_post', $_POST, $current_user );
		if ( isset( $_POST['paypal_email'] ) ) {
			$paypal_email                     = sanitize_text_field( $_POST['paypal_email'] );
			$payment_settings['paypal_email'] = $paypal_email;
			update_user_meta( $user_id, 'md_paypal_email', $paypal_email ); // deprecated
		}
		$payment_settings = apply_filters( 'md_payment_settings', $payment_settings );

		// Storing mail settings
		$mail_settings = ( isset( $_POST['mail_settings'] ) ? $_POST['mail_settings'] : $mail_settings );
		if ( ! empty( $mail_settings ) ) {
			foreach ( $mail_settings as $k => $v ) {
				$mail_settings[ $k ] = sanitize_text_field( $v );
			}
		}
		// Attaching the mail settings to the $payment_settings variable
		$payment_settings['mail_settings'] = $mail_settings;

		update_user_meta( $user_id, 'md_payment_settings', $payment_settings );
	}
	$form = array();
	if ( isset( $epp_fes ) && $epp_fes ) {
		$form[] = array(
			'label'  => __( 'Paypal Email', 'memberdeck' ),
			'value'  => ( isset( $paypal_email ) ? $paypal_email : '' ),
			'name'   => 'paypal_email',
			'type'   => 'email',
			'class'  => 'required',
			'wclass' => 'form-row',
		);
	}
	$success      = null;
	$error        = null;
	$payment_form = new MD_Form( $form );
	$output       = $payment_form->build_form();
	//echo '<div class="memberdeck">';
	// TODO : Changed
	include_once 'templates/_creatorSettings.php';
	$content = ob_get_contents();
	ob_end_clean();

	return $content;
}


add_action( 'init', 'nano_md_ide_check_payment_settings' );



/* ------------------------
 * Payment History Functions
 * wp-content/themes/nanosteam_child/library/my_account/templates/_orderList.php
 *
------------------------------*/
/**
 * List all orders for the user
 *
 * @param $orders
 * @param $crowdfunding
 * @param $levels
 * @param $default_timezone
 */
function nano_stripe_orders( $orders, $crowdfunding, $levels, $default_timezone ) {

	$stripe_api_version = '2017-08-15';

	if ( is_user_logged_in() ) {

		if ( ! current_user_can( 'create_edit_projects' ) ) {
			return;
		}



		// Listing Pending Payout Amounts
		$pending_orders   = nano_ide_creator_preorder_list();
		$processed_orders = nano_ide_creator_order_list();
		// Backer Payments
		$backer_payments = nano_token_payments( $orders, $crowdfunding, $levels, $default_timezone );

		// Backer Refunds
		$backer_refunds = nano_token_refund_list();

		// Backer Refunds
		if ( current_user_can( 'administrator' )  ) {
			$backer_refunds_all = nano_token_refund_list_all();
		} else {
			$backer_refunds_all = '';
		}

		$content = '';


		$content .= '<h4>' . __( 'Creator Payments', 'nanosteam8n' ) . '</h4>';


		if ( '' != $processed_orders ) {

			$content .= '<h6>' . __( 'Processed Payments.', 'nanosteam8n' ) . '</h6>';
			$content .= '<div class="table-responsive-sm">';
			$content .= '<table class="table table-hover table-sm">';
			$content .= $processed_orders;
			$content .= '</table></div>';
		}

		if ( '' != $pending_orders ) {

			$content .= '<hr>';
			$content .= '<h6>' . __( 'Potential upcoming payments if your fundraising goals are met', 'nanosteam8n' ) . '</h6>';
			$content .= '<div class="table-responsive-sm">';
			$content .= '<table class="table table-hover table-sm">';
			$content .= $pending_orders;
			$content .= '</table></div>';

		}

		if ( '' == $processed_orders && '' == $pending_orders ) {

			$content .= '<h6>' . __( 'You haven\'t started any projects yet!', 'nanosteam8n' ) . '</h6>';

		}

		$content .= '<h4>' . __( 'Backer Payments', 'nanosteam8n' ) . '</h4>';

		if ( '' != $backer_payments ) {

			$content .= '<div class="table-responsive-sm">';
			$content .= '<table class="table table-hover table-sm">';
			$content .= $backer_payments;
			$content .= '</table></div>';

		} else {

			$content .= '<h6>' . __( 'You haven\'t backed any projects yet - we encourage you to be a Steam Backer!', 'nanosteam8n' ) . '</h6>';

		}



		if ( '' != $backer_refunds ) {

			$content .= '<h4>' . __( 'Backer Credit Refunds', 'nanosteam8n' ) . '</h4>';
			$content .= '<p>' . __( 'For backed projects which were unsuccessful with fundraising. Your tokens will expire if not used before the expiry date.', 'nanosteam8n' ) . '</p>';
			$content .= '<div class="table-responsive-sm">';
			$content .= '<table class="table table-hover table-sm">';
			$content .= $backer_refunds;
			$content .= '</table></div>';

		}


		if ( '' != $backer_refunds_all ) {

			$content .= '<h4>' . __( 'All Unused Backer Credit Refunds', 'nanosteam8n' ) . '</h4>';
			$content .= '<div class="table-responsive-sm">';
			$content .= '<table class="table table-hover table-sm">';
			$content .= $backer_refunds_all;
			$content .= '</table></div>';

		}

		echo $content;

	}
}

/**
 * Recursive multidimensional in_array check
 *
 * @param $needle
 * @param $haystack
 * @param $column
 *
 * @return bool
 */
function in_array_r( $needle, $haystack, $column ) {
	if ( in_array( $needle, array_column( $haystack, $column ) ) ) { // search value in the array
		return true;
	} else {
		return false;
	}
}


/**
 * Retrieve a creator's potential orders
 *
 * @return string
 */
function nano_ide_creator_preorder_list() {
	$current_user = wp_get_current_user();
	$user_id      = $current_user->ID;
	$user_login   = $current_user->user_login;

	$content = '';
	$user    = get_user_by( 'id', $user_id );

	$creator_args = array(
		'post_type'      => 'ignition_product',
		'post_status'    => 'publish',
		'posts_per_page' => - 1,
		'tax_query'      => array(
			array(
				'taxonomy'         => 'author',
				'field'            => 'name',
				'terms'            => $user_login,
				'include_children' => false,
			),
		),
	);

	$created_projects = get_posts( apply_filters( 'id_creator_args', $creator_args ) );

	if ( ! empty( $created_projects ) ) {

		foreach ( $created_projects as $created_project ) {
			$project_id  = get_post_meta( $created_project->ID, 'ign_project_id', true );
			$project     = new ID_Project( $project_id );
			$the_project = $project->the_project();

			if ( ! empty( $the_project ) ) {
				$post_id   = $created_project->ID;
				$deck      = new Deck( $project_id );
				$mini_deck = $deck->mini_deck();

				$level = nano_get_level_id( $project_id );

				if ( isset( $level ) ) {

					$orders = ID_Member_Order::get_orders_by_level( $level );

					$project_success = '0' != $mini_deck->days_left ? true : false;

					if ( count( $orders ) > 0 && true == $project_success ) {

						$buttons  = '<div>';
						$buttons .= '<button class="payment-history btn btn-sm btn-primary px-2 py-1 rounded-circle" title="Expand" type="button" data-toggle="collapse" data-target="#nano-pending-' . $project_id . $post_id . '" aria-expanded="false" aria-controls="nano-pending-' . $project_id . $post_id . '">';
						$buttons .= '<i class="fa fa-plus"></i></button>';
						$buttons .= '</div>';

						$ign_project_id = get_post_meta( $post_id, 'ign_project_id', true );
						$project_name   = nano_get_project_name( $ign_project_id );

						$content .= '<tr>';
						$content .= '<td class="d-none d-sm-table-cell" style="width: 80px !important;">' . date( get_option( 'date_format' ), strtotime( $mini_deck->item_fund_end ) ) . '<small> (' . $mini_deck->days_left . ' days left)</small></td>';
						$content .= '<td class="title">';
						$content .= '<strong><a href="' . get_permalink( $mini_deck->post_id ) . '">' . $project_name . '</a></strong><br class="d-sm-none">';
						$content .= '<span class="d-sm-none">' . date( get_option( 'date_format' ), strtotime( $mini_deck->item_fund_end ) ) . ' ( ' . $mini_deck->days_left . ' days left )</span>';
						$content .= '<div id="nano-pending-' . $project_id . $post_id . '" class="collapse small text-muted">';
						$content .= '<div class="d-none d-sm-block"><div class="row"><div class="col-sm-3"><strong>Backer</strong></div><div class="col-sm-3"><strong>Amount</strong></div><div class="col-sm-3"><strong>Date</strong></div></div></div>';

						$last_element = end( $orders );
						foreach ( $orders as $order ) {

							$content .= '<div class="row">';
							// Anonymous Check
							$anonymous = ID_Member_Order::get_order_meta( $order->id - 1, 'anonymous_checkout', true );
							$name      = false == $anonymous ? get_the_author_meta( 'display_name', $order->user_id ) : __( 'Anonymous', 'nanosteam8n' );
							// Matching Grant Name instead of user name if this project has the grant application and is approved.
							$name = ( 2 == $order->user_id || 3 == $order->user_id ) && get_post_meta( $post_id, 'fifty_fifty_grant', true ) == 'approved' ? 'Nanosteam Matching Grant' : $name;


							$price = 'credit' == $order->transaction_id || 0 == stripos( $order->transaction_id, 'nano' ) ? ID_Member_Order::get_order_meta( $order->id, 'pwyw_price' ) : $order->price;

							$content .= '<div class="col-sm-3"><strong class="d-sm-none">Backer:</strong> ' . ( 'Anonymous' == $name || 'Nanosteam Matching Grant' == $name ? $name : '<a href="' . get_author_posts_url( $order->user_id ) . '">' . $name . '</a>' ) . '</div>';
							$content .= '<div class="col-sm-3"><strong class="d-sm-none">Amount:</strong> $' . number_format( $price, 2 ) . '</div>';
							$content .= '<div class="col-sm-3"><strong class="d-sm-none">Date:</strong> ' . date( get_option( 'date_format' ), strtotime( $order->order_date ) ) . '</div>';
							$content .= '</div>' . ( $last_element != $order ? '<hr class="d-sm-none">' : '<div class="d-sm-none pb-3"></div>' );

						}
						$content .= '</div>';
						$content .= '</td>';
						$content .= '<td class="text-right" >$' . preg_replace( '/[^0-9]/', '', $mini_deck->p_current_sale ) . ' / $' . $mini_deck->item_fund_goal . ' </td>';
						$content .= '<td class="text-right" style="width: 80px !important;">' . $buttons . '</td>';
						$content .= '</tr>';

					}
				}
			}
		}
	} else {
		$content = '';
	}

	return $content;
}


/**
 * Retrieve a creators processed orders
 *
 * @return string
 */
function nano_ide_creator_order_list() {

	$settings = get_option( 'memberdeck_gateways' );

	$current_user = wp_get_current_user();
	$user_id      = $current_user->ID;

	if ( ! empty( $settings ) ) {
		if ( is_array( $settings ) ) {
			$test = $settings['test'];
			if ( 1 == $test ) {
				$url = '/test';
			} else {
				$url = '';
			}
		}
	}

	$current_user = wp_get_current_user();
	if ( current_user_can( 'administrator' ) ) {
		$current_user = get_option( 'admin_email' );
		$current_user = get_user_by( 'email', $current_user );
	}
	$user_id    = $current_user->ID;
	$user_login = $current_user->user_login;

	$content = '';

	$user = get_user_by( 'id', $user_id );

	$creator_args = array(
		'post_type'      => 'ignition_product',
		'post_status'    => ( current_user_can( 'administrator' ) ? array(
			'publish',
			'private',
		) : array( 'publish' ) ),
		'posts_per_page' => - 1,
		'tax_query'      => array(
			array(
				'taxonomy'         => 'author',
				'field'            => 'name',
				'terms'            => $user_login,
				'include_children' => false,
			),
		),
	);

	$created_projects = get_posts( apply_filters( 'id_creator_args', $creator_args ) );

	if ( ! empty( $created_projects ) ) {

		foreach ( $created_projects as $created_project ) {
			$project_id  = get_post_meta( $created_project->ID, 'ign_project_id', true );
			$project     = new ID_Project( $project_id );
			$the_project = $project->the_project();

			if ( ! empty( $the_project ) ) {

				$post_id   = $created_project->ID;
				$deck      = new Deck( $project_id );
				$mini_deck = $deck->mini_deck();

				$level                          = nano_get_level_id( $project_id );
				$orders                         = ID_Member_Order::get_orders_by_level( $level );
				$project_success                = '' != $mini_deck->successful && '0' == $mini_deck->days_left ? true : false;
				$default_credit_project_post_id = get_option( 'default_project' );

				if ( count( $orders ) > 0 && true == $project_success && $post_id != $default_credit_project_post_id ) {

					$first_transfer_processed_id = get_post_meta( $post_id, 'first_transfer_transaction_id', true );
					$first_transfer_initiated    = get_post_meta( $post_id, 'first_transfer_transaction_initiated', true );
					$first_transfer_delivered    = get_post_meta( $post_id, 'first_transfer_transaction_delivered', true );

					$final_transfer_processed_id = get_post_meta( $post_id, 'final_transfer_transaction_id', true );
					$final_transfer_initiated    = get_post_meta( $post_id, 'final_transfer_transaction_date_initiated', true );
					$final_transfer_delivered    = get_post_meta( $post_id, 'final_transfer_transaction_date_delivered', true );

					// All In
					$standard_transfer_processed_id = get_post_meta( $post_id, 'standard_transfer_transaction_id', true );
					$standard_transfer_initiated    = get_post_meta( $post_id, 'standard_transfer_transaction_date_initiated', true );
					$standard_transfer_delivered    = get_post_meta( $post_id, 'standard_transfer_transaction_date_delivered', true );

					$std_delivery_message   = '';
					$first_delivery_message = '';
					$final_delivery_message = '';

					if ( '' !== $standard_transfer_initiated ) {

						$std_delivery_message = 'Expected Delivery: ' . date( 'm/d/Y', strtotime( $standard_transfer_initiated . ' + 15 days' )  );
						if ( '' !== $standard_transfer_delivered ) {
							$std_delivery_message = 'Delivered: ' . $standard_transfer_delivered;
						}
					}

					if ( '' !== $first_transfer_initiated ) {
						$first_delivery_message = 'Expected Delivery: ' . date( 'm/d/Y', strtotime( $first_transfer_initiated . ' + 15 days' ) );
						if ( '' !== $first_transfer_delivered ) {
							$first_delivery_message = 'Delivered: ' . $first_transfer_delivered;
						}
					}

					if ( '' !== $final_transfer_initiated ) {
						$final_delivery_message = 'Expected Delivery: ' . date( 'm/d/Y', strtotime( $final_transfer_initiated . ' + 15 days' ) );
						if ( '' !== $final_transfer_delivered ) {
							$final_delivery_message = 'Delivered: ' . $final_transfer_delivered;
						}
					}

					if ( '' !== $first_transfer_initiated && '' == $final_transfer_initiated ) {
						$final_delivery_message = 'Milestone Not Complete';
					}

					$first_transfer_processed_date    = $first_transfer_initiated != '' ?
						'<div class="col-sm-6">First Payment Processed: ' . get_post_meta( $post_id, 'first_transfer_transaction_initiated', true ) . '</div>' .
						'<div class="col-sm-6">' . $first_delivery_message . '</div>' : '';
					$final_transfer_processed_date    = $final_transfer_initiated != '' ?
						'<div class="col-sm-6">Final Payment Processed: ' . get_post_meta( $post_id, 'final_transfer_transaction_date_initiated', true ) . '</div>' .
						'<div class="col-sm-6">' . $final_delivery_message . '</div>' : '';
					$final_transfer_processed_date    = '' !== $first_transfer_initiated && '' == $final_transfer_initiated ?
						'<div class="col-sm-6">Final Payment Processed: PENDING</div>' .
						'<div class="col-sm-6">' . $final_delivery_message . '</div>' : '';
					$standard_transfer_processed_date = $standard_transfer_initiated != '' ?
						'<div class="col-sm-6">Payment Processed: ' . get_post_meta( $post_id, 'standard_transfer_transaction_date_initiated', true ) . '</div>' .
						'<div class="col-sm-6">' . $std_delivery_message . '</div>' : '';

					$matching_payment_initialized = get_post_meta( $post_id, 'first_transfer_transaction_initiated', true ) != '' ? date( 'm/d/Y', strtotime( get_post_meta( $post_id, 'first_transfer_transaction_initiated', true ) ) ) : '';
					$matching_payment_initialized = get_post_meta( $post_id, 'final_transfer_transaction_date_initiated', true ) != '' && '' != $matching_payment_initialized ? date( 'm/d/Y', strtotime( get_post_meta( $post_id, 'final_transfer_transaction_date_initiated', true ) ) ) : $matching_payment_initialized;
					$standard_payment_initialized = get_post_meta( $post_id, 'standard_transfer_transaction_date_initiated', true ) != '' ? date( 'm/d/Y', strtotime( get_post_meta( $post_id, 'standard_transfer_transaction_date_initiated', true ) ) ) : '';

					$payment_initiated_date = $matching_payment_initialized . $standard_payment_initialized;
					$payment_initiated_date = '' != $payment_initiated_date ? $payment_initiated_date : 'PENDING';

					$matching_grant_project_check = get_post_meta( $post_id, 'fifty_fifty_grant', true ) == 'approved' ? true : false;

					// FEES & TOTALS
					$fees              = nano_fee_list( $orders, $post_id );

					// Total and Fees
					$total_fund_raised = $fees['total_before_fees'];
					$total_fees        = $fees['total_fees'];
					$stripe_fees       = $fees['stripe_fees'];
					$nanosteam_fees    = $fees['nanosteam_fees'];
					$total_after_fees  = $fees['total_after_fees'];

					$fees  = '<div class="row mb-1 pb-1 border-bottom">';
					$fees .= '<div class="col-sm-12"><strong>Fee Details</strong></div>';
					$fees .= '<div class="col-sm-12">';
					$fees .= 'Stripe Fee: $' . $stripe_fees;
					$fees .= '<br>Nanosteam Fee: $' . $nanosteam_fees;
					$fees .= '<br>Total to you: $' . $total_after_fees;
					if ( true == $matching_grant_project_check ) {

						$fees .= '<br>First Payment: $' . ( ( $total_fund_raised / 2 ) - $total_fees );
						$fees .= '<br>Final Payment: $' . ( $total_fund_raised / 2 );

					}
					$fees .= '</div></div>';

					$processed_payment_date  = $first_transfer_processed_date;
					$processed_payment_date .= $final_transfer_processed_date;
					$processed_payment_date .= $standard_transfer_processed_date;

					$content .= '<tr>';
					$content .= '<td class="d-none d-sm-table-cell" style="width: 80px !important;">' . $payment_initiated_date . '</td>';
					$content .= '<td class="title-name">';
					$content .= '<strong><a href="' . get_permalink( $mini_deck->post_id ) . '">' . get_the_title($post_id) . '</a></strong><br class="d-sm-none">';
					$content .= '<span class="d-sm-none">' . $payment_initiated_date . ' / $' . preg_replace( '/[^0-9]/', '', $mini_deck->p_current_sale ) . ' / $' . $mini_deck->item_fund_goal . '</span>';
					$content .= '<div id="nano-' . $project_id . $post_id . '" class="collapse small text-muted">';
					$content .= '<div class="row mb-1 pb-1 border-bottom">';
					$content .= '<div class="col-sm-12"><strong>Payment Details</strong></div>';
					$content .= '' != $processed_payment_date ? $processed_payment_date : '<div class="col-sm-6">Payment Processed: PENDING</div><div class="col-sm-6">Expected Delivery: PENDING</div>';
					$content .= '</div>';
					$content .= ( '' != $processed_payment_date ? $fees : '' );
					$content .= '<div class="row">';
					$content .= '<div class="col-sm-12"><strong>Backer Details</strong></div>';
					$content .= '<div class="col-sm-12 d-none d-sm-block"><div class="row"><div class="col-sm-6"><strong>Name</strong></div><div class="col-sm-3"><strong>Amount</strong></div><div class="col-sm-3"><strong>Order Date</strong></div></div></div>';
					$content .= '<div class="col-sm-12">';
					foreach ( $orders as $order ) {

						// Anonymous Check
						$anonymous = ID_Member_Order::get_order_meta( $order->id, 'anonymous_checkout', true );
						$name      = false == $anonymous ? get_the_author_meta( 'display_name', $order->user_id ) : __( 'Anonymous', 'nanosteam8n' );
						// Matching Grant Name instead of user name if this project has the grant application and is approved.
						$name = ( 2 == $order->user_id || 3 == $order->user_id ) && get_post_meta( $post_id, 'fifty_fifty_grant', true ) == 'approved' ? 'Nanosteam Matching Grant' : $name;

						$price    = 'credit' == $order->transaction_id || false != stripos( $order->transaction_id, 'nano' ) || 0 == stripos( $order->transaction_id, 'nano' ) ? ID_Member_Order::get_order_meta( $order->id, 'pwyw_price' ) : $order->price;
						$content .= '<div class="row">';
						$content .= '<div class="col-sm-6"><strong class="d-sm-none">Backer:</strong> ' . ( 'Anonymous' == $name || 'Nanosteam Matching Grant' == $name ? $name : '<a href="' . get_author_posts_url( $order->user_id ) . '">' . $name . '</a>' ) . '</div>';
						$content .= '<div class="col-sm-3"><strong class="d-sm-none">Amount:</strong> $' . number_format( $price, 2 ) . '</div>';
						$content .= '<div class="col-sm-3"><strong class="d-sm-none">Order Date:</strong> ' . date( get_option( 'date_format' ), strtotime( $order->order_date ) ) . '</div>';
						$content .= '</div>';

					}
					$content .= '</div></div></div>';
					$content .= '</td>';

					$content .= '<td class="text-right d-none d-sm-table-cell">$' . number_format( floatval( preg_replace( '/[^0-9]/', '', $mini_deck->p_current_sale ) ), 2 ) . ' / $' . number_format( floatval( preg_replace( '/[^0-9]/', '', $mini_deck->item_fund_goal ) ), 2 ) . ' </td>';
					$content .= '<td class="text-right"  style="width: 80px !important;">';
					$content .= '<button class="payment-history btn btn-sm btn-primary px-2 py-1 rounded-circle" title="Expand" type="button" data-toggle="collapse" data-target="#nano-' . $project_id . $post_id . '" aria-expanded="false" aria-controls="nano-' . $project_id . $post_id . '">';
					$content .= '<i class="fa fa-plus"></i></button>';
					$content .= '</td></tr>';
				}
			}
		}
	} else {

		$content = '';

	}

	return $content;

}
