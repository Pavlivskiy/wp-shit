<tr class="project">

	<?php global $post;

	$user_login      = $current_user->user_login;
	$original_author = get_post_meta( $post_id, 'original_author', true );
	$leader_link     = false !== get_user_by( 'login', $original_author ) ? get_user_by( 'login', $original_author ) : get_user_by( 'email', $original_author );
	$team_lead       = $original_author != $user_login && '' != $original_author ? '<small class="d-block"><a> Team Lead: <a href="' . get_author_posts_url( $leader_link->ID ) . '">' . $leader_link->data->display_name . '</a></small>' : '';

	$is_published            = strtoupper( $status ) != 'PUBLISH' ? false : true;
	$is_published_for_delete = strtoupper( $status ) != 'PUBLISH' ? false : true;
	$preview                 = strtoupper( $status ) != 'PUBLISH' ? '&preview=1' : '';
	$nonce                   = wp_create_nonce( 'ajax-delete-project-' . $post_id );
	$further_requirements    = '' !== get_post_meta( $post_id, 'project_review_message', true ) && 'PUBLISH' !== strtoupper( $status ) ? ' <a href="' . md_get_durl() . $prefix . 'edit_project=' . $post_id . '">(Additional Requirements)</a>' : '';
	// Lab Notes Check
	$author_check                 = false;
	$org_author                   = false;
	$project_status               = false;
	$creator_args                 = array( 'field' => 'name' );
	$checking                     = wp_get_post_terms( $post_id, 'author', $creator_args );
	$project_status               = nano_funding_status( $post_id );
	$original_author              = get_post_meta( $post_id, 'original_author', true );
	$nano_current_user            = wp_get_current_user()->user_login;
	$check_current_user_if_member = multi_dimen_array_search_all_keys( $checking, 'name', $nano_current_user );


	$successfully_funded = in_array( nano_funding_status( $post_id ), array(
		'COMPLETED',
		'SUCCESS'
	) ) ? '<br><strong>Successfully Funded</strong>' : '';

	if ( in_array( nano_funding_status( $post_id ), array( 'LIVE' ) ) ) {
		$successfully_funded = '<br><strong>Currently Fundraising</strong>';
	}

	if ( in_array( nano_funding_status( $post_id ), array( 'REALLOCATED', 'EXPIRED' ) ) ) {
		$successfully_funded = '<br><strong>Unsuccessful</strong>';
	}

	if ( in_array( ucfirst( $status ), array( 'Pending' ) ) ) {
		$successfully_funded = '<br><strong>Under Review</strong>';
	}

	if ( in_array( ucfirst( $status ), array( 'Draft' ) ) ) {
		$successfully_funded = '<br><strong>Not Submitted</strong>';
	}


	if ( is_array( $check_current_user_if_member ) ) {

		if ( 'publish' == get_post_status() && ( 'SUCCESS' === $project_status || 'COMPLETED' === $project_status ) ) {
			$author_check   = true;
			$project_status = true;
		}

		if ( $nano_current_user === $original_author ) {
			$author_check = true;
			$org_author   = true;
		}
	}

	$team_labnote = true === $is_published && false === $org_author && true === $project_status ? '&labnotes#labnotes' : '';


	?>

	<td class="thumb">
		<?php

		if ( get_post_meta( $post_id, 'project_main', true ) != '' || has_post_thumbnail( $post_id ) ) {
			$thumbnail_url = wp_get_attachment_image_src( ( get_post_meta( $post_id, 'project_main', true ) != '' ? get_post_meta( $post_id, 'project_main', true ) : get_post_thumbnail_id() ), 'nano-crop-16-9-sm', true );
			$img_obj       = '<img class="img-fluid mb-sm-0 mb-2" src="' . $thumbnail_url[0] . '" width="' . $thumbnail_url[1] . '" height="' . $thumbnail_url[2] . '" alt="' . get_the_title() . '" />';
		} elseif ( get_post_meta( $post_id, 'ign_product_image1', true ) != '' ) {
			$retina  = '';
			$image   = aq_resize( get_post_meta( $post_id, 'ign_product_image1', true ), 'true' === $retina ? 510 : 255, null, false, false );
			$img_obj = '<img class="img-fluid mb-sm-0 mb-2" src="' . $image[0] . '" width="' . $image[1] . '" height="' . $image[2] . '" alt="' . get_the_title() . '" />';
		} elseif ( get_post_meta( $post_id, 'ign_product_video', true ) != '' ) {
			$video   = get_post_meta( $post_id, 'ign_product_video', true );
			$video   = '' . check_video_url( $video ) . '';
			$img_obj = '<img class="img-fluid mb-sm-0 mb-2" src="' . $video . '" alt="' . get_the_title() . '" />';
		} else {
			$img_obj = '<img style="object-fit: cover;" class="img-fluid mb-sm-0 mb-2" src="' . get_stylesheet_directory_uri() . '/images/dashboard-download-placeholder.jpg' . '" alt="' . get_the_title() . '" />';
		}
		echo( true === $org_author  ? '<a href="' . md_get_durl() . $prefix . 'edit_project=' . $post_id . '">' : '' );
		echo $img_obj;
		echo( true === $org_author  ? '</a>' : '' );

		?>
		<div class="d-sm-none">
			<div class="w-100">
				<h5><?php echo( true === $org_author ? '<a href="' . md_get_durl() . $prefix . 'edit_project=' . $post_id . '">' . get_the_title( $post_id ) . '</a>' : get_the_title( $post_id ) ); ?></h5>
				<small class="muted">
					Status: <?php echo  ( strtoupper( $status ) == 'PUBLISH' ? __( 'Published on: ', 'krown' ) . get_the_date( '', $post_id ) : ucfirst( $status ) ) . $further_requirements . $successfully_funded; ?></small>
				<?php
				echo $team_lead;
				echo( false == $is_published && false === $org_author ? '<small class="d-block">Once the project has been reviewed, approved and is succesful in its fundraising goal you will have access to this project.</small>' : '' );
				echo( true === $is_published && false == $org_author && true !== $project_status  ? '<small class="d-block">When the project has been successful with its fundraising goal you will have access to this project.</small>' : '' );
				?>
			</div>
			<div class="text-right">
				<div class="btn-group" role="group" aria-label="">
					<?php echo $original_author == $user_login ? '<input name="nano_security_' . $post_id . '" class="d-none" type="hidden" data-post-id="' . $post_id . '" value="' . $nonce . '">' : ''; ?>
					<?php echo( true == $project_status && true === $author_check ? '<a class="btn btn-sm btn-outline-primary" href="' . md_get_durl() . $prefix . 'edit_project=' . $post_id . $team_labnote . '"><i class="fa fa-pencil"></i></a>' : '' ); ?>
					<a class="btn btn-sm btn-outline-primary" href="<?php echo $permalink . $preview; ?>"><i class="fa fa-eye"></i></a>
					<?php echo true === $org_author && false == $is_published_for_delete ? '<a class="btn btn-sm btn-outline-primary delete_project" href="#"><i class="fa fa-trash-o"></i></a>' : ''; ?>
					<?php echo ( true === $project_status ? '<a href="' . md_get_durl() . '?edit_project=' . $post_id . '&labnotes#labnotes"  class="btn btn-sm btn-outline-primary"><i class="fa fa-file-text-o"></i></a>' : '' ); ?>
				</div>
			</div>
		</div>
	</td>


	<td class="title w-50 d-none d-sm-table-cell">
		<h5><?php echo( true === $org_author ? '<a href="' . md_get_durl() . $prefix . 'edit_project=' . $post_id . '">' . get_the_title( $post_id ) . '</a>' : get_the_title( $post_id ) ); ?></h5>
		<small class="muted">
			Status: <?php echo( strtoupper( $status ) == 'PUBLISH' ? __( 'Published on: ', 'krown' ) . get_the_date( '', $post_id ) : ucfirst( $status ) ) . $further_requirements . $successfully_funded;  ?></small>
		<?php
		echo $team_lead;
		echo( false == $is_published && false === $org_author ? '<small class="d-block">Once the project has been reviewed, approved and is succesful in its fundraising goal you will have access to this project.</small>' : '' );
		echo( true === $is_published && false == $org_author && true !== $project_status  ? '<small class="d-block">When the project has been successful with its fundraising goal you will have access to this project.</small>' : '' );
		?>
	</td>

	<td class="buttons d-none d-sm-table-cell text-right align-middle">
		<div class="btn-group" role="group" aria-label="">
			<?php echo $user_login == $original_author ? '<input name="nano_security_' . $post_id . '" class="d-none" type="hidden" data-post-id="' . $post_id . '" value="' . $nonce . '">' : ''; ?>
			<?php echo ( true == $project_status && true === $author_check ? '<a class="btn btn-sm btn-outline-primary" href="' . md_get_durl() . $prefix . 'edit_project=' . $post_id . $team_labnote . '"><i class="fa fa-pencil"></i></a>' : '' ); ?>
			<a class="btn btn-sm btn-outline-primary" href="<?php echo $permalink . $preview; ?>"><i class="fa fa-eye"></i></a>
			<?php echo ( true === $org_author && false == $is_published_for_delete ? '<a class="btn btn-sm btn-outline-primary delete_project" href="#"><i class="fa fa-trash-o"></i></a>' : '' ); ?>
			<?php echo ( true === $project_status ? '<a href="' . md_get_durl() . '?edit_project=' . $post_id . '&labnotes#labnotes"  class="btn btn-sm btn-outline-primary"><i class="fa fa-file-text-o"></i></a>' : '' ); ?>
		</div>
	</td>


</tr>