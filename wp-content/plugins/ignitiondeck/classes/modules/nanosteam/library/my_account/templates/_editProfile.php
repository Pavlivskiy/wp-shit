<div class="memberdeck ignitiondeck">
	<?php include_once '_mdProfileTabs.php'; ?>
	<div class="md-box-wrapper full-width cf">
		<?php
		$title = get_user_meta( $current_user->ID, 'nano_user_title', true );
		myaccount_tab_nav( $success, $error );
		$nano_avatar_img     = get_user_meta( $current_user->ID, 'nano_idc_avatar', true );
		$nano_avatar_msg     = '' == $nano_avatar_img ? '<div class="upload-msg avatar_img">' . __( 'Select your photo', 'nanosteam_8n' ) . '</div>' : '<img class="img-fluid" src="' . $nano_avatar_img . '">';
		$user_email_settings = get_user_meta( $current_user->ID, 'nano_user_email_settings', true );
		$creator_pledge      = isset( $user_email_settings ) && is_array( $user_email_settings ) && 'creator_notification_pledge' == $user_email_settings['creator_notification_pledge'] ? ' checked' : '';
		$creator_backer      = isset( $user_email_settings ) && is_array( $user_email_settings ) && 'creator_notification_backers' == $user_email_settings['creator_notification_backers'] ? ' checked' : '';
		$backer_notify       = isset( $user_email_settings ) && is_array( $user_email_settings ) && 'backer_notification_updates' == $user_email_settings['backer_notification_updates'] ? ' checked' : '';

		if ( isset( $_POST['edit-profile-submit'] ) ) {
			if ( isset( $_POST['pw'] ) ) {
				$pw = sanitize_text_field( $_POST['pw'] );
			}
			if ( isset( $_POST['cpw'] ) ) {
				$cpw = sanitize_text_field( $_POST['cpw'] );
			}
		} else {
			$pw  = '';
			$cpw = '';
		}

		$pidna                 = isset( $_GET['pidna'] ) ? $_GET['pidna'] : '';
		$page_check            = isset( $_POST['nano_page'] ) ? $_POST['nano_page'] : '';
		$about_tab_check       = 'about-you' == $page_check || 'about-you' == $pidna || ( '' == $pidna && '' == $page_check ) || ( '' == $pidna && 'default' == $page_check && isset($_GET['edited']) ) ? 'show active' : '';
		$settings_tab_check    = 'settings' == $page_check || 'settings' == $pidna ? 'show active' : '';
		$endorsement_tab_check = 'endorsements' == $page_check || 'endorsements' == $pidna ? 'show active' : '';

		// Tokens
		$today = nano_get_formated_date_time_gmt( 'm/d/Y' );
		$back  = date( 'm/d/Y', strtotime( '-30 days', strtotime( $today ) ) );

		?>
		<form action="?edit-profile=<?php echo( isset( $current_user->ID ) ? $current_user->ID : '' ); ?>&amp;edited=1"
				method="POST" id="edit-profile" name="edit-profile" enctype="multipart/form-data" novalidate>
			<div class="md-box">
				<div class="md-profile">
					<div id="logged-input" class="no">
						<div class="tab-content" id="myTabContent">
							<div class="tab-pane fade <?php echo $about_tab_check; ?>" id="about-you" role="tabpanel"
									aria-labelledby="about-you-tab">
								<h4><?php _e( 'About You', 'nanosteam_8n' ); ?></h4>
								<p><?php _e( 'Provide us an overview of you, your likes, your wishes, your dreams.', 'nanosteam_8n' ); ?></p>
								<p class="desc-note">
									<strong><?php _e( 'Note: ', 'nanosteam_8n' ); ?></strong><?php _e( 'Fields marked with <span class="starred">*</span> are displayed publicly.', 'nanosteam_8n' ); ?>
								</p>

								<div class="form-group form-row wp-media-buttons">
									<div class="col-sm-12">
										<p><?php _e( 'Upload Avatar', 'nanosteam_8n' ); ?></p>
										<?php wp_nonce_field( 'nano_image_secure', 'nano_image_security' ); ?>
										<div class="nano-avatar-image nano-upload form-row pb-3">
											<div class="formval col-sm-3">
												<input type="hidden" class="d-none" name="idc_avatar" value=""/>
												<div class="custom-file">
													<span class="sr-only">Upload Avatar</span>
													<input type="file" id="nano_idc_avatar" name="nano_idc_avatar"
															class="nano-image-file custom-file-input nano_idc_avatar"
															data-url="<?php echo $nano_avatar_img; ?>"
															value="<?php echo $nano_avatar_img; ?>" accept="image/*"
															aria-label="Upload Avatar" data-fv-field="nano_idc_avatar">
													<label class="custom-file-label" for="nano_idc_avatar">Choose
														image</label>
												</div>
											</div>
											<div class="formval">
												<input type="hidden" id="nano_idc_avatar_hidden"
														name="nano_idc_avatar_hidden"
														value="<?php echo $nano_avatar_img; ?>"
														data-url="<?php echo $nano_avatar_img; ?>" accept="image/*"
														required="" data-fv-field="nano_idc_avatar_hidden">
												<input type="hidden" id="nano_page" name="nano_page"
														value="<?php echo( isset( $_POST['nano_page'] ) ? $_POST['nano_page'] : 'default' ); ?>">
											</div>
											<div class="formval d-none">
												<input type="hidden" id="nano_idc_avatar_removed"
														name="nano_idc_avatar_removed" value="no">
												<div class="invalid-feedback"></div>
											</div>
											<div class="w-100"></div>
											<div class="main_crop croppie-container pb-3 pt-4 col-sm-3 mb-4">
												<?php echo $nano_avatar_msg; ?>
												<div class="nano-upload-wrap">
													<div class="upload-container"></div>
												</div>
											</div>
											<div style="height: 150px;" class="d-none hidden_crop col-sm-3"></div>
										</div>
									</div>
								</div>


								<div class="form-group form-row ">
									<div class="col-sm-4 formval">
										<label for="first-name"><?php _e( 'First Name', 'nanosteam_8n' ); ?></label>
										<input type="text" size="20" class="form-control px-3" name="first-name"
												value="<?php echo( isset( $user_firstname ) ? $user_firstname : '' ); ?>"/>
									</div>
									<div class="col-sm-4 formval">
										<label for="last-name"><?php _e( 'Last Name', 'nanosteam_8n' ); ?></label>
										<input type="text" size="20" class="form-control px-3" name="last-name"
												value="<?php echo( isset( $user_lastname ) ? $user_lastname : '' ); ?>"/>
									</div>
								</div>

								<div class="form-group form-row ">
									<div class="col-sm-3 formval">
										<label for="nicename"><?php _e( 'Display Name <span class="starred">*</span>', 'nanosteam_8n' ); ?></label>
										<input type="text" size="20" class="form-control px-3" name="nicename"
												value="<?php echo( isset( $nicename ) ? $nicename : '' ); ?>"/>
									</div>
									<div class="col-sm-3 formval">
										<label for="location"><?php _e( 'Company Location', 'nanosteam_8n' ); ?></label>
										<input type="location" size="20" class="form-control px-3" name="location"
												value="<?php echo( isset( $location ) ? $location : '' ); ?>"/>
									</div>
								</div>

								<div class="form-row form-group">
									<div class="col-sm-6 formval">
										<label for="title"><?php _e( 'Title', 'nanosteam_8n' ); ?></label>
										<input type="text" size="20" class="form-control px-3" name="nano_user_title"
												value="<?php echo $title; ?>"/>
									</div>
								</div>

								<div class="form-row form-group">
									<div class="col-sm-6 formval">
										<label for="description"><?php _e( 'Bio', 'nanosteam_8n' ); ?></label>
										<textarea row="10" maxlength="450" class="form-control px-3 js-auto-size"
												name="description"><?php echo( isset( $description ) ? stripslashes( html_entity_decode( $description ) ) : '' ); ?></textarea>
									</div>
								</div>

								<div class="form-row form-group">
									<div class="col-12 formval">
										<h4><?php _e( 'Your Social Media', 'nanosteam_8n' ); ?></h4>
									</div>
									<div class="col-sm-3 formval">
										<label for="url"><?php _e( 'Website URL <span class="starred">*</span>', 'nanosteam_8n' ); ?></label>
										<input type="text" size="20" class="form-control px-3" name="url"
												formnovalidate="formnovalidate"
												value="<?php echo( isset( $url ) ? $url : '' ); ?>"/>
									</div>
									<div class="col-sm-3 formval">
										<label for="twitter"><?php _e( 'Twitter URL', 'nanosteam_8n' ); ?></label>
										<input type="text" size="20" class="form-control px-3" name="twitter"
												formnovalidate="formnovalidate"
												value="<?php echo( isset( $twitter ) ? $twitter : '' ); ?>"/>
									</div>
								</div>
								<div class="form-row form-group">
									<div class="col-sm-3 formval">
										<label for="facebook"><?php _e( 'Facebook URL', 'nanosteam_8n' ); ?></label>
										<input type="text" size="20" class="form-control px-3" name="facebook"
												formnovalidate="formnovalidate"
												value="<?php echo( isset( $facebook ) ? $facebook : '' ); ?>"/>
									</div>
								</div>
								<?php echo do_action( 'idc_profile_social_after' ); ?>
								<button type="submit" id="edit-profile-submit-1" class="submit-button btn btn-primary"
										name="edit-profile-submit"><?php _e( 'Update Profile', 'nanosteam_8n' ); ?></button>
								<a class="ml-2 btn btn-primary"
										href="<?php echo get_author_posts_url( $current_user->ID ); ?>">View Profile</a>
							</div>
							<div class="tab-pane fade <?php echo $settings_tab_check; ?>" id="settings" role="tabpanel"
									aria-labelledby="settings-tab">
								<h4 class="mb-4"><?php _e( 'Account Settings', 'nanosteam_8n' ); ?></h4>

								<div class="form-row form-group">
									<div class="col-sm-6 formval">
										<label for="email"><?php _e( 'Email Address <span class="starred">*</span>', 'nanosteam_8n' ); ?></label>
										<input type="email" size="20" class="form-control px-3" autocomplete="username email" name="email"
												value="<?php echo( isset( $email ) ? $email : '' ); ?>"/>
									</div>
								</div>

								<div class="form-row form-group">
									<div class="col-sm-6 formval">
										<label for="pw"><?php _e( 'Password', 'nanosteam_8n' ); ?></label>
										<small class="form-text text-muted d-block d-sm-none">
											<strong><?php _e( 'Note:', 'nanosteam_8n' ); ?></strong> <?php _e( 'changing your password will clear login cookies. You will need to login again after saving.', 'nanosteam_8n' ); ?>
										</small>
										<input type="password" size="20" class="form-control px-3" autocomplete="new-password" name="pw" value="">
										<small class="form-text text-muted d-none d-sm-block">
											<strong><?php _e( 'Note:', 'nanosteam_8n' ); ?></strong> <?php _e( 'changing your password will clear login cookies. You will need to login again after saving.', 'nanosteam_8n' ); ?>
										</small>
									</div>
								</div>
								<div class="form-row form-group mb-4">
									<div class="col-sm-6 formval">
										<label for="cpw"><?php _e( 'Re-enter Password', 'nanosteam_8n' ); ?></label>
										<input type="password" size="20" class="form-control px-3" autocomplete="new-password" name="cpw" value="">
									</div>
								</div>

								<h4 class="mb-4"><?php _e( 'Steam Leader Email Settings', 'nanosteam_8n' ); ?></h4>

								<div class="form-row form-group mb-4">
									<div class="col-sm-6">
										<div class="custom-control custom-checkbox formval">
											<input type="checkbox"
													class="form-control form-control-sm custom-control-input"
													name="creator_notification_pledge" id="creator_notification_pledge"
													value="creator_notification_pledge" <?php echo( isset( $user_email_settings ) && is_array( $user_email_settings ) ? $creator_pledge : ' checked' ); ?>
											>
											<label class="custom-control-label"
													for="creator_notification_pledge"><?php _e( 'New pledge activity', 'nanosteam_8n' ); ?></label>
										</div>
										<div class="custom-control custom-checkbox formval">
											<input type="checkbox"
													class="form-control form-control-sm custom-control-input"
													name="creator_notification_backers"
													id="creator_notification_backers"
													value="creator_notification_backers" <?php echo( isset( $user_email_settings ) && is_array( $user_email_settings ) ? $creator_backer : ' checked' ); ?>
											>
											<label class="custom-control-label"
													for="creator_notification_backers"><?php _e( 'Notifications from Backers', 'nanosteam_8n' ); ?></label>
										</div>
									</div>
								</div>

								<h4 class="mb-4"><?php _e( 'Steam Roller Email Settings', 'nanosteam_8n' ); ?></h4>

								<div class="form-row form-group mb-5">
									<div class="col-sm-6">
										<div class="custom-control custom-checkbox formval">
											<input type="checkbox"
													class="form-control form-control-sm custom-control-input"
													name="backer_notification_updates" id="backer_notification_updates"
													value="backer_notification_updates" <?php echo( isset( $user_email_settings ) && is_array( $user_email_settings ) ? $backer_notify : ' checked' ); ?>
											>
											<label class="custom-control-label"
													for="backer_notification_updates"><?php _e( 'Updates from Projects you\'ve supported', 'nanosteam_8n' ); ?></label>
										</div>
									</div>
								</div>

								<?php echo do_action( 'md_profile_extrafields' ); ?>
								<?php if ( ! is_idc_free() ) { ?>
									<?php if ( $show_subscriptions ) { ?>
										<strong><?php _e( 'Subscriptions', 'nanosteam_8n' ); ?></strong>
										<p class="desc-note"><?php _e( 'Manage active subscriptions', 'nanosteam_8n' ); ?></p>
										<div class="form-row">
											<p class="sub_response"></p>
										</div>
										<div class="form-row half">
											<span class="idc-dropdown">
												<select name="sub_list"
													class="idc-dropdown__select .idc-dropdown__select--white"
													data-userid="<?php echo $user_id; ?>">
													<option value="0"><?php _e( 'Select Subscription', 'nanosteam_8n' ); ?></option>
													<?php
													if ( isset( $plans ) ) {
														foreach ( $plans as $plan ) {
															echo '<option value="' . $plan['id'] . '" data-gateway="' . $plan['gateway'] . '">' . $plan['plan_id'] . '</option>';
														}
													}
													?>
												</select>
											</span>
										</div>
										<div class="form-row half">
											&nbsp;<button name="cancel_sub" class="hidden invert inline"
													disabled><?php _e( 'Cancel Subscription', 'nanosteam_8n' ); ?></button>
										</div>
									<?php } ?>
								<?php } ?>
								<button type="submit" id="edit-profile-submit-2" class="submit-button btn btn-primary"
										name="edit-profile-submit"><?php _e( 'Update Profile', 'nanosteam_8n' ); ?></button>

								<a class="ml-2 btn btn-primary"
										href="<?php echo get_author_posts_url( $current_user->ID ); ?>">View Profile</a>
							</div>
							<div class="<?php echo $endorsement_tab_check; ?>" id="endorsements">
								<?php endorsements_backend(); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<div class="modal fade" id="FormErrors" tabindex="-1" role="dialog" aria-labelledby="FormErrorsTitle" aria-hidden="true">
	<div class="modal-dialog modal-center modal" role="document">
		<div class="modal-content">
			<div class="modal-header border-0">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="p-3 pt-0">
				<div class="row">
					<div class="col-sm mb-2 mb-sm-0" id="valerrors"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	jQuery( document ).on( 'idfMediaPopup', function( e, inputID ) {
		jQuery( '.wp_media_image' ).data( 'active', 0 );
		jQuery( '.' + inputID + '_image' ).data( 'active', 1 );
	});
	jQuery( document ).on( 'idfMediaSelected', function( e, attachment ) {
		if ( jQuery( '.idc_avatar_image' ).data( 'active' ) === 1 ) {
			jQuery( '.idc_avatar_image' ).data( 'active', 0 );
			jQuery( '.idc_avatar_image' ).children( 'img' ).attr( 'src', attachment.url ).show();
		}
	});
</script>