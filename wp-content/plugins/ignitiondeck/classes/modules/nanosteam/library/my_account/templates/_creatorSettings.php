<div class="memberdeck">
	<form method="POST" action="" id="payment-settings" class="payment-settings" novalidate>
		<div>
			<?php myaccount_tab_nav( $success, $error ); ?>
			<?php if ( isset( $epp_fes ) && $epp_fes ) { ?>
				<li class="md-box half">
					<div class="md-profile paypal-settings">
						<?php echo apply_filters( 'md_payment_settings_error', '' ); ?>
						<!-- echo $output; -->
						<h3><?php _e( 'Paypal', 'nanosteam_8n' ); ?></h3>
						<p> <?php _e( 'Add your paypal email id ', 'nanosteam_8n' ); ?></p>
						<div class="form-row">
							<label for="paypal_email"><?php _e( 'Paypal Email', 'nanosteam_8n' ); ?></label>
							<input type="email" id="" name="paypal_email" class="required"
									value="<?php echo $paypal_email; ?>">
						</div>
					</div>
				</li>
			<?php } ?>

			<div class="form-row form-group">
				<div class="border p-4 mb-2 bg-light col-sm-12">
					<h4 class="strong"><span><i
									class="fa fa-lock mr-2"></i></span><?php _e( 'Save your card for future payments', 'nanosteam_8n' ); ?>
					</h4>
					<?php
					$show_icc         = allow_instant_checkout();
					$instant_checkout = instant_checkout();
					if ( ! empty( $show_icc ) ) {
						?>
						<p><?php _e( 'Your payment information is securely and safely stored using Stripe, our encrypted payment provider.', 'nanosteam_8n' ); ?></p>
						<div class="input-group mt-3">
							<div class="custom-control custom-checkbox custom-control-inline">
								<input type="checkbox" class="instant_checkout custom-control-input"
										id="instant_checkout"
										name="instant_checkout" <?php echo( isset( $instant_checkout ) && 1 == $instant_checkout ? 'checked="checked"' : '' ); ?>
										value="1"/>
								<label class="custom-control-label"
										for="instant_checkout"><?php _e( 'Save my card', 'nanosteam_8n' ); ?></label>
							</div>
							<div class="input-group-append pl-2">
								<input type="submit" name="creator_settings_submit" id="creator_settings_submit"
										class="btn btn-sm btn-primary" value="<?php _e( 'Save', 'nanosteam_8n' ); ?>"/>
							</div>
						</div>
					<?php } else { ?>
						<p><?php _e( 'Once you\'ve supported a project you can save your card here to use for future use. Your payment information is securely and safely stored using Stripe, our encrypted payment provider.', 'nanosteam_8n' ); ?></p>
					<?php } ?>
				</div>
				<?php do_action( 'md_profile_extrasettings' ); ?>
			</div>

			<?php if ( $enable_mailchimp ) { ?>
				<div class="md-box half">
					<div class="md-profile mail-chimp">
						<h2><?php _e( 'Mailchimp', 'nanosteam_8n' ); ?></h2>
						<p>
							<?php _e( 'Sign up for a free ', 'nanosteam_8n' ); ?>
							<a href="http://eepurl.com/DqCdz"><?php _e( 'Mailchimp', 'nanosteam_8n' ); ?></a>
							<?php _e( 'account to build your own mailing list comprised of each of your supporters.', 'nanosteam_8n' ); ?>
						</p>
						<div class="form-row">
							<label for="mailchimp_key">
								<?php _e( 'Mailchimp API Key', 'nanosteam_8n' ); ?>
							</label>
							<input type="text" name="mail_settings[mailchimp_key]" id="mailchimp_key"
									value="<?php echo( isset( $mail_settings['mailchimp_key'] ) ? $mail_settings['mailchimp_key'] : '' ); ?>"/>
						</div>
						<div class="form-row">
							<label for="mailchimp_list">
								<?php _e( 'Mailchimp List ID', 'nanosteam_8n' ); ?>
							</label>
							<input type="text" name="mail_settings[mailchimp_list]" id="mailchimp_list"
									value="<?php echo( isset( $mail_settings['mailchimp_list'] ) ? $mail_settings['mailchimp_list'] : '' ); ?>"/>
						</div>

					</div>
				</div>
			<?php } ?>
			<?php nano_stripe_connect(); ?>
			<?php //do_action( 'md_payment_settings_extrafields', $payment_settings ); ?>
			<?php //do_action( 'md_payment_settings_sidebar' ); ?>
		</div>
	</form>
</div>