<?php
global $permalink_structure;
if ( empty( $permalink_structure ) ) {
	$prefix = '&amp;';
} else {
	$prefix = '?';
}
if ( is_user_logged_in() && ! isset( $current_user ) ) {
	$current_user = wp_get_current_user();
}
if ( isset( $current_user ) ) {
	$orders = ID_Member_Order::get_orders_by_user( $current_user->ID );
	$orders = array_reverse( $orders );
}
$durl = md_get_durl();
