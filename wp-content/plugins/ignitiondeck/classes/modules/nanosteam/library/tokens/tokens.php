<?php if ( ! defined( 'ABSPATH' ) ) {
	die( 'Direct access forbidden.' );
}



/**
 * Function to run the token update function for users every day
 */

function nano_user_token_update_cron() {

	$users = get_users( array( 'fields' => array( 'ID' ) ) );
	foreach ( $users as $user_id ) {
		nano_user_token_update( $user_id->ID );
	}

}



/**
 * Retrieve a user's profile and check for expired tokens and adjust if tokens are expired.
 *
 * @param $user_id
 */

function nano_user_token_update( $user_id ) {

	$settings = get_option( 'memberdeck_gateways' );

	if ( ! empty( $settings ) ) {
		if ( is_array( $settings ) ) {
			$test = $settings['test'];
			if ( 1 == $test ) {
				$url = '/test';
			} else {
				$url = '';
			}
		}
	}
	// This is set for when not updating all users and only a specific user at the time of purchase if useing tokens.
	if ( '' == $user_id ) {
		$current_user = wp_get_current_user();
		$user_id      = $current_user->ID;
	}

	// Getting User Data
	global $wpdb;

	$charset_collate = $wpdb->get_charset_collate();
	$table_name      = $wpdb->prefix . 'nano_token_meta_data';

	if ( $wpdb->get_var( "SHOW TABLES LIKE '$table_name'" ) == $table_name ) {

		$sql             = "SELECT * FROM $table_name WHERE user_id LIKE %s";
		$sql             = $wpdb->prepare( $sql, $user_id );
		$refunded_tokens = $wpdb->get_results( $sql );

		if ( isset( $refunded_tokens ) && is_array( $refunded_tokens ) ) {

			$total_refunded = 0;
			$total_unused   = 0;
			foreach ( $refunded_tokens as $refunded_token ) {
				if ( isset( $refunded_token->tokens_unused ) && 0 != $refunded_token->tokens_unused && 'expired' != $refunded_token->has_expired ) {
					$refunded_amount = $refunded_token->token_amount;
					$total_refunded += $refunded_amount;
					$refund_unused   = $refunded_token->tokens_unused;
					$total_unused   += $refund_unused;
				}
			}
			$total_unused = (string) $total_unused;

			// Get the user's tokens.
			$member     = new ID_Member( $user_id );
			$md_credits = $member->get_user_credits();

			$difference = $total_refunded - $md_credits;

			$sub_total_refunded = 0;
			$count              = 0;

			foreach ( $refunded_tokens as $key => $refunded_token ) {

				if ( ( isset( $refunded_token->tokens_unused ) && 0 != $refunded_token->tokens_unused && 'expired' != $refunded_token->has_expired ) ) {

					$refunded_amount          = $refunded_token->token_amount;
					$stripe_fee               = isset($refunded_token->stripe_fee ) ? $refunded_token->stripe_fee : '';
					$refund_unused            = $refunded_token->tokens_unused;
					$post_id                  = $refunded_token->post_id;
					$project_id               = isset( $refunded_token->idk_project_id ) ? $refunded_token->idk_project_id : $refunded_token->project_id;
					$nano_project_id          = isset( $refunded_token->nano_project_uid ) ? $refunded_token->nano_project_uid : '';
					$stripe_charge_id         = isset( $refunded_token->stripe_txn_id ) ? $refunded_token->stripe_txn_id : '';
					$order_id                 = $refunded_token->order_id;
					$token_refund_date        = $refunded_token->token_refund_date;
					$token_expiry_date        = $refunded_token->token_expiry_date;
					$expiry_check_org         = $refunded_token->has_expired;
					$available                = $refunded_amount;
					$org_token_used_date      = $refunded_token->token_used_date;
					$token_used_date          = false;
					$nano_org_new_project_uid = isset( $refunded_token->nano_new_project_uid ) && '' != $refunded_token->nano_new_project_uid ? $refunded_token->nano_new_project_uid : '';
					$nano_new_project_uid     = isset( $refunded_token->nano_new_project_uid ) && '' != $refunded_token->nano_new_project_uid ? $refunded_token->nano_new_project_uid : '';

					if ( $difference > 0 ) {
						if ( $difference >= $refunded_amount ) {
							$available       = 0;
							$difference      = $difference - $refunded_amount;
							$token_used_date = isset( $refunded_token->token_used_date ) && empty( $refunded_token->token_used_date ) == false ? $refunded_token->token_used_date : nano_get_formated_date_time_gmt( 'm/d/Y H:i:s' );
						} elseif ( $difference < $refunded_amount ) {
							$available       = $refunded_amount - $difference;
							$difference      = 0;
							$token_used_date = isset( $refunded_token->token_used_date ) && empty( $refunded_token->token_used_date ) == false ? $refunded_token->token_used_date : nano_get_formated_date_time_gmt( 'm/d/Y H:i:s' );
						}
					}

					// Date check and countdown to Expiry
					$date                    = strtotime( $token_expiry_date . '11:59 PM' );
					$remaining               = $date - time();
					$days_remaining          = floor( $remaining / 86400 ) == 1 ? floor( $remaining / 86400 ) . ' day' : floor( $remaining / 86400 ) . ' days';
					$hours_remaining         = floor( ( $remaining % 86400 ) / 3600 ) == 1 ? floor( ( $remaining % 86400 ) / 3600 ) . ' hour' : floor( ( $remaining % 86400 ) / 3600 ) . ' hours';
					$hours_minutes_remaining = floor( ( $remaining % 86400 ) / 3600 ) == 0 && floor( $remaining / 86400 ) == 0 ? ' / ' . floor( ( $remaining / 60 ) ) . ' minutes' : '';
					$expiry_date             = $available . ' / ' . $refunded_amount . ' ' . apply_filters( 'idc_credits_label', __( 'Credits', 'memberdeck' ), true, $refunded_amount );
					$until_expiry            = $days_remaining . ' / ' . $hours_remaining . $hours_minutes_remaining;

					// Expiry of Tokens: Check if tokens have expired and if so withdraw the credits.

					$expiry_check = $expiry_check_org;

					if ( ( 0 >= $remaining && 'expired' != $expiry_check_org ) ) {
						//ID_Member_Credit::use_credits($user_id, $available);
						nano_use_credits( $user_id, $available );
						$until_expiry    = 'Expired';
						$expiry_check    = 'expired';
						$token_used_date = nano_get_formated_date_time_gmt( 'm/d/Y H:i:s' );
						// Update this transaction's token refund to indicate it has expired on Stripe. Will also include the unused amount from the transaction.
						$order_sub = new ID_Member_Order( $order_id );
						$idc_order = $order_sub->get_order();
						nano_stripe_transaction_update( null, $stripe_charge_id, $nano_project_id, 'EXPIRED' );
					}

					if ( 'expired' == $expiry_check ) {

						nano_update_user_token_meta(
							$user_id, $post_id, $project_id, $nano_project_id, $stripe_charge_id, $order_id,
							$refunded_amount, $token_refund_date, $token_expiry_date, $token_used_date,
							$nano_new_project_uid, $nano_org_new_project_uid, $expiry_check, $expiry_check_org,
							$available, $org_token_used_date, $refund_unused, $stripe_fee
						);

					}
				}
			}
		}
	}
}


/**
 * Update the user's token data
 *
 * @param $user_id
 * @param $post_id
 * @param $project_id
 * @param $nano_project_id
 * @param $stripe_charge_id
 * @param $order_id
 * @param $refunded_amount
 * @param $token_refund_date
 * @param $token_expiry_date
 * @param $token_used_date
 * @param $nano_new_project_uid
 * @param $nano_org_new_project_uid
 * @param $expiry_check
 * @param $expiry_check_org
 * @param $available
 * @param $org_token_used_date
 * @param $refund_unused
 */

function nano_update_user_token_meta(
	$user_id, $post_id, $project_id, $nano_project_id, $stripe_charge_id, $order_id,
	$refunded_amount, $token_refund_date, $token_expiry_date, $token_used_date,
	$nano_new_project_uid, $nano_org_new_project_uid, $expiry_check, $expiry_check_org,
	$available, $org_token_used_date, $refund_unused, $stripe_fee ) {

	// Only use if DEV ENV for testing database updates. Setting to true will bypass array difference checks
	$dev_reset = false;

	$token_details = array(
		'user_id'              => $user_id,
		'post_id'              => $post_id,
		'idk_project_id'       => $project_id,
		'nano_project_uid'     => $nano_project_id,
		'stripe_txn_id'        => $stripe_charge_id,
		'order_id'             => $order_id,
		'token_amount'         => $refunded_amount,
		'stripe_fee'           => $stripe_fee,
		'token_refund_date'    => $token_refund_date,
		'token_expiry_date'    => $token_expiry_date,
		'token_used_date'      => $token_used_date,
		'nano_new_project_uid' => $nano_new_project_uid,
		'has_expired'          => $expiry_check,
		'tokens_unused'        => $available,
	);

	$token_details_org = array(
		'user_id'              => $user_id,
		'post_id'              => $post_id,
		'idk_project_id'       => $project_id,
		'nano_project_uid'     => $nano_project_id,
		'stripe_txn_id'        => $stripe_charge_id,
		'order_id'             => $order_id,
		'token_amount'         => $refunded_amount,
		'stripe_fee'           => $stripe_fee,
		'token_refund_date'    => $token_refund_date,
		'token_expiry_date'    => $token_expiry_date,
		'token_used_date'      => $org_token_used_date,
		'nano_new_project_uid' => $nano_org_new_project_uid,
		'has_expired'          => $expiry_check_org,
		'tokens_unused'        => $refund_unused,
	);

	$difference = array_diff( $token_details, $token_details_org );

	if ( isset( $difference['tokens_unused'] ) || isset( $difference['has_expired'] ) || isset( $difference['nano_new_project_uid'] ) || isset( $difference['token_used_date'] ) || true == $dev_reset ) {

		global $wpdb;
		$charset_collate = $wpdb->get_charset_collate();
		$table_name      = $wpdb->prefix . 'nano_token_meta_data';

		if ( $wpdb->get_var( "SHOW TABLES LIKE '$table_name'" ) == $table_name ) {

			$result = $wpdb->query( $wpdb->prepare( "UPDATE $table_name
                SET token_used_date = %s, nano_new_project_uid = %s, has_expired = %s, tokens_unused = %s
             WHERE order_id = %s", $token_used_date, $nano_new_project_uid, $expiry_check, $available, $order_id )
			);

			nano_debug_query( $result, $difference, 'nano_update_user_token_meta' );

		}
	}
}


/**
 * Retrieve and list all payments
 *
 * @param $orders
 * @param $crowdfunding
 * @param $levels
 * @param $default_timezone
 *
 * @return string
 */

function nano_token_payments( $orders, $crowdfunding, $levels, $default_timezone ) {

	// Loading elements for the default memberdeck profile page on checkout
	if ( ! isset( $_GET['idc_orders'] ) ) {

		$user_id = wp_get_current_user();
		$user_id = $user_id->ID;

		$default_timezone = get_option( 'timezone_string' );
		if ( empty( $default_timezone ) ) {
			$default_timezone = 'UTC';
		}

		$levels = ID_Member_Level::get_levels();

		if ( class_exists( 'ID_Member_Order' ) ) {
			// Orders
			$misc   = ' WHERE user_id = "' . $user_id . '"';
			$orders = ID_Member_Order::get_orders( null, null, $misc );
			$orders = array_reverse( $orders );
		}
	}

	$content = '';

	if ( count( $orders ) > 0 ) {

		$i = 1;

		foreach ( $orders as $order ) {

			$j = 0;
			foreach ( $levels as $level ) {
				$level_id = $level->id;
				if ( $order->level_id == $level_id ) {
					$order_level_key = $j;
					break;
				}
				$j ++;
			}

			$price = $order->price;

			if ( $crowdfunding && 0 == $price ) {
				$mdid_order = mdid_by_orderid( $order->id );

				$meta = ID_Member_Order::get_order_meta( $order->id, 'gateway_info', true );

				$time_zone = new DateTimeZone( $default_timezone );
				$datetime  = new DateTime( $order->order_date );
				$datetime->setTimezone( $time_zone );
				$transation_id = 'credit' == $order->transaction_id ? 'Refunded Credits' : $order->transaction_id;
				$price         = 0 == $order->price ? ID_Member_Order::get_order_meta( $order->id, 'pwyw_price', true ) : $order->price;

				$buttons  = '<div>';
				$buttons .= '<a href="?idc_orders=1&amp;view_receipt=' . $order->id . '" title="View Receipt" class="btn btn-sm btn-primary px-2 py-1 mb-1 rounded-circle"><i class="fa fa-file-text-o"></i></a>';
				$buttons .= '<button class="payment-history btn btn-sm btn-primary px-2 py-1 ml-1 mb-1 rounded-circle" title="Expand" type="button" data-toggle="collapse" data-target="#nano_collapse_' . $order->id . '" aria-expanded="false" aria-controls="' . $transation_id . $order->id . '">';
				$buttons .= '<i class="fa fa-plus"></i></button>';
				$buttons .= '</div>';

				if ( ! empty( $mdid_order ) ) {
					// These represent actual credit card purchases to the nanosteam account. We will never see these unless we change the structure.

					if ( ! empty( $meta ) ) {
						if ( 'credit' == $meta['gateway'] ) {

							$pay_id    = $mdid_order->pay_info_id;
							$id_order  = new ID_Order( $pay_id );
							$the_order = $id_order->get_order();

							if ( ! empty( $the_order ) ) {

								$price = $the_order->prod_price;

							}
						}
					}
				}

				$content .= '<tr>';
				$content .= '<td class="d-none d-sm-table-cell" style="width: 80px !important;">' . date( get_option( 'date_format' ), strtotime( $datetime->format( 'Y-m-d H:i:s' ) ) ) . '</td>';
				$content .= '<td class="title-name">';
				$content .= '<strong>' . ( isset( $levels[ $order_level_key ] ) ? $levels[ $order_level_key ]->level_name : '' ) . '</strong><br class="d-sm-none">';
				$content .= '<span class="d-sm-none">' . date( get_option( 'date_format' ), strtotime( $datetime->format( 'Y-m-d H:i:s' ) ) ) . ' - $' . $price . '</span>';
				$content .= '<div class="collapse small text-muted" id="nano_collapse_' . $order->id . '">';
				$content .= '<strong>Transaction ID:</strong><br class="d-sm-none"> ' . $transation_id . '<br>';
				$content .= '<strong>Order ID:</strong> 100' . $order->id;
				$content .= '</div></td>';
				$content .= '<td class="text-right d-none d-sm-table-cell">$' . number_format( $price, 2 ) . '</td>';
				$content .= '<td class="text-right" style="width: 80px !important;">' . $buttons . '</td>';
				$content .= '</tr>';
			}

			$i ++;
		}
	} else {
		$content .= '';
	}

	return $content;

}



/**
 * Retrieve and list any refunded tokens for the specific backer
 *
 * @return string
 */

function nano_token_refund_list() {

	$content = '';

	if ( isset( $_GET['edit-profile'] ) || isset( $_GET['idc_orders'] ) ) {

		$current_user = wp_get_current_user();
		$user_id      = $current_user->ID;

		global $wpdb;
		$charset_collate = $wpdb->get_charset_collate();
		$table_name      = $wpdb->prefix . 'nano_token_meta_data';

		if ( $wpdb->get_var( "SHOW TABLES LIKE '$table_name'" ) == $table_name ) {

			$refunded_tokens = $wpdb->get_results( "SELECT * FROM $table_name WHERE user_id = $user_id" );

			$content = '';

			if ( isset( $refunded_tokens[0] ) ) {

				$total_refunded = 0;
				$total_unused   = 0;
				foreach ( $refunded_tokens as $refunded_token ) {
					if ( isset( $refunded_token->tokens_unused ) && 0 != $refunded_token->tokens_unused && 'expired' != $refunded_token->has_expired ) {
						$refunded_amount = $refunded_token->token_amount;
						$total_refunded += $refunded_amount;
						$refund_unused   = $refunded_token->tokens_unused;
						$total_unused   += $refund_unused;
					}
				}

				// Get the user's tokens.
				$member     = new ID_Member( $user_id );
				$md_credits = $member->get_user_credits();

				$difference         = $total_refunded - $md_credits;
				$sub_total_refunded = 0;
				$count              = 0;

				foreach ( $refunded_tokens as $key => $refunded_token ) {

					if ( isset( $refunded_token->tokens_unused ) && 0 != $refunded_token->tokens_unused && 'expired' != $refunded_token->has_expired ) {

						$refunded_amount   = $refunded_token->token_amount;
						$refund_unused     = $refunded_token->tokens_unused;
						$post_id           = $refunded_token->post_id;
						$project_id        = isset( $refunded_token->idk_project_id ) ? $refunded_token->idk_project_id : $refunded_token->project_id;
						$nano_project_id   = isset( $refunded_token->nano_project_uid ) ? $refunded_token->nano_project_uid : '';
						$stripe_charge_id  = isset( $refunded_token->stripe_txn_id ) ? $refunded_token->stripe_txn_id : '';
						$order_id          = $refunded_token->order_id;
						$project_name      = get_the_title( $post_id );
						$project_url       = get_post_permalink( $refunded_token->post_id );
						$token_refund_date = $refunded_token->token_refund_date;
						$token_expiry_date = $refunded_token->token_expiry_date;
						$available         = $refunded_amount;
						$token_used_date   = nano_get_formated_date_time_gmt( 'm/d/Y H:i:s' );

						if ( $difference > 0 ) {
							if ( $difference >= $refunded_amount ) {
								$available  = 0;
								$difference = $difference - $refunded_amount;
							} elseif ( $difference < $refunded_amount ) {
								$available  = $refunded_amount - $difference;
								$difference = 0;
							}
						}

						// Date check and countdown to Expiry
						$date                    = strtotime( $token_expiry_date . '11:59 PM' );
						$remaining               = $date - time();
						$days_remaining          = floor( $remaining / 86400 ) == 1 ? floor( $remaining / 86400 ) . ' day' : floor( $remaining / 86400 ) . ' days';
						$hours_remaining         = floor( ( $remaining % 86400 ) / 3600 ) == 1 ? floor( ( $remaining % 86400 ) / 3600 ) . ' hour' : floor( ( $remaining % 86400 ) / 3600 ) . ' hours';
						$hours_minutes_remaining = floor( ( $remaining % 86400 ) / 3600 ) == 0 && floor( $remaining / 86400 ) == 0 ? ' / ' . floor( ( $remaining / 60 ) ) . ' minutes' : '';
						$token_total             = apply_filters( 'idc_credits_label', __( 'Credits', 'memberdeck' ), true, $refunded_amount ) . ' ' . number_format( $available, 2 ) . ' / ' . number_format( $refunded_amount, 2 );
						$until_expiry            = $days_remaining . ' / ' . $hours_remaining . $hours_minutes_remaining;

						if ( 0 != $available ) {
							$sub_total_refunded += $refunded_amount;
							$content            .= '<tr>';
							$content            .= '<td class="d-none d-sm-table-cell" style="width: 80px !important;">' . $token_refund_date . '</td>';
							$content            .= '<td class="title-name"><strong><a href="' . $project_url . '">' . $project_name . '</a></strong>';
							$content            .= '<div id="nano-' . $post_id . $order_id . '" class="collapse small text-muted">';
							$content            .= '<span class="d-sm-none"><strong>Date Refunded: </strong>' . $token_refund_date . '</span><br class="d-sm-none">';
							$content            .= '<span class="d-sm-none"><strong>Credits Avail / Total: </strong>' . $token_total . '</span><br class="d-sm-none">';
							$content            .= '<strong>Days Remaining: </strong>' . $until_expiry;
							$content            .= '</div>';
							$content            .= '</td>';
							$content            .= '<td class="d-none d-sm-table-cell text-right">' . $token_total . '</td>';
							$content            .= '<td class="text-right" style="width: 80px !important;"><button class="payment-history btn btn-sm btn-primary px-2 py-1 rounded-circle" title="Expand" type="button" data-toggle="collapse" data-target="#nano-' . $post_id . $order_id . '" aria-expanded="false" aria-controls="nano-' . $post_id . $order_id . '"><i class="fa fa-plus"></i></button></td>';
							$content            .= '</tr>';
							$count ++;
						}
					}
				}

				if ( $count > 0 ) {

					$content .= '<tr>';
					$content .= '<td class="d-none d-sm-table-cell">' . null . '</td>';
					$content .= '<td>' . null . '</td>';
					$content .= '<td class="text-right"><strong>Total Unused</strong><span class="d-sm-none">: ' . $md_credits . '</span></td>';
					$content .= '<td class="d-none d-sm-table-cell">' . $md_credits . '</td>';
					$content .= '</tr>';

				}
			} else {

				$content .= '';
			}
		}
	}

	return $content;
}



/**
 * Retrieve and list any refunded tokens for all Backers
 *
 * @return string
 */

function nano_token_refund_list_all() {

	$content = '';

	if ( isset( $_GET['edit-profile'] ) || isset( $_GET['idc_orders'] ) ) {

		$current_user = wp_get_current_user();
		$user_id      = $current_user->ID;

		global $wpdb;
		$charset_collate = $wpdb->get_charset_collate();
		$table_name      = $wpdb->prefix . 'nano_token_meta_data';

		if ( $wpdb->get_var( "SHOW TABLES LIKE '$table_name'" ) == $table_name ) {

			$refunded_tokens = $wpdb->get_results( "SELECT * FROM $table_name" );

			$content = '';

			if ( isset( $refunded_tokens[0] ) ) {

				// Getting users
				$user_id_array = array();
				foreach ( $refunded_tokens as $refunded_token ) {
					$user_id_array[] = $refunded_token->user_id;
				}
				$user_id_array = array_unique($user_id_array);

				foreach ( $user_id_array as $user_id ) {

					$total_refunded = 0;
					$total_unused   = 0;
					foreach ( $refunded_tokens as $refunded_token ) {
						if ( isset( $refunded_token->tokens_unused ) && 0 != $refunded_token->tokens_unused && 'expired' != $refunded_token->has_expired && $refunded_token->user_id == $user_id ) {
							$refunded_amount = $refunded_token->token_amount;
							$total_refunded += $refunded_amount;
							$refund_unused   = $refunded_token->tokens_unused;
							$total_unused   += $refund_unused;
						}
					}

					// Get the user's tokens.
					$member     = new ID_Member( $user_id );
					$md_credits = $member->get_user_credits();

					$difference         = $total_refunded - $md_credits;
					$sub_total_refunded = 0;
					$count              = 0;

					if ( $md_credits > 0 ) {
						$content .= '<tr><td colspan="4"><h6>' . get_user_by( 'id', $user_id )->user_login . ' / User ID: '. $user_id .'</h6></td></tr>';
					}

					$token_count_check = 0;

					foreach ( $refunded_tokens as $key => $refunded_token ) {

						if ( isset( $refunded_token->tokens_unused ) && 0 != $refunded_token->tokens_unused && 'expired' != $refunded_token->has_expired && $refunded_token->user_id == $user_id ) {

							$user_id           = $refunded_token->user_id;
							$refunded_amount   = $refunded_token->token_amount;
							$refund_unused     = $refunded_token->tokens_unused;
							$post_id           = $refunded_token->post_id;
							$project_id        = isset( $refunded_token->idk_project_id ) ? $refunded_token->idk_project_id : $refunded_token->project_id;
							$nano_project_id   = isset( $refunded_token->nano_project_uid ) ? $refunded_token->nano_project_uid : '';
							$stripe_charge_id  = isset( $refunded_token->stripe_txn_id ) ? $refunded_token->stripe_txn_id : '';
							$order_id          = $refunded_token->order_id;
							$project_name      = get_the_title( $post_id );
							$project_url       = get_post_permalink( $refunded_token->post_id );
							$token_refund_date = $refunded_token->token_refund_date;
							$token_expiry_date = $refunded_token->token_expiry_date;
							$available         = $refunded_amount;
							$token_used_date   = nano_get_formated_date_time_gmt( 'm/d/Y H:i:s' );

							if ( $difference > 0 ) {
								if ( $difference >= $refunded_amount ) {
									$available  = 0;
									$difference = $difference - $refunded_amount;
								} elseif ( $difference < $refunded_amount ) {
									$available  = $refunded_amount - $difference;
									$difference = 0;
								}
							}

							// Date check and countdown to Expiry
							$date                    = strtotime( $token_expiry_date . '11:59 PM' );
							$remaining               = $date - time();
							$days_remaining          = floor( $remaining / 86400 ) == 1 ? floor( $remaining / 86400 ) . ' day' : floor( $remaining / 86400 ) . ' days';
							$hours_remaining         = floor( ( $remaining % 86400 ) / 3600 ) == 1 ? floor( ( $remaining % 86400 ) / 3600 ) . ' hour' : floor( ( $remaining % 86400 ) / 3600 ) . ' hours';
							$hours_minutes_remaining = floor( ( $remaining % 86400 ) / 3600 ) == 0 && floor( $remaining / 86400 ) == 0 ? ' / ' . floor( ( $remaining / 60 ) ) . ' minutes' : '';
							$token_total             = number_format( $available, 2 ) . ' / ' . number_format( $refunded_amount, 2 );
							$until_expiry            = $days_remaining . ' / ' . $hours_remaining . $hours_minutes_remaining;

							if ( 0 != $available ) {

								$token_count_check  += $available;
								$sub_total_refunded += $refunded_amount;
								$content            .= '<tr>';
								$content            .= '<td class="d-none d-sm-table-cell" style="width: 80px !important;">' . $token_refund_date . '</td>';
								$content            .= '<td class="title-name"><strong><a href="' . $project_url . '">' . $project_name . '</a></strong>';
								$content            .= '<div id="nano-' . $post_id . $order_id . '" class="collapse small text-muted">';
								$content            .= '<span class="d-sm-none"><strong>Date Refunded: </strong>' . $token_refund_date . '</span><br class="d-sm-none">';
								$content            .= '<span class="d-sm-none"><strong>Credits Avail / Total: </strong>' . $token_total . '</span><br class="d-sm-none">';
								$content            .= '<strong>Days Remaining: </strong>' . $until_expiry;
								$content            .= '</div>';
								$content            .= '</td>';
								$content            .= '<td class="d-none d-sm-table-cell text-right">' . $token_total . '</td>';
								$content            .= '<td class="text-right" style="width: 80px !important;"><button class="payment-history btn btn-sm btn-primary px-2 py-1 rounded-circle" title="Expand" type="button" data-toggle="collapse" data-target="#nano-' . $post_id . $order_id . '" aria-expanded="false" aria-controls="nano-' . $post_id . $order_id . '"><i class="fa fa-plus"></i></button></td>';
								$content            .= '</tr>';
								$count ++;
							}
						}
					}

					if ( $count > 0 ) {

						$content .= '<tr>';
						$content .= '<td colspan="3" class="text-right"><strong class="ml-3">Total Unused</strong><span class="d-sm-none">: ' . $md_credits . '</span></td>';
						$content .= '<td class="d-none d-sm-table-cell">'. $md_credits . '</td>';
						$content .= '</tr>';

					}

				}
			} else {

				$content .= '';
			}
		}
	}

	return $content;
}


/**
 * Tokens refund to backer when fundraising is unsuccessful
 * Tokens refund has a time limit of 30 days from refund date
 */

function nano_tokens_refund() {

	//
	// Set to true for resetting the user token system.
	// WARNING This will remove all tokens from the user's account, only use in the DEV ENV.
	//

	$date      = nano_get_formated_date_time_gmt( 'm/d/Y' );
	$days_back = '-90 days';
	$month     = date( 'm', strtotime( $days_back, strtotime( $date ) ) );
	$day       = date( 'd', strtotime( $days_back, strtotime( $date ) ) );
	$year      = date( 'Y', strtotime( $days_back, strtotime( $date ) ) );

	$args = array(
		'post_type'      => 'ignition_product',
		'post_status'    => 'publish',
		'posts_per_page' => - 1,
		'date_query'     => array(
			array(
				'after'     => array(
					'year'  => $year,
					'month' => $month,
					'day'   => $day,
				),
				'inclusive' => true,
			),
		),

	);

	$query = new WP_Query( $args );

	if ( $query->have_posts() ) {

		while ( $query->have_posts() ) {
			$query->the_post();

			$post_id = get_the_id();

			$project_id      = get_post_meta( $post_id, 'ign_project_id', true );
			$nano_project_id = get_post_meta( $post_id, 'project_uid', true );
			$is_closed       = get_post_meta( $post_id, 'ign_project_closed', true ) == '1' ? true : false;
			$is_successful   = get_post_meta( $post_id, 'ign_project_success', true ) == '1' ? true : false;
			$is_refunded     = get_post_meta( $post_id, 'tokens_refunded', true ) == '1' ? true : false;

			if ( ( true == $is_closed && false == $is_successful && false == $is_refunded ) ) {

				// Project Backer Details

				$level_id       = nano_get_level_id( get_post_meta( $post_id, 'ign_project_id', true ) );
				$project_orders = ID_Member_Order::get_orders_by_level( $level_id );

				foreach ( $project_orders as $key => $order ) {

					$user                = get_user_by( 'id', $order->user_id );
					$datetime            = nano_get_formated_date_time_gmt( 'Y-m-d H:i:sa' );
					$user_email          = $user->user_email;
					$user_id             = $user->ID;
					$token_amount        = ID_Member_Order::get_order_meta( $order->id, 'pwyw_price' );
					$token_refund_date   = nano_get_formated_date_time_gmt( 'm/d/Y' );
					$token_expire_option = get_option( 'nano_token_expiry', '30' ) != '' ? get_option( 'nano_token_expiry', '30' ) : '30';
					$token_expiry_date   = date( 'm/d/Y', strtotime( $token_expire_option . ' days', strtotime( $token_refund_date ) ) );
					$payment_method      = strpos( $order->transaction_id, 'nano' ) !== false ? 'cash' : 'no cash';
					$stripe_fee          = ID_Member_Order::get_order_meta( $order->id, 'stripe_fee', true );
					$stripe_fee          = '' !== $stripe_fee && NULL !== $stripe_fee ? $stripe_fee : 0;

					global $wpdb;
					$table_name = $wpdb->prefix . 'nano_token_meta_data';

					if ( 'cash' == $payment_method ) {
						$stripe_charge_id = explode( 'nano-', $order->transaction_id );
						if ( is_array( $stripe_charge_id ) ) {
							$stripe_charge_id = $stripe_charge_id[1];
						}

					} elseif ( 'no cash' == $payment_method ) {

						// Check to see the if the project was unsuccessful
						$order_id = $order->id;

						// If transaction is token based rather than stripe based
						$sql           = "SELECT * FROM $table_name WHERE order_id LIKE %s";
						$sql_prepare   = $wpdb->prepare( $sql, $order_id );
						$token_refunds = $wpdb->get_results( $sql_prepare );
						$token_refund  = isset( $token_refunds[0] ) ? $token_refunds[0] : null;

						$message = isset( $token_refund ) ? $token_refund->stripe_txn_id : 'Previously refunded credits';

						// Adjustments to this
						$parent_stripe_charge_id = idc_get_order_meta( $order_id, 'parent_stripe_txn_id', true );

						$stripe_charge_id = '' !== $parent_stripe_charge_id || null !== $parent_stripe_charge_id ? $parent_stripe_charge_id : $message;

					}

					$token_details = array(
						'time'                 => $datetime,
						'user_id'              => $user_id,
						'post_id'              => $post_id,
						'idk_project_id'       => $project_id,
						'nano_project_uid'     => $nano_project_id,
						'stripe_txn_id'        => $stripe_charge_id,
						'order_id'             => $order->id,
						'token_amount'         => $token_amount,
						'stripe_fee'           => $stripe_fee,
						'token_refund_date'    => $token_refund_date,
						'token_expiry_date'    => $token_expiry_date,
						'token_used_date'      => false,
						'nano_new_project_uid' => false,
						'has_expired'          => false,
						'tokens_unused'        => $token_amount,
					);

					// Create the table if it doesn't exist
					$charset_collate = $wpdb->get_charset_collate();

					$sql = "CREATE TABLE $table_name (
                            id smallint(9) NOT NULL AUTO_INCREMENT,
                            time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
                            user_id varchar(255) NOT NULL,
                            post_id bigint(20) NOT NULL,
                            idk_project_id varchar(255) NOT NULL,
                            nano_project_uid varchar(255) NOT NULL,
                            stripe_txn_id varchar(255) NOT NULL,
                            order_id varchar(255) NOT NULL,
                            token_amount varchar(255) NOT NULL,
                            stripe_fee varchar(255) NOT NULL,
                            token_refund_date varchar(255) NOT NULL,
                            token_expiry_date varchar(255) NOT NULL,
                            token_used_date varchar(255) NOT NULL,
                            nano_new_project_uid varchar(255) NOT NULL,
                            has_expired varchar(255) NOT NULL,
                            tokens_unused varchar(255) NOT NULL,
                            UNIQUE KEY id (id) ) $charset_collate;";

					require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

					maybe_create_table( $table_name, $sql );

					// We've added a new column, lets add it.
					$row = $wpdb->get_results( "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '$table_name' AND column_name = 'stripe_fee'" );
					if ( empty( $row ) ) {
						$result = $wpdb->query( "ALTER TABLE $table_name ADD stripe_fee varchar(255) NOT NULL" );
					}

					nano_debug_query( $result, $sql, 'nano_tokens_refund' );

					if ( isset( $user_id ) ) {
						send_error('nano_tokens_refund fired');
						// Add tokens to the Backer's account.
						//ID_Member::add_credits($user_id, $token_amount);
						nano_add_credits( $user_id, $token_amount );
						// Add that this project has been refunded by adding a meta key
						add_post_meta( $post_id, 'tokens_refunded', true, true );
						// Create the nano_token_meta_data table and add the user's token data.
						$insert = $wpdb->insert( $table_name, $token_details );
						// Debug if necessary
						nano_debug_query( $insert, $sql, 'nano_tokens_refund (insert)' );
						//Email the user that they have tokens refunded.
						nano_email_notify( $post_id, $user_email, 'nanosteam_backer_refund' );
					}
				}
			}
		}
		/* Restore original Post Data */
		wp_reset_postdata();
	}
}


/* ------------------------
 * Tokens refund to backer when fundraising is unsuccessful
 * Tokens refund has a time limit of 30 days from refund date
 *
------------------------------*/


function nano_tokens_refund_reminder() {
	$users = get_users( array( 'fields' => array( 'ID' ) ) );

	global $wpdb;
	$charset_collate = $wpdb->get_charset_collate();
	$table_name      = $wpdb->prefix . 'nano_token_meta_data';

	if ( $wpdb->get_var( "SHOW TABLES LIKE '$table_name'" ) == $table_name ) {

		foreach ( $users as $user_id ) {

			$token_refunds = $wpdb->get_results( "SELECT * FROM $table_name WHERE user_id = $user_id->ID" );

			if ( count( $token_refunds ) > 1 ) {

				$refunded_order_ids  = array();
				$email_reminder_sent = maybe_unserialize( get_user_meta( $user_id->ID, 'token_refuned_reminder_email' ) );
				if ( ! is_array( $email_reminder_sent ) ) {
					$email_reminder_sent = array();
				}

				foreach ( $token_refunds as $token_refund ) {

					$order_id = $token_refund->order_id;
					$has_expried = $token_refund->has_expired;

					if ( $token_refund->tokens_unused > 0 && 'expired' !== $has_expried ) {

						if ( ( is_array( $email_reminder_sent ) && ! in_array( $order_id, $email_reminder_sent ) ) || false == $email_reminder_sent ) {

							$today        = time();
							$token_expiry = strtotime( $token_refund->token_expiry_date );
							$date         = nano_get_formated_date_time_gmt( 'm/d/Y' );
							$days         = '-5 days';
							$days_back    = strtotime( date( 'm/d/Y', strtotime( $days, strtotime( $date ) ) ) );

							if ( $days_back < $token_expiry && $today > $token_expiry ) {

								$post_id              = $token_refund->post_id;
								$user                 = get_user_by( 'id', $user_id->ID );
								$user_email           = $user->data->user_email;
								$refunded_order_ids[] = $order_id;

								nano_email_notify( $post_id, $user_email, 'nanosteam_backer_refund_reminder' );
								send_error( 'refund reminder email fired' );

							}
						}
					}
				}

				$merged_reminders_emails = serialize( array_unique( array_merge( $email_reminder_sent, $refunded_order_ids ) ) );
				update_user_meta( $user_id->ID, 'token_refuned_reminder_email', $merged_reminders_emails );

			}
		}
	}
}

/* ------------------------
 * Tokens refund to backer when they cancel a pledge
 * Tokens refund has a time limit of 30 days from refund date
 * This is not used with Nanotsteam but is a feature built into the native IDK system
 *
------------------------------*/

function nano_token_refund_cancelled_pledge() {
	// The call for cancelling the order/pledge
	// Restoring the credits, getting level details for the credits
	$level = ID_Member_Level::get_level( $_GET['level'] );
	if ( ! empty( $level ) ) {
		$order = new ID_Member_Order( $_GET['cancel_pledge'] );

		$check = ID_Member_Order::get_order_meta( $_GET['cancel_pledge'], 'pwyw_price', true );

		$project_id  = $_GET['level'] + 1;
		$project     = new ID_Project( $project_id );
		$the_project = $project->the_project();
		$post_id     = $project->get_project_postid();

		if ( ! empty( $order ) ) {
			// Getting order details
			$the_order = $order->get_order();
			if ( ! empty( $the_order ) ) {
				$user_id = $the_order->user_id;

				$credits = ID_Member_Order::get_order_meta( $_GET['cancel_pledge'], 'pwyw_price', true );

				if ( '' != $credits ) {

					$token_refund_date = nano_get_formated_date_time_gmt( 'm/d/Y' );
					$token_expiry_date = date( 'm/d/Y', strtotime( '30 days', strtotime( $token_refund_date ) ) );

					$project_id      = $_GET['level'] + 1;
					$nano_project_id = get_post_meta( $post_id, 'project_uid', true );
					$project         = new ID_Project( $project_id );
					$the_project     = $project->the_project();
					$post_id         = $project->get_project_postid();

					$payment_method = strpos( $order->transaction_id, 'nano' ) !== false ? 'cash' : 'no cash';
					if ( 'cash' == $payment_method ) {
						$stripe_charge_id = explode( 'nano-', $order->transaction_id );
						if ( is_array( $stripe_charge_id ) ) {
							$stripe_charge_id = $stripe_charge_id[1];
						}
					} elseif ( 'no cash' == $payment_method ) {
						$stripe_charge_id = $order->transaction_id;
					}

					$token_details = array(
						'user_id'           => $user_id,
						'post_id'           => $post_id,
						'idk_project_id'    => $project_id,
						'nano_project_uid'  => $nano_project_id,
						'stripe_txn_id'     => $stripe_charge_id,
						'order_id'          => $order->id,
						'token_amount'      => $credits,
						'token_refund_date' => $token_refund_date,
						'token_expiry_date' => $token_expiry_date,
						'has_expired'       => false,
						'tokens_unused'     => $credits,
					);

					// Now removing the order and applying tokens to user's account

					$order->delete_order();
					ID_Order::delete_order( $_GET['pay_id'] );
					mdid_remove_order( $_GET['cancel_pledge'] );
					// Adding those nano Tokens to Backer's account
					//ID_Member::add_credits($user_id, $credits);
					nano_add_credits( $user_id, $credits );
					// Applying the token meta to the user for expiry ext.
					add_user_meta( $user_id, '_token_refund', $token_details );
				}
			}
		}
	}

}


/**
 * Getting the post_id of the project by its unique project uid.
 *
 * @param $project_uid
 *
 * @return mixed
 */

function get_post_id_by_uid( $project_uid ) {

	global $wpdb;
	// this adds the prefix which is set by the user upon instillation of WordPress
	$table_name = $wpdb->prefix . 'nano_meta_data';
	$project    = $wpdb->get_results(
		$wpdb->prepare( "
            SELECT * FROM $table_name 
            WHERE project_uid = %s",
			$project_uid
		)
	);

	if ( isset( $project[0] ) ) {

		return $project[0]->post_id;

	}

}

/**
 * Process payout to nanosteam of the Fee from the project
 *
 * @param $nanosteam_fee
 * @param $stripe_fee
 * @param $project_id
 * @param $creator_id
 * @param $project_fund_raised
 * @param $project_name
 * @param $project_url
 * @param $admin_id
 *
 * @return string
 */
function nanosteam_payout( $nanosteam_fee, $stripe_fee, $project_id, $creator_id, $project_fund_raised, $project_name, $project_url, $admin_id ) {

	$settings = get_option( 'memberdeck_gateways' );

	if ( ! empty( $settings ) ) {
		if ( is_array( $settings ) ) {
			$test = $settings['test'];
			if ( 1 == $test ) {
				$key = $settings['tsk'];
			} else {
				$key = $settings['sk'];
			}
		}
	}

	if ( ! class_exists( 'Stripe\Stripe' ) ) {
		require_once IDC_PATH . 'lib/stripe-php-4.2.0/init.php';
	}

	try {

		// Set the API Version & Key
		\Stripe\Stripe::setApiVersion( NANO_STRIPE_API_VERSION );
		\Stripe\Stripe::setApiKey( $key );

		// Transfer the fee amount to nanosteam
		// TODO https://stripe.com/docs/api#create_payout
		$fee_transfer = \Stripe\Payout::create( array(
			'amount'   => ( $nanosteam_fee * 100 ), // * 100 for converting to stripe system without decimals
			'currency' => 'usd',
			'metadata' => array(
				'project_id'                     => $project_id,
				'nano_creator_id'                => $creator_id,
				'total_funded'                   => $project_fund_raised,
				'nanosteam_fee'                  => $nanosteam_fee,
				'stripe_fee'                     => $stripe_fee,
				'project_name'                   => $project_name,
				'project_url'                    => $project_url,
				'user_id_who_initiated_transfer' => $admin_id,
			),
		) );

		send_error( '$fee transer' );
		send_error( $fee_transfer );

		$error = ' And a fee of $' . $nanosteam_fee . '  has been paid to the nanosteam account successfully. Please review your payout history in your stripe admin dashboard.';

	} catch ( \Stripe\Error\Card $e ) {
		// Since it's a decline, \Stripe\Error\Card will be caught
		stripe_error_email_admin_notification( $e, 'nanosteam_payout' );
	} catch ( \Stripe\Error\RateLimit $e ) {
		// Too many requests made to the API too quickly
		$body  = $e->getJsonBody();
		$err   = $body['error'];
		$error = 'Status ' . $e->getHttpStatus() . '. ' . $err['message'];
	} catch ( \Stripe\Error\InvalidRequest $e ) {
		// Invalid parameters were supplied to Stripe's API
		$body  = $e->getJsonBody();
		$err   = $body['error'];
		$error = 'Status ' . $e->getHttpStatus() . '. ' . $err['message'];
	} catch ( \Stripe\Error\Authentication $e ) {
		// Authentication with Stripe's API failed
		// (maybe you changed API keys recently)
		$body  = $e->getJsonBody();
		$err   = $body['error'];
		$error = 'Status ' . $e->getHttpStatus() . '. ' . $err['message'];
	} catch ( \Stripe\Error\ApiConnection $e ) {
		// Network communication with Stripe failed
		$body  = $e->getJsonBody();
		$err   = $body['error'];
		$error = 'Status ' . $e->getHttpStatus() . '. ' . $err['message'];
	} catch ( \Stripe\Error\Base $e ) {
		// Display a very generic error to the user, and maybe send
		// yourself an email
		$body  = $e->getJsonBody();
		$err   = $body['error'];
		$error = 'Status ' . $e->getHttpStatus() . '. ' . $err['message'];
	} catch ( Exception $e ) {
		// Something else happened, completely unrelated to Stripe
		$error = 'Exception ' . $e;
	}

	return $error;

}


/**
 * Calculate the fee from the total raised and calculate the
 *
 * @param $total
 * @param $post_id
 *
 * @return mixed|null
 */

function nano_fee( $total, $post_id ) {

	$sc_settings          = get_option( 'md_sc_settings' );
	$funding_goal         = get_post_meta( $post_id, 'ign_fund_goal', true );
	$matching_grant_check = get_post_meta( $post_id, 'fifty_fifty_grant', true );
	$val                  = null;

	if ( ! empty( $sc_settings ) ) {

		$fee = null;

		if ( ! is_array( $sc_settings ) ) {
			$sc_settings = unserialize( $sc_settings );
		}
		if ( 'approved' == $matching_grant_check ) {
			$total = $total - ( $funding_goal / 2 );
		}
		if ( is_array( $sc_settings ) ) {
			$app_fee  = apply_filters( 'idc_app_fee', $sc_settings['app_fee'], '' );
			$fee_type = apply_filters( 'idc_fee_type', $sc_settings['fee_type'], '' );
			$fee      = apply_filters( 'nano_idc_fee_amount', $app_fee, $total, $fee_type, 'stripe' );
		}

		$val = $fee;

	}

	return $val;

}

/**
 * Calculate the fee from the the single order
 *
 * @param $total
 * @param $post_id
 *
 * @return mixed|null
 */

function nano_single_order_fee( $total, $transaction_id ) {

	$sc_settings = get_option( 'md_sc_settings' );
	$val         = null;

	if ( ! empty( $sc_settings ) ) {

		$fee = null;

		if ( ! is_array( $sc_settings ) ) {
			$sc_settings = unserialize( $sc_settings );
		}
		if ( is_array( $sc_settings ) ) {
			$app_fee  = apply_filters( 'idc_app_fee', $sc_settings['app_fee'], '' );
			$fee_type = apply_filters( 'idc_fee_type', $sc_settings['fee_type'], '' );
			$fee      = apply_filters( 'nano_idc_fee_amount', $app_fee, $total, $fee_type, 'stripe' );

		}

		$val = $fee;

		// If this is a matching grant order we remove any nanosteam fees asociated with the order.
		if ( strpos( strtoupper( $transaction_id ), 'NANOSTEAM' ) !== false ) {
			$val = 0;
		}

	}

	return $val;

}


/**
 * Adding a tallied amout of all the stripe fees from refunded credits to a credit order.
 *
 * @param $order
 * @param $current_user
 * @param $response
 *
 * @return float|int
 */
function nano_add_stripe_fee_to_credit_order( $order, $current_user, $response ) {

	$order_amt  = idc_get_order_meta( $order->id, 'pwyw_price', true );
	$stripe_amt = ( idc_get_order_meta( $order->id, 'stripe_fee', true ) / 100 );

	global $wpdb;
	$charset_collate = $wpdb->get_charset_collate();
	$table_name      = $wpdb->prefix . 'nano_token_meta_data';

	$refunded_credits = $wpdb->get_results( "SELECT * FROM $table_name WHERE user_id = $current_user->ID AND has_expired = ''" );

	if ( is_array( $refunded_credits ) && 0 < count( $refunded_credits ) ) {

		$stripe_amt_plus = 0;
		$orders_combined = 0;

		foreach ( $refunded_credits as $c_order ) {

			$refunded_amount   = $c_order->token_amount;
			$refunded_left     = $c_order->tokens_unused;
			$stripe_fee        = '' !== $c_order->stripe_fee && 0 != $c_order->stripe_fee ? $c_order->stripe_fee : ( 10 > $c_order->token_amount ? 82 : 50 );
			$stripe_percentage = ( $stripe_fee / 1000 );
			$c_order_datetime  = new DateTime( $c_order->token_used_date );
			$order_date_time   = new DateTime( $order->order_date );
			$diff              = abs( $c_order_datetime->getTimestamp() - $order_date_time->getTimestamp() );

			$project_uids           = maybe_unserialize( $c_order->nano_new_project_uid );
			$project_order_id_check = '';

			//view_var('c_order: ' . $c_order->order_id);

			if ( is_array( $project_uids ) ) {

				//view_var('02');

				$stripe_sub = 0;

				foreach ( $project_uids as $project_uid ) {

					if ( ( $pos = strpos( $project_uid, '/' ) ) !== false ) {
						$project_order_id_check = substr( $project_uid, $pos + 1 );
					}

					if ( $project_order_id_check === $order->id ) {

						//view_var('03');

						if ( ( $refunded_amount - $refunded_left ) == $order_amt ) {

							//view_var('04');

							$stripe_sub = $stripe_percentage * $order_amt;

						} elseif ( $refunded_left == 0 ) {

							//view_var('05');

							$stripe_amt_plus += $stripe_fee / 1000;
							$orders_combined += $refunded_amount;

							// If we stop here then we do just like in the first if
							$stripe_sub = $stripe_percentage * $order_amt;

							//view_var($stripe_sub);

						} elseif ( ( $refunded_amount - $refunded_left ) + $orders_combined == $order_amt ) {

							//view_var('06');

							$stripe_sub = $stripe_amt_plus + ( $stripe_percentage * $order_amt );

						} else {

							//view_var('07');

							$stripe_sub = $stripe_percentage * $order_amt;

						}

					}

				}

				$stripe_amt_plus = $stripe_sub;

			}

		}

		if ( false === $response ) {
			// Adding the actual stripe fee to this order for processing later.
			$stripe_amt = $stripe_amt_plus * 100; // Getting rid of the decimals for consistency between the other stripe fee additions.
			//ID_Member_Order::update_order_meta( $order->id, 'stripe_fee', $stripe_amt_raw );
		}

	}

	if ( $response === true ) {

		if ( null === idc_get_order_meta( $order->id,'stripe_fee', true ) ) {

			$stripe_amt_plus = empty( $stripe_amt_plus ) ? 0 : $stripe_amt_plus;

			$stripe_amt = $stripe_amt_plus * 100;
			//ID_Member_Order::update_order_meta( $order->id, 'stripe_fee', $stripe_amt_new_raw );

		}

		return $stripe_amt;

	}

}

/**
 * Return the total stripe fees for a project
 *
 * @param $orders
 *
 * @return int|null
 */

function nano_stripe_fee_total( $orders ) {

	$val = null;

	$stripe_fee        = 0;
	$credit_stripe_fee = 0;

	foreach ( $orders as $order ) {

		$stripe_fee += ID_Member_Order::get_order_meta( $order->id, 'stripe_fee', true );

		$base_fee = ID_Member_Order::get_order_meta( $order->id, 'stripe_fee', true );

		if ( 'credit' == $order->transaction_id ) {

			if ( NULL === idc_get_order_meta( $order->id,'stripe_fee', true ) ) {

				$current_user       = get_user_by( 'id', $order->user_id );
				$credit_stripe_fee += nano_add_stripe_fee_to_credit_order( $order, $current_user, true );

			}

			/*

			$user_id          = $order->user_id;
			// Check to see the if the project was unsuccessful
			global $wpdb;
			$table_name = $wpdb->prefix . 'nano_token_meta_data';
			$sql        = "SELECT * FROM $table_name WHERE user_id LIKE %s";
			$sql        = $wpdb->prepare( $sql, $user_id );
			$tokens     = $wpdb->get_results( $sql );

			$order_date = date( 'm/d/Y H:i', strtotime( $order->order_date ) );
			$order_total = ID_Member_Order::get_order_meta( $order->id, 'pwyw_price', true );


			foreach ( $tokens as $token ) {

				$user_order_date = date( 'm/d/Y H:i', strtotime( $token->token_used_date ) );



				if ( $order_date == $user_order_date ) {

					if ( isset( $token->stripe_fee ) ) {

						$token_refund    = $token->token_amount;
						$full_stripe_fee = convert_to_numeric( $token->stripe_fee );

						$percentage = ( $full_stripe_fee / 100 ) / $token_refund;

						$current_user = get_user_by( 'id', $order->user_id );
						$stripe_fee   += nano_add_stripe_fee_to_credit_order( $order, $current_user, true );

						//$stripe_fee += ( $percentage * $order_total ) * 100;

					}
				}
			}
			*/
		}

	}

	$val = ( $stripe_fee + $credit_stripe_fee ) / 100;

	return $val;

}

/**
 * Filter for calculating the fee and returning
 */
function nano_idc_fee_calculation($fee, $price, $fee_type, $gateway) {
	$price = '' !== $price ? $price : 0;

	if ( $gateway == "pp-adaptive" ) {
		if ( $fee_type == 'percentage' ) {
			$fee = round( floatval( $price ) * ( floatval( $fee ) / 100 ), 2 );
		} else {
			// As fee is stored in Cents in Enterprise settings
			$fee = $fee / 100;
		}
	} else if ( $gateway == "stripe" ) {
		if ( $fee_type == 'percentage' ) {
			$fee = $price * ( $fee / 100 );
		}
	} else {
		if ( $fee_type == 'percentage' ) {
			$fee = number_format( $price * ( $fee / 100 ), 2, '.', '' );
		}
	}

	return $fee;
}
add_filter('nano_idc_fee_amount', 'nano_idc_fee_calculation', 10, 4);


/**
 * Generate a itemized listing of all fees associated with a project
 *
 * @param $orders
 * @param $post_id
 *
 * @return array
 */
function nano_fee_list( $orders, $post_id ) {

	//$total_before_fee = '' !== get_post_meta( $post_id, 'ign_fund_raised', true ) ? get_post_meta( $post_id, 'ign_fund_raised', true ) : 0;
	$stripe_fee = nano_stripe_fee_total( $orders );

	$total_before_fee = 0;
	foreach ( $orders as $order ) {
		$total_before_fee += idc_get_order_meta( $order->id, 'pwyw_price', true );
	}

	$nanosteam_fee   = nano_fee( $total_before_fee, $post_id );
	$total_fee       = $nanosteam_fee + $stripe_fee;
	$final_after_fee = $total_before_fee - $total_fee;

	$fees                      = array();
	$fees['nanosteam_fees']    = convert_to_numeric( $nanosteam_fee );
	$fees['stripe_fees']       = convert_to_numeric( $stripe_fee );
	$fees['total_fees']        = convert_to_numeric( $total_fee );
	$fees['total_after_fees']  = convert_to_numeric( $final_after_fee );
	$fees['total_before_fees'] = convert_to_numeric( $total_before_fee );

	return $fees;

}


/**
 * Get the status of whether funds have been transferred to the creator
 *
 * @param $post_id
 *
 * @return bool
 */

function nano_tranfer_status( $post_id ) {

	$first_transfer           = get_post_meta( $post_id, 'first_transfer_transaction_id', true ) != '' ? true : false;
	$final_transfer           = get_post_meta( $post_id, 'final_transfer_transaction_id', true ) != '' ? true : false;
	$standard_transfer        = get_post_meta( $post_id, 'standard_transfer_transaction_id', true ) != '' ? true : false;
	$legacy_transfer          = get_post_meta( $post_id, 'transfer_transaction_id', true ) != '' ? true : false;
	$matching_transfer_status = ( true == $first_transfer && true == $final_transfer ) || ( true == $first_transfer && false == $final_transfer ) ? true : false;
	$transfer_status          = ( true == $matching_transfer_status && true == $standard_transfer ) || true == $legacy_transfer ? true : false;

	return $transfer_status;
}