<?php if ( ! defined( 'ABSPATH' ) ) {
	die( 'Direct access forbidden.' );
}

class IDC_Nano_Fifty_Metaboxes {

	function __construct() {
		add_action( 'add_meta_boxes', array( &$this, 'generate_metabox' ) );
		add_action( 'save_post', array( &$this, 'save_metabox' ) );
		add_action( 'save_post', array( &$this, 'save_page_metabox' ) );
	}

	function generate_metabox( $post ) {
		$post_types = get_post_types();
		$types      = array( 'ignition_product' );

		if ( 'ignition_product' == get_post_type() ) {

			//Payout Rule Check
			$project_success_status = get_post_meta( $_GET['post'], 'ign_project_success', true ) == '1' ? true : false;
			$project_fund_goal      = get_post_meta( $_GET['post'], 'ign_fund_goal', true );
			$project_fund_raised    = get_post_meta( $_GET['post'], 'ign_fund_raised', true );
			$project_fund_status    = absint( $project_fund_raised ) >= absint( $project_fund_goal ) ? true : false;
			$project_published      = isset( $_GET['post'] ) && strtoupper( get_post_status( $_GET['post'] ) ) === 'PUBLISH' ? true : false;

			foreach ( $types as $type ) {
				add_meta_box( 'nano_grant', __( 'Nanosteam Matching Grant', 'nanosteam_8n' ), array(
					&$this,
					'nano_50_metabox',
				), $type, 'side', 'high' );
				add_meta_box( 'nano_project_review_email', __( 'Nanosteam Project Review Emails', 'nanosteam_8n' ), array(
					&$this,
					'nanao_project_review_emails',
				), $type, 'normal', 'high' );
				add_meta_box( 'nano_project_review', __( 'Nanosteam Project Review', 'nanosteam_8n' ), array(
					&$this,
					'nanao_project_review',
				), $type, 'normal', 'high' );
				add_meta_box( 'nano_project_overview', __( 'Nanosteam Additional Project Details', 'nanosteam_8n' ), array(
					&$this,
					'render_metabox',
				), $type, 'normal', 'high' );
				add_meta_box( 'nano_project_id', __( 'Project ID', 'nanosteam_8n' ), array(
					&$this,
					'project_id',
				), $type, 'side', 'high' );
				if ( ( true == $project_success_status || true == $project_fund_status ) && true === $project_published && true === nanosteam_privledge_check() ) {
					add_meta_box( 'nano_project_payout', __( 'Payout This Project', 'nanosteam_8n' ), array(
						&$this,
						'payout_metabox',
					), $type, 'side', 'high' );
				}
			}
		}

		if ( 'page' == get_post_type() && 'ignition_product' != get_post_type() ) {
			$type = 'page';
			// Revolution Slider
			add_meta_box( 'nano_slider_id', __( 'Revolution Slider', 'nanosteam_8n' ), array(
				&$this,
				'nano_revolution_slider_metabox',
			), $type, 'side', 'high' );

			// Mobile Content WP Editor field
			add_meta_box( 'nano_mobile_content_id', __( 'Mobile Design', 'nanosteam_8n' ), array(
				&$this,
				'nano_mobile_metabox',
			), $type, 'normal', 'high' );
		}
	}

	function nano_50_metabox( $post ) {
		wp_nonce_field( 'nano_fifty_meta', 'nano_fifty_metabox' );
		$nano_fifty_review     = get_post_meta( $post->ID, 'fifty_fifty_grant', true );
		$nano_fifty_admin_note = get_post_meta( $post->ID, 'fifty_comments', true );
		include NANO_STRIPE . '/library/project_form/admin/_nanosteam50Metabox.php';
	}

	function nanao_project_review( $post ) {
		wp_nonce_field( 'nano_project_review_meta', 'nano_project_review_metabox' );
		include NANO_STRIPE . '/library/project_form/admin/_nanosteamProjectReviewMetabox.php';
	}

	function nanao_project_review_emails( $post ) {
		wp_nonce_field( 'nano_project_review_meta', 'nano_project_review_metabox' );
		include NANO_STRIPE . '/library/project_form/admin/_nanosteamProjectReviewEmailMetabox.php';
	}

	function render_metabox( $post ) {
		wp_nonce_field( 'nano_project_meta', 'nano_project_metabox' );
		include NANO_STRIPE . '/library/project_form/admin/_nanosteamProjectMetabox.php';
	}

	function payout_metabox( $post ) {

		$alert   = payout_problem_alert( $post->ID );
		$message = isset( $alert['message'] ) ? $alert['message'] : '';

		wp_nonce_field( 'nano_project_payout_meta', 'nano_project_payout_metabox' );
		echo '<strong>This project achieved it\'s fundraising goal. Transfer funds to the creator\'s account by clicking the button below. </strong><p><small><strong>Warning:</strong> If you initiate the transfer before the end of the fundraising the project will be closed to further fundraising.</small></p> ' . $message;
		include NANO_STRIPE . '/library/project_form/admin/_nanosteamPayoutMetabox.php';
	}

	function nano_mobile_metabox( $post ) {
		wp_nonce_field( 'nano_project_mobile_meta', 'nano_mobile_metabox' );
		//echo '<strong>This project achieved it\'s fundraising goal. Transfer funds to the creator\'s account by clicking the button below. </strong><p><small><strong>Warning:</strong> If you initiate the transfer before the end of the fundraising the project will be closed to further fundraising.</small></p> ';
		include NANO_STRIPE . '/library/project_form/admin/_nanosteamMobileMetabox.php';
	}

	function nano_revolution_slider_metabox( $post ) {
		wp_nonce_field( 'nano_revolution_slider_meta', 'nano_revolution_slider_metabox' );
		include NANO_STRIPE . '/library/project_form/admin/_nanosteamRevolutionSlider.php';
	}

	function project_id( $post ) {
		$project_uid = get_post_meta( $post->ID, 'project_uid', true ) != '' ? get_post_meta( $post->ID, 'project_uid', true ) : 'PENDING';
		echo 'This project\'s unique ID is <strong>' . $project_uid . '</strong> ';
	}

	function save_page_metabox( $post_id ) {

		if ( 'page' != get_post_type() ) {
			return;
		}

		if ( isset( $_POST ) ) {
			if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
				if ( ! current_user_can( 'edit_page', array( $post_id ) ) ) {
					return;
				}
			} else {
				if ( ! current_user_can( 'edit_posts' ) ) {
					return;
				}
			}
			if ( isset( $_POST['nano_revolution_slider_metabox'] ) && ! wp_verify_nonce( $_POST['nano_revolution_slider_metabox'], 'nano_revolution_slider_meta' ) ) {
				return;
			}
		}

		// check for autosave
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
			return;
		}

		if ( isset( $_POST['post_ID'] ) ) {

			$post_id = absint( $_POST['post_ID'] );

			$nano_rev_slider          = isset( $_POST['nano_revolution_slider_alias'] ) ? sanitize_text_field( $_POST['nano_revolution_slider_alias'] ) : '';
			$nano_rev_slider_original = get_post_meta( $post_id, 'nano_revolution_slider_alias', true );
			$nano_mobile_content      = isset( $_POST['nano_mobile_content'] ) ? sanitize_post_field( 'post_content', $_POST['nano_mobile_content'], $_POST['post_ID'], 'db' ) : '';

			update_post_meta( $post_id, 'nano_revolution_slider_alias', $nano_rev_slider, $nano_rev_slider_original );
			update_post_meta( $post_id, 'nano_mobile_content', $nano_mobile_content );

		}

	}

	function save_metabox( $post_id ) {

		if ( isset( $_POST ) ) {
			if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
				if ( ! current_user_can( 'edit_page', array( $post_id ) ) ) {
					return;
				}
			} else {
				if ( ! current_user_can( 'edit_posts' ) ) {
					return;
				}
			}
			if ( ! isset( $_POST['nano_fifty_metabox'] ) ) {
				return;
			} else {
				if ( ! wp_verify_nonce( $_POST['nano_project_metabox'], 'nano_project_meta' ) ) {
					return;
				}
			}
			$post_id = absint( $_POST['post_ID'] );
			$post    = $_POST;

			// Project Review
			$nano_project_subject_org  = get_post_meta( $post_id, 'project_review_subject', true ) != '' ? array_values( array_slice( get_post_meta( $post_id, 'project_review_subject' ), - 1 ) )[0] : '';
			$nano_project_review_org   = get_post_meta( $post_id, 'project_review_message', true ) != '' ? array_values( array_slice( get_post_meta( $post_id, 'project_review_message' ), - 1 ) )[0] : '';
			$nano_project_subject      = isset( $_POST['project_review_subject'] ) ? sanitize_text_field( $_POST['project_review_subject'] ) : '';
			$nano_project_review       = isset( $_POST['project_review_message'] ) ? sanitize_text_field( $_POST['project_review_message'] ) : '';
			$nano_project_review_radio = isset( $_POST['project_review_status_radio'] ) ? sanitize_text_field( $_POST['project_review_status_radio'] ) : '';
			$nano_project_review_email = isset( $_POST['project_review_status_email'] ) ? sanitize_text_field( $_POST['project_review_status_email'] ) : '';
			$nano_review_admin_note    = isset( $_POST['project_admin_notes'] ) ? sanitize_text_field( $_POST['project_admin_notes'] ) : '';

			// Grant Review
			$nano_fifty_review     = isset( $_POST['fifty_fifty_grant'] ) ? sanitize_text_field( $_POST['fifty_fifty_grant'] ) : '';
			$nano_fifty_admin_note = isset( $_POST['fifty_comments'] ) ? implode( "\n", array_map( 'sanitize_text_field', explode( "\n", $_POST['fifty_comments'] ) ) ) : '';

			$nano_location_city        = isset( $_POST['nano_location_city'] ) ? sanitize_text_field( $_POST['nano_location_city'] ) : '';
			$nano_location_country     = isset( $_POST['nano_location_country'] ) ? sanitize_text_field( $_POST['nano_location_country'] ) : '';
			$nano_goal_timeline        = isset( $_POST['nano_goal_timeline'] ) ? sanitize_text_field( $_POST['nano_goal_timeline'] ) : '';
			$nano_goal_milestones      = isset( $_POST['nano_goal_milestones'] ) ? $_POST['nano_goal_milestones'] : '';
			$nano_goal_milestones_date = isset( $_POST['nano_goal_milestone_date'] ) ? $_POST['nano_goal_milestone_date'] : '';
			$nano_team_members         = isset( $_POST['nano_team_members'] ) ? $_POST['nano_team_members'] : '';
			$nano_team_leader_bio      = isset( $_POST['nano_team_leader_bio'] ) ? sanitize_text_field( $_POST['nano_team_leader_bio'] ) : '';
			$nano_project_significance = isset( $_POST['nano_project_significance'] ) ? sanitize_text_field( $_POST['nano_project_significance'] ) : '';

			$nano_fifty_grant_status = update_post_meta( $post_id, 'fifty_fifty_grant', $nano_fifty_review );

			if ( $nano_project_subject != $nano_project_subject_org && '' != $nano_project_subject ) {
				add_post_meta( $post_id, 'project_review_subject', $nano_project_subject );
			}
			if ( $nano_project_review != $nano_project_review_org && '' != $nano_project_review ) {
				add_post_meta( $post_id, 'project_review_message', $nano_project_review );
			}
			update_post_meta( $post_id, 'project_review_status_radio', $nano_project_review_radio );
			update_post_meta( $post_id, 'project_review_status_email', $nano_project_review_email );
			update_post_meta( $post_id, 'project_admin_notes', $nano_review_admin_note );
			update_post_meta( $post_id, 'fifty_comments', $nano_fifty_admin_note );
			// update_post_meta( $post_id, 'nano_location_city', $nano_location_city );
			// update_post_meta( $post_id, 'nano_location_country', $nano_location_country );
			// update_post_meta( $post_id, 'nano_goal_timeline', $nano_goal_timeline );
			// update_post_meta( $post_id, 'nano_goal_milestones', $nano_goal_milestones );
			// update_post_meta( $post_id, 'nano_goal_milestone_date', $nano_goal_milestones_date );
			// update_post_meta( $post_id, 'nano_team_members', $nano_team_members );
			// update_post_meta( $post_id, 'nano_team_leader_bio', $nano_team_leader_bio );
			// update_post_meta( $post_id, 'nano_project_significance', $nano_project_significance );

			if ( true == $nano_fifty_grant_status && ! in_array( get_post_meta( $post_id, 'fifty_fifty_grant', true ), array( 'pending', '' ), true ) ) {
				$recipient = get_user_by( 'login', get_post_meta( $post_id, 'original_author', true ) ) ? get_user_by( 'login', get_post_meta( $post_id, 'original_author', true ) ) : '';
				$recipient = '' != $recipient ? $recipient->user_email : '';
				nano_email_notify( $post_id, $recipient, 'fifty_fifty_grant' );
				// Apply nanosteam macthing grant to the project and add it to the ordering system.
				nano_matching_grant_ordering( $post_id );
			}
		}
	}
}
