<?php

// Bootstrap 4 custom form builder
// For instruction on how to label your classes - https://getbootstrap.com/docs/4.0/components/forms/

class NANO_Form {

	var $fields;

	function __construct(
		$fields = null
	) {
		$this->fields = $fields;
	}

	function build_form( $vars = null ) {
		$output = '';
		foreach ( $this->fields as $field ) {
			if ( isset( $field['label'] ) ) {
				$label = $field['label'];
			} else {
				$label = '';
			}
			if ( isset( $field['name'] ) ) {
				$name = $field['name'];
			} else {
				$name = '';
			}
			if ( isset( $field['id'] ) ) {
				$id = $field['id'];
			} else {
				$id = null;
			}
			if ( isset( $field['wclass'] ) ) {
				$wclass = ( ( isset( $field['type'] ) && '' != $field['type'] ) ? 'formval ' : '' ) . $field['wclass'];
			} else {
				$wclass = null;
			}
			if ( isset( $field['class'] ) ) {
				$class = ( 'file' == $field['type'] || 'radio' == $field['type'] || 'submit' == $field['type'] ? '' : 'form-control form-control-sm ' ) . $field['class'];
			} else {
				$class = 'form-control form-control-sm';
			}
			if ( isset( $field['type'] ) ) {
				$type = $field['type'];
			} else {
				$type = '';
			}
			if ( isset( $field['options'] ) ) {
				$options = $field['options'];
			} else {
				$options = null;
			}
			if ( isset( $field['value'] ) ) {
				$value = apply_filters( 'idcf_fes_' . $name . '_value', $field['value'], ( isset( $vars['post_id'] ) ? $vars['post_id'] : null ) );
			} else {
				$value = null;
			}
			if ( isset( $field['misc'] ) ) {
				$misc = $field['misc'];
			} else {
				$misc = '';
			}
			if ( isset( $field['input_addon'] ) ) {
				$input_addon_position   = $field['input_addon'][1];
				$input_addon_wrap_start = isset( $field['input_addon'] ) ? '<div class="input-group">' : '';
				$input_addon_prepend    = 'left' == $input_addon_position ? '<div class="input-group-prepend">' . $field['input_addon'][0] . '</div>' : '';
				$input_addon_append     = 'left' != $input_addon_position ? '<div class="input-group-append">' . $field['input_addon'][0] . '</div>' : '';
				$input_addon_wrap_end   = isset( $field['input_addon'] ) ? '</div>' : '';
			} else {
				// $input_addon_position   = '';
				$input_addon_wrap_start = '';
				// $input_addon            = '';
				$input_addon_prepend  = '';
				$input_addon_append   = '';
				$input_addon_wrap_end = '';
			}
			if ( isset( $field['help'] ) ) {
				// Creating array of all help fields for WP_admin management
				$help_field = is_array( get_option( 'nano_help_fields' ) ) ? get_option( 'nano_help_fields' ) : array();
				if ( isset( $field['heading'] ) && '' !== $field['heading'] ) {
					$help_field_id = strtolower( str_replace( ' ', '_', $field['heading'] ) );
				} elseif ( isset( $field['label'] ) && '' !== $field['label'] ) {
					$help_field_id = strtolower( str_replace( ' ', '_', $field['label'] ) );
				} elseif ( isset( $field['name'] ) && '' !== $field['name'] ) {
					$help_field_id = $field['name'];
				} elseif ( isset( $field['description'] ) && '' !== $field['description'] ) {
					$help_field_id = strtolower( str_replace( ' ', '_', $field['description'] ) );
					$help_field_id = preg_replace( '#[[:punct:]]#', '', $help_field_id );
				} else {
					$help_field_id = '';
				}
				if ( '' !== $help_field_id && 'blank' !== $help_field_id ) {
					$help_field = array_merge( array( $help_field_id ), $help_field );
					$help_field = array_unique( $help_field );
					update_option( 'nano_help_fields', $help_field );
				}
				$help = '<span class="help" title="' . $field['help'] . '" data-toggle="tooltip" data-placement="top"><i class="fa fa-question-circle"></i></span>';
			} else {
				$help = '';
			}
			if ( isset( $field['heading'] ) ) {
				$heading = '<div class="nan_heading row"><div class="col-sm-12"><h4>' . $field['heading'] . $help . '</h4></div></div>';
			} else {
				$heading = '';
			}
			if ( isset( $field['description'] ) ) {
				// Creating array of all description fields for WP_admin management
				$desk_field = is_array( get_option( 'nano_description_fields' ) ) ? get_option( 'nano_description_fields' ) : array();
				if ( isset( $field['heading'] ) && '' !== $field['heading'] ) {
					$desk_field_id = strtolower( str_replace( ' ', '_', $field['heading'] ) );
				} elseif ( isset( $field['label'] ) && '' !== $field['label'] ) {
					$desk_field_id = strtolower( str_replace( ' ', '_', $field['label'] ) );
				} elseif ( isset( $field['name'] ) && '' !== $field['name'] ) {
					$desk_field_id = $field['name'];
				} elseif ( isset( $field['description'] ) && '' !== $field['description'] ) {
					$desk_field_id = strtolower( str_replace( ' ', '_', $field['description'] ) );
					$desk_field_id = preg_replace( '#[[:punct:]]#', '', $desk_field_id );
				} else {
					$desk_field_id = '';
				}
				if ( '' !== $desk_field_id && 'blank' !== $desk_field_id ) {
					$desk_field = array_merge( array( $desk_field_id ), $desk_field );
					$desk_field = array_unique( $desk_field );
					update_option( 'nano_description_fields', $desk_field );
				}
				$description_t = '<div class="d-none d-sm-block col-sm-12"><small class="form-text">' . $field['description'] . ( '' == $heading ? $help : '' ) . '</small></div>';
				$description_b = '<small class="form-text d-block d-sm-none">' . $field['description'] . ( '' == $heading ? $help : '' ) . '</small>';
			} else {
				$description_t = '';
				$description_b = '';
			}
			// Start Building
			ob_start();
			$post_id = ( isset( $vars['post_id'] ) ? $vars['post_id'] : null );
			do_action( 'fes_' . $name . '_before', $post_id );
			$output .= ob_get_contents();
			ob_end_clean();
			$output .= $heading;
			if ( isset( $field['before'] ) ) {
				$output .= $field['before'];
			}
			$output .= $description_t;
			$output .= isset( $wclass ) || '' != $wclass ? '<div ' . ( isset( $wclass ) ? 'class="' . $wclass . '"' : '' ) . '>' : '';
			switch ( $type ) {
				case 'text':
					$output .= '<label class="sr-only"  for="' . $id . '">' . apply_filters( 'fes_' . $name . '_label', $label ) . '</label>';
					$output .= $input_addon_wrap_start . $input_addon_prepend;
					$output .= '<input type="text" id="' . $id . '" name="' . $name . '" class="' . $class . '" value="' . $value . '" ' . $misc . ' aria-label="' . apply_filters( 'fes_' . $name . '_label', $label ) . '">';
					$output .= $input_addon_append . $input_addon_wrap_end;
					break;
				case 'email':
					$output .= '<label class="sr-only"  for="' . $id . '">' . apply_filters( 'fes_' . $name . '_label', $label ) . '</label>';
					$output .= $input_addon_wrap_start . $input_addon_prepend;
					$output .= '<input type="email" id="' . $id . '" name="' . $name . '" class="' . $class . '" value="' . $value . '" ' . $misc . ' aria-label="' . apply_filters( 'fes_' . $name . '_label', $label ) . '">';
					$output .= $input_addon_append . $input_addon_wrap_end;
					break;
				case 'number':
					$output .= '<label class="sr-only"  for="' . $id . '">' . apply_filters( 'fes_' . $name . '_label', $label ) . '</label>';
					$output .= $input_addon_wrap_start . $input_addon_prepend;
					$output .= '<input type="number" id="' . $id . '" name="' . $name . '" class="' . $class . ' number-field" value="' . ( ( ! empty( $value ) ) ? $value : 0 ) . '" ' . $misc . ' aria-label="' . apply_filters( 'fes_' . $name . '_label', $label ) . '">';
					$output .= $input_addon_append . $input_addon_wrap_end;
					break;
				case 'password':
					$output .= '<label class="sr-only"  for="' . $id . '">' . apply_filters( 'fes_' . $name . '_label', $label ) . '</label>';
					$output .= $input_addon_wrap_start . $input_addon_prepend;
					$output .= '<input type="password" id="' . $id . '" name="' . $name . '" class="' . $class . '" value="' . $value . '" ' . $misc . ' aria-label="' . apply_filters( 'fes_' . $name . '_label', $label ) . '">';
					$output .= $input_addon_append . $input_addon_wrap_end;
					break;
				case 'file':
					$output .= '<div class="custom-' . $type . '" >';
					$output .= '<input type="file" id="' . $id . '" name="' . $name . '" class="custom-file-input ' . $class . '" value="' . $value . '" ' . $misc . ' aria-label="' . apply_filters( 'fes_' . $name . '_label', $label ) . '">';
					$output .= '<label for="' . $id . '" class="custom-file-label"></label>';
					$output .= '</div>';
					break;
				case 'date':
					$output .= '<label class="sr-only"  for="' . $id . '">' . apply_filters( 'fes_' . $name . '_label', $label ) . '</label>';
					$output .= $input_addon_wrap_start . $input_addon_prepend;
					$output .= '<input type="date" id="' . $id . '" name="' . $name . '" class="' . $class . '" value="' . $value . '" ' . $misc . ' aria-label="' . apply_filters( 'fes_' . $name . '_label', $label ) . '">';
					$output .= $input_addon_append . $input_addon_wrap_end;
					break;
				case 'tel':
					$output .= '<label class="sr-only"  for="' . $id . '">' . apply_filters( 'fes_' . $name . '_label', $label ) . '</label>';
					$output .= $input_addon_wrap_start . $input_addon_prepend;
					$output .= '<input type="tel" id="' . $id . '" name="' . $name . '" class="' . $class . '" value="' . $value . '" ' . $misc . ' aria-label="' . apply_filters( 'fes_' . $name . '_label', $label ) . '">';
					$output .= $input_addon_append . $input_addon_wrap_end;
					break;
				case 'hidden':
					$output .= '<input type="hidden" id="' . $id . '" name="' . $name . '" value="' . $value . '" ' . $misc . '>';
					break;
				case 'select':
					$output .= '<label class="sr-only" for="' . $id . '">' . apply_filters( 'fes_' . $name . '_label', $label ) . '</label>';
					$output .= '<select id="' . $id . '" name="' . $name . '" class="custom-' . $type . ' ' . $class . '" ' . $misc . '>';
					foreach ( $options as $option ) {
						$output .= '<option value="' . $option['value'] . '" ' . ( $option['value'] == $value ? 'selected="selected"' : '' ) . ' ' . $misc . ' ' . ( isset( $option['misc'] ) ? $option['misc'] : '' ) . '>' . $option['title'] . '</option>';
					}
					$output .= '</select>';
					break;
				case 'checkbox':
					$output .= '<div class="custom-control custom-' . $type . '">';
					$output .= '<input type="checkbox" id="' . $id . '" name="' . $name . '" class="custom-control-input ' . $class . '"  value="' . $value . '" ' . $misc . ' aria-label="' . apply_filters( 'fes_' . $name . '_label', $label ) . '">';
					$output .= '<label class="custom-control-label'. ( is_admin() ? ' d-inline-block pr-2' : '' ) .'" for="' . $id . '">' . apply_filters( 'fes_' . $name . '_label', $label ) . '</label>';
					$output .= '</div>';
					break;
				case 'radio':
					//$output .= '<div class="custom-control custom-' . $type . '">';
					$output .= '<input type="radio" id="' . $id . '" name="' . $name . '" class="custom-control-input ' . $class . '"  value="' . $value . '" ' . $misc . ' aria-label="' . apply_filters( 'fes_' . $name . '_label', $label ) . '">';
					$output .= '<label class="custom-control-label'. ( is_admin() ? ' d-inline-block pr-2' : '' ) .'"  for="' . $id . '">' . apply_filters( 'fes_' . $name . '_label', $label ) . '</label>';
					//$output .='</div>';
					break;
				case 'textarea':
					$output .= '<label class="sr-only"  for="' . $id . '">' . apply_filters( 'fes_' . $name . '_label', $label ) . '</label>';
					$output .= '<textarea id="' . $id . '" name="' . $name . '" class="' . $class . '" ' . $misc . '>' . $value . '</textarea>';
					break;
				case 'wpeditor':
					ob_start();
					echo '<label class="sr-only"  for="' . $id . '">' . apply_filters( 'fes_' . $name . '_label', $label ) . '</label>';
					wp_editor(
						html_entity_decode( $value ),
						$id,
						array(
							'editor_class'  => $class,
							'textarea_name' => $name,
							'media_buttons' => 1,
							'textarea_rows' => 6,
							'editor_css'    => '<style>* {font-family: "Lato", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif;}</style>',
							'tinymce'       => array(
								//'selector' => 'textarea',
								//'content_style' => '* {font-family: "Lato", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif;}'
								'content_css' => get_stylesheet_directory_uri() . '/editor-style.css',
							),
						)
					);
					$output .= ob_get_contents();
					ob_end_clean();
					break;
				case 'submit':
					$output .= '<button type="submit" id="' . $id . '" name="' . $name . '" class="px-3 btn-block btn ' . $class . '" ' . $misc . '>' . $value . '</button>';
					break;
			}
			$output .= isset( $wclass ) || '' != $wclass ? '</div>' : '';
			//$output .= $description_b;
			if ( isset( $field['after'] ) ) {
				$output .= $field['after'];
			}
			ob_start();
			do_action( 'fes_' . $name . '_after', $post_id );
			$output .= ob_get_contents();
			ob_end_clean();
		}

		return $output;
	}
}
