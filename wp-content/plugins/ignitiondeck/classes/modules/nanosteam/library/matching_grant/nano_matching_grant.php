<?php


/**
 * If project is approved for matching grant - Add nanosSTEAM as a backer and add the order to the system for tracking
 *
 * @param string $post_id
 */

function nano_matching_grant_ordering( $post_id ) {
	global $wpdb;
	$tz = get_option( 'timezone_string' );
	if ( empty( $tz ) ) {
		$tz = 'UTC';
	}
	date_default_timezone_set( $tz );

	$nano_fifty_review   = get_post_meta( $post_id, 'fifty_fifty_grant', true );
	$nano_fifty_goal_amt = get_post_meta( $post_id, 'ign_fund_goal', true );
	$nano_fifty_goal_amt = $nano_fifty_goal_amt / 2;
	$credits             = $nano_fifty_goal_amt;

	if ( 'approved' == $nano_fifty_review && $nano_fifty_goal_amt < ( ( get_option( 'nano_grant_amt', 1501 ) ) / 2 + 1 ) ) {

		//Add order to NanoSTEAM

		//User ID
		$email      = get_option( 'admin_email' );
		$member     = new ID_Member();
		$check_user = $member->check_user( $email );
		$user_id    = $check_user->ID;
		$user_meta  = get_user_meta( $user_id );

		//Product ID
		$project_id = get_post_meta( $post_id, 'ign_project_id', true );
		$product_id = nano_get_level_id( $project_id );

		$access_levels = array( $product_id );

		// TXN ID
		$txn_id = 'nanosteam Matching Grant';

		// E Date
		$exp    = strtotime( '+90 days' );
		$e_date = date( 'Y-m-d h:i:s', $exp );
		$paykey = md5( $email . time() );

		// Add tokens to the admin account based on matching goal amount
		if ( isset( $user_id ) && isset( $credits ) ) {
			nano_add_credits( $user_id, $credits );
		}

		// Get the user's tokens.
		$member     = new ID_Member( $user_id );
		$md_credits = $member->get_user_credits();

		// Add product order and subtract tokens from user

		if ( $md_credits >= $credits ) {

			$match_user = $member->match_user( $user_id );

			if ( ! empty( $match_user ) ) {

				// Adding the order to the backer

				if ( isset( $match_user->access_level ) ) {
					// let's combine levels
					$old_levels = maybe_unserialize( $match_user->access_level );
					if ( is_array( $old_levels ) ) {
						$key = array();
						foreach ( $old_levels as $key['val'] ) {
							$access_levels[] = $key['val'];
						}
					}
				}
				if ( isset( $match_user->data ) ) {
					// let's combine data
					$old_data = unserialize( $match_user->data );
					// do we need any data for credit purchases?
					//$old_data[] =  array('customer_id' => $custid);
				} else {
					$old_data = array();
				}

				$order = new ID_Member_Order( null, $user_id, $product_id, null, $txn_id, '', 'active', $e_date, $nano_fifty_goal_amt );

				$new_order = $order->add_order();
				MD_Keys::set_licenses( $user_id, $product_id );
				$user = array(
					'user_id' => $user_id,
					'level'   => $access_levels,
					'data'    => $old_data,
				);

				//Update the user
				ID_Member::update_user( $user );

				ID_Member_Order::update_order_meta( $new_order, 'pwyw_price', $nano_fifty_goal_amt );
				nano_use_credits( $user_id, $nano_fifty_goal_amt );

				// don't send receipt for now
				do_action( 'memberdeck_payment_success', $user_id, $new_order, $paykey, '', 'credit' );

				// Adding the order to the project as the user order does not automatically apply to the project
				$table = $wpdb->prefix . 'ign_pay_info';
				$sql   = "INSERT INTO $table (first_name,last_name,email,address,country,state,city,zip,product_id,product_level,prod_price,status,created_at)
                        VALUES (
                            
                            '" . esc_attr( $user_meta['first_name'][0] ) . "',
                            '" . esc_attr( $user_meta['last_name'][0] ) . "',
                            '" . esc_attr( $email ) . "',
                            '" . esc_attr( 'address' ) . "',
                            '" . esc_attr( 'country' ) . "',
                            '" . esc_attr( 'state' ) . "',
                            '" . esc_attr( 'city' ) . "',
                            '" . esc_attr( 'zip' ) . "',
                            '" . absint( $project_id ) . "',
                            '" . absint( '1' ) . "',
                            '" . esc_attr( $nano_fifty_goal_amt ) . "',
                            '" . esc_attr( 'C' ) . "',
                            '" . nano_get_formated_date_time_gmt( 'Y-m-d H:i:s' ) . "'
                        )";
				$result = $wpdb->query( $sql );

				// Debugging
				nano_debug_query( $result, $sql, 'nano_matching_grant_ordering' );

				$pay_info_id = $wpdb->insert_id;

				do_action( 'id_modify_order', $pay_info_id, 'insert' );
				do_action( 'id_insert_order', $pay_info_id );

				// Update the project's funding and percentage meta data
				$post = get_post( $post_id );
				set_project_meta( $post_id, $post, '' );
			}
		}
	}
}



/**
 * Check to see if the second lab milsestone has been published
 *
 * @param $post_id
 *
 * @return bool
 */

function nano_second_milsetone_achieved( $post_id ) {

	$project_uid = get_post_meta( $post_id, 'project_uid', true );

	global $wpdb;

	// this adds the prefix which is set by the user upon instillation of WordPress
	$table_name = $wpdb->prefix . 'nano_meta_data';

	// Adding an order number to each type starting at 000
	$sql     = "SELECT * FROM $table_name WHERE project_uid LIKE %s";
	$sql     = $wpdb->prepare( $sql, $project_uid );
	$results = $wpdb->get_results( $sql );

	// this will get the data from your table
	$project_status = isset( $results[0] ) ? unserialize( $results[0]->project_status ) : '';

	$second_payment_check = isset( $project_status['date_of_2nd_lab'] ) && '' != $project_status['date_of_2nd_lab'] ? true : false;

	return $second_payment_check;

}



