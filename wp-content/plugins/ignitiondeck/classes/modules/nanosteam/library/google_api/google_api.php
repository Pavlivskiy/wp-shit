<?php

/*---------------------------------
 vardump script
------------------------------------*/

function view_var2( $i ) {
	echo '<pre style="background-color: #000; color: greenyellow; padding: 10px">';
	echo var_dump( $i );
	echo '</pre>';
}

/**
 * Send values to php error log file
 *
 * @param $i
 */
function send_error_goog( $i ) {
	error_log( print_r( $i, true ) );
}


function update_google_analtics_cron() {
// Load the Google API PHP Client Library.
	require_once __DIR__ . '/google/vendor/autoload.php';

	$analytics = initializeAnalytics();
	$response  = getReport( $analytics );

	nanoWriteApiData( $response );

}

// Run the metadata for all projects daily daily
if ( ! wp_next_scheduled( 'nano_cron_google_analtics_hook' ) ) {
	wp_schedule_event( time(), '30min', 'nano_cron_google_analtics_hook' );
}

add_action( 'nano_cron_google_analtics_hook', 'update_google_analtics_cron' );


/**
 * Query the $response and write to a local file which will be updated every 30 minutes.
 *
 * @param $response
 *
 * @return array of data.
 */
function nanoWriteApiData( $response ) {

	if ( getenv( 'HTTP_HOST' ) == 'nanosteam.staging.wpengine.com' || getenv( 'HTTP_HOST' ) == 'nanosteam.site' ) {
		$data_file_name = '/data/nanosteam_api_data_staging.json';
	} else {
		$data_file_name = '/data/nanosteam_api_data_live.json';
	}

	$data     = json_encode( (array) $response );
	$data_file = fopen( __DIR__ . $data_file_name, 'w' );
	fwrite( $data_file, $data );
	fclose( $data_file );

}


/**
 * Initializes an Analytics Reporting API V4 service object.
 *
 * @return An authorized Analytics Reporting API V4 service object.
 */
function initializeAnalytics() {

	if ( getenv( 'HTTP_HOST' ) == 'nanosteam.staging.wpengine.com' || getenv( 'HTTP_HOST' ) == 'nanosteam.site' ) {
		$service_json_key = '/service-account-credentials-staging.json';
	} else {
		$service_json_key = '/service-account-credentials-live.json';
	}

	// Use the developers console and download your service account
	// credentials in JSON format. Place them in this directory or
	// change the key file location if necessary.
	$key_file_location = __DIR__ . $service_json_key;

	// Create and configure a new client object.
	$client = new Google_Client();
	$client->setApplicationName( 'Nanosteam Popular Projects Analytics' );
	$client->setAuthConfig( $key_file_location );
	$client->setScopes( [ 'https://www.googleapis.com/auth/analytics.readonly' ] );
	$analytics = new Google_Service_AnalyticsReporting( $client );

	return $analytics;

}


/**
 * Queries the Analytics Reporting API V4.
 *
 * @param service An authorized Analytics Reporting API V4 service object.
 *
 * @return The Analytics Reporting API V4 response.
 *
 * https://ga-dev-tools.appspot.com/query-explorer/
 * metrics: ga:users
 * dimensions: ga:pagePath
 */
function getReport( $analytics ) {

	// Replace with your view ID, for example XXXX.

	if ( getenv( 'HTTP_HOST' ) == 'nanosteam.staging.wpengine.com' || getenv( 'HTTP_HOST' ) == 'nanosteam.site' ) {
		$view_id = '153649671';
	} else {
		$view_id = '153649671';
	}

	// Create the DateRange object.
	$date_range = new Google_Service_AnalyticsReporting_DateRange();
	$date_range->setStartDate( '60daysAgo' );
	$date_range->setEndDate( 'today' );

	// Create the Metrics object.
	$sessions = new Google_Service_AnalyticsReporting_Metric();
	$sessions->setExpression( 'ga:users' );
	$sessions->setAlias( 'users' );

	//Create the Dimensions object.
	$page_path = new Google_Service_AnalyticsReporting_Dimension();
	$page_path->setName( 'ga:pagePath' );

	// Create the ReportRequest object.
	$request = new Google_Service_AnalyticsReporting_ReportRequest();
	$request->setViewId( $view_id );
	$request->setDateRanges( $date_range );
	$request->setMetrics( array( $sessions ) );
	$request->setDimensions( array( $page_path ) );

	$body = new Google_Service_AnalyticsReporting_GetReportsRequest();
	$body->setReportRequests( array( $request ) );

	return $analytics->reports->batchGet( $body );
}


/**
 * Parses and prints the Analytics Reporting API V4 response.
 *
 * @param An Analytics Reporting API V4 response.
 */
function printResults( $reports ) {
	for ( $report_index = 0; $report_index < count( $reports ); $report_index ++ ) {
		$report            = $reports[ $report_index ];
		$header            = $report->getColumnHeader();
		$dimension_headers = $header->getDimensions();
		$metric_headers    = $header->getMetricHeader()->getMetricHeaderEntries();
		$rows              = $report->getData()->getRows();

		for ( $row_index = 0; $row_index < count( $rows ); $row_index ++ ) {
			$row        = $rows[ $row_index ];
			$dimensions = $row->getDimensions();
			$metrics    = $row->getMetrics();
			for ( $i = 0; $i < count( $dimension_headers ) && $i < count( $dimensions ); $i ++ ) {
				print( $dimension_headers[ $i ] . ': ' . $dimensions[ $i ] . "\n" );
			}

			for ( $j = 0; $j < count( $metrics ); $j ++ ) {
				$values = $metrics[ $j ]->getValues();
				for ( $k = 0; $k < count( $values ); $k ++ ) {
					$entry = $metric_headers[ $k ];
					print( $entry->getName() . ': ' . $values[ $k ] . "\n" );
				}
			}
		}
	}
}