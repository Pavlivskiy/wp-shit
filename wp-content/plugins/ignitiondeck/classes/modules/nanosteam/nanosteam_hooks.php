<?php if ( ! defined( 'ABSPATH' ) ) {
	die( 'Direct access forbidden.' );
}


/**
 * Disable the export project function from IDK as it exports too much data and should be considered a security risk
 */

function disable_export_project() {
	if ( isset( $_GET['export_project'] ) ) {
		$_GET['export_project'] = 0;
	}
}

add_action( 'init', 'disable_export_project', 1 );


/**
 * Create Admin Meta Box
 */

function admin_nano_fifty_metabox() {
	$metabox = new IDC_Nano_Fifty_Metaboxes();
}


/**
 * Adding the stripe connect custom signup to the project creation form
 */
function nano_stripe_connect() {
	//$form = new ID_FES(null, (isset($vars) ? $vars : null));
	include_once NANO_STRIPE . '/library/project_form/public/_nanoStripeConnect.php';

}

add_action( 'md_payment_settings_extrafields', 'nano_stripe_connect' );

/**
 * Adjust the projects ability to save more than one category
 */

function save_multi_categories() {
	$post_id = isset( $_GET['edit_project'] ) ? $_GET['edit_project'] : '';
	if ( isset( $_GET['edit_project'] ) ) {
		$cat_terms = get_post_meta( $post_id, 'project_category' );
		wp_set_object_terms( $post_id, $cat_terms, 'project_category', false );
	}
}


/**
 * Sanitize a form input sharing the same name where we accept an array of values
 *
 * @param $post_field
 *
 * @return array
 */

function nano_sanitize_array( $post_field ) {
	$field_array = array();
	$count       = 0;
	foreach ( $_POST[ $post_field ] as $key => $field ) {
		if ( ! empty( $field ) ) {
			if ( 'nano_team_members' == $post_field ) {
				if ( filter_var( $field, FILTER_VALIDATE_EMAIL ) ) {
					$field_array[] = sanitize_email( $field );
				}
			} elseif ( 'nano_help_field' == $post_field || 'nano_description_field' == $post_field ) {
				if ( 'nano_help_field' == $post_field ) {
					$field_array[ $key ] = sanitize_text_field( $field );
				}
				if ( 'nano_description_field' == $post_field ) {
					$field_array[ $key ] = sanitize_text_field( $field );
				}
			} else {
				$field_array[] = sanitize_text_field( $field );
			}
		}
		$count ++;
	}

	return $field_array;
}

function nano_help_field( $var ) {
	if ( is_array( get_option( 'nano_help_field' ) ) && isset( get_option( 'nano_help_field' ){$var} ) ) {
		$field = get_option( 'nano_help_field' ){$var};
	} else {
		$field = 'blank';
	}

	return $field;
}

function nano_description_field( $var ) {
	if ( is_array( get_option( 'nano_description_field' ) ) && isset( get_option( 'nano_description_field' ){$var} ) ) {
		$field = get_option( 'nano_description_field' ){$var};
	} else {
		$field = '';
	}

	return $field;
}


/**
 * Save NanoSTEAM info when project created
 *
 * @param $post_id
 * @param $post_file
 * @param $hidden_post_file
 * @param $size
 *
 * @return mixed
 */


function crop_image( $post_id, $post_file, $hidden_post_file, $size ) {

	$nano_image        = $_FILES[ $post_file ];
	$upload_dir        = wp_upload_dir();
	$attach_id         = '';
	$resized_image_url = '';

	if ( isset( $_POST[ $hidden_post_file ] ) && '' != $_POST[ $hidden_post_file ] ) {

		$data = $_POST[ $hidden_post_file ];
		$data = explode( ',', $data );
		$data = base64_decode( $data[1] );

		$image = $data;

		// @new
		$upload_path = str_replace( '/', DIRECTORY_SEPARATOR, $upload_dir['path'] ) . DIRECTORY_SEPARATOR;

		$decoded  = $image;
		$filename = wp_get_current_user()->ID . '.png';

		$hashed_filename = md5( $filename . microtime() ) . '_' . $filename;

		// @new
		$image_upload = file_put_contents( $upload_path . $hashed_filename, $decoded );

		//HANDLE UPLOADED FILE
		if ( ! function_exists( 'wp_handle_sideload' ) ) {
			require_once( ABSPATH . 'wp-admin/includes/file.php' );
		}

		// Without that I'm getting a debug error!?
		if ( ! function_exists( 'wp_get_current_user' ) ) {
			require_once( ABSPATH . 'wp-includes/pluggable.php' );
		}

		// @new
		$file             = array();
		$file['error']    = '';
		$file['tmp_name'] = $upload_path . $hashed_filename;
		$file['name']     = $hashed_filename;
		$file['type']     = 'image/png';
		$file['size']     = filesize( $upload_path . $hashed_filename );

		// upload file to server
		// @new use $file instead of $image_upload
		$results = wp_handle_sideload( $file, array( 'test_form' => false ) );

	} else {
		$results = wp_handle_upload( $nano_image, array( 'test_form' => false ) );
	}

	$image_filetype = wp_check_filetype( basename( $results['file'] ), null );

	if ( strtolower( 'png' ) == $image_filetype['ext'] || strtolower( 'jpg' ) == $image_filetype['ext'] || strtolower( 'gif' ) == $image_filetype['ext'] || strtolower( 'jpeg' ) == $image_filetype['ext'] ) {
		$hero_attachment = array(
			'guid'           => $upload_dir['url'] . '/' . basename( $results['file'] ),
			'post_mime_type' => $image_filetype['type'],
			'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $results['file'] ) ),
			'post_content'   => '',
			'post_status'    => 'inherit',
		);
		$hero_posted     = true;

		// Insert the attachment.
		$attach_id = wp_insert_attachment( $hero_attachment, $results['url'], $post_id );

		// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
		require_once( ABSPATH . 'wp-admin/includes/image.php' );

		// Generate the metadata for the attachment, and update the database record.
		$attach_data = wp_generate_attachment_metadata( $attach_id, $results['file'] );
		wp_update_attachment_metadata( $attach_id, $attach_data );

		$resized_image_url = wp_get_attachment_image_src( $attach_id, $size );

	} else {
		$hero_posted = false;
	}

	$value['results']       = $results;
	$value['attachment_id'] = $attach_id;
	$value['image']         = $resized_image_url;
	$value['posted']        = $hero_posted;

	//send_error($value);

	return $value;

}

/**
 * Cropping function for ulpoaded images to projects
 */

function crop_image_ajax() {

	// First check the nonce, if it fails the function will break
	check_ajax_referer( 'nano_image_secure', 'security' );

	$post_file        = $_POST['post_file'];
	$hidden_post_file = $_POST['hidden_post_file'];
	$aspect           = $_POST['aspect'];
	$upload_dir       = wp_upload_dir();
	$results          = '';

	if ( isset( $hidden_post_file ) && '' != $hidden_post_file ) {

		$data  = $hidden_post_file;
		$data  = explode( ',', $data );
		$data  = base64_decode( $data[1] );
		$image = $data;

		// @new
		$upload_path = str_replace( '/', DIRECTORY_SEPARATOR, $upload_dir['path'] ) . DIRECTORY_SEPARATOR;

		$decoded  = $image;
		$filename = wp_get_current_user()->ID . '.jpg';

		$hashed_filename = md5( $filename . microtime() ) . '_' . $filename;

		// @new
		$image_upload = file_put_contents( $upload_path . $hashed_filename, $decoded );

		//HANDLE UPLOADED FILE
		if ( ! function_exists( 'wp_handle_sideload' ) ) {
			require_once( ABSPATH . 'wp-admin/includes/file.php' );
		}

		// Without that I'm getting a debug error!?
		if ( ! function_exists( 'wp_get_current_user' ) ) {
			require_once( ABSPATH . 'wp-includes/pluggable.php' );
		}

		// @new
		$file             = array();
		$file['error']    = '';
		$file['tmp_name'] = $upload_path . $hashed_filename;
		$file['name']     = $hashed_filename;
		$file['type']     = 'image/jpg';
		$file['size']     = filesize( $upload_path . $hashed_filename );

		// upload file to server
		// @new use $file instead of $image_upload
		$results = wp_handle_sideload( $file, array( 'test_form' => false ) );

	}

	$current_user = wp_get_current_user();
	$user_id      = $current_user->ID;

	if ( isset( $results['file'] ) ) {
		send_error('crop fired');
		$_SESSION['results'][ $user_id ][ $aspect ] = $results;
		echo json_encode(
			array(
				'results' => 'complete',
				'message' => __( 'Image Saved' ),
				'url'     => $results['url']
			)
		);
	} else {
		send_error('crop failed');
		echo json_encode(
			array(
				'results' => false,
				'message' => __( 'There was a problem' ),
			)
		);
	}

	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		wp_die();
	}

}

/**
 * Returning image results from uploaded images from project creation form
 *
 * @param $post_id
 * @param $size
 *
 * @return mixed
 */

function crop_image_ajax_results( $post_id, $size ) {

	$current_user = wp_get_current_user();
	$user_id      = $current_user->ID;

	$upload_dir = wp_upload_dir();
	$aspect     = $size === 'nano-crop-16-9-sm' ? 'square' : 'circle';
	$results    = $_SESSION['results'][ $user_id ][ $aspect ];

	$value = '';

	if ( isset( $results['file'] ) ) {

		$value = array();

		$image_filetype = wp_check_filetype( basename( $results['file'] ), null );

		if ( strtolower( 'png' ) == $image_filetype['ext'] || strtolower( 'jpg' ) == $image_filetype['ext'] || strtolower( 'gif' ) == $image_filetype['ext'] || strtolower( 'jpeg' ) == $image_filetype['ext'] ) {
			$hero_attachment = array(
				'guid'           => $upload_dir['url'] . '/' . basename( $results['file'] ),
				'post_mime_type' => $image_filetype['type'],
				'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $results['file'] ) ),
				'post_content'   => '',
				'post_status'    => 'inherit',
			);
			$hero_posted     = true;

			// Insert the attachment.

			if ( '' == $post_id ) {
				$post_id = 0;
			}

			$attach_id = wp_insert_attachment( $hero_attachment, $results['url'], $post_id );

			// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
			require_once( ABSPATH . 'wp-admin/includes/image.php' );

			// Generate the metadata for the attachment, and update the database record.
			$attach_data = wp_generate_attachment_metadata( $attach_id, $results['file'] );
			wp_update_attachment_metadata( $attach_id, $attach_data );

			$resized_image_url = wp_get_attachment_image_src( $attach_id, $size );

		} else {
			$hero_posted = false;
		}

		$_SESSION['results'][ $user_id ][ $aspect ] = array();

		$value['results']       = $results;
		$value['attachment_id'] = $attach_id;
		$value['image']         = $resized_image_url;
		$value['posted']        = $hero_posted;

	}

	return $value;

}


/**
 * Restrict the types of files a user can upload
 *
 * @param $mimes
 *
 * @return array
 */
function nano_restrict_mime($mimes) {

	if ( ! current_user_can( 'administrator' ) ) {
		$mimes = array(
			'png'          => 'image/png',
			'jpg|jpeg|jpe' => 'image/jpeg',
			'gif'          => 'image/gif',
		);
		remove_filter( 'upload_mimes', 'krown_mime_types' );
	}
	return $mimes;
}

add_filter('upload_mimes','nano_restrict_mime');

/**
 * returns the precise unix filetime when the extensions was last updated. (Cache busting)
 *
 * @param $filename
 *
 * @return array
 */
function nano_filetime( $filename ) {
	$version_func = filemtime( NANO_STRIPE . $filename );

	return $version_func;
}


/**
 * enables the media uploader button on the front end if in the creater or edit project section
 *
 */
function nano_load_media_and_editor_front_end() {
	if ( is_admin() ) {
		return;
	}
	if ( isset( $_GET['create_project'] ) || isset( $_GET['edit_project'] ) ) {
		wp_enqueue_media();
	}
}

add_action( 'wp_enqueue_scripts', 'nano_load_media_and_editor_front_end' );

/**
 * Clean up and remove portions of the WP media uploader on the front end.
 *
 * @param $tabs
 *
 * @return mixed
 */
function nano_remove_media_tabs($tabs) {
	if ( isset( $_GET['create_project'] ) || isset( $_GET['edit_project'] ) ) {
		if ( ! current_user_can( 'administrator' ) ) {
			unset( $tabs['mediaLibraryTitle'] );
			unset( $tabs['uploadFilesTitle'] );
			unset( $tabs['insertFromUrlTitle'] );
			unset( $tabs['insertMediaTitle'] );
			unset( $tabs['createGalleryTitle'] );
			unset( $tabs['createPlaylistTitle'] );
			unset( $tabs['createVideoPlaylistTitle'] );
		}
	}
	return $tabs;
}

add_filter( 'media_view_strings', 'nano_remove_media_tabs' );


/**
 * Removing the text tab for the project creation flow on the wp_editor
 *
 * @param $settings
 *
 * @return mixed
 */
function nano_editor_settings($settings) {
	if ( isset( $_GET['create_project'] ) || isset( $_GET['edit_project'] ) ) {
		if ( ! current_user_can( 'administrator' ) ) {
			$settings['quicktags'] = false;

			return $settings;
		} else {
			$settings['quicktags'] = true;

			return $settings;
		}
	}
}

add_filter('wp_editor_settings', 'nano_editor_settings');

/**
 * Upadting metadata for the project
 *
 * @param $post_id
 * @param $post
 */

function save_nano_meta( $post_id, $post ) {

	// Lets check to see if the image has been changed in the project admin or not.
	// If so, we need to replace the project's default image.
	if (is_admin()) {
		$creator_project_image = get_post_meta( $post_id, 'project_main', true );
		$admin_project_image   = get_post_thumbnail_id( $post_id );

		if ( '' !== $admin_project_image && $admin_project_image !== $creator_project_image ) {

			update_post_meta( $post_id, 'project_main', $admin_project_image );

		}
	}


	$current_user = wp_get_current_user();
	$user_id      = $current_user->ID;
	$user_login   = $current_user->user_login;
	$user_email   = $current_user->user_email;
	if ( is_user_logged_in() ) {
		if ( ! current_user_can( 'create_edit_projects' ) ) {
			return;
		}
		if ( ( isset( $_GET['create_project'] ) && $user_id == $post->post_author ) || ( isset( $_GET['edit_project'] ) && get_post_meta( $post_id, 'original_author', true ) == $user_login ) ) {

			$nano_fifty_grant          = isset( $_POST['nano_fifty_grant_apply'] ) ? sanitize_text_field( $_POST['nano_fifty_grant_apply'] ) : get_post_meta( $post_id, 'nano_fifty_grant_apply', true );
			$nano_video_link           = isset( $_POST['project_video'] ) ? wp_kses_post( $_POST['project_video'] ) : '';
			$nano_location_city        = isset( $_POST['nano_location_city'] ) ? sanitize_text_field( $_POST['nano_location_city'] ) : '';
			$nano_location_country     = isset( $_POST['nano_location_country'] ) ? sanitize_text_field( $_POST['nano_location_country'] ) : '';
			$nano_location_state       = isset( $_POST['nano_location_state'] ) ? sanitize_text_field( $_POST['nano_location_state'] ) : '';
			$nano_goal_timeline        = isset( $_POST['nano_goal_timeline'] ) ? sanitize_textarea_field( $_POST['nano_goal_timeline'] ) : '';
			$nano_goal_milestones      = isset( $_POST['nano_goal_milestones'] ) ? nano_sanitize_array( 'nano_goal_milestones' ) : '';
			$nano_goal_milestones_date = isset( $_POST['nano_goal_milestone_date'] ) ? nano_sanitize_array( 'nano_goal_milestone_date' ) : '';
			$nano_team_members         = isset( $_POST['nano_team_members'] ) ? nano_sanitize_array( 'nano_team_members' ) : '';
			$nano_team_leader_name     = isset( $_POST['nano_team_leader_name'] ) ? sanitize_text_field( $_POST['nano_team_leader_name'] ) : '';
			$nano_team_leader_title    = isset( $_POST['nano_team_leader_title'] ) ? sanitize_text_field( $_POST['nano_team_leader_title'] ) : '';
			$nano_team_leader_bio      = isset( $_POST['nano_team_leader_bio'] ) ? sanitize_textarea_field( $_POST['nano_team_leader_bio'] ) : '';
			$nano_project_significance = isset( $_POST['nano_project_significance'] ) ? sanitize_textarea_field( $_POST['nano_project_significance'] ) : '';
			$nano_budget_description   = isset( $_POST['nano_budget_description'] ) ? sanitize_textarea_field( $_POST['nano_budget_description'] ) : '';
			$nano_budget_title         = isset( $_POST['nano_budget_title'] ) ? nano_sanitize_array( 'nano_budget_title' ) : '';
			$nano_budget_value         = isset( $_POST['nano_budget_value'] ) ? nano_sanitize_array( 'nano_budget_value' ) : '';
			$nano_endorsers            = isset( $_POST['nano_endorsement_members'] ) ? nano_sanitize_array( 'nano_endorsement_members' ) : '';
			$nano_long_description     = isset( $_POST['nano_project_long_description'] ) ? sanitize_textarea_field( $_POST['nano_project_long_description'] ) : '';
			$nano_short_description    = isset( $_POST['nano_project_short_description'] ) ? sanitize_textarea_field( $_POST['nano_project_short_description'] ) : '';
			$nano_save_continue        = isset( $_POST['save_continue'] ) ? sanitize_text_field( $_POST['save_continue'] ) : '';

			// Handle the Team Leader Image
			$wp_upload_dir = wp_upload_dir();
			if ( isset( $_FILES['nano_team_leader'] ) && $_FILES['nano_team_leader']['size'] > 0 ) {

				$image_resize = crop_image_ajax_results( $post_id, 'thumbnail' );

				if ( isset( $image_resize['results'] ) ) {
					$results           = $image_resize['results'];
					$resized_image_url = $image_resize['image'];
					$hero_posted       = $image_resize['posted'];
				}
			}

			if ( isset( $results['url'] ) ) {
				$results = $resized_image_url[0];
				//update_post_meta( $post_id, 'nano_team_leader', $results );
				update_user_meta( $user_id, 'nano_idc_avatar', $results );
			}

			if ( isset( $_POST['nano_team_leader_removed'] ) && 'yes' == $_POST['nano_team_leader_removed'] ) {
				delete_user_meta( $user_id, 'nano_idc_avatar' );
			}

			// Handle the Project Image
			$wp_upload_dir = wp_upload_dir();
			if ( isset( $_FILES['project_main'] ) && $_FILES['project_main']['size'] > 0 ) {

				//$image_resize_hero = crop_image($post_id, 'project_hero', 'nano_featured_image_hidden', 'nano-crop-16-9-lg');
				$image_resize_hero = crop_image_ajax_results( $post_id, 'nano-crop-16-9-sm' );

				if ( isset( $image_resize_hero['results'] ) ) {
					$results_hero           = $image_resize_hero['results'];
					$resized_image_url_hero = $image_resize_hero['image'];
					$hero_main_posted       = $image_resize_hero['posted'];
					$hero_attached_id       = $image_resize_hero['attachment_id'];
				}

			}

			if ( isset( $results_hero['url'] ) ) {
				$results_hero = $results_hero['url'];
				update_post_meta( $post_id, 'project_main', $hero_attached_id );
				// Set the thumbnail
				$set_thumbnail = set_post_thumbnail( $post_id, $hero_attached_id );
				//$set_thumbnail = update_post_meta( $post_id, '_thumbnail_id', $hero_attached_id );

			}

			if ( isset( $_POST['project_main_removed'] ) && 'yes' == $_POST['project_main_removed'] ) {
				delete_post_meta( $post_id, 'project_main' );
			}

			// Multiple Categories

			$cat_values = array();

			if ( ! empty( $_POST['project_category_1'] ) || ! empty( $_POST['project_category_0'] ) ) {
				$cat_values = array( $_POST['project_category_0'], $_POST['project_category_1'] );
				delete_post_meta( $post_id, 'project_category' );
				foreach ( $cat_values as $category ) {
					add_post_meta( $post_id, 'project_category', $category, false );
				}
			}

			if ( isset( $_GET['create_project'] ) && $_GET['create_project'] ) {
				$author = wp_get_current_user()->user_login;
				update_post_meta( $post_id, 'original_author', $author );
				update_post_meta( $post_id, 'preview_post', true );
			}

			$nano_fifty_grant_apply = update_post_meta( $post_id, 'nano_fifty_grant_apply', $nano_fifty_grant );

			$update_city = update_post_meta( $post_id, 'nano_location_city', $nano_location_city );

			update_post_meta( $post_id, 'nano_location_country', $nano_location_country );

			update_post_meta( $post_id, 'nano_location_state', $nano_location_state );

			update_post_meta( $post_id, 'nano_goal_timeline', $nano_goal_timeline );

			update_post_meta( $post_id, 'nano_goal_milestones', $nano_goal_milestones );

			update_post_meta( $post_id, 'nano_goal_milestone_date', $nano_goal_milestones_date );

			$update_teammember = update_post_meta( $post_id, 'nano_team_members', $nano_team_members );

			update_post_meta( $post_id, 'nano_budget_description', $nano_budget_description );

			update_post_meta( $post_id, 'nano_budget_title', $nano_budget_title );

			update_post_meta( $post_id, 'nano_budget_value', $nano_budget_value );

			// Team Leader
			$current_user   = wp_get_current_user();
			$org_author     = get_post_meta( $post_id, 'original_author', true );
			$org_author     = '' != $org_author ? ( false !== get_user_by( 'login', $org_author ) ? get_user_by( 'login', $org_author ) : get_user_by( 'email', $org_author ) ) : '';
			$team_leader_id = '' != $org_author ? $org_author->ID : $current_user->ID;
			wp_update_user(
				array(
					'ID'           => $team_leader_id,
					'display_name' => $nano_team_leader_name,
				)
			);
			update_user_meta( $team_leader_id, 'nano_user_title', $nano_team_leader_title );

			update_post_meta( $post_id, 'nano_team_leader_bio', $nano_team_leader_bio );

			update_post_meta( $post_id, 'nano_project_significance', $nano_project_significance );

			update_post_meta( $post_id, 'nano_project_long_description', $nano_long_description );

			update_post_meta( $post_id, 'nano_project_short_description', $nano_short_description );



			$saved_and_continue = update_post_meta( $post_id, 'save_continue', $nano_save_continue );

			$creator_endorser_check = array_search( $user_email, $nano_endorsers );

			if ( false != $creator_endorser_check || null != $creator_endorser_check ) {
				unset( $nano_endorsers[ $creator_endorser_check ] );
				$update_endorser = update_post_meta( $post_id, 'nano_endorsement_members', $nano_endorsers );
			} else {
				$update_endorser = update_post_meta( $post_id, 'nano_endorsement_members', $nano_endorsers );
			}

			//Add in post geolocation data
			if ( true == $update_city ) {
				geo_locate_by_city_state( $nano_location_city, $nano_location_state, $post_id );
			}

			// Send Email to Admin if 50/50 Application is added
			if ( false !== $nano_fifty_grant_apply ) {
				if ( get_post_meta( $post_id, 'nano_fifty_grant_apply', true ) == 'yes' ) {
					$to             = get_option( 'admin_email' );
					$ign_project_id = get_post_meta( $post_id, 'ign_project_id', true );
					$project_name   = get_the_title( $post_id );
					$edit_link      = get_edit_post_link( $post_id );
					$subject        = 'A project has been submitted for Matching Grant review.';
					$message        = 'A project has recently been submitted for your review. In addition it has also been submitted for Matching Grant review as well.' . "\n";
					$message       .= 'Project: ' . $project_name . "\n";
					$message       .= 'Review Here: ' . $edit_link . "\n";

					wp_mail( $to, $subject, $message );
				}
			}

			// Send Team Member Emails out to new team member additions
			if ( false !== $update_teammember ) {
				$team_members = get_post_meta( $post_id, 'nano_team_members', true );
				foreach ( $team_members as $team_member ) {
					$user            = get_user_by( 'email', $team_member );
					$check_if_mailed = null !== get_user_meta( $user->ID, 'project_connection', true ) ? get_user_meta( $user->ID, 'project_connection', true ) : null;
					$check_if_mailed = is_array( $check_if_mailed ) ? $check_if_mailed : array();
					$project_ids     = $check_if_mailed;
					array_push( $project_ids, $post_id );

					$update_team_user = update_user_meta( $user->ID, 'project_connection', array_unique( $project_ids ) );

					$recipient = $user->user_email;
					if ( false !== $update_team_user ) {
						nano_email_notify( $post_id, $recipient, 'nanosteam_teammember' );
					}
				}
			}

			// Send Endorserment Invitation Emails on project save/submit/update
			if ( false !== $update_endorser ) {
				$project_endorsers = get_post_meta( $post_id, 'nano_endorsement_members', true );
				foreach ( $project_endorsers as $project_endorser ) {
					$user         = get_user_by( 'email', $project_endorser );
					$current_user = wp_get_current_user();
					$user_email   = $current_user->data->user_email;
					if ( $project_endorser != $user_email ) {
						$check_if_endorser_mailed = is_array( get_user_meta( $user->ID, 'project_endorsement_connection', true ) ) ? get_user_meta( $user->ID, 'project_endorsement_connection', true ) : array();
						$end_recipient            = $user->user_email;
						$project_ids              = $check_if_endorser_mailed;
						array_push( $project_ids, $post_id );

						$update_endorser_member = update_user_meta( $user->ID, 'project_endorsement_connection', array_unique( $project_ids ) );
						if ( false !== $update_endorser_member ) {
							nano_email_notify( $post_id, $end_recipient, 'nano_endorsement_member' );
						}
					}
				}
			}
		}

		if ( isset( $_GET['edit_project'] ) && $_GET['edit_project'] ) {
			delete_post_meta( $post_id, 'preview_post' );
			update_post_meta( $post_id, 'preview_post', true );
		}
	}
}


/**
 * Redirect if project has been saved using the modal
 */

function saved_project_redirect() {

	if ( isset( $_GET['edit_project'] ) ) {

		$post_ID = $_GET['edit_project'];

		$saved_post_redirect   = get_post_meta( $post_ID, 'save_continue', true );
		$preview_post_redirect = get_post_meta( $post_ID, 'preview_post', true );

		if ( '' != $saved_post_redirect ) {
			update_post_meta( $post_ID, 'save_continue', '' );
			wp_safe_redirect( $saved_post_redirect );
			exit;
		}

		if ( isset( $preview_post_redirect ) && true == $preview_post_redirect ) {
			update_post_meta( $post_ID, 'preview_post', false );
			$url = home_url() . '?post_type=ignition_product&p=' . $_GET['edit_project'] . '&preview=1';
			wp_safe_redirect( $url );
			exit;
		}
	}
}

add_action( 'wp_loaded', 'saved_project_redirect' );

function redirect_if_page_register() {

	if ( isset( $_GET['action'] ) && $_GET['action'] == 'register' ) {
		header( 'location: ' . $_SERVER['HTTP_REFERER'] );
	}

}

add_action( 'wp_loaded', 'redirect_if_page_register' );



/**
 * Redirect to preview anytime the preview project has been clicked (after saving post).
 *
 * @param int $post_id The post ID.
 * @param post $post The post object.
 * @param bool $update Whether this is an existing post being updated or not.
 */
function redirect_after_first_save( $post_id, $post, $update ) {
	/*
	* In production code, $slug should be set only once in the plugin,
	* preferably as a class property, rather than in each function that needs it.
	*/
	$post_type = get_post_type( $post_id );

	// If this isn't a 'ignition_product' post, don't update it.
	if ( 'ignition_product' != $post_type ) {
		return;
	}

	// - Redirect to the preview page.

	$url = home_url() . '?post_type=ignition_product&p=' . $post_id . '&preview=1';
	wp_safe_redirect( $url );


}

//add_action( 'save_post', 'redirect_after_first_save', 10, 3 );
//add_action( 'post_updated', 'check_values', 10, 3 ); //don't forget the last argument to allow all three arguments of the function


/**
 * Replace the popup purchase form with a proper modal style setup
 *
 * @param $project_id
 * @param null $the_deck
 */

function nano_idcf_level_select_lb( $project_id, $the_deck = null ) {
	ob_start();
	$project = new ID_Project( $project_id );
	global $pwyw;
	if ( 'default' == $the_deck->deck_type ) {
		$post_id = $the_deck->post_id;
		$image   = idc_checkout_image( $post_id );
		if ( isset( $the_deck->level_data ) ) {
			$level_data = $the_deck->level_data;
		} else {
			$level_data = null;
		}
		if ( ! empty( $level_data ) ) {

			$purchase_url = getPurchaseURLfromType( $project_id, 'purchaseform' );
			$action       = apply_filters( 'idcf_purchase_url', $purchase_url, $project_id );
			include NANO_STRIPE . '/library/idc_templates/_lbLevelSelect.php';
			$content = ob_get_contents();
			ob_end_flush();
			echo $content;
		}
	}

	return;
}

/**
 * Return the project's ID by the order level
 *
 * @param $order_level
 *
 * @return string|null
 */
function nano_project_id_by_order_level( $order_level ) {

	global $wpdb;

	$table_name  = $wpdb->prefix . 'mdid_assignments';
	$sql         = "SELECT * FROM $table_name WHERE level_id LIKE %s";
	$sql_prepare = $wpdb->prepare( $sql, $order_level );
	$project_id  = $wpdb->get_results( $sql_prepare );
	$project_id  = isset( $project_id[0] ) ? $project_id[0]->project_id : $order_level + 1;

	return $project_id;

}


/**
 * Return the project's ID by the order level
 *
 * @param $order_level
 *
 * @return string|null
 */
function nano_project_id_by_order_id_via_token($order_id) {

	global $wpdb;

	$table_name  = $wpdb->prefix . 'nano_token_meta_data';
	$sql         = "SELECT * FROM $table_name WHERE order_id LIKE %s";
	$sql_prepare = $wpdb->prepare( $sql, $order_id );
	$project_id  = $wpdb->get_results( $sql_prepare );
	//$project_id  = isset( $project_id[0] ) ? $project_id[0]->project_id : $order_id + 1;

	return $project_id;

}

/**
 * Return the project's post ID by the order level
 *
 * @param $order_level
 *
 * @return string|null
 */
function nano_post_id_by_order_level_id( $order_level ) {

	global $wpdb;

	$table_name  = $wpdb->prefix . 'mdid_assignments';
	$sql         = "SELECT * FROM $table_name WHERE level_id LIKE %s";
	$sql_prepare = $wpdb->prepare( $sql, $order_level );
	$project_id  = $wpdb->get_results( $sql_prepare );
	$project_id  = isset( $project_id[0] ) ? $project_id[0]->project_id : $order_level + 1;

	if ( !isset( $project_id[0] ) ) {
		send_error('Function: nano_post_id_by_order_level_id() was unable to properly located the post ID. ');
	}

	$idc_table_name      = $wpdb->prefix . 'postmeta';
	$sql_post_id         = "SELECT post_id FROM $idc_table_name WHERE meta_key = 'ign_project_id' AND meta_value = %d ORDER BY meta_id DESC LIMIT 1";
	$sql_post_id_prep    = $wpdb->prepare( $sql_post_id, $project_id );
	$sql_post_id_results = $wpdb->get_row( $sql_post_id_prep );

	$value = isset($sql_post_id_results->post_id) ? $sql_post_id_results->post_id : null;
	return $value;

}

/**
 * Add Matching Grant Sub Menu to IDK
 */

function nano_admin_settings() {

	// Setup and initialize the class for saving our options.
	$serializer = new Serializer();
	$serializer->init();

	// Setup the class used to retrieve our option value.
	$deserializer = new Deserializer();

	// Setup the administrative functionality.
	$admin = new Submenu( new Submenu_Page( $deserializer ) );
	$admin->init();

}



/**
 * After a project is approved we need to check in on it and then update the project's status
 * This fires once per day.
 * @param $post_id
 */

function nano_update_project_status_meta_data( $post_id ) {

	$date_posted                            = get_the_date( 'm/d/Y', $post_id );
	$date_expiration                        = get_post_meta( $post_id, 'ign_fund_end', true );
	$matching_grant_creator_paid_date_first = get_post_meta( $post_id, 'first_transfer_transaction_initiated', true );
	$matching_grant_creator_paid_date_final = get_post_meta( $post_id, 'final_transfer_transaction_date_initiated', true ) != '' ? ' / ' . get_post_meta( $post_id, 'final_transfer_transaction_date_initiated', true ) : '';
	$standard_creator_paid_date             = get_post_meta( $post_id, 'standard_transfer_transaction_date_initiated', true );
	$creator_paid_date                      = $standard_creator_paid_date . $matching_grant_creator_paid_date_first . $matching_grant_creator_paid_date_final;

	//Lab Notes
	$date_of_2nd_lab = '';
	$project_id      = get_post_meta( $post_id, 'ign_project_id', true );
	$args            = array(
		'post_type'      => 'ignition_update',
		'meta_key'       => 'idfu_project_update',
		'posts_per_page' => - 1,
		'meta_value'     => $project_id,
	);
	$posts           = get_posts( $args );

	// This is where we check to see if the creator has completed their second labnote if this is a matching grant projects
	if ( isset( $posts[1] ) ) {
		$date_of_2nd_lab = $posts[1];
		$date_of_2nd_lab = get_the_date( 'm/d/Y', $date_of_2nd_lab->ID );
	}

	//Funding Status
	$project_state = nano_funding_status( $post_id );

	global $wpdb;

	$charset_collate = $wpdb->get_charset_collate();
	$table_name      = $wpdb->prefix . 'nano_meta_data';

	$current_user = wp_get_current_user();

	$project_status = serialize(
		array(
			'funding_status'  => $project_state,  // Completed, Expired, Success, Live, or Reallocated
			'date_posted'     => $date_posted,
			'date_completed'  => $creator_paid_date,
			'date_of_2nd_lab' => $date_of_2nd_lab,
			'date_expiration' => $date_expiration,
		)
	);

	$project_uid = get_post_meta( $post_id, 'project_uid', true );

	// Updating the DB record
	$wpdb->query( $wpdb->prepare( "UPDATE $table_name
                SET project_status = %s
             WHERE project_uid = %s", $project_status, $project_uid )
	);

	if( $wpdb->last_error !== '') {

		$str   = htmlspecialchars( $wpdb->last_error, ENT_QUOTES );
		$query = htmlspecialchars( $wpdb->last_query, ENT_QUOTES );

		// If somehow there was a project created but the creator has not created a stripe connect account note it here.

		$subject = 'There was a problem updating the status of a project in the wp_nano_meta_data table.';
		$message = ( 'Project URL: ' . get_permalink( $post_id ) . "\n" );
		$message .= ( 'WPDB QUERY: ' . $query . "\n" );
		$message .= ( 'WPDB ERROR: ' . $str . "\n" );
		$message .= ( 'Error from function: nano_update_project_status_meta_data( $post_id )' . "\n" );

		email_general_admin_notification( $subject, $message );

	}

}

/**
 * After a project is approved we need to update it's payment status during its lifecycle
 *
 * @param $post_id
 */

function nano_update_payment_status_meta_data( $post_id ) {

	$project_uid = get_post_meta( $post_id, 'project_uid', true );
	$level_id    = nano_get_level_id( get_post_meta( $post_id, 'ign_project_id', true ) );
	$orders      = class_exists( 'ID_Member_Order' ) ? ID_Member_Order::get_orders_by_level( $level_id ) : '';
	$payments    = array();

	if ( isset( $orders ) ) {

		$count = 0;
		foreach ( $orders as $order ) {

			// Payment Method
			$payment_method = strpos( $order->transaction_id, 'nano' ) !== false ? 'cash' : 'no cash';

			// Donor Stripe ID
			$donor_stripe_id = get_user_meta( $order->user_id, 'stripe_customer_id', true );

			// Populate the array
			$payments[ $count ]['payment_id']      = $order->id;
			$payments[ $count ]['transaction_id']  = $order->transaction_id;
			$payments[ $count ]['payment_method']  = $payment_method;
			$payments[ $count ]['donor_id']        = $order->user_id;
			$payments[ $count ]['donor_stripe_id'] = $donor_stripe_id;
			$count ++;

		}

		// Put it all together.
		$payments_serialized = serialize( $payments );
		$project_uid         = get_post_meta( $post_id, 'project_uid', true );

		global $wpdb;

		$charset_collate = $wpdb->get_charset_collate();
		$table_name      = $wpdb->prefix . 'nano_meta_data';

		// Updating the DB record
		$wpdb->query( $wpdb->prepare( "UPDATE $table_name
                SET payments = %s
             WHERE project_uid = %s", $payments_serialized, $project_uid )
		);

		// Error reporting
		if( $wpdb->last_error !== '') {

			$str   = htmlspecialchars( $wpdb->last_error, ENT_QUOTES );
			$query = htmlspecialchars( $wpdb->last_query, ENT_QUOTES );

			$subject = 'There was a problem updating the payment status of a project in the wp_nano_meta_data table.';
			$message = ( 'Project URL: ' . get_permalink( $post_id ) . "\n" );
			$message .= ( 'WPDB QUERY: ' . $query . "\n" );
			$message .= ( 'WPDB ERROR: ' . $str . "\n" );
			$message .= ( 'Error from function: nano_update_payment_status_meta_data( $post_id )' . "\n" );

			email_general_admin_notification( $subject, $message );

		}

	}

}


/**
 * Need to check the state of the project. Whether it is LIVE, EXPIRED, SUCCESS, COMPLETED
 *
 * @param $post_id
 *
 * @return string
 */
function nano_funding_status( $post_id ) {

	// Funding Status
	$is_closed                = get_post_meta( $post_id, 'ign_project_closed', true ) == '1' ? true : false;
	$is_successful            = get_post_meta( $post_id, 'ign_project_success', true ) == '1' ? true : false;
	$is_refunded              = get_post_meta( $post_id, 'tokens_refunded', true ) == '1' ? true : false;
	$first_transfer           = get_post_meta( $post_id, 'first_transfer_transaction_id', true ) != '' ? true : false;
	$final_transfer           = get_post_meta( $post_id, 'final_transfer_transaction_id', true ) != '' ? true : false;
	$standard_transfer        = get_post_meta( $post_id, 'standard_transfer_transaction_id', true ) != '' ? true : false;
	$matching_transfer_status = ( false == $first_transfer && false == $final_transfer ) || ( true == $first_transfer && false == $final_transfer ) ? false : true;
	$transfer_status          = ( false == $matching_transfer_status && false == $standard_transfer ) ? false : true;

	// Updating the customer's stripe charge data

	$project_state = '';

	if ( false == $is_closed ) {
		$project_state = 'LIVE';
	}
	if ( ( false == $transfer_status && true == $is_successful && true == $is_closed ) ) {
		$project_state = 'SUCCESS';
	}
	if ( true == $transfer_status && true == $is_successful && true == $is_closed ) {
		$project_state = 'COMPLETED';
	}
	if ( false == $transfer_status && false == $is_successful && true == $is_closed && false == $is_refunded ) {
		$project_state = 'EXPIRED';
	}
	if ( true == $is_refunded ) {
		$project_state = 'REALLOCATED';
	}

	return $project_state;

}


/**
 * Check if the admin user is the correct user and whether the project is in the correct mode for that user to proceed.
 */
function nanosteam_privledge_check() {

	$privlege = false;
	$dev = false;

	$settings = get_option( 'memberdeck_gateways' );

	if ( ! empty( $settings ) ) {
		if ( is_array( $settings ) ) {
			$test = $settings['test'];
			if ( 1 == $test ) {
				$key = $settings['tsk'];
				$dev = true;
			} else {
				$key = $settings['sk'];
			}
		}
	}

	if ( false == $dev && get_current_user_id() == 2 && is_admin() ) {
		$privlege = true ;
	} else if ( true === $dev && current_user_can( 'administrator' ) && is_admin() ) {
		$privlege = true ;
	}

	return $privlege;

}

/**
 * Only for testing with Stripe
 *
 * @param $order
 */

function stripe_test_transfer( $order ) {

	$post_id = get_the_ID();

	$sc_settings = get_option( 'md_sc_settings' );
	$sc_settings = maybe_unserialize( $sc_settings );

	$total_before_fee = ID_Member_Order::get_order_meta( $order->id, 'pwyw_price' );
	$stripe_fee       = ( ID_Member_Order::get_order_meta( $order->id, 'stripe_fee', true ) ) / 100;
	$fee_percentage   = $sc_settings['app_fee'] / 100;
	$nanosteam_fee    = number_format( $total_before_fee * $fee_percentage, 2 );
	$total_fee        = $nanosteam_fee + $stripe_fee;
	$final_after_fee  = $total_before_fee - $total_fee;

	$settings = get_option( 'memberdeck_gateways' );

	if ( ! empty( $settings ) ) {
		if ( is_array( $settings ) ) {
			$test = $settings['test'];
			if ( 1 == $test ) {
				$key = $settings['tsk'];
			} else {
				$key = $settings['sk'];
			}
		}
	}

	if ( ! empty( $key ) ) {
		if ( ! class_exists( 'Stripe\Stripe' ) ) {
			require_once IDC_PATH . 'lib/stripe-php-4.2.0/init.php';
		}

		try {

			//Project Parameters
			$project_id     = get_post_meta( $post_id, 'project_uid', true );
			$ign_project_id = get_post_meta( $post_id, 'ign_project_id', true );
			$project_name   = nano_get_project_name( $ign_project_id );
			$project_url    = get_permalink( $post_id );

			// Creator Parameters
			$creator_id          = get_post_meta( $post_id, 'original_author', true );
			$creator_id          = false !== get_user_by( 'login', $creator_id ) ? get_user_by( 'login', $creator_id ) : get_user_by( 'email', $creator_id );
			$creator_id          = $creator_id->ID;
			$user_parameters     = nano_get_sc_params( $creator_id );
			$user_stripe_account = $user_parameters->id;

			// Platform Account Parameters
			$master_parameters     = get_master_sc_params();
			$master_stripe_account = $master_parameters->id;

			// Admin Payment (Deposit) Account
			$stripe_deposit_account = get_option( 'nano_deposit_account' );

			$message = 'Fee transfer from charge for project ID: ' . $project_id;

			// Set the API Version & Key
			\Stripe\Stripe::setApiVersion( NANO_STRIPE_API_VERSION );
			\Stripe\Stripe::setApiKey( $key );

			// Transfer the nanosteam FEE amount to Admin Payout's external ACCT.

			$fee_transfer = \Stripe\Transfer::create(
				array(
					'amount'      => $nanosteam_fee * 100,
					// * 100 for stripe to account properly. No decimals for them.
					'currency'    => 'usd',
					'destination' => $stripe_deposit_account,
					'description' => $message,
					'source_type' => 'bank_account',
					'metadata'    => array(
						'project_id'      => $project_id,
						'nano_creator_id' => $creator_id,
						'total_funded'    => $total_before_fee,
						'total_fee'       => $total_fee,
						'nanosteam_fee'   => $nanosteam_fee,
						'stripe_fee'      => $stripe_fee,
						'project_name'    => $project_name,
						'project_url'     => $project_url,
					),
				)
			);

			$fee_transfer = json_encode( $fee_transfer );
			$fee_transfer = json_decode( $fee_transfer );
			send_error( '$fee_transfer' );
			send_error( $fee_transfer );

		} catch ( \Stripe\Error\Card $e ) {
			// Since it's a decline, \Stripe\Error\Card will be caught
			send_error( $e );

		} catch ( \Stripe\Error\RateLimit $e ) {
			// Too many requests made to the API too quickly
			send_error( $e );

		} catch ( \Stripe\Error\InvalidRequest $e ) {
			// Invalid parameters were supplied to Stripe's API
			send_error( $e );

		} catch ( \Stripe\Error\Authentication $e ) {
			// Authentication with Stripe's API failed
			// (maybe you changed API keys recently)
			send_error( $e );

		} catch ( \Stripe\Error\ApiConnection $e ) {
			// Network communication with Stripe failed
			send_error( $e );

		} catch ( \Stripe\Error\Base $e ) {
			// Display a very generic error to the user, and maybe send
			// yourself an email
			send_error( $e );

		} catch ( Exception $e ) {
			// Something else happened, completely unrelated to Stripe
			send_error( $e );
		}
	}
}


/**
 * Generate a local time stamp instead of UTC.
 *
 * @param $format
 *
 * @return false|string
 */

function nano_get_formated_date_time_utc( $format ) {

	if ( '' == $format ) {
		$format = get_option( 'date_format' ) . ' ' . get_option( 'time_format' );
	}

	$date_utc = new \DateTime("now", new \DateTimeZone("UTC"));

	return $date_utc->format( $format );

}

function nano_get_formated_date_time_gmt( $format ) {

	$timestamp_gmt = current_time( 'timestamp', $gmt = 1 );

	if ( '' == $format ) {
		$format = get_option( 'date_format' ) . ' ' . get_option( 'time_format' );
	}

	$s = gmdate( 'Y-m-d H:i:s', $timestamp_gmt );
	$s = get_date_from_gmt( $s, $format );

	return $s;

}



function nano_orders_raised( $post_id ) {

	$project_id = get_post_meta( $post_id, 'ign_project_id', true );
	$level_id = nano_get_level_id( $project_id );
	$orders = ID_Member_Order::get_orders_by_level( $level_id );

	$sub_total = 0;
	foreach ( $orders as $order ) {

		if ( 0 == $order->price ) {

			$sub_total += idc_get_order_meta( $order->id, 'pwyw_price', true );

		} else {

			$sub_total += $order->price;

		}

	}

	if ( is_numeric( $sub_total ) && floor( $sub_total ) != $sub_total && $sub_total < 100 ) {

		$total = number_format( $sub_total, '2', '.', ',' );

	} else {

		$total = number_format( $sub_total, '0', '.', ',' );

	}

	return $total;

}

function nano_percent_raised( $post_id ) {

	$project_id = get_post_meta( $post_id, 'ign_project_id', true );
	$level_id = nano_get_level_id( $project_id );
	$orders = ID_Member_Order::get_orders_by_level( $level_id );

	$total = 0;
	foreach ( $orders as $order ) {

		if ( 0 == $order->price ) {

			$total += idc_get_order_meta( $order->id, 'pwyw_price', true );

		} else {

			$total += $order->price;

		}

	}

	$project_goal = get_post_meta( $post_id, 'ign_fund_goal', true );

	if ( $project_goal > 0 ) {

		$percent      = ( $total / $project_goal ) * 100;

		$percent = round_special( $percent );

		if ( is_numeric( $percent ) && floor( $percent ) != $percent && $percent < 10 ) {

			$total = number_format( $percent, '1', '.', '' );

		} else {

			$total = number_format( $percent, '0', '.', '' );

		}

	}

	return $total;


}

function round_special( $x ) {
	if ( $x == 0 ) {
		return 0;
	}

	$rounded  = round( $x, 2 );
	$minValue = .1;

	if ( $rounded < $minValue ) {
		return number_format( $minValue, 1, '.', '' );
	} else {
		return number_format( $rounded, 1,  '.', '' );
	}

}



/**
 * Only used once but keeping for reference.
 * This scans every IDK project and applies a thumbnail to the project admin if it is not already there.
 */
function update_admin_thumbnails() {

	$args = array(
		'post_type'      => 'ignition_product',
		'post_status'    => array('publish', 'pending', 'draft'),
		'posts_per_page' => - 1,
	);

	$query = new WP_Query( $args );

	if ( $query->have_posts() ) {

		while ( $query->have_posts() ) {

			$query->the_post();
			$post_id = get_the_id();

			// Adding thumbnail if missing to admin
			$thumb_check = get_post_meta( $post_id, 'project_main', true );
			$thumb_meta  = get_post_meta( $post_id, '_thumbnail_id', true );

			if ( '' !== $thumb_check && '' == $thumb_meta ) {
				set_post_thumbnail( $post_id, $thumb_check );
			}
		}
	}

}

/**
 * Setting up a daily cron to go through current projects and update their table entries.
 */

function update_metadata_cron() {

	send_error('daily meta cron has fired');

	$args = array(
		'post_type'      => 'ignition_product',
		'post_status'    => 'publish',
		'posts_per_page' => - 1,
	);

	$query = new WP_Query( $args );

	if ( $query->have_posts() ) {

		while ( $query->have_posts() ) {
			$query->the_post();
			$post_id = get_the_id();

			$project_closed_date    = get_post_meta( $post_id, 'ign_fund_end', true );
			$todays_date            = nano_get_formated_date_time_gmt( 'Y-m-d' );
			$project_success_status = get_post_meta( $post_id, 'ign_project_success', true ) == '1' ? true : false;
			$time1                  = strtotime( $project_closed_date . '+15 days' );
			$time2                  = strtotime( $todays_date );

			if ( $time2 < $time1 ) {
				nano_update_payment_status_meta_data( $post_id );
				nano_update_project_status_meta_data( $post_id );
			}

			if ( $project_success_status === true ) {
				payout_problem_alert( $post_id );
			}
		}
	}

	// Check to see if tokens are about to expire within 5 days and if so send out an email reminder
	nano_tokens_refund_reminder();

	// Once a project has achieved its second milstone send an email to the admin every day until it has been paid
	nano_admin_notify_macthing_grant_milestone();

	// After a backer has supported a project we initiate a series of follow up emails
	nano_donor_engagement_after_first_donation();


}


/**
 * Disabling quick edit for Projects as we need projects to be fully reviewed before publishing is allowed.
 *
 * @param $actions
 *
 * @return mixed
 */

function nano_remove_quick_edit( $actions ) {
	$current_screen = get_current_screen();
	if ( $current_screen->id === 'edit-ignition_product' ) {
		unset( $actions['inline hide-if-no-js'] );
	}
	return $actions;
}
add_filter('post_row_actions','nano_remove_quick_edit',10,1);


$settings = get_option( 'memberdeck_gateways' );

// If in development mode lets run these functions continuously
if ( ! empty( $settings ) ) {
	if ( is_array( $settings ) ) {
		$test = $settings['test'];
		if ( 1 == $test ) {
			//add_action( 'init', 'update_metadata_cron' );
		}
	}
}

// Run the metadata for all projects daily
if ( ! wp_next_scheduled( 'nano_cron_metadata_daily_hook' ) ) {
	wp_schedule_event( time(), 'daily', 'nano_cron_metadata_daily_hook' );
}

// Run the metadata for all projects weekly
if ( ! wp_next_scheduled( 'nano_cron_metadata_weekly_hook' ) ) {
	wp_schedule_event( time(), 'weekly', 'nano_cron_metadata_weekly_hook' );
}

// Run the metadata for all projects every 30 minutes
if ( ! wp_next_scheduled( 'nano_cron_30min_hook' ) ) {
	wp_schedule_event( time(), '30min', 'nano_cron_30min_hook' );
}

add_action( 'nano_cron_metadata_daily_hook', 'update_metadata_cron' );
add_action( 'nano_cron_hook', 'update_google_analtics_cron' );
add_action( 'nano_cron_hook', 'update_30min_metadata_cron' );
add_action( 'nano_cron_metadata_weekly_hook', 'update_weekly_metadata_cron' );


/**
 * Cron jobs (Weekly)
 */

function update_weekly_metadata_cron() {
	nano_matching_grant_2nd_milestone_email_reminder();
}

/**
 * Cron jobs (30 Min)
 */

function update_30min_metadata_cron() {
	// Update credit refund orders with their stripe fee & parent stripe transaction ID
	nano_token_tracking_update();
}

function payout_problem_alert( $post_id ) {

	$alert = array();

	// Payout Status
	$project_success_status       = get_post_meta( $post_id, 'ign_project_success', true ) == '1' ? true : false;

	if ( $project_success_status === true ) {

		$matching_grant_project_check = get_post_meta( $post_id, 'fifty_fifty_grant', true ) == 'approved' ? true : false;
		$second_milestone_check       = nano_second_milsetone_achieved( $post_id );

		// Project Payment Statuses
		// Matching Grant
		$first_transfer_processed_id = get_post_meta( $post_id, 'first_transfer_transaction_id', true );
		$first_transfer_initiated    = get_post_meta( $post_id, 'first_transfer_transaction_initiated', true );
		$first_transfer_delivered    = get_post_meta( $post_id, 'first_transfer_transaction_delivered', true );

		$final_transfer_processed_id = get_post_meta( $post_id, 'final_transfer_transaction_id', true );
		$final_transfer_initiated    = get_post_meta( $post_id, 'final_transfer_transaction_date_initiated', true );
		$final_transfer_delivered    = get_post_meta( $post_id, 'final_transfer_transaction_date_delivered', true );

		// All In
		$standard_transfer_processed_id = get_post_meta( $post_id, 'standard_transfer_transaction_id', true );
		$standard_transfer_initiated    = get_post_meta( $post_id, 'standard_transfer_transaction_date_initiated', true );
		$standard_transfer_delivered    = get_post_meta( $post_id, 'standard_transfer_transaction_date_delivered', true );

		$overdue_date = false;
		$type = false;

		if ( false == $matching_grant_project_check ) {

			if ( '' !== $standard_transfer_processed_id && 'PENDING' === $standard_transfer_delivered ) {

				// Check if it has been longer than 7 days.
				$overdue_date = strtotime( $standard_transfer_initiated ) < strtotime( '-7 day' ) ? true : false;
				$type = 'STANDARD';

			}

		} else if ( true == $matching_grant_project_check ) {

			if ( '' !== $first_transfer_processed_id && 'PENDING' === $first_transfer_delivered && '' == $final_transfer_processed_id ) {

				// Check if it has been longer than 7 days.
				$overdue_date = strtotime( $first_transfer_initiated ) < strtotime( '-7 day' ) ? true : false;
				$type = 'FIRST';

			} else if ( '' !== $first_transfer_processed_id && '' !== $final_transfer_processed_id && 'PENDING' === $final_transfer_delivered ) {

				// Check if it has been longer than 7 days.
				$overdue_date = strtotime( $final_transfer_initiated ) < strtotime( '-7 day' ) ? true : false;
				$type = 'FINAL';

			}

		}

		if ( $overdue_date === true ) {

			$message = '<p style="color:red">This project\'s payout status is 7 days past the day when it was initiated</p>';

			if ( '' === get_post_meta( $post_id, 'payout_alert', true ) ) {

				$message = '<p style="color:red">The project: ' . get_the_title( $post_id ) . ' payout status is 7 days past the day when it was initiated.</p> ' . "\n";
				$message .= '<p>Please goto: ' . get_permalink( $post_id ) . '</p>' . "\n";
				$message .= '<p>This is also noted in Payout Reports.</p>' . "\n";
				$type    = 'overdue_payout';
				stripe_error_email_admin_notification( $message, $type );
			}

			add_post_meta( $post_id, 'payout_alert', '1' );

			$alert['message'] = $message;
			$alert['type'] = $type;

		}

	}

	return $alert;

}

function get_original_author_by_project_level_id( $level_id ) {

	global $wpdb;

	$table_name      = $wpdb->prefix . 'mdid_assignments';
	$sql             = "SELECT * FROM $table_name WHERE level_id LIKE %s";
	$prepare         = $wpdb->prepare( $sql, $level_id );
	$results         = $wpdb->get_results( $prepare );
	$project_id      = isset ( $results[0] ) ? $results[0]->project_id : '';
	$project         = new ID_Project( $project_id );
	$post_id         = $project->get_project_postid();
	$original_author = get_post_meta( $post_id, 'original_author', true );

	if ( '' === $original_author || FALSE === $original_author ) {
		$original_author_id = get_post_field( 'post_author', $post_id );
		$original_author    = get_user_by( 'id', $original_author_id );
		$original_author    = $original_author->user_login;
	}
	return $original_author;

}

function nano_get_ign_project( $id ) {
	global $wpdb;
	$proj_query = $wpdb->prepare('SELECT * FROM '.$wpdb->prefix.'ign_products WHERE id=%d', absint($id));
	$proj_return = $wpdb->get_row($proj_query);
	return $proj_return;
}

function nano_get_project_name( $project_id ) {

	// use the project id to get our level data
	$level_id = nano_get_level_id( $project_id );

	$return = ID_Member_Level::get_level( $level_id );
	// we have that data, lets store it in vars
	$level_name = isset ( $return->level_name ) ? $return->level_name : '';

	return $level_name;

}

function nano_get_level_id( $project_id ) {

	// Getting the level ID of the project by its project ID.
	global $wpdb;
	$table_name       = $wpdb->prefix . 'mdid_assignments';
	$sql              = "SELECT * FROM $table_name WHERE project_id LIKE %s";
	$sql_prepare      = $wpdb->prepare( $sql, $project_id );
	$project_level_id = $wpdb->get_results( $sql_prepare );
	$project_level_id = isset( $project_level_id[0] ) ? $project_level_id[0]->level_id : null;

	return $project_level_id;

}

function nano_get_project_id( $level_id ) {

	// Getting the level ID of the project by its project ID.
	global $wpdb;
	$table_name       = $wpdb->prefix . 'mdid_assignments';
	$sql              = "SELECT * FROM $table_name WHERE level_id LIKE %s";
	$sql_prepare      = $wpdb->prepare( $sql, $level_id );
	$project_id = $wpdb->get_results( $sql_prepare );
	$project_id = isset( $project_id[0] ) ? $project_id[0]->project_id : null;

	return $project_id;

}

function nano_get_memberdeck_level_duplicates_by_title($value) {
	global $wpdb;
	// Duplicate Memberdeck Table Titles
	$memberdeck_table   = $wpdb->prefix . 'memberdeck_levels';
	$memberdeck_sql     = "SELECT * FROM $memberdeck_table WHERE level_name LIKE %s";
	$memberdeck_val     = $value;
	$memberdeck_prepare = $wpdb->prepare( $memberdeck_sql, $memberdeck_val );
	$memberdeck_results = $wpdb->get_results( $memberdeck_prepare );

	return $memberdeck_results;
}


/**
 * Adding live switch to the instant checkout checkbox on the checkout page
 */

function ajax_instant_checkout() {

	// First check the nonce, if it fails the function will break
	check_ajax_referer( 'nano_instant_checkout_secure', 'security' );

	if ( is_user_logged_in() ) {
		$current_user     = wp_get_current_user();
		$user_id          = $current_user->ID;
		$instant_checkout = instant_checkout();
		if ( 'on' == $_POST['instant_checkout'] ) {
			$instant_checkout = '1';
			echo json_encode(
				array(
					'loggedin' => false,
					'message'  => __( 'Successfully invited' ),
				)
			);
		} elseif ( 'off' == $_POST['instant_checkout'] ) {
			$instant_checkout = '0';
			echo json_encode(
				array(
					'loggedin' => false,
					'message'  => __( 'Done' ),
				)
			);
		}
		update_user_meta( $user_id, 'instant_checkout', $instant_checkout );
	}

	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		wp_die();
	}

}



/**
 * Duplicates a post & its meta and it returns the new duplicated Post ID
 * @param  [int] $post_id The Post you want to clone
 * @return [int] The duplicated Post ID
 */
function nano_duplicate_post_test($post_id) {

	global $wpdb;

	$oldpost = get_post( $post_id );

	$args = array(
		'comment_status' => $oldpost->comment_status,
		'ping_status'    => $oldpost->ping_status,
		'post_author'    => $oldpost->post_author,
		'post_content'   => $oldpost->post_content,
		'post_excerpt'   => $oldpost->post_excerpt,
		'post_name'      => $oldpost->post_name,
		'post_parent'    => $oldpost->post_parent,
		'post_password'  => $oldpost->post_password,
		'post_status'    => 'draft',
		'post_title'     => $oldpost->post_title,
		'post_type'      => $oldpost->post_type,
		'to_ping'        => $oldpost->to_ping,
		'menu_order'     => $oldpost->menu_order,
	);

	/*
	 * insert the post by wp_insert_post() function
	 */
	$new_post_id = wp_insert_post( $args );


	/*
	 * get all current post terms ad set them to the new post draft
	 */
	$taxonomies = get_object_taxonomies( $oldpost->post_type ); // returns array of taxonomy names for post type, ex array("category", "post_tag");
	foreach ( $taxonomies as $taxonomy ) {
		$post_terms = wp_get_object_terms( $post_id, $taxonomy, array( 'fields' => 'slugs' ) );
		wp_set_object_terms( $new_post_id, $post_terms, $taxonomy, false );
		send_error('wp_set_object_terms');
		send_error($new_post_id);
		send_error($post_terms);
		send_error($taxonomy);
	}

	/*
	 * duplicate all post meta just in two SQL queries
	 */
	$post_meta_infos = $wpdb->get_results( "SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id" );
	if ( count( $post_meta_infos ) != 0 ) {
		$sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
		foreach ( $post_meta_infos as $meta_info ) {
			$meta_key = $meta_info->meta_key;
			if ( $meta_key == '_wp_old_slug' ) {
				continue;
			}
			$meta_value      = addslashes( $meta_info->meta_value );
			$sql_query_sel[] = "SELECT $new_post_id, '$meta_key', '$meta_value'";
		}
		$sql_query .= implode( " UNION ALL ", $sql_query_sel );

		send_error('$sql_query_sel');

		$wpdb->query( $sql_query );
	}

	// return $new_post_id;

}

/**
 * Checking the project title as the user saves their post and prevents duplicates.
 * This should also resolve any auto duplicates from occuring as well.
 */

add_filter( 'wp_insert_post_data', 'duplicate_title_check', '99', 2 );

function duplicate_title_check( $data , $postarr ) {

	if ( 'ignition_product' === $data['post_type'] ) {

		// First check the nonce, if it fails the function will break

		$new_project_name = sanitize_text_field( $postarr['post_title'] );
		$new_project_name = str_replace( array( ':', ' ', '.', "'" ), '', strtolower( $new_project_name ) );

		// If this is just a previously renamed project do not do any checks until the user changes the project's name.
		if ( $new_project_name === 'pleasechooseanothertitle' || ( isset( $data['post_type'] ) && 'nav_menu_item' === $data['post_type'] ) ) {
			return $data;
		}

		// Current user
		$current_user_id = $data['post_author'];

		// Project Details
		$this_post_id             = $postarr['ID'];
		$this_post_published_date = new DateTime( $data['post_date'] );
		//$this_post_published_date = new DateTime( $data['post_modified'] );

		global $wpdb;

		$custom_post_type     = 'ignition_product'; // define your custom post type slug here
		$custom_post_status_a = 'pending';
		$custom_post_status_b = 'publish';
		$custom_post_status_c = 'draft';

		// A sql query to return all post titles
		$prepare = $wpdb->prepare( "SELECT ID, post_title FROM {$wpdb->posts} WHERE post_status = %s OR post_status = %s OR post_status = %s AND post_type = %s", $custom_post_status_a, $custom_post_status_b, $custom_post_status_c, $custom_post_type );
		$results = $wpdb->get_results( $prepare, ARRAY_A );

		// Redo the results array but changing the title to lower case and removing spaces.
		$output = array();

		if ( isset( $results ) & is_array( $results ) & 1 < count( $results ) ) {
			foreach ( $results as $index => $post ) {

				$projects_name = sanitize_text_field( $post['post_title'] );
				$projects_name = str_replace( array( ':', ' ', '.', "'" ), '', strtolower( $projects_name ) );

				$output[ $index ]['post_title'] = $projects_name;
				$output[ $index ]['ID']         = $post['ID'];
			}
		}

		$check_for_titles = multi_dimen_array_search_all_keys( $output, 'post_title', $new_project_name );
		$count_titles     = is_array( $check_for_titles ) ? count( $check_for_titles ) : '';

		if ( is_array( $check_for_titles ) ) {

			// For some reason there are more than 1 project with the same title
			if ( 1 < $count_titles ) {

				foreach ( $check_for_titles as $key => $title ) {

					// The first key element always wins and we'll pass this by, any others are duplicates and must be changed.
					if ( 0 < $key ) {

						$post_id = $title['ID'];

						$this_post_author_id = $data['post_author'];
						$post_author_id      = get_post_field( 'post_author', $post_id );
						$post_type_is        = get_post_type( $post_id );

						// make sure that this is the authors project to change.
						if ( $this_post_author_id == $post_author_id && 'ignition_product' === $post_type_is ) {

							// Check if this is just a save of the same post but the title is identical to an earlier post.
							if ( $post_id == $this_post_id ) {

								$data['post_title'] = 'Please choose another title';
								$data['post_name']  = 'please-choose-another-title-' . $this_post_id;


							} else {

								// Retrieving the interval difference between the post date of this post and the original post
								$post_published_date = new DateTime( get_post_time( 'Y-m-d H:i:s', false, $post_id, false ) );
								$interval_days       = $this_post_published_date->diff( $post_published_date )->format( '%a' );
								$interval_hours      = $this_post_published_date->diff( $post_published_date )->format( '%h' );
								$interval_minutes    = $this_post_published_date->diff( $post_published_date )->format( '%i' );
								$interval_seconds    = $this_post_published_date->diff( $post_published_date )->format( '%s' );
								$interval_total      = ( $interval_days * 24 * 60 * 60 ) + ( $interval_hours * 60 * 60 ) + ( $interval_minutes * 60 ) + $interval_seconds;

								// If the project was created more than 10 seconds ago proceed
								if ( 10 < $interval_total ) {

									$data['post_title'] = 'Please choose another title';
									$data['post_name']  = 'please-choose-another-title-' . $this_post_id;

								} elseif ( 10 > $interval_total ) {

									send_error( '08 - AUTO DUPE' );
									send_error( $title );
									// the project was created automatically. Time to send this one to the admin.
									// The time this post was created was less and is an auto DUPE. Time to change this over to the admin
									$data['post_author'] = get_user_by( 'email', get_option( 'admin_email' ) )->ID;
									$data['post_title']  = 'THIS IS AN AUTO DUPE OF ' . $data['post_title'];
									$data['post_name']   = 'this-is-a-dupe-of-' . $data['post_name'];

									//Changing the posts orginal author meta - this is important.
									update_post_meta( $this_post_id, 'original_author', get_user_by( 'email', get_option( 'admin_email' ) )->user_login );

								}
							}
						}
					}
				}

			} else {

				send_error( '09' );

				$post_id = $check_for_titles[0]['ID'];

				// If the post id of the found duplicate does not match we will change the post name but leave the post with
				// the project creator if the interval of this post is less that 5 seconds then we will name this a duplicate post
				// and change the author to site administrator.
				if ( $post_id != $this_post_id ) {

					// Retrieving the interval difference between the post date of this post and the original post
					$post_published_date = new DateTime( get_post_time( 'Y-m-d H:i:s', false, $post_id, false ) );
					$interval_days       = $this_post_published_date->diff( $post_published_date )->format( '%a' );
					$interval_hours      = $this_post_published_date->diff( $post_published_date )->format( '%h' );
					$interval_minutes    = $this_post_published_date->diff( $post_published_date )->format( '%i' );
					$interval_seconds    = $this_post_published_date->diff( $post_published_date )->format( '%s' );
					$interval_total      = ( $interval_days * 24 * 60 * 60 ) + ( $interval_hours * 60 * 60 ) + ( $interval_minutes * 60 ) + $interval_seconds;

					// If the project was created more than 5 seconds ago proceed
					if ( 10 < $interval_total ) {

						$data['post_title'] = 'Please choose another title';
						$data['post_name']  = 'please-choose-another-title-' . $this_post_id;

					} elseif ( 10 > $interval_total ) {

						send_error( '12 - AUTO DUPE' );
						// the project was created automatically. Time to send this one to the admin.

						// The time this post was created was less and is an auto DUPE. Time to change this over to the admin
						$data['post_author'] = get_user_by( 'email', get_option( 'admin_email' ) )->ID;
						$data['post_title']  = 'THIS IS AN AUTO DUPE OF ' . $data['post_title'];
						$data['post_name']   = 'this-is-a-dupe-of-' . $data['post_name'];

						//Changing the posts orginal author meta - this is important.
						update_post_meta( $this_post_id, 'original_author', get_user_by( 'email', get_option( 'admin_email' ) )->user_login );


					}

				}

			}

		}

	}

	return $data;

}

/**
 * Syncronizing Level IDs with Project Ids.
 */

function nano_level_crosscheck() {

	// Getting all projects, levels and associations
	// Projects
	$projects = ID_Project::get_all_projects();
	// Levels
	$levels = ID_Member_Level::get_levels();
	// Associations
	global $wpdb;
	$asssociations_table = $wpdb->prefix . 'mdid_assignments';
	$asssociations_sql   = "SELECT * FROM $asssociations_table";
	$asssociations_vals  = $wpdb->get_results( $asssociations_sql );

	cross_check_projects($wpdb, $projects, $levels, $asssociations_table, $asssociations_vals);
	cross_check_levels($wpdb, $projects, $levels, $asssociations_table, $asssociations_vals);
	cross_check_duplicates($wpdb, $projects, $levels, $asssociations_table, $asssociations_vals);

}

function cross_check_projects($wpdb, $projects, $levels, $asssociations_table, $asssociations_vals) {
	// Need to go through each project by ID first and check again memebrdeck level ID
	foreach ( $projects as $project ) {

		$product_title = '' != $project->ign_product_title ? $project->ign_product_title : '';
		$project_name  = '' != $project->product_name ? $project->product_name : ( '' != $product_title ? $product_title : '' );
		$project_name  = str_replace( array( ':', ' ', '.', "'" ), '', $project_name );
		$project_id    = $project->id;

		foreach ( $levels as $key => $level ) {

			$level_name       = $level->level_name;
			$level_name_mysql = $level_name;
			$level_name       = str_replace( array( ':', ' ', '.', "'" ), '', $level_name );
			$level_id         = $level->id;

			//Checking if this is also a duplicate
			$duplicate_levels = multi_dimen_array_search_all_keys( $asssociations_vals, 'level_id', $level_id );
			$duplicate_count  = is_array( $duplicate_levels ) ? count( $duplicate_levels ) : 0;

			// Checking to see if there are any memberdeck table entries which have duplicate titles
			// always returns array

			if ( $project_name === $level_name && '' !== $project_name && '' !== $level_name && 1 === $duplicate_count ) {

				// Associations Table
				$md_asssociations_table = $wpdb->prefix . 'mdid_assignments';
				$mdid_sql         = "SELECT * FROM $md_asssociations_table WHERE project_id LIKE %d";
				$mdid_val_id      = $project_id;
				$mdid_sql_prepare = $wpdb->prepare( $mdid_sql, $mdid_val_id );
				$mdid_results     = $wpdb->get_results( $mdid_sql_prepare );

				$mdlvl_sql         = "SELECT * FROM $md_asssociations_table WHERE level_id LIKE %d";
				$mdlvl_val_id      = $level_id;
				$mdlvl_sql_prepare = $wpdb->prepare( $mdlvl_sql, $mdlvl_val_id );
				$mdlvl_results     = $wpdb->get_results( $mdlvl_sql_prepare );

				$project_name_check = '';
				if ( isset($mdlvl_results[0]) ) {
					$project_name_check_id = $mdlvl_results[0]->project_id;
					$project_name_check_isd = nano_get_ign_project($project_name_check_id);
					$product_title_check = '' != $project_name_check_isd->ign_product_title ? $project_name_check_isd->ign_product_title : '';
					$project_name_check  = '' != $project_name_check_isd->product_name ? $project_name_check_isd->product_name : ( '' != $product_title_check ? $product_title_check : '' );
					$project_name_check  = str_replace( array( ':', ' ', '.', "'" ), '', $project_name_check );
				}

				if (  isset( $mdid_results[0] ) && ( $project_name !== $project_name_check ) &&  ( $project_id !== $mdid_results[0]->project_id || $level_id !== $mdid_results[0]->level_id ) ) {

					$md_id_level_name = ID_Member_Level::get_level( $mdid_results[0]->level_id );
					$md_id_level_name = str_replace( array( ':', ' ', '.', "'" ), '', $md_id_level_name->level_name );
					$md_id_level_id   = $md_id_level_name;

					if ( $project_name !== $md_id_level_name ) {

						$assignments_sql = $wpdb->prepare( "UPDATE $asssociations_table SET level_id = %d WHERE project_id = %d", $level_id, $project_id );
						$assignments_res = $wpdb->query( $assignments_sql );

					}
				}
			}
		}
	}
}

function cross_check_levels($wpdb, $projects, $levels, $asssociations_table, $asssociations_vals) {
	// Need to go through each project by ID first and check again memebrdeck level ID
	// Now that we've done our first association fix with the projects. We'll go through the levels and do the same against the projects.
	foreach ( $levels as $key => $level ) {

		$level_name       = $level->level_name;
		$level_name_mysql = $level_name;
		$level_name       = str_replace( array( ':', ' ', '.', "'" ), '', $level_name );
		$level_id         = $level->id;

		//Checking if this is also a duplicate
		$duplicate_levels = multi_dimen_array_search_all_keys( $asssociations_vals, 'level_id', $level_id );
		$duplicate_count  = is_array( $duplicate_levels ) ? count( $duplicate_levels ) : 0;

		// Checking to see if there are any memberdeck table entries which have duplicate titles
		// always returns array
		$memberdeck_duplicates      = nano_get_memberdeck_level_duplicates_by_title( $level_name_mysql );
		$memberdeck_duplicate_count = is_array( $memberdeck_duplicates ) ? count( $memberdeck_duplicates ) : 0;


		if ( 1 < $memberdeck_duplicate_count && 1 < $duplicate_count ) {

			foreach ( $memberdeck_duplicates as $dupe_key => $memberdeck_duplicate ) {

				if ( '' !== $memberdeck_duplicate->level_name ) {

					$member_deck_level_id = $memberdeck_duplicate->id;

					$project_key = 0;

					foreach ( $projects as $temp_key => $project ) {

						$product_title = '' != $project->ign_product_title ? $project->ign_product_title : '';
						$project_name  = '' != $project->product_name ? $project->product_name : ( '' != $product_title ? $product_title : '' );
						$project_name  = str_replace( array( ':', ' ', '.', "'" ), '', $project_name );
						$project_id    = $project->id;

						if ( $project_name === $level_name && '' !== $project_name && '' !== $level_name ) {

							// Associations Table
							$mdid_sql         = "SELECT * FROM $asssociations_table WHERE project_id LIKE %d";
							$mdid_val_id      = $project_id;
							$mdid_sql_prepare = $wpdb->prepare( $mdid_sql, $mdid_val_id );
							$mdid_results     = $wpdb->get_results( $mdid_sql_prepare );

							if ( isset( $mdid_results[0] ) ) {

								$member_deck_level_id = $memberdeck_duplicates[$project_key]->id;

								//view_var( 'MATCHED: ' . $project_name );
								//view_var( 'UPDATE PROJECT ID:' . $project_id . ' WITH LEVEL ID:' . $member_deck_level_id );
								//view_var($project_name);
								$assignments_sql = $wpdb->prepare( "UPDATE $asssociations_table SET level_id = %d WHERE project_id = %d", $member_deck_level_id, $project_id );
								$assignments_res = $wpdb->query( $assignments_sql );
								//view_var($assignments_res);

								$project_key ++;

							}
						}
					}
				}
			}
		}
	}
}

function cross_check_duplicates($wpdb, $projects, $levels, $asssociations_table, $asssociations_vals) {
	// Need to go through each project by ID first and check again memebrdeck level ID
	// Now that we've done our first association fix with the projects. We'll go through the levels and do the same against the projects.
	foreach ( $levels as $key => $level ) {

		$level_name       = $level->level_name;
		$level_name_mysql = $level_name;
		$level_name       = str_replace( array( ':', ' ', '.', "'" ), '', $level_name );
		$level_id         = $level->id;

		//Checking if this is also a duplicate
		$duplicate_levels = multi_dimen_array_search_all_keys( $asssociations_vals, 'level_id', $level_id );
		$duplicate_count  = is_array( $duplicate_levels ) ? count( $duplicate_levels ) : 0;

		// Checking to see if there are any memberdeck table entries which have duplicate titles
		// always returns array
		$memberdeck_duplicates      = nano_get_memberdeck_level_duplicates_by_title( $level_name_mysql );
		$memberdeck_duplicate_count = is_array( $memberdeck_duplicates ) ? count( $memberdeck_duplicates ) : 0;


		if ( 1 < $memberdeck_duplicate_count && 1 < $duplicate_count ) {

			foreach ( $memberdeck_duplicates as $dupe_key => $memberdeck_duplicate ) {

				if ( '' !== $memberdeck_duplicate->level_name ) {

					$member_deck_level_id = $memberdeck_duplicate->id;

					$project_key = 0;

					foreach ( $projects as $temp_key => $project ) {

						$product_title = '' != $project->ign_product_title ? $project->ign_product_title : '';
						$project_name  = '' != $project->product_name ? $project->product_name : ( '' != $product_title ? $product_title : '' );
						$project_name  = str_replace( array( ':', ' ', '.', "'" ), '', $project_name );
						$project_id    = $project->id;

						if ( $project_name === $level_name && '' !== $project_name && '' !== $level_name ) {

							// Associations Table
							$mdid_sql         = "SELECT * FROM $asssociations_table WHERE project_id LIKE %d";
							$mdid_val_id      = $project_id;
							$mdid_sql_prepare = $wpdb->prepare( $mdid_sql, $mdid_val_id );
							$mdid_results     = $wpdb->get_results( $mdid_sql_prepare );

							if ( isset( $mdid_results[0] ) ) {

								$member_deck_level_id = $memberdeck_duplicates[$project_key]->id;

								//view_var( 'MATCHED: ' . $project_name );
								//view_var( 'UPDATE PROJECT ID:' . $project_id . ' WITH LEVEL ID:' . $member_deck_level_id );
								//view_var($project_name);
								$assignments_sql = $wpdb->prepare( "UPDATE $asssociations_table SET level_id = %d WHERE project_id = %d", $member_deck_level_id, $project_id );
								$assignments_res = $wpdb->query( $assignments_sql );
								//view_var($assignments_res);

								$project_key ++;

							}
						}
					}
				}
			}
		}
	}
}


/**
 * returns the fee breakdown for a project in an array
 *
 * @param $stripe_fee
 * @param $total_donations
 *
 * @return array
 */
function project_fees( $stripe_fee, $total_donations ) {

	$stripe_fee = '' !== $stripe_fee ? $stripe_fee : '0';
	$stripe_fee = number_format( $stripe_fee, 2, '.', '' );

	$total_donations = '' !== $total_donations ? $total_donations : '0';
	$total_donations = number_format( $total_donations, 2, '.', '' );

	$md_Settings = maybe_unserialize( get_option( 'md_sc_settings' ) );

	$application_fee = $md_Settings['app_fee'];
	$fee_type = $md_Settings['fee_type'];

	$fees = array();

	if ( 'percentage' === $fee_type ) {
		$fees['nanosteam'] = ( $application_fee / 100) * $total_donations ;
	} else {
		$fees['nanosteam'] = strval( $total_donations - $application_fee );
	}

	$fees['nanosteam'] = number_format( $fees['nanosteam'], 2, '.', '' );

	$fees['total'] = $stripe_fee + $fees['nanosteam'];
	$fees['total'] = number_format( $fees['total'], 2, '.', '' );

	return $fees;

}

function convert_to_numeric($var) {

	$var = '' != $var ? $var : '0';
	$val = number_format( $var, 2, '.', '' );

	return $val;

}

/**
 * Ajax Email function to send email to creators if their project requires additional material for it will be published
 * Processing button is located in the project's sidebar metabox under "Payout this project"
 */

function ajax_nano_project_review() {

	// First check the nonce, if it fails the function will break
	check_ajax_referer( 'nano_project_review_meta', 'security' );

	$project_review_subject      = isset( $_POST['project_review_subject'] ) ? $_POST['project_review_subject'] : '';
	$project_review_message      = isset( $_POST['project_review_message'] ) ? $_POST['project_review_message'] : '';
	$project_review_status_radio = isset( $_POST['project_review_status_radio'] ) ? $_POST['project_review_status_radio'] : '';
	$project_review_status_email = isset( $_POST['project_review_status_email'] ) ? $_POST['project_review_status_email'] : '';
	$post_ID                     = isset( $_POST['id'] ) ? $_POST['id'] : '';
	$recipient                   = get_post_meta( $post_ID, 'original_author', true );

	add_post_meta( $post_ID, 'project_review_subject', $project_review_subject );
	add_post_meta( $post_ID, 'project_review_message', $project_review_message );
	update_post_meta( $post_ID, 'project_review_status_radio', $project_review_status_radio );

	if ( '' == $project_review_status_email || 'pending' == $project_review_status_email ) {
		update_post_meta( $post_ID, 'project_review_status_email', 'sent' );
		echo json_encode( array( 'message' => __( 'Email Sent' ) ) );
		nano_email_notify( $post_ID, $recipient, 'nanosteam_project_needs_more_requirements' );
	} elseif ( 'sent' == $project_review_status_email ) {
		update_post_meta( $post_ID, 'project_review_status_email', 'pending' );
		echo json_encode( array( 'message' => __( 'There was a problem' ) ) );
	}

	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		wp_die();
	}

}

/*
 * Email admin when the creator updates their project after recieving an email from the project review process for
 * further updates to their project
 */

function nano_ping_admin_when_creator_updates_submission( $new_status, $old_status, $post ) {

	$post_id                     = $post->ID;
	$original_author             = get_post_meta( $post_id, 'original_author', true );
	$current_user                = wp_get_current_user();
	$project_review_status_email = get_post_meta( $post_id, 'project_review_status_email', true );

	if ( 'pending' == $new_status && 'pending' == $old_status ) {

		if ( $current_user->data->user_login == $original_author ) {

			if ( 'sent' == $project_review_status_email && ! isset( $_POST['project_continue'] ) ) {

				$post_link      = get_edit_post_link( $post_id );
				$post_permalink = get_site_url() . '?post_type=ignition_product&p=' . $post_id . '&preview=1';
				$post_title     = $post->post_title;
				$post_author    = get_author_posts_url( $current_user->data->ID );

				$to      = get_option( 'admin_email' );
				$subject = 'The project ' . $post_title . ' has been recently re-submitted for your review.';
				$message = 'Project Creator: ' . $post_author . "\r\n";
				$message .= 'Project Preview: ' . $post_permalink . "\r\n";
				$message .= 'Edit Project: ' . $post_link . "\r\n";
				wp_mail( $to, $subject, $message );

			}
		}
	}
}

add_action( 'transition_post_status', 'nano_ping_admin_when_creator_updates_submission', 10, 3 );


/**
 * For error handling of $wpdb queries
 *
 * @param $result
 * @param $data
 */
function nano_debug_query( $result, $data, $function ) {

		global $wpdb;

		$str   = htmlspecialchars( $wpdb->last_error, ENT_QUOTES );
		$query = htmlspecialchars( $wpdb->last_query, ENT_QUOTES );

		$output = '$wpdb query error from function: ' . $function . "\n";
		$output .= 'Last Error: ';
		$output .= $str;

		$output .= "\n\nLast Query: ";
		$output .= $query;

		if ( false === $result ) {
			$result = new WP_Error( 'query_failed', 'No update.', $data );
		}
		elseif ( 0 === $result ) {
			$result = new WP_Error( 'update_failed', 'Updated zero rows.', $data );
		}
		elseif ( 0 < $result ) {
			$result = 'Success';
		}

		// Only abort, if we got an error
		if ( is_wp_error( $result ) ) {

			$message = $output.$result;
			send_error( $message );
			email_general_admin_notification('There has been a $wpdb query error.', $message );

		}

}

/**
 * Replace the IDC Shortcode for the checkout page
 * wp-content/plugins/idcommerce/idcommerce-shortcodes.php
 *
 * @param $attrs
 *
 * @return string
 */

function nano_memberdeck_checkout( $attrs ) {

	send_error( 'nano_memberdeck_checkout' );

	ob_start();
	$url              = ( isset( $_SERVER['HTTPS'] ) ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	$customer_id      = customer_id();
	$instant_checkout = instant_checkout();
	$renewable        = false;
	$user_check       = false;
	global $crowdfunding;
	global $first_data;
	global $pwyw;
	global $global_currency;
	global $stripe_api_version;
	$global_currency_symbol = idc_currency_to_symbol( $global_currency );
	// use the shortcode attr to get our level id
	$product_id = $attrs['product'];
	do_action( 'doing_idc_checkout', $product_id );
	if ( isset( $pwyw ) && $pwyw ) {
		if ( isset( $_GET['price'] ) && $_GET['price'] > 0 ) {
			if ( 'BTC' == $global_currency || 'credits' == $global_currency ) {
				$pwyw_price = number_format( sprintf( '%f', floatval( $_GET['price'] ) ), 2, '.', '' );
			} else {
				$pwyw_price = number_format( floatval( esc_attr( $_GET['price'] ) ), 2, '.', '' );
			}
		} elseif ( isset( $_POST['price'] ) && $_POST['price'] > 0 ) {
			if ( 'BTC' == $global_currency || 'credits' == $global_currency ) {
				$pwyw_price = number_format( sprintf( '%f', floatval( $_POST['price'] ) ), 2, '.', '' );
			} else {
				$pwyw_price = number_format( floatval( esc_attr( $_POST['price'] ) ), 2, '.', '' );
			}
		}
	}

	// get the user info
	if ( is_user_logged_in() ) {
		$current_user    = wp_get_current_user();
		$email           = $current_user->user_email;
		$fname           = $current_user->user_firstname;
		$lname           = $current_user->user_lastname;
		$project         = new ID_Project( $product_id );
		$post_id         = $project->get_project_postid();
		$original_author = get_post_meta( $post_id, 'original_author', true );
		$user_check      = $current_user->user_login == $original_author ? true : false;
		// Check first if this user is allowed to purchase
		// #dev send to function
		$is_purchases_blocked = get_user_meta( $current_user->ID, 'block_purchasing', true );
		if ( ( ! empty( $is_purchases_blocked ) && '1' == $is_purchases_blocked ) ) {
			//TODO : Changed path
			include_once IDC_PATH . 'templates/_purchasesBlocked.php';
			$content = ob_get_contents();
			ob_end_clean();

			return $content;
		}
		$member    = new ID_Member( $current_user->ID );
		$user_data = ID_Member::user_levels( $current_user->ID );
		if ( ! empty( $user_data ) ) {
			$user_levels = unserialize( $user_data->access_level );
		} else {
			$user_levels = null;
		}
		// lets see how many levels this user owns
		if ( is_array( $user_levels ) ) {
			foreach ( $user_levels as $level ) {
				if ( $level == $product_id ) {
					$renewable = ID_Member_Level::is_level_renewable( $level );
					// Check if order exists for that product, if yes, then renewable is true
					// #devnote should be part of above function
					if ( is_user_logged_in() ) {
						$last_order     = new ID_Member_Order( null, $current_user->ID, $product_id );
						$get_last_order = $last_order->get_last_order();
						if ( empty( $get_last_order ) ) {
							$renewable = false;
						}
					}
					if ( ! $renewable ) {
						$already_valid = true;
					}
				}
			}
		}
	}
	$settings = get_option( 'md_receipt_settings' );
	if ( ! empty( $settings ) ) {
		$settings       = maybe_unserialize( $settings );
		$coname         = apply_filters( 'idc_company_name', $settings['coname'] );
		$guest_checkout = $settings['guest_checkout'];
	} else {
		$coname         = '';
		$guest_checkout = 0;
	}
	// #devnote why are we changing var name here?
	// Settings assigning to general variable
	$general = maybe_unserialize( $settings );

	$gateways = get_option( 'memberdeck_gateways' );
	if ( ! empty( $gateways ) ) {
		// gateways are saved and we can now get settings from Stripe and Paypal
		if ( is_array( $gateways ) ) {
			$mc         = ( isset( $gateways['manual_checkout'] ) ? $gateways['manual_checkout'] : 0 );
			$pp_email   = ( isset( $gateways['pp_email'] ) ? $gateways['pp_email'] : '' );
			$test_email = ( isset( $gateways['test_email'] ) ? $gateways['test_email'] : '' );
			$pk         = ( isset( $gateways['pk'] ) ? $gateways['pk'] : '' );
			$sk         = ( isset( $gateways['sk'] ) ? $gateways['sk'] : '' );
			$tpk        = ( isset( $gateways['tpk'] ) ? $gateways['tpk'] : '' );
			$tsk        = ( isset( $gateways['tsk'] ) ? $gateways['tsk'] : '' );
			$test       = ( isset( $gateways['test'] ) ? $gateways['test'] : 0 );
			$epp        = ( isset( $gateways['epp'] ) ? $gateways['epp'] : 0 );
			$es         = ( isset( $gateways['es'] ) ? $gateways['es'] : 0 );
			$esc        = ( isset( $gateways['esc'] ) ? $gateways['esc'] : 0 );
			$ecb        = ( isset( $gateways['ecb'] ) ? $gateways['ecb'] : '' );
			$eauthnet   = ( isset( $gateways['eauthnet'] ) ? $gateways['eauthnet'] : '0' );
			$eppadap    = ( isset( $gateways['eppadap'] ) ? $gateways['eppadap'] : '0' );
			$efd        = ( isset( $gateways['efd'] ) ? $gateways['efd'] : '0' );
			if ( isset( $efd ) && $efd ) {
				$gateway_id = $gateways['gateway_id'];
				$fd_pw      = $gateways['fd_pw'];
				$efd        = $gateways['efd'];
			}
			if ( ! is_idc_free() && function_exists( 'is_id_pro' ) && is_id_pro() ) {
				if ( $es ) {
					// #devnote remove this
					// Do nothing because Stripe customer id is already set
				} elseif ( $efd ) {
					$fd_card_details = fd_customer_id();
					if ( ! empty( $fd_card_details ) ) {
						$customer_id = $fd_card_details['fd_token'];
					}
				} elseif ( $eauthnet ) {
					$authorize_customer_id = authnet_customer_id();
					if ( ! empty( $authorize_customer_id ) ) {
						$customer_id = $authorize_customer_id['authorizenet_payment_profile_id'];
					} else {
						$customer_id = '';
					}
				}
				$customer_id = apply_filters( 'idc_checkout_form_customer_id', $customer_id, $product_id, $gateways );


				$check_claim = apply_filters( 'md_level_owner', get_option( 'md_level_' . $product_id . '_owner' ) );
				if ( ! empty( $check_claim ) ) {
					if ( '1' == $esc ) {
						$md_sc_creds = get_sc_params( $check_claim );
						if ( ! empty( $md_sc_creds ) ) {
							$sc_accesstoken = $md_sc_creds->access_token;
							$sc_pubkey      = $md_sc_creds->stripe_publishable_key;
						}
					}
					if ( '1' == $epp ) {
						$claimed_paypal = get_user_meta( $check_claim, 'md_paypal_email', true );
					}
				}
			}
		}
	}

	$cc_currency_symbol = '$';
	$cc_currency        = 'USD';
	if ( isset( $es ) && 1 == $es ) {
		// #devnote can this move up?
		if ( ! class_exists( 'Stripe' ) ) {
			//TODO : Changed path
			require_once IDC_PATH . 'lib/stripe-php-4.2.0/init.php';
		}
		if ( isset( $test ) && '1' == $test ) {
			\Stripe\Stripe::setApiKey( $tsk );
		} else {
			\Stripe\Stripe::setApiKey( $sk );
		}
		// get stripe currency
		$stripe_currency = 'USD';
		$stripe_symbol   = '$';
		if ( ! empty( $gateways ) ) {
			if ( is_array( $gateways ) ) {
				$stripe_currency = $gateways['stripe_currency'];
				$stripe_symbol   = md_currency_symbol( $stripe_currency );
			}
		}
	} elseif ( isset( $efd ) && 1 == $efd ) {
		$endpoint = 'https://api.globalgatewaye4.firstdata.com/transaction/v12';
		$wsdl     = 'https://api.globalgatewaye4.firstdata.com/transaction/v12/wsdl';
	}

	// use that id to get our level data
	$return = ID_Member_Level::get_level( $product_id );
	// we have that data, lets store it in vars
	$level_name = $return->level_name;
	$txn_type   = $return->txn_type;
	if ( 'preauth' == $txn_type ) {
		$guest_checkout = 0;
	}
	if ( $renewable ) {
		$level_price = $return->renewal_price;
	} else {
		$renewable   = false;
		$level_price = $return->level_price;
		if ( isset( $pwyw_price ) && $pwyw_price > $level_price ) {
			$level_price = $pwyw_price;
		}
	}
	if ( ! is_idc_free() ) {
		// Check if this product is an upgrade of another product, if yes, then get the difference of level prices. But not for recurring levels.
		if ( 'recurring' !== $return->level_type && ! $renewable ) {
			$idc_pathways    = new ID_Member_Pathways( null, $product_id );
			$product_pathway = $idc_pathways->get_product_pathway();
			if ( ! empty( $product_pathway ) ) {
				$idc_pathways->upgrade_pathway = $product_pathway->upgrade_pathway;
				$level_difference              = $idc_pathways->get_lower_product_difference( $level_price, ( is_user_logged_in() ? $current_user->ID : '' ) );
				if ( $level_difference > 0 ) {
					// Setting new level price
					$level_price = $level_difference;
					// New pay what you want price
					$pwyw_price    = $level_price;
					$upgrade_level = true;
				}
			}
		}
	}

	$currency = memberdeck_pp_currency();
	if ( ! empty( $currency ) ) {
		$pp_currency = $currency['code'];
		$pp_symbol   = $currency['symbol'];
	} else {
		$pp_currency = 'USD';
		$pp_symbol   = '$';
	}
	// If payment gateway for CC payments is Authorize.Net, and level is recurring, make instant_checkout false
	if ( 'recurring' == $return->level_type && 1 == $gateways['eauthnet'] ) {
		$instant_checkout = false;
	}

	$type             = $return->level_type;
	$recurring        = $return->recurring_type;
	$limit_term       = $return->limit_term;
	$term_length      = $return->term_length;
	$combined_product = $return->combined_product;

	$credit_value = $return->credit_value;
	$cf_level     = false;
	if ( $crowdfunding ) {
		$cf_assignments = get_assignments_by_level( $product_id );
		if ( ! empty( $cf_assignments ) ) {
			$project_id    = $cf_assignments[0]->project_id;
			$project       = new ID_Project( $project_id );
			$the_project   = $project->the_project();
			$post_id       = $project->get_project_postid();
			$id_disclaimer = get_post_meta( $post_id, 'ign_disclaimer', true );
		}
	}

	// Getting credits value, if the product can be purchased using credits and if the user have credits, then add an option to purhcase using credits
	$paybycrd       = 0;
	$member_credits = 0;
	if ( isset( $general['enable_credits'] ) && 1 == $general['enable_credits'] ) {
		if ( isset( $member ) ) {
			$member_credits = $member->get_user_credits();
		}
		if ( $member_credits > 0 ) {
			if ( isset( $pwyw_price ) && 'credits' == $global_currency ) {
				$credit_value = $pwyw_price;
			}
			if ( $credit_value > 0 && $credit_value <= $member_credits ) {
				$paybycrd = 1;
			}
		}
	}

	if ( isset( $ecb ) && $ecb ) {
		$cb_currency = ( isset( $gateways['cb_currency'] ) ? $gateways['cb_currency'] : 'BTC' );
		$cb_symbol   = md_currency_symbol( $cb_currency );
	}

	// If there is a combined product for currency loaded product, then we have to see if payment gateway supports it or not
	// then show text in General text that this product is combined with another
	if ( $combined_product ) {

		$epp = $eppadap = $eauthnet = $ecb = $efd = $mc = $paybycrd = 0;

		// #devnote we should be using filters for this and modifying either gateways array or individual vars
		$combined_level = ID_Member_Level::get_level( $combined_product );
		add_filter( 'idc_info_price', function ( $level_price, $return ) use ( $combined_level ) {
			$info_price = $level_price + $combined_level->level_price;

			return $info_price;
		}, 10, 2 );
		// Now see if any CreditCard gateway is active which supports recurring products, we just need to see if we have
		// to show that text or not in General text of different payment methods
		$combined_purchase_gateways = idc_combined_purchase_allowed( $gateways );
	} else {
		$combined_purchase_gateways = array();
	}

	if ( ( ! isset( $already_valid ) || $return->enable_multiples || $renewable ) && false == $user_check ) {
		// they don't own this level, send forth the template
		$level_price = apply_filters( 'idc_product_price', $level_price, $product_id, $return );
		if ( '' !== $level_price && $level_price > 0 ) {
			if ( 'BTC' == $global_currency || 'credits' == $global_currency ) {
				$level_price = number_format( sprintf( '%f', (float) $level_price ), 2 );
			} else {
				$level_price = number_format( floatval( $level_price ), 2, '.', ',' );
			}
		}
		$info_price = apply_filters( 'idc_price_format', apply_filters( 'idc_info_price', $level_price, $return ) );
		// Getting the content of the terms page
		if ( ! empty( $general['terms_page'] ) ) {
			$terms_content = get_post( $general['terms_page'] );
		}
		if ( ! empty( $general['privacy_page'] ) ) {
			$privacy_content = get_post( $general['privacy_page'] );
		}

		include_once apply_filters( 'idc_checkout_template', 'library/idc_templates/_checkoutForm.php' );
		$content = ob_get_contents();
	} else {
		// they already own this one
		$content = '<form method="POST" id="idc_already_purchased" name="idc_already_purchased">';
		$content .= '<h5>' . __( 'You have already supported this project. Please', 'memberdeck' ) . ' <a href="' . wp_logout_url() . '">' . __( 'logout', 'memberdeck' ) . '</a> ' . __( 'and create a new account in order to purchase again', 'memberdeck' ) . '.</h5>';
		$content .= '<input type="hidden" name="user_email" class="user_vars" value="' . $email . '"/>';
		$content .= '<input type="hidden" name="user_login" class="user_vars" value="' . $current_user->user_login . '"/>';
		$content .= '</form>';
	}
	ob_end_clean();

	return $content;
}

