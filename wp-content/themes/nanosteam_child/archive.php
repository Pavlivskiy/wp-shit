<?php
/**
 * The template for displaying archives.
 */
get_header(); ?>


<?php

if ( have_posts() ) {

	if ( isset( $_GET['post_type'] ) && 'ignition_product' == $_GET['post_type'] ) {
		$type = 'id-product';
	}
	if ( get_post_type() == 'ignition_product' ) {
		$type = 'id-product';
	} else {
		$type = 'post';
	}

	if ( is_author() ) {
		krown_author_profile();
	}

	if ( 'id-product' == $type ) {

		if ( ! is_author() ) {
			echo do_shortcode( '[nano_categories]' );
			nano_filter_form();
		}
		echo '<div id="ign-archive" class="nano-style row">';
	}

	if ( isset( $_GET['nano_filtering'] ) ) {

		nano_filter_function();

	} else {

		while ( have_posts() ) :

			the_post();

			if ( 'id-product' == $type ) {

				get_template_part( 'content-ignition_product' );

			} else {

				get_template_part( 'content' );

			}

		endwhile;

	}

	if ( 'id-product' == $type ) {
		echo '</div>';
	}

	if ( 'id-product' == $type ) {

		global $wp_query;

		if ( $wp_query->max_num_pages > 1 ) {
			echo '<div id="add_more" class="row mx-auto mt-5 mb-5"><a id="more_posts" class="btn btn-sm btn-outline-primary mx-auto" href="">Load More</a></div>';
		}
	} else {
		krown_pagination( null, true );
	}
} else {
	echo nano_modal_page_load( 'no-project-categories' );
}

get_footer();
