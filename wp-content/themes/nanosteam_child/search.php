<?php
/**
 * The template for displaying archives.
 */
get_header(); ?>

<?php

if ( have_posts() ) {


	echo '<div id="ign-search" class="pb-4">';

	?>
	<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
		<label for="search" class="sr-only">Search</label>
		<input class="form-control" type="search" value="<?php echo ( isset( $_GET['s'] ) ? $_GET['s'] : '' ); ?>" name="s" id="s"
			placeholder="<?php _e( 'Type and hit enter', 'krown' ); ?>"/>
	</form>
	<?php

	while ( have_posts() ) {

		the_post();

		get_template_part( 'content' );

	}

	echo '</div>';

	krown_pagination( null, true );

} else {

	echo '<p>' . __( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'krown' ) . '</p>';
	?>
	<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
		<label for="search" class="sr-only">Search</label>
		<input class="form-control" type="search" value="<?php echo ( isset( $_GET['s'] ) ? $_GET['s'] : '' ); ?>" name="s" id="s"
			placeholder="<?php _e( 'Type and hit enter', 'krown' ); ?>"/>
	</form>
	<?php

}

?>

<?php get_footer(); ?>