<?php
/**
 * The Template for displaying all pages.
 */
get_header(); ?>

<?php if ( have_posts() ) {
	while ( have_posts() ) : the_post(); ?>

		<?php

		backer_id_purchase_header();

		if ( '' !== get_post_meta( get_the_ID(), 'nano_mobile_content', true ) && 'mobile' == is_mobile() ) {
			echo apply_filters( 'the_content', get_post_meta( get_the_ID(), 'nano_mobile_content', true ) );
			//echo do_shortcode( html_entity_decode( stripslashes( get_post_meta( get_the_ID(), 'nano_mobile_content', true ) ) ) );
		} else {
			the_content();
		}

		wp_link_pages( array(
				'before' => '<p class="wp-link-pages"><span>' . __( 'Pages:', 'krown' ) . '</span>'
			)
		);

		if ( comments_open() && ot_get_option( 'krown_allow_page_comments', 'false' ) == 'true' ) {
			comments_template( '', true );
		}

		?>

	<?php endwhile;
}

get_footer(); ?>