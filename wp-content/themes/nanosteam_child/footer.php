<?php
/**
 * The footer of the theme
 */
?>

</div>

<?php krown_sidebar(); ?>

<!-- Inner Wrapper End -->
</div>

<?php if ( krown_sidebar_class() == 'layout-left-sidebar' || krown_sidebar_class() == 'layout-right-sidebar' ) {
	echo ' </div>';
} ?>


<!-- Main Wrapper End -->
</div>

<!-- Footer #1 Wrapper Start -->

<?php if ( get_option( 'rb_o_ftrtype', 'full' ) == 'full' ) : ?>

	<footer id="footer1" class="footer bg-secondary py-5 text-white">

		<div class="container">

			<div class="row">

			<?php if ( get_option( 'rb_o_ftrareas', 'four' ) == 'four' ) : ?>

				<div class="col-sm-3">
					<?php
					if ( is_active_sidebar( 'krown_footer_widget_1' ) ) {
						dynamic_sidebar( 'krown_footer_widget_1' );
					}
					?>
				</div>

				<div class="col-sm-3">
					<?php
					if ( is_active_sidebar( 'krown_footer_widget_2' ) ) {
						dynamic_sidebar( 'krown_footer_widget_2' );
					}

					?>
				</div>

				<div class="col-sm-3">
					<?php
					if ( is_active_sidebar( 'krown_footer_widget_3' ) ) {
						dynamic_sidebar( 'krown_footer_widget_3' );
					}
					?>
				</div>

				<div class="col-sm-3">
					<?php
					if ( is_active_sidebar( 'krown_footer_widget_4' ) ) {
						dynamic_sidebar( 'krown_footer_widget_4' );
					}
					?>
				</div>

			<?php elseif ( get_option( 'rb_o_ftrareas' ) == 'three' ) : ?>
				<div class="col-sm-4">
					<div class="d-none d-sm-block">
						<?php
						if ( is_active_sidebar( 'krown_footer_widget_1' ) ) {
							dynamic_sidebar( 'krown_footer_widget_1' );
						}
						?>
					</div>
				</div>

				<div class="col-sm-4 text-center">
					<img src="<?php echo get_stylesheet_directory_uri() ?>/images/Atom_gray_98px.gif">
					<?php
					if ( is_active_sidebar( 'krown_footer_widget_2' ) ) {
						// dynamic_sidebar( 'krown_footer_widget_2' );
					}
					?>
				</div>

				<div class="col-sm-4">
					<?php
					if ( is_active_sidebar( 'krown_footer_widget_3' ) ) {
						dynamic_sidebar( 'krown_footer_widget_3' );
					}
					?>
				</div>

			<?php elseif ( get_option( 'rb_o_ftrareas' ) == 'two' ) : ?>

				<div class="col-sm-6">
					<?php
					if ( is_active_sidebar( 'krown_footer_widget_1' ) ) {
						dynamic_sidebar( 'krown_footer_widget_1' );
					}
					?>
				</div>

				<div class="col-sm-6">
					<?php
					if ( is_active_sidebar( 'krown_footer_widget_2' ) ) {
						dynamic_sidebar( 'krown_footer_widget_2' );
					}
					?>
				</div>

			<?php elseif ( get_option( 'rb_o_ftrareas' ) == 'one' ) : ?>

				<div class="col-sm-12">
					<?php
					if ( is_active_sidebar( 'krown_footer_widget_1' ) ) {
						dynamic_sidebar( 'krown_footer_widget_1' );
					}
					?>
				</div>

			<?php endif; ?>

		</div>

		</div>

	</footer>

<?php endif; ?>

<!-- Footer #1 Wrapper End -->

<!-- Footer #2 Wrapper Start -->

<footer id="footer2" class="footer bg-secondary py-5 text-white">

	<div class="container">

		<div class="row justify-content-center">

			<div class="col-sm-6">
				<?php
				if ( is_active_sidebar( 'krown_footer_widget_5' ) ) {
					dynamic_sidebar( 'krown_footer_widget_5' );
				}
				?>
			</div>

			<div class="col-sm-6">
				<?php
				if ( is_active_sidebar( 'krown_footer_widget_6' ) ) {
					dynamic_sidebar( 'krown_footer_widget_6' );
				}
				?>
			</div>

		</div>

	</div>

</footer>
<!-- Footer End -->

<!-- GTT Button -->
<a id="top" href="#"></a>

<?php

if ( is_user_logged_in() ) { ?>

	<div class="modal fade" tabindex="-1" role="dialog" id="start-a-project-m">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<?php echo nano_modal_page_load( 'start-a-project' ); ?>
				</div>
			</div>
		</div>
	</div>

<?php } ?>

<?php wp_footer(); ?>

<?php if ( true !== dev_check() ) { ?>

<!-- Global site tag (gtag.js) - Google Analytics -->

	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-127883881-2"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-127883881-2');
	</script>

	<script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-101529932-1', 'auto');
        ga('send', 'pageview');

	</script>

<?php } ?>

</body>
</html>