<?php
/**
 * PHPUnit bootstrap file
 *
 * @package Nanosteam_child
 */

ini_set('error_reporting', E_ALL); // or error_reporting(E_ALL);
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');


/**
 * The path to the main file of the plugins to test.
 */
define( 'TEST_ABSOLUTE_PATH', '/Users/linear/Sites/toptal/projects/nanosteam/' );
define( 'TEST_PLUGIN_IDF', TEST_ABSOLUTE_PATH . 'wp-content/plugins/ignitiondeck/idf.php' );
define( 'TEST_PLUGIN_IDC', TEST_ABSOLUTE_PATH . 'wp-content/plugins/idcommerce/idcommerce.php' );
define( 'TEST_PLUGIN_IDCF', TEST_ABSOLUTE_PATH . 'wp-content/plugins/ignitiondeck-crowdfunding/ignitiondeck.php' );

/**
 * The path to the WordPress tests checkout.
 */
define( 'WP_TESTS_DIR', TEST_ABSOLUTE_PATH . 'wp-content/themes/nanosteam_child/tests/tmp/wordpress-tests-libs' );


$_tests_dir = WP_TESTS_DIR;

if ( ! file_exists( $_tests_dir . '/includes/functions.php' ) ) {
	echo "Could not find $_tests_dir/includes/functions.php, have you run bin/install-wp-tests.sh ?" . PHP_EOL;
	exit( 1 );
}

$_SERVER['HTTP_USER_AGENT'] = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:62.0) Gecko/20100101 Firefox/62.0";


// Give access to tests_add_filter() function.
require_once $_tests_dir . '/includes/functions.php';
session_start();


/**
 * Registers theme
 */
function _register_theme() {

	$theme_dir = dirname( __DIR__ );
	$current_theme = basename( $theme_dir );
	$theme_root = dirname( $theme_dir );

	add_filter( 'theme_root', function() use ( $theme_root ) {
		return $theme_root;
	} );

	register_theme_directory( $theme_root );

	add_filter( 'pre_option_template', function() use ( $current_theme ) {
		return $current_theme;
	});
	add_filter( 'pre_option_stylesheet', function() use ( $current_theme ) {
		return $current_theme;
	});
}
tests_add_filter( 'muplugins_loaded', '_register_theme' );


function run_activate_plugin( $plugin ) {
	$current = get_option( 'active_plugins' );
	$plugin = plugin_basename( trim( $plugin ) );

	if ( !in_array( $plugin, $current ) ) {
		$current[] = $plugin;
		sort( $current );
		do_action( 'activate_plugin', trim( $plugin ) );
		update_option( 'active_plugins', $current );
		do_action( 'activate_' . trim( $plugin ) );
		do_action( 'activated_plugin', trim( $plugin) );
	}

	return null;
}

/**
 * Registers IDK plugins and their functions
 */
function _register_plugins() {

	// Manually creating the gateway information for the system based on the staging/dev system.
	$args = array(
		'pp_currency'                => 'USD',
		'pp_symbol'                  => '$',
		'pp_email'                   => '',
		'test_email'                 => '',
		'paypal_test_redirect'       => '',
		'paypal_redirect'            => '',
		'test'                       => 1,
		'epp'                        => '0',
		'stripe_currency'            => 'USD',
		'pk'                         => 'pk_live_EiFNEz541fRqf0KsbuoQWzfc',
		'sk'                         => 'sk_live_yasgYZQiJ2GAVwRVek00iVyG',
		'tpk'                        => 'pk_test_B9lggnxyaekGLPeANxiNoD3x',
		'tsk'                        => 'sk_test_zZoBVh8OPyutDRUvYTON9alq',
		'https'                      => '0',
		'manual_checkout'            => '0',
		'epp_fes'                    => '0',
		'es'                         => 1,
		'epwyw_stripe'               => '',
		'esc'                        => 1,
		'ecb'                        => 0,
		'eauthnet'                   => 0,
		'cb_currency'                => 'BTC',
		'cb_api_key'                 => '',
		'cb_api_secret'              => '',
		'auth_login_id'              => '',
		'auth_transaction_key'       => '',
		'gateway_id'                 => '',
		'fd_pw'                      => '',
		'key_id'                     => '',
		'hmac'                       => '',
		'efd'                        => 0,
		'eppadap'                    => 0,
		'ppada_currency'             => 'USD',
		'ppadap_api_username'        => '',
		'ppadap_api_password'        => '',
		'ppadap_api_signature'       => '',
		'ppadap_app_id'              => '',
		'ppadap_receiver_email'      => '',
		'ppadap_api_username_test'   => '',
		'ppadap_api_password_test'   => '',
		'ppadap_api_signature_test'  => '',
		'ppadap_app_id_test'         => '',
		'ppadap_receiver_email_test' => '',
		'ppadap_max_preauth_period'  => '',
	);

	update_option( 'memberdeck_gateways', $args );

	// Critical options to ensure plugins load correctly.
	add_option( 'is_id_pro', 1 );
	add_option( 'was_id_pro', 1 );
	add_option( 'is_idc_licensed', 1 );


	// Critical plugins to load
	$plugins = array(
		'ignitiondeck/idf.php',
		'ignitiondeck-crowdfunding/ignitiondeck.php',
		'idcommerce/idcommerce.php',
	);

	if ( ! function_exists( 'activate_plugin' ) ) {
		require_once TEST_ABSOLUTE_PATH . 'wp-admin/includes/plugin.php';
	}

	foreach ( $plugins as $plugin ) {


		if ( ! is_plugin_active( $plugin ) ) {
			run_activate_plugin( $plugin );
			$result = activate_plugin( $plugin );
		}

		if ( is_wp_error( $result ) ) {
			error_log( print_r( 'UNABLE TO ACTIVATE PLUGIN: ' . $plugin, true ) );
			// Process Any load errors
		}
	}

}

tests_add_filter( 'muplugins_loaded', '_register_plugins' );

/**
 * Loads the nanosteam module in the IDK plugin
 */
function _load_modules() {
	$id_module = new ID_Modules();
	$id_module->load_modules();
	$id_module->load_module( 'nanosteam' );
	ID_Modules::set_module_status( 'nanosteam', true );

}
tests_add_filter( 'plugins_loaded', '_load_modules' );




// Start up the WP testing environment.
require $_tests_dir . '/includes/bootstrap.php';