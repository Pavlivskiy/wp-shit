import 'package:flutter/material.dart';
import 'package:flutter_wordpress/flutter_wordpress.dart' as wp;
import 'display_posts.dart';
import 'nanosteam_colors.dart';

const PADDING_16 = EdgeInsets.all(16.0);
const PADDING_8 = EdgeInsets.all(8.0);
const PADDING_LOGIN = EdgeInsets.symmetric( vertical: 5.0, horizontal: 8.0 );

class LoginPage extends StatefulWidget {
  @override
  LoginPageState createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Welcome To!",
          style: TextStyle(
              fontWeight: FontWeight.w300,
              fontFamily: 'Kontrapunkt',
              fontSize: 30
          ),
        ),
        centerTitle: true,
          backgroundColor: HexColor('FFB400'),
      ),
      body: LoginFields(),
    );
  }
}

class LoginFields extends StatefulWidget {
  @override
  LoginFieldsState createState() => LoginFieldsState();
}

class LoginFieldsState extends State<LoginFields> {
  String _username;
  String _password;
  bool _isDetailValid = true;
  bool _isValidating = false;
  final logo = Hero(
    tag: 'Hero',
    child: CircleAvatar(
      radius: 48,
      child: Image.asset('asset/ns_logo_hz.png')
    ),
  );

  @override
  void initState() {
    super.initState();
    _username = 'james.roy@toptal.com';
    _password = 'hondaxr.102';
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
        child: SingleChildScrollView(
      child: Container(
        padding: PADDING_16,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsets.all(48.0),
              child: (
                  Hero(
                      tag: 'Logo',
                      child: (
                          Image.asset('assets/ns_logo_hz.png')
                      )
                  )
              ),
            ),
            Padding(
              padding: PADDING_LOGIN,
              child: _buildFormField(
                icon: Icon(Icons.email),
                labelText: "Email Address",
                hintText: "Email",
                initialText: _username,
                onChanged: _onUsernameChanged,
              ),
            ),
            Padding(
              padding: PADDING_LOGIN,
              child: _buildFormField(
                icon: Icon(Icons.lock),
                labelText: "Password",
                hintText: "Password",
                initialText: _password,
                obscureText: true,
                onChanged: _onPasswordChanged,
              ),
            ),
            _isDetailValid
                ? SizedBox(
                    width: 0.0,
                    height: 0.0,
                  )
                : Padding(
                    padding: PADDING_8,
                    child: Text(
                      "Invalid Username / Password",
                      style: TextStyle(
                        color: Colors.red,
                      ),
                    ),
                  ),
            FlatButton(
              onPressed: _isValidating ? () {} : _validateUser,
              color: HexColor('FFB400'),
              textColor: Colors.white,
              child: Padding(
                padding: PADDING_8,
                child: _isValidating ? SizedBox(
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation(Colors.white),
                    strokeWidth: 1,
                  ),
                  height: 20.0,
                  width: 20.0,
                ) : Text('Login'),
              ),
            ),
            FlatButton(
              onPressed: _isValidating ? () {} : _forgotUserPassword,
              color: null,
              textColor: HexColor('FFB400'),
              child: Padding(
                padding: PADDING_8,
                child: _isValidating ? SizedBox(
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation(Colors.white),
                    strokeWidth: 1,
                  ),
                  height: 20.0,
                  width: 20.0,
                ) : Text('Forgot Password'),
              ),
            ),
          ],
        ),
      ),
    ));
  }

  void _onUsernameChanged(String value) {
    _username = value;
  }

  Widget _buildFormField({
    Icon icon,
    String labelText,
    String hintText,
    String initialText,
    TextInputType inputType = TextInputType.text,
    bool obscureText = false,
    onChanged,
  }) {
    return TextField(
      decoration: InputDecoration(
        icon: icon,
        labelText: labelText,
        hintText: hintText,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(4.0),
        ),
      ),
      controller: TextEditingController(text: initialText),
      keyboardType: inputType,
      obscureText: obscureText,
      onChanged: onChanged,
    );
  }

  void _onPasswordChanged(String value) {
    _password = value;
  }

  void _validateUser() {
    setState(() {
      _isValidating = true;
    });

    wp.WordPress wordPress = new wp.WordPress(
      baseUrl: 'http://nanosteam.site',
      authenticator: wp.WordPressAuthenticator.JWT,
    );

    final response =
        wordPress.authenticateUser(username: _username, password: _password);

    response.then((user) {
      setState(() {
        _isDetailValid = true;
        _isValidating = false;

        _onValidUser(wordPress, user);
      });
    }).catchError((err) {
      print(err.toString());
      setState(() {
        _isDetailValid = false;
        _isValidating = false;
      });
    });
  }

  void _onValidUser(wp.WordPress wordPress, wp.User user) {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (context) => PostListPage(
              wordPress: wordPress,
        ),
      ),
    );
  }

  void _forgotUserPassword() {
    setState(() {
      _isValidating = true;
    });

    wp.WordPress wordPress = new wp.WordPress(
      baseUrl: 'http://nanosteam.site',
      authenticator: wp.WordPressAuthenticator.JWT,
      forgotpassword: wp.WordPressAuthenticator.JWT,
    );

    final response =
    wordPress.authenticateUser(username: _username, password: _password);

    response.then((user) {
      setState(() {
        _isDetailValid = true;
        _isValidating = false;

        _onValidUser(wordPress, user);
      });
    }).catchError((err) {
      print(err.toString());
      setState(() {
        _isDetailValid = false;
        _isValidating = false;
      });
    });
  }
}
