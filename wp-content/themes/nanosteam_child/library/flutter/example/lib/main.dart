import 'package:flutter/material.dart';
import 'login.dart';

void main() {
  runApp(WordPressApp());
}

class WordPressApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Nanosteam',
      theme: (
          ThemeData(
              fontFamily: "Lato",
              primaryColor: HexColor('FFB400'),
          )
      ),
      home: LoginPage(),
    );
  }
}

class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}


