<?php

if ( ! defined( 'ABSPATH' ) ) {
	die( 'Silence is golden.' );
}

/**
 * Derivived from: Mega Blocks for Gutenberg
 * SOURCE Plugin URI: http://webcodingplace.com/mega-blocks-gutenberg
 */

define( 'NSG_PATH', untrailingslashit( get_stylesheet_directory( __FILE__ ) . '/library/gutenburg' ) );
define( 'NSG_URL', untrailingslashit( get_stylesheet_directory_uri( __FILE__ ) . '/library/gutenburg' ) );


require_once( 'nanosteam_blocks_gutenburg_class.php' );


if ( class_exists( 'Nanosteam_Blocks_Gutenberg' ) ) {
	$mbg_init = new Nanosteam_Blocks_Gutenberg;
}