<?php
// Check if user is logged in, if not then load the ajax ready forms
if ( ! is_user_logged_in() && ! isset( $_GET['key'] ) ) {
	function login_forms() {
		get_template_part( 'library/login_register/ajax-auth' );
	}

	add_action( 'wp_footer', 'login_forms' );

}

/**
 * Loading the needed scripts
 */

function nano_ajax_auth_wp() {

	//Adding nonce field to IDK registration pages for google recaptcha
	add_action( 'idc_below_register_form', 'nano_nonce', 99 );
	// Enable the user with no privileges to run ajax_login() in AJAX
	add_action( 'wp_ajax_nopriv_ajax_login', 'ajax_login' );
	// Enable the user with no privileges to run ajax_register() in AJAX
	add_action( 'wp_ajax_nopriv_ajax_register', 'ajax_register' );
	add_action( 'wp_ajax_ajax_register', 'ajax_register' );
	// Enable the user with no privileges to run ajax_purchase() in AJAX
	add_action( 'wp_ajax_nopriv_ajax_purchase', 'ajax_purchase' );
	//add_action( 'wp_ajax_ajax_purchase', 'ajax_purchase' );
	// Enable the user with no privileges to run ajax_forgotPassword() in AJAX
	add_action( 'wp_ajax_nopriv_ajax_forgotpassword', 'ajax_forgotPassword' );

}

function nano_ajax_auth_init() {

	wp_register_script(
		'ajax-auth-script',
		get_stylesheet_directory_uri() . '/library/login_register/js/ajax-auth-script.js',
		array(
			'jquery',
			'nano-formvalidation-js',
			'formvalidation-bootstrap-js',
			'formvalidation-jquery-js',
		),
		1.0,
		true
	);

	wp_enqueue_script( 'ajax-auth-script' );

	wp_enqueue_script( 'password-strength-meter' );

	wp_localize_script(
		'ajax-auth-script',
		'ajax_auth_object',
		array(
			'ajaxurl'          => admin_url( 'admin-ajax.php' ),
			'google_recaptcha' => get_option( 'captcha_site_key_val' ),
			'redirecturl'      => md_get_durl() . '?edit-profile',
			'first_login_url'  => get_permalink( get_page_by_path( 'welcome' ) ),
			'loadingmessage'   => __( 'Loading, please wait...' ),
		)
	);

}

function nano_google_recaptcha_load_last() {
	wp_register_script( 'nano-google-recap', 'https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit#asyncload', array( 'jquery' ), 1.0, true );
	wp_enqueue_script( 'nano-google-recap' );
}

function nano_ajax_purchase() {

	wp_register_script(
		'ajax-auth-script',
		get_stylesheet_directory_uri() . '/library/login_register/js/ajax-auth-script.js',
		array(
			'jquery',
			'nano-formvalidation-js',
			'formvalidation-bootstrap-js',
			'formvalidation-jquery-js',
		),
		1.0,
		true
	);

	wp_enqueue_script( 'ajax-auth-script' );

	wp_localize_script(
		'ajax-auth-script',
		'ajax_auth_object',
		array(
			'ajaxurl'          => admin_url( 'admin-ajax.php' ),
			'google_recaptcha' => get_option( 'captcha_site_key_val' ),
			'redirecturl'      => md_get_durl() . '?edit-profile',
			'first_login_url'  => get_permalink( get_page_by_path( 'welcome' ) ),
			'loadingmessage'   => __( 'Loading, please wait...' ),
		)
	);
}

function nano_ajax_purchase_init() {
	add_action( 'wp_ajax_ajax_purchase', 'ajax_purchase' );
	add_action( 'wp_ajax_nopriv_ajax_purchase', 'ajax_purchase' );
}


// Execute the action only if the user isn't logged in
if ( ! is_user_logged_in() ) {
	add_action( 'wp_enqueue_scripts', 'nano_ajax_auth_init' );
	add_action( 'wp_enqueue_scripts', 'nano_google_recaptcha_load_last', 101 );
	add_action( 'init', 'nano_ajax_auth_wp' );
}


// Execute the action only if the user isn't logged in
if ( isset( $_GET['purchaseform'] ) ) {
	add_action( 'wp_enqueue_scripts', 'nano_ajax_purchase' );
	add_action( 'init', 'nano_ajax_auth_wp' );
}


//Add NONCE Recaptcha to IDK registration forms
function nano_nonce() {
	echo '<input type="hidden" class="hiddenRecaptcha" name="qt_hiddenRecaptcha" id="qt_hiddenRecaptcha">';
	echo '<div id="captcha2" class=""></div>';

	return wp_nonce_field( 'ajax-purchase-nonce', 'purchasesecurity' );
}

/**
 * Login via ajax
 */
function ajax_login() {

	// First check the nonce, if it fails the function will break
	check_ajax_referer( 'ajax-login-nonce', 'security' );

	// Nonce is checked, get the POST data and sign user on
	// Call auth_user_login
	// send_error('start login script');
	auth_user_login( $_POST['username'], $_POST['password'], 'Login' );

	wp_die();
}


/**
 * Register a new user via ajax
 */
function ajax_register() {

	// First check the nonce, if it fails the function will break
	check_ajax_referer( 'ajax-register-nonce', 'security' );

	// Check if reCaptcha is valid

	$recaptcha = $_POST['recaptcha'];
	if ( ! empty( $recaptcha ) ) {

		$google_url = 'https://www.google.com/recaptcha/api/siteverify';
		$secret     = get_option( 'captcha_secret_key_val' ); // Replace your Google Secret Key here
		$ip         = $_SERVER['REMOTE_ADDR'];
		$url        = $google_url . '?secret=' . $secret . '&response=' . $recaptcha . '&remoteip=' . $ip;
		$curl       = curl_init();
		curl_setopt( $curl, CURLOPT_URL, $url );
		curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $curl, CURLOPT_TIMEOUT, 10 );
		curl_setopt( $curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16' );
		$results = curl_exec( $curl );
		curl_close( $curl );
		$res = json_decode( $results, true );
		if ( ! $res['success'] ) {
			echo json_encode(
				array(
					'loggedin'  => false,
					'message'   => __( 'reCAPTCHA invalid' ),
					'res'       => $res,
					'secretkey' => $secret,
				)
			);
			wp_die();
		}
	} else {
		if ( ! defined( 'PHPUNIT_NANOSTEAM_TESTSUITE' ) ) {
			echo json_encode(
				array(
					'loggedin' => false,
					'message'  => __( 'Please enter reCAPTCHA' ),
				)
			);
			wp_die();
		}
	}

	// Nonce is checked, get the POST data and sign user on
	$info                  = array();
	$info['user_nicename'] = $info['nickname'] = $info['display_name'] = $info['first_name'] = $info['user_login'] = sanitize_user( $_POST['username'] );
	$info['user_pass']     = sanitize_text_field( $_POST['password'] );
	$info['user_email']    = sanitize_email( $_POST['email'] );

	$user_profile_page = strtolower( wp_generate_password( 16, false ) );

	$info = array(
		'user_pass'     => sanitize_text_field( $_POST['password'] ),
		'first_name'    => sanitize_text_field( $_POST['first_name'] ),
		'last_name'     => sanitize_text_field( $_POST['last_name'] ),
		'user_nicename' => generate_unique_user_nicename( $user_profile_page ),
		'user_login'    => sanitize_email( $_POST['email'] ),
		'user_email'    => sanitize_email( $_POST['email'] ),
		'display_name'  => sanitize_text_field( $_POST['first_name'] ),
	);

	// Register the user
	$user_register = wp_insert_user( $info );

	if ( is_wp_error( $user_register ) ) {

		$error = $user_register->get_error_codes();

		if ( in_array( 'empty_user_login', $error ) ) {
			echo json_encode(
				array(
					'loggedin' => false,
					'message'  => $user_register->get_error_message( 'empty_user_login' ),
				)
			);
		} elseif ( in_array( 'existing_user_login', $error ) ) {
			echo json_encode(
				array(
					'loggedin' => false,
					'message'  => __( 'This email address is already registered.' ),
				)
			);
		} elseif ( in_array( 'existing_user_email', $error ) ) {
			echo json_encode(
				array(
					'loggedin' => false,
					'message'  => __( 'This email address is already registered.' ),
				)
			);
		}
	} else {

		if ( function_exists( 'idc_welcome_email' ) ) {
			idc_welcome_email( $user_register, $info['user_email'] );
		} else {
			wp_new_user_notification( $user_register, null, 'both' );
		}
		assign_avatar_init( $user_register );
		auth_user_login( $info['user_email'], $info['user_pass'], 'Registration' );

	}

	wp_die();
}


/**
 * Purchase form via ajax
 */
function ajax_purchase() {

	send_error( 'ajax_purchase' );

	// First check the nonce, if it fails the function will break
	// check_ajax_referer( 'ajax-purchase-nonce', 'purchasesecurity' );
	// echo wp_verify_nonce($_POST['security'],'ajax-purchase-nonce');

	if ( ! wp_verify_nonce( $_POST['security'], 'ajax-purchase-nonce' ) ) {

		die( 'Security check' );

	} else {

		$post_id = $_POST['post_id'];

		if ( ! is_user_logged_in() ) {

			$user_login        = $_POST['username'];
			$nano_current_user = $user_login;

		} else {

			$user_login        = wp_get_current_user()->user_login;
			$nano_current_user = get_user_by( 'login', $user_login )->user_login;

		}

		send_error( 'ajax_purchase fired' );

		// Lets check to ensure the person logging in is not the creator or a team member.
		$author_check                 = false;
		$org_author                   = false;
		$creator_args                 = array( 'field' => 'name' );
		$checking                     = wp_get_post_terms( $post_id, 'author', $creator_args );
		$original_author              = get_post_meta( $post_id, 'original_author', true );
		$check_current_user_if_member = multi_dimen_array_search_all_keys( $checking, 'name', $nano_current_user );

		if ( is_array( $check_current_user_if_member ) || $nano_current_user == $original_author ) {

			$author_check = true;

		}

		// Check if reCaptcha is valid
		$recaptcha = isset( $_POST['recaptcha'] ) ? $_POST['recaptcha'] : null;

		if ( isset( $_POST['skip'] ) ) {

			if ( false == $author_check ) {

				echo json_encode(
					array(
						'loggedin' => true,
						'message'  => __( 'Payment Processing...' ),
					)
				);

			} else {

				echo json_encode(
					array(
						'loggedin' => false,
						'message'  => __( 'Sorry, you can\'t support your own project. <a title="logout" class="alert-link" href="/wp-login.php?action=logout">Please Logout</a> and log in as another user to support this project.' ),
					)
				);

			}

			wp_die();

		}

		if ( ! empty( $recaptcha ) ) {
			$google_url = 'https://www.google.com/recaptcha/api/siteverify';
			$secret     = get_option( 'captcha_secret_key_val' ); // Replace your Google Secret Key here
			$ip         = $_SERVER['REMOTE_ADDR'];
			$url        = $google_url . '?secret=' . $secret . '&response=' . $recaptcha . '&remoteip=' . $ip;
			$curl       = curl_init();
			curl_setopt( $curl, CURLOPT_URL, $url );
			curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1 );
			curl_setopt( $curl, CURLOPT_TIMEOUT, 10 );
			curl_setopt( $curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16' );
			$results = curl_exec( $curl );
			curl_close( $curl );
			$res = json_decode( $results, true );
			if ( ! $res['success'] && ! $res['timeout-or-duplicate'] && ( isset( $res['error-codes'] ) && 'timeout-or-duplicate' !== $res['error-codes'][0] ) ) {
				echo json_encode(
					array(
						'loggedin'  => false,
						'message'   => __( 'reCAPTCHA invalid' ),
						'res'       => $res,
						'secretkey' => $secret,
					)
				);
				wp_die();
			} else {
				if ( isset( $_POST['username'] ) ) {

					$user_login = $_POST['username'];
					$password   = $_POST['password'];
					$post_id    = $_POST['post_id'];
					$login      = 'User login';

					if ( 0 < username_exists( $user_login ) || 0 < email_exists( $user_login ) ) {

						$userdata = get_user_by( 'login', $user_login );

						if ( '' == $userdata || null == $userdata ) {
							$userdata = get_user_by( 'email', $user_login );
						}

						$result = wp_check_password( $password, $userdata->data->user_pass, $userdata->data->ID );

						if ( $result ) {

							auto_login( $userdata );

							if ( false == $author_check ) {

								echo json_encode(
									array(
										'loggedin' => false,
										'message'  => __( 'Payment Processing...' ),
									)
								);

							} else {

								echo json_encode(
									array(
										'loggedin' => false,
										'message'  => __( 'Sorry, you can\'t support your own project. <a title="logout" class="alert-link" href="/wp-login.php?action=logout">Please Logout</a> and log in as another user to support this project.' ),
									)
								);

							}
						} elseif ( 'User login' == $login ) {

							echo json_encode(
								array(
									'loggedin' => false,
									'message'  => __( 'This email is already registered. Please enter your password' ),
								)
							);

						} else {

							echo json_encode(
								array(
									'loggedin' => false,
									'message'  => __( 'Wrong email address or password.' ),
								)
							);

						}

						wp_die();

					} elseif ( 'User login' !== $login ) {
						echo json_encode(
							array(
								'loggedin' => false,
								'message'  => __( 'Wrong email address or password.' ),
							)
						);

						wp_die();
					}
				}
			}
		} else {
			if ( ! defined( 'PHPUNIT_NANOSTEAM_TESTSUITE' ) ) {
				echo json_encode(
					array(
						'loggedin' => false,
						'message'  => __( 'Please enter reCAPTCHA' ),
					)
				);
				wp_die();
			}
		}

		echo json_encode(
			array(
				'loggedin' => true,
				'message'  => __( 'Payment Processing...' ),
			)
		);
		// Nonce is checked, continue to IDK scripts

		wp_die();

	}

}


/**
 * Once a user has registered sign them on automatically
 *
 * @param $user_login
 * @param $password
 * @param $login
 */
function auth_user_login( $user_login, $password, $login ) {

	$info = array(
		'user_login'    => $user_login,
		'user_password' => $password,
		'remember'      => true,
	);

	//send_error( 'time of firing: ' . nano_get_formated_date_time_gmt('')

	if ( 0 < username_exists( $info['user_login'] ) || 0 < email_exists( $info['user_login'] ) ) {

		$userdata = get_user_by( 'login', $info['user_login'] );

		if ( '' == $userdata || null == $userdata ) {
			$userdata = get_user_by( 'email', $info['user_login'] );
		}

		$result = wp_check_password( $info['user_password'], $userdata->data->user_pass, $userdata->data->ID );

		if ( $result ) {

			auto_login( $userdata );

			$user       = wp_get_current_user();
			$first_name = $user->user_firstname;
			$last_name  = $user->user_lastname;

			echo json_encode(
				array(
					'loggedin' => true,
					'message'  => ( $login . ' successful, logging in...' ),
					'user'     => array( 'fname' => $first_name, 'lname' => $last_name, ),
				)
			);

		} elseif ( 'User login' == $login ) {

			echo json_encode(
				array(
					'loggedin' => false,
					'message'  => __( 'Incorrect password.' ),
				)
			);

		} else {

			echo json_encode(
				array(
					'loggedin' => false,
					'message'  => __( 'Wrong email address or password.' ),
				)
			);

		}
	} elseif ( 'User login' !== $login ) {

		echo json_encode(
			array(
				'loggedin' => false,
				'message'  => __( 'Wrong email address or password.' ),
			)
		);
	}

	wp_die();

}

/**
 * Check user credentials and log them in
 *
 * @param $user
 */
function auto_login( $user ) {

	if ( ! is_user_logged_in() ) {

		$user_id    = $user->data->ID;
		$user_login = $user->data->user_login;

		wp_set_current_user( $user_id, $user_login );
		wp_set_auth_cookie( $user_id );

	}
}

/**
 * Forgot password function
 */
function ajax_forgotPassword() {

	// First check the nonce, if it fails the function will break
	check_ajax_referer( 'ajax-forgot-nonce', 'security' );

	$account = sanitize_email( $_POST['username'] );

	if ( empty( $account ) ) {
		$error = 'Enter an username or e-mail address.';
	} else {
		if ( is_email( $account ) ) {
			if ( email_exists( $account ) ) {
				$get_by = 'email';
			} else {
				$error = 'There is no user registered with that email address.';
			}
		} elseif ( validate_username( $account ) ) {
			if ( username_exists( $account ) ) {
				$get_by = 'login';
			} else {
				$error = 'There is no user registered with that email address.';
			}
		} else {
			$error = 'Invalid e-mail address.';
		}
	}

	if ( empty( $error ) ) {

		// lets generate our new password
		//$random_password = wp_generate_password( 12, false );
		$random_password = wp_generate_password();

		// Get user data by field and data, fields are id, slug, email and login
		$user = get_user_by( $get_by, $account );


		// if  update user return true then lets send user an email containing the new password
		if ( isset( $user->ID ) ) {

			$settings = get_option( 'md_receipt_settings' );

			if ( ! empty( $settings ) ) {
				if ( ! is_array( $settings ) ) {
					$settings = unserialize( $settings );
				}
				$coname = apply_filters( 'idc_company_name', $settings['coname'] );
			} else {
				$coname = '';
			}

			$from = ''; // Set whatever you want like mail@yourdomain.com

			if ( ! ( isset( $from ) && is_email( $from ) ) ) {
				$sitename = strtolower( $_SERVER['SERVER_NAME'] );
				if ( substr( $sitename, 0, 4 ) == 'www.' ) {
					$sitename = substr( $sitename, 4 );
				}
				$from = 'donotreply@' . $sitename;
			}

			$to      = $user->user_email;
			$subject = 'Your new password';
			$sender  = 'From: ' . $coname . ' <' . $from . '>' . "\r\n";

			$html_header = nano_email_header( $subject, $subject );
			$html_footer = nano_email_footer();

			$crm_settings    = get_option( 'crm_settings' );
			$enable_mandrill = '';
			if ( ! empty( $crm_settings ) ) {
				$enable_mandrill = $crm_settings['enable_mandrill'];
			}

			//$message  = 0 == $enable_mandrill ? $html_header : '';
			$message = '<p>Your new password is: ' . $random_password . '</p>';
			//$message .= 0 == $enable_mandrill ? $html_footer : '';

			$headers  = 'From: ' . $coname . ' <donotreply@nanosteam.org>' . "\n";
			$headers .= 'Reply-To: donotreply@nanosteam.org ' . "\n";
			$headers .= "MIME-Version: 1.0\n";
			$headers .= "Content-Type: text/html; charset=UTF-8\n";

			$mail = wp_mail( $to, $subject, $message, $headers );
			//$mail = md_send_mail( $to, $headers, $subject, $message );
			//$mail = mail($to, $subject, $message, $headers);

			$update_user = wp_update_user(
				array(
					'ID'        => $user->ID,
					'user_pass' => $random_password,
				)
			);

			if ( $mail ) {
				$success = 'Check your email address for you new password.';
			} else {
				$error = 'System is unable to send you mail containing your new password.';
			}
		} else {
			$error = 'Oops! Something went wrong while updating your account.';
		}
	}

	if ( ! empty( $error ) ) {
		echo json_encode(
			array(
				'loggedin' => false,
				'message'  => $error,
			)
		);
	}

	if ( ! empty( $success ) ) {
		echo json_encode(
			array(
				'loggedin' => true,
				'message'  => $success,
			)
		);
	}

	wp_die();
}


/**
 * Forward user on first login
 *
 * @param $user_id
 */

function nanosteam_register_add_meta( $user_id ) {
	add_user_meta( $user_id, '_new_user', '1' );
}

add_action( 'user_register', 'nanosteam_register_add_meta' );

/**
 * First time a user logs in bring them to the welcome page
 *
 * @param $user_login
 * @param $user
 */
function nanosteam_first_user_login( $user_login, $user ) {
	$new_user = get_user_meta( $user->ID, '_new_user', true );

	$user_email_settings                                 = array();
	$user_email_settings['creator_notification_pledge']  = 'creator_notification_pledge';
	$user_email_settings['creator_notification_backers'] = 'creator_notification_backers';
	$user_email_settings['backer_notification_updates']  = 'backer_notification_updates';

	if ( '0' !== $new_user ) {
		assign_avatar_init( $user->ID );
		update_user_meta( $user->ID, 'nano_user_email_settings', $user_email_settings );
	}

	if ( '0' !== $new_user && isset( $_POST['action'] ) && 'idmember_create_customer' !== $_POST['action'] ) {
		update_user_meta( $user->ID, '_new_user', '0' );

		// Time to redirect to the welcome page
		$url = get_permalink( get_page_by_path( 'welcome' ) );
		wp_safe_redirect( $url );
		exit;
	}

	// When a backer signs up to support a project during purchase we assign a random user_nicename url.
	if ( '0' !== $new_user && isset( $_POST['action'] ) && 'idmember_create_customer' == $_POST['action'] ) {

		$user_profile_page = strtolower( wp_generate_password( 16, false ) );
		$userdata          = array(
			'ID'            => $user->ID,
			'user_nicename' => generate_unique_user_nicename( $user_profile_page ),
		);
		wp_update_user( $userdata );

	}

}

add_action( 'wp_login', 'nanosteam_first_user_login', 10, 2 );


/**
 * Redirects to the custom password reset page, or the login page
 * if there are errors.
 */
function redirect_to_custom_password_reset() {
	if ( 'GET' == $_SERVER['REQUEST_METHOD'] && ! isset( $_GET['purchaseform'] ) ) {
		$redirect_url = home_url( 'member-password-reset' );
		$redirect_url = add_query_arg( 'login', esc_attr( $_REQUEST['login'] ), $redirect_url );
		$redirect_url = add_query_arg( 'key', esc_attr( $_REQUEST['key'] ), $redirect_url );
		wp_redirect( $redirect_url );
		exit;
	}
}

add_action( 'login_form_lostpassword', 'redirect_to_custom_password_reset' );

/**
 * Resets the user's password if the password reset form was submitted.
 */
function do_password_reset() {
	if ( 'POST' == $_SERVER['REQUEST_METHOD'] ) {
		$rp_key   = $_REQUEST['rp_key'];
		$rp_login = $_REQUEST['rp_login'];
		$user     = check_password_reset_key( $rp_key, $rp_login );
		if ( ! $user || is_wp_error( $user ) ) {
			$message = get_error_message( $user->get_error_code() );
			nano_basic_modal( $message );
			exit;
		}
		if ( isset( $_POST['pass1'] ) ) {
			if ( empty( $_POST['pass1'] ) ) {
				// Password is empty
				$redirect_url = home_url( 'member-password-reset' );
				$redirect_url = add_query_arg( 'key', $rp_key, $redirect_url );
				$redirect_url = add_query_arg( 'login', $rp_login, $redirect_url );
				$redirect_url = add_query_arg( 'error', 'password_reset_empty', $redirect_url );
				wp_redirect( $redirect_url );
				exit;
			}
			// Parameter checks OK, reset password
			reset_password( $user, $_POST['pass1'] );
			// Auto Login the user now
			auto_login( $user );
			$new_user_check = get_user_meta( $user->ID, '_new_user', true );
			// If new user send them to the welcome page
			if ( '0' !== $new_user_check ) {
				update_user_meta( $user->ID, '_new_user', '0' );

				// Time to redirect to the welcome page
				$url = get_permalink( get_page_by_path( 'welcome' ) );
				wp_safe_redirect( $url );
				exit;
			} else {
				wp_redirect( home_url( '?password=changed' ) );
			}
		} else {
			echo 'Invalid request.';
		}
		exit;
	}
}

function nano_basic_modal( $message ) {

	//get_template_part( 'library/login_register/ajax-auth' );
	include( locate_template( 'library/login_register/ajax-auth.php' ) );

	$content  = '<script type="application/javascript">jQuery(document).ready(function() { jQuery( "#password-form" ).modal( "show" ) });</script>';
	$content .= '<div id="nonce_base_imm"></div>';

	echo $content;
}

/**
 * A shortcode for rendering the form used to reset a user's password.
 *
 * @param  array $attributes Shortcode attributes.
 * @param  string $content The text content for shortcode. Not used.
 *
 * @return string  The shortcode output
 */
function render_password_reset_form( $attributes, $content = null ) {
	if ( isset( $_GET['key'] ) && isset( $_GET['login'] ) ) {
		// Parse shortcode attributes
		$default_attributes = array( 'show_title' => false );
		$attributes         = shortcode_atts( $default_attributes, $attributes );

		if ( is_user_logged_in() ) {
			$message = __( 'You are already signed in.', 'personalize-login' );
			nano_basic_modal( $message );
		} else {
			if ( isset( $_REQUEST['login'] ) && isset( $_REQUEST['key'] ) ) {
				$attributes['login'] = $_REQUEST['login'];
				$attributes['key']   = $_REQUEST['key'];

				$user = check_password_reset_key( $attributes['key'], $attributes['login'] );

				if ( ! $user || is_wp_error( $user ) ) {
					$message = get_error_message( $user->get_error_code() );
					nano_basic_modal( $message );
				}
				// Error messages
				$errors = array();
				if ( isset( $_REQUEST['error'] ) ) {
					$error_codes = explode( ',', $_REQUEST['error'] );
					foreach ( $error_codes as $code ) {
						$errors [] = get_error_message( $code );
					}
				}
				$attributes['errors'] = $errors;
				if ( ! is_wp_error( $user ) ) {
					return get_template_html( 'password_reset_form', $attributes );
				}
			} else {
				$message = __( 'Invalid password reset link', 'personalize-login' );
				nano_basic_modal( $message );
			}
		}
	}
}

/**
 * Renders the contents of the given template to a string and returns it.
 *
 * @param string $template_name The name of the template to render (without .php)
 * @param array $attributes The PHP variables for the template
 *
 * @return string               The contents of the template.
 */
function get_template_html( $template_name, $attributes = null ) {
	if ( ! $attributes ) {
		$attributes = array();
	}
	ob_start();
	do_action( 'personalize_login_before_' . $template_name );
	require( $template_name . '.php' );
	do_action( 'personalize_login_after_' . $template_name );
	$html = ob_get_contents();
	ob_end_clean();

	return $html;
}

/**
 * Finds and returns a matching error message for the given error code.
 *
 * @param string $error_code The error code to look up.
 *
 * @return string               An error message.
 */
function get_error_message( $error_code ) {

	switch ( $error_code ) {
		// Login errors
		case 'empty_username':
			return __( 'You do have an email address, right?', 'personalize-login' );
		case 'empty_password':
			return __( 'You need to enter a password to login.', 'personalize-login' );
		case 'invalid_username':
			return __(
				"We don't have any users with that email address. Maybe you used a different one when signing up?",
				'personalize-login'
			);
		case 'incorrect_password':
			$err = __(
				"The password you entered wasn't quite right. <a href='%s'>Did you forget your password</a>?",
				'personalize-login'
			);

			return sprintf( $err, wp_lostpassword_url() );
		// Registration errors
		case 'email':
			return __( 'The email address you entered is not valid.', 'personalize-login' );
		case 'email_exists':
			return __( 'An account exists with this email address.', 'personalize-login' );
		case 'closed':
			return __( 'Registering new users is currently not allowed.', 'personalize-login' );
		case 'captcha':
			return __( 'The Google reCAPTCHA check failed. Are you a robot?', 'personalize-login' );
		// Lost password
		case 'empty_username':
			return __( 'You need to enter your email address to continue.', 'personalize-login' );
		case 'invalid_email':
		case 'invalidcombo':
			return __( 'There are no users registered with this email address.', 'personalize-login' );
		// Reset password
		case 'expired_key':
			return __( 'The password reset link you used is not valid anymore. Please enter your email address to receive your password.', 'personalize-login' );
		case 'invalid_key':
			return __( 'The password reset link you used is not valid anymore. Please enter your email address to receive your password.', 'personalize-login' );
		case 'password_reset_mismatch':
			return __( "The two passwords you entered don't match.", 'personalize-login' );
		case 'password_reset_empty':
			return __( "Sorry, we don't accept empty passwords.", 'personalize-login' );
		default:
			break;
	}

	return __( 'An unknown error occurred. Please try again later.', 'personalize-login' );
}

function activate_page() {
	// Information needed for creating the plugin's pages
	$page_definitions = array(
		'member-password-reset' => array(
			'title'   => __( 'Pick a New Password', 'personalize-login' ),
			'content' => '[custom-password-reset-form]',
		),
		'member-login'          => array(
			'title'   => __( 'Sign In', 'personalize-login' ),
			'content' => '[custom-login-form]',
		),
	);
	foreach ( $page_definitions as $slug => $page ) {

		if ( ! get_page_by_path( $slug ) ) {

			// Check that the page doesn't exist already
			$query = new WP_Query( 'pagename=' . $slug );
			if ( ! $query->have_posts() ) {
				// Add the page using the data from the array above
				wp_insert_post(
					array(
						'post_content'   => $page['content'],
						'post_name'      => $slug,
						'post_title'     => $page['title'],
						'post_status'    => 'publish',
						'post_type'      => 'page',
						'ping_status'    => 'closed',
						'comment_status' => 'closed',
					)
				);
			}
		}
	}
}

add_action( 'init', 'activate_page' );
add_action( 'login_form_rp', 'redirect_to_custom_password_reset' );
add_action( 'login_form_resetpass', 'redirect_to_custom_password_reset' );
add_action( 'login_form_rp', 'do_password_reset' );
add_action( 'login_form_resetpass', 'do_password_reset' );
add_shortcode( 'custom-password-reset-form', 'render_password_reset_form' );
