<div class="modal fade" tabindex="-1" role="dialog" id="password-reset-form">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<form name="resetpassform" id="resetpassform" action="<?php echo site_url( 'wp-login.php?action=resetpass' ); ?>" method="post" autocomplete="off">
				<input type="hidden" id="user_login" name="rp_login" value="<?php echo esc_attr( $attributes['login'] ); ?>" autocomplete="off" />
				<input type="hidden" name="rp_key" value="<?php echo esc_attr( $attributes['key'] ); ?>" />
				<div class="modal-body text-center pt-0">
					<h5 class="modal-title text-center py-4"><?php _e( 'Pick a New Password', 'personalize-login' ); ?></h5>
					<div style="<?php echo( count( $attributes['errors'] ) > 0 ? '' : 'display:none;' ); ?>" class="nano_status alert alert-info small">
						<?php if ( count( $attributes['errors'] ) > 0 ) : ?>
							<?php foreach ( $attributes['errors'] as $error ) : ?>
								<p>
									<?php echo $error; ?>
								</p>
							<?php endforeach; ?>
						<?php endif; ?>
					</div>
					<?php wp_nonce_field( 'ajax-passwordreset-nonce', 'security' ); ?>
					<div class="form-group formval mb-0">
						<label for="pass1"><?php _e( 'New password', 'personalize-login' ); ?></label>
						<div class="input-group">
							<input type="password" name="pass1" id="pass1" class="form-control" size="24" value="<?php echo esc_attr( wp_generate_password( 16 ) ); ?>" autocomplete="off" aria-describedby="pass-strength-result" />
							<div class="input-group-append">
								<span class="input-group-text" id="toggle-view"><i class="fa fa-eye"></i></span>
							</div>
						</div>
					</div>
					<div class="form-group formval mb-1">
						<div id="pass-strength-result" class="hide-if-no-js p-2 bg-light" aria-live="polite"><?php _e( 'Strength indicator' ); ?></div>
					</div>
					<div class="form-group mb-1">
						<p class="small"><?php echo wp_get_password_hint(); ?></p>
					</div>
					<div class="form-group formval mb-1">
						<div class="d-flex justify-content-center small">
							<div class="custom-control custom-checkbox muted">
								<input type="checkbox" class="custom-control-input required" id="terms_agree"
										name="terms_agree" value="terms_agree" required>
								<label class="custom-control-label" for="terms_agree">I agree to the <a href=""
											data-toggle="modal"
											data-target="#nano-terms-conditions">terms of service</a>
								</label>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input id="resetpass-button" class="submit_button btn btn-sm btn-primary px-3" type="submit" value="<?php _e( 'Reset Password', 'personalize-login' ); ?>">
					<button type="button" class="btn btn-sm btn-secondary px-3" data-dismiss="modal">Cancel</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="nano-terms-conditions">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<?php echo nano_modal_page_load( 'terms-of-service' ); ?>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn-sm btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>