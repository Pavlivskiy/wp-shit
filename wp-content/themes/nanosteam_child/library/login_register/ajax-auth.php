<div class="modal fade" tabindex="-1" role="dialog" id="login-form">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<form id="login" class="log-forms" method="post" action="login">
				<div class="modal-body text-center pt-0">
					<h5 class="modal-title text-center py-4">Sign In</h5>
					<p class="small muted">New to <strong>nanosteam</strong>? <a class="hide-login" id="pop_signup" href=""><strong>Sign Up</strong></a></p>
					<div style="display: none" class="nano_status alert alert-info small"></div>
					<?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
					<div class="form-group formval mb-1">
						<label for="username" class="sr-only">Email Address</label>
						<input id="username" type="email" class="required form-control px-3 mb-1" name="username"
								placeholder="Email Address" autocomplete="username email" required>
					</div>
					<div class="form-group formval mb-1">
						<label for="password" class="sr-only">Password</label>
						<input id="password" type="password" class="required form-control px-3" name="password"
								placeholder="Password" autocomplete="current-password" required>
					</div>
					<a id="pop_forgot" class="btn btn-sm px-2" href="<?php echo wp_lostpassword_url(); ?>"><strong>Lost
							password?</strong></a>
				</div>
				<div class="modal-footer">
					<input class="submit_button btn btn-sm btn-primary" type="submit" value="Login">
					<button type="button" class="cancel_pop_modals btn btn-sm btn-secondary px-3" data-dismiss="modal">Cancel</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="register-form">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<form id="register" class="log-forms" method="post" action="register">
				<div class="modal-body text-center py-0">
					<h5 class="modal-title text-center py-4">Sign Up</h5>
					<p class="small muted">Already have an account? <a id="pop_login" href=""><strong>Login</strong></a></p>
					<div style="display: none" class="nano_status alert alert-info small"></div>
					<?php wp_nonce_field( 'ajax-register-nonce', 'registersecurity' ); ?>
					<div class="form-group formval mb-1">
						<label for="first-name" class="sr-only">First Name</label>
						<input id="first-name" type="text" name="first-name" class="required form-control"
								placeholder="First Name" required>
					</div>
					<div class="form-group formval mb-1">
						<label for="last-name" class="sr-only">Last Name</label>
						<input id="last-name" type="text" name="last-name" class="required form-control"
								placeholder="Last Name" required>
					</div>
					<div class="form-group formval mb-1">
						<label for="email" class="sr-only">Email</label>
						<input id="email" type="text" class="required email form-control" name="email"
								placeholder="Email" autocomplete="username email" required>
					</div>
					<div class="form-group formval mb-1">
						<label for="signonpassword" class="sr-only">Password</label>
						<input id="signonpassword" type="password" class="required form-control" name="signonpassword"
								placeholder="Password" autocomplete="new-password" required>
					</div>
					<div class="form-group formval mb-1">
						<label for="password2" class="sr-only">Confirm Password</label>
						<input type="password" id="password2" class="required form-control" name="password2"
								placeholder="Confirm Password" autocomplete="new-password" required>
					</div>
					<div class="form-group mb-1">
						<div id="captcha1"></div>
					</div>
					<div class="form-group formval mb-1">
						<div class="d-flex justify-content-center small">
							<div class="custom-control custom-checkbox muted">
								<input type="checkbox" class="custom-control-input required" id="terms_agree"
										name="terms_agree" value="terms_agree" required>
								<label class="custom-control-label" for="terms_agree">I agree to the <a href=""
											data-toggle="modal"
											data-target="#nano-terms-conditions">terms of service</a>
								</label>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<input id="submit_r" class="btn btn-sm btn-primary" type="submit" value="Signup">
					<button type="button" class="btn btn-sm btn-secondary px-3" data-dismiss="modal">Cancel</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="password-form">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<form id="forgot_password" action="forgot_password" class="log-forms" method="post">
				<div class="modal-body text-center pt-0">
					<h5 class="modal-title text-center py-4"><?php echo ( isset( $_GET['key'] ) && isset( $_GET['login'] ) ? 'Reset Password' : 'Forgot Password' ); ?></h5>
					<div style="<?php echo( isset( $message ) ? '' : 'display: none' ); ?>" class="nano_status alert alert-info small">
						<?php echo( isset( $message ) ? $message : '' ); ?>
					</div>
					<?php wp_nonce_field( 'ajax-forgot-nonce', 'forgotsecurity' ); ?>
					<div class="form-group formval mb-1">
						<label for="user_login" class="sr-only">Email Address</label>
						<input id="user_login" type="text" class="required form-control px-3" name="user_login"
								placeholder="Email Address" required>
					</div>
				</div>
				<div class="modal-footer">
					<input id="nano-forgotpw" class="btn btn-sm btn-primary" type="submit" value="Submit">
					<button type="button" class="cancel_pop_modals btn btn-sm btn-secondary px-3" data-dismiss="modal">Cancel</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="nano-terms-conditions">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<?php echo nano_modal_page_load( 'terms-of-service' ); ?>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn-sm btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>