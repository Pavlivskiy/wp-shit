jQuery( document ).ready( function( $ ) {
	let formId = jQuery( 'form#login' );
	const forms = jQuery( 'form#login, form#register, form#forgot_password, form#payment-form' );
	const buttons = jQuery( '#pop_forgot, .md-requiredlogin .lostpassword, #show_login, a.show_login, #show_signup, .comment-reply-login, .must-log-in a, #pop_login, #pop_signup, .reveal-login' );

	// As the order of the IDK event comes first, we need to give the following script a higher priority.
	// https://stackoverflow.com/questions/2360655/jquery-event-handlers-always-execute-in-order-they-were-bound-any-way-around-t
	// [name] is the name of the event "click", "mouseover", ..
	// same as you'd pass it to bind()
	// [fn] is the handler function
	$.fn.bindFirst = function( name, fn ) {
		// bind as you normally would
		// don't want to miss out on any jQuery magic
		this.on( name, fn );

		// Thanks to a comment by @Martin, adding support for
		// namespaced events too.
		this.each( function() {
			const handlers = $._data( this, 'events' )[ name.split( '.' )[ 0 ] ];

			//console.log(handlers);
			// take out the handler we just inserted from the end
			const handler = handlers.pop();

			// move it at the beginning
			handlers.splice( 0, 0, handler );
		} );
	};

	if ( jQuery( '#idc_already_purchased' ).length ) {
		jQuery( '#nano-checkout' ).modal( 'show' );
	}

	if ( jQuery( '#payment-form' ).length ) {
		formId = jQuery( '#payment-form' );

		jQuery( '.checkout-wrapper' ).on( 'mouseover', '#payment-form', function( e ) {
			if ( false === jQuery( this ).hasClass( 'secure' ) ) {
				jQuery( '#payment-form #id-main-submit' ).bindFirst( 'click', function( e ) {
					if ( false === formId.hasClass( 'secure' ) ) {
						e.stopImmediatePropagation();
					}
				} );
			}
		} );

		jQuery( '#login-form' ).on( 'hidden.bs.modal', function( e ) {
			formId = jQuery( '#payment-form' );
		} );
	}

	jQuery( '.reveal-login' ).on( 'click', function() {
		formId = jQuery( 'form#login' );
	} );
	jQuery( '#pop_signup' ).on( 'click', function() {
		formId = jQuery( 'form#register' );
	} );
	jQuery( '#pop_forgot' ).on( 'click', function() {
		formId = jQuery( 'form#forgot_password' );
	} );
	jQuery( '#pop_login' ).on( 'click', function() {
		formId = jQuery( 'form#login' );
	} );

	//buttons.on( 'click', variableValidation( formId, flagCheck ) );

	buttons.on( 'click', function() {
		console.log('buttons clicked');
		variableValidation( formId );
	} );

	if ( jQuery( '#nonce_base_imm, #id-main-submit' ).length ) {
		jQuery( document ).on( 'shown.bs.modal', function( e ) {
			formId = jQuery( e.target.getElementsByTagName( 'form' )[ 0 ] );
            console.log('#nonce_base_imm, #id-main-submit clicked');
			variableValidation( formId );
		} );
	}

	// When the user is going to submit the form for review validate the entire form
	function variableValidation( theFormId ) {
		let fvl = '';
		let instantCheckout = '';
		let formInputs = '';

		if ( 'payment-form' === theFormId.attr( 'id' ) ) {
			var fields = {
				'first-name': {
					validators: {
						notEmpty: {
							message: 'This field is required. ',
						},
					},
				},
				'last-name': {
					validators: {
						notEmpty: {
							message: 'This field is required. ',
						},
					},
				},
				user_login: {
					validators: {
						emailAddress: {
							message: 'The value is not a valid email address. ',
						},
					},
				},
				email: {
					validators: {
						notEmpty: {
							message: 'Email is required. ',
						},
						emailAddress: {
							message: 'The value is not a valid email address. ',
						},
					},
				},
				pw: {
					validators: {
						notEmpty: {
							message: 'Password is required. ',
						},
					},
				},
				cpw: {
					validators: {
						notEmpty: {
							message: 'This value must match your password. ',
						},
						identical: {
							compare: function() {
								return jQuery( '#pw' ).val();
							},
							message: 'This value does not match. ',
						},
					},
				},
				nano_cc: {
					validators: {
						notEmpty: {
							message: 'Credit Card is required. ',
						},
						creditCard: {
							message: 'The credit card number is not valid',
						},
					},
				},
				nano_mm: {
					validators: {
						notEmpty: {
							message: 'Month is required. ',
						},
					},
				},
				nano_yy: {
					validators: {
						notEmpty: {
							message: 'Year is required. ',
						},
					},
				},
				nano_cvc: {
					validators: {
						notEmpty: {
							message: 'CVC is required. ',
						},
					},
				},
				nano_zip: {
					validators: {
						notEmpty: {
							message: 'ZIP is required. ',
						},
					},
				},
			};
			instantCheckout = jQuery( '#instant_checkout' );
			formInputs = jQuery( '#payment-form input' );
		} else {
			fields = {
				username: {
					validators: {
						emailAddress: {
							message: 'The value is not a valid email address. ',
						},
					},
				},
				password: {
					validators: {
						notEmpty: {
							message: 'This field cannot be empty. ',
						},
					},
				},
				'first-name': {
					validators: {
						notEmpty: {
							message: 'This field is required. ',
						},
					},
				},
				'last-name': {
					validators: {
						notEmpty: {
							message: 'This field is required. ',
						},
					},
				},
				user_login: {
					validators: {
						emailAddress: {
							message: 'The value is not a valid email address. ',
						},
					},
				},
				email: {
					validators: {
						emailAddress: {
							message: 'The value is not a valid email address. ',
						},
					},
				},
				signonpassword: {
					validators: {
						notEmpty: {
							message: 'Password is required. ',
						},
					},
				},
				password2: {
					validators: {
						notEmpty: {
							message: 'This value must match your password. ',
						},
						identical: {
							compare: function() {
								return jQuery( '#signonpassword' ).val();
							},
							message: 'This value does not match. ',
						},
					},
				},
				terms_agree: {
					validators: {
						notEmpty: {
							message: 'You must agree with the terms and conditions',
						},
					},
				},
			};
		}

		theFormId.formValidation( {
			fields: fields,
			plugins: {
				bootstrap: new FormValidation.plugins.Bootstrap( {

					rowSelector: function( field, ele ) {
						// field is the field name
						// ele is the field element
						switch ( field ) {
							default:
								return '.formval';
						}
					},

				} ),
				trigger: new FormValidation.plugins.Trigger( {
					event: 'keyup change focus blur',
				} ),
				submitButton: new FormValidation.plugins.SubmitButton( {} ),
				sequence: new FormValidation.plugins.Sequence(),
			},
		} );

		if ( 0 < theFormId.length ) {
			fvl = theFormId.data( 'formValidation' );

			// Revalidate the confirmation password when changing the password
			if ( jQuery( '#signonpassword' ).length && 0 < jQuery( '#signonpassword' ).val().length ) {
				jQuery( '#password2' ).on( 'keyup', function() {
					fvl.revalidateField( 'password2', 'identical' );
				} );
			}

			jQuery( '#nano-checkout' ).on( 'keydown', 'input', function() {
				if ( jQuery( '.nano_status_checkout' ).hasClass( 'show' ) ) {
					jQuery( '.nano_status_checkout' ).removeClass( 'show' );
				}
			} );

			var monitor = setInterval( function() {
				const elem = document.activeElement;
				if ( elem && elem.tagName == 'IFRAME' ) {
					clearInterval( monitor );

					if ( jQuery( '.nano_status_checkout' ).hasClass( 'show' ) ) {
						jQuery( '.nano_status_checkout' ).removeClass( 'show' );
					}
				}
			}, 100 );

			// Disable validation fields if the instant checkout checkbox is checked
			if ( jQuery( '#instant' ).length > 0 ) {
				// Check on initial page load, otherwise watch the checkbox for change
				if ( true === jQuery( '#instant_checkout' ).prop( 'checked' ) ) {
					fvl.disableValidator( 'nano_cc' );
					fvl.disableValidator( 'nano_mm' );
					fvl.disableValidator( 'nano_yy' );
					fvl.disableValidator( 'nano_cvc' );
					fvl.disableValidator( 'nano_zip' );
				}

				jQuery( '#instant_checkout' ).change( function() {
					if ( true == jQuery( '#instant_checkout' ).prop( 'checked' ) && ! jQuery( '#instant label' ).hasClass( 'new' ) ) {
						fvl.disableValidator( 'nano_cc' );
						fvl.disableValidator( 'nano_mm' );
						fvl.disableValidator( 'nano_yy' );
						fvl.disableValidator( 'nano_cvc' );
						fvl.disableValidator( 'nano_zip' );
						if ( ! jQuery( '#card-number' ).val().length ) {
							jQuery( '#stripe-input' ).hide();
						}
					} else {
						fvl.enableValidator( 'nano_cc' );
						fvl.enableValidator( 'nano_mm' );
						fvl.enableValidator( 'nano_yy' );
						fvl.enableValidator( 'nano_cvc' );
						fvl.enableValidator( 'nano_zip' );
					}
				} );
			}

			if ( jQuery( '#logged-input' ).length && jQuery( '#logged-input' ).hasClass( 'yes' ) ) {
				fvl.disableValidator( 'first-name' );
				fvl.disableValidator( 'last-name' );
				fvl.disableValidator( 'email' );
				if ( jQuery( 'input[name=pw]' ).length ) {
					fvl.disableValidator( 'pw' );
					fvl.disableValidator( 'cpw' );
				}
			}

			/* ------------------------
                *  Instant Checkout Save for Credit Card
                *
                ------------------------------*/

			// If is
			if ( jQuery( '#first-name' ).length ) {
				jQuery( '.nano_instant_checkout' ).removeClass( 'd-none' );
			}

			if ( instantCheckout.length ) {
				if ( 1 == jQuery( '.pay_selector' ).length && ! jQuery( '#pay-with-credits' ).length ) {
					jQuery( '.nano_instant_checkout' ).removeClass( 'd-none' );
				}

				if ( jQuery( '#pay-with-credits' ).length ) {
					jQuery( '.pay_selector, .checkout-header' ).show();
					jQuery( '#id-main-submit' ).prop( 'disabled', true );
					jQuery( '#instant' ).hide();
				}

				if ( ! jQuery( '#pay-with-credits' ).length ) {
					if ( ! jQuery( '#instant_checkout' ).is( ':checked' ) ) {
						jQuery( '#stripe-input' ).show();
						jQuery( '.nano_instant_checkout' ).removeClass( 'd-none' );
					} else if ( jQuery( '#instant_checkout' ).is( ':checked' ) ) {
						jQuery( '.nano_instant_checkout' ).removeClass( 'd-none' );
					}
				}

				jQuery( '.pay_selector' ).on( 'click', function() {
					jQuery( '#id-main-submit' ).prop( 'disabled', false );
					if ( jQuery( '#pay-with-stripe' ).hasClass( 'active' ) ) {
						jQuery( '.nano_instant_checkout' ).removeClass( 'd-none' );
						jQuery( '#instant' ).show();
						if ( ! jQuery( '#instant_checkout' ).is( ':checked' ) ) {
							jQuery( '#stripe-input' ).show();
						}
					}
					if ( jQuery( '#pay-with-credits' ).hasClass( 'active' ) ) {
						jQuery( '.nano_instant_checkout' ).addClass( 'd-none' );
						jQuery( '#instant' ).hide();
						fvl.disableValidator( 'nano_cc' );
						fvl.disableValidator( 'nano_mm' );
						fvl.disableValidator( 'nano_yy' );
						fvl.disableValidator( 'nano_cvc' );
						fvl.disableValidator( 'nano_zip' );
						if ( ! jQuery( '#card-number' ).val().length ) {
							jQuery( '#stripe-input' ).hide();
						}
					}
				} );

				formInputs.on( 'change keyup', function() {
					if ( ( 1 < jQuery( '#card-number' ).val().length ) && ( 1 < jQuery( '#month' ).val().length ) && ( 1 < jQuery( '#year' ).val().length ) && ( 1 < jQuery( '#card-cvc' ).val().length ) && ( 4 < jQuery( '#card-error' ).val().length ) ) {
						jQuery( '#instant label' ).text( 'Remember this card for future donations' );
					}
				} );

				// Perform AJAX to save card or not
				instantCheckout.on( 'change', function( e ) {
					let security = jQuery( '#nano_instant_checkout_security' ).val(),
						action = 'ajax_instant_checkout',
						instantState = 'off';

					if ( this.checked ) {
						instantState = 'on';
					} else {
						jQuery( '#stripe-input' ).show();
					}

					jQuery.ajax( {
						type: 'POST',
						dataType: 'json',
						url: id_ajaxurl,
						data: {
							action: action,
							security: security,
							instant_checkout: instantState,
						},
						success: function( data ) {

						},
						error: function( data ) {

						},
					} );
					e.preventDefault();
				} );
			}

			// Lets do some ajax. All these functions reside in custom-ajax-auth.php
			fvl.on( 'core.form.invalid', function( e, data ) {
				console.log( 'invalid' );
			} );

			fvl.on( 'core.form.valid', function( e ) {

				let $form = theFormId,
					nanoStatus = jQuery( 'div.nano_status' ),
					action = 'ajax_login',
					username = jQuery( 'form#login #username' ).val(),
					password = jQuery( 'form#login #password' ).val(),
					email = '',
					firstname = '',
					lastname = '',
					security = jQuery( 'form#login #security' ).val(),
					recaptcha = '',
					form = jQuery( 'form#login' ),
					ctrl = theFormId.attr( 'id' ),
					postId = '';

				if ( jQuery( '#instant' ).length && jQuery( '#stripe-input' ).attr( 'data-customer-id' ).length > 0 ) {
					theFormId.addClass( 'secure' );
				}

				if ( 'register' === ctrl ) {
					action = 'ajax_register';
					username = jQuery( '#register #email' ).val();
					password = jQuery( '#signonpassword' ).val();
					firstname = jQuery( '#first-name' ).val();
					lastname = jQuery( '#last-name' ).val();
					email = jQuery( '#email' ).val();
					security = jQuery( '#registersecurity' ).val();
					recaptcha = jQuery( '#g-recaptcha-response' ).val();
					form = jQuery( 'form#register' );
				}
				if ( 'payment-form' === ctrl ) {
					action = 'ajax_purchase';
					username = jQuery( 'input[name="email"]' ).val();
					password = jQuery( 'input[name="pw"]' ).val();
					firstname = jQuery( '#first-name' ).val();
					lastname = jQuery( '#last-name' ).val();
					email = username;
					security = jQuery( '#purchasesecurity' ).val();
					recaptcha = jQuery( '#g-recaptcha-response-1' ).val();
					form = jQuery( 'form#payment-form' );
					nanoStatus = jQuery( 'div.nano_status_checkout' );
					postId = jQuery( '#re-open' ).attr( 'data-id' );
				}
				if ( 'forgot_password' === ctrl ) {
					action = 'ajax_forgotpassword';
					username = jQuery( '#user_login' ).val();
					security = jQuery( '#forgotsecurity' ).val();
					form = jQuery( 'form#forgot_password' );
				}

				if ( ! form.hasClass( 'secure' ) || 'login' === ctrl || 'register' === ctrl || 'forgot_password' === ctrl ) {
					// Use Ajax to submit form data
					nanoStatus.show().text( ajax_auth_object.loadingmessage ).addClass( 'show' );
					if ( jQuery( '#nano-checkout' ).length ) {
						jQuery( '#nano-checkout' ).animate( { scrollTop: 0 }, 1000 );
					}
					console.log( 'login script starting' );
					console.log( new Date( $.now() ) );

					jQuery.ajax( {
						type: 'POST',
						dataType: 'json',
						url: ajax_auth_object.ajaxurl,
						data: {
							action: action,
							username: username,
							password: password,
							first_name: firstname,
							last_name: lastname,
							email: email,
							security: security,
							recaptcha: recaptcha,
							post_id: postId,
						},
						success: function( data ) {
							const Author = 'Sorry'; // Check if returned message contains sorry.
							const message = data.message;

							if ( message.indexOf( Author ) !== -1 ) {
								nanoStatus.html( data.message );
							} else {
								nanoStatus.text( data.message );
							}

							if ( true === data.loggedin ) {
								if ( 'register' === ctrl ) {
									// similar behavior as an HTTP redirect
									document.location.replace( ajax_auth_object.first_login_url );
								}
								if ( 'login' === ctrl ) {
									// similar behavior to an HTTP redirect
									if ( jQuery( '#nonce_base_imm' ).length ) {
										document.location.replace( ajax_auth_object.first_login_url );
									} else if ( jQuery( '#payment-form' ).length ) {
										jQuery( '#payment-form' ).addClass( 'secure' );
										jQuery( '#captcha2' ).remove();
										jQuery( '#login-form' ).modal( 'hide' );
										jQuery( '#payment-form #logged-input' ).addClass( 'yes' );
										jQuery( '#payment-form #logged-input, .login-help' ).hide();
										jQuery( '#payment-form' ).modal( 'show' );
										jQuery( 'input[name="email"]' ).val( jQuery( '#login-form #username' ).val() );
										jQuery( '#first-name' ).val( data.user.fname );
										jQuery( '#last-name' ).val( data.user.lname );
									} else {
										document.location.replace( '' );
									}
								}
								if ( 'payment-form' === ctrl ) {
									form.addClass( 'secure' );
									jQuery( '#captcha2' ).remove();
									jQuery( '#login-form' ).modal( 'hide' );
									jQuery( '#payment-form #logged-input' ).addClass( 'yes' ).hide();
									jQuery( '#id-main-submit' ).click();
								}
								if ( 'forgot_password' === ctrl ) {
									theFormId.find( '#nano-forgotpw' ).val( 'Continue' );
									theFormId.find( '#nano-forgotpw' ).on( 'click', function( e ) {
										e.preventDefault();
										jQuery( '#login-form' ).modal( 'show' );
										jQuery( '#password-form' ).modal( 'hide' );
									} );
								}
							} else if ( 'register' === ctrl || 'payment-form' === ctrl ) {
								grecaptcha.reset();
							}
						},
						error: function( data ) {
							nanoStatus.text( 'Sorry there was an error. Please try again.' );
							console.log( 'error' );
							console.log( data );
						},
					} );
				} else if ( 'payment-form' === ctrl && form.hasClass( 'secure' ) ) {
					//nanoStatus.hide();

					security = jQuery( '#purchasesecurity' ).val();
					action = 'ajax_purchase';
					postId = jQuery( '#re-open' ).attr( 'data-id' );

					// Need to check user
					$.ajax( {
						type: 'POST',
						dataType: 'json',
						url: ajax_auth_object.ajaxurl,
						data: {
							action: action,
							security: security,
							post_id: postId,
							skip: 'skip',
						},
						success: function( data ) {
							const Author = 'Sorry';
							const message = data.message;

							if ( message.indexOf( Author ) !== -1 ) {
								nanoStatus.html( data.message );
							} else {
								nanoStatus.text( data.message );
							}

							if ( true === data.loggedin ) {
								// Not a teammember - continue
								jQuery( '#id-main-submit' ).click();
							} else {
								jQuery( '#id-main-submit' ).prop( 'disabled', true );

								nanoStatus.show().text( ajax_auth_object.loadingmessage ).addClass( 'show' );

								if ( jQuery( '#nano-checkout' ).length ) {
									jQuery( '#nano-checkout' ).animate( { scrollTop: 0 }, 1000 );
								}
							}
						},
						error: function( data ) {
							console.log( 'error' );
							console.log( data );
						},

					} );
				}
			} );
		}
	}

	// Display form from link inside a popup
	jQuery( '#pop_login, #pop_signup' ).on( 'click', function( e ) {
		if ( 'pop_signup' === jQuery( this ).attr( 'id' ) ) {
			jQuery( '#login-form' ).modal( 'hide' );
			jQuery( '#password-form' ).modal( 'hide' );
			if ( ! jQuery( '#card-number' ).length ) {
				jQuery( '#register-form' ).modal( 'show' );
			}
		} else if ( 'pop_login' === jQuery( this ).attr( 'id' ) ) {
			jQuery( '#login-form' ).modal( 'show' );
			jQuery( '#register-form' ).modal( 'hide' );
			jQuery( '#password-form' ).modal( 'hide' );
		} else {
			jQuery( '#login-form' ).modal( 'show' );
		}

		return false;
	} );

	if ( jQuery( '#payment-form' ).length ) {
		jQuery( '.cancel_pop_modals' ).on( 'click', function( e ) {
			jQuery( '#payment-form' ).show();
		} );

		jQuery( '#login-form, #password-form' ).on( 'hidden.bs.modal', function() {
			// do something…
			jQuery( 'body' ).addClass( 'modal-open' );
			if ( ! jQuery( '#login-form, #password-form' ).hasClass( 'show' ) ) {
				jQuery( '#payment-form' ).show();
				if ( jQuery( '.nano_status_checkout' ).hasClass( 'show' ) ) {
					jQuery( '#nano-checkout' ).animate( { scrollTop: 0 }, 1000 );
				}
			}
		} );

		jQuery( '#login-form, #password-form' ).on( 'shown.bs.modal', function() {
			// do something…
			jQuery( '#payment-form' ).hide();
		} );
	}

	jQuery( '#register-form' ).on( 'shown.bs.modal', function( e ) {
		jQuery( 'body' ).addClass( 'modal-open' );
	} );

	// Display lost password form
	jQuery( '#pop_forgot, .md-requiredlogin .lostpassword' ).click( function() {
		jQuery( '#password-form' ).modal( 'show' );
		jQuery( '#register-form' ).modal( 'hide' );
		jQuery( '#login-form' ).modal( 'hide' );
		return false;
	} );

	// Show the login/signup popup on click
	jQuery( '#show_login, a.show_login, #show_signup, .comment-reply-login, .must-log-in a, .reveal-login, .login-redirect' ).on( 'click', function( e ) {
		jQuery( '#login-form' ).modal( 'show' );
		e.preventDefault();
	} );

	if ( jQuery( '#password-reset-form' ).length ) {
		jQuery( '#password-reset-form' ).modal( 'show' );
		jQuery( '#password-reset-form' ).on( 'keyup mouseover', '#pass1, #pass2', function( e ) {
			checkPasswordStrength(
				jQuery( '#pass1' ), // First password field
				jQuery( '#pass-strength-result' ), // Strength meter
				jQuery( 'input[type=submit]' ), // Submit button
				[ 'black', 'listed', 'word' ] // Blacklisted words
			);
			if ( 'pass1' === jQuery( this ).attr( 'id' ) ) {
				jQuery( '#pass2' ).val( jQuery( this ).val() );
			}
			if ( 'pass2' === jQuery( this ).attr( 'id' ) ) {
				jQuery( '#pass1' ).val( jQuery( this ).val() );
			}
		} );
		jQuery( '#pass1' ).after( '<input type="text" name="pass2" id="pass2" class="form-control" value="" autocomplete="off" />' );

		jQuery( '#password-reset-form' ).on( 'shown.bs.modal', function( e ) {
			checkPasswordStrength(
				jQuery( '#pass1' ), // First password field
				jQuery( '#pass-strength-result' ), // Strength meter
				jQuery( 'input[type=submit]' ), // Submit button
				[ 'black', 'listed', 'word' ] // Blacklisted words
			);

			jQuery( '#pass2' ).val( jQuery( this ).val() );
		} );

		jQuery( '#toggle-view' ).on( 'click', function() {
			jQuery( this ).find( 'i' ).toggleClass( 'fa-eye fa-eye-slash' );
			jQuery( this ).closest( '.input-group' ).toggleClass( 'show' );
		} );

		jQuery( 'input[type=submit]' ).on( 'click', function( e ) {
			if ( ! jQuery( '#terms_agree' ).attr( 'checked' ) ) {
				e.stopPropagation();
			}
		} );
	}

	function checkPasswordStrength( $pass1,
		$strengthResult,
		$submitButton,
		blacklistArray ) {
		const pass1 = $pass1.val();
		const pass2 = $pass1.val();

		// Reset the form & meter
		$submitButton.attr( 'disabled', 'disabled' );
		$strengthResult.removeClass( 'short bad good strong' );

		// Extend our blacklist array with those from the inputs & site data
		blacklistArray = blacklistArray.concat( wp.passwordStrength.userInputBlacklist() );

		// Get the password strength
		const strength = wp.passwordStrength.meter( pass1, blacklistArray, pass2 );

		// Add the strength meter results
		switch ( strength ) {
			case 2:
				$strengthResult.removeClass( 'bg-light bg-info bg-success bg-warning' ).addClass( 'bg-danger text-white' ).html( pwsL10n.bad );
				break;

			case 3:
				$strengthResult.removeClass( 'bg-light bg-danger bg-success bg-warning' ).addClass( 'bg-info text-white' ).html( pwsL10n.good );
				break;

			case 4:
				$strengthResult.removeClass( 'bg-light bg-info bg-danger bg-warning' ).addClass( 'bg-success text-white' ).html( pwsL10n.strong );
				break;

			case 5:
				$strengthResult.removeClass( 'bg-light bg-info bg-success bg-danger' ).addClass( 'bg-warning text-white' ).html( pwsL10n.mismatch );
				break;

			default:
				$strengthResult.removeClass( 'bg-light bg-info bg-success bg-danger' ).addClass( 'bg-danger text-white' ).html( pwsL10n.short );
		}

		// The meter function returns a result even if pass2 is empty,
		// enable only the submit button if the password is strong and
		// both passwords are filled up
		if ( 4 === strength && '' !== pass2.trim() ) {
			$submitButton.removeAttr( 'disabled' );
		}

		return strength;
	}
} );

// Google Recaptcha

onloadCallback = function() {
	let correctCaptchaQuote;
	let widgetId1;
	let widgetId2;

	// Renders the HTML element with id 'example1' as a reCAPTCHA widget.
	// The id of the reCAPTCHA widget is assigned to 'widgetId1'.
	if ( document.getElementById( 'captcha1' ) ) {
		widgetId1 = grecaptcha.render( document.getElementById( 'captcha1' ), {
			sitekey: ajax_auth_object.google_recaptcha,
		} );
	}
	if ( document.getElementById( 'captcha2' ) ) {
		correctCaptchaQuote = function( response ) {
			jQuery( '#qt_hiddenRecaptcha' ).val( response );
		};

		widgetId2 = grecaptcha.render( document.getElementById( 'captcha2' ), {
			sitekey: ajax_auth_object.google_recaptcha,

			//'callback' : correctCaptcha_quote
		} );
	}
};
