<?php if ( ! defined( 'ABSPATH' ) ) {
	die( 'Direct access forbidden.' );
}

/**
 * Removes conflicting redirect functions for phpunit to work properly
 */

if (defined('PHPUNIT_NANOSTEAM_TESTSUITE' )) {

	if ( ! function_exists( 'nano_stop_redirect_actions' ) ) {
		function nano_stop_redirect_actions() {

			remove_action( 'admin_init', 'idf_install_flags' );
			remove_action( 'admin_init', 'nano_redirect_admin' );

		}

		add_action( 'admin_init', 'nano_stop_redirect_actions', 0 );
	}

}

// Load nanosteam IDK module
function nano_load_modules() {


	if ( class_exists( 'ID_Modules' ) ) {


		$idc_modules    = new IDC_Modules();
		$active_modules = ID_Modules::get_active_modules();

		if ( true === ( $active_modules === false || true === ( is_array( $active_modules ) && count( $active_modules ) === 0 ) ) || ( true === ( is_array( $active_modules ) && true !== in_array( 'nanosteam', $active_modules ) ) ) ) {

			$id_module = new ID_Modules();

			if ( true === in_array( 'nanosteam', $id_module->get_modules() ) ) {

				$id_module->load_module( 'nanosteam' );
				ID_Modules::set_module_status( 'nanosteam', true );

			}

		}

		if ( true === ( $active_modules === false || true === ( is_array( $active_modules ) && count( $active_modules ) === 0 ) ) && true === in_array( 'anon_checkout', $idc_modules->get_modules() ) ) {
			$id_module->load_module( 'anon_checkout' );
			ID_Modules::set_module_status( 'anon_checkout', true );
		}


	}


}

add_action( 'init', 'nano_load_modules' );


/**
 * Enqueue Child Styles & Scripts
 */

function nano_enqueue() {

	// Styles

	// Cache Breaker
	$version      = filemtime( get_stylesheet_directory() . '/style.css' );
	$version_func = filemtime( get_stylesheet_directory() . '/js/functions.js' );

	// Unified Stylesheet based on SCSS files
	wp_enqueue_style( 'nano-style', get_stylesheet_directory_uri() . '/style.css', false, $version );

	// Custom SVG icons for badges, etc.
	wp_enqueue_style( 'nano-icon', get_stylesheet_directory_uri() . '/library/project_icons/css/fontello.css' );

	//Scripts

	// Bootstrap Related
	wp_register_script( 'popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js', false, '1.11.0', true );
	wp_register_script( 'bootstrap-js', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js', array( 'popper' ), '4.0.0', true );
	wp_enqueue_script( 'popper' );
	wp_enqueue_script( 'bootstrap-js' );
	//wp_register_style('font-awesome-nan', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.7.2/css/all.min.css', '', '5.7.2');
	//wp_enqueue_style('font-awesome-nan');

	// Getting rid of the IgnitionDeck and Backer Styles
	wp_dequeue_style( 'idf' );
	wp_dequeue_style( 'magnific' );
	wp_dequeue_style( 'ignitiondeck-base' );
	wp_dequeue_style( 'ignitiondeck-style' );
	wp_dequeue_style( 'sc_buttons' );
	wp_dequeue_style( 'idcommerce' );
	wp_dequeue_style( 'idfu' );
	//wp_dequeue_style('font-awesome');

	wp_deregister_style( 'idf' );
	wp_deregister_style( 'magnific' );
	wp_deregister_style( 'ignitiondeck-base' );
	wp_deregister_style( 'ignitiondeck-style' );
	wp_deregister_style( 'sc_buttons' );
	wp_deregister_style( 'idcommerce' );
	wp_deregister_style( 'idfu' );
	//wp_deregister_style('font-awesome');

	// Removing unwanted JS scripts from parent and IDK
	// Krown Theme Scripts
	remove_action( 'wp_enqueue_scripts', 'krown_custom_css', 101 );
	remove_action( 'wp_enqueue_scripts', 'krown_enqueue_scripts', 100 );

}

add_action( 'wp_enqueue_scripts', 'nano_enqueue', 90 );


/**
 * Grabbing all localizations and adding them to the single JS file for all JS files.
 */
function nano_localize_enqueue() {

	$version_func        = get_option( 'nano_csp_nonce' );
	$minified_merged_url = get_stylesheet_directory() . '/js/min/minified-nano_localized.js';
	$minified_merged_uri = get_stylesheet_directory_uri() . '/js/min/minified-nano_localized.js';

	wp_register_script( 'nanoScript', get_stylesheet_directory_uri() . '/js/functions.js', array( 'jquery', 'bootstrap-js' ), $version_func, false );
	wp_register_script( 'nano_localized', $minified_merged_uri, '', ( file_exists( $minified_merged_url ) ? filemtime( $minified_merged_url ) : '112234' ), false );

	wp_enqueue_script( 'nanoScript' );
	wp_enqueue_script( 'nano_localized' );

	$user_id              = is_user_logged_in() ? wp_get_current_user()->ID : null;
	$skip_start_a_project = get_user_meta( $user_id, 'nano_start_project_check', true );

	// Localized Variables to JS Scripts
	$data_to_pass = array(
		'posts_pp'   => get_option( 'posts_per_page' ),
		'skip_start' => $skip_start_a_project,
	);
	wp_localize_script( 'nanoScript', 'php_vars', $data_to_pass );

}

add_action( 'wp_enqueue_scripts', 'nano_localize_enqueue', 0 );


/**
 * Dequeue parent theme styles which are no longer needed
 */

function nano_custom_dequeue() {
	// Persisten Krown Backer Styles
	wp_dequeue_style( 'krown-style-parties' );
	wp_dequeue_style( 'krown-style' );
	wp_dequeue_style( 'krown-font-body' );

	wp_deregister_style( 'krown-style-parties' );
	wp_deregister_style( 'krown-style' );
	wp_deregister_style( 'krown-font-body' );
}

add_action( 'wp_enqueue_scripts', 'nano_custom_dequeue', 9999 );
add_action( 'wp_head', 'nano_custom_dequeue', 9999 );



/**
 *  Enqueue Child and Parent Admin Styles
 */

function nanosteam_admin_styles() {

	$parent_style = 'krown-admin'; // This is 'krown-style' for the Backer theme.
	wp_enqueue_style( $parent_style, get_template_directory_uri() . '/css/admin-style.css' );

	wp_enqueue_style( 'nano-admin',
		get_stylesheet_directory_uri() . '/css/admin-style.css',
		array( $parent_style ),
		wp_get_theme()->get( filemtime( get_stylesheet_directory() . '/css/admin-style.css' ) )
	);

	wp_enqueue_style( 'nano-icon-admin',
		get_stylesheet_directory_uri() . '/library/project_icons/css/fontello.css'
	);

	// Bootstrap Related
	wp_register_script( 'popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js', false, '1.11.0', true );
	wp_register_script( 'bootstrap-js', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js', array( 'popper' ), '4.0.0', true );
	wp_enqueue_script( 'popper' );
	wp_enqueue_script( 'bootstrap-js' );

}

add_action( 'admin_enqueue_scripts', 'nanosteam_admin_styles' );



/**
 *  Enqueue Child and Parent Admin Styles
 */

function nanosteam_admin_styles_dequeue() {

	wp_dequeue_style( 'ot-admin-css' );
	wp_deregister_style( 'ot-admin-css' );

}

add_action( 'ot_admin_styles_after', 'nanosteam_admin_styles_dequeue' );


/**
 * Adding single inline style for logo fallback
 */
function nano_logo_style() {


	$logo         = get_template_directory_uri() . '/images/logo.png';
	$svg          = get_stylesheet_directory_uri() . '/images/ns_logo_hz.svg';
	$dasboard_svg = get_stylesheet_directory_uri() . '/images/Icon-Dashboard.svg';
	$dasboard_img = get_stylesheet_directory_uri() . '/images/Icon-Dashboard.png';
	

	//$custom_css = ".dashboard {background-image: url({$dasboard_svg});}.no-svg .dashboard {background-image: url({$dasboard_img});}";
	$custom_css = ".navbar-brand.logo {background-image: url({$svg});}.no-svg .navbar-brand.logo {background-image: url({$logo});}li.dashboard {background-image: url({$dasboard_svg});}.no-svg .dashboard {background-image: url({$dasboard_img});}";

	echo "<style>$custom_css</style>";

}

add_action('wp_head', 'nano_logo_style', 100);

/**
 * Disable the emoji's
 */
function disable_emojis() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
	add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
}
add_action( 'init', 'disable_emojis' );

/**
 * Filter function used to remove the tinymce emoji plugin.
 *
 * @param array $plugins
 * @return array Difference betwen the two arrays
 */
function disable_emojis_tinymce( $plugins ) {
	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} else {
		return array();
	}
}


/**
 * Remove emoji CDN hostname from DNS prefetching hints.
 *
 * @param array $urls URLs to print for resource hints.
 * @param string $relation_type The relation type the URLs are printed for.
 * @return array Difference betwen the two arrays.
 */
function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
	if ( 'dns-prefetch' == $relation_type ) {
		/** This filter is documented in wp-includes/formatting.php */
		$emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );

		$urls = array_diff( $urls, array( $emoji_svg_url ) );
	}

	return $urls;
}

function my_deregister_scripts(){
	wp_dequeue_script( 'wp-embed' );
}
add_action( 'wp_footer', 'my_deregister_scripts' );


/**
 * Nanosteam Project Image
 *
 * @param $post_id
 */
function nano_project_image( $post_id, $size ) {

	if ( has_post_thumbnail( $post_id ) || get_post_meta( $post_id, 'project_main', true ) != '' ) {
		$thumbnail_url = wp_get_attachment_image_src( ( get_post_meta( $post_id, 'project_main', true ) != '' ? get_post_meta( $post_id, 'project_main', true ) : get_post_thumbnail_id( $post_id ) ), $size, true );
		$img_obj       = '<img class="card-img-top img-fluid" src="' . $thumbnail_url[0] . '" width="' . $thumbnail_url[1] . '" height="' . $thumbnail_url[2] . '" alt="' . get_the_title() . '" />';
	} elseif ( get_post_meta( $post_id, 'ign_product_image1', true ) != '' ) {
		$image   = aq_resize( get_post_meta( $post_id, 'ign_product_image1', true ), 'true' === $retina ? 510 : 255, null, false, false );
		$img_obj = '<img class="card-img-top img-fluid" src="' . $image[0] . '" width="' . $image[1] . '" height="' . $image[2] . '" alt="' . get_the_title() . '" />';
	} elseif ( get_post_meta( $post_id, 'ign_product_video', true ) != '' ) {
		$video   = get_post_meta( $post_id, 'ign_product_video', true );
		$video   = '' . check_video_url( $video ) . '';
		$img_obj = '<div class="video-container-box"><img class="video-img" src="' . $video . '" alt="' . get_the_title() . '" /></div>';
	} else {
		$img_obj = '<img style="object-fit: cover;" class="img-fluid mb-sm-0 mb-2" src="' . get_stylesheet_directory_uri() . '/images/dashboard-download-placeholder.jpg' . '" alt="' . get_the_title() . '" />';
	}

	return $img_obj;
}


use MatthiasMullie\Minify;

/**
 * Merge and minify all targeted JS and CSS scripts
 */
function minify_all_scripts() {
	global $wp_scripts;

	//remove_action('wp_head', 'wp_print_scripts');
	//remove_action('wp_head', 'wp_print_head_scripts', 9);
	//remove_action('wp_head', 'wp_enqueue_scripts', 1);

	// make sure to update the path to where you cloned the projects to!
	$path = get_stylesheet_directory() . DIRECTORY_SEPARATOR . 'includes/vendor';
	require_once $path . '/minify/src/Minify.php';
	require_once $path . '/minify/src/CSS.php';
	require_once $path . '/minify/src/JS.php';
	require_once $path . '/minify/src/Exception.php';
	require_once $path . '/minify/src/Exceptions/BasicException.php';
	require_once $path . '/minify/src/Exceptions/FileImportException.php';
	require_once $path . '/minify/src/Exceptions/IOException.php';
	require_once $path . '/path-converter/src/ConverterInterface.php';
	require_once $path . '/path-converter/src/Converter.php';

	/*
		#1. Reorder the handles based on its dependency,
		The result will be saved in the to_do property ($wp_scripts->to_do)
	*/
	$wp_scripts->all_deps( $wp_scripts->queue );

	// New file location: E:xampp\htdocs\wordpresswp-content\theme\wdc\merged-script.js
	$minified_merged_url = get_stylesheet_directory() . '/js/min/minified-nano_localized.js';
	$minified_merged_uri = get_stylesheet_directory_uri() . '/js/min/minified-nano_localized.js';

	$minified_js_merged_url = get_stylesheet_directory() . '/js/min/minified-scripts.js';
	$minified_js_merged_uri = get_stylesheet_directory_uri() . '/js/min/minified-scripts.js';
	$minified_js_merged_uri = get_stylesheet_directory_uri() . '/js/min/minified-scripts.js';


	// Determine if any of the files to be merged have been updated and if so the let the following javascrip loop
	// file to process them again.

	if ( empty( $_SERVER['HTTPS'] ) ) {
		$http_state = 'http:';
	} else {
		$http_state =  'https:';
	}

	$js_func_run    = false;
	$js_cross_check = is_array( get_option( 'jqs_cross_check' ) ) ? get_option( 'jqs_cross_check' ) : array();
	$new_js_files   = count( array_diff( $wp_scripts->to_do, $js_cross_check ) );

	if ( $new_js_files > 0 || get_option( 'http_state' ) !== $http_state ) {
		$difference = array_values( array_diff( $wp_scripts->to_do, $js_cross_check ) );
		$new_array  = array_merge( $difference, $js_cross_check );
		//wp_cache_delete( 'jqs_cross_check' );
		//delete_option( 'jqs_cross_check' );
		update_option( 'jqs_cross_check', $new_array, '', 'no' );
		update_option( 'http_state', $http_state, '', 'no'  );
		$js_func_run = true;
	}

	// Checking Age of files and also if the files already exist as a minified version
	foreach ( $wp_scripts->to_do as $handle ) {

		// New file location: E:xampp\htdocs\wordpresswp-content\theme\wdc\merged-script.js
		$minified_dir = get_stylesheet_directory() . DIRECTORY_SEPARATOR . 'js/min/minified-' . $handle . '.js';

		/*
			Clean up url, for example wp-content/themes/wdc/main.js?v=1.2.4
			become wp-content/themes/wdc/main.js
		*/
		$src = strtok( $wp_scripts->registered[ $handle ]->src, '?' );

		/**
		 * #2. Combine javascript file.
		 */
		// If src is url http / https
		if ( strpos( $src, 'http' ) !== false ) {
			// Get our site url, for example: http://webdevzoom.com/wordpress
			$site_url = site_url();

			/*
				If we are on local server, then change url to relative path,
				e.g. http://webdevzoom.com/wordpress/wp-content/plugins/wpnewsman/css/menuicon.css
				become: /wp-content/plugins/wpnewsman/css/menuicon.css,
				this is for reduse the HTTP Request
				if not, e.g. https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css,
				then leave as is (we'll skip it)
			*/
			if ( strpos( $src, $site_url ) !== false ) {
				$js_file_path = str_replace( $site_url, '', $src );
			} else {
				$js_file_path = $src;
			}

			/*
				To be able to use file_get_contents function we need to remove slash,
				e.g. /wp-content/plugins/wpnewsman/css/menuicon.css
				become wp-content/plugins/wpnewsman/css/menuicon.css
			*/
			$js_file_path = ltrim( $js_file_path, '/' );
		} else {
			$js_file_path = ltrim( $src, '/' );
		}

		// Check if any of the files listed have a newer modifcation date than the merged filed
		if ( file_exists( $js_file_path ) ) {
			if ( strpos( $js_file_path, 'wp-content' ) !== false && strpos( $js_file_path, 'minified' ) === false && strpos( $js_file_path, 'localized' ) === false && strpos( $js_file_path, 'themepunch' ) === false && strpos( $js_file_path, 'query-monitor' ) === false && strpos( $js_file_path, '.min.' ) === false ) {
				if ( file_exists( $minified_dir ) ) {
					if ( filemtime( $js_file_path ) > filemtime( $minified_dir ) ) {
						$js_func_run = true;
					}
				}
				if ( ! file_exists( $minified_dir ) ) {
					$js_func_run = true;
				}
			}
		}
	}

	$minifier_local = new Minify\JS( '' );

	// Loop javascript files and save new minified version

	foreach ( $wp_scripts->to_do as $handle ) {

		// New file location: E:xampp\htdocs\wordpresswp-content\theme\wdc\merged-script.js
		$minified_dir = get_stylesheet_directory() . DIRECTORY_SEPARATOR . 'js/min/minified-' . $handle . '.js';
		// New file location: http://www.website.copm/wordpresswp-content/theme/wdc/script.js
		$minified_uri = get_stylesheet_directory_uri() . DIRECTORY_SEPARATOR . 'js/min/minified-' . $handle . '.js';
		/*
			Clean up url, for example wp-content/themes/wdc/main.js?v=1.2.4
			become wp-content/themes/wdc/main.js
		*/
		$src = strtok( $wp_scripts->registered[ $handle ]->src, '?' );

		/**
		 * #2. Combine javascript file.
		 */
		// If src is url http / https
		if ( strpos( $src, 'http' ) !== false ) {
			// Get our site url, for example: http://webdevzoom.com/wordpress
			$site_url = site_url();

			/*
				If we are on local server, then change url to relative path,
				e.g. http://webdevzoom.com/wordpress/wp-content/plugins/wpnewsman/css/menuicon.css
				become: /wp-content/plugins/wpnewsman/css/menuicon.css,
				this is for reduse the HTTP Request
				if not, e.g. https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css,
				then leave as is (we'll skip it)
			*/
			if ( strpos( $src, $site_url ) !== false ) {
				$js_file_path = str_replace( $site_url, '', $src );
			} else {
				$js_file_path = $src;
			}

			/*
				To be able to use file_get_contents function we need to remove slash,
				e.g. /wp-content/plugins/wpnewsman/css/menuicon.css
				become wp-content/plugins/wpnewsman/css/menuicon.css
			*/
			$js_file_path = ltrim( $js_file_path, '/' );
		} else {
			$js_file_path = ltrim( $src, '/' );
		}

		// Check wether file exists then merge
		if ( file_exists( $js_file_path ) ) {

			// Don't include any WP crit files in the merge
			if ( strpos( $js_file_path, 'wp-content' ) !== false && strpos( $js_file_path, 'query-monitor' ) === false && strpos( $js_file_path, 'themepunch' ) === false && strpos( $js_file_path, '.min.' ) === false ) {

				$file_age = file_exists( $minified_dir ) ? filemtime( $minified_dir ) : 'def';

				if ( empty( $_SERVER['HTTPS'] ) ) {
					$http_state = 'http:';
				} else {
					$http_state = 'https:';
				}

				// Check if any of the files listed have a newer modifcation date than the merged filed

				if ( @key_exists( 'data', $wp_scripts->registered[ $handle ]->extra ) ) {
					$obj      = $wp_scripts->registered[ $handle ];
					$localize = $obj->extra['data'] . '';
					//$minifier_js = new Minify\JS( $localize );
					$minifier_local->add( $localize );
				}

				if ( true == $js_func_run ) {

					$minifier_js = new Minify\JS( $js_file_path );
					$minifier_js->minify( $minified_dir );

				}

				if ( strpos( $js_file_path, 'localized' ) !== false ) {
					$file_age = get_option( 'nano_csp_nonce' );
				}

				// Unregister and dequeue the original scripts and move them to the footer minified
				// We keep the original handle to ensure any php vars are still going to be transferred.
				wp_dequeue_script( $handle );
				wp_deregister_script( $handle );

				$handle = 'min-' . $handle;
				wp_register_script( $handle, $minified_uri, '', $file_age, true );
				wp_enqueue_script( $handle );
			}
		}
	}

	$minifier_local->minify( $minified_merged_url );

}

function minify_all_scripts_new() {
	global $wp_scripts;

	$value     = get_the_ID();
	$first_key = get_the_ID();

	if ( count($_GET) > 0 ) {
		$value     = reset( $_GET );
		$first_key = key( $_GET );
	}

	if ( true == is_single( get_the_ID() ) ) {
		$first_key = 'post_project';
	}

	if ( !is_user_logged_in() ) {
		$first_key = 'unlogged_' . $first_key;
	}

	//remove_action('wp_head', 'wp_print_scripts');
	//remove_action('wp_head', 'wp_print_head_scripts', 9);
	//remove_action('wp_head', 'wp_enqueue_scripts', 1);

	// make sure to update the path to where you cloned the projects to!
	$path = get_stylesheet_directory() . DIRECTORY_SEPARATOR . 'includes/vendor';
	require_once $path . '/minify/src/Minify.php';
	require_once $path . '/minify/src/CSS.php';
	require_once $path . '/minify/src/JS.php';
	require_once $path . '/minify/src/Exception.php';
	require_once $path . '/minify/src/Exceptions/BasicException.php';
	require_once $path . '/minify/src/Exceptions/FileImportException.php';
	require_once $path . '/minify/src/Exceptions/IOException.php';
	require_once $path . '/path-converter/src/ConverterInterface.php';
	require_once $path . '/path-converter/src/Converter.php';

	/*
		#1. Reorder the handles based on its dependency,
		The result will be saved in the to_do property ($wp_scripts->to_do)
	*/
	$wp_scripts->all_deps( $wp_scripts->queue );

	// New file location: E:xampp\htdocs\wordpresswp-content\theme\wdc\merged-script.js
	$minified_merged_url = get_stylesheet_directory() . '/js/min/minified-nano_localized.js';
	$minified_merged_uri = get_stylesheet_directory_uri() . '/js/min/minified-nano_localized.js';

	$minified_js_merged_url = get_stylesheet_directory() . '/js/min/minified-scripts-'.$first_key.'.js';
	$minified_js_merged_uri = get_stylesheet_directory_uri() . '/js/min/minified-scripts-'.$first_key.'.js';

	$minified_wp_js_merged_url = get_stylesheet_directory() . '/js/min/minified-wp-scripts-'.$first_key.'.js';
	$minified_wp_js_merged_uri = get_stylesheet_directory_uri() . '/js/min/minified-wp-scripts-'.$first_key.'.js';


	// Determine if any of the files to be merged have been updated and if so the let the following javascrip loop
	// file to process them again.

	if ( empty( $_SERVER['HTTPS'] ) ) {
		$http_state = 'http:';
	} else {
		$http_state =  'https:';
	}

	$js_func_run    = false;
	$js_cross_check = is_array( get_option( 'jqs_cross_check_' . $first_key ) ) ? get_option( 'jqs_cross_check_' . $first_key ) : array();
	$new_js_files   = count( array_diff( $wp_scripts->to_do, $js_cross_check ) );

	if ( $new_js_files > 0 || get_option( 'http_state' ) !== $http_state ) {
		$difference = array_values( array_diff( $wp_scripts->to_do, $js_cross_check ) );
		$new_array  = array_merge( $difference, $js_cross_check );
		//wp_cache_delete( 'jqs_cross_check' );
		//delete_option( 'jqs_cross_check' );
		update_option( 'jqs_cross_check_' . $first_key, $new_array, '' );
		update_option( 'http_state', $http_state, '' );
		$js_func_run = true;
	}

	// Checking Age of files and also if the files already exist as a minified version
	foreach ( $wp_scripts->to_do as $handle ) {

		// New file location: E:xampp\htdocs\wordpresswp-content\theme\wdc\merged-script.js
		$minified_dir = get_stylesheet_directory() . DIRECTORY_SEPARATOR . 'js/min/minified-' . $handle . '.js';

		/*
			Clean up url, for example wp-content/themes/wdc/main.js?v=1.2.4
			become wp-content/themes/wdc/main.js
		*/
		$src = strtok( $wp_scripts->registered[ $handle ]->src, '?' );

		/**
		 * #2. Combine javascript file.
		 */
		// If src is url http / https
		if ( strpos( $src, 'http' ) !== false ) {
			// Get our site url, for example: http://webdevzoom.com/wordpress
			$site_url = site_url();

			/*
				If we are on local server, then change url to relative path,
				e.g. http://webdevzoom.com/wordpress/wp-content/plugins/wpnewsman/css/menuicon.css
				become: /wp-content/plugins/wpnewsman/css/menuicon.css,
				this is for reduse the HTTP Request
				if not, e.g. https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css,
				then leave as is (we'll skip it)
			*/
			if ( strpos( $src, $site_url ) !== false ) {
				$js_file_path = str_replace( $site_url, '', $src );
			} else {
				$js_file_path = $src;
			}

			/*
				To be able to use file_get_contents function we need to remove slash,
				e.g. /wp-content/plugins/wpnewsman/css/menuicon.css
				become wp-content/plugins/wpnewsman/css/menuicon.css
			*/
			$js_file_path = ltrim( $js_file_path, '/' );
		} else {
			$js_file_path = ltrim( $src, '/' );
		}

		// Check if any of the files listed have a newer modifcation date than the merged filed
		if ( file_exists( $js_file_path ) ) {
			if ( strpos( $js_file_path, 'idcommerec.min.js' ) === false
				 && strpos( $js_file_path, 'query-monitor' ) === false
				 && strpos( $js_file_path, 'google.com' ) === false
				 && strpos( $js_file_path, 'jquery/jquery.js' ) === false
				 && strpos( $js_file_path, 'themepunch' ) === false
			     && strpos( $js_file_path, 'nano_localized' ) === false) {

				if ( strpos( $js_file_path, 'wp-content' ) !== false ) {
					if ( ! file_exists( $minified_js_merged_url ) ) {
						$js_func_run = true;
					} elseif ( filemtime( $js_file_path ) > filemtime( $minified_js_merged_url ) ) {
						$js_func_run = true;
					}

				}

				if ( strpos( $js_file_path, 'wp-includes' ) !== false || strpos( $js_file_path, 'wp-admin' ) !== false ) {

					if ( !file_exists( $minified_wp_js_merged_url ) ) {
						$js_func_run = true;
					} elseif ( filemtime( $js_file_path ) > filemtime( $minified_wp_js_merged_url ) ) {
						$js_func_run = true;
					}

				}
			}
		}
	}

	$minifier_local = new Minify\JS( '' );
	$minifier_js    = new Minify\JS( '' );
	$minifier_wp_js = new Minify\JS( '' );

	wp_register_script( 'merged-min-js-' . $first_key, $minified_js_merged_uri, '', ( file_exists( $minified_js_merged_url ) ? filemtime( $minified_js_merged_url ) : '11234' ), true );
	wp_enqueue_script( 'merged-min-js-' . $first_key );

	wp_register_script( 'merged-wp-min-js-' . $first_key, $minified_wp_js_merged_uri, '', ( file_exists( $minified_wp_js_merged_url ) ? filemtime( $minified_wp_js_merged_url ) : '11234' ), true );
	wp_enqueue_script( 'merged-wp-min-js-' . $first_key );


	// Loop javascript files and save new minified version

	foreach ( $wp_scripts->to_do as $handle ) {

		// New file location: E:xampp\htdocs\wordpresswp-content\theme\wdc\merged-script.js
		$minified_dir = get_stylesheet_directory() . DIRECTORY_SEPARATOR . 'js/min/minified-' . $handle . '.js';
		// New file location: http://www.website.copm/wordpresswp-content/theme/wdc/script.js
		$minified_uri = get_stylesheet_directory_uri() . DIRECTORY_SEPARATOR . 'js/min/minified-' . $handle . '.js';
		/*
			Clean up url, for example wp-content/themes/wdc/main.js?v=1.2.4
			become wp-content/themes/wdc/main.js
		*/
		$src = strtok( $wp_scripts->registered[ $handle ]->src, '?' );

		/**
		 * #2. Combine javascript file.
		 */
		// If src is url http / https
		if ( strpos( $src, 'http' ) !== false ) {
			// Get our site url, for example: http://webdevzoom.com/wordpress
			$site_url = site_url();

			/*
				If we are on local server, then change url to relative path,
				e.g. http://webdevzoom.com/wordpress/wp-content/plugins/wpnewsman/css/menuicon.css
				become: /wp-content/plugins/wpnewsman/css/menuicon.css,
				this is for reduse the HTTP Request
				if not, e.g. https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css,
				then leave as is (we'll skip it)
			*/
			if ( strpos( $src, $site_url ) !== false ) {
				$js_file_path = str_replace( $site_url, '', $src );
			} else {
				$js_file_path = $src;
			}

			/*
				To be able to use file_get_contents function we need to remove slash,
				e.g. /wp-content/plugins/wpnewsman/css/menuicon.css
				become wp-content/plugins/wpnewsman/css/menuicon.css
			*/
			$js_file_path = ltrim( $js_file_path, '/' );
		} else {
			$js_file_path = ltrim( $src, '/' );
		}

		// Check wether file exists then merge
		if ( file_exists( $js_file_path ) ) {

			// Don't include any WP crit files in the merge
			if ( strpos( $js_file_path, 'idcommerec.min.js' ) === false
				 && strpos( $js_file_path, 'query-monitor' ) === false
				 && strpos( $js_file_path, 'google.com' ) === false
				 && strpos( $js_file_path, 'localized' ) === false
				 && strpos( $js_file_path, 'jquery/jquery.js' ) === false
				 && strpos( $js_file_path, 'themepunch' ) === false ) {

				$file_age = file_exists( $minified_dir ) ? filemtime( $minified_dir ) : 'def';

				if ( empty( $_SERVER['HTTPS'] ) ) {
					$http_state = 'http:';
				} else {
					$http_state = 'https:';
				}

				// Check if any of the files listed have a newer modifcation date than the merged filed

				if ( @key_exists( 'data', $wp_scripts->registered[ $handle ]->extra ) ) {
					$obj      = $wp_scripts->registered[ $handle ];
					$localize = $obj->extra['data'] . '';
					//$minifier_js = new Minify\JS( $localize );
					$minifier_local->add( $localize );
				}

				if ( true == $js_func_run ) {


					//$minifier_js = new Minify\JS( $js_file_path );
					//$minifier_js->minify( $minified_dir );

					if ( strpos( $js_file_path, 'wp-content' ) !== false ) {
						$minifier_js->add( $js_file_path );
					}

					if ( strpos( $js_file_path, 'wp-includes' ) !== false || strpos( $js_file_path, 'wp-admin' ) !== false ) {
						$minifier_wp_js->add( $js_file_path );
					}

				}

				if ( strpos( $js_file_path, 'localized' ) !== false ) {
					$file_age = get_option( 'nano_csp_nonce' );
				}

				// Unregister and dequeue the original scripts and move them to the footer minified
				// We keep the original handle to ensure any php vars are still going to be transferred.
				wp_dequeue_script( $handle );
				wp_deregister_script( $handle );

				$handle = 'min-' . $handle;
				//wp_register_script( $handle, $minified_uri, '', $file_age, true );
				//wp_enqueue_script( $handle );
			}
		}
	}

	$minifier_local->minify( $minified_merged_url );

	if ( true == $js_func_run ) {

		$minifier_js->minify( $minified_js_merged_url );
		$minifier_wp_js->minify( $minified_wp_js_merged_url );

	}

}

add_action( 'wp_enqueue_scripts', 'minify_all_scripts_new', 100 );


function nano_merge_styles() {
	// Print all loaded Styles (CSS)
	global $wp_styles;

	// make sure to update the path to where you cloned the projects to!
	$path = get_stylesheet_directory() . DIRECTORY_SEPARATOR . 'includes/vendor';
	require_once $path . '/minify/src/Minify.php';
	require_once $path . '/minify/src/CSS.php';
	require_once $path . '/minify/src/JS.php';
	require_once $path . '/minify/src/Exception.php';
	require_once $path . '/minify/src/Exceptions/BasicException.php';
	require_once $path . '/minify/src/Exceptions/FileImportException.php';
	require_once $path . '/minify/src/Exceptions/IOException.php';
	require_once $path . '/path-converter/src/ConverterInterface.php';
	require_once $path . '/path-converter/src/Converter.php';

	$css_func_run    = false;
	$css_cross_check = is_array( get_option( 'css_cross_check' ) ) ? get_option( 'css_cross_check' ) : array();
	$new_css_files   = count( array_diff( $wp_styles->queue, $css_cross_check ) );

	//send_error('	send_error($new_css_files)');
	//send_error($new_css_files);
	//send_error($wp_styles->queue);

	if ( $new_css_files > 0 ) {

		update_option( 'css_cross_check', $wp_styles->queue );

	}

	// New file location: E:xampp\htdocs\wordpresswp-content\theme\wdc\merged-script.js
	$minified_css_dir = get_stylesheet_directory() . DIRECTORY_SEPARATOR . 'css/minified-merged.css';
	$minified_css_uri = get_stylesheet_directory_uri() . DIRECTORY_SEPARATOR . 'css/minified-merged.css';

	// Determine if any of the files to be merged have been updated and if so the let the following javascrip loop
	// file to process them again.

	if ( empty( $_SERVER['HTTPS'] ) ) {
		$http_state = 'http:';
	} else {
		$http_state = 'https:';
	}

	if ( $new_css_files > 0 || get_option( 'http_state' ) !== $http_state ) {
		//wp_cache_delete( 'css_cross_check' );
		//delete_option( 'css_cross_check' );
		$difference = array_diff( $wp_styles->to_do, $css_cross_check );
		$new_array  = array_merge( $difference, $css_cross_check );
		update_option( 'css_cross_check', $new_array, '', 'no' );
		update_option( 'http_state', $http_state, '', 'no'  );
		$css_func_run = true;
	}

	// Check to see if any of the CSS files are newer than the minified css file
	foreach ( $wp_styles->queue as $style ) {

		$src = $wp_styles->registered[ $style ]->src;

		if ( strpos( $src, 'http' ) !== false ) {
			// Get our site url, for example: http://webdevzoom.com/wordpress
			$site_url = site_url();

			/*
				If we are on local server, then change url to relative path,
				e.g. http://webdevzoom.com/wordpress/wp-content/plugins/wpnewsman/css/menuicon.css
				become: /wp-content/plugins/wpnewsman/css/menuicon.css,
				this is for reduse the HTTP Request
				if not, e.g. https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css,
				then leave as is (we'll skip it)
			*/
			if ( strpos( $src, $site_url ) !== false ) {
				$css_file_path = str_replace( $site_url, '', $src );
			} else {
				$css_file_path = $src;
			}

			/*
				To be able to use file_get_contents function we need to remove slash,
				e.g. /wp-content/plugins/wpnewsman/css/menuicon.css
				become wp-content/plugins/wpnewsman/css/menuicon.css
			*/
			$css_file_path = ltrim( $css_file_path, '/' );
		} else {
			$css_file_path = ltrim( $src, '/' );
		}

		// Checking File Ages VS the merged file

		if ( strpos( $css_file_path, 'wp-content' ) !== false ) {

			if ( file_exists( $minified_css_dir ) ) {
				if ( filemtime( $css_file_path ) > filemtime( $minified_css_dir ) ) {
					$css_func_run = true;
				}
			} else {
				$css_func_run = true;
			}
		}
	}

	$minifier_css = new Minify\CSS( '' );

	// Go through each CSS file in the list and minify and resave.
	// Also will check on the age of the files and resave if required.
	foreach ( $wp_styles->queue as $style ) {


		// New file location: E:xampp\htdocs\wordpresswp-content\theme\wdc\merged-script.js
		//$minified_css_dir = get_stylesheet_directory() . DIRECTORY_SEPARATOR . 'css/minified-' . $style . '.css';
		//$minified_css_uri = get_stylesheet_directory_uri() . DIRECTORY_SEPARATOR . 'css/minified-' . $style . '.css';

		$src = $wp_styles->registered[ $style ]->src;

		if ( strpos( $src, 'http' ) !== false ) {
			// Get our site url, for example: http://webdevzoom.com/wordpress
			$site_url = site_url();

			/*
				If we are on local server, then change url to relative path,
				e.g. http://webdevzoom.com/wordpress/wp-content/plugins/wpnewsman/css/menuicon.css
				become: /wp-content/plugins/wpnewsman/css/menuicon.css,
				this is for reduse the HTTP Request
				if not, e.g. https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css,
				then leave as is (we'll skip it)
			*/
			if ( strpos( $src, $site_url ) !== false ) {
				$css_file_path = str_replace( $site_url, '', $src );
			} else {
				$css_file_path = $src;
			}

			/*
				To be able to use file_get_contents function we need to remove slash,
				e.g. /wp-content/plugins/wpnewsman/css/menuicon.css
				become wp-content/plugins/wpnewsman/css/menuicon.css
			*/
			$css_file_path = ltrim( $css_file_path, '/' );
		} else {
			$css_file_path = ltrim( $src, '/' );
		}

		// Checking File Ages VS the merged file

		if ( strpos( $css_file_path, 'wp-content' ) !== false ) {

			if ( true === $css_func_run || $new_css_files > 0 ) {
				//$minifier_css = new Minify\CSS( $css_file_path );
				//$minifier_css->minify( $minified_css_dir );

				$minifier_css->add( $css_file_path );
			}

			//wp_register_style( $style . '-min', $minified_css_uri, '', filemtime( $minified_css_dir ), '' );
			//wp_enqueue_style( $style . '-min' );
		}
	}

	if ( true === $css_func_run || $new_css_files > 0 ) {
		$minifier_css->minify( $minified_css_dir );
	}
	wp_register_style( 'merged-min', $minified_css_uri, '', filemtime( $minified_css_dir ), '' );
	wp_enqueue_style( 'merged-min' );

	// Go through all the registered styles and take everything off the site except for the new minified versions
	foreach ( $wp_styles->queue as $style ) {

		$src = $wp_styles->registered[ $style ]->src;

		if ( strpos( $src, 'http' ) !== false ) {
			// Get our site url, for example: http://webdevzoom.com/wordpress
			$site_url = site_url();

			/*
				If we are on local server, then change url to relative path,
				e.g. http://webdevzoom.com/wordpress/wp-content/plugins/wpnewsman/css/menuicon.css
				become: /wp-content/plugins/wpnewsman/css/menuicon.css,
				this is for reduse the HTTP Request
				if not, e.g. https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css,
				then leave as is (we'll skip it)
			*/
			if ( strpos( $src, $site_url ) !== false ) {
				$css_file_path = str_replace( $site_url, '', $src );
			} else {
				$css_file_path = $src;
			}

			/*
				To be able to use file_get_contents function we need to remove slash,
				e.g. /wp-content/plugins/wpnewsman/css/menuicon.css
				become wp-content/plugins/wpnewsman/css/menuicon.css
			*/
			$css_file_path = ltrim( $css_file_path, '/' );
		} else {
			$css_file_path = ltrim( $src, '/' );
		}

		if ( strpos( $css_file_path, 'wp-content' ) !== false ) {
			if ( strpos( $css_file_path, 'minified' ) === false ) {
				$styles_to_skip = array( 'logo-style', 'query-monitor' );
				if ( ! in_array( $style, $styles_to_skip ) ) {
					wp_dequeue_style( $style );
					wp_deregister_style( $style );
				}
			}
		}
	}

}

add_action( 'wp_enqueue_scripts', 'nano_merge_styles', 100 );


function add_defer_attribute( $tag, $handle ) {

	// add script handles to the array below
	$scripts_not_to_defer = array( 'jquery-core', 'revmin', 'tp-tools', 'stripe', 'ajax-auth-script', 'nano_localized', 'min-idcommerce-js', 'wp-tinymce-root', 'wp-tinymce' );

	//if ( isset( $_GET['create_project'] ) ) {
		//$scripts_not_to_defer = array_merge( array( 'nanoCroppie', 'nanoCroppie_js' ), $scripts_not_to_defer );
	//}

	if ( !in_array( $handle, $scripts_not_to_defer ) ) {
		return str_replace( ' src', ' defer="defer" src', $tag );
	}

	return $tag;
}

if ( ! is_admin() ) {

	add_filter( 'script_loader_tag', 'add_defer_attribute', 10, 2 );

}


/**
 * The default $editors value:
 *
 * array( 'WP_Image_Editor_Imagick', 'WP_Image_Editor_GD' )
 *
 * @param $editors
 *
 * @return array
 */
function select_wp_image_editors( $editors ) {
	return array( 'WP_Image_Editor_GD', 'WP_Image_Editor_Imagick' );
}

if ( getenv( 'HTTP_HOST' ) != 'nanosteam.site' ) {

	add_filter( 'wp_image_editors', 'select_wp_image_editors' );

}


/**
 * A check based on the local dev ENV to skip some analytics scripts.
 * @return bool
 */
function dev_check() {

	$dev = false;

	if ( getenv( 'HTTP_HOST' ) == 'nanosteam.site' ) {

		$dev = true;

	}

	return $dev;

}




/**
 *  Redirect Subscribers on login if used /wp-admin/ or /wp-login
 *
 * @param $redirect_to
 * @param $request
 * @param $user
 *
 * @return string
 */
function nano_login_redirect( $redirect_to, $request, $user ) {
	//is there a user to check?

	if ( isset( $user->roles ) && is_array( $user->roles ) ) {
		//check for admins
		if ( ! strpos( $redirect_to, 'checkout' ) ) {
			if ( ! in_array( 'administrator', $user->roles ) ) {
				// redirect them to the default place
				$redirect_to = get_permalink( 8 ) . '?edit-profile';
			}
		}
	}

	return $redirect_to;
}

add_filter( 'login_redirect', 'nano_login_redirect', 10, 3 );


/**
 * Redirect Subscribers on logout
 */
function nano_auto_redirect_after_logout() {
	wp_redirect( home_url() );
	exit();
}

add_action( 'wp_logout', 'nano_auto_redirect_after_logout' );


/**
 * Redirect back to homepage and not allow access to
 * WP admin for Subscribers / IDK users.
 */
function nano_redirect_admin() {
	if ( ! defined( 'DOING_AJAX' ) && ! current_user_can( 'edit_posts' ) ) {
		wp_redirect( md_get_durl() . '?edit-profile' );
		exit;
	}
}
// TODO Remove this action for Unit Testing
add_action( 'admin_init', 'nano_redirect_admin' );


/**
 * Format WordPress User's "Display Name" to Full Name on Login
 *
 * @param $username
 */
function nano_format_user_display_name_on_login( $username ) {
	$user = get_user_by( 'login', $username );

	$first_name = get_user_meta( $user->ID, 'first_name', true );
	$last_name  = get_user_meta( $user->ID, 'last_name', true );

	$full_name = trim( $first_name . ' ' . $last_name );

	if ( ! empty( $full_name ) && ( $user->data->display_name != $full_name ) ) {
		$userdata = array(
			'ID'           => $user->ID,
			'display_name' => $full_name,
		);

		wp_update_user( $userdata );
	}
}

add_action( 'wp_login', 'nano_format_user_display_name_on_login' );


/**
 * Remove the "Private" & "Protected" from post titles
 *
 * @param $content
 *
 * @return string
 */
function title_format( $content ) {
	return '%s';
}

add_filter( 'private_title_format', 'title_format' );
add_filter( 'protected_title_format', 'title_format' );


/**
 * vardump & Error log and search scripts
 *
 * @param $i
 */
function view_var( $i ) {
	echo '<pre style="background-color: lightgreen; color: greenyellow !important;  padding: 10px; overflow: scroll; position:relative; z-index: 100000;">';
	echo var_dump( $i );
	echo '</pre>';
}

/**
 * Send values to php error log file
 *
 * @param $i
 */
function send_error( $i ) {
	error_log( print_r( $i, true ) );
}

/**
 * Search in multdimensional arrays by column for specific values.
 *
 * @param $array
 * @param $field
 * @param $value
 *
 * @return bool|int|string
 */
function multi_dimen_array_search( $array, $field, $value ) {
	foreach ( $array as $key => $arr ) {
		if ( is_object( $arr ) ) {
			if ( $arr->{$field} == $value ) {
				return $key;
			}
		} else {
			if ( $arr[$field] == $value ) {
				return $key;
			}
		}
	}

	return false;
}

function multi_dimen_array_search_all_keys( $array, $field, $value ) {
	$keys = array();
	foreach ( $array as $key => $arr ) {
		if ( is_object( $arr ) ) {

			if ( $arr->{$field} == $value ) {
				$keys[] = $arr;
			}
		} else {
			if ( $arr[$field] == $value ) {
				$keys[] = $arr;
			}
		}
	}

	if ( isset( $keys[0] ) ) {
		return $keys;
	} else {
		return false;
	}
}

// Compliles a new array of key names to use to clear option field caches
function nano_array_key_search($array, $key_part_name) {

	$new_array = array();

	foreach ( $array as $key => $value ) {

		if ( stripos( $key , $key_part_name ) !== false ) {
			$new_array[] = $key;
		}

	}

	if ( isset( $new_array[0] ) ) {
		return $new_array;
	} else {
		return false;
	}

}


/**
 *  Allow WEBM file format for uploading
 *
 * @param $mimes
 *
 * @return mixed
 */
function custom_mimes( $mimes ) {
	$mimes['webm'] = 'video/webm';

	return $mimes;
}

add_filter( 'upload_mimes', 'custom_mimes' );



/**
 * Logged In Menu
 */

function nano_logged_in_menu() {

	register_nav_menu( 'logged_in', __( 'Logged In Menu', 'nanosteam_8n' ) );
	register_nav_menu( 'logged_out_mobile', __( 'Logged Out Mobile', 'nanosteam_8n' ) );
	register_nav_menu( 'logged_in_mobile', __( 'Logged In Mobile', 'nanosteam_8n' ) );

}

add_action( 'after_setup_theme', 'nano_logged_in_menu' );

/**
 * Secure headers Content Security Policy
 */


function nano_content_security_policy() {
	// Enforce the use of HTTPS
	header( "Strict-Transport-Security: max-age=31536000; includeSubDomains" );
	// Prevent Clickjacking
	header("X-Frame-Options: SAMEORIGIN");
	// Prevent XSS Attack
	$csp  = "default-src 'self' *.bootstrapcdn.com *.cloudflare.com *.stripe.com *.gstatic.com *.google-analytics.com data: blob:; ";
	$csp .= "frame-ancestors 'self' *.stripe.com; ";
	$csp .= "style-src 'self' 'unsafe-inline' *.bootstrapcdn.com *.googleapis.com *.gstatic.com; ";
	$csp .= "script-src 'self' 'unsafe-inline' 'unsafe-eval' *.cloudflare.com *.cloudflare.com *.bootstrapcdn.com *.stripe.com *.google-analytics.com *.google.com *.gstatic.com *.googletagmanager.com ;  ";
	$csp .= "img-src 'self' 'unsafe-inline' *.youtube.com *.vimeo.com *.google-analytics.com data: blob: ;";
	$csp .= "connect-src 'self' 'unsafe-inline' *.google-analytics.com; ";
	//$csp .= "child-src 'self' 'unsafe-inline' *.google.com *.gstatic.com *.stripe.com; ";
	$csp .= "frame-src  'self' 'unsafe-inline' *.youtube.com *.vimeo.com *.google.com *.gstatic.com *.stripe.com; "; // Duplicate child-src
	header( "X-WebKit-CSP: $csp" ); // Safari testing
	header( "Content-Security-Policy: $csp" ); // FF 23+ Chrome 25+ Safari 7+ Opera 19+
	header( "X-Content-Security-Policy: $csp" ); // IE 10+
	// Block Access If XSS Attack Is Suspected
	header( "X-XSS-Protection: 1; mode=block" );
	// Prevent MIME-Type Sniffing
	header( "X-Content-Type-Options: nosniff" );
	// Referrer Policy
	header( "Referrer-Policy: no-referrer-when-downgrade" );
}

if ( ! wp_doing_ajax() || ! is_admin() ) {
	add_action( 'send_headers', 'nano_content_security_policy', 1 );
}

/**
 *
 *  Force http/s for images in WordPress
 *
 *  Source:
 *  https://core.trac.wordpress.org/ticket/15928#comment:63
 *
 *  @param $url
 *  @param $post_id
 *
 *  @return string
 */
function nano_ssl_post_thumbnail_urls( $url, $post_id ) {

	//Skip file attachments
	if ( ! wp_attachment_is_image( $post_id ) ) {
		return $url;
	}

	//Correct protocol for https connections
	list( $protocol, $uri ) = explode( '://', $url, 2 );

	if ( is_ssl() ) {
		if ( 'http' == $protocol ) {
			$protocol = 'https';
		}
	} else {
		if ( 'https' == $protocol ) {
			$protocol = 'http';
		}
	}

	return $protocol . '://' . $uri;
}

add_filter( 'wp_get_attachment_url', 'nano_ssl_post_thumbnail_urls', 10, 2 );


/**
 * Load a random base64 nonce for use on each page load for all CSP inline scripts.
 */
function nano_script_nonce() {
	update_option( 'nano_csp_nonce', base64_encode( random_str(12, 'abcdefghijklmnopqrstuvwxyz') ) );
}

add_action( 'wp_loaded', 'nano_script_nonce' );

/**
 * Generate a random string, using a cryptographically secure
 * pseudorandom number generator (random_int)
 *
 * For PHP 7, random_int is a PHP core function
 * For PHP 5.x, depends on https://github.com/paragonie/random_compat
 *
 * @param int $length      How many characters do we want?
 * @param string $keyspace A string of all possible characters
 *                         to select from
 * @return string
 */
function random_str($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') {
	$pieces = [];
	$max    = mb_strlen( $keyspace, '8bit' ) - 1;
	for ( $i = 0; $i < $length; ++ $i ) {
		$pieces [] = $keyspace[ random_int( 0, $max ) ];
	}

	return implode( '', $pieces );
}


add_action('check_admin_referer', 'logout_without_confirm', 10, 2);

function logout_without_confirm($action, $result) {
	/**
	 * Allow logout without confirmation
	 */
	if ($action == "log-out" && !isset($_GET['_wpnonce'])) {
		$redirect_to = '/';
		$location = str_replace('&amp;', '&', wp_logout_url($redirect_to));
		header("Location: $location");
		die;
	}
}

/**
 * Add login links
 *
 * @param $items
 * @param $args
 *
 * @return string
 */

if ( ! function_exists( 'krown_add_dashboard_links' ) ) {

	function krown_add_dashboard_links( $items, $args ) {

		/*
		$durl = md_get_durl();

		$dashboard_check = is_page( array( 'dashboard', 'how-it-works', 'about-us' ) ) ? 'active' : '';
		$dasboard_child  = isset( $_GET['creator_projects'] ) || isset( $_GET['edit_project'] ) || isset( $_GET['create_project'] ) ? 'active' : '';
		$myaccount_child = isset( $_GET['payment_settings'] ) || isset( $_GET['edit-profile'] ) || isset( $_GET['idc_orders'] ) ? 'active' : '';
		$howitworks_child = is_page( array( 'how-it-works' ) ) ? 'active' : '';
		$aboutus_child = is_page( array( 'about-us' ) ) ? 'active' : '';
		if ( is_mobile() == 'mobile' ) {
			if ( is_user_logged_in() ) {
				$items .= '<li id="menu-item-131" class="' . $dashboard_check . ' menu-item dropdown dropleft">';
				$items .= '<a id="logged_in_menu" title="Dashboard" href="#" class="dropdown-toggle nav-link dashboard mx-2" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dashboard</a>';
				$items .= '<div class="dropdown-menu fadein" aria-labelledby="logged_in_menu">';
				$items .= '<a title="Projects" href="' . $durl . '?creator_projects=1" class="' . $dasboard_child . ' menu-item dropdown-item" id="menu-item-132">Projects</a>';
				$items .= '<a title="My Account" href="' . $durl . '?edit-profile" class="' . $myaccount_child . ' menu-item  dropdown-item" id="menu-item-133">My Account</a>';
				$items .= '<a title="How it works" href="/how-it-works/" class="' . $howitworks_child . ' menu-item dropdown-item" id="menu-item-manual-134">How it works</a>';
				$items .= '<a title="About Us" href="/about-us/" class="' . $aboutus_child . ' menu-item dropdown-item" id="menu-item-manual-135">About Us</a>';
				$items .= '<a title="Log Out" class="dropdown-item " href="' . wp_logout_url( get_permalink() ) . '">' . __( 'Log Out', 'nanosteam_8n' ) . '</a>';
				$items .= '</div>';
				$items .= '</li>';
			} elseif ( ! is_user_logged_in() ) {
				$items .= '<li class="nav-item"><a title="Sign In" id="show_login" class="dashboard show_login nav-link " href="">' . __( 'Sign In', 'nanosteam_8n' ) . '</a></li>';
			}
		} else {

			if ( is_user_logged_in() ) {
				$items .= '<li id="menu-item-131" class="' . $dashboard_check . ' menu-item dropdown dropleft d-none d-md-inline-block">';
				$items .= '<a id="logged_in_menu" title="Dashboard" href="#" class="dropdown-toggle nav-link dashboard mx-2" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dashboard</a>';
				$items .= '<div class="dropdown-menu fadein" aria-labelledby="logged_in_menu">';
				$items .= '<a title="Projects" href="' . $durl . '?creator_projects=1" class="' . $dasboard_child . ' menu-item dropdown-item" id="menu-item-132">Projects</a>';
				$items .= '<a title="My Account" href="' . $durl . '?edit-profile" class="' . $myaccount_child . ' menu-item  dropdown-item" id="menu-item-133">My Account</a>';
				$items .= '<a title="Log Out" class="dropdown-item " href="' . wp_logout_url( get_permalink() ) . '">' . __( 'Log Out', 'nanosteam_8n' ) . '</a>';
				$items .= '</div>';
				$items .= '</li>';
				$items .= '<li class="nav-item d-block d-md-none"><a title="Log Out" class="nav-link " href="' . wp_logout_url( get_permalink() ) . '">' . __( 'Log Out', 'nanosteam_8n' ) . '</a></li>';
				$items .= '<li class="nav-item d-block d-md-none"><a title="Projects" href="' . $durl . '?creator_projects=1" class="' . $dasboard_child . ' menu-item nav-link" id="menu-item-132">Projects</a></li>';
				$items .= '<li class="nav-item d-block d-md-none"><a title="My Account" href="' . $durl . '?edit-profile=7" class="' . $myaccount_child . ' menu-item nav-link" id="menu-item-133">My Account</a></li>';

			} elseif ( ! is_user_logged_in() ) {
				$items .= '<li class="nav-item"><a title="Sign In" id="show_login" class="show_login nav-link" href="q">' . __( 'Sign In', 'nanosteam_8n' ) . '</a></li>';
			}
		}
		*/
		$items .= '<li class="nav-item"><a class="nav-link"  data-toggle="collapse" href="#collapseForm" aria-expanded="false" aria-controls="collapseForm"><i class="fa fa-search"></i></a></li>';

		return $items;

	}
}

if ( function_exists( 'md_get_durl' ) ) {
	add_filter( 'wp_nav_menu_items', 'krown_add_dashboard_links', 10, 2 );
}


/**
 * Custom commenting function
 *
 * @param $comment
 * @param $args
 * @param $depth
 */
function nano_comment( $comment, $args, $depth ) {

	$author_email = $comment->comment_author_email;
	$user         = get_user_by( 'email', $author_email );
	$user_id      = isset( $user->ID ) ? $user->ID : '';

	// Establishing the projects POST ID
	$post_id     = get_post_meta( $comment->comment_post_ID );
	$project_id  = isset( $post_id['idfu_project_update'] ) ? $post_id['idfu_project_update'][0] : ( isset( $post_id['ign_project_id'] ) ? $post_id['ign_project_id'][0] : '' );
	$project     = new ID_Project( $project_id );
	$the_project = $project->the_project();
	$deck        = new Deck( $project_id );
	$mini_deck   = $deck->mini_deck();
	$post_id     = $mini_deck->post_id;

	// Comment Author
	$name            = isset( $user->display_name ) ? apply_filters( 'ide_profile_name', $user->display_name, $user ) : '';
	$nano_avatar     = get_user_meta( $user_id, 'nano_idc_avatar', true );
	$nano_avatar_img = isset( $nano_avatar[0] ) && is_array( $nano_avatar ) ? $nano_avatar[0] : $nano_avatar;
	$nano_avatar_img = '<img width=50 height=50 class="img-fluid rounded-circle mr-3" src="' . $nano_avatar_img . '">';

	$retina             = krown_retina();
	$GLOBALS['comment'] = $comment;

	switch ( $comment->comment_type ) :
		case '':
			?>

			<div id="comment-<?php comment_ID(); ?>" class="comment col-sm-12">

				<div class="comment-content">

					<div class="media comment-meta mb-2">

						<?php echo '<a href="' . get_author_posts_url( $user_id ) . '">' . $nano_avatar_img . '</a>'; ?>
						<div class="media-body">
							<h6 class="comment-title m-0"><?php echo '<a href="' . get_author_posts_url( $user_id ) . '">' . apply_filters( 'ide_backer_name', $name, $user ) . '</a> <span class="badge badge-light">' . nano_commenter_status( $project_id, $post_id, $user_id ) . '</span>'; ?></h6>
							<div class="small">
								<span class="comment-date mr-2"><?php echo comment_date( __( 'F j, Y', 'krown' ) ); ?></span>
								<?php comment_reply_link( array_merge( $args, array( 'depth'      => $depth,
								                                                     'max_depth'  => 3,
								                                                     'reply_text' => '' . __( 'Reply', 'krown' )
								) ) ); ?>
							</div>
						</div>
					</div>

					<div class="comment-text">

						<?php comment_text(); ?>

						<?php if ( '0' == $comment->comment_approved ) : ?>
							<em class="await"><?php _e( 'Your comment is awaiting moderation.', 'krown' ); ?></em>
						<?php endif; ?>

					</div>

				</div>

			</div>

			<?php
			break;
		case 'pingback':
		case 'trackback':
			?>

			<li class="post pingback">
				<p><?php _e( 'Pingback:', 'krown' ); ?><?php comment_author_link(); ?><?php edit_comment_link( __( '(Edit)', 'krown' ), ' ' ); ?></p>
			</li>
			<?php
			break;
	endswitch;
}


/**
 * If a user has selected to not see the start a project page then automatcially redirect them to the project page
 *
 * @param $items
 *
 * @return mixed
 */
function change_menu( $items ) {

	foreach ( $items as $item ) {

		if ( strtoupper( $item->title ) == 'START A PROJECT' ) {

			$user    = wp_get_current_user();
			$user_id = $user->ID;

			$skip_start_a_project = get_user_meta( $user_id, 'nano_start_project_check', true );

			if ( 'nano_start_project_check' === $skip_start_a_project ) {
				$item->url = get_bloginfo( 'url' ) . '/dashboard/?create_project=1';
			}
		}
	}

	return $items;

}

add_filter( 'wp_nav_menu_objects', 'change_menu' );


/**
 * Adding Revolution Slider above content
 */
function revolution_slider_page_header() {
	$post_id         = get_the_ID();
	$nano_rev_slider = get_post_meta( $post_id, 'nano_revolution_slider_alias', true );
	$device_type     = is_mobile();
	$resolution      = '';

	if ( class_exists( 'RevSlider' ) ) {
		$rev_slider = new RevSlider();
		$sliders    = $rev_slider->getAllSliderAliases();
	} else {
		$sliders = array();
	}

	if ( 'tablet' == $device_type ) {
		$resolution = '-tablet';
		if ( !in_array( $nano_rev_slider . $resolution, $sliders ) ) {
			$resolution = '';
		}
	}
	if ( 'mobile' == $device_type ) {
		$resolution = '-mobile';
		if ( !in_array( $nano_rev_slider . $resolution, $sliders ) ) {
			$resolution = '';
		}
	}

	if ( '' != $nano_rev_slider ) {
		if ( function_exists( 'putRevSlider' ) ) {
			putRevSlider( $nano_rev_slider . $resolution );
		}
	}
}


/**
 * Cropping Uploaded Images to 16x9 format
 */
add_image_size( 'nano-crop-16-9-sm', 835, 446, true );
add_image_size( 'nano-crop-16-9-xs', 400, 214, true );

/**
 * Adding new field to project categories for icon inclusion
 *
 * @param $taxonomy
 */
function project_category_add_meta_fields( $taxonomy ) {
	?>
	<div class="form-field term-group">
		<label for="icon_id"><?php _e( 'Enter the NanoSTEAM icon name (ie: icon-chemistry)', 'nanosteam_8n' ); ?></label>
		<input type="text" id="icon_id" name="icon_id"/>
	</div>
	<div class="form-field term-group">
		<label for="nano_steam_disciplines"><?php _e( 'Select each discipline associated with this category', 'nanosteam_8n' ); ?></label>
		<input type="checkbox" name="nano_steam_disciplines[]" value="science"> Science<br>
		<input type="checkbox" name="nano_steam_disciplines[]" value="technology"> Technology<br>
		<input type="checkbox" name="nano_steam_disciplines[]" value="engineering"> Engineering<br>
		<input type="checkbox" name="nano_steam_disciplines[]" value="art"> Art<br>
		<input type="checkbox" name="nano_steam_disciplines[]" value="math"> Math<br>
	</div>
	<?php
}

add_action( 'project_category_add_form_fields', 'project_category_add_meta_fields', 10, 2 );


/**
 * Editing Project Category Meta Page
 *
 * @param $term
 * @param $taxonomy
 */
function project_category_edit_meta_fields( $term, $taxonomy ) {
	$my_field      = get_term_meta( $term->term_id, 'icon_id', true );
	$my_discipline = get_term_meta( $term->term_id, 'nano_steam_disciplines', false );
	$my_discipline = isset( $my_discipline ) && count( $my_discipline ) > 0 ? $my_discipline[0] : array();
	?>
	<tr class="form-field term-group-wrap">
		<th scope="row">
			<label for="icon_id"><?php _e( 'Enter the NanoSTEAM icon name (ie: icon-chemistry)', 'nanosteam_8n' ); ?></label>
		</th>
		<td>
			<input type="text" id="icon_id" name="icon_id" value="<?php echo $my_field; ?>"/>
		</td>
	</tr>
	<tr class="form-field term-group-wrap">
		<th scope="row">
			<label for="nano_steam_disciplines"><?php _e( 'Select each discipline associated with this category', 'nanosteam_8n' ); ?></label>
		</th>
		<td>
			<input type="checkbox"
					name="nano_steam_disciplines[]" <?php if ( isset( $my_discipline ) && in_array( 'science', $my_discipline ) ) { echo ' checked '; } ?> value="science"> Science<br>
			<input type="checkbox"
					name="nano_steam_disciplines[]" <?php if ( isset( $my_discipline ) && in_array( 'technology', $my_discipline ) ) { echo ' checked '; } ?> value="technology"> Technology<br>
			<input type="checkbox"
					name="nano_steam_disciplines[]" <?php if ( isset( $my_discipline ) && in_array( 'engineering', $my_discipline ) ) { echo ' checked '; } ?> value="engineering"> Engineering<br>
			<input type="checkbox"
					name="nano_steam_disciplines[]" <?php if ( isset( $my_discipline ) && in_array( 'art', $my_discipline ) ) { echo ' checked '; } ?> value="art"> Art<br>
			<input type="checkbox"
					name="nano_steam_disciplines[]" <?php if ( isset( $my_discipline ) && in_array( 'math', $my_discipline ) ) { echo ' checked '; } ?> value="math"> Math<br>
		</td>
	</tr>
	<?php
}

add_action( 'project_category_edit_form_fields', 'project_category_edit_meta_fields', 4, 2 );


/**
 * Saving user category data
 *
 * @param $term_id
 * @param $tag_id
 */
function project_category_save_taxonomy_meta( $term_id, $tag_id ) {
	if ( isset( $_POST['icon_id'] ) ) {
		update_term_meta( $term_id, 'icon_id', esc_attr( $_POST['icon_id'] ) );
	} else {
		update_term_meta( $term_id, 'icon_id', 'icon-' . esc_attr( $_POST['slug'] ) );
	}
	if ( isset( $_POST['nano_steam_disciplines'] ) ) {
		$disciplines = array();
		foreach ( $_POST['nano_steam_disciplines'] as $val ) {
			$disciplines[] = $val;
		}
		update_term_meta( $term_id, 'nano_steam_disciplines', $disciplines );
	}
}

add_action( 'created_project_category', 'project_category_save_taxonomy_meta', 10, 2 );
add_action( 'edited_project_category', 'project_category_save_taxonomy_meta', 10, 2 );



/**
 * Showing the icon in the main column
 *
 * @param $columns
 *
 * @return mixed
 */
function project_category_add_field_columns( $columns ) {
	$columns['icon_id']                = __( 'Icon', 'nanosteam_8n' );
	$columns['nano_steam_disciplines'] = __( 'Discipline', 'nanosteam_8n' );

	return $columns;
}

add_filter( 'manage_edit-project_category_columns', 'project_category_add_field_columns' );



/**
 * Adding the icon content to each row
 *
 * @param $content
 * @param $column_name
 * @param $term_id
 *
 * @return string
 */
function project_category_add_field_column_contents( $content, $column_name, $term_id ) {
	switch ( $column_name ) {
		case 'icon_id':
			$content = '<i class="' . get_term_meta( $term_id, 'icon_id', true ) . '"></i> ' . get_term_meta( $term_id, 'icon_id', true );
			break;
		case 'nano_steam_disciplines':
			$contents = get_term_meta( $term_id, 'nano_steam_disciplines', false );
			if ( isset( $contents[0] ) ) {
				foreach ( $contents[0] as $val ) {
					$content .= $val . ', ';
				}
			}
			break;
	}

	return $content;
}

add_filter( 'manage_project_category_custom_column', 'project_category_add_field_column_contents', 10, 3 );


/**
 * Return the project ID of the default token project which all payments are sent to.
 *
 * @return mixed
 */
function nano_get_default_project_id() {

	$post_id    = get_option( 'default_project' );
	$project_id = get_post_meta( $post_id, 'ign_project_id', true );

	return $project_id;

}





/**
 * Protect Token Payment Project from deletion even from admin
 *
 * @param $post_id
 */
function nano_restrict_post_deletion( $post_id ) {
	$protected_project = get_option( 'default_project' );
	if ( $post_id == $protected_project ) {
		exit( 'The page you were trying to delete is protected.' );
	}
}

add_action( 'wp_trash_post', 'nano_restrict_post_deletion', 10, 1 );
add_action( 'before_delete_post', 'nano_restrict_post_deletion', 10, 1 );


/**
 * Popular Products List
 */
function nano_popular_projects() {

	$query = new WP_Query( array(
		'post_type'      => 'ignition_product',
		'posts_per_page' => - 1,
		'post_status'    => 'publish',
	) );

	while ( $query->have_posts() ) {
		$query->the_post();
		$post_id    = get_the_ID();
		$project_id = get_post_meta( $post_id, 'ign_project_id', true );

		if ( class_exists( 'ID_Project' ) ) {

			//Backers Total
			$project       = new ID_Project( $project_id );
			$backers_total = ID_Order::get_orders_by_project( $project_id );
			$backers_total = isset( $backers_total ) ? count( $backers_total ) : 0; // Total Number of Backers

			//Google Unique Users
			if ( getenv( 'HTTP_HOST' ) == 'nanosteam.staging.wpengine.com' || getenv( 'HTTP_HOST' ) == 'nanosteam.site' ) {
				$daat_file_dir = '/library/google_api/data/nanosteam_api_data_staging.json';
			} else {
				$daat_file_dir = '/library/google_api/data/nanosteam_api_data_live.json';
			}

			$google_api_data = file_get_contents( NANO_STRIPE . $daat_file_dir );
			$google_api_data = str_replace( array( '\u0000*\u0000' ), '', $google_api_data );
			$google_api_data = json_decode( $google_api_data, true );
			$unique_users    = $google_api_data;
			$unique_users    = isset( $unique_users['modelData']['reports'][0]['data']['rows'] ) ? $unique_users['modelData']['reports'][0]['data']['rows'] : null;

			$post_slug              = get_post_field( 'post_name', get_post( $post_id ) );
			$nano_backer_multiplier = get_option( 'nano-backer-multiplier', 300 );

			$counter = 0;

			if ( null !== $unique_users ) {

				foreach ( $unique_users as $user ) {

					$page_id    = $user['dimensions'][0];
					$page_views = $user['metrics'][0]['values'][0]; // Unqiue Page Views

					if ( 0 == $counter ) {
						update_post_meta( $post_id, 'nano_idp_popular', '1' );
					}

					$counter ++;

					if ( strpos( $page_id, $post_slug ) ) {
						$metric = ( $backers_total * $nano_backer_multiplier ) + $page_views;
						update_post_meta( $post_id, 'nano_idp_popular', $metric );
					}
				}
			}
		}
	}

	wp_reset_query();

}

// Adding Endpoint
add_rewrite_endpoint( 'nano_stripe', EP_PERMALINK | EP_PAGES );


/**
 * Filtering the projects on the front end
 */
function nano_filter_form() {

	$obj = get_queried_object();

	$cat_name     = 'ignition_product' != $obj->name ? $obj->name : 'Filter...';
	$selected_zip = ( isset( $_GET['nano_filtering'] ) && isset( $_POST['categoryfilter'] ) && $_POST['categoryfilter'] == 'zip' ? ' selected' : '' );
	$selected_pop = ( isset( $_GET['nano_filtering'] ) && isset( $_POST['categoryfilter'] ) && $_POST['categoryfilter'] == 'popular' ? ' selected' : '' );
	$selected_urg = ( isset( $_GET['nano_filtering'] ) && isset( $_POST['categoryfilter'] ) && $_POST['categoryfilter'] == 'days_left' ? ' selected' : '' );

	?>

	<form action="<?php echo site_url(); ?>/wp-admin/admin-ajax.php" method="POST" id="filter">
		<div class="row pt-lg-5 pb-3">
			<div class="col-md-6 col-sm-4 col-lg-4">
				<div class="input-group">
					<select name="categoryfilter" class="custom-select">
						<option><?php echo $cat_name; ?></option>
						<option <?php echo $selected_zip; ?> value="zip">Distance to you</option>
						<option <?php echo $selected_urg; ?> value="days_left">Days Left</option>
						<option value="featured">Featured</option>
						<option value="DESC">Newest</option>
						<option value="ASC">Oldest</option>
						<option <?php echo $selected_pop; ?> value="popular">Popularity</option>
						<option value="percent_raised">Percent raised</option>
						<option value="funds_raised">Amount raised</option>
					</select>
					<div class="input-group-append">
						<button class="btn btn-primary ml-1">Apply filter</button>
					</div>
					<input type="hidden" name="action" value="nanofilter">
					<input type="hidden" name="page_slug"
							value="<?php echo( isset( get_queried_object()->slug ) ? get_queried_object()->slug : '' ); ?>">
				</div>
			</div>
		</div>
	</form>

	<?php

}


/**
 * The ajax response to the filtered user query
 */
function nano_filter_function() {

	$ppp   = get_option( 'posts_per_page' );
	$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;

	$args = array(
		'post_type'      => 'ignition_product',
		'post_status'    => 'publish',
		'posts_per_page' => $ppp,
		'paged'          => $paged,
	);

	// Define order based on given attributes

	if ( 'days_left' == $_POST['categoryfilter'] ) {
		$args['orderby']  = 'meta_value_num';
		$args['meta_key'] = 'ign_days_left';
		$args['order']    = 'DESC';
	} elseif ( 'percent_raised' == $_POST['categoryfilter'] ) {
		$args['orderby']  = 'meta_value_num';
		$args['meta_key'] = 'ign_percent_raised';
	} elseif ( 'funds_raised' == $_POST['categoryfilter'] ) {
		$args['orderby']  = 'meta_value_num';
		$args['meta_key'] = 'ign_fund_raised';
	} elseif ( 'featured' == $_POST['categoryfilter'] ) {
		$args['orderby']  = 'meta_value_num';
		$args['meta_key'] = 'krown_idp_featured';
	} elseif ( 'popular' == $_POST['categoryfilter'] ) {
		$args['orderby']  = 'meta_value_num';
		$args['meta_key'] = 'nano_idp_popular';
	} elseif ( 'ASC' == $_POST['categoryfilter'] || 'DESC' == $_POST['categoryfilter'] ) {
		$args['order'] = $_POST['categoryfilter'];
	} else {
		$args['orderby'] = 'date';
	}

	if ( isset( $_POST['page_slug'] ) && '' != $_POST['page_slug'] ) {

		$args['tax_query'] = array(
			array(
				'taxonomy' => 'project_category',
				'field'    => 'slug',
				'terms'    => $_POST['page_slug'],
			),
		);
	}

	$distance = null;

	// Order projects by geo location to user
	if ( 'zip' == $_POST['categoryfilter'] ) {

		$args = modify_wp_query_geo_location( $args );

	}

	$query = new WP_Query( $args );

	$_POST['max_pages'] = $query->max_num_pages;
	$_POST['page_num']  = '1';

	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) :
			$query->the_post();

			if ( 'zip' == $_POST['categoryfilter'] ) {
				//Geo Location
				$user             = wp_get_current_user();
				$user_id          = $user->ID;
				$user_geo_meta    = get_user_meta( $user_id, 'nano_location_zip', true );
				$user_info        = '' != $user_geo_meta || false != $user_geo_meta ? (array) get_user_meta( $user_id, 'nano_location_zip', true ) : $_SESSION['user_loc'];
				$user_lat         = isset( $user_info['latitude'] ) ? $user_info['latitude'] : 0;
				$user_long        = isset( $user_info['longitude'] ) ? $user_info['longitude'] : 0;
				$project_lat_long = get_post_meta( get_the_ID(), 'nano_location_zip', true );
				$project_lat      = isset( $project_lat_long->latitude ) ? $project_lat_long->latitude : 0;
				$project_long     = isset( $project_lat_long->longitude ) ? $project_lat_long->longitude : 0;
				$distance         = '' != $project_lat_long || false != $project_lat_long ? (int) geo_distance( $user_lat, $user_long, $project_lat, $project_long ) . ' miles away' : '';
				set_query_var( 'distance', $distance );

			}
			get_template_part( 'content-ignition_product' );
		endwhile;
		wp_reset_postdata();
	} else {
		echo 'No posts found';
	}

	view_more_button( $query );

	if ( !isset($_GET['nano_filtering']) ) {
		wp_die();
	}
}


add_action( 'wp_ajax_nanofilter', 'nano_filter_function' );
add_action( 'wp_ajax_nopriv_nanofilter', 'nano_filter_function' );



/**
 * Lazy Load Paginated Archival Pages
 */
function more_post_ajax() {

	$offset = $_POST['offset'];
	$ppp    = get_option( 'posts_per_page' );
	header( 'Content-Type: text/html' );

	$catfilter = $_POST['vars'][0]['value'];
	$page_slug = isset( $_POST['vars'][2]['value'] ) ? $_POST['vars'][2]['value'] : '';

	$args = array(
		'post_type'      => 'ignition_product',
		'post_status'    => 'publish',
		'posts_per_page' => $ppp,
		'offset'         => $offset,
	);

	// Define order based on given attributes

	if ( 'days_left' == $catfilter ) {
		$args['orderby']  = 'meta_value_num';
		$args['meta_key'] = 'ign_days_left';
		$args['order']    = 'DESC';
	} elseif ( 'percent_raised' == $catfilter ) {
		$args['orderby']  = 'meta_value_num';
		$args['meta_key'] = 'ign_percent_raised';
	} elseif ( 'funds_raised' == $catfilter ) {
		$args['orderby']  = 'meta_value_num';
		$args['meta_key'] = 'ign_fund_raised';
	} elseif ( 'featured' == $catfilter ) {
		$args['orderby']  = 'meta_value_num';
		$args['meta_key'] = 'krown_idp_featured';
	} elseif ( 'popular' == $catfilter ) {
		$args['orderby']  = 'meta_value_num';
		$args['meta_key'] = 'nano_idp_popular';
	} elseif ( 'ASC' == $catfilter || 'DESC' == $catfilter ) {
		$args['order'] = $catfilter;
	} else {
		$args['orderby'] = 'date';
	}

	if ( '' != $page_slug ) {

		$args['tax_query'] = array(
			array(
				'taxonomy' => 'project_category',
				'field'    => 'slug',
				'terms'    => $page_slug,
			),
		);

	}

	$distance = null;

	// Order projects by geo location to user
	if ( 'zip' == $catfilter ) {

		$args = modify_wp_query_geo_location( $args );

	}

	$query = new WP_Query( $args );

	$_POST['max_pages'] = $query->max_num_pages - 1;

	while ( $query->have_posts() ) {
		$query->the_post();

		if ( 'zip' == $catfilter ) {
			//Geo Location
			$user             = wp_get_current_user();
			$user_id          = $user->ID;
			$user_geo_meta    = get_user_meta( $user_id, 'nano_location_zip', true );
			$user_info        = '' != $user_geo_meta || false != $user_geo_meta ? (array) get_user_meta( $user_id, 'nano_location_zip', true ) : $_SESSION['user_loc'];
			$user_lat         = isset( $user_info['latitude'] ) ? $user_info['latitude'] : 0;
			$user_long        = isset( $user_info['longitude'] ) ? $user_info['longitude'] : 0;
			$project_lat_long = get_post_meta( get_the_ID(), 'nano_location_zip', true );
			$project_lat      = isset( $project_lat_long->latitude ) ? $project_lat_long->latitude : 0;
			$project_long     = isset( $project_lat_long->longitude ) ? $project_lat_long->longitude : 0;
			$distance         = '' != $project_lat_long || false != $project_lat_long ? (int) geo_distance( $user_lat, $user_long, $project_lat, $project_long ) . ' miles away' : '';
			set_query_var( 'distance', $distance );

		}

		get_template_part( 'content-ignition_product' );
	}

	view_more_button( $query );

	exit;
}

add_action( 'wp_ajax_nopriv_more_post_ajax', 'more_post_ajax' );
add_action( 'wp_ajax_more_post_ajax', 'more_post_ajax' );



/**
 * Function to hide button via jquery once the end of the projects have been displayed
 *
 * @param $query
 */
function view_more_button( $query ) {

	$page        = $_POST['page_num'];
	$pages       = number_format( $_POST['max_pages'], 0, '.', '' );
	$show_button = '<div id="end" style="display: none"></div>';

	if ( $page >= $pages ) {

	} else {
		$show_button = '';
	}

	echo $show_button;

}



/**
 * Allow project creator to delete their project if it has not been published yet
 */
function nano_project_author_delete_project_ajax() {

	$post_id = isset( $_POST['post_id'] ) ? $_POST['post_id'] : null;

	// First check the nonce, if it fails the function will break
	check_ajax_referer( 'ajax-delete-project-' . $post_id, 'security' );

	$post_id         = isset( $_POST['post_id'] ) ? $_POST['post_id'] : null;
	$current_user    = wp_get_current_user();
	$original_author = get_post_meta( $post_id, 'original_author', true );

	if ( $current_user->data->user_login == $original_author ) {
		wp_trash_post( $post_id );
		echo json_encode(
			array(
				'status'  => 'success',
				'message' => __( 'Your project has been deleted.' ),
			)
		);
	} else {
		echo json_encode(
			array(
				'status'  => 'fail',
				'message' => __( 'Sorry there was a problem and we could not delete this project.' ),
			)
		);
	}

	wp_die();
}

add_action( 'wp_ajax_nopriv_nano_project_author_delete_project_ajax', 'nano_project_author_delete_project_ajax' );
add_action( 'wp_ajax_nano_project_author_delete_project_ajax', 'nano_project_author_delete_project_ajax' );



/**
 * Assign random user icon on account creation
 *
 * @param $user_id
 */
function assign_avatar_init( $user_id ) {

	$random_number = rand( 1, 7 );
	$avatar        = get_stylesheet_directory_uri() . '/images/nanosteam_user_avatar_0' . $random_number . '.png';

	update_user_meta( $user_id, 'nano_idc_avatar', $avatar );

}


/**
 * Assign random user profile url and checks to make sure it doesn't already exist. Uses a 16 character string randomly generated
 *
 * @param $x
 */
function generate_unique_user_nicename( $x ) {

	// set up args to query
	$args = array(
		'search'         => sanitize_title( $x ),
		'search_columns' => array( 'user_nicename' )
	);
	// query for user
	$user_url_exists = new WP_User_Query( $args );
	$results = $user_url_exists->get_results();
	// if user url is found, check for new one
	if( !empty( $results ) ) {
		$i = substr( sanitize_title( $x ), intval( strrpos( sanitize_title( $x ) ,'-' ) ) + 1 );
		if( is_numeric( $i ) ) {
			$i++;
			$y = substr($x, 0, strrpos($x,'-' ));
			$company_name = $y . '-' . $i;
		} else {
			$company_name = $x . '-1';
		}
		$new_company_name = sanitize_title( $company_name );
		return generate_unique_user_nicename( $new_company_name );
	} else {
		return sanitize_title( $x );
	}
}




/**
 * Setup WP-CRON to query the google API
 * https://codex.wordpress.org/Function_Reference/wp_cron
 */
function nano_cron_func_hourly() {
	// Load the token refund system every hour to check if any projects have expired without achieving success and if so to refund the backers with nano tokens
	nano_tokens_refund();
	// Cross check all projects and their levels to ensure payments go to their respective projects.
	nano_level_crosscheck();
}

if ( ! wp_next_scheduled( 'nano_cron_func_hourly' ) ) {
	wp_schedule_event( time(), 'hourly', 'nano_cron_func_hourly' );
}

add_action( 'nano_cron_hook', 'nano_cron_func_hourly' );

if ( ! wp_next_scheduled( 'nano_cron_daily_hook' ) ) {
	wp_schedule_event( time(), 'daily', 'nano_cron_daily_hook' );
}

add_action( 'nano_cron_daily_hook', 'nano_cron_daily_func' );

function nano_cron_daily_func() {
	// Load the Google API to track which projects have seen the most unqiue visits
	//add_action('init', 'nano_popular_projects');
	nano_popular_projects();

	endorsement_reminder(); // Sending out endorsement email reminders to potential endorsers every 3 days up to 21 days.
	project_submission_reminder(); // Sending out reminders to project creators to finish up with their submission. Exprires after 60 days.
	nano_user_token_update_cron(); // Check if a user's tokens have expired or not and if so to expire them.
	delete_option( 'css_cross_check' ); // Refreshing of the styles to ensure we're loading everything with the minifier.
	delete_option( 'jqs_cross_check' ); // Refreshing of the scripts to ensure we're loading everything with the minifier.

	// Refreshing of the scripts to ensure we're loading everything with the minifier.
	$jqs_option_files = ( nano_array_key_search( wp_load_alloptions(), 'jqs_cross_check' ) );
	if ( is_array( $jqs_option_files ) ) {
		foreach ( $jqs_option_files as $jqs_option_file ) {

			wp_cache_delete( $jqs_option_file );
			delete_option( $jqs_option_file );

		}
	}
}


add_filter( 'cron_schedules', 'nano_cron_schedules' );

function nano_cron_schedules( $schedules ) {

	if(!isset($schedules["weekly"])) {
		$schedules['weekly'] = array(
			'interval' => 604800, // 604800 seconds = 1 week
			'display'  => __( 'Once Weekly' ),
		);
	}
	if(!isset($schedules["30min"])){
		$schedules["30min"] = array(
			'interval' => 30*60,
			'display' => __('Once every 30 minutes'));
	}

	return $schedules;
}


/**
 * Join posts and postmeta tables
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_join
 *
 * @param $join
 *
 * @return string
 */
function cf_search_join( $join ) {
	global $wpdb;

	if ( is_search() ) {
		$join .= ' LEFT JOIN ' . $wpdb->postmeta . ' ON ' . $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
	}

	return $join;
}

add_filter( 'posts_join', 'cf_search_join' );

/**
 * Modify the search query with posts_where
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where
 *
 * @param $where
 *
 * @return null|string|string[]
 */
function cf_search_where( $where ) {
	global $pagenow, $wpdb;

	if ( is_search() ) {
		$where = preg_replace(
			'/\(\s*" . $wpdb->posts . ".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/', '(' . $wpdb->posts . '.post_title LIKE $1) OR (" . $wpdb->postmeta . ".meta_value LIKE $1)', $where
		);
	}

	return $where;
}

add_filter( 'posts_where', 'cf_search_where' );

/**
 * Prevent duplicates
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct
 *
 * @param $where
 *
 * @return string
 */
function cf_search_distinct( $where ) {
	global $wpdb;

	if ( is_search() ) {
		return 'DISTINCT';
	}

	return $where;
}

add_filter( 'posts_distinct', 'cf_search_distinct' );


/**
 * Search through two object arrays and display their differences.
 *
 * @param $a1
 * @param $a2
 *
 * @return array
 */
function recursive_array_diff($a1, $a2) {
	$r = array();
	$a1 = (array)$a1;
	$a2 = (array)$a2;

	foreach ($a1 as $k => $v) {
		if (array_key_exists($k, $a2)) {
			if (is_array($v)) {
				$rad = recursive_array_diff($v, $a2[$k]);
				if (count($rad)) { $r[$k] = $rad; }
			} else {
				if ($v != $a2[$k]) {
					$r[$k] = $v;
				}
			}
		} else {
			$r[$k] = $v;
		}
	}
	return $r;
}



/**
 * Pagination
 *
 * @param null $query
 * @param bool $paginated
 * @param int $range
 * @param bool $echo
 *
 * @return string
 */
function krown_pagination( $query = null, $paginated = false, $range = 2, $echo = true ) {

	if ( null == $query ) {
		global $wp_query;
		$query = $wp_query;
	}

	$page  = $query->query_vars['paged'];
	$pages = $query->max_num_pages;

	if ( 0 == $page ) {
		$page = 1;
	}

	$html = '';

	if ( $pages > 1 ) {

		$html .= '<nav aria-label="Page navigation">';
		$html .= '<ul class="pagination">';

		if ( ! $paginated ) {

			if ( $page + 1 <= $pages ) {
				$html .= '<li class="page-item"><a class="prev page-link" href="' . get_pagenum_link( $page + 1 ) . '">' . '<i class="krown-icon-arrow_left"></i>' . __( 'Older Post', 'nanosteam_8n' ) . '</a></li>';
			}

			if ( $page - 1 >= 1 ) {
				$html .= '<li class="page-item"><a class="next page-link" href="' . get_pagenum_link( $page - 1 ) . '">' . __( 'Newer Post', 'nanosteam_8n' ) . '<i class="krown-icon-arrow_right"></i></a></li>';
			}
		} else {

			for ( $i = 1; $i <= $pages; $i ++ ) {

				if ( 1 == $i || $i == $pages || $i == $page || ( $i >= $page - $range && $i <= $page + $range ) ) {
					$html .= '<li class="page-item"><a href="' . get_pagenum_link( $i ) . '"' . ( $page == $i ? ' class="active page-link"' : ' class="page-link"' ) . '>' . $i . '</a></li>';
				} elseif ( ( 1 != $i && $i == $page - $range - 1 ) || ( $i != $page && $i == $page + $range + 1 ) ) {
					$html .= '<li class="page-item"><a class="none page-link">...</a></li>';
				}
			}

			$html .= '<li class="page-item"><a class="next page-link" href="' . get_pagenum_link( $page + 1 ) . '">' . __( 'Next', 'nanosteam_8n' ) . '<i class="krown-icon-arrow_right"></i></a></li>';

		}

		$html .= '</ul>';
		$html .= '</nav>';

	}

	if ( $echo ) {
		echo $html;
	} else {
		return $html;
	}

}


/**
 * Changing the login logo URL and Description
 *
 * @return string
 */
function my_login_logo_url() {
	return home_url();
}

add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo_url_title() {
	return 'Nanosteam Foundation';
}

add_filter( 'login_headertitle', 'my_login_logo_url_title' );


/* ------------------------
 * Single Project Page View Functions
 *
 *
------------------------------*/

/**
 * Share buttons
 *
 * @param $post_id
 */
function krown_share_buttons( $post_id ) {

	global $post;

	$html = '<aside class="share-buttons">';

	//$html .= '<p>' . __( 'Share this', 'nanosteam_8n' ) . ' ' . ( is_singular( 'ignition_product' ) ? __( 'project', 'nanosteam_8n' ) : __( 'post', 'nanosteam_8n' ) ) . '</p>';

	$html .= '<div class="holder clearfix">';

	$url   = urlencode( get_permalink( $post_id ) );
	$title = urlencode( get_the_title( $post_id ) );
	$media = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'large' );

	$stack = '';

	$html .= '<a target="_blank" class="btn-twitter" href="https://twitter.com/home?status=' . $title . '+' . $url . '">';
	$html .= '<span class="fa-stack"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-twitter fa-stack-1x fa-inverse"></i></span></a>';

	$html .= '<a target="_blank" class="btn-facebook" href="https://www.facebook.com/share.php?u=' . $url . '&title=' . $title . '">';
	$html .= '<span class="fa-stack"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-facebook-f fa-stack-1x fa-inverse"></i></span></a>';

	//$html .= '<a target="_blank" class="btn-pinterest" href="http://pinterest.com/pin/create/bookmarklet/?media=' . $media[0] . '&url=' . $url . '&is_video=false&description=' . $title . '"></a>';

	if ( is_singular( 'ignition_product' ) ) {

		$project_id = get_post_meta( $post_id, 'ign_project_id', true );
		$html .= '<a id="share-embed" class="social-share social-sharing" data-toggle="popover" 
		title="Embed this project on a website" 
		data-content="&#60;iframe frameBorder=&#34;0&#34; scrolling=&#34;no&#34; src=&#34;' . home_url() . '/?ig_embed_widget=1&product_no=' . ( isset( $project_id ) ? $project_id : '' ) . '&#34; width=&#34;214&#34; height=&#34;366&#34;&#62;&#60;/iframe&#62;">
		<span class="fa-stack">
		<i class="fa fa-circle fa-stack-2x link-primary"></i>
		<i class="fa fa-code fa-stack-1x fa-inverse"></i>
		</span>
		</a>';

	}


	$html .= '</div></aside>';

	echo $html;

}



/**
 * Checking if the logged in user visiting a project page is a backer, team member or team leader
 *
 * @param $project_id
 * @param $post_id
 * @param $user_id
 *
 * @return string
 */
function nano_commenter_status( $project_id, $post_id, $user_id ) {

	$original_author    = get_post_meta( $post_id, 'original_author', true );
	$original_author_id = get_user_by( 'email', $original_author );
	$original_author_id = isset( $original_author_id->ID ) ? $original_author_id->ID : '';

	// Current user's backed projects
	$user_backed_projects = get_user_meta( $user_id, 'backed_projects', true ) ? get_user_meta( $user_id, 'backed_projects', true ) : array();

	// The project's team members always see all posts
	$project_team = wp_get_post_terms( $post_id, 'author' );

	$team_member_id = array();
	foreach ( $project_team as $team_member ) {
		$team_member_name    = $team_member->name;
		$team_member_details = get_user_by( 'login', $team_member_name );
		$team_member_id[]    = $team_member_details->ID;
	}

	$value = '';

	if ( in_array( $project_id, $user_backed_projects ) ) {

		$value = 'Backer';
	}

	if ( in_array( $user_id, $team_member_id ) ) {

		$value = 'Team Member';

	}

	if ( $original_author_id == $user_id ) {

		$value = 'Team Leader';

	}

	return $value;

}

function nano_project_days_left($end_date = null) {

	$tz = get_option('timezone_string');
	if (empty($tz)) {
		$tz = 'UTC';
	}
	date_default_timezone_set($tz);

	$end_date .= ' 23:59:59';
	$end_date = DateTime::createFromFormat(idf_date_format().' H:i:s', $end_date);
	$days_left = ( date_timestamp_get($end_date) - time() )/60/60/24;

	$days_hours = 'days';
	//echo $days_left;
	if ($days_left < 1) {
		if ($days_left > 0) {
			// convert to hours
			$days_hours = 'hours';
		}
	}

	return $days_hours;
}

/**
 * Append an alert div on the funding section if verification is required
 *
 * @param $sc_params
 *
 * @return array|string
 */

function verification_notify_type( $sc_params ) {

	$data         = isset( $sc_params->verification ) ? $sc_params->verification : '';
	$date         = isset( $data->due_by ) ? ( $data->due_by ? '<br>This is due by: <strong>' . date( 'm/d/Y', $data->due_by ) . '</strong>.' : '' ) : '';
	$verification = isset( $data->fields_needed ) ? ( $data->fields_needed ? $data->fields_needed : array() ) : array();
	$disabled 	  = isset( $data->disabled_reason ) && null !== $data->disabled_reason ? $data->disabled_reason : '';
	$rejected     = strpos( $disabled, 'rejected' ) !== false ? true : false;
	$result       = '';
	$link 		  = isset( $_GET['payment_settings'] ) ? '<a class="alert-link" href="#funding_container">Please see below.</a>' : '<a class="alert-link" href="'. get_site_url() .'/dashboard/?payment_settings=1">Please see here.</a>';

	if ( count( $verification ) > 0 || '' !== $disabled ) {
		$result .= count( $verification ) > 0 ? '<div class="alert alert-danger" role="alert">Your Funding Account has been setup but there are still some requirements before you can begin accepting payments for your projects. ' . $date . ' ' . $link . '</div>' : '';
		$result .= ( '' === $disabled || !strpos( $disabled, 'rejected'  ) ) && 0 == count( $verification ) && true !== $rejected ? '<div class="alert alert-info" role="alert">Stripe is currently reviewing your account. Please check here for updates.</div>' : '';
		$result .= strpos( $disabled, 'rejected'  ) > 0 && 0 == count( $verification ) && true == $rejected ? '<div class="alert alert-danger" role="alert">Stripe has finished with their review has declined to setup this account. Unfortunately you will not be able to use this system.</div>' : '';
		if ( in_array( 'legal_entity.verification.document', $verification, true ) ) {
			$result = array( $result, $date );
		}
		if ( true == $rejected ) {
			$result = array( $result, 'rejected' );
		}
	}

	return $result;
}


/**
 * List of backers on the project
 *
 * @param $project_id
 */
function nano_backers( $project_id ) {

	// Getting crowdfunding project details
	$total_shown    = 5;
	$project        = new ID_Project( $project_id );
	$post_id        = $project->get_project_postid();
	$the_project    = $project->the_project();
	$level_id       = nano_get_level_id( $project_id );
	$project_orders = ID_Member_Order::get_orders_by_level( $level_id );
	$all_orders     = ID_Order::get_total_orders_by_project( $project_id );

	$order_count = $all_orders->count;
	$content     = '';
	if ( ! empty( $project_orders ) ) {

		$mdid_orders = $project_orders;

		$content = '<div id="more" class="ign_backer_list" data-count="' . $order_count . '">';
		if ( ! empty( $mdid_orders ) ) {
			$content .= '<p>Backed by ';
			$i        = 1;
			$ii       = 1;
			$counts   = array_count_values( array_flip( array_column( $mdid_orders, 'user_id' ) ) );

			$order_count  = count( $counts );
			$total_orders = count( $mdid_orders );

			foreach ( $mdid_orders as $key => $mdid_order ) {

				if ( isset( $counts[ $i - 1 ] ) || 1 == $total_orders ) {

					// Getting user info
					$user_info = get_user_by( 'ID', $mdid_order->user_id );
					$order_id  = $mdid_order->id - 1; // This is the cash order

					if ( false !== $user_info ) {

						// Anonymous Check
						$anonymous = ID_Member_Order::get_order_meta( $order_id, 'anonymous_checkout', true );

						$name = false == $anonymous ? $user_info->display_name : __( 'Anonymous', 'nanosteam8n' );

						// Matching Grant Name instead of user name if this project has the grant application and is approved.
						$name = ( 2 == $user_info->ID || 3 == $user_info->ID ) && get_post_meta( $post_id, 'fifty_fifty_grant', true ) == 'approved' ? 'Nanosteam Matching Grant' : $name;

						$ending_punct = '';

						if ( $ii < $total_shown ) {
							$ending_punct = ', ';
						}
						if ( $ii == $order_count - 1 ) {
							$ending_punct = ' and ';
						}
						if ( $ii == $order_count - 1 && $order_count > absint( $total_shown ) ) {
							$ending_punct = ' and <span class="more_backers"> <a id="more_btn" class="" href="#more" data-first="0" data-last="9" data-total="' . $order_count . '" data-project="' . $project_id . '"> ' . ( $order_count - $total_shown ) . __( ' more ', 'nanosteam8n' ) . '</a>' . ( absint( $total_shown + 1 ) == $order_count ? 'backer' : 'backers' ) . '</span>';
						}
						if ( end( $mdid_orders )->id == $mdid_order->id ) {
							$ending_punct = '. ';
						}

						// Writing into table
						$content .= $ii > absint( $total_shown ) && ( $total_shown <= $order_count ) ? '<span class="hidden-backer d-none">' . ( end( $mdid_orders )->id == $mdid_order->id ? '' : ', ' ) . ' ' : '';
						$content .= isset( $user_info->ID ) && false == $anonymous ? '<a class="backer_list' . ( $order_count > absint( $total_shown + 1 ) ? 'new_nano_backer_item' : '' ) . '" href="' . get_author_posts_url( $user_info->ID ) . '">' : '';
						$content .= $name;
						$content .= isset( $user_info->ID ) && false == $anonymous ? '</a>' : '';
						$content .= $ii > absint( $total_shown ) && ( $total_shown <= $order_count ) ? '</span> ' : '';
						$content .= $ending_punct;

						$ii ++;

					}

				}
				$i ++;
			}
		}
		$content .= '</p></div>';
	}
	echo $content;

}


/**
 * WP Text Editor Formats
 *
 * @param $settings
 *
 * @return mixed
 */
function krown_mce_custom_styles( $settings ) {

	$settings['theme_advanced_blockformats'] = 'h1,h2,h3,h4,h5,h6';

	$style_formats = array(
		array( 'title' => 'Heading Light', 'inline' => 'span', 'classes' => 'heading-light' ),
		array( 'title' => 'Heading Normal', 'inline' => 'span', 'classes' => 'heading-normal' ),
		array( 'title' => 'All Caps', 'inline' => 'span', 'classes' => 'text-uppercase' ),
	);

	$settings['style_formats'] = json_encode( $style_formats );

	return $settings;

}


/**
 * Pagewidget
 *
 * @param $attrs
 *
 * @return mixed|null
 */
function nano_id_project_page_widget( $attrs ) {

	$val = null;

	if ( isset( $attrs['product'] ) ) {
		ob_start();
		$project_id = $attrs['product'];
		$deck       = new Deck( $project_id );
		$custom     = false;
		if ( isset( $attrs['deck'] ) ) {
			$deck_id  = $attrs['deck'];
			$settings = Deck::get_deck_attrs( $deck_id );
			if ( ! empty( $settings ) ) {
				$attrs  = unserialize( $settings->attributes );
				$custom = true;
			}
		}
		$the_deck = $deck->the_deck();
		$custom   = apply_filters( 'idcf_custom_deck', $custom, $the_deck->post_id );
		$attrs    = apply_filters( 'idcf_deck_attrs', ( isset( $attrs ) ? $attrs : null ), $the_deck->post_id );
		include 'library/shortcodes/templates/_igWidget.php';
		$output = ob_get_contents();
		ob_end_clean();

		$val = apply_filters( 'id_widget', $output );
	} else {
		$val = null;
	}

	return $val;

}

remove_shortcode( 'project_page_widget', 'id_projectPageWidget' );
add_shortcode( 'project_page_widget', 'nano_id_project_page_widget' );



/**
 * Purchase Now Button
 *
 * @param $args
 *
 * @return mixed
 */
function nano_idc_button( $args ) {
	global $global_currency;
	if ( 'credits' == $global_currency ) {
		$currency_symbol = '$';
	} else {
		$currency_symbol = md_currency_symbol( $global_currency );
	}
	$args = apply_filters( 'idc_button_args', $args );
	do_action( 'idc_button_before', $args );
	// Using GET variable to check if the form is submitted, as we need price as well in GET vars which is in GET var
	if ( isset( $_GET['idc_button_submit'] ) ) {
		// we need to submit some args with this
		$price         = ( isset( $_POST['price'] ) ? sanitize_text_field( $_POST['price'] ) : sanitize_text_field( $_POST['total'] ) );
		$args['price'] = $price;
		do_action( 'idc_button_submit', $args );
	}
	$button  = '<div class="memberdeck">';
	$button .= '<button type="' . ( isset( $args['type'] ) ? $args['type'] : '' ) . '" id="' . ( isset( $args['id'] ) ? $args['id'] : '' ) . '" class="idc_shortcode_button submit-button ' . ( isset( $args['classes'] ) ? $args['classes'] : '' ) . '" ' . ( isset( $args['product'] ) ? 'data-product="' . $args['product'] . '"' : '' ) . ' data-source="' . ( isset( $args['source'] ) ? $args['source'] : '.idc_button_lightbox' ) . '">' . ( isset( $args['text'] ) ? $args['text'] : '' ) . '</button>';
	$button .= '</div>';
	if ( isset( $args['product'] ) ) {
		$product_id = $args['product'];
		if ( strpos( $product_id, ',' ) ) {
			$product_id = explode( ',', $product_id );
		}
		if ( is_array( $product_id ) ) {
			$level = array();
			foreach ( $product_id as $k => $v ) {
				$level[] = ID_Member_Level::get_level( $v );
			}
		} else {
			$level = ID_Member_Level::get_level( $product_id );
		}
		ob_start();
		include_once 'library/shortcodes/templates/_idcButtonContent.php';
		$button .= ob_get_contents();
		ob_clean();
	}
	do_action( 'idc_button_after', $args );

	return apply_filters( 'idc_button', $button, $args );
}

remove_shortcode( 'idc_button', 'idc_button' );
add_shortcode( 'idc_button', 'nano_idc_button' );


/**
 * Replacing the iframe share function
 */
function nano_embedWidget() {
	global $wpdb;
	$tz = get_option('timezone_string');
	if (empty($tz)) {
		$tz = 'UTC';
	}
	date_default_timezone_set($tz);
	$theme_name = getThemeFileName();

	echo "<link rel='stylesheet' id='ignitiondeck-iframe-css'  href='" . get_stylesheet_directory_uri() . "/css/nano-iframe-css.css' type='text/css' media='all' />";
	if (isset($_GET['product_no'])) {
		$project_id = $_GET['product_no'];
	}

	if (!empty($project_id)) {
		$deck = new Deck($project_id);
		$the_deck = $deck->the_deck();
		$post_id = $deck->get_project_postid();

		$project_desc = '' !== get_post_meta( $post_id, 'nano_project_long_description', true ) ? get_post_meta( $post_id, 'nano_project_long_description', true ) : get_post_meta( $post_id, 'ign_project_description', true );


		//GETTING the main settings of ignitiondeck
		$settings = getSettings();
		$logo_on = true;
		if (is_id_pro() && $settings->id_widget_logo_on !== '1') {
			$logo_on = false;
		}

		//GETTING project URL
		$product_url = getProjectURLfromType($project_id);

		echo '<div style="nano-style row">';
		include get_stylesheet_directory() . '/content-ignition_product.php';
		echo '</div>';
	}
	exit;
}
remove_action('init', 'embedWidget');
if (isset($_GET['ig_embed_widget'])) {
	add_action('init', 'nano_embedWidget');
}


/**
 * Loading the page content of pages like terms of service into a modal
 *
 * @param $page_slug
 *
 * @return string
 */
function nano_modal_page_load( $page_slug ) {

	$test = get_page_by_path($page_slug);

	$content =  do_shortcode($test->post_content);

	return $content;

}


/**
 * Detemine if URL is a youtube or vimeo video the run approriate functions to return the video thumb
 *
 * @param $url
 *
 * @return string
 */
function check_video_url( $url ) {

	$val       = '';
	$image_url = parse_url( $url );
	if ( isset( $image_url['host'] ) && ( 'www.youtube.com' == $image_url['host'] || 'youtube.com' == $image_url['host'] || 'youtu.be' == $image_url['host'] ) ) {
		$uid = '/watch' == $image_url['path'] ? explode( 'v=', $image_url['query'] ) : $image_url['path'];
		if ( is_array( $uid ) ) {
			$uid = '/' . $uid[1];
		}

		$val = 'https://img.youtube.com/vi' . $uid . '/0.jpg';

	} elseif ( isset( $image_url['host'] ) && ( 'www.vimeo.com' == $image_url['host'] || 'vimeo.com' == $image_url['host'] ) ) {
		$hash = unserialize( file_get_contents( 'https://vimeo.com/api/v2/video' . $image_url['path'] . '.php' ) );

		$val = $hash[0]['thumbnail_large'];
	}

	return $val;
}


/**
 * Relevanssi User Meta Field Search
 */
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
if ( is_plugin_active( 'relevanssi/relevanssi.php' ) ) {
	add_filter( 'pre_option_relevanssi_index_user_meta', 'rlv_index_user_meta_fields' );
	function rlv_index_user_meta_fields( $value ) {
		return 'description,location,nano_badge,nano_user_title';
	}
}


/**
 * Comparison function to look at the dates from milestones to ensure they are presented in cronological order
 * @param $a
 * @param $b
 *
 * @return false|int
 */
function date_compare( $a, $b ) {
	$t1 = strtotime( $a );
	$t2 = strtotime( $b );

	return $t1 - $t2;
}



/**
 * Creating the zip_code_table
 */
function nano_setup_us_zip_table() {

	// Create the table if it doesn't exist
	global $wpdb;

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

	// Creating the zip code table
	$charset_collate = $wpdb->get_charset_collate();
	$table_name      = $wpdb->prefix . 'zipcodes';

	if ( $wpdb->get_var( "SHOW TABLES LIKE '$table_name'" ) != $table_name ) {

		$sql = "CREATE TABLE $table_name (
            zip_code char(5) NOT NULL,
            city varchar(255) DEFAULT NULL,
            state varchar(2) DEFAULT NULL,
            state_full varchar(30) DEFAULT NULL,
            latitude decimal(12,6) DEFAULT NULL,
            longitude decimal(12,6) DEFAULT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='U.S. Zip Code Database' ROW_FORMAT=COMPACT;";

		maybe_create_table( $table_name, $sql );


		// Getting the zipcode CSV file

		$zipcode_file = get_stylesheet_directory() . '/library/files/US_city_zipcodes.csv';
		$ext          = substr( $zipcode_file, strrpos( $zipcode_file, "." ), ( strlen( $zipcode_file ) - strrpos( $zipcode_file, "." ) ) );

		if ( $ext == ".csv" ) {

			$file = fopen( $zipcode_file, "r" );

			$count = 0;

			while ( ( $emapData = fgetcsv( $file, 10000, "," ) ) !== false ) {

				$zip_details = array(
					'zip_code'   => $emapData[0],
					'city'       => $emapData[1],
					'state'      => $emapData[2],
					'state_full' => nano_us_states( $emapData[2] ),
					'latitude'   => $emapData[3],
					'longitude'  => $emapData[4],
				);

				$wpdb->insert( $table_name, $zip_details );

				$count++;

				// The CSV file does NOT include long state names. We'll add them here to the table.

			}
			fclose( $file );
			send_error( "CSV File has been successfully Updated." );
		}

	} else {

		// DB Table is already created and we only need to add in the full state name to the table.
		$row = $wpdb->get_results( "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '$table_name' AND column_name = 'state_full'" );

		// Check to see if the column is already created
		if ( empty( $row ) ) {
			$result = $wpdb->query( "ALTER TABLE $table_name ADD state_full varchar(30) NOT NULL" );

			nano_debug_query( $result, '"ALTER TABLE $table_name ADD state_full varchar(30) NOT NULL"', 'nano_setup_us_zip_table' );
		}

		$us_states   = array();
		$us_states[] = array( 'value' => 'AL', 'title' => 'Alabama' );
		$us_states[] = array( 'value' => 'AK', 'title' => 'Alaska' );
		$us_states[] = array( 'value' => 'AZ', 'title' => 'Arizona' );
		$us_states[] = array( 'value' => 'AR', 'title' => 'Arkansas' );
		$us_states[] = array( 'value' => 'CA', 'title' => 'California' );
		$us_states[] = array( 'value' => 'CO', 'title' => 'Colorado' );
		$us_states[] = array( 'value' => 'CT', 'title' => 'Connecticut' );
		$us_states[] = array( 'value' => 'DE', 'title' => 'Delaware' );
		$us_states[] = array( 'value' => 'DC', 'title' => 'District Of Columbia' );
		$us_states[] = array( 'value' => 'FL', 'title' => 'Florida' );
		$us_states[] = array( 'value' => 'GA', 'title' => 'Georgia' );
		$us_states[] = array( 'value' => 'HI', 'title' => 'Hawaii' );
		$us_states[] = array( 'value' => 'ID', 'title' => 'Idaho' );
		$us_states[] = array( 'value' => 'IL', 'title' => 'Illinois' );
		$us_states[] = array( 'value' => 'IN', 'title' => 'Indiana' );
		$us_states[] = array( 'value' => 'IA', 'title' => 'Iowa' );
		$us_states[] = array( 'value' => 'KS', 'title' => 'Kansas' );
		$us_states[] = array( 'value' => 'KY', 'title' => 'Kentucky' );
		$us_states[] = array( 'value' => 'LA', 'title' => 'Louisiana' );
		$us_states[] = array( 'value' => 'ME', 'title' => 'Maine' );
		$us_states[] = array( 'value' => 'MD', 'title' => 'Maryland' );
		$us_states[] = array( 'value' => 'MA', 'title' => 'Massachusetts' );
		$us_states[] = array( 'value' => 'MI', 'title' => 'Michigan' );
		$us_states[] = array( 'value' => 'MN', 'title' => 'Minnesota' );
		$us_states[] = array( 'value' => 'MS', 'title' => 'Mississippi' );
		$us_states[] = array( 'value' => 'MO', 'title' => 'Missouri' );
		$us_states[] = array( 'value' => 'MT', 'title' => 'Montana' );
		$us_states[] = array( 'value' => 'NE', 'title' => 'Nebraska' );
		$us_states[] = array( 'value' => 'NV', 'title' => 'Nevada' );
		$us_states[] = array( 'value' => 'NH', 'title' => 'New Hampshire' );
		$us_states[] = array( 'value' => 'NJ', 'title' => 'New Jersey' );
		$us_states[] = array( 'value' => 'NM', 'title' => 'New Mexico' );
		$us_states[] = array( 'value' => 'NY', 'title' => 'New York' );
		$us_states[] = array( 'value' => 'NC', 'title' => 'North Carolina' );
		$us_states[] = array( 'value' => 'ND', 'title' => 'North Dakota' );
		$us_states[] = array( 'value' => 'OH', 'title' => 'Ohio' );
		$us_states[] = array( 'value' => 'OK', 'title' => 'Oklahoma' );
		$us_states[] = array( 'value' => 'OR', 'title' => 'Oregon' );
		$us_states[] = array( 'value' => 'PA', 'title' => 'Pennsylvania' );
		$us_states[] = array( 'value' => 'RI', 'title' => 'Rhode Island' );
		$us_states[] = array( 'value' => 'SC', 'title' => 'South Carolina' );
		$us_states[] = array( 'value' => 'SD', 'title' => 'South Dakota' );
		$us_states[] = array( 'value' => 'TN', 'title' => 'Tennessee' );
		$us_states[] = array( 'value' => 'TX', 'title' => 'Texas' );
		$us_states[] = array( 'value' => 'UT', 'title' => 'Utah' );
		$us_states[] = array( 'value' => 'VT', 'title' => 'Vermont' );
		$us_states[] = array( 'value' => 'VA', 'title' => 'Virginia' );
		$us_states[] = array( 'value' => 'WA', 'title' => 'Washington' );
		$us_states[] = array( 'value' => 'WV', 'title' => 'West Virginia' );
		$us_states[] = array( 'value' => 'WI', 'title' => 'Wisconsin' );
		$us_states[] = array( 'value' => 'WY', 'title' => 'Wyoming' );

		foreach ( $us_states as $state ) {

			$state_short = $state['value'];
			$state_full  = $state['title'];

			$sql           = "SELECT * FROM $table_name WHERE state LIKE %s AND state_full LIKE %s";
			$sql_prepare   = $wpdb->prepare( $sql, $state_short, $state_full );
			$check_result  = $wpdb->get_results( $sql_prepare );

			if ( 1 > count($check_result) ) {

				$update_zip_table = $wpdb->prepare( "UPDATE $table_name SET state_full = %s WHERE state = %s", $state_full, $state_short );
				$wpdb->query( $update_zip_table );

			}

		}

	}

}

add_action( 'init', 'nano_setup_us_zip_table' );

function nano_us_states($state) {

	$cat_options   = array();
	$cat_options[] = array( 'value' => 'AL', 'title' => 'Alabama' );
	$cat_options[] = array( 'value' => 'AK', 'title' => 'Alaska' );
	$cat_options[] = array( 'value' => 'AZ', 'title' => 'Arizona' );
	$cat_options[] = array( 'value' => 'AR', 'title' => 'Arkansas' );
	$cat_options[] = array( 'value' => 'CA', 'title' => 'California' );
	$cat_options[] = array( 'value' => 'CO', 'title' => 'Colorado' );
	$cat_options[] = array( 'value' => 'CT', 'title' => 'Connecticut' );
	$cat_options[] = array( 'value' => 'DE', 'title' => 'Delaware' );
	$cat_options[] = array( 'value' => 'DC', 'title' => 'District Of Columbia' );
	$cat_options[] = array( 'value' => 'FL', 'title' => 'Florida' );
	$cat_options[] = array( 'value' => 'GA', 'title' => 'Georgia' );
	$cat_options[] = array( 'value' => 'HI', 'title' => 'Hawaii' );
	$cat_options[] = array( 'value' => 'ID', 'title' => 'Idaho' );
	$cat_options[] = array( 'value' => 'IL', 'title' => 'Illinois' );
	$cat_options[] = array( 'value' => 'IN', 'title' => 'Indiana' );
	$cat_options[] = array( 'value' => 'IA', 'title' => 'Iowa' );
	$cat_options[] = array( 'value' => 'KS', 'title' => 'Kansas' );
	$cat_options[] = array( 'value' => 'KY', 'title' => 'Kentucky' );
	$cat_options[] = array( 'value' => 'LA', 'title' => 'Louisiana' );
	$cat_options[] = array( 'value' => 'ME', 'title' => 'Maine' );
	$cat_options[] = array( 'value' => 'MD', 'title' => 'Maryland' );
	$cat_options[] = array( 'value' => 'MA', 'title' => 'Massachusetts' );
	$cat_options[] = array( 'value' => 'MI', 'title' => 'Michigan' );
	$cat_options[] = array( 'value' => 'MN', 'title' => 'Minnesota' );
	$cat_options[] = array( 'value' => 'MS', 'title' => 'Mississippi' );
	$cat_options[] = array( 'value' => 'MO', 'title' => 'Missouri' );
	$cat_options[] = array( 'value' => 'MT', 'title' => 'Montana' );
	$cat_options[] = array( 'value' => 'NE', 'title' => 'Nebraska' );
	$cat_options[] = array( 'value' => 'NV', 'title' => 'Nevada' );
	$cat_options[] = array( 'value' => 'NH', 'title' => 'New Hampshire' );
	$cat_options[] = array( 'value' => 'NJ', 'title' => 'New Jersey' );
	$cat_options[] = array( 'value' => 'NM', 'title' => 'New Mexico' );
	$cat_options[] = array( 'value' => 'NY', 'title' => 'New York' );
	$cat_options[] = array( 'value' => 'NC', 'title' => 'North Carolina' );
	$cat_options[] = array( 'value' => 'ND', 'title' => 'North Dakota' );
	$cat_options[] = array( 'value' => 'OH', 'title' => 'Ohio' );
	$cat_options[] = array( 'value' => 'OK', 'title' => 'Oklahoma' );
	$cat_options[] = array( 'value' => 'OR', 'title' => 'Oregon' );
	$cat_options[] = array( 'value' => 'PA', 'title' => 'Pennsylvania' );
	$cat_options[] = array( 'value' => 'RI', 'title' => 'Rhode Island' );
	$cat_options[] = array( 'value' => 'SC', 'title' => 'South Carolina' );
	$cat_options[] = array( 'value' => 'SD', 'title' => 'South Dakota' );
	$cat_options[] = array( 'value' => 'TN', 'title' => 'Tennessee' );
	$cat_options[] = array( 'value' => 'TX', 'title' => 'Texas' );
	$cat_options[] = array( 'value' => 'UT', 'title' => 'Utah' );
	$cat_options[] = array( 'value' => 'VT', 'title' => 'Vermont' );
	$cat_options[] = array( 'value' => 'VA', 'title' => 'Virginia' );
	$cat_options[] = array( 'value' => 'WA', 'title' => 'Washington' );
	$cat_options[] = array( 'value' => 'WV', 'title' => 'West Virginia' );
	$cat_options[] = array( 'value' => 'WI', 'title' => 'Wisconsin' );
	$cat_options[] = array( 'value' => 'WY', 'title' => 'Wyoming' );

	$result = multi_dimen_array_search_all_keys( $cat_options, 'value', $state);

	$final_result = '';
	if ( is_array( $result ) ) {
		$final_result = $result[0]['title'];
	}

	return $final_result;

}


/**
 * For listing the budget items on the single product page
 *
 * @param $post_id
 */
function nano_budget( $post_id ) {

	$nano_budget_titles = get_post_meta( $post_id, 'nano_budget_title', true );
	$nano_budget_values = get_post_meta( $post_id, 'nano_budget_value', true );

	$content = array();

	if ( is_array( $nano_budget_titles ) ) {

		foreach ( $nano_budget_titles as $k => $v ) {

			$content['label'][] = '' != $v ? $v : 'Title';
			$content['val'][]   = isset( $nano_budget_values[ $k ] ) && '' != $nano_budget_values[ $k ] ? $nano_budget_values[ $k ] : '50';

		}
	}

	return $content;

}


/**
 * A more granular mobile detection function
 */
function is_mobile() {
	$tablet_browser = 0;
	$mobile_browser = 0;

	if ( isset($_SERVER['HTTP_USER_AGENT']) ) {

		if ( preg_match( '/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower( $_SERVER['HTTP_USER_AGENT'] ) ) ) {
			$tablet_browser ++;
		}

		if ( preg_match( '/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower( $_SERVER['HTTP_USER_AGENT'] ) ) ) {
			$mobile_browser ++;
		}

		if ( ( isset( $_SERVER['HTTP_ACCEPT'] ) && ( strpos( strtolower( $_SERVER['HTTP_ACCEPT'] ), 'application/vnd.wap.xhtml+xml' ) > 0 ) ) or ( ( isset( $_SERVER['HTTP_X_WAP_PROFILE'] ) or isset( $_SERVER['HTTP_PROFILE'] ) ) ) ) {
			$mobile_browser ++;
		}

		$mobile_ua     = strtolower( substr( $_SERVER['HTTP_USER_AGENT'], 0, 4 ) );
		$mobile_agents = array(
			'w3c ',
			'acs-',
			'alav',
			'alca',
			'amoi',
			'audi',
			'avan',
			'benq',
			'bird',
			'blac',
			'blaz',
			'brew',
			'cell',
			'cldc',
			'cmd-',
			'dang',
			'doco',
			'eric',
			'hipt',
			'inno',
			'ipaq',
			'java',
			'jigs',
			'kddi',
			'keji',
			'leno',
			'lg-c',
			'lg-d',
			'lg-g',
			'lge-',
			'maui',
			'maxo',
			'midp',
			'mits',
			'mmef',
			'mobi',
			'mot-',
			'moto',
			'mwbp',
			'nec-',
			'newt',
			'noki',
			'palm',
			'pana',
			'pant',
			'phil',
			'play',
			'port',
			'prox',
			'qwap',
			'sage',
			'sams',
			'sany',
			'sch-',
			'sec-',
			'send',
			'seri',
			'sgh-',
			'shar',
			'sie-',
			'siem',
			'smal',
			'smar',
			'sony',
			'sph-',
			'symb',
			't-mo',
			'teli',
			'tim-',
			'tosh',
			'tsm-',
			'upg1',
			'upsi',
			'vk-v',
			'voda',
			'wap-',
			'wapa',
			'wapi',
			'wapp',
			'wapr',
			'webc',
			'winw',
			'winw',
			'xda ',
			'xda-',
		);

		if ( in_array( $mobile_ua, $mobile_agents ) ) {
			$mobile_browser ++;
		}

		if ( strpos( strtolower( $_SERVER['HTTP_USER_AGENT'] ), 'opera mini' ) > 0 ) {
			$mobile_browser ++;
			//Check for tablets on opera mini alternative headers
			$stock_ua = strtolower( isset( $_SERVER['HTTP_X_OPERAMINI_PHONE_UA'] ) ? $_SERVER['HTTP_X_OPERAMINI_PHONE_UA'] : ( isset( $_SERVER['HTTP_DEVICE_STOCK_UA'] ) ? $_SERVER['HTTP_DEVICE_STOCK_UA'] : '' ) );
			if ( preg_match( '/(tablet|ipad|playbook)|(android(?!.*mobile))/i', $stock_ua ) ) {
				$tablet_browser ++;
			}
		}

		if ( $tablet_browser > 0 ) {
			return 'tablet';
		} elseif ( $mobile_browser > 0 ) {
			return 'mobile';
		} else {
			return 'desktop';
		}

	} else {

		return 'desktop';

	}
}

/**
 * Adding Nanosteam Colors to WP Text Editor
 *
 * @param $init
 *
 * @return mixed
 *
 */
function my_mce4_options( $init ) {
	$default_colours        = '
	"000000", "Black",
	"993300", "Burnt orange",
	"333300", "Dark olive",
	"003300", "Dark green",
	"003366", "Dark azure",
	"000080", "Navy Blue",
	"333399", "Indigo",
	"333333", "Very dark gray",
	"800000", "Maroon",
	"FF6600", "Orange",
	"808000", "Olive",
	"008000", "Green",
	"008080", "Teal",
	"0000FF", "Blue",
	"666699", "Grayish blue",
	"808080", "Gray",
	"FF0000", "Red",
	"FF9900", "Amber",
	"99CC00", "Yellow green",
	"339966", "Sea green",
	"33CCCC", "Turquoise",
	"3366FF", "Royal blue",
	"800080", "Purple",
	"999999", "Medium gray",
	"FF00FF", "Magenta",
	"FFCC00", "Gold",
	"FFFF00", "Yellow",
	"00FF00", "Lime",
	"00FFFF", "Aqua",
	"00CCFF", "Sky blue",
	"993366", "Brown",
	"C0C0C0", "Silver",
	"FF99CC", "Pink",
	"FFCC99", "Peach",
	"FFFF99", "Light yellow",
	"CCFFCC", "Pale green",
	"CCFFFF", "Pale cyan",
	"99CCFF", "Light sky blue",
	"CC99FF", "Plum",
	"FFFFFF", "White"
	';
	$custom_colours         = '
	"fcb619", "Primary",
	"005dcb", "Science / Info",
	"00bdd9", "Egg Blue",
	"7f2ca2", "Seance Purple",
	"d72229", "Crimson Red",
	"fc9c19", "Poppy Orange",
	"ffe900", "Turbo Yellow",
	"3dc644", "Apple Green",
	"017574", "Pine Green",
	"001d79", "Resolution Blue",
	"36404a", "Text 1",
	"171c20", "Text 2"
	';
	$init['textcolor_map']  = '[' . $custom_colours . ']';
	$init['textcolor_rows'] = 6; // expand colour grid to 6 rows

	return $init;
}

add_filter( 'tiny_mce_before_init', 'my_mce4_options' );


/**
 * Replace the widget sidebar wrapper
 *
 * @param $id
 */
function krown_sidebar_output( $id ) {

	if ( is_active_sidebar( $id ) ) {

		echo '<aside id="sidebar" class="col-sm-3 col-md-4"> ';
		dynamic_sidebar( $id );
		echo '</aside>';

	}

}


/*
 * Disable the IDC user page from everyone except Roxanne or if in dev mode then also James
 */

add_action( 'admin_menu', 'nano_adjust_the_admin_menu', 999 );
function nano_adjust_the_admin_menu() {
	$admin = wp_get_current_user();
	$dev   = false;

	$settings = get_option( 'memberdeck_gateways' );
	if ( ! empty( $settings ) ) {
		if ( is_array( $settings ) ) {
			$test = $settings['test'];
			if ( 1 == $test ) {
				$key = $settings['tsk'];
				$dev = true;
			} else {
				$key = $settings['sk'];
				$dev = false;
			}
		}
	}
	if ( false == $dev ) {
		if ( 2 != $admin->ID || 3 != $admin->ID ) {
			// 2 is Roxanne's user ID
			//
			remove_submenu_page( 'idc', 'idc-users' );
		}
	}
}
