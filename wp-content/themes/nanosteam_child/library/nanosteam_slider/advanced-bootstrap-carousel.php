<?php
/*
 * Slider for nanosteam which supports video & images utilizing the bootstrap carousel system
 * Based on Advanced Bootstrap Carousel : https://wordpress.org/plugins/advanced-bootstrap-carousel/
 *
 * THIS IS NO LONGER USED BUT I HAVE LEFT THIS FOR A FUTURE POTENTIAL USE. JAMES ROY
*/

define( 'NANO_VERSION', '2.0.0' );
define( 'NANO_PLUGIN', __FILE__ );
define( 'NANO_PLUGIN_BASENAME', plugin_basename( NANO_PLUGIN ) );

// Advanced Bootstrap Carousel custom post type
add_action( 'init', 'nano_slider_post_type');
function nano_slider_post_type() {

	$labels = array(
		'name'               => 'Nanosteam Bootstrap Carousel',
		'singular_name'      => 'Nanosteam Bootstrap Carousel',
		'menu_name'          => 'Nanosteam Slider',
		'parent_item_colon'  => 'Parent:',
		'all_items'          => 'All Carousels',
		'view_item'          => 'View Carousel',
		'add_new_item'       => 'Add New Carousel',
		'add_new'            => 'Add New Carousel',
		'edit_item'          => 'Edit Carousel',
		'update_item'        => 'Update Carousel',
		'search_items'       => 'Search Carousels',
		'not_found'          => 'Carousel Not found',
		'not_found_in_trash' => 'Carousel Not found in Trash',
	);
	$args   = array(
		'labels'              => $labels,
		'public'              => true,
		'exclude_from_search' => true,
		'publicly_queryable'  => false,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'query_var'           => true,
		'rewrite'             => true,
		'capability_type'     => 'page',
		'has_archive'         => true,
		'hierarchical'        => false,
		'menu_position'       => 20,
		'menu_icon'           => 'dashicons-slides',
		'supports'            => array( 'title' ),
	);
	register_post_type( 'nano_slider', $args );
}

// Create a taxonomy for the carousel post type
function nano_slider_taxonomies() {
	$args = array( 'hierarchical' => true );
	register_taxonomy( 'nano_slider_category', 'nano_slider', $args );
}

add_action( 'init', 'nano_slider_taxonomies', 0 );

function nano_slider_add_image_support() {
	$supportedTypes = get_theme_support( 'post-thumbnails' );
	if ( false === $supportedTypes ) {
		add_theme_support( 'post-thumbnails', array( 'nano_slider' ) );
		add_image_size( 'featured_preview', 100, 55, true );
	} elseif ( is_array( $supportedTypes ) ) {
		$supportedTypes[0][] = 'nano_slider';
		add_theme_support( 'post-thumbnails', $supportedTypes[0] );
		add_image_size( 'featured_preview', 100, 55, true );
	}
}

add_action( 'after_setup_theme', 'nano_slider_add_image_support' );

require_once( 'nano_slider_admin_view.php' );
require_once( 'nano_slider_front_view.php' );
require_once( 'nano_slider_admin_settings.php' );