<?php
// Shortcode
function nano_slider_shortcode( $atts, $content = null ) {
	// Set default shortcode attributes
	$options = get_option( 'nano_slider_settings' );
	if ( ! $options ) {
		nano_slider_set_options();
		$options = get_option( 'nano_slider_settings' );
	}
	$options['id'] = '';
	$options['slug'] = '';

	// Parse incomming $atts into an array and merge it with $defaults
	$atts = shortcode_atts( $options, $atts );

	return nano_slider_frontend( $atts );
}

add_shortcode( 'ns-carousel', 'nano_slider_shortcode' );

// Display carousel
function nano_slider_frontend( $atts ) {

	if ( isset( $atts['slug'] ) && '' != $atts['slug'] ) {

		$slug = $atts['slug'];

		// Build the attributes
		$id   = rand( 0, 999 ); // use a random ID so that the CSS IDs work with multiple on one page
		$args = array(
			'name'           => $slug,
			'post_type'      => 'nano_slider',
			'posts_per_page' => '-1',
			'orderby'        => $atts['orderby'],
		);
		if ( ! isset( $atts['image_size'] ) ) {
			$atts['image_size'] = 'full';
		}
		if ( ! isset( $atts['use_javascript_animation'] ) ) {
			$atts['use_javascript_animation'] = '1';
		}
		if ( '' != $atts['id'] ) {
			$args['p'] = $atts['id'];
		}

		// Collect the carousel content. Needs printing in two loops later (bullets and content)
		$loop   = new WP_Query( $args );
		$images = array();
		$output = '';
		while ( $loop->have_posts() ) {
			$loop->the_post();

			$post_id = get_the_ID();

			$slides = get_post_meta( $post_id, 'repeatable_fields', true );

			if ( '' != $slides ) {

				foreach ( $slides as $slide ){

					$image_id        = isset( $slide['img_id'] ) ? $slide['img_id'] : '';
					$title           = isset( $slide['name'] ) ? $slide['name'] : '';
					$content         = isset( $slide['description'] ) ? $slide['description'] : '';
					$main_url        = isset( $slide['url'] ) ? $slide['url'] : '';
					$button_1_name   = isset( $slide['button_1_name'] ) ? $slide['button_1_name'] : '';
					$button_1_url    = isset( $slide['button_1_url'] ) ? $slide['button_1_url'] : '';
					$button_2_name   = isset( $slide['button_2_name'] ) ? $slide['button_2_name'] : '';
					$button_2_url    = isset( $slide['button_2_url'] ) ? $slide['button_2_url'] : '';
					$image_src       = wp_get_attachment_image_src( $image_id, 'nano-16-9-sm' );
					$image_src_thumb = wp_get_attachment_image_src( get_post_thumbnail_id(), 'featured_preview' );
					$video_dir       = $image_src !== false ? '' : get_attached_file( $image_id );
					$image_src       = $image_src !== false ? $image_src[0] : wp_get_attachment_url( $image_id );


					$image_src_thumb = $image_src_thumb[0];
					$images[]        = array(
						'post_id'       => $post_id,
						'img_id'		=> $image_id,
						'video_dir'		=> $video_dir,
						'title'         => $title,
						'content'       => $content,
						'img_src'       => $image_src,
						'main_url'      => $main_url,
						'button_1_name' => $button_1_name,
						'button_1_url'  => $button_1_url,
						'button_2_name' => $button_2_name,
						'button_2_url'  => $button_2_url,
					);

				}
			}
		}
		// Check we actually have something to show
		if ( count( $images ) > 0 ) {
			ob_start();
			?>

			<div style="height: 400px;" id="nano_slider_<?php echo $id; ?>" class="carousel slide row <?php if ( $atts['effect'] === 'fade' ) {
				echo "carousel-fade";
			} elseif ( 'vslide' === $atts['effect'] ) {
				echo "vertical-slider";
			} ?> <?php if ( $atts['showindicator'] === 'numbered' ) {
				echo "carousel-indicator-numbered";
			} ?>" <?php if ( $atts['use_javascript_animation'] == '0' ) {
				echo ' data-ride="carousel"';
			} ?> data-interval="<?php echo $atts['interval']; ?>">
				<?php
				require_once( 'nano_slider_front_view_4.php' );
				?>
			</div>


			<?php // Javascript animation fallback
			if ( $atts['use_javascript_animation'] == '1' ) { ?>
				<script type="text/javascript">
                    jQuery( document ).ready( function() {
                        jQuery( '#nano_slider_<?php echo $id; ?>' ).carousel( {
                            interval: <?php echo $atts['interval']; ?>
                        } );
                    } );
				</script>
			<?php }

			// Collect the output
			$output = ob_get_contents();
			ob_end_clean();
		}

		// Restore original Post Data
		wp_reset_postdata();

		return $output;
	}
}

