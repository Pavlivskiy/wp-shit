<?php
function nano_slider_get_featured_image( $post_ID ) {
	$post_thumbnail_id = get_post_thumbnail_id( $post_ID );
	if ( $post_thumbnail_id ) {
		$post_thumbnail_img = wp_get_attachment_image_src( $post_thumbnail_id, 'featured_preview' );

		return $post_thumbnail_img[0];
	}
}

function nano_slider_columns_head( $defaults ) {
	$defaults['featured_image'] = 'Carousel Image';
	$defaults['category']       = 'Carousel Category';
	$defaults['slug']           = 'Carousel Slug';

	return $defaults;
}

function nano_slider_columns_content( $column_name, $post_ID ) {

	if ( 'featured_image' == $column_name ) {
		//$post_featured_image = nano_slider_get_featured_image( $post_ID );
		$slides = get_post_meta( $post_ID, 'repeatable_fields', true );

		$thumb = 'No Thumbnail Available';

		if ( '' != $slides && isset( $slides[0]['img_id'] ) ) {
			$first_img_id = $slides[0]['img_id'];
			$image_src    = wp_get_attachment_image_src( $first_img_id, 'nano-crop-16-9-sm' );

			if ( false !== $image_src ) {
				// Images

				$image_src = $image_src[0];
				$thumb     = '<img src="' . $image_src . '" alt="" style="max-width:100%;" />';
			} else {
				// Videos

				$image_src = $slides[0]['image'];
				$file      = str_replace( '1920', '720', $image_src );
				$haystack  = array( 'webm', 'mp4', 'ogv' );

				$string = 'mp4';
				foreach ( $haystack as $string ) {
					if ( strpos( $image_src, $string ) !== false ) {
						$type = $string;
					}
				}

				$background  = '<video class="object_fitt" id="video" muted autoplay loop preload>';
				$background .= '<source src="' . $file . '" type="video/' . $type . '"></source>';
				$background .= '</video>';

				$thumb = $background;
			}
		}

		echo '<a href="' . get_edit_post_link( $post_ID ) . '">' . $thumb . '</a>';

	}
	if ( 'category' == $column_name ) {
		$post_categories = get_the_terms( $post_ID, 'nano_slider_category' );
		if ( $post_categories ) {
			$output = '';
			foreach ( $post_categories as $cat ) {
				$output .= $cat->name . ', ';
			}
			echo trim( $output, ', ' );
		} else {
			echo 'No categories';
		}
	}
	if ( 'slug' == $column_name ) {
		echo get_post_field( 'post_name', get_post() );
	}
}

add_filter( 'manage_nano_slider_posts_columns', 'nano_slider_columns_head' );
add_action( 'manage_nano_slider_posts_custom_column', 'nano_slider_columns_content', 10, 2 );

/**
 * Repeatable Custom Fields in a Metabox
 * Author: Helen Hou-Sandi
 *
 * From a bespoke system, so currently not modular - will fix soon
 * Note that this particular metadata is saved as one multidimensional array (serialized)
 */

add_action( 'admin_init', 'nano_slider_add_meta_boxes', 1 );
function nano_slider_add_meta_boxes() {
	add_meta_box( 'repeatable-fields', 'Nanosteam Slider', 'nano_slider_repeatable_meta_box_display', 'nano_slider', 'normal', 'default' );
}

function nano_slider_repeatable_meta_box_display() {
	global $post;
	$repeatable_fields = get_post_meta( $post->ID, 'repeatable_fields', true );
	wp_nonce_field( 'nano_slider_repeatable_meta_box_nonce', 'nano_slider_repeatable_meta_box_nonce' );
	?>

	<table id="repeatable-fieldset-one" class="wp-list-table widefat fixed striped nano_slider">
		<thead>
		<tr>
			<th scope="col">Title</th>
			<th scope="col">Descripition</th>
			<th scope="col">Read More Link</th>
			<th scope="col">Image / Video</th>
			<th scope="col">Button 1 Text / Button 1 URL</th>
			<th scope="col">Button 2 Text / Button 2 URL</th>
			<th scope="col"></th>
		</tr>
		</thead>
		<tbody>
		<?php

		if ( $repeatable_fields ) :

			foreach ( $repeatable_fields as $key => $field ) {
				?>
				<tr>
					<td><input type="text" class="widefat" name="name[]" value="<?php if ( $field['name'] != '' ) {
							echo esc_attr( $field['name'] );
						} ?>"/></td>

					<td>
						<textarea class="widefat" name="description[]"><?php if ( $field['description'] != '' ) {
								echo esc_attr( $field['description'] );
							} ?></textarea>
					</td>

					<td><input type="text" class="widefat" name="url[]" value="<?php if ( $field['url'] != '' ) {
							echo esc_attr( $field['url'] );
						} else {
							echo 'http://';
						} ?>"/></td>

					<td><?php
						/**
						 * The actual field that will hold the URL for our file
						 */
						?>
						<input type="url" class="widefat" name="image[]"
								value="<?php if ( isset( $field['image'] ) && $field['image'] != '' ) {
									echo esc_attr( $field['image'] );
								} ;?>" readonly>
						<input type="hidden" class="widefat" name="img_id[]" value="<?php if ( isset( $field['img_id'] ) && $field['img_id'] != '' ) {
							echo esc_attr( $field['img_id'] );
						} ;?>">

						<?php
						/**
						 * The button that opens our media uploader
						 * The `data-media-uploader-target` value should match the ID/unique selector of your field.
						 * We'll use this value to dynamically inject the file URL of our uploaded media asset into your field once successful (in the myplugin-media.js file)
						 */
						?>
						<button type="button" class="button events_video_upload_btn"
								data-media-uploader-target="#nano_slider_media_<?php echo $key; ?>"><?php _e( 'Upload Media', 'nanosteam_8n' ) ?></button>
					</td>
					<td><input type="text" class="widefat" name="button_1_name[]" placeholder="button text" value="<?php if ( isset( $field['button_1_name'] ) && $field['button_1_name'] != '' ) {
							echo esc_attr( $field['button_1_name'] );
						} ?>"/><input type="text" class="widefat" name="button_1_url[]" placeholder="http://" value="<?php if ( isset( $field['button_1_url'] ) && $field['button_1_url'] != '' ) {
							echo esc_attr( $field['button_1_url'] );
						} ?>"/></td>
					<td><input type="text" class="widefat" name="button_2_name[]" placeholder="button text"  value="<?php if ( isset( $field['button_2_name'] ) && $field['button_2_name'] != '' ) {
							echo esc_attr( $field['button_2_name'] );
						} ?>"/><input type="text" class="widefat" name="button_2_url[]" placeholder="http://" value="<?php if ( isset( $field['button_2_url'] ) &&$field['button_2_url'] != '' ) {
							echo esc_attr( $field['button_2_url'] );
						} ?>"/></td>
					<td><a class="button remove-row" href="#">Remove</a></td>
				</tr>
				<?php
			}
		else :
			// show a blank one
			?>
			<tr>
				<td><input type="text" class="widefat" name="name[]"/></td>
				<td><textarea class="widefat" name="description[]"></textarea></td>
				<td><input type="text" class="widefat" name="url[]" value="http://"/></td>
				<td>
					<input type="url" class="widefat" name="image[]" value="" readonly><br>
					<input type="hidden" name="img_id[]" value="">
					<button type="button" class="button events_video_upload_btn"
							data-media-uploader-target="#nano_slider_media"><?php _e( 'Upload Media', 'nanosteam_8n' ) ?></button>
				</td>
				<td><input type="text" class="widefat" name="button_1_name[]" placeholder="button text" value=""><input type="text" class="widefat" name="button_1_url[]" placeholder="http://" value=""></td>
				<td><input type="text" class="widefat" name="button_2_name[]" placeholder="button text" value=""><input type="text" class="widefat" name="button_2_url[]" placeholder="http://" value=""></td>
				<td><a class="button remove-row" href="#">Remove</a></td>
			</tr>
		<?php endif; ?>

		<!-- empty hidden one for jQuery -->
		<tr class="empty-row screen-reader-text">
			<td><input type="text" class="widefat" name="name[]"/></td>
			<td><textarea class="widefat" name="description[]"></textarea></td>
			<td><input type="text" class="widefat" name="url[]" value="http://"></td>
			<td>
				<input type="url" class="widefat" name="image[]" value="" readonly><br>
				<input type="hidden" class="widefat" name="img_id[]" value="">
				<button type="button" class="button events_video_upload_btn"
						data-media-uploader-target="#nano_slider_media"><?php _e( 'Upload Media', 'nanosteam_8n' ) ?></button>
			</td>
			<td><input type="text" class="widefat" name="button_1_name[]" placeholder="button text" value=""><input type="text" class="widefat" name="button_1_url[]" placeholder="http://" value=""></td>
			<td><input type="text" class="widefat" name="button_2_name[]" placeholder="button text" value=""><input type="text" class="widefat" name="button_2_url[]" placeholder="http://" value=""></td>
			<td><a class="button remove-row" href="#">Remove</a></td>

		</tr>
		</tbody>
	</table>

	<p><a id="add-row" class="button" href="#">Add another</a></p>
	<?php
}

function nano_slider_repeatable_meta_box_save( $post_id ) {
	if ( ! isset( $_POST['nano_slider_repeatable_meta_box_nonce'] ) ||
	     ! wp_verify_nonce( $_POST['nano_slider_repeatable_meta_box_nonce'], 'nano_slider_repeatable_meta_box_nonce' ) ) {
		return;
	}

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	if ( ! current_user_can( 'edit_post', $post_id ) ) {
		return;
	}

	$old     = get_post_meta( $post_id, 'repeatable_fields', true );
	$new     = array();

	$names          = isset( $_POST['name'] ) ? $_POST['name'] : '';
	$descriptions   = isset( $_POST['description'] ) ? $_POST['description'] : '';
	$images         = isset( $_POST['image'] ) ? $_POST['image'] : '';
	$image_ids      = isset( $_POST['img_id'] ) ? $_POST['img_id'] : '';
	$button_1_names = isset( $_POST['button_1_name'] ) ? $_POST['button_1_name'] : '';
	$button_1_urls  = isset( $_POST['button_1_url'] ) ? $_POST['button_1_url'] : '';
	$button_2_names = isset( $_POST['button_2_name'] ) ? $_POST['button_2_name'] : '';
	$button_2_urls  = isset( $_POST['button_2_url'] ) ? $_POST['button_2_url'] : '';
	$urls         = $_POST['url'];

	$count = count( $names );

	for ( $i = 0; $i < $count; $i ++ ) {
		if ( '' != $names[ $i ] ) {
			$new[ $i ]['name']          = stripslashes( strip_tags( $names[ $i ] ) );
			$new[ $i ]['description']   = stripslashes( strip_tags( $descriptions[ $i ] ) );
			$new[ $i ]['image']         = stripslashes( strip_tags( $images[ $i ] ) );
			$new[ $i ]['img_id']        = stripslashes( strip_tags( $image_ids[ $i ] ) );
			$new[ $i ]['button_1_name'] = stripslashes( strip_tags( $button_1_names[ $i ] ) );
			$new[ $i ]['button_1_url']  = stripslashes( strip_tags( $button_1_urls[ $i ] ) );
			$new[ $i ]['button_2_name'] = stripslashes( strip_tags( $button_2_names[ $i ] ) );
			$new[ $i ]['button_2_url']  = stripslashes( strip_tags( $button_2_urls[ $i ] ) );

			if ( 'http://' == $urls[ $i ] ) {
				$new[ $i ]['url'] = '';
			} else {
				$new[ $i ]['url'] = stripslashes( $urls[ $i ] );
			} // and however you want to sanitize
		}
	}
	if ( ! empty( $new ) && $new != $old ) {
		update_post_meta( $post_id, 'repeatable_fields', $new );
	} elseif ( empty( $new ) && $old ) {
		delete_post_meta( $post_id, 'repeatable_fields', $old );
	}
}

add_action( 'save_post', 'nano_slider_repeatable_meta_box_save' );

/**
 * Load the media uploader and our custom myplugin-media.js file
 * Change `nano_slider_custom_post_type` to whatever the post type for your metabox is
 * You may also need to change the `plugins_url()` path to match your plugin folder structure (currently assumes flat
 * with no subfolders)
 */
function nano_slider_load_admin_scripts( $hook ) {
	global $typenow;

	if ( 'nano_slider' == $typenow ) {
		wp_enqueue_media();

		// Registers and enqueues the required javascript.
		wp_register_script( 'nano-slider-image', get_stylesheet_directory_uri() . '/library/nanosteam_slider/asset/js/nano_slider_media_uploader.js', 'jquery', '1.1.0', true );
		wp_localize_script( 'nano-slider-image', 'meta_image',
			array(
				'title'  => __( 'Choose or Upload Media', 'events' ),
				'button' => __( 'Use this media', 'events' ),
			)
		);
		wp_enqueue_script( 'nano-slider-image' );
	}
}

add_action( 'admin_enqueue_scripts', 'nano_slider_load_admin_scripts', 10, 1 );