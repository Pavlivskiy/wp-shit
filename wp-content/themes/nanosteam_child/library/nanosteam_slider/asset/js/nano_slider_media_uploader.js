/**
 * Load media uploader on pages with our custom metabox
 */
jQuery( document ).ready( function( $ ) {

    'use strict';

    // Instantiates the variable that holds the media library frame.
    var metaImageFrame;

    // Runs when the media button is clicked.
    $( '#repeatable-fieldset-one' ).on( 'click', '.events_video_upload_btn', function( e ) {

        // Get the btn
        var target = $( this ).closest( 'td' ).find( 'input[name="image[]"]' );
        var id = $( this ).closest( 'td' ).find( 'input[name="img_id[]"]' );

        // Get the field target
        //var field = target.data( 'media-uploader-target' );

        // Prevents the default action from occuring.
        e.preventDefault();

        // Sets up the media library frame
        metaImageFrame = wp.media.frames.metaImageFrame = wp.media({
            title: meta_image.title,
            button: { text: 'Use this file' }
        });

        // Runs when an image is selected.
        metaImageFrame.on( 'select', function() {

            // Grabs the attachment selection and creates a JSON representation of the model.
            var media_attachment = metaImageFrame.state().get( 'selection' ).first().toJSON();

            // Sends the attachment URL to our custom image input field.
            $( target ).val( media_attachment.url );
            $( id ).val( media_attachment.id );


        });

        // Opens the media library frame.
        metaImageFrame.open();

    });

    $( '#add-row' ).on( 'click', function() {
        var row = $( '.empty-row.screen-reader-text' ).clone( true );
        row.removeClass( 'empty-row screen-reader-text' );
        row.insertBefore( '#repeatable-fieldset-one tbody>tr:last' );
        return false;
    });

    $( '.remove-row' ).on( 'click', function() {
        $( this ).parents( 'tr' ).remove();
        return false;
    });

});
