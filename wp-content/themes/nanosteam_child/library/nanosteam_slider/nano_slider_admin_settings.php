<?php
// Set up settings defaults
register_activation_hook( __FILE__, 'nano_slider_set_options' );
function nano_slider_set_options() {
	$defaults = array(
		'interval'                 => '5000',
		'showcontrols'             => 'true',
		'showindicator'            => 'true',
		'customprev'               => '',
		'customnext'               => '',
		'orderby'                  => 'menu_order',
		'order'                    => 'ASC',
		'category'                 => '',
		'before_title'             => '<h4>',
		'after_title'              => '</h4>',
		'before_caption'           => '<p>',
		'after_caption'            => '</p>',
		'image_size'               => 'full',
		'id'                       => '',
		'twbs'                     => '3',
		'use_javascript_animation' => '1',
		'effect'                   => 'slide',
	);
	add_option( 'nano_slider_settings', $defaults );
}

// Clean up on uninstall
register_activation_hook( __FILE__, 'nano_slider_deactivate' );
function nano_slider_deactivate() {
	delete_option( 'nano_slider_settings' );
}


// Render the settings page
class nano_slider_settings_page {
	// Holds the values to be used in the fields callbacks
	private $options;

	// Start up
	public function __construct() {
		add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
		add_action( 'admin_init', array( $this, 'page_init' ) );
	}

	// Add settings page
	public function add_plugin_page() {
		add_submenu_page( 'edit.php?post_type=nano_slider', 'Settings', 'Settings', 'manage_options', 'nano-slider-settings', array(
			$this,
			'create_admin_page',
		) );
	}

	// Options page callback
	public function create_admin_page() {
		// Set class property
		$this->options = get_option( 'nano_slider_settings' );
		if ( ! $this->options ) {
			nano_slider_set_options();
			$this->options = get_option( 'nano_slider_settings' );
		}
		?>
		<div class="wrap">
			<h2>Nanosteasm Carousel Settings</h2>
			<div class="notice notice-success is-dismissible">

			</div>
			<h3>Shortcodes:</h3>
			<p>If you are using Videos in your slider please ensure the videos are all uploaded at the same time and that they
				use the same file names. For example, home_page_1920.mp4. This system will look for the webm and ogv
				versions of this video with the same file name.</p>
			<p>Additionally, we are also pushing mobile friendly versions of these videos so please also upload the 1280
				and 720 versions using the same files names as the primary video but as home_page_1280.mp4 and home_page_720.mp4 respectively.</p>
			<strong>Default</strong>
			<span class="shortcode"><input type="text" value="[ns-carousel slug=&quot;slug&quot;]" class="large-text code" readonly="readonly"
						onfocus="this.select();"></span>
			<strong>Slide with custom Interval</strong>
			<span class="shortcode"><input type="text" value="[ns-carousel slug=&quot;slug&quot; interval=&quot;900&quot;]"
						class="large-text code" readonly="readonly" onfocus="this.select();"></span>
			<strong>Slider with Control</strong>
			<span class="shortcode"><input type="text" value="[ns-carousel slug=&quot;slug&quot; showcontrols=&quot;true&quot;]"
						class="large-text code" readonly="readonly" onfocus="this.select();"></span>
			<strong>Slider With Numbered Indicator</strong>
			<span class="shortcode"><input type="text" value="[ns-carousel slug=&quot;slug&quot; showindicator=&quot;numbered&quot;]"
						class="large-text code" readonly="readonly" onfocus="this.select();"></span>
			<form method="post" action="options.php">
				<?php
				settings_fields( 'nano_slider_settings' );
				do_settings_sections( 'nano-slider-settings' );
				submit_button();
				?>
			</form>
		</div>
		<?php
	}

	// Register and add settings
	public function page_init() {
		register_setting(
			'nano_slider_settings', // Option group
			'nano_slider_settings', // Option name
			array( $this, 'sanitize' ) // Sanitize
		);

		// Sections
		add_settings_section(
			'nano_slider_settings_behaviour', // ID
			'Carousel Basic Settings', // Title
			array( $this, 'nano_slider_settings_behaviour_header' ), // Callback
			'nano-slider-settings' // Page
		);
		add_settings_section(
			'nano_slider_settings_setup', // ID
			'Carousel Advance Settings', // Title
			array( $this, 'nano_slider_settings_setup' ), // Callback
			'nano-slider-settings' // Page
		);

		// Behaviour Fields
		add_settings_field(
			'interval', // ID
			'Slide Interval (milliseconds)', // Title
			array( $this, 'interval_callback' ), // Callback
			'nano-slider-settings', // Page
			'nano_slider_settings_behaviour' // Section
		);
		add_settings_field(
			'showcontrols', // ID
			'Show Slide Controls?', // Title
			array( $this, 'showcontrols_callback' ), // Callback
			'nano-slider-settings', // Page
			'nano_slider_settings_behaviour' // Section
		);
		add_settings_field(
			'effect', // ID
			'Slide Effect', // Title
			array( $this, 'effect_callback' ), // Callback
			'nano-slider-settings', // Page
			'nano_slider_settings_behaviour' // Section
		);
		add_settings_field(
			'showindicator', // ID
			'Show Slide Indicator?', // Title
			array( $this, 'showindicator_callback' ), // Callback
			'nano-slider-settings', // Page
			'nano_slider_settings_behaviour' // Section
		);
		add_settings_field(
			'orderby', // ID
			__( 'Order Slides By', 'nano-slider-settings' ), // Title
			array( $this, 'orderby_callback' ), // Callback
			'nano-slider-settings', // Page
			'nano_slider_settings_behaviour' // Section
		);


		// Carousel Setup Section
		add_settings_field(
			'twbs', // ID
			__( 'Twitter Bootstrap Version', 'nano-slider-settings' ), // Title
			array( $this, 'twbs_callback' ), // Callback
			'nano-slider-settings', // Page
			'nano_slider_settings_setup' // Section
		);
		add_settings_field(
			'image_size', // ID
			__( 'Image Size', 'nano-slider-settings' ), // Title
			array( $this, 'image_size_callback' ), // Callback
			'nano-slider-settings', // Page
			'nano_slider_settings_setup' // Section
		);
		add_settings_field(
			'use_javascript_animation', // ID
			__( 'Use Javascript to animate carousel?', 'nano-slider-settings' ), // Title
			array( $this, 'use_javascript_animation_callback' ), // Callback
			'nano-slider-settings', // Page
			'nano_slider_settings_setup' // Section
		);


		// Markup Section
		add_settings_field(
			'customprev', // ID
			__( 'Custom prev button class', 'nano-slider-settings' ), // Title
			array( $this, 'customprev_callback' ), // Callback
			'nano-slider-settings', // Page
			'nano_slider_settings_markup' // Section
		);
		add_settings_field(
			'customnext', // ID
			__( 'Custom next button class', 'nano-slider-settings' ), // Title
			array( $this, 'customnext_callback' ), // Callback
			'nano-slider-settings', // Page
			'nano_slider_settings_markup' // Section
		);
		add_settings_field(
			'before_title', // ID
			__( 'HTML before title', 'nano-slider-settings' ), // Title
			array( $this, 'before_title_callback' ), // Callback
			'nano-slider-settings', // Page
			'nano_slider_settings_markup' // Section
		);
		add_settings_field(
			'after_title', // ID
			__( 'HTML after title', 'nano-slider-settings' ), // Title
			array( $this, 'after_title_callback' ), // Callback
			'nano-slider-settings', // Page
			'nano_slider_settings_markup' // Section
		);
		add_settings_field(
			'before_caption', // ID
			__( 'HTML before caption text', 'nano-slider-settings' ), // Title
			array( $this, 'before_caption_callback' ), // Callback
			'nano-slider-settings', // Page
			'nano_slider_settings_markup' // Section
		);
		add_settings_field(
			'after_caption', // ID
			__( 'HTML after caption text', 'nano-slider-settings' ), // Title
			array( $this, 'after_caption_callback' ), // Callback
			'nano-slider-settings', // Page
			'nano_slider_settings_markup' // Section
		);

	}

	// Sanitize each setting field as needed -  @param array $input Contains all settings fields as array keys
	public function sanitize( $input ) {
		$new_input = array();
		foreach ( $input as $key => $var ) {
			if ( 'twbs' == $key || 'interval' == $key || 'background_images_height' == $key ) {
				$new_input[ $key ] = absint( $input[ $key ] );
			} elseif ( 'link_button_before' == $key || 'link_button_after' == $key || 'before_title' == $key || 'after_title' == $key || 'before_caption' == $key || 'after_caption' == $key ) {
				$new_input[ $key ] = $input[ $key ]; // Don't sanitise these, meant to be html!
			} else {
				$new_input[ $key ] = sanitize_text_field( $input[ $key ] );
			}
		}

		return $new_input;
	}

	// Print the Section text
	public function nano_slider_settings_behaviour_header() {
		echo '<p>' . __( 'Basic setup of how each Carousel will function, what controls will show and which images will be displayed.', 'nano-slider-settings' ) . '</p>';
	}

	public function nano_slider_settings_setup() {
		echo '<p>' . __( 'Change the setup of the carousel - how it functions.', 'nano-slider-settings' ) . '</p>';
	}

	public function nano_slider_settings_link_buttons_header() {
		echo '<p>' . __( 'Options for using a link button instead of linking the image directly.', 'nano-slider-settings' ) . '</p>';
	}

	public function nano_slider_settings_markup_header() {
		echo '<p>' . __( 'Customise which CSS classes and HTML tags the Carousel uses.', 'nano-slider-settings' ) . '</p>';
	}

	// Callback functions - print the form inputs
	// Carousel behaviour
	public function interval_callback() {
		printf( '<input type="text" id="interval" name="nano_slider_settings[interval]" value="%s" size="15" />',
		isset( $this->options['interval'] ) ? esc_attr( $this->options['interval'] ) : '' );
		echo '<p class="description">' . __( 'How long each image shows for before it slides. Set to 0 to disable animation.', 'nano-slider-settings' ) . '</p>';
	}

	public function showcaption_callback() {
		if ( isset( $this->options['showcaption'] ) && 'false' == $this->options['showcaption'] ) {
			$nano_slider_showcaption_t = '';
			$nano_slider_showcaption_f = ' selected="selected"';
		} else {
			$nano_slider_showcaption_t = ' selected="selected"';
			$nano_slider_showcaption_f = '';
		}
		echo '<select id="showcaption" name="nano_slider_settings[showcaption]">
			<option value="true" ' . $nano_slider_showcaption_t . '>' . __( 'Show', 'nano-slider-settings' ) . '</option>
			<option value="false" ' . $nano_slider_showcaption_f . '>' . __( 'Hide', 'nano-slider-settings' ) . '</option>
		</select>';
	}

	public function showcontrols_callback() {
		if ( isset( $this->options['showcontrols'] ) && 'false' == $this->options['showcontrols'] ) {
			$nano_slider_showcontrols_t = '';
			$nano_slider_showcontrols_f = ' selected="selected"';
			$nano_slider_showcontrols_c = '';
		} elseif ( isset( $this->options['showcontrols'] ) && 'true' == $this->options['showcontrols'] ) {
			$nano_slider_showcontrols_t = ' selected="selected"';
			$nano_slider_showcontrols_f = '';
			$nano_slider_showcontrols_c = '';
		} elseif ( isset( $this->options['showcontrols'] ) && 'custom' == $this->options['showcontrols'] ) {
			$nano_slider_showcontrols_t = '';
			$nano_slider_showcontrols_f = '';
			$nano_slider_showcontrols_c = ' selected="selected"';
		}
		echo '<select id="showcontrols" name="nano_slider_settings[showcontrols]">
			<option value="true" ' . $nano_slider_showcontrols_t . ' >' . __( 'Show', 'nano-slider-settings' ) . '</option>
			<option value="false" ' . $nano_slider_showcontrols_f . ' >' . __( 'Hide', 'nano-slider-settings' ) . '</option>
			<option value="custom" ' . $nano_slider_showcontrols_c . ' >' . __( 'Custom', 'nano-slider-settings' ) . '</option>
		</select>';
	}

	public function effect_callback() {
		if ( isset( $this->options['effect'] ) && 'hslide' == $this->options['effect'] ) {
			$nano_slider_effect_t = ' selected="selected"';
			$nano_slider_effect_f = '';
			$nano_slider_effect_c = '';
		} elseif ( isset( $this->options['effect'] ) && 'fade' == $this->options['effect'] ) {
			$nano_slider_effect_t = '';
			$nano_slider_effect_f = ' selected="selected"';
			$nano_slider_effect_c = '';
		} elseif ( isset( $this->options['effect'] ) && 'vslide' == $this->options['effect'] ) {
			$nano_slider_effect_t = '';
			$nano_slider_effect_f = '';
			$nano_slider_effect_c = ' selected="selected"';
		} else {
			$nano_slider_effect_t = '';
			$nano_slider_effect_f = ' selected="selected"';
			$nano_slider_effect_c = '';
		}
		echo '<select id="effect" name="nano_slider_settings[effect]">
			<option value="hslide" ' . $nano_slider_effect_t . ' >' . __( 'Horizontal Slide', 'nano-slider-settings' ) . '</option>
			<option value="vslide" ' . $nano_slider_effect_c . ' >' . __( 'Vertical Slide', 'nano-slider-settings' ) . '</option>
			<option value="fade" ' . $nano_slider_effect_f . ' >' . __( 'Fade', 'nano-slider-settings' ) . '</option>
			
		</select>';
		echo '<p class="description">' . __( 'Sliding Effect of Image. (Eg.- Fade, Horizontal Slide, Vertical Slide)', 'nano-slider-settings' ) . '</p>';
	}

	public function showindicator_callback() {
		if ( isset( $this->options['showindicator'] ) && 'false' == $this->options['showindicator'] ) {
			$nano_slider_showindicator_t = '';
			$nano_slider_showindicator_f = ' selected="selected"';
			$nano_slider_showindicator_c = '';
		} elseif ( isset( $this->options['showindicator'] ) && 'true' == $this->options['showindicator'] ) {
			$nano_slider_showindicator_t = ' selected="selected"';
			$nano_slider_showindicator_f = '';
			$nano_slider_showindicator_c = '';
		} elseif ( isset( $this->options['showindicator'] ) && 'numbered' == $this->options['showindicator'] ) {
			$nano_slider_showindicator_t = '';
			$nano_slider_showindicator_f = '';
			$nano_slider_showindicator_c = ' selected="selected"';
		}
		echo '<select id="showindicator" name="nano_slider_settings[showindicator]">
			<option value="true"' . $nano_slider_showindicator_t . '>Bullet Indicators</option>
			<option value="false"' . $nano_slider_showindicator_f . '>Hide Indicators</option>
			<option value="numbered"' . $nano_slider_showindicator_c . '>Numbered Indicators</option>
		</select>';
	}

	public function orderby_callback() {
		$orderby_options = array(
			'menu_order' => __( 'Menu order, as set in Carousel overview page', 'nano-slider-settings' ),
			'date'       => __( 'Date slide was published', 'nano-slider-settings' ),
			'rand'       => __( 'Random ordering', 'nano-slider-settings' ),
			'title'      => __( 'Slide title', 'nano-slider-settings' ),
		);
		echo '<select id="orderby" name="nano_slider_settings[orderby]">';
		foreach ( $orderby_options as $val => $option ) {
			echo '<option value="' . $val . '"';
			if ( isset( $this->options['orderby'] ) && $this->options['orderby'] == $val ) {
				echo ' selected="selected"';
			}
			echo ">$option</option>";
		}
		echo '</select>';
	}

	public function order_callback() {
		if ( isset( $this->options['order'] ) && 'DESC' == $this->options['order'] ) {
			$nano_slider_showcontrols_a = '';
			$nano_slider_showcontrols_d = ' selected="selected"';
		} else {
			$nano_slider_showcontrols_a = ' selected="selected"';
			$nano_slider_showcontrols_d = '';
		}
		echo '<select id="order" name="nano_slider_settings[order]">
			<option value="ASC" ' . $nano_slider_showcontrols_a . ' >' . __( 'Ascending', 'nano-slider-settings' ) . '</option>
			<option value="DESC" ' . $nano_slider_showcontrols_d . ' >' . __( 'Decending', 'nano-slider-settings' ) . '</option>
		</select>';
	}

	public function category_callback() {
		$cats = get_terms( 'nano_slider_category' );
		echo '<select id="orderby" name="nano_slider_settings[category]">
			<option value="">' . __( 'All Categories', 'nano-slider-settings' ) . '</option>';
		foreach ( $cats as $cat ) {
			echo '<option value="' . $cat->name . '"';
			if ( isset( $this->options['category'] ) && $this->options['category'] == $cat->name ) {
				echo ' selected="selected"';
			}
			echo '>' . $cat->name . '</option>';
		}
		echo '</select>';
	}

	// Setup Section
	public function twbs_callback() {
		if ( isset( $this->options['twbs'] ) && '3' == $this->options['twbs'] ) {
			$nano_slider_twbs3 = ' selected="selected"';
			$nano_slider_twbs4 = '';
		} else {
			$nano_slider_twbs3 = '';
			$nano_slider_twbs4 = ' selected="selected"';
		}
		echo '<select id="twbs" name="nano_slider_settings[twbs]">
			<option value="3" ' . $nano_slider_twbs3 . ' >3.x (Default)</option>
			<option value="4" ' . $nano_slider_twbs4 . ' >4.x </option>
		</select>';
		echo '<p class="description">' . __( "Set according to which version of Bootstrap you're using.", 'nano-slider-settings' ) . '</p>';
	}

	public function image_size_callback() {
		$image_sizes = get_intermediate_image_sizes();
		echo '<select id="image_size" name="nano_slider_settings[image_size]">
			<option value="full"';
		if ( isset( $this->options['image_size'] ) && 'full' == $this->options['image_size'] ) {
			echo ' selected="selected"';
		}
		echo '>Full (default)</option>';
		foreach ( $image_sizes as $size ) {
			echo '<option value="' . $size . '"';
			if ( isset( $this->options['image_size'] ) && $this->options['image_size'] == $size ) {
				echo ' selected="selected"';
			}
			echo '>' . ucfirst( $size ) . '</option>';
		}
		echo '</select>';
		echo '<p class="description">' . __( 'If your carousels are small, you can use a smaller image size to increase page speed.', 'nano-slider-settings' ) . '</p>';
	}

	public function use_javascript_animation_callback() {
		echo '<select id="use_javascript_animation" name="nano_slider_settings[use_javascript_animation]">';
		echo '<option value="1"';
		if ( isset( $this->options['use_javascript_animation'] ) && 1 == $this->options['use_javascript_animation'] ) {
			echo ' selected="selected"';
		}
		echo '>Yes (default)</option>';
		echo '<option value="0"';
		if ( isset( $this->options['use_javascript_animation'] ) && 0 == $this->options['use_javascript_animation'] ) {
			echo ' selected="selected"';
		}
		echo '>No</option>';
		echo '</select>';
		echo '<p class="description">' . __( "The Bootstrap Carousel is designed to work usign data-attributes. Sometimes the animation doesn't work correctly with this, so the default is to include a small portion of Javascript to fire the carousel. You can choose not to include this here.", 'nano-slider-settings' ) . '</p>';
	}

}

if ( is_admin() ) {
	$nano_slider_settings_page = new nano_slider_settings_page();
}

// Add settings link on plugin page
function nano_slider_settings_link( $links ) {
	$settings_link = '<a href="edit.php?post_type=nano_slider&page=nano-slider-settings">' . __( 'Settings', 'nano-slider-settings' ) . '</a>';
	array_unshift( $links, $settings_link );

	return $links;
}

$nano_slider_plugin = NANO_PLUGIN_BASENAME;
add_filter( "plugin_action_links_$nano_slider_plugin", 'nano_slider_settings_link' );


function nano_slider_msg() {

}

// Now we set that function up to execute when the admin_notices action is called
add_action( 'admin_notices', 'nano_slider_msg' );
