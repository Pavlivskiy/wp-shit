<?php
// wp_enqueue_style( 'nano_slider', plugins_url( 'asset/css/nano_slider_advanced_4.css', __FILE__ ), array(), TWABC_VERSION );
// First content - the carousel indicators
if ( count( $images ) > 1 ) {

	if ( 'false' != $atts['showindicator'] ) { ?>

		<ol class="carousel-indicators">
			<?php
			$ind = 0;
			foreach ( $images as $key => $image ) {
				$ind ++;
				?>
				<li data-target="#nano_slider_<?php echo $id; ?>"
						data-slide-to="<?php echo $key; ?>" <?php echo 0 == $key ? 'class="active"' : ''; ?>><?php if ( 'numbered' === $atts['showindicator'] ) {
						echo $ind;
					} ?></li>
			<?php } ?>
		</ol>
	<?php }
} ?>

	<div class="carousel-inner" style="height: 400px;">
		<?php
		// Carousel Content
		foreach ( $images as $key => $image ) {

			$haystack    = array( 'webm', 'mp4', 'ogv' );
			$is_video    = false;
			$sources     = '';
			$device_type = is_mobile();

			if ( strpos( $image['img_src'], '1920' ) !== false ) {
				$res_replace = '1920';
			}
			if ( strpos( $image['img_src'], '1280' ) !== false ) {
				$res_replace = '1280';
			}
			if ( strpos( $image['img_src'], '720' ) !== false ) {
				$res_replace = '720';
			}

			// Lets grab the videos related to the selected video (if it exists) and add to our sources.
			foreach ( $haystack as $string ) {
				if ( strpos( $image['img_src'], $string ) !== false ) {
					$is_video = true;
					foreach ( $haystack as $sub_str ) {

						if ( 'desktop' == $device_type ) {
							$resolution = '1920';
						}
						if ( 'tablet' == $device_type ) {
							$resolution = '1280';
						}
						if ( 'mobile' == $device_type ) {
							$resolution = '720';
						}
						$replace = array(
							$res_replace => $resolution,
						);

						$file = strtr( $image['video_dir'], $replace );


						if ( file_exists( $file ) ) {

							$replace_alt = array(
								$string      => $sub_str,
								$res_replace => $resolution,
							);

							$replace_poster = array(
									$string  => 'jpg',
								$res_replace => $resolution,
							);

							$file     = strtr( $image['img_src'], $replace_alt );
							$poster   = file_exists( str_replace( $string, 'jpg', $image['video_dir'] ) ) ? ' poster="' . strtr( $image['img_src'], $replace_poster ) . '"' : '';
							$sources .= '<source src="' . $file . '" type="video/' . $sub_str . '"></source>';
						}
					}
				}
			}

			if ( true == $is_video ) {
				$background  = '<video class="object_fitt" id="video" muted="true" autoPlay="true" loop="true" playsInline="true"'. $poster .'>';
				$background .= $sources;
				$background .= '</video>';
			} else {
				$background = '<img class="object_fitt" src="' . $image['img_src'] . '" alt="' . $image['title'] . '" />';
			}


			?>

			<div style="height: 400px;" class="carousel-item <?php echo 0 == $key ? 'active' : ''; ?>"
					id="nano_slider-item-<?php echo $image['post_id']; ?>">
				<?php
				// Regular behaviour - display image with link around it
				// The Caption div
				// Caption

				echo $background;
				if ( strlen( $image['content'] ) > 0 || strlen( $image['title'] ) > 0 ) {
					echo '<div class="carousel-caption" style="top:10%; bottom: 10%;">';
					echo '<div class="container h-100">';
					echo '<div class="row align-items-start text-white h-100">';
					echo '<div class="fadein text-left col-sm-7  border-left border-primary align-self-start" style="border-width: 10px; border-left-width: 10px !important;">';
					echo '<h1 class="display-4">' . $image['title'] . '</h1>';
					echo '<p class="lead">' . $image['content'] . '</p>';
					echo ( strlen( $image['main_url'] ) > 8 ? '<a class="strong" href="' . $image['main_url'] . '">Read More</a>' : '' );
					echo '</div>';
					echo '<div class="fadein text-sm-right col-sm-5 align-self-end">';
					echo ( isset ( $image['button_1_name'] ) && strlen( $image['button_1_name'] ) > 1 ? '<a class="btn btn-sm btn-outline-primary" href="' . $image['button_1_url'] . '">' . $image['button_1_name'] . '</a>' : '' );
					echo ( isset ( $image['button_2_name'] ) && strlen( $image['button_2_name'] ) > 1 ? '<a class="btn btn-sm btn-outline-primary ml-2" href="' . $image['button_2_url'] . '">' . $image['button_2_name'] . '</a>' : '' );
					echo '</div>';
					echo '</div></div>';
					echo '</div>';
				}
				?>
			</div>
		<?php } ?>
	</div>

<?php
// Previous / Next controls
if ( count( $images ) > 1 ) {
	if ( 'true' === $atts['showcontrols'] ) { ?>
		<a class="carousel-control-prev" href="#nano_slider_<?php echo $id; ?>" data-slide="prev" role="button">
			<i class="fa fa-3x fa-flip-horizontal icon-arrow-right fa-inverse" aria-hidden="true"></i>
			<span class="sr-only">Previous</span></a>
		<a class="carousel-control-next" href="#nano_slider_<?php echo $id; ?>" data-slide="next" role="button">
			<i class="fa fa-3x icon-arrow-right fa-inverse" aria-hidden="true"></i>
			<span class="sr-only">Next</span></a>
		</a>
	<?php }
} ?>