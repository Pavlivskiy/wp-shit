<?php if ( ! defined( 'ABSPATH' ) ) {
	die( 'Direct access forbidden.' );
}

// Add the Shortcodes

// Removing the Backer Grid Shortcode
remove_shortcode( 'project_grid' );

add_action( 'init', 'nano_shortcodes', 99 );

function nano_shortcodes() {
		if ( is_mobile() == 'mobile' ) {
			add_shortcode( 'project_grid', 'nano_project_grid_mobile_shortcode' );
			register_block_type( 'nanosteam/carousel', array(
				'render_callback' => 'nano_project_grid_mobile_shortcode',
			) );
		} else {
			add_shortcode( 'project_grid', 'nano_project_grid_shortcode' );
			register_block_type( 'nanosteam/carousel', array(
				'render_callback' => 'nano_project_grid_shortcode',
			) );
		}
	add_shortcode( 'nano_categories', 'nano_list_categories' );
	add_shortcode( 'nano_start_project_form', 'nano_start_checkbox' );
}


/**
 * Listing all projects categories
 *
 * @return string
 */
function nano_list_categories() {
	$args     = array(
		'hide_empty' => false,
	);
	$taxonomy = 'project_category';
	$terms    = get_terms( $taxonomy, $args ); // Get all terms of a taxonomy
	$counter  = 0;

	/*
	// Sorting by categories with the most submissions
	usort( $terms, function ( $a, $b ) {
		return $a->count - $b->count;
	} );
	*/

	//$terms = array_reverse( $terms );

	$collapsed = is_mobile() == 'mobile' ? 'collapse' : 'collapse show';
	$collapsed_button = is_mobile() == 'mobile' ? 'false' : 'true';

	$array_terms = array(
		'earth-science',
		'technology',
		'art-and-design',
		'engineering',
		'mathematics',
	);

	if ( is_mobile() == 'mobile' ) {
		$array_terms = array();
	}

	if ( $terms && ! is_wp_error( $terms ) ) {

		$html = '<div class="mb-2 mb-lg-0 ' . $collapsed . '" id="categoryButtons"><div class="row">';

		foreach ( $terms as $term ) {

			$active  = is_tax( 'project_category', $term->slug ) ? ' active' : '';
			$term_id = $term->term_id;
			$icon    = get_term_meta( $term_id, 'icon_id', true ) != '' ? get_term_meta( $term_id, 'icon_id', true ) : 'icon-' . $term->slug;

			if ( 0 == $counter && is_mobile() !== 'mobile' ) {
				$html .= '<div class="col-12 pt-4 pt-lg-0"><div class="row justify-content-md-center pb-4"><div class="col-md-1"></div> ';
				$html .= '<div class="col-md-2 col-sm-3 col text-center"><a class=" btn btn-sm p-0" href="' . get_term_link( 'earth-science', $taxonomy ) . '"><i class="hero_cat mb-2 fa-3x icon-earth-science"></i><span class="d-block text-dark">Earth <strong class="text-primary">S</strong>cience</span></a></div>';
				$html .= '<div class="col-md-2 col-sm-3 col text-center"><a class=" btn btn-sm p-0" href="' . get_term_link( 'technology', $taxonomy ) . '"><i class="hero_cat mb-2 fa-3x icon-technology"></i><span class="d-block text-dark"><strong class="text-primary">T</strong>echnology</span></a></div>';
				$html .= '<div class="col-md-2 col-sm-3 col text-center"><a class=" btn btn-sm p-0" href="' . get_term_link( 'engineering', $taxonomy ) . '"><i class="hero_cat mb-2 fa-3x icon-engineering"></i><span class="d-block text-dark"><strong class="text-primary">E</strong>ngineering</span></a></div>';
				$html .= '<div class="col-md-2 col-sm-3 col text-center"><a class=" btn btn-sm p-0" href="' . get_term_link( 'art-and-design', $taxonomy ) . '"><i class="hero_cat mb-2 fa-3x icon-art-design"></i><span class="d-block text-dark"><strong class="text-primary">A</strong>rt & Design</span></a></div>';
				$html .= '<div class="col-md-2 col-sm-3 col text-center"><a class=" btn btn-sm p-0" href="' . get_term_link( 'mathematics', $taxonomy ) . '"><i class="hero_cat mb-2 fa-3x icon-mathematics"></i><span class="d-block text-dark"><strong class="text-primary">M</strong>athematics</span></a></div>';
				$html .= '<div class="col-md-1"></div></div></div>';
			}
			// Navigation Highlight

			if ( ! in_array( $term->slug, $array_terms ) ) {

				$html .= '<div class="col col-sm-3 col-xl-2">';
				$html .= '<a class="' . $active . ' btn btn-sm p-0" href="' . get_term_link( $term, $taxonomy ) . '">';
				$html .= '<i class="fa-2x' . ' ' . $icon . '"></i>';
				$html .= '<span class="ml-2 text-dark">' . $term->name . '</span>';
				$html .= '</a></div>';

			}

			$counter ++;
		}
		$html .= '</div></div>';
		$html .= '<button class="btn btn-sm btn-block btn-outline-primary d-lg-none mb-4 mb-lg-0'. ( 'mobile' === is_mobile() ? ' mt-4' : '' ) .'" type="button" data-toggle="collapse" data-target="#categoryButtons" aria-controls="categoryButtons" aria-expanded="' . $collapsed_button . '" aria-label="Toggle navigation">Toggle Categories <i class="fa fa-angle-'. ( $collapsed_button == 'true' ? 'up' : 'down' ) .'" aria-hidden="true"></i></button>';
	}

	return $html;

}


/**
 * Displays a checkbox to the user on the project welcome screen to skip seeing it again
 *
 * @return string
 */
function nano_start_checkbox() {

	$current_user = wp_get_current_user();
	$user_id      = absint( $current_user->ID );

	$start_project_preference = get_user_meta( $user_id, 'nano_start_project_check', true );
	$checked                  = 'nano_start_project_check' == $start_project_preference ? 'checked' : '';

	$content  = '<form action="/dashboard/?create_project=1" method="POST" id="nano_start_project" name="nano_start_project" enctype="multipart/form-data">';
	$content .= '<div class="form-row form-group">';
	$content .= '<div class="offset-sm-6 col-sm-6 text-right">';
	$content .= '<button type="submit" class="create_project btn btn-primary">Start your project</button>';
	$content .= '<button type="button" class="ml-2 btn btn-secondary" data-dismiss="modal">Cancel</button>';
	$content .= '</div></div>';
	$content .= '<div class="form-row form-group mb-4 mt-1">';
	$content .= '<div class="d-flex col"><div class="ml-auto">';
	$content .= '<div class="custom-control custom-checkbox">';
	$content .= '<input ' . $checked . ' class="form-control form-control-sm custom-control-input" name="nano_start_project_check" id="nano_start_project_check" value="nano_start_project_check" type="checkbox">';
	$content .= '<label class="custom-control-label" for="nano_start_project_check">Don\'t display this again.</label>';
	$content .= '</div></div></div></div></form>';

	return $content;

}

/**
 * sees if the checkbox is checked from and skips the welcome page
 */
function check_nano_start_checkbox() {
	if ( isset( $_POST['nano_start_project_check'] ) && 'nano_start_project_check' == $_POST['nano_start_project_check'] ) {

		$current_user = wp_get_current_user();
		$user_id      = absint( $current_user->ID );

		$start_project_preference = get_user_meta( $user_id, 'nano_start_project_check', true );

		update_user_meta( $user_id, 'nano_start_project_check', $_POST['nano_start_project_check'], $start_project_preference );
	}
}

add_action( 'init', 'check_nano_start_checkbox' );

/*
 *  Nano Projects Grid
 *
 * [nano_grid columns="#" max="#" category="#" deck="#" orderby="days_left" order="ASC" author="admin"]
 * Required Attributes
 * columns: How many projects wide.
 * max: The number of projects you want to show in the grid. Using -1 will show all filtered projects (good to set this to a multiple of the columns).
 *
 * Optional Attributes
 * category: Enter the category ID of the ‘Project Category’ taxonomy if you only want to show projects from certain categories.
 * deck: Enter the ID number of the custom deck you wish to use for display.
 * orderby: You can set what you want the projects to order by. Possible values are ‘featured_projects‘, ‘days_left’, ‘percent_raised’, ‘funds_raised’, ‘rand’ (random), ‘title’, ‘date’ (default).
 * order: You can set the orderby to order ascending or descending using these values: ‘ASC’ or ‘DESC’.
 * author: Enter the username of the author’s post you wish to display. Only one author is allowed.
 */


function nano_project_grid_shortcode( $atts ) {

	$orderby             = isset( $atts['orderby'] ) ? $atts['orderby'] : 'date';
	$order               = isset( $atts['order'] ) ? $atts['order'] : 'ASC';
	$atts['category']    = isset( $atts['category'] ) ? ( '0' === $atts['category'] ? null : $atts['category'] ) : null;
	$atts['deskColumns'] = isset( $atts['deskColumns'] ) ? $atts['deskColumns'] : '3';
	$atts['tabColumns']  = isset( $atts['tabColumns'] ) ? $atts['tabColumns'] : '2';
	$atts['mobColumns']  = isset( $atts['mobColumns'] ) ? $atts['mobColumns'] : '1';


	if ( is_mobile() === 'tablet' ) {
		$atts['visible'] = isset( $atts['visible'] ) ? $atts['visible'] : ( isset( $atts['tabColumns'] ) ? $atts['tabColumns'] : '2' );
	} else {
		$atts['visible']     = isset( $atts['visible'] ) ? $atts['visible'] : ( isset( $atts['deskColumns'] ) ? $atts['deskColumns'] : '4' );
	}

	// Default attributes

	extract( shortcode_atts( array(
		'el_class'    => '',
		'category'    => '',
		'author'      => '',
		'post_status' => 'publish',
		'orderby'     => 'date',
		'order'       => 'ASC',
		'max'         => '-1',
		'visible'     => '3',
		'pagination'  => 'no',
		'style'       => 'carousel',
	), $atts ) );

	// Start carousel (if case)

	$rand_id = rand( 99, 9999 );

	// Define categories if present (multiple)

	$carousel = true;

	if ( isset( $atts['category'] ) && '' != $atts['category'] ) {

		$category = $atts['category'];
		$args     = array(
			'post_type'   => 'ignition_product',
			'post_status' => 'publish',
			'meta_query'  => array(
				'relation' => 'OR',
				//This additional query is to find only projects which are still fundraising
				array(
					'key'     => 'ign_project_closed',
					'compare' => 'NOT EXISTS',
				),
				array(
					'key'     => 'ign_project_closed',
					'value'   => '1',
					'compare' => '!=',
				),
			),
			'tax_query'   => array(
				//This additional query is to find project within certain categories
				array(
					'taxonomy' => 'project_category',
					'field'    => 'id',
					'terms'    => explode( ',', $category ),
				),
			),
		);
	} else {
		$args = array(
			'post_type'   => 'ignition_product',
			'post_status' => 'publish',
			'meta_query'  => array(
				'relation' => 'OR',
				//This additional query is to find only projects which are still fundraising
				array(
					'key'     => 'ign_project_closed',
					'compare' => 'NOT EXISTS',
				),
				array(
					'key'     => 'ign_project_closed',
					'value'   => '1',
					'compare' => '!=',
				),
			),
		);
	}

	// Define order based on given attributes

	if ( 'days_left' == $orderby ) {
		$args['orderby']  = 'meta_value_num';
		$args['meta_key'] = 'ign_days_left';
	} elseif ( 'percent_raised' == $orderby ) {
		$args['orderby']  = 'meta_value_num';
		$args['meta_key'] = 'ign_percent_raised';
	} elseif ( 'funds_raised' == $orderby ) {
		$args['orderby']  = 'meta_value_num';
		$args['meta_key'] = 'ign_fund_raised';
	} elseif ( 'featured' == $orderby ) {
		$args['orderby']  = 'meta_value_num';
		$args['meta_key'] = 'krown_idp_featured';
	} elseif ( 'popular' == $orderby ) {
		$args['orderby']  = 'meta_value_num';
		$args['meta_key'] = 'nano_idp_popular';
	} else {
		$args['orderby'] = $orderby;
	}

	$args['order'] = $order;

	// Define author if present (single)

	if ( '' != $author ) {
		$args['author_name'] = $author;
	}

	// Define number of projects

	$args['posts_per_page'] = $max;

	// Define pagination

	if ( 'yes' == $pagination ) {
		$args['paged'] = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : ( get_query_var( 'page' ) ? get_query_var( 'page' ) : 1 );
	}

	// Order projects by geo location to user
	if ( 'zip' == $orderby ) {
		$args = function_exists( 'modify_wp_query_geo_location' ) ? modify_wp_query_geo_location( $args ) : '';
	}

	// Start query and get object
	$all_posts = new WP_Query( $args );

	ob_start();

	$html = '';

	if ( 'carousel' == $style ) {
		$html .= '<div id="carousel-' . $rand_id . '" class="carousel slide">';
		$html .= '<div class="carousel-inner">';
	}

	$card_counter = 0;
	$base_counter = 0;

	if ( $all_posts->have_posts() ) {

		// Count Actual Posts
		while ( $all_posts->have_posts() ) {

			$all_posts->the_post();

			global $post;

			$tz = get_option('timezone_string');
			if (empty($tz)) {
				$tz = 'UTC';
			}
			date_default_timezone_set($tz);

			$end_date  = get_post_meta( $post->ID, 'ign_fund_end', true ) . ' 23:59:59';
			$end_date  = DateTime::createFromFormat( idf_date_format() . ' H:i:s', $end_date );
			$days_left = ( date_timestamp_get( $end_date ) - time() ) / 60 / 60 / 24;


			if ( 0 < $days_left ) {


				if ( 'featured' == $orderby ) {
					if ( get_post_meta( $post->ID, 'krown_idp_featured', true ) ) {
						$base_counter ++;
					}
				} elseif ( 'popular' == $orderby ) {
					if ( get_post_meta( $post->ID, 'nano_idp_popular', true ) ) {
						$base_counter ++;
					}
				} elseif ( 'zip' == $orderby ) {
					if ( get_post_meta( $post->ID, 'nano_location_zip', true ) != '' ) {
						$base_counter ++;
					}
				} else {
					$base_counter ++;
				}

			}

		}

		while ( $all_posts->have_posts() ) {

			$all_posts->the_post();

			global $post;

			$end_date  = get_post_meta( $post->ID, 'ign_fund_end', true ) . ' 23:59:59';
			$end_date  = DateTime::createFromFormat( idf_date_format() . ' H:i:s', $end_date );
			$days_left = ( date_timestamp_get( $end_date ) - time() ) / 60 / 60 / 24;

			if ( 0 < $days_left ) {

				if ( $card_counter < $base_counter ) {

					if ( 0 === $card_counter % $visible && 0 != $card_counter ) {
						echo '</div></div>';
					}

					if ( 0 === $card_counter % $visible ) {
						echo '<div class="carousel-item ' . ( 0 == $card_counter ? 'active' : '' ) . '"><div class="nano-style row">';
					}
				}

				if ( 'featured' == $orderby ) {
					if ( get_post_meta( $post->ID, 'krown_idp_featured', true ) ) {
						include( locate_template( 'content-ignition_product.php' ) );
						$card_counter ++;
					}
				} elseif ( 'popular' == $orderby ) {
					if ( get_post_meta( $post->ID, 'nano_idp_popular', true ) ) {
						include( locate_template( 'content-ignition_product.php' ) );
						$card_counter ++;
					}
				} elseif ( 'zip' == $orderby ) {

					if ( get_post_meta( $post->ID, 'nano_location_zip', true ) != '' ) {

						//Geo Location
						$user             = wp_get_current_user();
						$user_id          = $user->ID;
						$user_geo_meta    = get_user_meta( $user_id, 'nano_location_zip', true );
						$user_info        = '' != $user_geo_meta || false != $user_geo_meta ? (array) get_user_meta( $user_id, 'nano_location_zip', true ) : $_SESSION['user_loc'];
						$user_lat         = isset( $user_info['latitude'] ) ? $user_info['latitude'] : 0;
						$user_long        = isset( $user_info['longitude'] ) ? $user_info['longitude'] : 0;
						$project_lat_long = get_post_meta( $post->ID, 'nano_location_zip', true );
						$project_lat      = isset( $project_lat_long->latitude ) ? $project_lat_long->latitude : 0;
						$project_long     = isset( $project_lat_long->longitude ) ? $project_lat_long->longitude : 0;
						$distance         = '' != $project_lat_long || false != $project_lat_long ? (int) geo_distance( $user_lat, $user_long, $project_lat, $project_long ) . ' miles away' : '';

						include( locate_template( 'content-ignition_product.php' ) );

						$card_counter ++;
					}
				} else {
					include( locate_template( 'content-ignition_product.php' ) );
					$card_counter ++;
				}

				if ( $all_posts->post_count == $all_posts->current_post + 1 ) {

					$total = absint( is_int( $card_counter / $visible ) ? 0 : $visible - round( $card_counter / $visible, 0, PHP_ROUND_HALF_DOWN ) );

					for ( $i = 1; $i <= $total; $i ++ ) {
						echo '<div class="card fade"></div>';
					}
					echo '</div></div>';

				}
			}
		}
	}

	// Reset post data, close container and return output

	$html .= ob_get_contents();
	ob_end_clean();

	wp_reset_query();

	$html .= '</div><!-- close carousel-inner -->';

	if ( 'carousel' == $style ) {
		if ( $visible < $base_counter ) {
			$html .= '<a class="carousel-control-prev neg-mg-prev" href="#carousel-' . $rand_id . '" role="button" data-slide="prev">';
			$html .= '<i class="fa fa-2x fa-flip-horizontal icon-arrow-right" aria-hidden="true"></i><span class="sr-only">Previous</span></a>';
			$html .= '<a class="carousel-control-next neg-mg-next" href="#carousel-' . $rand_id . '" role="button" data-slide="next">';
			$html .= '<i class="fa fa-2x icon-arrow-right" aria-hidden="true"></i><span class="sr-only">Next</span></a>';
		}
		$html .= '</div><!-- close carousel -->';
	}

	// Add pagination

	if ( 'yes' == $pagination ) {
		$html .= krown_pagination( $all_posts, true, 2, false );
	}

	return $html;

}

function nano_project_grid_mobile_shortcode( $atts ) {

	$atts['orderby']     = isset( $atts['orderby'] ) ? $atts['orderby'] : 'date';
	$order               = isset( $atts['order'] ) ? $atts['order'] : 'ASC';
	$atts['category']    = isset( $atts['category'] ) ? ( '0' === $atts['category'] ? null : $atts['category'] ) : null;
	$atts['deskColumns'] = isset( $atts['deskColumns'] ) ? $atts['deskColumns'] : '3';
	$atts['tabColumns']  = isset( $atts['tabColumns'] ) ? $atts['tabColumns'] : '2';
	$atts['mobColumns']  = isset( $atts['mobColumns'] ) ? $atts['mobColumns'] : '1';
	$atts['visible']     = isset( $atts['visible'] ) ? $atts['visible'] : ( isset( $atts['mobColumns'] ) ? $atts['mobColumns'] : '1' );




		$html = '<div class="row">

	<div class="col-sm-12">

		<nav class="nav nav-pills border border-top-0 border-left-0 border-right-0">
			<a class="btn btn-sm border-bottom-2 px-2 px-sm-3 nav-item nav-link text-sm-center " data-toggle="tab" href="#days_left"
				role="tab" aria-controls="days_left"
				aria-expanded="true">' . __( 'Most Urgent', 'nanosteam_8n' ) . '</a>
			<a class="btn btn-sm px-2 px-sm-3 nav-item nav-link text-sm-center " data-toggle="tab" href="#zip" role="tab"
				aria-controls="zip">' . __( 'Nearby', 'nanosteam_8n' ) . '</a>
			<a class="btn btn-sm px-2 px-sm-3 nav-item nav-link text-sm-center active" data-toggle="tab" href="#popular" role="tab"
				aria-controls="popular">' . __( 'Popular', 'nanosteam_8n' ) . '</a>
		</nav>

	</div>

	<div class="col-md-12 col-lg-9">

		<!-- Tab panes -->
		<div class="tab-content">';

		$tabs = array(
			'days_left',
			'zip',
			'popular',
		);

		$ordered = array(
			'ASC',
			'DESC',
			'DESC',
		);

		foreach ( $tabs as $key => $tab ) {

			$orderby = $tab;
			$order   = $ordered[$key];

			// Default attributes

			extract( shortcode_atts( array(
				'el_class'    => '',
				'category'    => '',
				'author'      => '',
				'post_status' => 'publish',
				'orderby'     => 'date',
				'order'       => 'ASC',
				'max'         => '-1',
				'visible'     => '4',
				'pagination'  => 'no',
				'style'       => 'default',
			), $atts ) );

			// Start carousel (if case)

			$rand_id = rand( 99, 9999 );

			// Define categories if present (multiple)

			$carousel = true;

			if ( isset( $atts['category'] ) && '' != $atts['category'] ) {
				$category = $atts['category'];
				$args     = array(
					'post_type'   => 'ignition_product',
					'post_status' => 'publish',
					'meta_query'  => array(
						'relation' => 'OR',
						//This additional query is to find only projects which are still fundraising
						array(
							'key'     => 'ign_project_closed',
							'compare' => 'NOT EXISTS',
						),
						array(
							'key'     => 'ign_project_closed',
							'value'   => '1',
							'compare' => '!=',
						),
					),
					'tax_query'   => array(
						//This additional query is to find project within certain categories
						array(
							'taxonomy' => 'project_category',
							'field'    => 'id',
							'terms'    => explode( ',', $category ),
						),
					),
				);
			} else {
				$args = array(
					'post_type'   => 'ignition_product',
					'post_status' => 'publish',
					'meta_query'  => array(
						'relation' => 'OR',
						//This additional query is to find only projects which are still fundraising
						array(
							'key'     => 'ign_project_closed',
							'compare' => 'NOT EXISTS',
						),
						array(
							'key'     => 'ign_project_closed',
							'value'   => '1',
							'compare' => '!=',
						),
					),
				);
			}

			// Define order based on given attributes

			if ( 'days_left' == $orderby ) {
				$args['orderby']  = 'meta_value_num';
				$args['meta_key'] = 'ign_days_left';
			} elseif ( 'percent_raised' == $orderby ) {
				$args['orderby']  = 'meta_value_num';
				$args['meta_key'] = 'ign_percent_raised';
			} elseif ( 'funds_raised' == $orderby ) {
				$args['orderby']  = 'meta_value_num';
				$args['meta_key'] = 'ign_fund_raised';
			} elseif ( 'featured' == $orderby ) {
				$args['orderby']  = 'meta_value_num';
				$args['meta_key'] = 'krown_idp_featured';
			} elseif ( 'popular' == $orderby ) {
				$args['orderby']  = 'meta_value_num';
				$args['meta_key'] = 'nano_idp_popular';
			} else {
				$args['orderby'] = $orderby;
			}

			$args['order'] = $ordered[$key];

			// Define author if present (single)

			if ( '' != $author ) {
				$args['author_name'] = $author;
			}

			// Define number of projects

			$args['posts_per_page'] = $max;

			// Order projects by geo location to user
			if ( 'zip' == $tab ) {
				$args = function_exists( 'modify_wp_query_geo_location' ) ? modify_wp_query_geo_location( $args ) : '';
			}

			// Start query and get object
			$all_posts = new WP_Query( $args );

			ob_start();

			echo '<div class="tab-pane fade p-md-5 p-3 show '. ( 'popular' == $tab ? 'active' : '' ) .'" id="' . $tab . '" aria-labelledby="' . $tab . '-tab" role="tabpanel"><div class="nano-style row"><ul class="list-unstyled">';

			if ( $all_posts->have_posts() ) {

				while ( $all_posts->have_posts() ) {

					$all_posts->the_post();

					global $post;

					$distance = null;

					if ( get_post_meta( $post->ID, 'nano_location_zip', true ) != '' && 'zip' == $tab ) {

						//Geo Location
						$user             = wp_get_current_user();
						$user_id          = $user->ID;
						$user_geo_meta    = get_user_meta( $user_id, 'nano_location_zip', true );
						$user_info        = '' != $user_geo_meta || false != $user_geo_meta ? (array) get_user_meta( $user_id, 'nano_location_zip', true ) : $_SESSION['user_loc'];
						$user_lat         = isset( $user_info['latitude'] ) ? $user_info['latitude'] : 0;
						$user_long        = isset( $user_info['longitude'] ) ? $user_info['longitude'] : 0;
						$project_lat_long = get_post_meta( $post->ID, 'nano_location_zip', true );
						$project_lat      = isset( $project_lat_long->latitude ) ? $project_lat_long->latitude : 0;
						$project_long     = isset( $project_lat_long->longitude ) ? $project_lat_long->longitude : 0;
						$distance         = '' != $project_lat_long || false != $project_lat_long ? (int) geo_distance( $user_lat, $user_long, $project_lat, $project_long ) . ' miles away' : null;
					}

					include( locate_template( 'content-ignition_product.php' ) );

				}
			}

			// Reset post data, close container and return output

			echo '<form method="POST" action="/projects/?nano_filtering">';
			echo '<input type="hidden" name="categoryfilter" value="'. $tab .'">';
			echo '<button type="submit" class="mt-3 btn btn-sm btn-light border">View All</button></form>';
			echo '</div>';
			echo '</div>';

			$html .= ob_get_contents();
			ob_end_clean();

			wp_reset_query();

		}

		$html .= '</ul></div></div>';
		$html .= '</div>';

		return $html;


}



/**
 * Replacing the base revolution slider shortcode with this for mobile friendly setups
 *
 * @param $args
 * @param null $mid_content
 *
 * @return mixed|string
 */
function nano_rev_slider_shortcode( $args, $mid_content = null ) {

	extract( shortcode_atts( array( 'alias' => '' ), $args, 'rev_slider' ) );
	extract( shortcode_atts( array( 'settings' => '' ), $args, 'rev_slider' ) );
	extract( shortcode_atts( array( 'order' => '' ), $args, 'rev_slider' ) );

	if ( '' !== $settings ) {
		$settings = json_decode( str_replace( array( '({', '})', "'" ), array( '[', ']', '"' ), $settings ), true );
	}
	if ( '' !== $order ) {
		$order = explode( ',', $order );
	}

	// Added this to include tablet & mobile versions for nanosteam
	if ( is_mobile() == 'desktop' ) {
		// For desktop only
		$alias = $alias;
	} elseif ( is_mobile() == 'tablet' ) {
		// For tablet only
		$alias = $alias . '-tablet';
	} elseif ( is_mobile() == 'mobile' ) {
		$alias = $alias . '-mobile';
	}

	$sliderAlias = ( '' != $alias ) ? $alias : RevSliderFunctions::getVal( $args, 0 );

	$gal_ids = RevSliderFunctionsWP::check_for_shortcodes( $mid_content ); //check for example on gallery shortcode and do stuff

	ob_start();
	if ( ! empty( $gal_ids ) ) { //add a gallery based slider
		$slider = RevSliderOutput::putSlider( $sliderAlias, '', $gal_ids );
	} else {
		$slider = RevSliderOutput::putSlider( $sliderAlias, '', array(), $settings, $order );
	}
	$content = ob_get_contents();
	ob_clean();
	ob_end_clean();

	if ( ! empty( $slider ) ) {
		// Do not output Slider if we are on mobile
		$disable_on_mobile = $slider->getParam( 'disable_on_mobile', 'off' );
		if ( 'on' == $disable_on_mobile ) {
			$mobile = ( strstr( $_SERVER['HTTP_USER_AGENT'], 'Android' ) || strstr( $_SERVER['HTTP_USER_AGENT'], 'webOS' ) || strstr( $_SERVER['HTTP_USER_AGENT'], 'iPhone' ) || strstr( $_SERVER['HTTP_USER_AGENT'], 'iPod' ) || strstr( $_SERVER['HTTP_USER_AGENT'], 'iPad' ) || strstr( $_SERVER['HTTP_USER_AGENT'], 'Windows Phone' ) || wp_is_mobile() ) ? true : false;
			if ( $mobile ) {
				return false;
			}
		}

		$show_alternate = $slider->getParam( 'show_alternative_type', 'off' );

		if ( 'mobile' == $show_alternate || 'mobile-ie8' == $show_alternate ) {
			if ( strstr( $_SERVER['HTTP_USER_AGENT'], 'Android' ) || strstr( $_SERVER['HTTP_USER_AGENT'], 'webOS' ) || strstr( $_SERVER['HTTP_USER_AGENT'], 'iPhone' ) || strstr( $_SERVER['HTTP_USER_AGENT'], 'iPod' ) || strstr( $_SERVER['HTTP_USER_AGENT'], 'iPad' ) || strstr( $_SERVER['HTTP_USER_AGENT'], 'Windows Phone' ) || wp_is_mobile() ) {
				$show_alternate_image = $slider->getParam( 'show_alternate_image', '' );

				return '<img class="tp-slider-alternative-image" src="' . $show_alternate_image . '" data-no-retina>';
			}
		}

		//handle slider output types
		$outputType = $slider->getParam( 'output_type', '' );
		switch ( $outputType ) {
			case 'compress':
				$content = str_replace( "\n", '', $content );
				$content = str_replace( "\r", '', $content );

				return ( $content );
				break;
			case 'echo':
				echo $content; //bypass the filters
				break;
			default:
				return ( $content );
				break;
		}
	} else {
		return ( $content );
	} //normal output

}

remove_shortcode( 'rev_slider', 'rev_slider_shortcode' );
add_shortcode( 'rev_slider', 'nano_rev_slider_shortcode' );


/**
 * Adjusting Krown Columns to be bootstrap friendly
 *
 * @param $atts
 * @param $content
 *
 * @return string
 */
function nano_column_function( $atts, $content ) {

	extract( shortcode_atts( array(
		'el_class' => '',
		'width'    => '1/1',
		'pad_top'  => '5',
		'pad_bot'  => '5',
	), $atts ) );

	$html = '';

	$html .= '<div class="' . ( isset( $atts['el_position'] ) ? $atts['el_position'] . ' ' : '' );

	switch ( $width ) {
		case '1/1':
			$html .= 'col-sm-12';
			break;
		case '1/2':
			$html .= 'col-sm-6';
			break;
		case '1/4':
			$html .= 'col-sm-3';
			break;
		case '3/4':
			$html .= 'col-sm-9';
			break;
		case '1/3':
			$html .= 'col-sm-4';
			break;
		case '2/3':
			$html .= 'col-sm-8';
			break;
		default:
			$html .= 'col-sm-12';
	}

	$html .= ' ' . $el_class . ' pt-' . $pad_top . ' pb-' . $pad_bot . '">';
	$html .= do_shortcode( $content );
	$html .= '</div>';

	return $html;

}

remove_shortcode( 'krown_column' );
add_shortcode( 'krown_column', 'nano_column_function' );

// Hero Section

/* ------------------------
-----   Row   -----
------------------------------*/

/**
 * Row shortcode
 *
 * @param $atts
 * @param $content
 *
 * @return string
 */
function nano_row_function( $atts, $content ) {

	extract( shortcode_atts( array(
		'el_class' => '',
	), $atts ) );

	$html = '<div class="row ' . ( '' != $el_class ? ' ' . $el_class : '' ) . '">' . do_shortcode( $content ) . '</div>';

	return $html;

}

add_shortcode( 'row', 'nano_row_function' );

/**
 * Container shortcode
 *
 * @param $atts
 * @param $content
 *
 * @return string
 */
function nano_container_function( $atts, $content ) {

	extract( shortcode_atts( array(
		'el_class' => '',
	), $atts ) );

	$html = '<div class="container"><div class="row">';
	$html .= '' !== $el_class ? '<div class=" ' . $el_class . '" > ' . do_shortcode( $content ) . '</div > ' : do_shortcode( $content );
	$html .= '</div ></div > ';

	return $html;

}

add_shortcode( 'container', 'nano_container_function' );



/**
 * Button shortcode
 *
 * @param $atts
 * @param $content
 *
 * @return string
 */
function nano_button_function( $atts, $content ) {


	extract( shortcode_atts( array(
		'el_class' => '',
		'label'    => 'Button',
		'target'   => '_blank',
		'style'    => 'normal',
		'size'     => 'medium',
		'url'      => '#',
	), $atts ) );

	$size = 'medium' == $size ? '' : '';
	$size = 'small' == $size ? 'sm' : '';
	$size = 'large' == $size ? 'lg' : '';

	$style = 'normal' == $style ? 'btn-dark' : '';
	$style = 'color' == $style ? 'btn-primary' : '';

	$html = '<a class="btn btn-' . $size . ' ' . $style . ( '' != $el_class ? ' ' . $el_class : '' ) . '" href="' . $url . '" target="' . $target . '">' . $label . '</a>';

	return $html;

}

remove_shortcode( 'krown_button' );
add_shortcode( 'krown_button', 'nano_button_function' );



/**
 * Excerpt Shortcode
 *
 * @param string $length_callback
 * @param string $more_callback
 *
 * @return array|mixed|string|void
 */
function krown_excerpt( $length_callback = '', $more_callback = 'krown_excerptmore' ) {

	global $post;

	if ( function_exists( $length_callback ) ) {
		add_filter( 'excerpt_length', $length_callback );
	}

	if ( function_exists( $more_callback ) ) {
		add_filter( 'excerpt_more', $more_callback );
	}

	$output = get_the_excerpt();

	$post_id = $post->ID;
	$ignition_output = '' !== get_post_meta( $post_id, 'nano_project_long_description', true ) ? get_post_meta( $post_id, 'nano_project_long_description', true ) : get_post_meta( $post_id, 'ign_project_description', true );


	if ( empty( $output ) && '' == $ignition_output ) {

		$output = strip_tags( preg_replace( "~(?:\[/?)[^\]]+/?\]~s", '', $post->post_content ) );

		// If the excerpt is empty (on pages created 100% with shortcodes), we should take the content, strip shortcodes, remove all HTML tags, then return the correct number of words

		$output = explode( ' ', $output, $length_callback() );
		array_pop( $output );
		$output = implode( ' ', $output ) . $more_callback();

	} else {

		if ( '' != $ignition_output ) {
			$output = $ignition_output;
		}

		// Continue with the regular excerpt method
		$output = apply_filters( 'wptexturize', $output );
		$output = apply_filters( 'convert_chars', $output );

	}

	return $output;

}



/* ------------------------
-----   Accordion   -----
------------------------------*/


function nano_krown_accordion_function( $atts, $content ) {

	extract( shortcode_atts( array(
		'el_class' => '',
		'type'     => 'accordion',
		'opened'   => '0',
	), $atts ) );

	//$html = '<div data-opened="' . $opened . '" class="krown-accordion ' . $type . ' ' . ( $el_class != '' ? ' ' . $el_class : '' ) . ' clearfix">';
	$html = '<div id="accordion">';

	$html .= do_shortcode( $content );

	$html .= '</div>';

	return $html;

}

remove_shortcode( 'krown_accordion', 'krown_accordion_function' );
add_shortcode( 'krown_accordion', 'nano_krown_accordion_function' );


function nano_krown_accordion_section_function( $atts, $content ) {

	static $i = 1;

	extract( shortcode_atts( array(
		'title' => 'Section',
	), $atts ) );

	$html = '<div class="card">
    <div class="card-header" id="headingOne">
      <h5 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapse' . $i . '" aria-expanded="' . ( 1 == $i ? 'true' : 'false' ) . '" aria-controls="collapse' . $i . '">
          ' . $title . '
        </button>
      </h5>
    </div>

    <div id="collapse' . $i . '" class="collapse ' . ( 1 == $i ? 'show' : '' ) . '" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body">
        ' . do_shortcode( $content ) . '
      </div>
    </div>
  </div>';

	$i ++;

	return $html;

}

remove_shortcode( 'krown_accordion_section', 'krown_accordion_section_function' );
add_shortcode( 'krown_accordion_section', 'nano_krown_accordion_section_function' );

