<?php

$page_layout = isset( $post ) ? get_post_meta( $post->ID, 'krown_page_style', true ) : '';

$menu             = 'primary';
$nav_class        = 'navbar-expand-md justify-content-between';
$menu_class       = '';
$conatainer_class = 'd-block';

if ( 'mobile' === is_mobile() ) {
	$menu             = 'logged_out_mobile';
	$menu_class       = 'd-flex';
	$nav_class        = 'navbar-expand d-block pt-3 p-0 pb-0';
	$conatainer_class = 'border-bottom border-top';
}

if ( is_user_logged_in() ) {
	$menu             = 'logged_in';
	$menu_class       = '';
	$nav_class        = 'navbar-expand-md justify-content-between';
	$conatainer_class = 'collapse navbar-collapse';
}

if ( 'mobile' === is_mobile() && is_user_logged_in() ) {
	$menu             = 'logged_in_mobile';
	$menu_class       = 'd-flex';
	$nav_class        = 'navbar-expand d-block pt-3 p-0 pb-0';
	$conatainer_class = 'border-bottom border-top';
}


?>

	<!DOCTYPE html>
	<!--[if lt IE 8]>
	<html <?php language_attributes(); ?> class="ie7 ie" xmlns="http://www.w3.org/1999/xhtml"> <![endif]-->
	<!--[if IE 8]>
	<html <?php language_attributes(); ?> class="ie8 ie" xmlns="http://www.w3.org/1999/xhtml"> <![endif]-->
	<!--[if gt IE 8]><!-->
<html <?php language_attributes(); ?> xmlns="http://www.w3.org/1999/xhtml"> <!--<![endif]-->
	<head>

		<!-- META -->

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- LINKS -->

		<link rel="profile" href="http://gmpg.org/xfn/11"/>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>"/>

		<?php if ( get_option( 'krown_fav' ) != '' ) : ?>

			<link rel="shortcut icon" type="image/x-icon" href="<?php echo get_option( 'krown_fav' ); ?>"/>

		<?php endif; ?>

		<!-- WP HEAD -->

		<?php wp_head(); ?>

	</head>

<body id="body" <?php body_class( get_option( 'krown_sticky', 'regular-header' ) . ' no-touch ' . ( isset( $post ) ? get_post_meta( $post->ID, 'krown_page_layout', true ) : '' ) . ' no-js' ); ?>>

<div class="wrapper">

	<!-- Nav Start -->
	<nav class="navbar navbar-light bg-white sticky-top <?php echo $nav_class; ?>">

		<?php if ( 'mobile' === is_mobile() ) {
			echo '<div class="d-flex justify-content-center pb-3">';
		} ?>
			<!-- Logo Start -->
			<a class="navbar-brand logo" href="<?php echo home_url(); ?>"></a>
			<!-- Logo End -->

			<?php if ( is_user_logged_in() ) { ?>

				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-primary"
						aria-controls="navbar-primary" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

			<?php } ?>

		<?php if ( 'mobile' === is_mobile() ) {
			echo '</div>';
		} ?>

		<?php

		if ( has_nav_menu( 'primary' ) || has_nav_menu( 'logged_in' ) || has_nav_menu( 'logged_out_mobile' ) ) {

			wp_nav_menu(
				array(
					'container'       => 'div',
					'container_class' => $conatainer_class,
					'container_id'    => 'navbar-primary',
					'menu_class'      => 'navbar-nav ml-auto ' . $menu_class,
					'echo'            => true,
					'before'          => '',
					'after'           => '',
					'link_before'     => '',
					'link_after'      => '',
					'depth'           => 2,
					'theme_location'  => $menu,
					'walker'          => new wp_bootstrap4_navwalker(),
				)
			);

		}
		?>

	</nav>
	<!-- Nav End -->

	<!-- Project Search -->
<?php if ( get_option( 'krown_p_search', 'disabled' ) == 'enabled' ) : ?>

	<div class="container-fluid fixed-top collapse stick-margin-top" id="collapseForm">
		<div class="row justify-content-end">
			<div class="col col-sm-4 col-lg-3">
				<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
					<label for="search" class="sr-only">Search</label>
					<input class="form-control" type="search" value="<?php echo ( isset( $_GET['s'] ) ? $_GET['s'] : '' ); ?>" name="s" id="s"
							placeholder="<?php _e( 'Type and hit enter', 'krown' ); ?>"/>
				</form>
			</div>
		</div>
	</div>

<?php endif; ?>

	<!-- Main Wrapper Start -->

<div id="content"
		class="<?php echo krown_sidebar_class(); ?><?php echo krown_md_ch_class(); ?>"<?php echo get_option( 'krown_sticky' ) == 'sticky-header' ? ' style="top:' . ( get_option( 'krown_logo_height', '23' ) + 77 ) . 'px"' : ''; ?>>

<?php
krown_custom_header();
revolution_slider_page_header();
//view_var(get_user_meta( wp_get_current_user()->ID ) );
?>

<div class="<?php echo( ( $page_layout == 'no-boxed' || is_author() ) && !isset( $_GET['s'] ) ? 'container-fluid' : 'container py-sm-5' ); ?>">



<?php echo krown_check_page_title(); ?>

<?php if ( krown_sidebar_class() == 'layout-left-sidebar' || krown_sidebar_class() == 'layout-right-sidebar' ) : ?>

	<div class="row">

	<div class="col-sm-9 col-md-8 <?php echo get_post_meta( $post->ID, 'krown_page_style', true ) . ' ' . ( krown_sidebar_class() == 'layout-left-sidebar' ? 'order-last' : '' ); ?>">

<?php else : ?>

	<div class="normal-content">

<?php endif; ?>