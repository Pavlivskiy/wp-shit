jQuery( function( $ ) {
	let navState, distance, width, $window, textAdjust, count;

	const modal = $( '.modal' );
	const scrollCheck = $( '.scroll-check' );
	const instantCheckout = '';
	let formInputs;

	/* ------------------------
     *  Delete Project by Project Author
     *
     ------------------------------*/

	function ajaxDeleteProject() {
		const button = $( '.delete_project' ),
			url = id_ajaxurl,
			action = 'nano_project_author_delete_project_ajax',
			alert = $( 'div.nano_alert' );

		button.on( 'click', function( e ) {
			const postId = $( this ).parent().find( 'input' ).attr( 'data-post-id' ),
				security = $( this ).parent().find( 'input' ).val(),
				thisEl = $( this ),
				r = confirm( 'Are you sure you want to delete this project?' );

			e.preventDefault();

			if ( true === r ) {
				$.ajax( {
					type: 'POST',
					dataType: 'json',
					url: url,
					data: {
						action: action,
						security: security,
						post_id: postId,
					},
					success: function( data ) {
						if ( 'success' === data.status ) {
							thisEl.closest( 'tr.project' ).remove();
						}

						alert.removeClass( 'd-none' ).addClass( 'show' ).text( data.message );
					},
					error: function( data ) {
						console.log( data );
					},
				} );
			}
		} );
	}
	ajaxDeleteProject();

	/* ------------------------
     *  Ajax Project Filtering
     *
     ------------------------------*/

	function ajaxProjectFilter() {
		const filter = $( '#filter' );
		const content = $( '#ign-archive' );
		const morebutton = $( '#more_posts' );
		const postsPP = php_vars.posts_pp;
		let page = 1; // What page we are on.
		const ppp = postsPP; // Post per page
		const $grid = $( '.nano-style' );

		filter.submit( function() {
			$.ajax( {
				url: filter.attr( 'action' ),
				data: filter.serialize(), // form data
				type: filter.attr( 'method' ), // POST
				beforeSend: function( xhr ) {
					filter.find( 'button' ).text( 'Loading...' ); // changing the button label
				},
				success: function( response ) {
					const $data = content.html( response );

					filter.find( 'button' ).text( 'Apply filter' ); // changing the button label back
					$data.hide();
					$data.fadeIn( 500 );
					page = 1;
				},
				complete: function( response ) {
					if ( 0 < $( '#end' ).length ) {
						morebutton.hide();
					} else {
						morebutton.show();
					}
				},
			} );
			return false;
		} );

		/* ------------------------
         *  Ajax Load More Pages
         *
         ------------------------------*/

		morebutton.on( 'click', function( e ) { // When btn is pressed.
			const scrollPosition = $( window ).scrollTop();

			e.preventDefault();

			$.ajax( {
				type: 'POST',
				url: filter.attr( 'action' ),
				data: {
					action: 'more_post_ajax',
					offset: ( page * ppp ) + 1,
					ppp: ppp,
					vars: filter.serializeArray(),
					page_num: page,
				},
				beforeSend: function( xhr ) {
					morebutton.text( 'Loading...' ); // changing the button label
				},
				success: function( response ) {
					const $newcontent = $( response ).addClass( 'fadein' );
					const $data = $grid.append( $newcontent );

					morebutton.text( 'Load More' );
					page++;

					if ( 0 < $( '#end' ).length ) {
						morebutton.hide();
					} else {
						morebutton.show();
					}
				},
				complete: function( response ) {
					$( window ).scrollTop( scrollPosition );
				},
				error: function( response ) {
					console.log( response );
				},
			} );
		} );
	}

	if ( 0 < $( '#ign-archive' ).length ) {
		ajaxProjectFilter();
	}

	/* ------------------------
     *  Tooltip
     *
     ------------------------------*/

	$( function() {
		$( '[data-toggle="tooltip"]' ).tooltip();
	} );

	/* ------------------------
     *  Opening discussion tab when going to page from external URL
     *
     ------------------------------*/

	$( function() {
		let hash = window.location.hash;
		const scrollmem = $( 'body' ).scrollTop();

		hash = '#respond' === hash || ( 0 <= hash.indexOf( 'comment' ) ) ? '#discussion' : hash;
		hash && $( '.nav.nav-pills a[href="' + hash + '"]' ).tab( 'show' );

		$( '.nav-tabs a' ).click( function( e ) {
			$( this ).tab( 'show' );
			window.location.hash = this.hash;
			$( 'html,body' ).scrollTop( scrollmem );
		} );
	} );

	/* ------------------------
     *  Profile Page Check
     *
     ------------------------------*/

	if ( $( '#nano-titles' ).length ) {
		navState = $( '#nano_page' );
		$( 'a[data-toggle="tab"]' ).on( 'shown.bs.tab', function( e ) {
			//e.target // newly activated tab
			const targetElement = e.target.getAttribute( 'aria-controls' );

			//console.log(e.relatedTarget); // previous active tab
			navState.val( targetElement );
		} );

		$( '.endorsements-tab' ).on( 'click', function( e ) {
			e.preventDefault();
			$( '#endorsements-tab' ).tab( 'show' );
		} );
	}

	$( '.payment-history' ).on( 'click', function() {
		const icon = $( this ).find( '.fa' );

		icon.toggleClass( 'fa-plus fa-minus' );
	} );

	/* ------------------------
     *  Labnotes button hide/display on project page
     *
     ------------------------------*/

	if ( $( '#labnote_lnk' ).length ) {
		$( 'a[data-toggle="tab"]' ).on( 'shown.bs.tab', function( e ) {
        	const activeLink = $( e.target );
			const labnoteButton = $( '#add_lab_note' );

        	if ( activeLink.attr( 'id' ) === 'labnote_lnk' ) {
				labnoteButton.removeClass( 'd-none' );
			} else if ( ! labnoteButton.hasClass( 'd-none' ) ) {
				labnoteButton.addClass( 'd-none' );
			}
		} );
	}

	/* ------------------------
    *  Modal
    *
    ------------------------------*/

	// Add Print Classes for Modal
	modal.on( 'shown.bs.modal', function() {
		$( '.modal' ).addClass( 'printSection' );
		$( 'body' ).addClass( 'printNone' );
	} );

	// Remove classes
	modal.on( 'hidden.bs.modal', function() {
		$( '.modal' ).removeClass( 'printSection' );
		$( 'body' ).removeClass( 'printNone' );
	} );

	if ( ! jQuery( '#project_goal' ).length ) {
		jQuery( '.nano-logged-in' ).on( 'click', function( e ) {
			if ( php_vars.skip_start !== 'nano_start_project_check' ) {
				e.preventDefault();
				$( '#start-a-project-m' ).modal( 'show' );
			}
		} );
	}

	// Project profile page
	// Add more backers to the list.

	jQuery( '#more_btn' ).click( function( e ) {
		e.preventDefault();
		$( '.more_backers' ).fadeOut( 300 ).hide();
		$( '.hidden-backer' ).removeClass( 'd-none' ).addClass( 'fadein' );
	} );

	// Initiate popup modal for iframe share box
	jQuery( '.share-buttons' ).on( 'mouseover', '.social-sharing', function() {
		let container = 'body';
		if ( jQuery( '.modal-open .nano-receipt' ).length ) {
			container = '.social-sharing-options-wrapper';
		}
		jQuery( this ).css( 'cursor', 'pointer' );
		jQuery( this ).popover( {
			container: container,
			content: '',
		} );
	} );

	jQuery( '.link-terms-conditions a' ).click( function( e ) {
		e.preventDefault();
		jQuery( '#nano-terms-conditions' ).modal( 'show' );
	} );

	jQuery( '.open_checkout_modal a' ).click( function( e ) {
		e.preventDefault();
		jQuery( '#nano-checkout' ).modal( 'show' );
	} );

	jQuery( '.link-privacy-policy a' ).click( function( e ) {
		e.preventDefault();
		jQuery( '#nano-privacy' ).modal( 'show' );
	} );

	// Checkout Form

	$( '.idc_lightbox' ).on( 'shown.bs.modal', function( e ) {
		$( document ).keypress( function( e ) {
			if ( 13 == e.which ) {
				$( '.lb_level_submit' ).click();
			}
		} );
	} );

	// Ensure that any values entered into the amount backed should always be a whole number
	// Ensure the backer cannot donate more than the goal amount.
	// Checked for decimals but have allowed again for review.

	jQuery( '#value' ).on( 'keyup keypress', function( event ) {
		const value = jQuery( this ).val();
		const total = jQuery( '#total' );
		const goal = jQuery( '#max_donation_amt' ).text();

		jQuery( this ).val( jQuery( this ).val().replace( /[^0-9\.]/g, '' ) );

		if ( ( ( 46 !== event.which && 8 !== event.which ) || -1 !== $( this ).val().indexOf( '.' ) ) && ( 48 > event.which || 57 < event.which ) ) {
			event.preventDefault();
		}

		if ( ( -1 !== value.indexOf( '.' ) ) && ( 2 < value.substring( value.indexOf( '.' ) ).length ) ) {
			event.preventDefault();
		}

		//total.val(Math.round(parseFloat(value)));
		total.val( value );

		if ( value.length >= goal.length ) {
			if ( parseInt( value, 10 ) > parseInt( goal, 10 ) ) {
				jQuery( this ).val( goal );
				total.val( goal );
				jQuery( '#errmsg' ).html( 'Sorry, you can only back up to $' + goal ).show().delay( 4000 ).fadeOut( 'slow' );
			}
		}
		if ( 0 === Math.round( parseFloat( value ) ) ) {
			jQuery( '#errmsg' ).html( 'Sorry, you need to enter a valid amount' ).show().delay( 4000 ).fadeOut( 'slow' );
			jQuery( '.lb_level_submit' ).prop( 'disabled', true );
		} else {
			jQuery( '.lb_level_submit' ).prop( 'disabled', false );
		}
	} );

	// Credit Card format check

	( function( $ ) {
		/**
         *
         */
		const _self = this;

		/**
         *
         */
		_self.card_types = [
			{
				name: 'American Express',
				code: 'ax',
				pattern: /^3[47]/,
				valid_length: [ 15 ],
				formats: [
					{
						length: 15,
						format: 'xxxx xxxxxxx xxxx',
					},
				],
			}, {
				name: 'Diners Club',
				code: 'dc',
				pattern: /^3[68]/,
				valid_length: [ 14 ],
				formats: [
					{
						length: 14,
						format: 'xxxx xxxx xxxx xx',
					},
				],
			}, {
				name: 'JCB',
				code: 'jc',
				pattern: /^35/,
				valid_length: [ 16 ],
				formats: [
					{
						length: 16,
						format: 'xxxx xxxx xxxx xxxx',
					},
				],
			}, {
				name: 'Visa',
				code: 'vs',
				pattern: /^4/,
				valid_length: [ 16 ],
				formats: [
					{
						length: 16,
						format: 'xxxx xxxx xxxx xxxx',
					},
				],
			}, {
				name: 'Mastercard',
				code: 'mc',
				pattern: /^5[1-5]/,
				valid_length: [ 16 ],
				formats: [
					{
						length: 16,
						format: 'xxxx xxxx xxxx xxxx',
					},
				],
			},
		];

		/**
         *
         */
		this.isValidLength = function( cc_num, card_type ) {
			for ( const i in card_type.valid_length ) {
				if ( cc_num.length <= card_type.valid_length[ i ] ) {
					return true;
				}
			}
			return false;
		};

		/**
         *
         */
		this.getCardType = function( cc_num ) {
			for ( const i in _self.card_types ) {
				const card_type = _self.card_types[ i ];
				if ( cc_num.match( card_type.pattern ) && _self.isValidLength( cc_num, card_type ) ) {
					return card_type;
				}
			}
		};

		/**
         *
         */
		this.getCardFormatString = function( cc_num, card_type ) {
			for ( const i in card_type.formats ) {
				const format = card_type.formats[ i ];
				if ( cc_num.length <= format.length ) {
					return format.format;
				}
			}
		};

		/**
         *
         */
		this.formatCardNumber = function( cc_num, card_type ) {
			let numAppendedChars = 0;
			let formattedNumber = '';
			let cardFormatString, cardFormatIndex;

			if ( ! card_type ) {
				return cc_num;
			}

			cardFormatString = _self.getCardFormatString( cc_num, card_type );
			for ( let i = 0; i < cc_num.length; i++ ) {
				cardFormatIndex = i + numAppendedChars;
				if ( ! cardFormatString || cardFormatIndex >= cardFormatString.length ) {
					return cc_num;
				}

				if ( 'x' !== cardFormatString.charAt( cardFormatIndex ) ) {
					numAppendedChars++;
					formattedNumber += cardFormatString.charAt( cardFormatIndex ) + cc_num.charAt( i );
				} else {
					formattedNumber += cc_num.charAt( i );
				}
			}

			return formattedNumber;
		};

		/**
         *
         */
		this.monitorCcFormat = function( $elem ) {
			const cc_num = $elem.val().replace( /\D/g, '' );
			const card_type = _self.getCardType( cc_num );
			$elem.val( _self.formatCardNumber( cc_num, card_type ) );
			_self.addCardClassIdentifier( $elem, card_type );
		};

		/**
         *
         */
		this.addCardClassIdentifier = function( $elem, card_type ) {
			let classIdentifier = 'cc_type_unknown';
			if ( card_type ) {
				classIdentifier = 'cc_type_' + card_type.code;
			}

			if ( ! $elem.hasClass( classIdentifier ) ) {
				let classes = '';
				for ( const i in _self.card_types ) {
					classes += 'cc_type_' + _self.card_types[ i ].code + ' ';
				}
				$elem.removeClass( classes + 'cc_type_unknown' );
				$elem.addClass( classIdentifier );
			}
		};

		/**
         *
         */
		$( function() {
			$( document ).find( '.card-number.required' ).each( function() {
				const $elem = $( this );
				if ( $elem.is( 'input' ) ) {
					$elem.on( 'input', function() {
						_self.monitorCcFormat( $elem );
					} );
				}
			} );
		} );
	}( jQuery ) );

	/* ------------------------
    *  Check if element that needs to be fixed at top of page is at the top.
    *
    ------------------------------*/

	if ( scrollCheck.length ) {
		distance = scrollCheck.offset().top - 120;
		$window = $( window );

		$window.scroll( function() {
			if ( $window.scrollTop() >= distance ) {
				if ( ! scrollCheck.hasClass( 'container' ) ) {
					scrollCheck.addClass( 'container fixed-top supportbtn-margin-top' );
					$( '.scroll-check > div' ).addClass( 'row justify-content-end' );
					$( '.scroll-check > div > div' ).addClass( 'col-sm-4 col-lg-3' );
				}
			}
			if ( $window.scrollTop() <= distance ) {
				if ( scrollCheck.hasClass( 'container' ) ) {
					scrollCheck.removeClass( 'container fixed-top stick-margin-top' );
					$( '.scroll-check > div' ).removeClass( 'row justify-content-end' );
					$( '.scroll-check > div > div' ).removeClass( 'col-sm-4 col-lg-3' );
				}
			}
		} );
	}

	/* ------------------------
    *  Text area auto-resize
    *  https://github.com/javierjulio/textarea-autosize/blob/master/dist/jquery.textarea_autosize.min.js
    *
    ------------------------------*/

	function adjustHeight( ctrl ) {
		const heightEl = ctrl.scrollHeight + 5;
		const heightBase = ctrl.scrollHeight;
		if ( 90 < ctrl.scrollHeight ) {
			$( ctrl ).delay( 200 ).animate( { height: heightEl }, 200 );
		} else {
			//$( ctrl ).animate({ height: 115 }, 200 );
			$( ctrl ).delay( 200 ).animate( { height: heightBase }, 200 );
		}
	}

	textAdjust = $( 'textarea' );
	count = textAdjust.length;

	textAdjust.each( function( event ) {
		const heightAdj = ( this );

		$( this ).height( 56 );

		if ( ! --count && 0 < count ) {
			$( this ).off( event );
		}
	} );

	textAdjust.on( 'focus blur', function( e ) {
		adjustHeight( this );
	} );

	textAdjust.on( 'keyup', function( e ) {
		const charCode = ( 'undefined' === typeof e.which ) ? e.keyCode : e.which;
		if ( charCode === 190 || charCode === 110 || charCode === 32 ) {
			adjustHeight( this );
		}
	} );

	textAdjust.on( 'blur', function() {
		// $( this ).animate({ height: 80 }, { duration: 200 });
	} );
} );
