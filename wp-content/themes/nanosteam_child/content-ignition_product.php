<?php
/**
 * The default template for displaying IgnitionDeck projects.
 */


if ( ! isset( $_GET['ig_embed_widget'] ) && isset( $post->ID ) ) {
	$post_id = $post->ID;
}

$project_id         = get_post_meta( $post_id, 'ign_project_id', true );
$project            = new ID_Project( $project_id );
$project_descrption = '' !== get_post_meta( $post_id, 'nano_project_short_description', true ) ? get_post_meta( $post_id, 'nano_project_short_description', true ) : get_post_meta( $post_id, 'ign_project_description', true );
$days_hours         = nano_project_days_left( $project->end_date() );
$raised             = nano_orders_raised( $post_id );
$raised             = '' !== $raised && $raised > 0 ? '$' . $raised : '';
$percent            = nano_percent_raised( $post_id );

$bar_val_percent = 100 > $percent ? number_format( $percent, 0 ) : 100;
$fund_goal       = number_format( absint( get_post_meta( $post_id, 'ign_fund_goal', true ) ), 0 );

$colors = get_option( 'krown_colors' );
$retina = krown_retina();

//Author
$author       = get_user_by( 'email', get_post_meta( $post_id, 'original_author', true ) ) != false ? get_user_by( 'email', get_post_meta( $post_id, 'original_author', true ) ) : get_user_by( 'id', get_post( $post_id )->post_author );
$author_name  = $author->display_name;
$author_title = get_user_meta( $author->ID, 'nano_user_title', true );
$author_image = get_user_meta( $author->ID, 'nano_idc_avatar', true ) != '' ? get_user_meta( $author->ID, 'nano_idc_avatar', true ) : null;
$author_image = isset( $author_image[0] ) && is_array( $author_image ) ? $author_image[0] : $author_image;
$author_image = isset( $author_image ) ? '<img alt="' . $author_name . '" src="' . $author_image . '" class="avatar photo mr-2 rounded-circle align-self-center" height="50" width="50">' : '<img alt="' . $author_name . '" src="' . get_stylesheet_directory_uri() . '/images/krown-gravatar.png" class="avatar avatar-50 photo mr-2 rounded-circle align-self-center" height="50" width="50">';

$img_obj = nano_project_image( $post_id, 'nano-crop-16-9-xs' );
//Check if shortcode or if in archive and display
$distance = isset( $distance ) && '' != $distance ? $distance : get_query_var( 'distance' );

$columns = 'col-lg-4 col-md-6';

if ( isset( $carousel ) ) {

	$deskColumns = 12 / ( isset( $atts['visible'] ) ? $atts['visible'] : ( isset( $atts['deskColumns'] ) ? $atts['deskColumns'] : '4' ) );
	$tabColumns  = 12 / ( isset( $atts['tabColumns'] ) ? $atts['tabColumns'] : '2' );
	$mobColumns  = 12 / ( isset( $atts['mobColumns'] ) ? $atts['mobColumns'] : '1' );
	$columns     = 'col-lg-' . $deskColumns . ' col-sm-' . $tabColumns . ' col-' . $mobColumns;

}

if ( is_mobile() == 'mobile' && isset( $carousel ) ) {

	echo '<li class="media border border-left-0 border-top-0 border-right-0 pb-3 mt-3 position-relative">';
	echo '<a class="card-link" href="' . get_the_permalink( $post_id ) . '"></a>';

	$img_obj = nano_project_image( $post_id, 'nano-crop-16-9-xs' );
	echo $img_obj;

	echo '<div class="media-body ml-3">';
	echo '<p class="m-0 p-0">'. get_the_title( $post_id ) .'</p>';

	echo '</div>';

	//  <h5 class="mt-0 mb-1">List-based media object</h5>
	//  Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
	//</div>'

	echo '</li>';

} else {

	?>
	<div class="<?php echo $columns; ?> d-sm-flex pb-sm-2">
		<div class="card my-2 bg-white">
			<a class="card-link" target="_parent" href="<?php the_permalink( $post_id ); ?>"></a>
			<div class="card-img-top">
				<?php

				$img_obj = nano_project_image( $post_id, 'nano-crop-16-9-xs' );
				echo $img_obj;

				?>

				<div class="progress">
					<div class="progress-bar bg-primary" role="progressbar"
							style="width: <?php echo $bar_val_percent ?>%"
							aria-valuenow="<?php echo $bar_val_percent; ?>" aria-valuemin="0" aria-valuemax="100"></div>
				</div>
			</div>

			<div class="card-body pb-1 pt-2 px-2 pt-sm-3 px-sm-3">
				<a href="<?php the_permalink( $post_id ); ?>">
					<h6 class="title text-uppercase heading-normal"><?php echo get_the_title( $post_id ); ?></h6>
				</a>
				<?php echo ( 'mobile' != is_mobile() ? '<p class="card-text small d-none d-lg-inline">' . wp_trim_words( $project_descrption, 12, '...' ) . '</p>' : '' ); ?>
			</div>

			<div class="card-footer pb-2 px-2 pb-sm-3 px-sm-3 row m-0">
				<?php if ( ! isset( $_GET['ig_embed_widget'] ) ) { ?>
					<div class="col-sm-12 p-0 pb-1 pb-sm-2">
						<div class="media">
							<?php echo $author_image; ?>
							<div class="media-body">
								<p class="m-0 pb-0 font-weight-bold clearfix"><?php echo $author_name; ?></p>
								<p class="text-muted small p-0 m-0"><?php echo $author_title; ?></p>
							</div>
						</div>
					</div>
				<?php } ?>
				<div class="col-lg-7 p-0">
					<span class="d-block small font-weight-bold"><?php echo( isset( $distance ) ? $distance : '' ); ?></span>
					<span class="mr-auto small font-weight-bold"><?php echo number_format( $percent, 0, '.', ',' ) . __( '% Funded of $', 'nanosteam_8n' ) . $fund_goal; ?></span>
				</div>
				<div class="col-lg-5 p-0 text-lg-right">
					<span class="small font-weight-bold"><?php
						$plural = $project->days_left() != 1 ? 's' : '';
						echo $project->days_left() . ' ' . $days_hours . ' ' . __( 'left', 'nanosteam_8n' );
						?></span>
				</div>
				<?php if ( isset( $_GET['ig_embed_widget'] ) ) { ?>
					<div class="col mt-1">
						<a href="<?php the_permalink( $post_id ); ?>" target="_parent"
								class="btn btn-small btn-primary btn-block">Support</a>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>

	<?php


}
