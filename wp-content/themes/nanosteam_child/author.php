<?php
/**
 * The template for displaying archives.
 */
get_header();

$creator_profile = nano_ide_creator_profile_display();
$creator_total   = isset( $creator_profile['total'] ) && 0 != $creator_profile['total'] ? '<a class="btn btn-sm px-3 flex-sm-fill text-sm-center active" id="created-tab" data-toggle="tab" href="#project_created" role="tab" aria-controls="project_created" aria-expanded="true"><sup>' . $creator_profile['total'] . '</sup> Project' . ( $creator_profile['total'] > 1 ? 's' : '' ) . ' Created</a>' : '';

$backer_profile = nano_ide_backer_profile_display();
$backer_total   = isset( $backer_profile['total'] ) && 0 != $backer_profile['total'] ? '<a class="btn btn-sm px-3 flex-sm-fill text-sm-center ' . ( '' == $creator_total ? 'active' : '' ) . '" id="backed-tab" data-toggle="tab" href="#project_backed" role="tab" aria-controls="project_backed"><sup>' . $backer_profile['total'] . '</sup> Project' . ( $backer_profile['total'] > 1 ? 's' : '' ) . ' Backed</a>' : '';

$labnotes       = labnotes();
$labnotes_total = isset( $labnotes['total'] ) && $labnotes['total'] > 0 ? '<a class="btn btn-sm px-3 flex-sm-fill text-sm-center" id="labnotes-tab" data-toggle="tab" href="#labnotes" role="tab" aria-controls="labnotes"><sup>' . $labnotes['total'] . '</sup> Lab Note' . ( $labnotes['total'] > 1 ? 's' : '' ) . '</a>' : '';

$endorsements       = endorsements_user_profile();
$endorsements_total = isset( $endorsements['total'] ) && $endorsements['total'] > 0 ? '<a class="btn btn-sm px-3 flex-sm-fill text-sm-center" id="endorsements-tab" data-toggle="tab" href="#endorsements" role="tab" aria-controls="endorsements"><sup>' . $endorsements['total'] . '</sup> Endorsement' . ( $endorsements['total'] > 1 ? 's' : '' ) . '</a>' : '';

?>

	<div class="ignitiondeck backer_profile">
		<div class="backer_info">
			<div class="row bg-primary py-5">
				<div class="container">
					<?php nano_profile_details(); ?>
				</div>
			</div>
			<div class="row pt-5">
				<div class="container">
					<?php nano_profile_badges(); ?>
					<div class="col-sm-12 border border-top-0 border-left-0 border-right-0">
						<nav class="nav nav-pills flex-column flex-sm-row">
							<?php echo $creator_total . $backer_total . $labnotes_total . $endorsements_total; ?>
						</nav>
					</div>
					<div class="tab-content">
						<div class="tab-pane fade p-md-5 p-3 show <?php echo( $creator_total != '' ? 'active' : '' ); ?>"
								id="project_created" aria-labelledby="created-tab" role="tabpanel">
							<?php echo $creator_profile['content']; ?>
						</div>
						<div class="tab-pane fade p-md-5 p-3 show <?php echo( $creator_total == '' ? 'active' : '' ); ?>"
								id="project_backed" aria-labelledby="backed-tab" role="tabpanel">
							<?php echo $backer_profile['content']; ?>
						</div>
						<div class="tab-pane fade p-md-5 p-3 show" id="labnotes" aria-labelledby="labnotes-tab"
								role="tabpanel">
							<?php echo $labnotes['content']; ?>
						</div>
						<div class="tab-pane fade p-md-5 p-3 show" id="endorsements" aria-labelledby="endorsements-tab"
								role="tabpanel">
							<?php echo $endorsements['content']; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php
get_footer();
