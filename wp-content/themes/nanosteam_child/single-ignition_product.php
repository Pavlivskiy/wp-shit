<?php
/**
 * The Template for displaying all single projects.
 */
get_header();

?>

<?php while ( have_posts() ) : the_post();

	$_SESSION['order_type']         = false;
	$_SESSION['nano_project_price'] = false;
	$_SESSION['nano_project_id']    = false;

	$post_id         = $post->ID;
	$project_id      = get_post_meta( $post_id, 'ign_project_id', true );
	$project         = new ID_Project( $project_id );
	$raised          = nano_orders_raised( $post_id );
	$raised          = '' !== $raised && $raised > 0 ? '$' . $raised : '';
	$percent         = nano_percent_raised( $post_id );
	$bar_val_percent = '' !== $percent ? $percent : '';
	$fund_goal       = number_format( absint( get_post_meta( $post_id, 'ign_fund_goal', true ) ), 0 );
	$ends_on         = ( get_post_meta( $post_id, 'ign_project_closed', true ) == 1 ? 'Ended on ' : 'Ends on ' ) . get_post_meta( $post_id, 'ign_fund_end', true );
	$days_hours      = nano_project_days_left( $project->end_date() );

	$_SESSION['nano_project_id'] = $project_id; // Setting the project ID for token transfer later on.
	$faq_updates_attr            = array( 'product' => $project_id );
	$author                      = get_user_by( 'email', get_post_meta( $post_id, 'original_author', true ) ) != false ? get_user_by( 'email', get_post_meta( $post_id, 'original_author', true ) ) : get_user_by( 'id', get_post( $post_id )->post_author );

	//Author
	$author_name     = $author->display_name;
	$author_title    = get_user_meta( $author->ID, 'nano_user_title', true );
	$author_bio      = wpautop( get_post_meta( $post_id, 'nano_team_leader_bio', true ) );
	$nano_avatar     = get_user_meta( $author->ID, 'nano_idc_avatar', true );
	$nano_avatar_img = isset( $nano_avatar[0] ) && is_array( $nano_avatar ) ? $nano_avatar[0] : $nano_avatar;
	$author_image    = '<img class="img-fluid rounded-circle" src="' . $nano_avatar_img . '">';

	$updates = html_entity_decode( stripslashes( apply_filters( 'idcf_updates_text', get_post_meta( $post_id, 'ign_updates', true ) ) ) );

	$faqs                      = wpautop( html_entity_decode( stripslashes( get_post_meta( $post_id, 'ign_faqs', true ) ) ) );
	$video                     = get_post_meta( $post_id, 'ign_product_video', true );
	$img_1                     = get_post_meta( $post_id, 'project_main', true ) != '' ? get_post_meta( $post_id, 'project_main', true ) : ( has_post_thumbnail( $post_id ) ? wp_get_attachment_url( get_post_thumbnail_id(), 'full' ) : get_post_meta( $post_id, 'ign_product_image1', true ) );
	$gallery                   = array(
		$img_1,
		get_post_meta( $post_id, 'ign_product_image2', true ),
		get_post_meta( $post_id, 'ign_product_image3', true ),
		get_post_meta( $post_id, 'ign_product_image4', true ),
	);
	$project_long_desc         = wpautop( ( '' !== get_post_meta( $post_id, 'nano_project_long_description', true ) ? get_post_meta( $post_id, 'nano_project_long_description', true ) : get_post_meta( $post_id, 'project_long_description', true ) ) );
	$nano_abstract             = wpautop( ( '' !== get_post_meta( $post_id, 'nano_project_short_description', true ) ? get_post_meta( $post_id, 'nano_project_short_description', true ) : get_post_meta( $post_id, 'project_short_description', true ) ) );
	$retina                    = krown_retina();
	$nano_location_city        = get_post_meta( $post_id, 'nano_location_city', true );
	$nano_location_state       = get_post_meta( $post_id, 'nano_location_state', true );
	$nano_goal_timeline        = wpautop( get_post_meta( $post_id, 'nano_goal_timeline', true ) );
	$nano_team_leader          = get_post_meta( $post_id, 'nano_team_leader_name', true );
	$nano_team_leader_title    = get_post_meta( $post_id, 'nano_team_leader_title', true );
	$nano_team_leader_bio      = wpautop( get_post_meta( $post_id, 'nano_team_leader_bio', true ) );
	$nano_project_significance = wpautop( get_post_meta( $post_id, 'nano_project_significance', true ) );
	$nano_goal_milestones      = get_post_meta( $post_id, 'nano_goal_milestones', true );
	$nano_goal_milestone_date  = get_post_meta( $post_id, 'nano_goal_milestone_date', true );
	$nano_budget_description   = wpautop( get_post_meta( $post_id, 'nano_budget_description', true ) );
	$nano_budget_title         = get_post_meta( $post_id, 'nano_budget_title', true );
	$nano_budget_value         = get_post_meta( $post_id, 'nano_budget_value', true );
	$max_donation_amt          = get_option( 'max_donation_amount', 10000 );

	if ( is_array( $nano_goal_milestone_date) ) {
		uasort( $nano_goal_milestone_date, 'date_compare' );
	}
	// Milstones
	$i         = 0;
	$milestone = '';

	if ( isset( $nano_goal_milestones ) && '' != $nano_goal_milestones && is_array( $nano_goal_milestones ) && count( $nano_goal_milestones ) > 0 ) {

		$len = count( $nano_goal_milestones );

		$milestone .= '<div class="nano-milestones pl-sm-4">';
		foreach ( $nano_goal_milestone_date as $key => $nano_goal_milestone_dat ) {
			$last = '';
			if ( $i == $len - 1 ) {
				$last = 'nano-date-last ';
			}

			$milestone .= '<div class="d-table-row">';
			$milestone .= '<dt class="' . $last . 'nano-date d-table-cell pb-2"><strong>' . $nano_goal_milestone_dat . '</strong></dt>';
			$milestone .= '<dd class="nano-goal d-table-cell pl-2 pb-2">' . ( isset( $nano_goal_milestones[ $key ] ) ? $nano_goal_milestones[ $key ] : '' ) . '</dd>';
			$milestone .= '</div>';
			$i ++;
		}
		$milestone .= '</div>';
	}

	ip_info( 'visitor', 'Location' );

	//Team Members
	$nano_team_members = get_post_meta( $post_id, 'nano_team_members', true );
	$team_member_links = nano_team_links( $post_id );

	$project_descrption = '' !== get_post_meta( $post_id, 'nano_project_long_description', true ) ? get_post_meta( $post_id, 'nano_project_long_description', true ) : get_post_meta( $post_id, 'ign_project_description', true );
	?>

	<div class="row">

		<div class="col project-details">

			<h5 class="text-primary text-uppercase title"><?php the_title(); ?></h5>

			<h1 class="py-4 w-75 d-none d-sm-block"><?php echo wp_trim_words( $nano_abstract, 10, '...' ); ?></h1>
			<h5 class="d-sm-none"><?php echo wp_trim_words( $nano_abstract, 10, '...' ); ?></h5>

			<p class="small mb-1 mb-sm-2"><i class="fa fa-user-o fa-fw text-primary" aria-hidden="true"></i> Project By&nbsp <?php echo $team_member_links; ?>
				<?php
				if ( $nano_location_city || $nano_location_state ) {

					echo ' <span class="">Based in ' . $nano_location_city . ( $nano_location_state && $nano_location_city ? ', ' : '' ) . $nano_location_state . '.</span>';

				}
				?>
			</p>

		</div>

	</div>

	<div class="row pb-2 pb-sm-5">

		<div class="col-md-8 col-lg-9">

				<?php

				if ( '' != $video ) {

					$parsed_video = parse_url( $video );
					if ( empty( $parsed_video['scheme'] ) ) {
						$video = 'https://' . ltrim( $video, '/' );
					}

					if ( strpos( $video, 'iframe' ) > - 1 ) {
						echo html_entity_decode( stripslashes( $video ) );
					} else {
						echo '<div class="embed-responsive embed-responsive-16by9">';
						echo $wp_embed->run_shortcode( '[embed width="845" height="474"]' . $video . '[/embed]' );
						echo '</div>';
					}

				} else {

					foreach ( $gallery as $slide ) {
						if ( '' != $slide ) {
							$thumbnail_url = wp_get_attachment_image_src( ( get_post_meta( $post_id, 'project_main', true ) != '' ? get_post_meta( $post_id, 'project_main', true ) : get_post_thumbnail_id() ), 'nano-crop-16-9-sm', true );
							echo '<img class="img-fluid" src="' . $thumbnail_url[0] . '" style="width:100%" alt="" />';
						}
					}
				}

				?>


		</div>

		<div class="col-md-4 col-lg-3">
			<div class="row">

			<?php

			$author_check                 = false;
			$endorser_check				  = false;
			$org_author                   = false;
			$project_status               = false;
			$creator_args                 = array( 'field' => 'name' );
			$checking                     = wp_get_post_terms( get_the_ID(), 'author', $creator_args );
			$project_status               = nano_funding_status( get_the_ID() );
			$original_author              = get_post_meta( get_the_ID(), 'original_author', true );
			$nano_current_user            = wp_get_current_user()->user_login;
			$check_current_user_if_member = multi_dimen_array_search_all_keys( $checking, 'name', $nano_current_user );

			if ( isset( $_GET['post_type'] ) && $_GET['post_type'] == 'ignition_product' && isset( $_GET['preview'] ) && $_GET['preview'] == '1' ) {
				$author_check = true;
			}

			// Endorsers
			if ( isset( $_GET['post_type'] ) && $_GET['post_type'] == 'ignition_product' && isset( $_GET['p'] ) && false !== is_user_logged_in() ) {
				$endorsed_projects = get_user_meta( wp_get_current_user()->ID, 'project_endorsement_connection', true );
				$endorsements      = get_post_meta( $post_id, 'nano_endorsements', true );


				if ( in_array( $_GET['p'], $endorsed_projects ) && ! in_array( $_GET['p'], $endorsements ) ) {
					$endorser_check = true;
				}

				if ( is_array( $endorsements ) && count( $endorsements ) > 0 ) {

					foreach ( $endorsements as $key => $endorsement ) {

						if ( isset( $endorsement[ $nano_current_user ] ) ) {

							$endorser_check = false;

						}
					}
				}
			}

			if ( is_array( $check_current_user_if_member ) ) {

				$author_check = true;

				if ( 'publish' == get_post_status() && ( 'SUCCESS' === $project_status || 'COMPLETED' === $project_status ) ) {
					$author_check   = true;
					$project_status = true;
				}

				if ( $nano_current_user === $original_author ) {
					$author_check = true;
					$org_author   = true;

					// If the project thumbnail has not been set, we'll set it here too.

					$thumb_check = get_post_meta( $post_id, 'project_main', true );
					$thumb_meta  = get_post_meta( $post_id, '_thumbnail_id', true );

					if ( '' !== $thumb_check && '' == $thumb_meta ) {

						set_post_thumbnail( $post_id, $thumb_check );

					}
				}
			}

			?>

				<div class="col-12 order-1 order-sm-1">
					<?php if ( true === $org_author ) { ?>
					<?php echo ( ( true === $author_check && $project_status === true ) || true === $org_author ? '<a href="' . md_get_durl() . '?edit_project=' . get_the_ID() . '"  class="btn btn-primary btn-block mb-2"><strong>' . ( true === $org_author ? __( 'Edit your project', 'nanosteam_8n' ) : __( 'Add a Lab Note', 'nanosteam_8n' ) ) . '</strong></a>' : '' ); ?>
					<?php } ?>
					<?php if ( true === $endorser_check ) { ?>
						<?php echo '<a href="' . md_get_durl() . '/dashboard/?edit-profile"  class="btn btn-primary btn-block mb-2"><strong>' . __( 'Endorse this project', 'nanosteam_8n' ) . '</strong></a>'; ?>
					<?php } ?>
				</div>
				<div class="col-4 col-sm-12 d-none d-sm-block order-sm-2">
					<h1 class="text-primary"><?php echo $raised; ?></h1>
				</div>
				<div class="col-sm-12 order-2 order-sm-3 d-sm-none">
					<div class="progress v-large mt-3">
						<div class="progress-bar bg-primary" role="progressbar" style="width: <?php echo $bar_val_percent ?>%"
								aria-valuenow="<?php echo $bar_val_percent; ?>" aria-valuemin="0" aria-valuemax="100">
						</div>
					</div>
				</div>
				<div class="col-6 col-sm-12 order-3 order-sm-3">
					<h4 class="m-0 d-sm-none text-primary"><?php echo $raised ?></h4>
					<p class="m-0 d-sm-none text-muted small"><?php echo __( ' pledged of $', 'nanosteam_8n' ) . $fund_goal; ?></p>
					<p class="d-none d-sm-block"><?php echo $bar_val_percent . ' ' . __( '% raised of $', 'nanosteam_8n' ) . $fund_goal; ?></p>
					<div id="max_donation_amt" class="d-none"><?php echo $max_donation_amt; ?></div>
				</div>
				<div class="col-6 col-sm-12 order-4 order-sm-5">
					<h4 class="m-0 d-sm-none text-right"><?php echo $project->days_left(); ?></h4>
					<p class="m-0 d-sm-none text-muted small text-right"><?php echo $days_hours . ' ' . __( 'left', 'nanosteam_8n' ) ; ?></p>
					<p class="d-none d-sm-block"><?php echo $project->days_left() . ' ' . $days_hours . ' ' . __( 'left', 'nanosteam_8n' ); ?></p>
				</div>
				<div class="col-12 order-5 order-sm-4">
					<hr class="d-sm-none">
					<?php nano_backers( $project_id ); ?>
				</div>
				<div class="col-12 order-1 order-sm-6">
					<?php echo( $project->days_left() > 0 && true != $author_check && 'publish' == get_post_status() ? '<div class="ign-supportnow scroll-check bg-sm-white pt-2 pt-sm-0"><a href="" data-toggle="modal" data-target=".idc_lightbox" class="btn btn-primary btn-block mb-2 mb-sm-4">' . __( 'Give', 'nanosteam_8n' ) . '</a></div>' : '' ); ?>
					<p class="small muted d-none d-sm-block"><?php echo $ends_on; ?></p>
				</div>
				<div class="col-12 order-6 order-sm-7">
					<?php krown_share_buttons( $post_id ); ?>
				</div>

			</div>
		</div>

	</div>

	<div class="row">

		<div class="col-sm-12">

			<nav class="nav nav-pills border border-top-0 border-left-0 border-right-0">
				<a class="btn btn-sm border-bottom-2 px-2 px-sm-3 nav-item nav-link text-sm-center  active" data-toggle="tab" href="#overview"
						role="tab" aria-controls="overview"
						aria-expanded="true"><?php _e( 'Campaign', 'nanosteam_8n' ); ?></a>
				<a id="labnote_lnk" class="btn btn-sm px-2 px-sm-3 nav-item nav-link text-sm-center " data-toggle="tab" href="#labnotes" role="tab"
						aria-controls="labnotes"><?php echo  __( 'Lab Note', 'nanosteam_8n' ) . ( count_lab_notes( $faq_updates_attr ) == 1 ? '' : 's' ) . '<sup>' . count_lab_notes( $faq_updates_attr ) . '</sup>'; ?></a>
				<a class="btn btn-sm px-2 px-sm-3 nav-item nav-link text-sm-center " data-toggle="tab" href="#faq" role="tab"
						aria-controls="faq"><?php _e( 'FAQ', 'nanosteam_8n' ); ?></a>
				<a class="btn btn-sm px-2 px-sm-3 nav-item nav-link text-sm-center  " data-toggle="tab" href="#discussion" role="tab"
						aria-controls="discussion"><?php comments_number( 'Discussion', 'Discussion <sup>1</sup>', ' Discussions <sup>%</sup>' ); ?></a>
			</nav>

		</div>

		<div class="col-md-12 col-lg-9">

			<!-- Tab panes -->
			<div class="tab-content">

				<div class="tab-pane fade p-md-5 p-3 show active" id="overview" aria-labelledby="overview-tab"
						role="tabpanel">

					<?php

					echo '<h5 class="mb-4">' . __( 'Goals', 'nanosteam_8n' ) . '</h5>';

					echo '<div class="pl-sm-4">';

					echo do_action( 'id_content_before', $project_id );

					the_content();

					if ( '' != $project_long_desc ) {
						echo nl2br( html_entity_decode( $project_long_desc ) );
					}

					if ( function_exists( 'idstretch_install' ) ) {
						echo do_shortcode( '[project_stretch_goals product="' . $project_id . '"]' );
					}

					echo '</div>';

					if ( isset( $nano_abstract ) && '' != $nano_abstract ) {
						echo '<h5 class="pt-5 mb-4">' . __( 'Abstract', 'nanosteam_8n' ) . '</h5>';
						echo '<div class="pl-sm-4">';
						echo $nano_abstract;
						echo '</div>';
					}
					if ( isset( $nano_project_significance ) && '' != $nano_project_significance ) {
						echo '<h5 class="pt-5 mb-4">' . __( 'Project Significance', 'nanosteam_8n' ) . '</h5>';
						echo '<div class="pl-sm-4">';
						echo $nano_project_significance;
						echo '</div>';
					}
					if ( isset( $nano_goal_timeline ) && '' != $nano_goal_timeline ) {
						echo '<h5 class="pt-5 mb-4">' . __( 'Timeline', 'nanosteam_8n' ) . '</h5>';
						echo '<div class="pl-sm-4">';
						echo $nano_goal_timeline;
						echo '</div>';
					}
					if ( isset( $milestone ) && '' != $milestone ) {
						echo '<h5 class="pt-5 mb-4">' . __( 'Milestones', 'nanosteam_8n' ) . '</h5>';
						echo $milestone;
					}
					if ( ( isset( $nano_budget_description ) && '' != $nano_budget_description ) || ( isset( $nano_budget_title ) && '' != $nano_budget_title ) || ( isset( $nano_budget_value ) && '' != $nano_budget_value ) ) {
						echo '<h5 class="pt-5 mb-4">' . __( 'Budget', 'nanosteam_8n' ) . '</h5>';
						echo '<div class="row">';
						echo '<div class="pl-sm-4 col-sm-12">';
						echo $nano_budget_description;
						echo '</div>';
						echo '<div class="pl-sm-4 col-sm-4 col-8 offset-2 offset-sm-0 pb-3 pb-md-0">';
						echo '<canvas id="budget_chart" class="pub" width="400" height="400"></canvas>';
						echo '</div>';
						echo '<div class="col-sm-8 d-flex align-items-center"><div id="legend" class="pb-3 w-100"></div></div>';
						echo '</div>';
					}

					endorsements_frontend( $post_id );

					if ( isset( $author_name ) && '' != $author_name ) {
						echo '<h5 class="pt-5 mb-4">' . __( 'Meet the team', 'nanosteam_8n' ) . '</h5>';
						echo '<div class="pl-sm-4">';
						echo '<div class="media mb-3">';
						echo '<a class="float-left mr-3" href="' . get_author_posts_url( $author->ID ) . '">' . $author_image . '</a>';
						echo '<div class="media-body">';
						echo '<h6>' . $author_name . ' <small class="text-muted">' . $author_title . '</small></h6>';
						echo $author_bio;
						echo '</div></div></div>';
						meet_the_team( $post_id );
					}

					?>

				</div>


				<div class="tab-pane fade py-5" id="labnotes" aria-labelledby="labnotes-tab" role="tabpanel">
					<div class="col d-md-none">
					<?php echo ( true === $org_author && true === $project_status ? '<a href="' . md_get_durl() . '?edit_project=' . get_the_ID() . '&labnotes#labnotes"  class="btn btn-primary btn-block mb-2"><strong>' . __( 'Add a Lab Note', 'nanosteam_8n' ) . '</strong></a>' : '' ); ?>
					</div>
					<?php
					if ( '' != $updates || has_action( 'id_updates' ) == true  && count_lab_notes( $faq_updates_attr ) != '' ) {
						echo '<div class="row">';
						echo apply_filters( 'id_updates', '', $faq_updates_attr );
						echo '</div>';
					} else {
						echo '<div style="height: 200px" class="px-md-5 pb-5 px-3 mb-5">';
						echo __( 'This project has not shared any Labnotes yet.', 'nanosteam_8n' );
						echo '</div>';
					}
					?>
				</div>

				<div class="tab-pane fade p-md-5 p-3" id="faq" aria-labelledby="faq-tab" role="tabpanel">
					<?php
					if ( '' != $faqs ) {
						echo apply_filters( 'id_faqs', '', $faq_updates_attr );
						echo $faqs;
					} else {
						echo '<div style="height: 200px" class="pb-5 mb-5">';
						echo __( 'This project has not shared any FAQs yet.', 'nanosteam_8n' );
						echo '</div>';
					}
					?>
				</div>
				<div class="tab-pane fade p-md-5 p-3" id="discussion" aria-labelledby="discussion-tab"
						role="tabpanel"><?php comments_template(); ?></div>
			</div>

		</div>

		<div class="col-sm-3 py-md-5">
			<?php echo ( true === $author_check && true === $project_status ? '<a href="' . md_get_durl() . '?edit_project=' . get_the_ID() . '&labnotes#labnotes" id="add_lab_note" class="btn btn-primary btn-block mb-2 d-none"><strong>' . __( 'Add a Lab Note', 'nanosteam_8n' ) . '</strong></a>' : '' ); ?>
		</div>

	</div>

<?php endwhile;

include NANO_STRIPE . '/library/idc_templates/_lbLevelSelect.php';

?>

<?php get_footer(); ?>