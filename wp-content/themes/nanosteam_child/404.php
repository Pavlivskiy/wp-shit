<?php
/**
 * 404 Page Template
 */

$standard            = __( 'We are really sorry but the page you requested is missing.', 'nanosteam_8n' );
$link                = '<a title="Sign In" id="show_login" href="">Sign In</a>';
$endorser_teammember = sprintf( esc_html__( 'Sorry, to see this please %s in first.', 'nanosteam_8n' ), $link );
$message             = isset( $_GET['p'] ) && isset( $_GET['post_type'] ) && false === is_user_logged_in() ? '<h2>' . $endorser_teammember . '</h2>' : '<h2>' . $standard . '</h2>';

get_header();
echo $message;
rewind_posts();
get_footer();

?>