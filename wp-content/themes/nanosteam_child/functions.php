<?php if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }


/* ------------------------
 * Disable Backer Theme Page Titles
 * Disable krown posts per page
 *
------------------------------*/

function krown_check_page_title() {}
function krown_id_ppp( $query ) {}


/* ------------------------
 * General Site Wide Functions
 *
------------------------------*/

require_once( get_stylesheet_directory() . '/library/functions/nano_functions.php' );


/* ------------------------
 * Gutenburg
 *
------------------------------*/

// require_once( get_stylesheet_directory() . '/library/gutenburg/nanosteam_gutenberg.php' );



/* ------------------------
 * Custom Shortcodes
 *
------------------------------*/

require_once( get_stylesheet_directory() . '/library/shortcodes/nanostream_shortcodes.php' );


/* ------------------------
 * A Better Nav Walker
 * The default Krown nav_walker did not take into account custom post types and regular post types for
 * Ancestor highlighting.
 *
------------------------------*/

require_once( get_stylesheet_directory() . '/library/functions/nav_walker.php' );


/* ------------------------
 * Login / Register / Lost Password Ajax Modal Scripts and Forms
 *
------------------------------*/

require_once( get_stylesheet_directory() . '/library/login_register/custom-ajax-auth.php' );

