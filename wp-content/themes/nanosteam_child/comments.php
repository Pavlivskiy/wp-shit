<?php
/**
 * Comments template
 */
?>

<?php if ( post_password_required() ) : ?>
	<p class="post-excerpt"><?php _e( 'This post is password protected. Enter the password to view any comments.', 'krown' ); ?></p>
	<?php
	return;
endif;
?>

	<aside id="comments">

		<div class="form-row">

			<div class="col-sm">

				<?php if ( ! comments_open() ) : ?>

					<p class="post-excerpt"><?php _e( 'Discussionss are closed.', 'krown' ); ?></p>

				<?php else : ?>

					<h3 id="comments-title"><?php _e( 'Discussion', 'krown' ); ?>
						(<?php comments_number( '0', '1', '%' ); ?>)</h3>

				<?php endif; ?>

				<?php if ( ! have_comments() ) : ?>

					<p class="post-excerpt"><?php _e( 'There isn\'t a discussion yet. Be the first one!', 'krown' ); ?></p>

				<?php endif; ?>

			</div>

		</div>

		<div id="comments-list" class="row"><?php wp_list_comments( array( 'callback' => 'nano_comment' ) ); ?></div>

		<?php paginate_comments_links(); ?>

		<?php

		$defaults = array(
			'fields'               => apply_filters( 'comment_form_default_fields', array(
				'author' => '<div class="form-row form-group"><div class="col-sm""><label for="author">' . __( 'Name', 'krown' ) . '</span></label><input id="author" class="form-control" name="author" type="text" value=""/></div>',
				'email'  => '<div class="col-sm"><label for="email">' . __( 'Email', 'krown' ) . '</span></label><input id="email" class="form-control" name="email" type="text" value="" /></div>',
				'url'    => '<div class="col-sm"><label for="url">' . __( 'Website', 'krown' ) . '</span></label><input id="url" class="form-control"  name="url" type="text" value="" /></div></div>'
			) ),
			'comment_field'        => '<div class="form-row form-group"><div class="col-sm-12" style="margin-left:0"><label for="comment">' . __( 'Your Comment', 'krown' ) . '</span></label><textarea class="form-control" id="comment" name="comment" rows="8"></textarea></div></div>',
			'must_log_in'          => '<p class="must-log-in">' . sprintf( __( 'You must be <a class="show_login" href="%s">logged in</a> to join the discussion.', 'krown' ), '' ) . '</p>',
			'logged_in_as'         => '<p class="logged-in-as">' . sprintf( __( 'You are logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>' ), admin_url( 'profile.php' ), $user_identity, wp_logout_url( apply_filters( 'the_permalink', get_permalink() ) ), 'krown' ) . '</p>',
			'comment_notes_before' => '',
			'comment_notes_after'  => '',
			'id_form'              => 'comment-form',
			'class_form'           => '',
			'id_submit'            => 'submit',
			'class_submit'         => 'btn btn-sm btn-primary',
			'title_reply'          => __( 'Write a comment', 'krown' ),
			'title_reply_to'       => __( 'Leave a Reply to %s', 'krown' ),
			'cancel_reply_link'    => __( 'Cancel reply', 'krown' ),
			'label_submit'         => __( 'Comment', 'krown' ),
		);

		comment_form( $defaults );

		?>

	</aside>