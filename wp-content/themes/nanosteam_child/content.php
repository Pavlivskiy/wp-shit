<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 */
?>

<hr>
<div>
	<?php
	$img_obj = '';

	if ( has_post_thumbnail( $post->ID ) || get_post_meta( $post->ID, 'project_main', true ) != '' ) {
		$thumbnail_url = wp_get_attachment_image_src( ( get_post_meta( $post->ID, 'project_main', true ) != '' ? get_post_meta( $post->ID, 'project_main', true ) : get_post_thumbnail_id() ), 'nano-crop-16-9-xs', true );
		$img_obj       = '<a class="" href="' . get_permalink( $post->ID ) . '"><img class="card-img-top img-fluid" src="' . $thumbnail_url[0] . '" width="' . $thumbnail_url[1] . '" height="' . $thumbnail_url[2] . '" alt="' . get_the_title() . '" /></a>';
	} elseif ( get_post_meta( $post->ID, 'ign_product_image1', true ) != '' ) {
		$image   = aq_resize( get_post_meta( $post->ID, 'ign_product_image1', true ), 'true' === $retina ? 510 : 255, null, false, false );
		$img_obj = '<a class="" href="' . get_permalink( $post->ID ) . '"><img class="card-img-top img-fluid" src="' . $image[0] . '" width="' . $image[1] . '" height="' . $image[2] . '" alt="' . get_the_title() . '" /></a>';
	} elseif ( get_post_meta( $post->ID, 'ign_product_video', true ) != '' ) {
		$video   = get_post_meta( $post->ID, 'ign_product_video', true );
		$video   = '' . check_video_url( $video ) . '';
		$img_obj = '<a class="" href="' . get_permalink( $post->ID ) . '"><img class="video-img card-img-bottom img-fluid" src="' . $video . '" alt="' . get_the_title() . '" /></a>';
	} else {
		$img_obj = '<img style="object-fit: cover;" class="img-fluid mb-sm-0 mb-2" src="' . get_stylesheet_directory_uri() . '/images/dashboard-download-placeholder.jpg' . '" alt="' . get_the_title() . '" />';
	}


	echo( '' == $img_obj ? '' : '<div class="row">' );
	echo( '' == $img_obj ? '' : '<div class="col-sm-2">' . $img_obj . '</div>' );

	?>

	<div class="<?php echo( '' == $img_obj ? 'offset-sm-2 col-sm-10' : 'col-sm-10' ); ?>">
		<div class="row">
			<div class="col-sm-12">
				<a href="<?php the_permalink(); ?>"><h6 class="card-title"><?php the_title(); ?></h6></a>
				<p class="card-text small"><?php echo wp_trim_words( krown_excerpt( 'krown_excerptlength_post_big' ), 18, '...' ); ?></p>
			</div>
		</div>
		<div class="row mt-2">
			<div class="col">
				<small><a href="<?php echo get_permalink(); ?>">
						<time pubdate
								datetime="<?php the_time( 'c' ); ?>"><?php the_time( __( 'F j, Y', 'krown' ) ); ?></time>
					</a>
					<?php if ( 'page' !== get_post_type() ) { ?> - <a href="<?php echo get_permalink(); ?>#discussion"><?php echo __( 'Comments', 'krown' ) . ' ' . get_comments_number( '0', '1', '%' ); ?></a><?php } ?>
					- <a href="<?php echo get_permalink(); ?>" class=""><?php _e( 'View', 'krown' ); ?></a></small>
			</div>
		</div>
	</div>

	<?php echo( '' == $img_obj ? '' : '</div>' ); ?>

</div>