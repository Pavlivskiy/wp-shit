<?php /* Template Name: Checkout Page Template */

get_header();

// Expire tokens that should be expired for the user if they haven't visited their profile page.
nano_user_token_update( '' );

// Nanosteams default token project
// TODO add meta box control to nanasteam admin for this.
$token_project_id = nano_get_default_project_id();

$_SESSION['nano_project_price'] = isset( $_GET['price'] ) ? $_GET['price'] : '';

// Reset the session cookie
$_SESSION['loaded']     = false;
$_SESSION['order_type'] = false;

$_SESSION['nano_project_id'] = '';
//Confirm that the project ID matches from the project page
if ( isset( $_GET['purchaseform'] ) ) {
	if ( isset( $_GET['mdid_checkout'] ) ) {
		$_SESSION['nano_project_id'] = $_GET['mdid_checkout'] == $_SESSION['nano_project_id'] ? $_SESSION['nano_project_id'] : $_GET['mdid_checkout'];
	} elseif ( isset( $_GET['prodid'] ) ) {
		$_SESSION['nano_project_id'] = $_GET['prodid'] == $_SESSION['nano_project_id'] ? $_SESSION['nano_project_id'] : $_GET['prodid'];
	}
}

// Setting the project ID for token transfer later on.

// Original project ID
$supported_project_id = isset( $_SESSION['nano_project_id'] ) ? $_SESSION['nano_project_id'] : ( isset( $_GET['mdid_checkout'] ) ? $_GET['mdid_checkout'] : ( isset( $_GET['prodid'] ) ? $_GET['prodid'] : '' ) );
$project              = new ID_Project( $supported_project_id );
$post_id              = $project->get_project_postid();

$project_title = get_the_title( $post_id );

echo '<div class="row"><div class="container py-5">';

echo '<div class="row"><div class="col-sm-12"></div></div>';

if ( have_posts() ) {

	while ( have_posts() ) :

		the_post();

		if ( isset( $project_title ) && '' != $project_title && isset( $_GET['purchaseform'] ) ) {

			?>

			<div class="about-to-support text-center">
				<span class="open_checkout_modal">
					<a class="btn btn-sm btn-primary" href="#" id="re-open" data-id="<?php echo $post_id; ?>">Click to re-open</a>
				</span>
			</div>

			<div class="modal fade" tabindex="-1" role="dialog" id="nano-checkout">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">

						<div class="modal-body">
							<div class="row"><div class="col-sm-12">
									<span class="payment-errors d-none"></span>
									<div class="nano_status_checkout alert alert-info col-sm-12 fade"></div>
								</div>
							</div>
							<h2>Thank you for supporting Nanosteam!</h2>
							<p class="mb-4">Our role is to connect great minds and great ideas to people with a greater sense of
								good.</p>
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</div>

			<?php

		}

		//the_content();
		wp_link_pages(
			array(
				'before' => '<p class="wp-link-pages"><span>' . __( 'Pages:', 'krown' ) . '</span>',
			)
		);

		if ( comments_open() && ot_get_option( 'krown_allow_page_comments', 'false' ) == 'true' ) {
			comments_template( '', true );
		}

		?>
		<?php
	endwhile;
}

echo '</div></div>';

?>

<div class="modal fade" tabindex="-1" role="dialog" id="nano-terms-conditions">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<?php echo nano_modal_page_load( 'terms-of-service' ); ?>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn-sm btn btn-secondary" data-dismiss="modal"><i class="fa fa-close"></i>
				</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="nano-privacy">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<?php echo nano_modal_page_load( 'privacy' ); ?>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn-sm btn btn-secondary" data-dismiss="modal"><i class="fa fa-close"></i>
				</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
    jQuery( document ).ready( function( e ) {
        if ( jQuery( '.checkout-wrapper' ).length ) {
            jQuery( '#nano-checkout' ).modal( { backdrop: 'static', keyboard: false } );
        }
    } );
</script>

<?php get_footer(); ?>
